PVector center;

void setup() {
  size(800, 800);
  center = new PVector(width/2, height/2);
}

void draw() {
  background(0);
  PVector mouse = new PVector(mouseX, mouseY);
  PVector position = PVector.sub(mouse, center);

  //position.normalize();
  //position.div(position.mag());
  //position.mult(10);
  //position.setMag(10);
  position.limit(200);

  pushMatrix();
  stroke(255);
  strokeWeight(5);
  translate(width/2, height/2);
  line(0, 0, position.x, position.y);
  popMatrix();

  fill(255);
  noStroke();
  textSize(40);
  text("mag: " + position.mag(), 10, 40);
  text("angle: " + degrees(position.heading()), 10, 90);
}
