Ball ball[];
int numBall = 30;

void setup() {
  size(1200, 800);
  background(0);
  ball = new Ball[numBall];
  for (int i=0; i<numBall; i++) {
    ball[i] = new Ball();
  }
  colorMode(HSB, 100);
}

void draw() {
  background(0);
  for (int i=0; i<numBall; i++) {
    ball[i].update();
    ball[i].display();
  }
}
