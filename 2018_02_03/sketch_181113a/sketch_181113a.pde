XML xml;

StringList names;
IntList ages;
StringList colors;

void setup() {
  xml = loadXML("animal.xml");
  XML[] children = xml.getChildren("animal");

  //print(xml);

  //print(children);
  names = new StringList();
  ages = new IntList();
  colors = new StringList();

  for (int i=0; i<children.length; i++) {
    println(children[i].getContent());
    println(children[i].getInt("age"));
    println(children[i].getString("color"));
    names.set(i, children[i].getContent());
    ages.set(i, children[i].getInt("age"));
    colors.set(i, children[i].getString("color"));
  }

  size(600, 600);
  textAlign(CENTER, CENTER);
  textSize(62);
}

void draw() {
  background(0);
  fill(255);
  noStroke();

  for (int i = 0; i<names.size(); i++) {
    String txt = names.get(i) + ": " + ages.get(i) + " " + colors.get(i);
    text(txt, width/2, 100+i*80);
  }
}
