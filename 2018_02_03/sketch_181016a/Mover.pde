class Mover{
  PVector pos;
  PVector speed;
  float tx, ty, txSpeed, tySpeed;
  float speedX_minMax, speedY_minMax;
  float w;
  
  Mover(){
    pos = new PVector(random(width), random(height));
    speed = new PVector(0, 0);
    tx = 0;
    ty = 0;
    txSpeed = random(0.023, 0.03);
    tySpeed = random(0.023, 0.03);
    speedX_minMax = random(3, 6);
    speedY_minMax = random(4, 7);
    w = random(10, 40);
  }
  Mover(float _x, float _y){
    pos = new PVector(_x, _y);
    speed = new PVector(0, 0);
    tx = 0;
    ty = 0;
    txSpeed = random(0.023, 0.03);
    tySpeed = random(0.023, 0.03);
    speedX_minMax = random(3, 6);
    speedY_minMax = random(4, 7);
    w = random(10, 40);
  }
  
  void update(){
    float sx = map(noise(tx), 0, 1, -speedX_minMax, speedX_minMax);
    float sy = map(noise(ty), 0, 1, -speedY_minMax, speedY_minMax);
    speed.set(sx, sy);
    pos.add(speed);
    tx += txSpeed;
    ty += tySpeed;
    
    if(pos.x < -w*0.5){
      pos.x = width;
    }
    if(pos.x > width+w*0.5){
      pos.x = 0;
    }
    if(pos.y < -w*0.5){
      pos.y = height;
    }
    if(pos.y > height+w*0.5){
      pos.y = 0;
    }
  }
  
  void display(){
    fill(255);
    ellipse(pos.x, pos.y, w, w);
  }
}
