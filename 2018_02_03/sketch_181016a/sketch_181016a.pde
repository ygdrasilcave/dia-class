import processing.sound.*;
SoundFile[] file;
int numsounds = 5;

ArrayList<Mover> m;
int count = 0;

void setup() {
  size(1000, 1000);
  m = new ArrayList<Mover>();
  for (int i=0; i<100; i++) {
    m.add(new Mover());
  }
  file = new SoundFile[numsounds];
  for (int i = 0; i < numsounds; i++) {
    file[i] = new SoundFile(this, (i+1) + ".wav");
  }
}

void draw() {
  background(0);
  for (int i=m.size()-1; i>=0; i--) {
    Mover m1 = m.get(i);
    Mover m2;
    float dist = 0;
    boolean isDead = false;
    int index = 0;
    for (int j=m.size()-1; j>=0; j--) {
      if (i != j) {
        m2 = m.get(j);
        dist = (PVector.sub(m1.pos, m2.pos)).mag();
        if (dist < (m1.w*0.5 + m2.w*0.5)) {
          isDead = true;
          if (m1.w > m2.w) {
            index = j;
            m1.w = m1.w + m2.w*0.5;
          } else {
            index = i;
            m2.w = m2.w + m1.w*0.5;
          }
        }
      }
    }
    m1.update();
    m1.display();
    if (isDead == true) {
      m.remove(index);
      //file[count].play(random(0.25, 2.0), 1.0);
      file[count].play(1.0, 1.0);
      count++;
      if (count > numsounds-1) {
        count = 0;
      }
    }
  }
}

void mousePressed() {
  m.add(new Mover(mouseX, mouseY));
}

void keyPressed() {
  if (key == ' ') {
    m.clear();
  }
  if (key == 'r') {
    for (int i=0; i<100; i++) {
      m.add(new Mover());
    }
  }
}
