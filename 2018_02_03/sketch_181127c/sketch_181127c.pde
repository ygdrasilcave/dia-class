import spacebrew.*;

String server="172.26.34.211";
String name="jhyoon range sub2";
String description ="ball sub2";

Spacebrew sb;

int posX = 0;
int posY = 0;
boolean changeColor = false;

void setup() {
  size(600, 600);

  sb = new Spacebrew( this );

  sb.addSubscribe( "ball_x_sub", "range" );
  sb.addSubscribe( "ball_y_sub", "range" );
  sb.addSubscribe( "button_pressed_sub", "boolean" );

  sb.connect(server, name, description );

  background(0);
}

void draw() {
  //background(0);
  fill(0, 10);
  noStroke();
  rect(0, 0, width, height);

  if(changeColor == true){
    fill(random(255), random(255), random(255));
  }else{
    fill(255);
  }
  noStroke();
  ellipse(posX, posY, 20, 20);
}

void onRangeMessage( String name, int value ) {  
  if (name.equals("ball_x_sub")) {
    posX = value;
  } else if (name.equals("ball_y_sub")) {
    posY = value;
  }
  //println(posX + " : " + posY);
}

void onBooleanMessage( String name, boolean value ) {
  if (name.equals("button_pressed_sub")) {
    changeColor = value;
  }
  println("value of boolean : " + changeColor);
}
