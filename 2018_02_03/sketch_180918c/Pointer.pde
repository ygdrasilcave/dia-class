class Pointer {
  PVector position;
  PVector target;
  int gap;

  Pointer(float _x, float _y, int _gap) {
    position = new PVector(_x, _y);
    target = new PVector(mouseX, mouseY);
    gap = _gap;
  }

  void update(PVector _target) {
    target.set(_target);
    target.sub(position);
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(target.heading());
    noFill();
    stroke(255);
    strokeWeight(2);
    ellipse(0, 0, gap/3, gap/3);
    line(0, 0, gap, 0);
    popMatrix();
  }
}
