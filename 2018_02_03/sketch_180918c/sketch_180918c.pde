Pointer[] ptr;
int gap = 50;

PVector insect;
PVector speed;
float tx, ty;
float txSpeed, tySpeed;
float tWing;
PVector[] tail;

void setup() {
  size(1200, 900);
  background(0);

  int num = int(width/gap) * int(height/gap);
  ptr = new Pointer[num];

  int index = 0;
  for (int _x=0; _x<width; _x+=gap) {
    for (int _y=0; _y<height; _y+=gap) {
      ptr[index] = new Pointer(_x+gap/2, _y+gap/2, gap);
      index++;
    }
  }

  insect = new PVector(width/2, height/2);
  speed = new PVector(random(2, 4), random(4, 6));
  tx = 0;
  ty = 0;
  txSpeed = random(0.03, 0.056);
  tySpeed = random(0.03, 0.056);

  tail = new PVector[60];
  for (int i=0; i<tail.length; i++) {
    tail[i] = new PVector(0, 0);
  }
}

void draw() {
  background(0);

  speed.set(map(noise(tx), 0, 1, -8, 8), map(noise(ty), 0, 1, -10, 10));
  insect.add(speed);

  if (insect.x > width) {
    insect.x = 0;
  }
  if (insect.x < 0) {
    insect.x = width;
  }
  if (insect.y > height) {
    insect.y = 0;
  }
  if (insect.y < 0) {
    insect.y = height;
  }

  for (int i=0; i<ptr.length; i++) {
    ptr[i].update(insect);
    ptr[i].display();
  }
  
  noStroke();
  fill(255, 0, 255);
  for (int i=0; i<tail.length; i++) {
    if (i != tail.length-1) {
      tail[i].set(tail[i+1]);
    }else{
      tail[i].set(insect);
    }
    ellipse(tail[i].x, tail[i].y, (i+1)/2, (i+1)/2);
  }

  pushMatrix();  
  noStroke();
  translate(insect.x, insect.y);
  rotate(speed.heading());
  fill(255, 0, 255);
  ellipse(-10, 0, 60, 30);
  fill(255, 0, 0);
  ellipse(20, 10, 10, 10);  
  ellipse(20, -10, 10, 10);
  fill(255, 255, 0);
  pushMatrix();
  translate(-10, -15);
  rotate(radians(map(sin(tWing), -1, 1, 270-50, 270+50)));
  ellipse(0, -20, 20, 40);
  popMatrix();
  pushMatrix();
  translate(-10, 15);
  rotate(radians(map(sin(tWing), -1, 1, 90+50, 90-50)));
  ellipse(0, 20, 20, 40);
  popMatrix();
  popMatrix();


  tx += txSpeed;
  ty += tySpeed;

  tWing += 0.56;
}
