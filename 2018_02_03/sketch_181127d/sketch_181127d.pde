import spacebrew.*;
import processing.video.*;
Capture cam;

String server="172.26.34.211";
String name="jhyoon range pub";
String description ="ball pub";

Spacebrew sb;

int posX = 0;
int posY = 0;

int red = 0;
int green = 0;
int blue = 0;

boolean toggle = false;

PImage img;
int index = 0;
boolean isFinish = false;

void setup() {
  size(640, 480);

  img = loadImage("ddd.jpg");
  //cam = new Capture(this, 640, 480);
  //cam.start();

  sb = new Spacebrew( this );

  sb.addPublish( "posX", "range", posX ); 
  sb.addPublish( "posY", "range", posY );
  sb.addPublish("red", "range", red);
  sb.addPublish("green", "range", green);
  sb.addPublish("blue", "range", blue);
  sb.addPublish( "button_pressed", "boolean", true );

  sb.connect(server, name, description );

  background(0);

  //frameRate(30);
}

void draw() { 
  /*if (cam.available() == true) {
    cam.read();
  }*/
  image(img, 0, 0, width, height);
  //image(cam, 0, 0, width, height);
  red = int(red(img.get(posX, posY)));
  green = int(green(img.get(posX, posY)));
  blue = int(blue(img.get(posX, posY)));

  posX+=2;

  if (posX > width) {
    posX = 0;
    posY+=2;
  }
  if (posY > height) {
    posY = 0;
    //isFinish = true;
  }
  

  if (isFinish == false) {
    sb.send("posX", posX);
    sb.send("posY", posY);
    sb.send("red", red);
    sb.send("green", green);
    sb.send("blue", blue);
  }
}

void keyPressed() {
  if (key == ' ' ) {
    if (toggle == true) {
      toggle = false;
    } else {
      toggle = true;
    }
    sb.send("button_pressed", toggle);
    posX = 0;
    posY = 0;
    isFinish = false;
  }
}

void movieEvent(Movie m) {
  m.read();
}
