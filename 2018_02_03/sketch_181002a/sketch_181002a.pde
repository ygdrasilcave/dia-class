Particle[] particles;

void setup() {
  size(800, 800);
  background(0);
  particles = new Particle[3];
  for (int i=0; i<particles.length; i++) {
    particles[i] = new Particle(random(width), random(height));
  }
}

void draw() {
  background(0);
  for (int i=0; i<particles.length; i++) {
    particles[i].update();
    particles[i].display();
  }
}
