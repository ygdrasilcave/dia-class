class Particle {
  PVector[] pos;
  PVector[] speed;

  Particle(float _x, float _y) {
    pos = new PVector[100];
    speed = new PVector[100];
    for (int i=0; i<pos.length; i++) {
      pos[i] = new PVector(_x, _y);
      speed[i] = new PVector(random(-3, 3), random(-4, 4));
    }
  }

  void update() {
    for (int i=0; i<pos.length; i++) {
      pos[i].add(speed[i]);
    }
  }

  void display() {
    fill(255);
    for (int i=0; i<pos.length; i++) {
      ellipse(pos[i].x, pos[i].y, 10, 10);
    }
  }
}
