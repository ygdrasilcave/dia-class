Pointer[] ptr;
int gap = 50;

void setup() {
  size(1200, 900);
  background(0);

  int num = int(width/gap) * int(height/gap);
  ptr = new Pointer[num];

  int index = 0;
  for (int _x=0; _x<width; _x+=gap) {
    for (int _y=0; _y<height; _y+=gap) {
      ptr[index] = new Pointer(_x+gap/2, _y+gap/2, gap);
      index++;
    }
  }

}

void draw() {
  background(0);
  for (int i=0; i<ptr.length; i++) {
    ptr[i].update();
    ptr[i].display();
  }
}
