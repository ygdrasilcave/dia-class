ArrayList<Particle> particles;
float x, y;
float angleInc;
float t;

void setup() {
  size(1000, 1000);
  background(0);

  particles = new ArrayList<Particle>();

  x = 0;
  y = 0;
  t = 0;
  angleInc = 10;
}

void draw() {
  background(0);

  for (int i=0; i<particles.size(); i++) {
    particles.get(i).update();
    particles.get(i).display();
    if (particles.get(i).checkAge() == false) {
      particles.remove(i);
    }
  }
  //println(particles.size());

  if (frameCount%10 == 0) {
    x = cos(radians(t))*400 + width/2;
    y = sin(radians(t))*400 + height/2;
    t += angleInc;
    particles.add(new Particle(x, y));
  }
  noFill();
  stroke(255);
  ellipse(width/2, height/2, 800, 800);
}

/*void mousePressed() {
  particles.add(new Particle(mouseX, mouseY));
}*/
