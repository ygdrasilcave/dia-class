PImage[] imgs;
int frames = 7;
int frame = 0;

void setup() {
  size(1000, 1000);
  imgs = new PImage[frames];
  for (int i=0; i<frames; i++) {
    imgs[i] = loadImage("ballarina_" + nf(i, 3) + ".png");
  }
}

void draw() {
  background(0);
  float step = int(map(mouseX, 0, width, 1, 10));
  //float step = 5;
  float inc = (TWO_PI/step)/frames;
  for (float t=0; t<TWO_PI; t+=inc*frames) {
    for (int i=0; i<frames; i++) {
      int index = (i+frame)%frames;
      float x = width/2 + cos(t+inc*i)*350;
      float y = height/2 + sin(t+inc*i)*350;
      pushMatrix();
      translate(x, y);
      rotate(HALF_PI+t+inc*i);
      tint(255 - i*20);
      image(imgs[index], -imgs[index].width/2, -imgs[index].height/2, imgs[index].width, imgs[index].height);
      /*if (i == 3) {
        fill(255, 0, 0);
        ellipse(0, 0, 30, 30);
      }*/
      popMatrix();
    }
  }
  frame++;  
  saveFrame("myVide_####.jpg");
}
