void setup() {
  size(600, 600);
  background(0);
  noStroke();

  fill(255);
  for (int x = 0; x < width; x+=10) {
    ellipse(x, random(height), 10, 10);
  }
  
  float t = 0;
  fill(255, 0, 0);
  for (int x = 0; x < width; x+=10) {
    ellipse(x, noise(t)*height, 10, 10);
    t += 0.032;
  }
}
