import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Easing[] easings = { 
  Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, Ani.CUBIC_IN, 
  Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, 
  Ani.SINE_IN, Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, 
  Ani.CIRC_IN_OUT, Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, 
  Ani.BACK_OUT, Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingsVariableNames = {
  "LINEAR", "QUAD_IN", "QUAD_OUT", "QUAD_IN_OUT", 
  "CUBIC_IN", "CUBIC_IN_OUT", "CUBIC_OUT", "QUART_IN", 
  "QUART_OUT", "QUART_IN_OUT", "QUINT_IN", "QUINT_OUT", 
  "QUINT_IN_OUT", "SINE_IN", "SINE_OUT", "SINE_IN_OUT", 
  "CIRC_IN", "CIRC_OUT", "CIRC_IN_OUT", "EXPO_IN", "EXPO_OUT", 
  "EXPO_IN_OUT", "BACK_IN", "BACK_OUT", "BACK_IN_OUT", "BOUNCE_IN", 
  "BOUNCE_OUT", "BOUNCE_IN_OUT", "ELASTIC_IN", "ELASTIC_OUT", 
  "ELASTIC_IN_OUT"
};

float x = 256;
float y = 256;
int diameter = 50;

int index = 0;

void setup() {
  size(512, 512);
  smooth();
  noStroke();
  // you have to call always Ani.init() first!
  Ani.init(this);
  println(easings.length);
  println(easingsVariableNames.length);
}

void draw() {
  background(255);
  fill(0);
  ellipse(x, y, diameter, diameter);
  
  fill(0);
  noStroke();
  textSize(20);
  text(easingsVariableNames[index], 20, 20);
}

void mouseReleased() {
  Ani.to(this, 1.0, "x", mouseX, easings[index]);
  Ani.to(this, 1.0, "y", mouseY, easings[index]);
}

void keyPressed(){
  if(key == ' '){   
    index++;
    if(index > easings.length-1){
      index = 0;
    }    
  }
}
