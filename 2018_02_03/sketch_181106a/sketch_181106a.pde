PImage[] imgs;
int frames = 7;
int frame = 0;

int current = 0;
int interval = 50;

boolean type = true;

void setup() {
  size(1000, 1000);
  imgs = new PImage[frames];
  for (int i=0; i<frames; i++) {
    imgs[i] = loadImage("ballarina_" + nf(i, 3) + ".png");
  }
}

void draw() {
  background(0);
  /*for (int i=0; i<frames; i++) {
   image(imgs[i], width/2-imgs[i].width/2, height/2-imgs[i].height/2, imgs[i].width, imgs[i].height);
   }*/

  float maxAngle = map(mouseY, 0, height, 0, TWO_PI);
  float tSpeed = map(mouseX, 0, width, 0.25, 0.85);

  for (float _t = 0.0; _t<maxAngle; _t+=tSpeed) {
    float x = width/2 + cos(_t)*350;
    float y = height/2 + sin(_t)*350;
    pushMatrix();
    translate(x, y);
    rotate(_t+HALF_PI);
    tint(255);
    image(imgs[frame], -imgs[frame].width/2, -imgs[frame].height/2, imgs[frame].width, imgs[frame].height);
    popMatrix();

    x = width/2 + cos(_t)*150;
    y = height/2 + sin(_t)*150;

    pushMatrix();
    translate(x, y);
    rotate(_t+HALF_PI+PI);
    tint(110);
    image(imgs[frame], -imgs[frame].width/2, -imgs[frame].height/2, imgs[frame].width, imgs[frame].height);
    popMatrix();
  }

  if (type == true) {
    if (current+interval < millis()) {
      current = millis();
      frame++;
      if (frame > frames-1) {
        frame = 0;
      }
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    type = !type;
  }
  if(key == 's'){
    saveFrame("myWork_" + minute() + "_" + second() + ".jpg");
  }
}

void mouseReleased() {
  if (type == false) {
    frame++;
    if (frame > frames-1) {
      frame = 0;
    }
  }
}
