import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Easing[] easings = { 
  Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, Ani.CUBIC_IN, 
  Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, 
  Ani.SINE_IN, Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, 
  Ani.CIRC_IN_OUT, Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, 
  Ani.BACK_OUT, Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingsVariableNames = {
  "LINEAR", "QUAD_IN", "QUAD_OUT", "QUAD_IN_OUT", 
  "CUBIC_IN", "CUBIC_IN_OUT", "CUBIC_OUT", "QUART_IN", 
  "QUART_OUT", "QUART_IN_OUT", "QUINT_IN", "QUINT_OUT", 
  "QUINT_IN_OUT", "SINE_IN", "SINE_OUT", "SINE_IN_OUT", 
  "CIRC_IN", "CIRC_OUT", "CIRC_IN_OUT", "EXPO_IN", "EXPO_OUT", 
  "EXPO_IN_OUT", "BACK_IN", "BACK_OUT", "BACK_IN_OUT", "BOUNCE_IN", 
  "BOUNCE_OUT", "BOUNCE_IN_OUT", "ELASTIC_IN", "ELASTIC_OUT", 
  "ELASTIC_IN_OUT"
};

float x;
float y;
float rotate;

Ani ani_x;
Ani ani_y;
Ani ani_rotate;

int index = 9;

boolean isXEnd = false;
boolean isYEnd = false;
boolean toggle = false;

int indexX = 0;
int indexY = 0;

int shape = 0;
float offsetX = 0;

void setup() {
  size(800, 800);
  
  colorMode(HSB);

  Ani.init(this);

  ani_x = new Ani(this, 1.5, "x", width/2, easings[index], "onEnd:ani_x_end");
  ani_y = new Ani(this, 1.0, "y", width/2+100, easings[index], "onEnd:ani_y_end");
  ani_rotate = new Ani(this, 1.0, "rotate", TWO_PI*2, easings[index]);
  background(0);
}

void draw() {

  if (isXEnd == true && isYEnd == true && toggle == false) {
    saveFrame("myWork_" + minute() + "_" + second() + ".jpg");
    println("animation has been finised");
    ani_x.setDuration(random(1.0, 4.5));
    ani_x.setEnd(random(200, width/2));
    ani_y.setDuration(random(1.5, 5.0));
    ani_y.setEnd(random(200, width/2));
    ani_rotate.setDuration(random(1.0, 4.0));
    ani_rotate.setEnd(random(HALF_PI, TWO_PI*2));
    indexX = int(random(easings.length));
    indexY = int(random(easings.length));
    ani_x.setEasing(easings[indexX]);
    ani_y.setEasing(easings[indexY]);
    background(0);
    ani_x.start();
    ani_y.start();
    ani_rotate.start();
    isXEnd = false;
    isYEnd = false;
    toggle = false;
    shape++;
    if(shape >= 3){
      shape = 0;
    }
    offsetX = random(-200, 200);
  }

  noFill();
  stroke(255);
  pushMatrix();
  translate(width/2, height/2);
  rotate(rotate);
  if (shape == 0) {
    ellipse(offsetX, 0, x, y);
  } else if (shape == 1) {
    rect(-x/2+offsetX, -y/2, x, y);
  }else if(shape == 2){
    createMyShape(offsetX, 20);
  }
  popMatrix();

  fill(0);
  noStroke();
  rect(0, 0, width/2, 100);

  fill(255);
  noStroke();
  textSize(20);
  text("X: " + easingsVariableNames[indexX], 20, 40);
  text("Y: " + easingsVariableNames[indexY], 20, 60);
}

void ani_x_end() {
  isXEnd = true;
  //println("ani_x ended");
}

void ani_y_end() {
  isYEnd = true;
  //println("ani_y ended");
}

void createMyShape(float _offsetX, int _num){  
  stroke(255);
  noFill(); 
  beginShape();
  for (int i=0; i<_num; i++) {
    float _t = (TWO_PI/_num)*i;

    float _x = 0;
    float _y = 0;

    if (i%2 == 0) {
      _x = cos(_t)*x/2+_offsetX;
      _y = sin(_t)*y/2;
    } else {
      _x = cos(_t)*(x/4)+_offsetX;
      _y = sin(_t)*(y/4);
    }
    vertex(_x, _y);
  }
  endShape(CLOSE);
}
