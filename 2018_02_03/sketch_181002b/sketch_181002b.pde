//Particle[] particles;
ArrayList<Particle> particles;

void setup() {
  size(800, 800);
  background(0);
  /*particles = new Particle[3];
  for (int i=0; i<particles.length; i++) {
    particles[i] = new Particle(random(width), random(height));
  }*/
  
  particles = new ArrayList<Particle>();
}

void draw() {
  background(0);
  /*for (int i=0; i<particles.length; i++) {
    particles[i].update();
    particles[i].display();
  }*/
  for(int i=0; i<particles.size(); i++){
    particles.get(i).update();
    particles.get(i).display();
    if(particles.get(i).checkAge() == false){
      particles.remove(i);
    }
  }
  println(particles.size());
}

void mousePressed(){
  particles.add(new Particle(mouseX, mouseY));
}
