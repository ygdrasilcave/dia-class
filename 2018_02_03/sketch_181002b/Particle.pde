class Particle {
  PVector[] pos;
  PVector[] speed;
  float age;
  float duration;
  float colorVal;
  float[] ballSize;

  Particle(float _x, float _y) {
    pos = new PVector[100];
    speed = new PVector[100];
    ballSize = new float[100];
    for (int i=0; i<pos.length; i++) {
      pos[i] = new PVector(_x, _y);
      speed[i] = new PVector(random(-3, 3), random(-4, 4));
      ballSize[i] = random(5, 20);
    }
    age = millis();
    duration = random(500, 3000);
    colorVal = 255;
  }

  void update() {
    for (int i=0; i<pos.length; i++) {
      pos[i].add(speed[i]);
    }
    colorVal = map(age+duration-millis(), 0, duration, 0, 255);
  }

  void display() {
    fill(colorVal);
    noStroke();
    for (int i=0; i<pos.length; i++) {
      ellipse(pos[i].x, pos[i].y, ballSize[i], ballSize[i]);
    }
  }

  boolean checkAge() {
    boolean alive = true;
    if (age + duration < millis()) {
      alive = false;
    }
    return alive;
  }
}
