float x = 0;
float y = 0;
float speedX = 1;
float speedY = 2;
float dirX = 1;
float dirY = 1;
float angle = 0;

PVector pos;
PVector speed;

void setup() {
  size(800, 800);
  background(0);
  pos = new PVector(0, 0);
  speed = new PVector(1, 2);
  rectMode(CENTER);
}

void draw() {
  background(0);
  fill(255);
  pushMatrix();
  translate(x, y);
  rotate(angle);
  rect(0, 0, 80, 80);
  rect(40, 0, 80, 10);
  popMatrix();

  x += speedX*dirX;
  y += speedY*dirY;

  if (x > width) {
    x = width;
    dirX = dirX*-1;
  }
  if (x < 0) {
    x = 0;
    dirX = dirX*-1;
  }
  if (y > height) {
    y = height;
    dirY = dirY*-1;
  }
  if (y < 0) {
    y = 0;
    dirY = dirY*-1;
  }

  fill(255, 0, 0);
  pushMatrix();
  translate(pos.x, pos.y);
  rotate(speed.heading());
  rect(0, 0, 40, 40);
  rect(40, 0, 40, 10);
  popMatrix();

  pos.add(speed);

  if (pos.x > width) {
    pos.x = width;
    speed.x = speed.x * -1;
  }
  if (pos.x < 0) {
    pos.x = 0;
    speed.x = speed.x * -1;
  }
  if (pos.y > height) {
    pos.y = height;
    speed.y = speed.y * -1;
  }
  if (pos.y < 0) {
    pos.y = 0;
    speed.y = speed.y * -1;
  }
}
