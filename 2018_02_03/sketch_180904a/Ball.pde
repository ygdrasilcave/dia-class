class Ball {
  float x;
  float y;
  float speedX;
  float speedY;
  float w;
  float t;
  float speedT;
  float rotVal;
  float speedRot;

  Ball(float _x, float _y) {
    x = _x;
    y = _y;
    speedX = random(2, 5);
    speedY = random(3, 4);
    //speedX = 0;
    //speedY = 0;
    w = random(20, 40);
    ;
    speedT = random(0.065, 0.25);
    speedRot = random(0.015, 0.05);
  }

  void update() {
    x = x + speedX;
    y = y + speedY;

    if (x > width-30) {
      x = width-30;
      speedX = speedX*-1;
    }
    if (x < 30) {
      x = 30;
      speedX = speedX*-1;
    }
    if (y > height-30) {
      y = height-30;
      speedY = speedY*-1;
    }
    if (y < 30) {
      y = 30;
      speedY = speedY*-1;
    }

    w = w + sin(t)*5;
    t = t + speedT;

    rotVal += speedRot;
  }

  void display(int _t) {
    if (_t == 0) {
      noFill();
      stroke(255);
      pushMatrix();
      translate(x, y);
      ellipse(0, 0, w, w);
      popMatrix();

      fill(255, 255, 0);
      noStroke();
      pushMatrix();
      translate(x, y);
      rotate(rotVal);
      ellipse(w+10, 0, 10, 10);
      popMatrix();
    }else if(_t == 1){
      noFill();
      stroke(255);
      pushMatrix();
      translate(x, y);
      ellipse(0, 0, w, w);
      popMatrix();
    }else if(_t == 2){
      fill(255, 255, 0);
      noStroke();
      pushMatrix();
      translate(x, y);
      rotate(rotVal);
      ellipse(w+10, 0, 10, 10);
      popMatrix();
    }
  }
}
