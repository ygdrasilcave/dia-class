Ball ball[];
int numBall = 30;
int t = 0;

void setup() {
  size(1200, 800);
  background(0);
  ball = new Ball[numBall];
  for (int i=0; i<numBall; i++) {
    ball[i] = new Ball(random(width), random(height));
  }
}

void draw() {
  //background(0);
  fill(0, 10);
  noStroke();
  rect(0, 0, width, height);
  for (int i=0; i<numBall; i++) {
    ball[i].update();
    ball[i].display(t);
  }
}

void keyPressed(){
  if(key == ' '){
    t++;
    if(t > 2){
      t = 0;
    }
  }
}
