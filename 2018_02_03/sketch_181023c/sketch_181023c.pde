void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  stroke(255);
  noFill();

  createMyShape(20);
}

void createMyShape(int _num){  
  stroke(255);
  noFill();
  
  beginShape();
  for (int i=0; i<_num; i++) {
    float t = (TWO_PI/_num)*i;

    float x = 0;
    float y = 0;

    if (i%2 == 0) {
      x = cos(t)*width/2 + width/2;
      y = sin(t)*width/2 + height/2;
    } else {
      x = cos(t)*width/3 + width/2;
      y = sin(t)*width/3 + height/2;
    }
    vertex(x, y);
  }
  endShape(CLOSE);
}
