import spacebrew.*;

String server="172.26.34.211";
String name="jhyoon range sub2";
String description ="ball sub2";

Spacebrew sb;

int posX = 0;
int posY = 0;
int red = 0;
int green = 0;
int blue = 0;
boolean changeColor = false;

void setup() {
  size(640, 480);

  sb = new Spacebrew( this );

  sb.addSubscribe( "posX", "range" );
  sb.addSubscribe( "posY", "range" );
  sb.addSubscribe( "red", "range" );
  sb.addSubscribe( "green", "range" );
  sb.addSubscribe( "blue", "range" );
  sb.addSubscribe( "button_pressed_sub", "boolean" );

  sb.connect(server, name, description );

  background(0);
}

void draw() {
  if(changeColor == true){
    background(0);
  }
  noStroke();
  fill(red, green, blue);
  rect(posX, posY, 2, 2);
}

void onRangeMessage( String name, int value ) {  
  if (name.equals("posX")) {
    posX = value;
  } else if (name.equals("posY")) {
    posY = value;
  } else if (name.equals("red")) {
    red = value;
  } else if (name.equals("green")) {
    green = value;
  } else if (name.equals("blue")) {
    blue = value;
  }
  //println(posX + " : " + posY);
}

void onBooleanMessage( String name, boolean value ) {
  if (name.equals("button_pressed_sub")) {
    changeColor = value;
  }
  println("value of boolean : " + changeColor);
}
