XML xml;

IntList tMin;
IntList tMax;
StringList condition;
StringList date;
String cityName;

PFont font;

int min = 100;
int max = 0;

int cityCounter = 0;
boolean toggle = true;

void setup() {
  loadData(cityCounter);

  size(800, 600);
  font = createFont("SeoulHangangEB.ttf", 42);
  textFont(font);
  textAlign(CENTER, CENTER);
}



void draw() {
  background(0);
  textSize(40);
  text(cityName, width/2, 40);

  float space = width/(tMin.size()+1);
  fill(0, 255, 255); 
  for (int i=0; i<tMin.size(); i++) {
    ellipse(space + i*space, map(tMin.get(i), min, max, 500, 200), 20, 20);
  }
  fill(255, 255, 0); 
  for (int i=0; i<tMax.size(); i++) {
    ellipse(space + i*space, map(tMax.get(i), min, max, 500, 200), 20, 20);
  }
  fill(255); 
  textSize(16);
  for (int i=0; i<condition.size(); i++) {
    pushMatrix();
    translate(space + i*space, 130);
    rotate(radians(-30));
    text(condition.get(i), 0, 0);
    popMatrix();
  }

  fill(255); 
  textSize(14);
  for (int i=0; i<condition.size(); i++) {
    String[] d = date.get(i).split(" ");
    pushMatrix();
    translate(space + i*space, 550);
    rotate(radians(-30));
    text(d[0], 0, 0);
    popMatrix();
  }
  
  if(minute()%5 == 0 && toggle == true){
    loadData(cityCounter);
    toggle = false;
  }else if(minute()%5==2 && toggle == false){
    toggle = true;
  }
}

void loadData(int _index) {
  xml = loadXML("http://www.weather.go.kr/weather/forecast/mid-term-rss3.jsp?stnId=105");

  XML data = xml.getChild("channel").getChild("item").getChild("description").getChild("body").getChildren("location")[_index];
  XML[] wonju = data.getChildren("data");
  //printArray(data);

  cityName = data.getChild("city").getContent();
  println(cityName);

  tMin = new IntList();
  tMax = new IntList();
  condition = new StringList();
  date = new StringList();
  for (int i=0; i<wonju.length; i++) {
    tMin.set(i, int(wonju[i].getChild("tmn").getContent()));
    tMax.set(i, int(wonju[i].getChild("tmx").getContent()));
    condition.set(i, wonju[i].getChild("wf").getContent());
    date.set(i, wonju[i].getChild("tmEf").getContent());
  }
  printArray(tMin);
  printArray(tMax);
  //printArray(condition);
  printArray(date);

  for (int i=0; i<tMin.size(); i++) {
    if (min > tMin.get(i)) {
      min = tMin.get(i);
    }
    if (max < tMax.get(i)) {
      max = tMax.get(i);
    }
  }
  println(min);
  println(max);
}

void keyPressed(){
  if(key == 'a'){
    cityCounter++;
    if(cityCounter > 18){
      cityCounter = 0;
    }
    loadData(cityCounter);
  }
}
