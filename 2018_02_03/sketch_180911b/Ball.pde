class Ball {
  PVector pos;
  PVector speed;
  float w;
  float t;
  float speedWT;
  float noiseTX, noiseTY;
  float noiseTXSpeed, noiseTYSpeed;
  float colorHue;
  float speedFactorX, speedFactorY;

  Ball() {
    w = random(20, 40);
    pos = new PVector(random(w, width-w), random(w, height-w));
    speed = new PVector(0, 0);
    speedWT = random(0.065, 0.25);
    noiseTX = 0;
    noiseTY = 0;
    noiseTXSpeed = random(0.005, 0.03);
    noiseTYSpeed = random(0.005, 0.03);
    colorHue = random(100);   
    speedFactorX = random(1, 6);
    speedFactorY = random(1, 8);
  }

  void update() {
    speed.set(map(noise(noiseTX), 0, 1, -speedFactorX, speedFactorX), map(noise(noiseTY), 0, 1, -speedFactorY, speedFactorY));
    pos.add(speed);

    if (pos.x > width) {
      pos.x = 0;
    }
    if (pos.x < 0) {
      pos.x = width;
    }
    if (pos.y > height) {
      pos.y = 0;
    }
    if (pos.y < 0) {
      pos.y = height;
    }

    w = w + sin(t)*2;
    t = t + speedWT;
    
    noiseTX += noiseTXSpeed;
    noiseTY += noiseTYSpeed;
  }

  void display() {
    noFill();
    stroke(100, 0, 100);
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(speed.heading());
    ellipse(0, 0, w, w);
    line(0, 0, w*1.5, -w/3);
    line(0, 0, w*1.5, w/3);
    fill(colorHue, 100, 100);
    noStroke();
    ellipse(w*1.5, -w/3, w/4, w/4);
    ellipse(w*1.5, w/3, w/4, w/4);
    popMatrix();
  }
}
