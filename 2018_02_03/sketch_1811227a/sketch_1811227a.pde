import spacebrew.*;

String server="172.26.34.211";
String name="jhyoon range pub";
String description ="ball pub";

Spacebrew sb;

int posX = 512;
int posY = 512;

int speedX = 6;
int speedY = 8;

boolean toggle = false;

void setup() {
  size(600, 600);
  
  sb = new Spacebrew( this );

  sb.addPublish( "ball_x", "range", posX ); 
  sb.addPublish( "ball_y", "range", posY );
  sb.addPublish( "button_pressed", "boolean", true );

  sb.connect(server, name, description );
  
  background(0);
}

void draw() { 
  //background(0);
  fill(0, 10);
  noStroke();
  rect(0,0,width,height);
  
  posX = posX + speedX;
  posY = posY + speedY;
  
  if(posX > width){
    posX = 0;
  }
  if(posX < 0){
    posX = width;
  }
  if(posY > height){
    posY = 0;
  }
  if(posY < 0){
    posY = height;
  }
  
  sb.send("ball_x", posX);
  sb.send("ball_y", posY);
  
  fill(255, 255, 0);
  noStroke();
  ellipse(posX, posY, 20, 20);
}

void keyPressed() {
  if (key == ' ' ) {
    if(toggle == true){
      toggle = false;
    }else{
      toggle = true;
    }
    sb.send("button_pressed", toggle);
  }
}
