const int sensorPin = A0;
const int ledPin = 9;

int sensorVal = 0;
int sensorMin = 1023;
int sensorMax = 0;
int outputVal = 0;

void blinkLED(int _pin, int _n, int _d){
  for(int i=0; i<_n; i++){
    digitalWrite(_pin, HIGH);
    delay(_d);
    digitalWrite(_pin, LOW);
    delay(_d);
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  blinkLED(13, 10, 100);

  int startTime = millis();
  while(millis() < startTime + 5000){
    sensorVal = analogRead(sensorPin);
    if(sensorVal < sensorMin){
      sensorMin = sensorVal;      
    }
    if(sensorVal > sensorMax){
      sensorMax = sensorVal; 
    }    
  }
  Serial.print("min :");
  Serial.print(sensorMin);
  Serial.print("     ");
  Serial.print("max :");
  Serial.println(sensorMax);

  blinkLED(13, 3, 500);
}

void loop() {
  sensorVal = analogRead(sensorPin);
  outputVal = map(sensorVal, sensorMin, sensorMax, 0, 255);
  outputVal = constrain(outputVal, 0, 255);
  analogWrite(ledPin, outputVal);
  /*Serial.print("sensorval :");
  Serial.println(sensorVal);
  Serial.print("outputVal :");
  Serial.println(outputVal);
  delay(100);*/
  Serial.println(outputVal);
  delay(10);
}
