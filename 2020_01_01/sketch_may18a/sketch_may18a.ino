int sensorVal = 0;
int outputVal = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  sensorVal = analogRead(0);
  outputVal = map(sensorVal, 80, 950, 0, 255);
  outputVal = constrain(outputVal, 0, 255);
  analogWrite(9, outputVal);
  Serial.print("sensorval :");
  Serial.println(sensorVal);
  Serial.print("outputVal :");
  Serial.println(outputVal);
  delay(100);
}
