const int sensorPin = A0;
const int ledPin = 9;

int sensorVal = 0;
int sensorMin = 1023;
int sensorMax = 0;
int outputVal = 0;

void blinkLED(int _pin, int _n, int _d){
  for(int i=0; i<_n; i++){
    digitalWrite(_pin, HIGH);
    delay(_d);
    digitalWrite(_pin, LOW);
    delay(_d);
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  blinkLED(13, 10, 100);

  int startTime = millis();
  while(millis() < startTime + 5000){
    sensorVal = analogRead(sensorPin);
    if(sensorVal < sensorMin){
      sensorMin = sensorVal;      
    }
    if(sensorVal > sensorMax){
      sensorMax = sensorVal; 
    }    
  }
  Serial.print("min :");
  Serial.print(sensorMin);
  Serial.print("     ");
  Serial.print("max :");
  Serial.println(sensorMax);

  blinkLED(13, 3, 500);
}

void loop() {
  //sensorVal = analogRead(sensorPin);
  sensorVal = average_01();
  outputVal = map(sensorVal, sensorMin, sensorMax, 0, 255);
  outputVal = constrain(outputVal, 0, 255);
  analogWrite(ledPin, outputVal);
  /*Serial.print("sensorval :");
  Serial.println(sensorVal);
  Serial.print("outputVal :");
  Serial.println(outputVal);
  delay(100);*/
  Serial.println(outputVal);
  delay(10);
}

unsigned long average_01(){
  static long sample_0, sample_1, sample_2, sample_3, sample_4, sample_5;
  sample_5 = sample_4;
  sample_4 = sample_3;
  sample_3 = sample_2;
  sample_2 = sample_1;
  sample_1 = analogRead(sensorPin);
  sample_0 = (sample_1 + sample_2 + sample_3 + sample_4 + sample_5)/5;
  return sample_0;
}

unsigned average_02(){
  int sum = 0;
  for(int i=0; i<10; i++){
    sum += analogRead(sensorPin);   
  }
  return (sum/10);
}

#define alpha 0.8
unsigned long average_03(){
  static long lastSample = 0;
  long newSample = analogRead(sensorPin);
  long avr = alpha*lastSample + (1-alpha)*newSample;
  lastSample = avr;
  return avr;
}
