int ledNum = 6;
int ledPins[] = {2, 3, 4, 7, 8, 9};
int analogInPin = 0;
boolean LED_ON = HIGH;
boolean LED_OFF = LOW;
int sensorValue = 0;
int ledLevel = 0;

void setup() {
  //ledNum = sizeof(ledPins)/sizeof(int);
  for (int led = 0; led < ledNum; led++)
  {
    pinMode(ledPins[led], OUTPUT);
  }
}

void loop() {
  sensorValue = analogRead(analogInPin);
  ledLevel = map(sensorValue, 0, 1023, 0, ledNum);
  for (int led = 0; led < ledNum; led++)
  {
    if (led < ledLevel ) {
      digitalWrite(ledPins[led], LED_ON);
    }
    else {
      digitalWrite(ledPins[led], LED_OFF);
    }
  }
}
