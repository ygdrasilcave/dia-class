int led_pin_0 = 9;
int led_pin_1 = 8;
void setup() {
  pinMode(led_pin_0, OUTPUT);
  pinMode(led_pin_1, OUTPUT);
}
void loop() {
  digitalWrite(led_pin_1, HIGH);
  for (int i = 0; i < 256; i++) {
    analogWrite(led_pin_0, i);
    delay(10);
  }
  delay(100);
  digitalWrite(led_pin_1, LOW);
  for (int i = 255; i >= 0; i--) {
    analogWrite(led_pin_0, i);
    delay(10);
  }
  delay(100);
}
