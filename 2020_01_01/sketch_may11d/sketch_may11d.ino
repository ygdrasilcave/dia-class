const int ledNum = 6;
const int ledPins[] = {2, 3, 4, 7, 8, 9};

void setup(){
  setUpPins();
  randomSeed(analogRead(0));
}

void loop(){
  int numOfLED = int(random(1, 7));
  int period1 = int(random(50, 501));
  int period2 = int(random(50, 501));
  turnOnLEDs(numOfLED);
  delay(period1);
  turnOffAllLEDs();
  delay(period2);
}

void setUpPins(){
  for (int i=0; i<ledNum; i++){
    pinMode(ledPins[i], OUTPUT);
  }
}
void turnOnLEDs (int number){
  if (number < 1) {
    number = 1; 
  }
  if (number > ledNum) {
    number = ledNum;
  }
  for (int i = 0; i<number; i++){
    digitalWrite(ledPins[i], HIGH);
  }
}
void turnOffAllLEDs(){
  for(int i=0; i<ledNum; i++){
    digitalWrite(ledPins[i], LOW);
  }
}

void turnOnAllLEDs(){
  for(int i=0; i<ledNum; i++){
    digitalWrite(ledPins[i], HIGH);
  }
}
