const int led = 9;
unsigned long currTime = 0;
unsigned long interval = 200;
boolean ledValue = true;

void setup(){
  pinMode(led, OUTPUT);
  Serial.begin(9600);
  currTime = millis();
}

void loop(){
  interval = map(analogRead(A0), 0, 1023, 100, 1000);
  if(currTime + interval < millis()){
    ledValue = !ledValue;
    currTime = millis();
  }
  digitalWrite(led, ledValue);
  Serial.println(interval);
}
