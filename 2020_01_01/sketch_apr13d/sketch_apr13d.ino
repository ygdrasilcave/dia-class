int switchPinUp = 9;
int switchPinDown = 10;
int prevStateUp = 0;
int currStateUp = 0;
int prevStateDown = 0;
int currStateDown = 0;
int counter = 0;

void setup() {
  Serial.begin(9600);
  pinMode(switchPinUp, INPUT);
  pinMode(switchPinDown, INPUT);
  for (int i = 0; i < 7; i++) {
    pinMode(i + 2, OUTPUT);
  }

  counter = 3;
  changeLEDLight();
}

void loop() {
  currStateUp = digitalRead(switchPinUp);
  currStateDown = digitalRead(switchPinDown);
  if (detectRisingEdgeUp() == 1) {
    counter++;
    if (counter > 6) {
      //conter = 6;
      counter = 0;
    }
  }
  if (detectRisingEdgeDown() == 1) {
    counter--;
    if (counter < 0) {
      //counter = 0;
      counter = 6;
    }
  }

  changeLEDLight();

  Serial.println(counter);
}

void changeLEDLight() {
  for (int i = 0; i < 7; i++) {
    if (i == counter) {
      digitalWrite(i + 2, HIGH);
    } else {
      digitalWrite(i + 2, LOW);
    }
  }
}

int detectRisingEdgeUp() {
  int result = 0;
  if ((prevStateUp == 0) && (currStateUp == 1)) {
    result = 1;
  } else {
    result = 0;
  }
  prevStateUp = currStateUp;
  return result;
}
int detectRisingEdgeDown() {
  int result = 0;
  if ((prevStateDown == 0) && (currStateDown == 1)) {
    result = 1;
  } else {
    result = 0;
  }
  prevStateDown = currStateDown;
  return result;
}
