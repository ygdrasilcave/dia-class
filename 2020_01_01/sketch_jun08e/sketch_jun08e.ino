#include <Servo.h>
Servo myServo;

const int dataNum = 4;
int index = 0;
int rawData[dataNum];

const int rPin = 3;
const int gPin = 5;
const int bPin = 6;

const int servoPin = 9;

const int minVal = 0;
const int maxVal = 255;

void setup() {
  Serial.begin(115200);
  for (int i = 0; i < dataNum; i++) {
    rawData[i] = 0;
  }
  myServo.attach(servoPin);
}

void loop() {
  if (Serial.available() > 0) {
    if (index == 0) {
      for (index; index < dataNum; index++) {
        rawData[index] = Serial.parseInt();
        if (index < dataNum - 1) {
          rawData[index] = constrain(rawData[index], minVal, maxVal);
        } else if (index == dataNum - 1) {
          rawData[index] = constrain(rawData[index], 0, 180);
        }
      }

      analogWrite(rPin, rawData[0]);
      analogWrite(gPin, rawData[1]);
      analogWrite(bPin, rawData[2]);
      myServo.write(rawData[3]);

      /*Serial.print(rawData[0]);
      Serial.print(",");
      Serial.print(rawData[1]);
      Serial.print(",");
      Serial.print(rawData[2]);
      Serial.print(",");
      Serial.print(rawData[3]);
      Serial.println();*/

      if (Serial.read() == '\n' && index == dataNum) {
        index = 0;
        //delay(10);
        //Serial.flush();
      }
    }

  }
}

/*
import processing.serial.*;
Serial myPort;

float x = 0;
float speedX = 3;
float dirX = 1;

void setup () {
  size(640, 480);        
  println(Serial.list());
  //myPort = new Serial(this, Serial.list()[1], 9600);
  myPort = new Serial(this, "COM6", 115200);
  background(255);
}

void draw () {
  background(255);
  
  noStroke();
  fill(0);
  ellipse(x, height/2, 20, 20);
  
  x += speedX*dirX;
  if(x > width-10){
    x = width-10;
    dirX *= -1;
  }
  if(x < 10){
    x = 10;
    dirX *= -1;
  }

  int redValue = int(map(x, 10, width-10, 0, 255));
  int greenValue = int(map(mouseX, 0, width, 0, 255));
  int blueValue = int(map(mouseY, 0, height, 0, 255));
  int servoValue = int(map(x, 10, width-10, 0, 180));
  
  String data = redValue + " " + greenValue + " " + blueValue + " " + servoValue + "\n";
  myPort.write(data);
  println(data);
}

 */
