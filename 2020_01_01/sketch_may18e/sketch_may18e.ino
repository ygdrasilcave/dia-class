#include <Servo.h>
Servo servoMotor;
int angle = 0;
int ledPin = 13;

void setup()
{
  pinMode(ledPin, OUTPUT);
  servoMotor.attach(6);
}

void loop()
{
  digitalWrite(ledPin, HIGH);
  servoMotor.write(0);
  delay(1000);
  digitalWrite(ledPin, LOW);
  for(angle = 0; angle < 180; angle += 1)
  {
    servoMotor.write(angle);
    delay(10);
  }
  delay(500);
}
