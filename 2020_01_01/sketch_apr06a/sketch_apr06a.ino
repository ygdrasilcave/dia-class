int led_pin_0 = 9;
int led_pin_1 = 8;
int button = 2;

void setup() {
  // put your setup code here, to run once:
  pinMode(button, INPUT);
  pinMode(led_pin_0, OUTPUT);
  pinMode(led_pin_1, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(button) == HIGH) {
    digitalWrite(led_pin_0, HIGH);
    digitalWrite(led_pin_1, LOW);
  } else {
    digitalWrite(led_pin_0, LOW);
    digitalWrite(led_pin_1, HIGH);
  }
}
