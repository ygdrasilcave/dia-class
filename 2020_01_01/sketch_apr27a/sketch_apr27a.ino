const int led = 9;
int interval = 0;

void setup(){
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  interval = map(analogRead(A0), 0, 1023, 100, 1000);
  digitalWrite(led, HIGH);
  delay(interval);
  digitalWrite(led, LOW);
  delay(interval);
  Serial.println(interval);
}
