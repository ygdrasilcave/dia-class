const int ledNum = 6;
const int ledPins[] = {2, 3, 4, 7, 8, 9};
int wait = 100;

void setup(){
  for (int led = 0; led < ledNum; led++)
  {
    pinMode(ledPins[led], OUTPUT);
  }
}

void loop() {
  wait = analogRead(0);
  wait = map(wait, 0, 1023, 10, 500);
  
  for (int led = 0; led < ledNum-1; led++)
  {
    digitalWrite(ledPins[led], HIGH);
    delay(wait);
    digitalWrite(ledPins[led + 1], HIGH);
    delay(wait);
    digitalWrite(ledPins[led], LOW);
    delay(wait*2);
  }
  for (int led = ledNum-1; led > 0; led--) {
    digitalWrite(ledPins[led], HIGH);
    delay(wait);
    digitalWrite(ledPins[led - 1], HIGH);
    delay(wait);
    digitalWrite(ledPins[led], LOW);
    delay(wait*2);
  }
}
