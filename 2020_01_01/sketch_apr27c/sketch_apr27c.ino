const int led = 9;
unsigned long currTime = 0;
unsigned long interval = 1000;
boolean ledValue = true;

void setup(){
  pinMode(led, OUTPUT);
  Serial.begin(9600);
  currTime = millis();
}

void loop(){
  if(currTime + interval < millis()){
    ledValue = !ledValue;
    currTime = millis();
  }
  digitalWrite(led, ledValue);
  Serial.println(interval);
}
