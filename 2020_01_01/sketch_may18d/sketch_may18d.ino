#include <Servo.h>
Servo servoMotor;
int angle = 0;
int ledPin = 13;

void setup()
{
  pinMode(ledPin, OUTPUT);
  servoMotor.attach(6);
}
void loop()
{
  digitalWrite(ledPin, HIGH);
  servoMotor.write(0);
  delay(1000);
  digitalWrite(ledPin, LOW);
  servoMotor.write(90);
  delay(1000);
  servoMotor.write(180);
  delay(1000);
  servoMotor.write(90);
  delay(1000);
}
