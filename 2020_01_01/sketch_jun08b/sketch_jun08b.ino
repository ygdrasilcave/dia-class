int value0_00, value0_01, value0_02;
int value1_00, value1_01, value1_02; 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  value0_00 = 10;
  value0_01 = 100;
  value0_02 = 1000;
  value1_00 = 1;
  value1_01 = 0;
  value1_02 = 1;

  Serial.print('A');
  Serial.print(",");
  Serial.print(value0_00);
  Serial.print(",");
  Serial.print(value0_01);
  Serial.print(",");
  Serial.print(value0_02);
  Serial.println();
  delay(1000);
  Serial.print('D');
  Serial.print(",");
  Serial.print(value1_00);
  Serial.print(",");
  Serial.print(value1_01);
  Serial.print(",");
  Serial.print(value1_02);
  Serial.println();
  delay(1000);
}

/*
import processing.serial.*;
Serial myPort;

void setup () {
  size(640, 480);        
  println(Serial.list());
  //myPort = new Serial(this, Serial.list()[1], 9600);
  myPort = new Serial(this, "COM6", 9600);
  myPort.bufferUntil('\n');
  background(0);
}

void draw () {
}

void serialEvent (Serial myPort) {
  String rawData = myPort.readStringUntil('\n');

  if (rawData != null) {
    rawData = trim(rawData);
    String[] data = rawData.split(",");
    
    if(data[0].charAt(0) == 'A' && data.length > 3){
      for(int i=1; i<data.length; i++){
        println("analog value " + i + " = " + data[i]);
      }
    }else if(data[0].charAt(0) == 'D' && data.length > 3){
      for(int i=1; i<data.length; i++){
        println("digital value " + i + " = " + data[i]);
      }
    }
  }
}
 */
