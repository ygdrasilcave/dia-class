int switchPin = 8;
int state = 0;
int counter = 0;

void setup(){
  Serial.begin(9600);
  pinMode(switchPin, INPUT);
}

void loop(){
  state = digitalRead(switchPin);
  if(state == HIGH){
    counter++;
  }
  Serial.println(counter);
}
