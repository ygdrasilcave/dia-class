const int potPin = A0;
const int ledPin = 9;
int potVal = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  potVal = analogRead(potPin);
  analogWrite(ledPin, potVal/4);
  Serial.println(potVal);
  //Serial.write(potVal/4);
  delay(10);
}


/*
 * 
 * 
 import processing.serial.*;
Serial myPort;

float data = 0;

void setup() {
  size(640, 480);
  background(201, 121, 255);
  
  println(Serial.list());
  myPort = new Serial(this, Serial.list()[1], 9600);
  //myPort = new Serial(this, "COM5", 9600);
  
  myPort.bufferUntil('\n');
}


void draw() {
  float r = map(data, 0, 1023, 0, 255);
  background(r, 121, 255);

  fill(255);
  stroke(0);
  strokeWeight(5);
  
  float w = map(data, 0, 1023, 0, height);
  ellipse(width/2, height/2, w, w);
  
  //println(data);
}

void serialEvent(Serial myPort){
  String rawData = myPort.readStringUntil('\n');
  
  if(rawData != null){
    data = float(trim(rawData));
  }
  
  //int rawData = myPort.read();
  //println(rawData);
  //if(rawData > 0){
  //  data = rawData;
  //}
}

*/
