int switchPinUp = 9;
int switchPinDown = 10;
int prevStateUp = 0;
int currStateUp = 0;
int prevStateDown = 0;
int currStateDown = 0;
int counter = 0;

void setup() {
  Serial.begin(9600);
  pinMode(switchPinUp, INPUT);
  pinMode(switchPinDown, INPUT);
}

void loop() {
  currStateUp = digitalRead(switchPinUp);
  currStateDown = digitalRead(switchPinDown);
  if(detectRisingEdgeUp() == 1){
    counter++;
  }
  if(detectRisingEdgeDown() == 1){
    counter--;
  }
  Serial.println(counter);
}

int detectRisingEdgeUp(){
  int result = 0;
  if((prevStateUp == 0) && (currStateUp == 1)){
    result = 1;
  }else{
    result = 0;
  }
  prevStateUp = currStateUp;
  return result;
}
int detectRisingEdgeDown(){
  int result = 0;
  if((prevStateDown == 0) && (currStateDown == 1)){
    result = 1;
  }else{
    result = 0;
  }
  prevStateDown = currStateDown;
  return result;
}
