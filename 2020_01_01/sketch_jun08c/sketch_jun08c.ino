const int pot0 = A0;
const int pot1 = A1;
const int sw0 = 7;
const int sw1 = 8;

int value0_00, value0_01;
int value1_00, value1_01; 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(sw0, INPUT_PULLUP);
  pinMode(sw1, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  value0_00 = average0();
  value0_01 = average1();

  value1_00 = digitalRead(sw0);
  value1_01 = digitalRead(sw1);

  Serial.print('A');
  Serial.print(",");
  Serial.print(value0_00);
  Serial.print(",");
  Serial.print(value0_01);
  Serial.println();
  delay(10);
  Serial.print('D');
  Serial.print(",");
  Serial.print(value1_00);
  Serial.print(",");
  Serial.print(value1_01);
  Serial.println();
  delay(10);
}

#define alpha 0.8
unsigned long average0(){
  static long lastSample = 0;
  long newSample = analogRead(pot0);
  long avr = alpha * lastSample + (1-alpha) * newSample;
  lastSample = avr;
  return avr;
}
unsigned long average1(){
  static long lastSample = 0;
  long newSample = analogRead(pot1);
  long avr = alpha * lastSample + (1-alpha) * newSample;
  lastSample = avr;
  return avr;
}

/*
import processing.serial.*;
Serial myPort;

float w, h;
int s, f;

void setup () {
  size(640, 480);        
  println(Serial.list());
  //myPort = new Serial(this, Serial.list()[1], 9600);
  myPort = new Serial(this, "COM6", 9600);
  myPort.bufferUntil('\n');
  background(255);
}

void draw () {
  background(255);

  if (f == 1) {
    noStroke();
    fill(0);
  } else if (f == 0) {
    stroke(0);
    noFill();
  }
  if (s == 1) {
    rect(width/2-w/2, height/2-h/2, w, h);
  } else if (s == 0) {
    ellipse(width/2, height/2, w, h);
  }
}

void serialEvent (Serial myPort) {
  String rawData = myPort.readStringUntil('\n');

  if (rawData != null) {
    rawData = trim(rawData);
    String[] data = rawData.split(",");

    if (data[0].charAt(0) == 'A' && data.length > 2) {
      for (int i=1; i<data.length; i++) {
        println("analog value " + i + " = " + data[i]);
      }
      w = float(data[1]);
      h = float(data[2]);
      w = map(w, 0, 1023, 0, width);
      h = map(h, 0, 1023, 0, height);
    } else if (data[0].charAt(0) == 'D' && data.length > 2) {
      for (int i=1; i<data.length; i++) {
        println("digital value " + i + " = " + data[i]);
      }
      s = int(data[1]);
      f = int(data[2]);
    }
  }
}
 */
