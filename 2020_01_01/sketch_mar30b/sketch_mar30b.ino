int 1led_pin = 7;

void setup() {
  // put your setup code here, to run once:
  pinMode(2, INPUT);
  pinMode(led_pin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(led_pin, digitalRead(2));
}
