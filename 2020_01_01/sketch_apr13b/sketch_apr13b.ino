int switchPin = 8;
int prevState = 0;
int currState = 0;
int counter = 0;

void setup() {
  Serial.begin(9600);
  pinMode(switchPin, INPUT);
}

void loop() {
  currState = digitalRead(switchPin);
  if(detectRisingEdge() == 1){
    counter++;
  }
  Serial.println(counter);
}

int detectRisingEdge(){
  int result = 0;
  if((prevState == 0) && (currState == 1)){
    result = 1;
  }else{
    result = 0;
  }
  prevState = currState;
  return result;
}
