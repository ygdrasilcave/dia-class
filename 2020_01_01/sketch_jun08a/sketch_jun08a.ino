const int potPin = A0;
const int ledPin = 9;
int potVal = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  potVal = analogRead(potPin);
  analogWrite(ledPin, potVal/4);
  Serial.println(potVal);
  //Serial.write(potVal/4);
  delay(10);
}


/*
import processing.serial.*;
Serial myPort;

int bar_x = 0;
float data = 0;

void setup () {
 size(640, 480);        
 println(Serial.list());
 //myPort = new Serial(this, Serial.list()[1], 9600);
 myPort = new Serial(this, "COM6", 9600);
 myPort.bufferUntil('\n');
 background(0);
}

void draw () {
 if (data > (height/3)*2) {
   stroke(255, 0, 0);
 }
 else if (data > height/3 && data <= (height/3)*2) {
   stroke(255, 255, 0);
 }
 else {
   stroke(0, 255, 0);
 }
 line(bar_x, height, bar_x, height - data);
 if (bar_x >= width) {
   bar_x = 0;
   background(0);
 }
 else {
   bar_x++;
 }
}

void serialEvent (Serial myPort) {
 String rawData = myPort.readStringUntil('\n');

 if (rawData != null) {
   data = float(trim(rawData));
   data = map(data, 0, 1023, 0, height);
 }
}
*/
