var myFont;
var strings;
var t;
var counter;
var prevSecond;

function preload() {
  myFont = loadFont('data/NanumBarunGothic.otf');
  strings = loadStrings('data/text.txt');
}

function setup() {
  // put setup code here
  createCanvas(800, 600);
  textFont(myFont, 34);
  t = 0.0;
  counter = 0;
  prevSecond = 0;
}

function draw() {
  // put drawing code here
  //background(0);
  fill(0, 35);
  rect(0, 0, width, height);

  textAlign(CENTER, CENTER);

  /*fill(255);
  for(var i=0; i<strings.length; i=i+2){
    text(strings[i], width/2, 30+i*40);
  }

  fill(255,0,0);
  for(var i=1; i<strings.length; i=i+2){
    text(strings[i], width/2, 30+i*40);
  }*/

  for(var i=0; i<strings.length; i=i+1){
    if(i%2 == 0){
      fill(255);
    }else{
      fill(0,255,0);
    }

    if(i == counter){
      textSize(32);
      text(strings[i], width/2+sin(t)*200, 30+i*48);
    }
    /*else if(i == 8){
      textSize(24 + cos(t)*12);
      text(strings[i], width/2+cos(t)*200, 30+i*48);
    }*/
    else{
      textSize(32);
      text(strings[i], width/2, 30+i*48);
    }

  }

  if(prevSecond != second()){
    counter ++;
    prevSecond = second();
    if(counter >= strings.length){
      counter = 0;
    }
  }

  t = t + 0.025;

  
  
  //console.log(counter);
}