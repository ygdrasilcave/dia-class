var h;
var t;
var s;

function setup() {
  // put setup code here
  createCanvas(800, 600);
  h = 0;
  t = 0;
  s = 50;
}

function draw() {
  // put drawing code here
  background(0);

  stroke(255, 0 ,0);
  for(var i=0; i<width; i=i+s){
    line(i, 0, i, height);
  }

  stroke(0, 255, 0);
  for(var i=0; i<height; i=i+s){
    line(0, i, width, i);
  }

  noStroke();
  for(var i=s*0.5; i<width; i+=s){
    for(var j=s*0.5; j<height; j+=s){
      fill(255);
      ellipse(i, j, s*0.8, s*0.8);
      fill(0);
      ellipse(i-s*0.22, j-s*0.15, s*0.15, s*0.15);
      ellipse(i+s*0.22, j-s*0.15, s*0.15, s*0.15);
      ellipse(i, j+s*0.15, s*0.55, s*h);
    }  
  }

  h = map(sin(t), -1, 1, 0.025, 0.31);
  t += 0.25;

  //console.log(x);
}