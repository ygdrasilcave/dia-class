function setup() {
  // put setup code here
  createCanvas(800, 600);

}

function draw() {
  // put drawing code here
  background(0);

  stroke(255, 0 ,0);
  line(400, 0, 400, 600);
  line(width/2-10, 0, width/2-10, height);
  line(width/2+10, 0, width/2+10, height);
  line(width/2-20, 0, width/2-20, height);
  line(width/2+20, 0, width/2+20, height);
  line(width/2-30, 0, width/2-30, height);
  line(width/2+30, 0, width/2+30, height);
  line(width/2-40, 0, width/2-40, height);
  line(width/2+40, 0, width/2+40, height);
  line(width/2-50, 0, width/2-50, height);
  line(width/2+50, 0, width/2+50, height);
  line(width/2-60, 0, width/2-60, height);
  line(width/2+60, 0, width/2+60, height);
  line(width/2-70, 0, width/2-70, height);
  line(width/2+70, 0, width/2+70, height);
  line(width/2-80, 0, width/2-80, height);
  line(width/2+80, 0, width/2+80, height);
  line(width/2-90, 0, width/2-90, height);
  line(width/2+90, 0, width/2+90, height);
  line(width/2-100, 0, width/2-100, height);
  line(width/2+100, 0, width/2+100, height);
  line(width/2-110, 0, width/2-110, height);
  line(width/2+110, 0, width/2+110, height);
  line(width/2-120, 0, width/2-120, height);
  line(width/2+120, 0, width/2+120, height);
  line(width/2-130, 0, width/2-130, height);
  line(width/2+130, 0, width/2+130, height);
  line(width/2-140, 0, width/2-140, height);
  line(width/2+140, 0, width/2+140, height);
  line(width/2-150, 0, width/2-150, height);
  line(width/2+150, 0, width/2+150, height);
  line(width/2-160, 0, width/2-160, height);
  line(width/2+160, 0, width/2+160, height);
  line(width/2-170, 0, width/2-170, height);
  line(width/2+170, 0, width/2+170, height);
  line(width/2-180, 0, width/2-180, height);
  line(width/2+180, 0, width/2+180, height);
  line(width/2-190, 0, width/2-190, height);
  line(width/2+190, 0, width/2+190, height);
  line(width/2-200, 0, width/2-200, height);
  line(width/2+200, 0, width/2+200, height);
  line(width/2-210, 0, width/2-210, height);
  line(width/2+210, 0, width/2+210, height);
  line(width/2-220, 0, width/2-220, height);
  line(width/2+220, 0, width/2+220, height);
  line(width/2-230, 0, width/2-230, height);
  line(width/2+230, 0, width/2+230, height);
  line(width/2-240, 0, width/2-240, height);
  line(width/2+240, 0, width/2+240, height);
  line(width/2-250, 0, width/2-250, height);
  line(width/2+250, 0, width/2+250, height);
  line(width/2-260, 0, width/2-260, height);
  line(width/2+260, 0, width/2+260, height);
  line(width/2-270, 0, width/2-270, height);
  line(width/2+270, 0, width/2+270, height);
  line(width/2-280, 0, width/2-280, height);
  line(width/2+280, 0, width/2+280, height);
  line(width/2-290, 0, width/2-290, height);
  line(width/2+290, 0, width/2+290, height);
  line(width/2-300, 0, width/2-300, height);
  line(width/2+300, 0, width/2+300, height);
  line(width/2-310, 0, width/2-310, height);
  line(width/2+310, 0, width/2+310, height);
  line(width/2-320, 0, width/2-320, height);
  line(width/2+320, 0, width/2+320, height);
  line(width/2-330, 0, width/2-330, height);
  line(width/2+330, 0, width/2+330, height);
  line(width/2-340, 0, width/2-340, height);
  line(width/2+340, 0, width/2+340, height);
  line(width/2-350, 0, width/2-350, height);
  line(width/2+350, 0, width/2+350, height);
  line(width/2-360, 0, width/2-360, height);
  line(width/2+360, 0, width/2+360, height);
  line(width/2-370, 0, width/2-370, height);
  line(width/2+370, 0, width/2+370, height);
  line(width/2-380, 0, width/2-380, height);
  line(width/2+380, 0, width/2+380, height);
  line(width/2-390, 0, width/2-390, height);
  line(width/2+390, 0, width/2+390, height);
  line(width/2-400, 0, width/2-400, height);
  line(width/2+400, 0, width/2+400, height);

  stroke(0, 255, 0);
  for(var i=0; i<height; i=i+10){
    line(0, i, width, i);
  }

  //console.log(x);
}