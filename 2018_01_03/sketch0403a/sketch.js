var soundFile;

function preload() {
  soundFormats('mp3', 'ogg', 'wav');
  soundFile = loadSound('data/voice.wav');
}

function setup() {
  // put setup code here
  createCanvas(800, 600);
  //soundFile.play();
  soundFile.loop();
}

function draw() {
  // put drawing code here
  background(200);

  var volume = map(mouseX, 0, width, 0, 1);
  volume = constrain(volume, 0, 1);
  soundFile.amp(volume);

  var speed = map(mouseY, 0, height, 0.25, 2.75);
  speed = constrain(speed, 0.01, 4);
  soundFile.rate(speed);

  stroke(0);
  fill(51, 100);
  ellipse(mouseX, 100, 48, 48);
  fill(255);
  noStroke();
  textSize(26);
  text('volume', mouseX, 100);
  stroke(0);
  fill(51, 100);
  ellipse(100, mouseY, 48, 48);
  fill(255);
  noStroke();
  text('speed', 100, mouseY);

}

/*function mousePressed(){
  soundFile.play();
}*/