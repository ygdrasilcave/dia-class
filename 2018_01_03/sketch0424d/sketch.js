var x;
var y;
var speedX;
var speedY;
var dirX;
var dirY;

function preload() {
}

function setup() {
  createCanvas(1024, 600);
  x = width/2;
  y = height/2;
  speedX = random(2, 6);
  speedY = random(3, 7);
  dirX = 1;
  dirY = 1;
}

function draw() {
  background(0);

  applyMatrix();
  translate(x, y);
  rotate(radians(map(mouseX, 0, width, 0, 360)));
  fill(255);
  noStroke();
  rect(0-50, 0-50, 100, 100);
  ellipse(120, 0, 20, 20);
  resetMatrix();
  x = x + dirX*speedX;
  y = y + dirY*speedY;

  if(x > width-50){
    x = width-50;
    dirX = dirX*-1;
  }
  if(x < 50){
    x = 50;
    dirX = dirX*-1;
  }
  if(y > height-50){
    y = height-50;
    dirY = dirY*-1;
  }
  if(y < 50){
    y = 50;
    dirY = dirY*-1;
  }

  applyMatrix();
  translate(mouseX, mouseY);
  rotate(radians(30));
  noFill();
  stroke(255, 0, 0);
  strokeWeight(3);
  rect(-50, -50, 100, 100);
  resetMatrix();
}

