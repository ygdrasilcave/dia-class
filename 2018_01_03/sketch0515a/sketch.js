//Creating animations

//animations like p5 images should be stored in variables
//in order to be displayed during the draw cycle
var cardianl, asterisk;

//it's advisable (but not necessary) to load the images in the preload function
//of your sketch otherwise they may appear with a little delay
function preload() {
  //create an animation from a sequence of numbered images
  //pass the first and the last file name and it will try to find the ones in between
  cardinal = loadAnimation("data/cardinal00.png", "data/cardinal07.png");
  cardinal.frameDelay = 3;
    
  //create an animation listing all the images files
  asterisk = loadAnimation("data/asterisk.png", "data/triangle.png", "data/square.png", "data/cloud.png", "data/star.png", "data/mess.png", "data/monster.png");
  asterisk.frameDelay = 6;
}

function setup() {
  createCanvas(800,800);
}

function draw() {
  clear();

  background(255,255,255);
 
  //specify the animation instance and its x,y position
  //animation() will update the animation frame as well
  animation(cardinal, 300, 150);
  animation(asterisk, 500, 150);
}




