var x1;
var x2;

function setup() {
  // put setup code here
  createCanvas(800, 600);
  x1 = 0;
  x2 = 0;
}

function draw() {
  // put drawing code here
  background(255);

  stroke(0);
  line(width/2, 0, width/2, height);

  line(width/2-10, 0, width/2-10, height);
  line(width/2-10-10, 0, width/2-10-10, height);

  line(width/2+10, 0, width/2+10, height);  
  line(width/2+10+10, 0, width/2+10+10, height);

  stroke(255, 0, 0);
  line(100+x1, 0, 100+x1, height);
  x1 = x1 + 1;

  stroke(0, 0, 255);
  line(width-100-x2, 0, width-100-x2, height);
  x2 = x2 + 2;

  //console.log(x);
}