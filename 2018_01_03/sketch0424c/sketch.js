var change = false;
var prevChange = true;

var x = [];
var y = [];
var totalNum = 150;
var counter = 0;

function preload() {
}

function setup() {
  createCanvas(1024, 600);
  for(var i=0; i<totalNum; i++){
    x[i] = width/2;
    y[i] = height/2;
  }
  counter = 0;
}

function draw() {
  background(255);

  stroke(120, 120, 120);
  fill(255, 255, 0);
  beginShape();
  for(var i=0; i<totalNum; i++){
    vertex(x[i], y[i]);
  }
  endShape();

  fill(0);
  noStroke();
  for(var i=0; i<totalNum; i++){
    ellipse(x[i], y[i], 10, 10);
  }

  if(keyIsPressed == true && prevChange == true){
    if(key == 'c' || key == 'C'){
    	if(change == false){
    		change = true;
    	}else{
    		change = false;
    	}
    	prevChange = false;
    	console.log(change);
    }else if(key == ' '){
      for(var i=0; i<totalNum; i++){
        x[i] = width/2;
        y[i] = height/2;
      }
      counter = 0;
    }
  }else if(keyIsPressed == false && prevChange == false){
  	if(key == 'c' || key == 'C'){
      prevChange = true;
    }
  }

  noStroke();
  fill(0);
  text(counter, 20, 20);
}

function mouseReleased(){
  if(change == true){
    x[counter] = mouseX;
    y[counter] = mouseY;
    counter++;
    if(counter >= totalNum){
      counter = 0;
    }
  }
}
