var rectSize = 20;
var cols;
var rows;

var rotVal = 0;

var rectOffset = 0;
var rectOffsetDir = 1;

var colorR = [];
var colorG = [];
var colorB = [];

var toggle = true;
var colorChange = 0;
var mode = false;

function preload() {
}

function setup() {
  createCanvas(1000, 600);
  background(0);
  cols = width/rectSize;
  rows = height/rectSize;
  for(var i=0; i<cols*rows; i++){
    colorR[i] = random(255);
    colorG[i] = random(255);
    colorB[i] = random(255);
  }
}

function draw() {
  background(0);
  fill(255);

  var index = 0;
  for(var y=0; y<rows; y++){
    for(var x=0; x<cols; x++){    
      applyMatrix();
      translate(x*rectSize, y*rectSize);
      rotate(radians(rotVal));

      if(mode == true){
        if(y > (200/rectSize) && y < ((height-200)/rectSize) && x > (200/rectSize) && x < ((width-200)/rectSize)){
          stroke(colorR[index], colorG[index], colorB[index]);
          noFill();
        }else{
          stroke(255);
          fill(colorR[index], colorG[index], colorB[index]);
        }
      }else{
        /*var _x = (x*rectSize)-(width/2);
        var _y = (y*rectSize)-(height/2);
        if(sqrt(_x*_x + _y*_y) < 250){*/
        /*var dir = dist(width/2, height/2, x*rectSize, y*rectSize);
        if(dir < 250){*/
        var dir = dist(width/2, height/2, x*rectSize, y*rectSize);
        if(dir < 250){
          stroke(255);
          fill(colorR[index], colorG[index], colorB[index]);
        }else{
          stroke(colorR[index], colorG[index], colorB[index]);
          noFill();
        }
      }

      rect(-(rectSize+rectOffset)*0.5, -(rectSize+rectOffset)*0.5, rectSize+rectOffset, rectSize+rectOffset);
      resetMatrix();
      index++;
    }
  }

  rotVal = rotVal + 1;
  rectOffset = rectOffset + (0.25 * rectOffsetDir);
  if(rectOffset > 10){
    rectOffset = 10;
    rectOffsetDir = -1;
  }else if(rectOffset < -10){
    rectOffset = -10;
    rectOffsetDir = 1;
  }

  if(second()%5 == 0 && toggle == true){
    colorChange++;
    if(colorChange > 2){
      colorChange = 0;
      //mode = !mode;
      if(mode == true){
        mode = false;
      }else{
        mode = true;
      }
    }
    colorChangeFunc();
    toggle = false;
  }else if(second()%5 == 1 && toggle == false){
    toggle = true;
  }
}

function colorChangeFunc(){
  if(colorChange == 0){
    for(var i=0; i<cols*rows; i++){
      colorR[i] = random(255);
      colorG[i] = random(255);
      colorB[i] = random(255);
    }
  }
  else if(colorChange == 1){
    for(var i=0; i<cols*rows; i++){
      colorR[i] = map(i, 0, (cols*rows)-1, 0, 255);
      colorG[i] = map(i, 0, (cols*rows)-1, 0, 255);
      colorB[i] = 0;
    }
  }
  else if(colorChange == 2){
    for(var i=0; i<cols*rows; i++){
      colorR[i] = 0;
      colorG[i] = map(i, 0, (cols*rows)-1, 0, 255);
      colorB[i] = map(i, 0, (cols*rows)-1, 0, 255);
    }
  }

}



