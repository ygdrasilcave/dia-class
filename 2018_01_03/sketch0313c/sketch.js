var myFont;
var strings;
var counter;

function preload() {
  myFont = loadFont('data/NanumBarunGothic.otf');
  strings = loadStrings('data/test.txt');
}

function setup() {
  createCanvas(600, 600);
  textFont(myFont, 36);
  counter = 0;
}

function draw() {
  background(79, 203, 0);

  fill(100, 0, 0);
  strokeWeight(1);
  stroke(0, 0, 255);
  rect(200, 300, 200, 100);

  fill(0, 100, 0);
  noStroke();
  ellipse(200, 300, 200, 100);

  fill(0);
  textAlign(CENTER, CENTER);
  text(strings[counter], width/2, height/2);
  counter++;
  if(counter >= strings.length){
    counter = 0;
  }

  //console.log(strings[0]); 
}