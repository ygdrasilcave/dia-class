function setup() {
  // put setup code here
  createCanvas(600, 600);
}

function draw() {
  // put drawing code here
  background(79, 203, 203);

  fill(100, 0, 0);
  strokeWeight(5);
  stroke(0, 0, 255);
  rect(200, 300, 200, 100);

  fill(0, 100, 0);
  noStroke();
  ellipse(200, 300, 200, 100);
}