var s;

function setup() {
  // put setup code here
  createCanvas(800, 600);
  s = 30;
}

function draw() {
  // put drawing code here
  background(0);

  stroke(255, 0 ,0);
  for(var i=0; i<width; i=i+s){
    line(i, 0, i, height);
  }

  stroke(0, 255, 0);
  for(var i=0; i<height; i=i+s){
    line(0, i, width, i);
  }

  noStroke();
  fill(255);
  for(var j=0; j<height; j=j+s){
    for(var i=0; i<width; i=i+s){
      ellipse(s*0.5+i, s*0.5+j, s*0.8, s*0.8);
    }
  }

  //console.log(x);
}