var x, y;
var speedX;
var speedY;
var rad;
var clear;
var noiseScale;
var tx, ty;

var strokeColor;

var dirX = 1;
var dirY = 1;

var speedXMin = 0;
var speedXMax = 10;
var speedXStep = 0.1;
var speedYMin = 0;
var speedYMax = 10;
var speedYStep = 0.1;
var radMin = 1;
var radMax = 150;
var radStep = 0.1;
var noiseScaleMin = 0;
var noiseScaleMax = 5;
var noiseScaleStep = 0.1

// the gui object
var gui;

var rot;
var rotSpeed;

var rotSpeedMin = 0;
var rotSpeedMax = 0.05;
var rotSpeedStep = 0.001;

var numgon;
var numgonMin = 3;
var numgonMax = 30;
var numgonStep = 1;

var noiseVal = {};
var nt = {};
var ntSpeed = {};

var shapeNoise = false;
var shapeNoiseToggle = 0;

function setup() {
  // create a canvas that fills the window
  createCanvas(windowWidth, windowHeight);

  x = width/2;
  y = height/2;

  speedX = 1;
  speedY = 1;

  rad = 10;

  clear = true;

  noiseScale = 0;
  tx = 0.0;
  ty = 0.0;

  strokeColor = [0, 0, 0];

  rotSpeed = 0;

  numgon = 3;

  for(var i=0; i<numgonMax; i++){
    noiseVal[i] = 0;
    nt[i] = 0;
    ntSpeed[i] = random(0.01, 0.025);
  }

  //console.log(noiseVal);

  // create the GUI
  gui = createGui('speed control');
  gui.addGlobals('numgon', 'shapeNoise','speedX', 'speedY', 'rad', 'strokeColor',
    'noiseScale', 'rotSpeed', 'clear');

  // only call draw when then gui is changed
  //noLoop();
  rectMode(CENTER);

  rot = 0;
}


function draw() {

  if(shapeNoise == true){
    shapeNoiseToggle = 1;
  }else{
    shapeNoiseToggle = 0;
  }

  if(clear == true){
    background(255);
  }

  x += speedX*dirX;
  y += speedY*dirY;

  x += map(noise(tx), 0.0, 1.0, -1, 1)*noiseScale;
  y += map(noise(ty), 0.0, 1.0, -1, 1)*noiseScale;
  tx+=0.015;
  ty+=0.025;

  if(x > width-rad/2.0){
    x = width-rad/2.0;
    dirX = dirX*-1;
  }else if(x < rad/2.0){
    x = rad/2.0;
    dirX = dirX*-1;
  }
  if(y > height-rad/2.0){
    y = height-rad/2.0;
    dirY = dirY*-1;
  }else if(y < rad/2.0){
    y = rad/2.0;
    dirY = dirY*-1;
  }

  applyMatrix();
  noFill();
  stroke(red(strokeColor), green(strokeColor),blue(strokeColor));
  strokeWeight(.25);
  translate(x, y);
  rotate(rot);
  //rect(0, 0, rad, rad);
  drawShape(0, 0, rad, rad, numgon);
  resetMatrix();

  rot = rot + rotSpeed;
}

// dynamically adjust the canvas to the window
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

function drawShape(_x, _y, _rad, _rad, _n){
  beginShape();
  
  for(var i=0; i<_n; i++){
    noiseVal[i] = map(noise(nt[i]), 0, 1, -_rad/2, _rad/2)*shapeNoiseToggle;
    var x = _x + cos((TWO_PI/_n)*i)*(_rad+noiseVal[i]);
    var y = _y + sin((TWO_PI/_n)*i)*(_rad+noiseVal[i]);
    vertex(x, y);
    nt[i] = nt[i] + ntSpeed[i];
  }
  
  endShape(CLOSE);
}
