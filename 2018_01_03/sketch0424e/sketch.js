var rot = 0;

function preload() {
}

function setup() {
  createCanvas(1024, 600);
}

function draw() {
  background(0);
  bodyRight();
  bodyLeft();
  armRight();
  armLeft();
  head();

  rot = radians(map(mouseY, 0, height, -45, 45));
}

function bodyRight(){
  applyMatrix();
  translate(mouseX, mouseY);
  rotate(rot);
  fill(255);
  noStroke();
  rect(-10, -50, 20, 100);

  applyMatrix();
  fill(255, 255, 0);
  translate(0, 50)
  rotate(radians(map(mouseX, 0, width, 0, 90)));
  rect(0, -10, 100, 20);
  resetMatrix();

  resetMatrix();

}

function bodyLeft(){
  applyMatrix();
  translate(mouseX, mouseY);
  rotate(rot);
  fill(255);
  noStroke();
  rect(-10, -50, 20, 100);

  applyMatrix();
  fill(255, 255, 0);
  translate(0, 50)
  rotate(radians(map(mouseX, 0, width, 180, 90)));
  rect(0, -10, 100, 20);
  resetMatrix();

  resetMatrix();
}

function armRight(){
  applyMatrix();
  translate(mouseX, mouseY);
  rotate(rot);
  fill(255);
  noStroke();
  rect(-10, -50, 20, 100);

  applyMatrix();
  fill(255, 255, 0);
  translate(0, -50)
  rotate(radians(map(mouseX, 0, width, 0, 90)));
  rect(0, -10, 100, 20);
  resetMatrix();

  resetMatrix();
}

function armLeft(){
  applyMatrix();
  translate(mouseX, mouseY);
  rotate(rot);
  fill(255);
  noStroke();
  rect(-10, -50, 20, 100);

  applyMatrix();
  fill(255, 255, 0);
  translate(0, -50)
  rotate(radians(map(mouseX, 0, width, 180, 90)));
  rect(0, -10, 100, 20);
  resetMatrix();

  resetMatrix();
}

function head(){
  applyMatrix();
  translate(mouseX, mouseY);
  rotate(rot);
  fill(255);
  noStroke();
  rect(-10, -50, 20, 100);

  applyMatrix();
  fill(255, 255, 0);
  translate(0, -50)
  rotate(radians(map(mouseX, 0, width, 45, 135)));
  ellipse(-25, 0, 50, 50)
  resetMatrix();

  resetMatrix();
}

