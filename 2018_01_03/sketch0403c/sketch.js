var soundFile;
var minPitch;
var volume;
var x;
var y;

function preload() {
  soundFormats('mp3', 'ogg', 'wav');
  soundFile = loadSound('data/teardrop2.wav');
}

function setup() {
  // put setup code here
  createCanvas(800, 600);
  minPitch = 0.5;
  volume = 1.0;
  x = 0;
  y = 20;
  textAlign(CENTER, CENTER);
  textSize(20);
  background(0);
}

function draw() {
  // put drawing code here
  //background(0);
  //console.log(speed);
  minPitch = map(mouseX, 0, width, 0, 1);
  volume = map(mouseY, 0, height, 0, 1.5);

}

/*function mousePressed(){
  soundFile.play();
}*/

function keyPressed(){
  noStroke();
  if(key == 'z' || key == 'Z'){
    soundFile.play();
    soundFile.rate(minPitch + 0.25);
    soundFile.amp(volume);
    fill((minPitch + 0.25)*300);
    drawText();
  }
  else if(key == 'v' || key == 'V'){
    soundFile.play();
    soundFile.rate(minPitch + 0.5);
    soundFile.amp(volume);
    fill((minPitch + 0.5)*300);
    drawText();
  }
  else if(key == 'x' || key == 'X'){
    soundFile.play();
    soundFile.rate(minPitch + 0.75);
    soundFile.amp(volume);
    fill((minPitch + 0.75)*300);
    drawText();
  }
  else if(key == 'c' || key == 'C'){
    soundFile.play();
    soundFile.rate(minPitch + 1.0);
    soundFile.amp(volume);
    fill((minPitch + 1.0)*300);
    drawText();
  }
  else if(key == 'u' || key == 'U'){
    soundFile.play();
    soundFile.rate(minPitch + 1.25);
    soundFile.amp(volume);
    fill((minPitch + 1.25)*300);
    drawText();
  }
  else if(key == 'y' || key == 'Y'){
    soundFile.play();
    soundFile.rate(minPitch + 1.5);
    soundFile.amp(volume);
    fill((minPitch + 1.5)*300);
    drawText();
  }
  else if(key == 'i' || key == 'I'){
    soundFile.play();
    soundFile.rate(minPitch + 1.75);
    soundFile.amp(volume);
    fill((minPitch + 1.75)*300);
    drawText();
  }
  else if(key == 'o' || key == 'O'){
    soundFile.play();
    soundFile.rate(minPitch + 2.0);
    soundFile.amp(volume);
    fill((minPitch + 2.0)*300);
    drawText();
  }
}

function drawText(){
  textSize(volume*20);
  text('Ah!', x, y);
  x+=20;
  if(x>width){
    x = 0;
    y+=20;
  }
}