var change = false;
var prevChange = true;

var x = [0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0];
var y = [0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0];


function preload() {
}

function setup() {
  createCanvas(1024, 600);
}

function draw() {
  background(255);

  if(change == true){
  	for(var i=0; i<45; i++){
  	  x[i] = random(-100, width+100);
  	  y[i] = random(-100, height+100);
    }
  }

  noStroke();
  fill(255, 255, 0);
  beginShape();  
  for(var i=0; i<20; i++){
  	vertex(x[i], y[i]);
  }
  endShape();

  fill(0, 255, 0, 120);
  beginShape();  
  for(var i=0; i<10; i++){
  	vertex(x[i+20], y[i+20]);
  }
  endShape();

  fill(255, 0, 255, 120);
  beginShape();  
  for(var i=0; i<15; i++){
  	vertex(x[i+30], y[i+30]);
  }
  endShape();

  noStroke();
  fill(0, 0, 0);
  for(var i=0; i<45; i=i+2){
  	ellipse(x[i], y[i], 10, 10);
  }

  noFill();
  stroke(0);
  for(var i=0; i<45; i++){
  	if(i%2 == 1){
  	  ellipse(x[i], y[i], 10, 10);
    }
  }

  noFill();
  stroke(120, 120, 120);
  beginShape();
  for(var i=0; i<45; i++){
  	vertex(x[i], y[i]);
  }
  endShape();

  if(keyIsPressed == true && prevChange == true){  	
  	if(change == false){
  		change = true;
  	}else{
  		change = false;
  	}
  	prevChange = false;
  	console.log(change);
  }else if(keyIsPressed == false && prevChange == false){
  	prevChange = true;
  }
}

/*function keyPressed(){
	console.log("bang");
}*/