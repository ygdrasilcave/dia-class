var x, y;
var speedX;
var speedY;
var rad;
var clear;
var noiseScale;
var tx, ty;

var strokeColor;

var dirX = 1;
var dirY = 1;

var speedXMin = 0;
var speedXMax = 10;
var speedXStep = 0.1;
var speedYMin = 0;
var speedYMax = 10;
var speedYStep = 0.1;
var radMin = 1;
var radMax = 150;
var radStep = 0.1;
var noiseScaleMin = 0;
var noiseScaleMax = 5;
var noiseScaleStep = 0.1

// the gui object
var gui;

function setup() {
  // create a canvas that fills the window
  createCanvas(windowWidth, windowHeight);

  x = width/2;
  y = height/2;

  speedX = 1;
  speedY = 1;

  rad = 10;

  clear = true;

  noiseScale = 0;
  tx = 0.0;
  ty = 0.0;

  strokeColor = [0, 0, 0];

  // create the GUI
  gui = createGui('speed control');
  gui.addGlobals('speedX', 'speedY', 'rad', 'strokeColor',
    'noiseScale', 'clear');

  // only call draw when then gui is changed
  //noLoop();
}


function draw() {

  if(clear == true){
    background(255);
  }

  x += speedX*dirX;
  y += speedY*dirY;

  x += map(noise(tx), 0.0, 1.0, -1, 1)*noiseScale;
  y += map(noise(ty), 0.0, 1.0, -1, 1)*noiseScale;
  tx+=0.015;
  ty+=0.025;

  if(x > width-rad/2.0){
    x = width-rad/2.0;
    dirX = dirX*-1;
  }else if(x < rad/2.0){
    x = rad/2.0;
    dirX = dirX*-1;
  }
  if(y > height-rad/2.0){
    y = height-rad/2.0;
    dirY = dirY*-1;
  }else if(y < rad/2.0){
    y = rad/2.0;
    dirY = dirY*-1;
  }

  noFill();
  stroke(red(strokeColor), green(strokeColor),blue(strokeColor));
  strokeWeight(.25);
  ellipse(x, y, rad, rad);
  

}

// dynamically adjust the canvas to the window
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
