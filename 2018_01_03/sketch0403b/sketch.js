var soundFile;
var currTime;
var duration;

function preload() {
  soundFormats('mp3', 'ogg', 'wav');
  soundFile = loadSound('data/teardrop.wav');
}

function setup() {
  // put setup code here
  createCanvas(800, 600);
  soundFile.play();
  soundFile.loop();
  currTime = millis();
  duration = 500;

}

function draw() {
  // put drawing code here
  background(200);

  soundFile.amp(1);

  var speed;
  if(currTime + duration < millis()){
    speed = random(0.0, 2.75);
    speed = constrain(speed, 0.01, 4);
    currTime = millis();
  }
  soundFile.rate(speed);

  duration = map(mouseX, 0, width, 50, 1000);

  //console.log(speed);
}

/*function mousePressed(){
  soundFile.play();
}*/

function keyPressed(){
  if(key == 'a' || key == 'A'){
    
  }
}