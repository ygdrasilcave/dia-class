let assets_folder = '../assets/sound/';
let audioFile;
let amp;

function preload(){
  audioFile = loadSound(assets_folder + 'five-dollars.mp3');
}

function setup() {
  let cnv = createCanvas(600, 600);
  cnv.mousePressed(canvasPressed);
  amp = new p5.Amplitude();
}

function canvasPressed() {
  audioFile.loop();
  //background(0, 200, 50);
}

function draw() {
  //background(0);
  fill(0, 20);
  rect(0,0,width,height);
  let rateValue = map(mouseX, 0, width, -2, 2);
  audioFile.rate(rateValue);
  let level = amp.getLevel();
  noFill();
  stroke(255);
  ellipse(width/2, height/2, level*2000, level*2000);
  //print(level);
  
}

function keyPressed(){
  //play([startTime], [rate], [amp], [cueStart], [duration])
  //print(audioFile.duration());
  //audioFile.play(0, 1, 1, 1.5, 1);
}