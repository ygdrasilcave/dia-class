//Collision detection and resolution
//move the mouse, the sprite responds to overlapings, collision,
//and displaces another sprite

let assets_folder = '../assets/playSource/';

let ani_box, ani_asterisk1, ani_asterisk2, ani_treasure;
let sprite_asterisk;
let group_box, group_treasure;

let debugMode = false;

function preload(){
  ani_box = loadAnimation(assets_folder+'box0001.png', assets_folder+'box0003.png');
  ani_asterisk1 = loadAnimation(assets_folder+'asterisk_normal0001.png', assets_folder+'asterisk_normal0003.png');
  ani_asterisk2 = loadAnimation(assets_folder+'asterisk_stretching0001.png', assets_folder+'asterisk_stretching0008.png');
  ani_treasure = loadAnimation(assets_folder+'small_circle0001.png', assets_folder+'small_circle0003.png');
}

function setup() {
  createCanvas(1000, 800);

  group_box = new Group();
  group_treasure = new Group();

  sprite_asterisk = createSprite(random(width), random(height));
  sprite_asterisk.addAnimation('normal', ani_asterisk1);
  sprite_asterisk.addAnimation('eating', ani_asterisk2);

  for(let i=0; i<4; i++){
    let sprite_box = createSprite(random(width), random(height));
    sprite_box.addAnimation('normal', ani_box);
    group_box.add(sprite_box);
  }

  for(let i=0; i<15; i++){
    let sprite_treasure = createSprite(random(width), random(height));
    sprite_treasure.addAnimation('normal', ani_treasure);
    group_treasure.add(sprite_treasure);
  }

  //noCursor();

  textAlign(CENTER, CENTER);
  textSize(32);
}

function draw() {
  background(200, 200, 200);

  sprite_asterisk.velocity.x = (mouseX - sprite_asterisk.position.x)*0.1;
  sprite_asterisk.velocity.y = (mouseY - sprite_asterisk.position.y)*0.1;

  sprite_asterisk.collide(group_box);

  //if(sprite_asterisk.overlap(group_treasure)==true){
  //  sprite_asterisk.changeAnimation('eating');
  //}else{
  //  sprite_asterisk.changeAnimation('normal');
  //}

  sprite_asterisk.overlap(group_treasure, get_treasure);
  if(sprite_asterisk.getAnimationLabel()=='eating' && sprite_asterisk.animation.getFrame() == sprite_asterisk.animation.getLastFrame()){
    sprite_asterisk.changeAnimation('normal');
  }

  if(group_treasure.size() <= 0){
    text("잘 먹었습니다.", width/2, height/2);
  }

  drawSprites();
}

function get_treasure(_collector, _collected){
  _collector.changeAnimation('eating');
  _collector.animation.changeFrame(0);
  _collected.remove();

  print(group_treasure.size());
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debugMode = !debugMode;
  }
}