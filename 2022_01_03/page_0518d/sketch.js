var num = 20;
var angle = 360;
var radius = 250;

var gui;

function setup() {
  createCanvas(1000, 800);

  gui = createGui('parameters').setPosition(10, 10);

  sliderRange(30, 300, 1);
  gui.addGlobals('radius');

  sliderRange(1, 100, 1);
  gui.addGlobals('num');
  
  sliderRange(15, 360, 1);
  gui.addGlobals('angle');
}

function draw() {
  background(0);

  fill(255, 255, 0);
  stroke(255);

  for(let i=0; i<num; i++){
    
    let x = width/2 + cos(radians((angle/num)*i)) * radius;
    let y = height/2 + sin(radians((angle/num)*i)) * radius;

    ellipse(x, y, 50, 50);
  }

}