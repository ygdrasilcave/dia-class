
function setup() {
  createCanvas (800,800);
  background('beige');

}

let circle_size = 20;


function draw() {

  noStroke();
  fill(193,132,56);
  circle(width/2,height/2,530);

  fill(255,105,148);
  circle(width/2,height/2,500);

  fill(244,91,142);
  circle(width/2,height/2,300);

  fill ('yellow');
  circle(350,410,circle_size);
  circle(250,350,circle_size);
  circle(350,250,circle_size);
  circle(450,380,circle_size);
  circle(550,440,circle_size);
  circle(380,610,circle_size);
  circle(520,320,circle_size);
  circle(190,310,circle_size);

  strokeWeight(12);
  stroke('red');
  point(405,500);
  point(300,510);
  point(500,280);
  point(290,270);
  point(435,250);
  point(500,550);

  strokeWeight(10);
  stroke(251,255,161);
  line(310,480,335,500);
  line(350,280,370,260);
  line(480,530,500,510);

  strokeWeight(10);
  stroke(141,250,255);
  line(250,370,270,390);
  line(500,240,520,260);
  line(430,570,410,550);
    
  stroke(255,194,233);
  noFill();
  strokeWeight(11);
  arc(270,480,50,50, HALF_PI, PI);
  arc(490,310,50,50,0, HALF_PI);

  


  noStroke();
  fill('beige');
  circle(width/2,height/2,200);

  if (mouseIsPressed) {
    noStroke();
    fill('beige');
    fill('black');
    circle(mouseX,mouseY,200);
  


  }

  


}

//function mousePressed() {
//  noStroke();
//  fill('beige');
//  fill('black');
//  circle(mouseX,mouseY,200);
//}