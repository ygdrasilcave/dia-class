let assets_folder = '../assets/sound/';
let audioFile;

function preload(){
  audioFile = loadSound(assets_folder + 'five-dollars.mp3');
}

function setup() {
  createCanvas(800, 800);
}

function draw() {
  background(0);
}

function keyPressed(){
  //play([startTime], [rate], [amp], [cueStart], [duration])
  print(audioFile.duration());
  audioFile.play(0, 1, 1, 1.5, 1);
}