let assets_folder = '../assets/sound/';
let audioFile;
//let amp;
let reverb;

function preload(){
  audioFile = loadSound(assets_folder + 'five-dollars.mp3');
}

function setup() {
  let cnv = createCanvas(600, 600);
  cnv.mousePressed(canvasPressed);
  //amp = new p5.Amplitude();

  reverb = new p5.Reverb();
  audioFile.disconnect();
  
  reverb.process(audioFile, 3, 2);
}

function canvasPressed() {
  audioFile.loop();
}

function draw() {
  //background(0);
  fill(0, 20);
  rect(0,0,width,height);
  noFill();
  stroke(255);
  ellipse(width/2, height/2, 100, 100);

  let dryWet = constrain(map(mouseX, 0, width, 0, 1), 0, 1);
  // 1 = all reverb, 0 = no reverb
  reverb.drywet(dryWet);
  
}

function keyPressed(){
  //play([startTime], [rate], [amp], [cueStart], [duration])
  //print(audioFile.duration());
  //audioFile.play(0, 1, 1, 1.5, 1);
}