let x = 15;
let y = 27;
let words = [];

let p1 = "죽는 날까지 하늘을 우러러 한 점 부끄럼이 없기를, 잎새에 이는 바람에도, 나는 괴로워했다. 별을 노래하는 마음으로 모든 죽어가는 것을 사랑해야지 그리고 나한테 주어진 길을 걸어가야겠다. 오늘 밤에도 별이 바람에 스치운다."
let p2 = "나 보기가 역겨워 가실 때에는 말없이 고이 보내 드리오리다 영변에 약산 진달래꽃 아름 따다 가실 길에 뿌리오리다 가시는 걸음걸음 놓인 그 꽃을 사뿐히 즈려밟고 가시옵소서 나 보기가 역겨워 가실 때에는 죽어도 아니 눈물 흘리오리다"

let words1 = [];
let words2 = [];
let index = 0;
let prevTime = 0;
let wordsHeight = 0;
let whichWords = 0;

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  y = 16;
  print((x + y)/2);

  words[0] = "what";
  words[1] = "are";
  words[2] = "arrays";
  print(words[0] + " " + words[1] + " " + words[2])
  print(words.length);

  print(p1);
  let _temp1 = p1.split(' ');
  print(_temp1);
  print(_temp1.length);

  textSize(60);
  let wordMaxWidth = 0;
  for(let i=0; i < _temp1.length; i=i+1){
      words1[i] = _temp1[i];
      if(textWidth(_temp1[i]) > wordMaxWidth){
        wordMaxWidth = textWidth(_temp1[i]);
      }
  }
  print(width);
  print(wordMaxWidth);

  let _temp2 = p2.split(' ');
  for(let i=0; i < _temp2.length; i=i+1){
      words2[i] = _temp2[i];
  }

  textAlign(CENTER, CENTER);

  prevTime = millis();
  print('prveTime: ' + str(prevTime));

  y = height;

  //frameRate(8);
}


function draw() {
  background(0);  //check this syntax with alpha value

  noStroke();
  fill(255);
  /*for(let i=0; i<16; i++){
    push();
    translate(width/2, height/2);
    rotate((TWO_PI/16.0)*i);
    text(words2[16+i], 100, 0);
    pop();
  }*/
  if(whichWords == 0){
    push();
    translate(0, y);
    for(let i=0; i < words1.length; i++){
      text(words1[i], width/2, 100 + 100*i);
    }
    pop();
  
    wordsHeight = 100 + 100*(words1.length-1);
    y = y-1;
    if(y < -wordsHeight){
      y = height;
      whichWords = 1;
    }
  }
  else{
    push();
    translate(0, y);
    for(let i=0; i < words2.length; i++){
      text(words2[i], width/2, 100 + 100*i);
    }
    pop();
  
    wordsHeight = 100 + 100*(words2.length-1);
    y = y-1;
    if(y < -wordsHeight){
      y = height;
      whichWords = 0
    }
  }
  /*
  let interval = map(mouseX, 0, width, 10, 1000);

  if(prevTime + interval < millis()){
    index++;
    if(index > words2.length){
      index = 0;
    }
    prevTime = millis();
  }
  */
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}