var num = 20;
var angle = 360;
var radius = 250;

var numMin = 1;
var numMax = 100;
var numStep = 1;

var angleMin = 15;
var angleMax = 360;
var angleStep = 1;

var radiusMin = 30;
var radiusMax = 300;
var radiusStep = 1;

var gui;

function setup() {
  createCanvas(1000, 800);

  gui = createGui('parameters').setPosition(10, 10);

  //sliderRange();
  gui.addGlobals('radius', 'num', 'angle');
}

function draw() {
  background(0);

  fill(255, 255, 0);
  stroke(255);

  for(let i=0; i<num; i++){
    
    let x = width/2 + cos(radians((angle/num)*i)) * radius;
    let y = height/2 + sin(radians((angle/num)*i)) * radius;

    ellipse(x, y, 50, 50);
  }

}