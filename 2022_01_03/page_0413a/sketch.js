//CREATION

let assets_folder = '../assets/playSource/';

let ghost;
let asterisk;

let slider_ghost_frame;
let slider_asterisk_frame;

function preload(){
  ghost = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
  asterisk = loadAnimation(assets_folder+'asterisk.png', assets_folder+'triangle.png', assets_folder+'square.png', assets_folder+'cloud.png', assets_folder+'star.png', assets_folder+'mess.png', assets_folder+'monster.png');
}

function setup() {
  //create a drawing canvas
  //createCanvas(windowWidth, windowHeight);
  createCanvas(800, 600);

  print(ghost.frameDelay);
  print(asterisk.frameDelay);
  //ghost.frameDelay = 30;

  slider_ghost_frame = createSlider(2, 30, 15);
  slider_ghost_frame.style('width', '300px');
  slider_ghost_frame.style('height', '10px');
  slider_ghost_frame.position(50, 50);

  slider_asterisk_frame = createSlider(2, 30, 15);
  slider_asterisk_frame.style('width', '300px');
  slider_asterisk_frame.style('height', '10px');
  slider_asterisk_frame.position(50, 100);
}


function draw() {
  background(0);

  let ghost_animation_speed = slider_ghost_frame.value();
  let asterisk_animation_speed = slider_asterisk_frame.value();

  //print(ghost_animation_speed);
  //print(asterisk_animation_speed);

  ghost.frameDelay = ghost_animation_speed;
  asterisk.frameDelay = asterisk_animation_speed;

  animation(ghost, 300, height/2);
  animation(asterisk, 500, height/2);
}

/*
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
*/