let i = 0;

function setup() {
  // put setup code here
  createCanvas(1200, 800);
  print("안녕하세요");

  textSize(42);
  textAlign(CENTER, CENTER);
  print("변수 let i 의 현재 값을 출력합니다:" + i)
  // setup 구문 안에서 변수 h는 없다!!!
  //print(h);

  let link1;
  link1 = createA('page_0323a/index.html', '다음 이야기는?', '_self');
  link1.position(100, 100);

  let link2;
  link2 = createA('https://www.yonsei.ac.kr', '연세대학교', '_blank');
  link2.position(100, 130);

  let link3;
  link3 = createA('https://p5js.org', '피파이브 닷 제이에스', '_blank');
  link3.position(100, 160);
}

function draw() {
  // put drawing code here
  background(255,255,0);
  fill(255);
  ellipse(width/2, height/2, 500, 100);

  if(mouseX > 440 && mouseY > 370 && mouseX < 477 && mouseY < 414){
    fill(255, 0, 255);
    rect(440, 370, 37, 44);
  }

  fill(0);
  text("반갑습니다. " + i, width*0.5, height*0.5);
  let h = hour();
  let m = minute();
  let s = second();
  fill(0);
  text(nf(h,2) + ":" + nf(m,2) + ":" + nf(s,2), width/2, height/2 + 50);
  //print("반갑습니다." + i);
  i = i + 1; // i++

  //console.log('x: ' + mouseX + ', y: ' + mouseY);
}


function mousePressed(){
  console.log('마우스를 클릭했습니다.');

  if(mouseX > 440 && mouseY > 370 && mouseX < 477 && mouseY < 414){
    window.open('page_0323b/index.html', '_self');
  }

}