//Collision detection and resolution
//move the mouse, the sprite responds to overlapings, collision,
//and displaces another sprite

let assets_folder = '../assets/playSource/';

let ani_box, ani_asterisk1, ani_asterisk2, ani_cloud, ani_circle;
let sprite_box, sprite_asterisk, sprite_cloud, sprite_circle;

let debugMode = false;

function preload(){
  ani_box = loadAnimation(assets_folder+'box0001.png', assets_folder+'box0003.png');
  ani_asterisk1 = loadAnimation(assets_folder+'asterisk_normal0001.png', assets_folder+'asterisk_normal0003.png');
  ani_asterisk2 = loadAnimation(assets_folder+'asterisk_circle0006.png', assets_folder+'asterisk_circle0008.png');
  ani_cloud = loadAnimation(assets_folder+'cloud_breathing0001.png', assets_folder+'cloud_breathing0009.png');
  ani_circle = loadAnimation(assets_folder+'plain_circle.png');
}

function setup() {
  createCanvas(1000, 800);

  sprite_box = createSprite(width/4, height/2);
  sprite_box.addAnimation('normal', ani_box);

  sprite_circle = createSprite(width/2, height/2, 100, 100);
  //sprite_circle.shapeColor = color(0,0,0,0);
  sprite_circle.addAnimation('normal', ani_circle);

  sprite_cloud = createSprite(3*(width/4), height/2);
  sprite_cloud.addAnimation('normal', ani_cloud);

  sprite_asterisk = createSprite(random(width), random(height));
  sprite_asterisk.addAnimation('normal', ani_asterisk1);
  sprite_asterisk.addAnimation('circle', ani_asterisk2);

  //noCursor();

  textAlign(CENTER, CENTER);
  textSize(16);
}

function draw() {
  background(255, 255, 255);

  sprite_asterisk.position.x = mouseX;
  sprite_asterisk.position.y = mouseY;

  //check and resolve the inteactions between sprites

  //collide also returns a true/false but it can simply be used to
  //resolve collisions.
  //If overlapping with box asterisk will be placed
  //in the closest non overlapping position
  sprite_asterisk.collide(sprite_box);



  //sprite.overlap() returns true if overlapping occours
  //note: by default the check is performed on the images bounding box
  //press mouse button to visualize them
  if(sprite_asterisk.overlap(sprite_circle) == true){
    sprite_asterisk.changeAnimation('circle');
  }else{
    sprite_asterisk.changeAnimation('normal');
  }

  

  //displace is the opposite of collide, the sprite in the parameter will
  //be pushed away but the sprite calling the function
  sprite_asterisk.displace(sprite_cloud);

  //if debug is set to true bounding boxes, centers and depths are visualized
  sprite_asterisk.debug = debugMode;
  sprite_box.debug = debugMode;
  sprite_circle.debug = debugMode;
  sprite_cloud.debug = debugMode;

  drawSprites();

  fill(0);
  noStroke();
  text("collide", width/4, height/2);
  text("overlap", width/2, height/2);
  text("displace", sprite_cloud.position.x, sprite_cloud.position.y);
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debugMode = !debugMode;
  }
}