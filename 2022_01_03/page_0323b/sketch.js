let i = 0;
let j = 3;

function setup() {
  createCanvas(640, 480);
  frameRate(15);
}

function draw() {
  background(random(50, 52));
  strokeWeight(10);

  //if(조건){
  //  i = i + 1;
  //}else{
  //  i = i - 1;    
  //}

  if (i <= 0){
    j = 3;
  }

  if (i >= 50){
    j = -3;
  }

  i = i + j;


  r = mouseX/width*256;
  g = mouseY/height*256;
  b = (mouseX/width)*(mouseY/height)*256;
  fill(r, g, b, 100);
  stroke(0);
  ellipse(0,0,10*i, 10*i);
  
  fill(256-r, 256-g, 256-b, 100);
  stroke(0);
  ellipse(width, height, 10*i, 10*i);
}