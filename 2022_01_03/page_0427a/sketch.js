let assets_folder = '../assets/playSource/';

let ani_floating;
let sprite_ghost;

function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
}

function setup(){
  createCanvas(800, 800);
  sprite_ghost = createSprite(width/2, height/2, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);
}

function draw(){
  background(255, 255, 0);

  //sprite_ghost.attractionPoint(magnitude, attractor_x, attractor_y);
  //The force(magnitude) is added to the current velocity.
  sprite_ghost.attractionPoint(0.1, mouseX, mouseY);
  sprite_ghost.maxSpeed = 5;

  drawSprites();
}
