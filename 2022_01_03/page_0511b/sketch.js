//Collision detection and resolution
//move the mouse, the sprite responds to overlapings, collision,
//and displaces another sprite

let assets_folder = '../assets/playSource/';

let ani_box, ani_circle;
let group_box, group_circle;

let debugMode = false;

function preload(){
  ani_box = loadAnimation(assets_folder+'box0001.png', assets_folder+'box0003.png');
  ani_circle = loadAnimation(assets_folder+'asterisk_circle0006.png', assets_folder+'asterisk_circle0008.png');
}

function setup() {
  createCanvas(1000, 800);

  group_box = new Group();
  group_circle = new Group();

  for(let i=0; i<20; i++){
    let sprite_box = createSprite(random(width), random(height));
    sprite_box.addAnimation('normal', ani_box);
    //sprite_box.immovable = true;

    let randValue = random(1);
    if(randValue < 0.25){
      sprite_box.rotation = 0;
    }else if(randValue >= 0.25 && randValue < 0.5){
      sprite_box.rotation = 90;
    }else if(randValue >= 0.5 && randValue < 0.75){
      sprite_box.rotation = 45;
    }else{
      sprite_box.rotation = -90;
    }
    
    sprite_box.scale = random(0.5, 1);

    group_box.add(sprite_box);
  }

  for(let i=0; i<50; i++){
    let sprite_circle = createSprite(random(width), random(height));
    sprite_circle.addAnimation('normal', ani_circle);
    //collider type Either "rectangle" or "circle"
    sprite_circle.setCollider('circle', -2, 2, 55);
    sprite_circle.scale = random(0.45, 1.0);
    sprite_circle.mass = sprite_circle.scale;
    //restitution is the dispersion of energy at each bounce
    //if = 1 the circles will bounce forever
    //if < 1 the circles will slow down
    //if > 1 the circles will accelerate until they glitch
    //sprite_circle.restitution = 0.9;
    sprite_circle.restitution = 0.95;
    sprite_circle.setSpeed(random(2, 3), random(0, 360));
    group_circle.add(sprite_circle);
  }


  //noCursor();

  textAlign(CENTER, CENTER);
  textSize(32);
}

function draw() {
  background(200, 200, 200);

  group_circle.bounce(group_box);

  for(let i=0; i<group_box.length; i++){
    let _box = group_box[i];
    if(_box.position.x < 0){
      _box.position.x = 1;
      _box.velocity.x = _box.velocity.x*-1;
    }
    if(_box.position.x > width){
      _box.position.x = width-1;
      _box.velocity.x = _box.velocity.x*-1;
    }
    if(_box.position.y < 0){
      _box.position.y = 1;
      _box.velocity.y = _box.velocity.y*-1;
    }
    if(_box.position.y > height){
      _box.position.y = height-1;
      _box.velocity.y = _box.velocity.y*-1;
    }
  }

  for(let i=0; i<group_circle.length; i++){
    let _circle = group_circle[i];
    for(let j=0; j<group_circle.length; j++){
      let _other = group_circle[j];
      if(i != j){
        _circle.bounce(_other);
      }
    }

    if(_circle.position.x < 0){
      _circle.position.x = 1;
      _circle.velocity.x = _circle.velocity.x*-1;
    }
    if(_circle.position.x > width){
      _circle.position.x = width-1;
      _circle.velocity.x = _circle.velocity.x*-1;
    }
    if(_circle.position.y < 0){
      _circle.position.y = 1;
      _circle.velocity.y = _circle.velocity.y*-1;
    }
    if(_circle.position.y > height){
      _circle.position.y = height-1;
      _circle.velocity.y = _circle.velocity.y*-1;
    }
  }

  drawSprites();
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debugMode = !debugMode;
  }
}