/*
.play()
.isPlaying()
.pause()
.setVolume()
let amp = new p5.Amplitude();
let level = amp.getLevel();
let mic = new p5.AudioIn();
mic.start();
let level = mic.getLevel();
*/

let assets_folder = '../assets/sound/';

let audioFiles = [];
let rythm01;

let x, speedX, dirX;

function preload(){
  for(let i=0; i<6; i++){
    audioFiles[i] = loadSound(assets_folder + 'five_dollars_' + nf(i+1, 2) + '.mp3');
  }
  rythm01 = loadSound(assets_folder + 'rythm-01.wav');
}


function setup() {
  createCanvas (800,800);
  background(0);
  //rythm01.loop();

  x = width/2;
  speedX = 5;
  dirX = 1;
}

function draw() {
  background(0);
  fill(255);
  ellipse(x, height/2, 50, 50);

  x += dirX*speedX;
  if(x > width-25){
    x = width-25;
    dirX *= -1;
    let index = int(random(6));
    if(index == 6){
      audioFiles[val-1].play(0, 1, 1, 0.1, 1);
    }else{
      audioFiles[index].play();
    }
  }
  if(x < 25){
    x = 25;
    dirX *= -1;
    audioFiles[3].play();
  }
  speedX = map(mouseX, 0, width, 1, 100);

}

function keyPressed(){
  val = int(key);
  if(val >= 1 && val <= 6){
    if(val == 6){
      audioFiles[val-1].play(0, 1, 1, 0.1, 1);
    }else{
      audioFiles[val-1].play();
    }
  }
}