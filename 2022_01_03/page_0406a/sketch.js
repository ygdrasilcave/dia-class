let x = 15;
let y = 27;
let words = [];

let myWord = "죽는 날까지 하늘을 우러러 한 점 부끄럼이 없기를, 잎새에 이는 바람에도, 나는 괴로워했다. 별을 노래하는 마음으로 모든 죽어가는 것을 사랑해야지 그리고 나한테 주어진 길을 걸어가야겠다. 오늘 밤에도 별이 바람에 스치운다."

let words2 = [];
let index = 0;
let prevTime = 0;

function setup() {
  //create a drawing canvas
  createCanvas(windowWidth, windowHeight);
  y = 16;
  print((x + y)/2);

  words[0] = "what";
  words[1] = "are";
  words[2] = "arrays";
  print(words[0] + " " + words[1] + " " + words[2])
  print(words.length);

  print(myWord);
  let _temp = myWord.split(' ');
  print(_temp);
  print(_temp.length);

  textSize(220);
  let wordMaxWidth = 0;
  for(let i=0; i < _temp.length; i=i+1){
      words2[i] = _temp[i];
      if(textWidth(_temp[i]) > wordMaxWidth){
        wordMaxWidth = textWidth(_temp[i]);
      }
  }
  print(width);
  print(wordMaxWidth);
  textAlign(CENTER, CENTER);

  prevTime = millis();
  print('prveTime: ' + str(prevTime));
}


function draw() {
  background(0);  //check this syntax with alpha value

  noStroke();
  fill(255);
  push();
  translate(width/2, height/2);
  rotate(HALF_PI/2.0);
  text(words2[index], 0, 0);
  pop();

  let interval = map(mouseX, 0, width, 10, 1000);

  if(prevTime + interval < millis()){
    index++;
    if(index > words2.length){
      index = 0;
    }
    prevTime = millis();
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}