//CONTROL
/*
.frameDelay
.looping
.playing

.getLastFrame()
.getFrame() //0 ~ lastFrame
.play()
.stop()
.rewind() //changeFrame(0)
.nextFrame()
.previousFrame()
.chageFrame()
.goToFrame()
*/

let assets_folder = '../assets/playSource/';

let explode;

let x, y;
let dirX;
let dirY;
let velocityX;
let velocityY;
let tx;
let ty;

function preload(){
  explode = loadAnimation(assets_folder+'asterisk_explode0001.png', assets_folder+'asterisk_explode0011.png');
}

function setup() {
  //create a drawing canvas
  //createCanvas(windowWidth, windowHeight);
  createCanvas(800, 600);

  x = width/2;
  y = height/2;
  dirX = 1;
  dirY = 1; 
  velocityX = random(1, 2);
  velocityY = random(1, 3);
  tx = 0.0;
  ty = 0.0;
}


function draw() {
  background(255, 255, 0);

  x = x + dirX*(velocityX + map(noise(tx), 0, 1, -3, 3));
  y = y + dirY*(velocityY + map(noise(ty), 0, 1, -3, 3));

  if(x > width-30){
    x = width-30;
    dirX = -1;
    changeParameter();
  }
  if(y > height-30){
    y = height-30;
    dirY = -1;
    changeParameter();
  }
  if(x < 30){
    x = 30;
    dirX = 1;
    changeParameter();
  }
  if(y < 30){
    y = 30;
    dirY = 1;
    changeParameter();
  }

  if(explode.getFrame() == explode.getLastFrame()){
    explode.changeFrame(7);
  }

  animation(explode, x, y);

  tx += 0.03;
  ty += 0.025;
}

function changeParameter(){
  explode.rewind();
  velocityX = random(1, 2);
  velocityY = random(1, 3);
}



/*
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
*/