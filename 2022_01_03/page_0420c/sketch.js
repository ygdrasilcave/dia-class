//CONTROL
/*
.frameDelay
.looping
.playing

.getLastFrame()
.getFrame() //0 ~ lastFrame
.play()
.stop()
.rewind() //changeFrame(0)
.nextFrame()
.previousFrame()
.chageFrame()
.goToFrame()
*/

/*
sprite method
.addAnimation
(.offX) //animation method to adjust the position of the each ani image
(.offY)
.changeAnimation
.mirrorX
.mirrorY
.velocity.x
.velocity.y
.position.x
.position.y
.rotation
.scale
*/

let assets_folder = '../assets/playSource/';

let ani_floating;
let ani_moving;
let ani_spinning;
let sprite_ghost;

let t = 0.0;

function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
  ani_moving = loadAnimation(assets_folder+'ghost_walk0001.png', assets_folder+'ghost_walk0004.png');
  ani_spinning = loadAnimation(assets_folder+'ghost_spin0001.png', assets_folder+'ghost_spin0003.png');
  ani_floating.offY = 18;
}

function setup() {
  //create a drawing canvas
  //createCanvas(windowWidth, windowHeight);
  createCanvas(800, 600);
  sprite_ghost = createSprite(width/2, height/2, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);

  sprite_ghost.addAnimation('moving', ani_moving);
  sprite_ghost.addAnimation('spinning', ani_spinning);

  sprite_ghost.changeAnimation('moving');
}


function draw() {
  background(255, 255, 0);

  if(mouseX > sprite_ghost.position.x+10){
    sprite_ghost.changeAnimation('moving');
    sprite_ghost.mirrorX(1);
    sprite_ghost.velocity.x = 2;
  }else if(mouseX < sprite_ghost.position.x-10){
    sprite_ghost.changeAnimation('moving');
    sprite_ghost.mirrorX(-1);
    sprite_ghost.velocity.x = -2;
  }else{
    sprite_ghost.changeAnimation('floating');
    sprite_ghost.mirrorX(1);
    sprite_ghost.velocity.x = 0;
  }

  if(mouseIsPressed == true){
    sprite_ghost.changeAnimation('spinning');
    //sprite_ghost.rotation = sprite_ghost.rotation - 5;
    sprite_ghost.rotation = sin(t)*45;
    t += 0.25;
  }else{
    sprite_ghost.rotation = 0;
  }

  sprite_ghost.scale = map(mouseY, 0, height, 0.1, 1.5);
  //sprite_ghost.scale = 5;

  drawSprites();
}


/*
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
*/