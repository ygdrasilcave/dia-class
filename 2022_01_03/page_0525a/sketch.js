let assets_folder = '../assets/sound/';
let audioFiles = [];

let x, y;
let speedX, dirX, speedY, dirY;

function preload(){
  for(let i=0; i<6; i++){
    audioFiles[i] = loadSound(assets_folder + 'five_dollars_' + nf(i+1, 2) + '.mp3');
  }
}

function setup() {
  createCanvas(800, 800);
  x = width/2;
  y = 15;
  speedX = 5;
  speedY = 5;
  dirX = 1;
  dirY = 1;
}

function draw() {
  background(0);
  fill(255);
  ellipse(x, y, 30, 30);

  speedX = map(mouseX, 0, width, 1, 50);
  speedY = map(mouseY, 0, height, 1, 50);

  x += dirX*speedX;
  y += dirY*speedY;
  if(x < 15){
    x = 15;
    dirX *= -1;
    audioFiles[4].play();
  }
  if(x > width-15){
    x = width-15;
    dirX *= -1;
    audioFiles[3].play();
  }
  if(y < 15){
    y = 15;
    dirY *= -1;
    audioFiles[0].play();
  }
  if(y > height-15){
    y = height-15;
    dirY *= -1;
    audioFiles[1].play();
  }
}

function keyPressed(){
  print(key);
  let val = int(key);
  print(val);
  if(val >= 1 && val <= 6){
    audioFiles[val-1].play();
  }
}