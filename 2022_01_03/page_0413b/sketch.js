//CONTROL
/*
.frameDelay
.looping
.playing

.getLastFrame()
.getFrame() //0 ~ lastFrame
.play()
.stop()
.rewind() //changeFrame(0)
.nextFrame()
.previousFrame()
.chageFrame()
.goToFrame()
*/

let assets_folder = '../assets/playSource/';

let sleep, explode, glitch, circle;

function preload(){
  sleep = loadAnimation(assets_folder+'asterisk_stretching0001.png', assets_folder+'asterisk_stretching0008.png');

  explode = loadAnimation(assets_folder+'asterisk_explode0001.png', assets_folder+'asterisk_explode0011.png');

  glitch = loadAnimation(assets_folder+'asterisk.png', assets_folder+'triangle.png', assets_folder+'square.png', assets_folder+'cloud.png', assets_folder+'star.png', assets_folder+'mess.png', assets_folder+'monster.png');
  glitch.playing = false;

  circle = loadAnimation(assets_folder+'asterisk_circle0000.png', assets_folder+'asterisk_circle0008.png');
  circle.looping = false;
}

function setup() {
  //create a drawing canvas
  //createCanvas(windowWidth, windowHeight);
  createCanvas(800, 600);
}


function draw() {
  background(255, 255, 0);

  if(mouseIsPressed == true){
    sleep.play();
    circle.goToFrame(0);
  }else{
    sleep.stop();
    circle.goToFrame(circle.getLastFrame());
  }

  if(explode.getFrame() == explode.getLastFrame()){
    explode.changeFrame(7);
  }

  animation(sleep, 100, height/2);
  animation(explode, 300, height/2);
  animation(glitch, 500, height/2);
  animation(circle, 700, height/2);
}

function mouseReleased(){
  //explode.rewind();
  explode.changeFrame(0);

  //glitch.nextFrame();
  glitch.previousFrame();
}

/*
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
*/