let assets_folder = '../assets/playSource/';

let ani_floating;
let sprite_ghost;
let noiseAngleFactor = 0.0;
let noiseSpeedFactor = 0.0;


function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
}

function setup(){
  createCanvas(800, 800);
  sprite_ghost = createSprite(width/2, height/2, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);
}

function draw(){
  background(255, 255, 0);

  //sprite_ghost.velocity.x = 2;
  //sprite_ghost.velocity.y = 1;
  //sprite_ghost.setSpeed(speed, angle);
  sprite_ghost.setSpeed(1+(noise(noiseSpeedFactor)*2), noise(noiseAngleFactor)*360);
  noiseAngleFactor += 0.025;
  noiseSpeedFactor += 0.12;

  if(sprite_ghost.position.x > width+50){
    sprite_ghost.position.x = -50;
  }
  if(sprite_ghost.position.x < -50){
    sprite_ghost.position.x = width+50;
  }
  if(sprite_ghost.position.y > height+50){
    sprite_ghost.position.y = -50;
  }
  if(sprite_ghost.position.y < -50){
    sprite_ghost.position.y = height+50;
  }
  

  drawSprites();
}
