let assets_folder = '../assets/playSource/';

let ani_floating;
let sprite_ghost;

let targetX = 0;
let targetY = 0;
let softToggle = true;

function preload(){
  ani_floating= loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
}

function setup(){
  createCanvas(800, 800);
  sprite_ghost = createSprite(0, 0, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);
  targetX = random(width);
  targetY = random(height);
}

function draw(){
  background(255, 255, 0);

  //sprite_ghost.velocity.x = 2;
  //sprite_ghost.velocity.y = 1;
  //sprite_ghost.setSpeed(speed, angle);

  let posX = sprite_ghost.position.x;
  let posY = sprite_ghost.position.y;
  let dist = sqrt((targetX-posX)*(targetX-posX) + (targetY-posY)*(targetY-posY));
  let angle = degrees(atan2(targetY-posY, targetX-posX));
  if(dist < 5){
    sprite_ghost.setSpeed(0, angle);
  }else{
    sprite_ghost.setSpeed(3, angle);
  }

  if(second()%2 == 0 && softToggle == true){
    targetX = random(width);
    targetY = random(height);
    softToggle = false;
  }else if(second()%2 == 1){
    softToggle = true;
  }

  noStroke();
  fill(255, 0, 0);
  ellipse(targetX, targetY, 10, 10);
  
  drawSprites();
}
