//CONTROL
/*
.frameDelay
.looping
.playing

.getLastFrame()
.getFrame() //0 ~ lastFrame
.play()
.stop()
.rewind() //changeFrame(0)
.nextFrame()
.previousFrame()
.chageFrame()
.goToFrame()
*/

let assets_folder = '../assets/playSource/';
let assets_sound = '../assets/sound/';

let explode;

let x, y;
let dirX;
let dirY;
let velocityX;
let velocityY;
let tx;
let ty;

let snd_bang1;
let snd_bang2;
let snd_amb;

function preload(){
  explode = loadAnimation(assets_folder+'asterisk_explode0001.png', assets_folder+'asterisk_explode0011.png');
  snd_bang1 = loadSound(assets_sound + 'bang.mp3');
  snd_bang2 = loadSound(assets_sound + 'bang2.mp3');
  snd_amb = loadSound(assets_sound + 'normal.mp3');
}

function setup() {
  //create a drawing canvas
  //createCanvas(windowWidth, windowHeight);
  createCanvas(800, 600);

  x = width/2;
  y = height/2;
  dirX = 1;
  dirY = 1; 
  velocityX = random(1, 2);
  velocityY = random(1, 3);
  tx = 0.0;
  ty = 0.0;

  snd_amb.loop();
  snd_amb.setVolume(0.05);

  snd_bang1.setVolume(0.1);
  snd_bang2.setVolume(0.15);
}


function draw() {
  background(255, 255, 0);

  x = x + dirX*(velocityX + map(noise(tx), 0, 1, -3, 3));
  y = y + dirY*(velocityY + map(noise(ty), 0, 1, -3, 3));

  if(x > width-30){
    x = width-30;
    dirX = -1;
    changeParameter();
  }
  if(y > height-30){
    y = height-30;
    dirY = -1;
    changeParameter();
  }
  if(x < 30){
    x = 30;
    dirX = 1;
    changeParameter();
  }
  if(y < 30){
    y = 30;
    dirY = 1;
    changeParameter();
  }

  if(explode.getFrame() == explode.getLastFrame()){
    explode.changeFrame(7);
  }

  animation(explode, x, y);

  tx += 0.03;
  ty += 0.025;
}

function changeParameter(){
  explode.rewind();
  velocityX = random(1, 2);
  velocityY = random(1, 3);
  let randValue = random(1);
  if(randValue > 0.5){
    snd_bang1.play();
  }else{
    snd_bang2.play();
  }
}



/*
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
*/