let assets_folder = '../assets/playSource/';

let ani_triangle, ani_cloud1, ani_cloud2, ani_platform;
let sprite_triangle, sprite_cloud, sprite_platform;

let debugMode = false;

function preload(){
  ani_triangle = loadAnimation(assets_folder+'triangle.png');
  ani_cloud1 = loadAnimation(assets_folder+'cloud_breathing0001.png', assets_folder+'cloud_breathing0009.png');
  ani_cloud2 = loadAnimation(assets_folder+'asterisk_normal0001.png', assets_folder+'asterisk_normal0003.png');
  ani_platform = loadAnimation(assets_folder+'platform.png');
}


function setup() {
  createCanvas(1000, 800);

  sprite_triangle = createSprite(200,100);
  sprite_triangle.addAnimation('normal', ani_triangle);

  sprite_cloud = createSprite(700, 450);
  sprite_cloud.addAnimation('normal', ani_cloud1);
  sprite_cloud.addAnimation('transformed', ani_cloud2);
  sprite_cloud.setCollider('circle', 0, 0, 50);

  sprite_platform = createSprite(width/2, height-100);
  sprite_platform.addAnimation('normal', ani_platform);

  sprite_triangle.depth = 10;
}

function draw() {
  background(200, 200, 200);

  sprite_triangle.velocity.x = 0;

  //UP_ARROW, DOWN_ARROW
  if(keyIsDown(LEFT_ARROW)==true){
    sprite_triangle.velocity.x = -5;
  }
  if(keyIsDown(RIGHT_ARROW)==true){
    sprite_triangle.velocity.x = 5;
  }

  if(keyIsDown(UP_ARROW)==true){
    sprite_triangle.velocity.y -= 0.9;
    //print(sprite_triangle.velocity.y);
    if(sprite_triangle.velocity.y < -6.0){
      sprite_triangle.velocity.y = -6.0;
    }
  }

  if(sprite_platform.overlapPixel(sprite_triangle.position.x, sprite_triangle.position.y+30) == false){
    sprite_triangle.velocity.y += 0.5;
    if(sprite_triangle.velocity.y > 10.0){
      sprite_triangle.velocity.y = 10.0;
    }
  }

  while (sprite_platform.overlapPixel(sprite_triangle.position.x, sprite_triangle.position.y+30) == true) {
    sprite_triangle.position.y += -1;
    sprite_triangle.velocity.y = 0;
  }

  if(sprite_cloud.overlapPoint(sprite_triangle.position.x, sprite_triangle.position.y) == true){
    sprite_cloud.changeAnimation('transformed');
  }else{
    sprite_cloud.changeAnimation('normal');
  }

  boundDebug(debugMode);
  drawSprites();
}

function boundDebug(_b){
  sprite_triangle.debug = _b;
  sprite_cloud.debug = _b;
  sprite_platform.debug = _b;
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debugMode = !debugMode;
  }
}