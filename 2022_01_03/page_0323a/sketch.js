let x = 600;
let y = 600;
let value1 = 51;
let value2 = 255;

let count = 0;

let current_time = 0;
let prev_time = 0;

let eye_animation_mode = true;

function setup() {
  // put setup code here
  createCanvas(x, y);
  background(0,0,0);

  textSize(60);
  textAlign(CENTER, CENTER);


  let _x = 18;
  if (_x > 10 && _x <= 17 ) {
  //if (_x > 10 || _x <= 17 ) {
    console.log("one");
  } else if (_x == 21) {
    console.log("two");
  } else if (_x === 18) {
    console.log("three");
  } else {
    console.log("four");
  }
}

function draw() {
  background(51);
  
  noStroke();
  fill(255,255,255);
  ellipse(x/2, y/2+35, 300, 300);
  ellipse(x/2, y/2-35, 300, 300);
  ellipse(x/2-150, y/2, 170, 170);
  ellipse(x/2+150, y/2, 170, 170);
  rect(x/2-150, y/2-35, 300, 70);


  strokeWeight(15);
  stroke(51);
  line(x/2+150, y/2, x/2+170, y/2);
  line(x/2-150, y/2, x/2-170, y/2);
  line(x/2+67,y/2-60, x/2+73, y/2-60);
  line(x/2-67,y/2-60, x/2-73, y/2-60);  
  line(x/2, y/2-20, x/2, y/2+20);
  line(x/2-15, y/2+65, x/2+15, y/2+70);
 
  strokeWeight(10);
  stroke(51);
  ellipse(x/2+70, y/2+5, 90, 90);
  ellipse(x/2-70, y/2+5, 90, 90);  

  //strokeWeight(25);
  //stroke(value1);
  //line(x/2+70, y/2+25, x/2+70, y/2-15);
  //line(x/2+90, y/2+5, x/2+50, y/2+5);
  //line(x/2-90, y/2+5, x/2-50, y/2+5);
  //line(x/2-70, y/2-15, x/2-70, y/2+25);
  fill(0);
  if(count == 0){
    text('+', x/2+70, y/2+15);
  }else if(count == 1){
    text('0', x/2+70, y/2+15);
  }else if(count == 2){
    text('ㅠ', x/2+70, y/2+15);
  }else{
    text('X', x/2+70, y/2+15);
  }

  strokeWeight(30);
  stroke(value1);
  line(x/2, y/4-10, x/2, y/4+10);
  line(x/2, y/4-10, x/2-25, y/4);
  line(x/2, y/4-10, x/2+25, y/4);

  strokeWeight(15);
  stroke(value2);
  point(400, 500);
  point(500, 200);
  point(100, 150);
  point(570, 380);
  point(270, 40);
  point(50, 390);
  strokeWeight(10);
  line(70, 90, 80, 100);
  line(70, 100, 80, 90);
  line(320, 550, 330, 560);
  line(320, 560, 330, 550);
  line(470, 150, 480, 160);
  line(470, 160, 480, 150);
  line(530, 50, 540, 60);
  line(540, 50, 530, 60);
  line(30, 490, 40, 480);
  line(30, 480, 40, 490);
  line(500, 480, 510, 490);
  line(500, 490, 510, 480);
  strokeWeight(8);
  point(400, 70);
  point(560, 570);
  point(200, 550);
  point(120, 450);
  point(170, 30);
  point(30, 240);
  point(460, 450);
  point(40, 70);
  point(90, 580);
  point(580, 260);
  point(440, 530);

  current_time = millis();

  if(current_time - prev_time > 100){
    if(eye_animation_mode == true){
      count = count + 1;
      if(count > 3){
        count = 0;
      }
    }
    prev_time = current_time;
  }

  if(mouseX > (x/2-70)-45 && mouseY > (y/2+5)-45 && mouseX < (x/2-70)+45 && mouseY < (y/2+5)+45){
    cursor('grab');
  }else if(mouseX > (x/2+70)-45 && mouseY > (y/2+5)-45 && mouseX < (x/2+70)+45 && mouseY < (y/2+5)+45){
    cursor('grab');
  }
  else{
    cursor(ARROW);
  }
  //count = second()%4;
  //console.log(count);
}


function mouseClicked() {
  if (value1 === 'rgb(255, 167, 167)') {
    value1 = 51;
  } else {
    value1 = 'rgb(255, 167, 167)';
  }

  if (value2 === 'rgb(92, 209, 229)') {
    value2 = 255;
  } else {
    value2 = 'rgb(92, 209, 229)';
  }

  if(eye_animation_mode == false){
    count = count + 1;
    if(count > 3){
      count = 0;
    }
  }

  //x/2-70, y/2+5, 90, 90
  //x/2+70, y/2+5, 90, 90
  if(mouseX > (x/2-70)-45 && mouseY > (y/2+5)-45 && mouseX < (x/2-70)+45 && mouseY < (y/2+5)+45){
    window.open('../index.html', '_self');
  }else if(mouseX > (x/2+70)-45 && mouseY > (y/2+5)-45 && mouseX < (x/2+70)+45 && mouseY < (y/2+5)+45){
    window.open('../page_0323b/index.html', '_self');
  }
}

function keyPressed(){
  if(key == 'a'){
    eye_animation_mode = true;
  }
  if(key == 'm'){
    eye_animation_mode = false;
  }

}

