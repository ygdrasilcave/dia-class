float t = 0;

void setup(){
  size(400, 200);
}

void draw(){
  background(255);
  
  noFill();
  stroke(0);
  
  float dia1 = random(50, 150);
  ellipse(100, height/2, dia1, dia1);
  
  float dia2 = noise(t)*150;
  ellipse(300, height/2, dia2, dia2);
  t += 0.065;
}
