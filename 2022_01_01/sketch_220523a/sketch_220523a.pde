float r;
float t;

void setup(){
  size(400, 400);
  background(255);
  r = 150;
  t = 0.0;
}

void draw(){
  background(255);
  
  float x = cos(t)*r + width/2;
  float y = sin(t)*r + height/2;
  
  fill(0);
  rect(x-10, height/2-10, 20, 20);
  rect(width/2-10, y-10, 20, 20);
  ellipse(x, y, 20, 20);
  
  t += 0.01;
}
