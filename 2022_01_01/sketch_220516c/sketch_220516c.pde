float r = 0;
float t = 0;
float theta = 45;
float x;
float y;

void setup() {
  size(400, 400);
  background(255);
  x = width/2;
  y = height/2;
}

void draw() {
  //background(255);
  fill(0);
  rect(x-2.5, y-2.5, 5, 5);
  
  //line(width/2, height/2, x, y);
  
  //r = noise(t)*150;
  //t += 0.035;
  
  r = map(sin(t), -1, 1, 50, 150);
  t += 0.05;

  theta = theta + 5;
  x = width/2 + cos(radians(theta))*r;
  y = height/2 + sin(radians(theta))*r;
}
