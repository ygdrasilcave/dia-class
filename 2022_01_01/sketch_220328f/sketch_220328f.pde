int dice = 0;

void setup(){
  size(800, 800);
  background(255);
  textSize(60);
  textAlign(CENTER, CENTER);
}

void draw(){
  background(255);
  fill(0);
  if(dice == 1){
    text("one", width/2, height/2);
    //println("one");
  }else if(dice == 2){
    text("two", width/2, height/2);
    //println("two");
  }else if(dice == 3){
    text("three", width/2, height/2);
    //println("three");
  }else if(dice == 4){
    text("four", width/2, height/2);
    //println("four");
  }else if(dice == 5){
    text("five", width/2, height/2);
    //println("five");
  }else{
    text("six", width/2, height/2);
    //println("six");
  }
}

void keyPressed(){
  println(int(key));
  if(key == '1' || key == '2' || key == '3' || key == '4' || key == '5' || key == '6'){
    dice = int(key) - int('0');
    //dice = int(key) - 48;
    print(dice);
  }
}

void mousePressed(){
  dice = int(random(1, 7));
  println(dice);
}
