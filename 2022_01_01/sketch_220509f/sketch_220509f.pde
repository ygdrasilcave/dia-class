void setup(){
  size(400, 400);
}

void draw(){
  background(0);
  drawRect(width/2, height/2, 100);

  println(myCal(10, 45) + 20);
}

void drawRect(float _x, float _y, float _dia){
  fill(255, 0, 0);
  ellipse(_x, _y, _dia, _dia);
}

float myCal(float _a, float _b){
  float _result = _a + _b;
  return _result;
}
