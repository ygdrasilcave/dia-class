Zoog[] z = new Zoog[10];
float[] speed = new float[10];

void setup() {
  size(800, 600);
  for (int i=0; i<z.length; i++) {
    z[i] = new Zoog(random(width), random(height), random(25, 75), random(50, 100), 10);
    speed[i] = random(2, 5);
    if(speed[i] < 3.5){
      speed[i] = 0;
    }
  }
}

void draw() {
  background(255);
  for (int i=0; i<z.length; i++) {
    z[i].jiggle(speed[i]);
    z[i].display();
  }
}
