PImage[] img;
int prev_time = 0;

void setup() {
  size(400, 400);
  img = new PImage[12];
  for (int i=0; i<12; i++) {
    img[i] = loadImage("PT_anim00" + nf(i, 2) + ".gif");
  }
  prev_time = millis();
}

int count = 0;

void draw() {
  background(255);

  image(img[count%12], width/2-img[count%12].width/2, height/2-img[count%12].height/2);

  if (millis() > prev_time + (1000/24)) {
    count++;
    prev_time = millis();
  }
}
