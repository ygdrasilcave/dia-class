float t = 0.0;
int ellipseSpace = 5;

void setup() {
  size(800, 400);
}

void draw() {
  background(255);
  fill(0);
  noStroke();

  for (int i = 0; i<(width/ellipseSpace); i++) {
    float y = (sin(t + (i*0.15)) + 1) * height/2;
    ellipse(i*ellipseSpace, y, 20, 20);
  }
  t += 0.025;

  /*
  float y1 = (sin(t) + 1) * (height/2);
   float y2 = (sin(t + 0.1) + 1) * (height/2);
   float y3 = (sin(t + 0.2) + 1) * (height/2);
   
   
   ellipse(width/2, y1, 20, 20);
   ellipse(width/2+20, y2, 20, 20);
   ellipse(width/2+40, y3, 20, 20);
   
   t += 0.05;
   */
}
