// Example 4-1: Variable declaration and initialization examples
int count = 0;          // Declare an int named count, assigned the value 0
char letter = 'f';      // Declare a char named letter, assigned the value 'a'
double d = 132.32;      // Declare a double named d, assigned the value 132.32
boolean happy_cat = false;  // Declare a boolean named happy, assigned the value false
float x = 4.0;          // Declare a float named x, assigned the value 4.0
float y = 0.0;                // Declare a float named y (no assignment)
println(y);
y = x + 5.2;            // Assign the value of x plus 5.2 to the previously declared y
println(y);
float z = x*y + 15.0;   // Declare a variable named z, assign it the value which 
               // is x times y plus 15.0.
               
boolean happy_dog = true;
               
