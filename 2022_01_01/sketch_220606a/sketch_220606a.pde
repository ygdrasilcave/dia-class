/**
 * oscP5parsing by andreas schlegel
 * example shows how to parse incoming osc messages "by hand".
 * it is recommended to take a look at oscP5plug for an
 * alternative and more convenient way to parse messages.
 * oscP5 website at http://www.sojamo.de/oscP5
 */

import oscP5.*;
import netP5.*;

OscP5 oscP5;
//NetAddress myRemoteLocation;

float value1 = 0;
float value2 = 0;
float value3 = 0;

float posX = 0;
float posY = 0;

float posX2 = 0;
float posY2 = 0;

void setup() {
  size(800, 800);
  frameRate(25);
  oscP5 = new OscP5(this, 57120);
  //myRemoteLocation = new NetAddress("127.0.0.1",12000);
}

void draw() {
  background(255);
  noStroke();
  fill(value1, value2, value3);
  rect(0,0,width,height);
  
  fill(map(posX2, -5, 5, 0, 255));
  ellipse(map(posX, 0, 14, 0, width), map(posY, 0, 4, 0, height), 50, 50);
  posX2 = map(posX2, -5, 5, 0, width);
  posY2 = map(posY2, -5, 5, 0, height);
  ellipse(posX2, posY2, 50, 50);
}

/*
void mousePressed() {
  OscMessage myMessage = new OscMessage("/test");
  myMessage.add(123);
  myMessage.add(12.34);
  myMessage.add("some text");
  oscP5.send(myMessage, myRemoteLocation); 
}
*/


void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/trigger")==true) {
    if(theOscMessage.checkTypetag("fff")) {
      value1 = theOscMessage.get(0).floatValue();  
      value2 = theOscMessage.get(1).floatValue();  
      value3 = theOscMessage.get(2).floatValue();  
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      //print("### received an osc message /trigger with typetag fff.");
      println("Values: " + value1 + ", " + value2 + ", " + value3);
      return;
    }  
  } 
  
  if(theOscMessage.checkAddrPattern("/cursor")==true) {
    if(theOscMessage.checkTypetag("ff")) {
      posX = theOscMessage.get(0).floatValue();  
      posY = theOscMessage.get(1).floatValue();  
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      //print("### received an osc message /trigger with typetag fff.");
      //println("Position: " + posX + ", " + posY);
      return;
    }  
  } 
  
  if(theOscMessage.checkAddrPattern("/cursor2")==true) {
    if(theOscMessage.checkTypetag("ff")) {
      posX2 = theOscMessage.get(0).floatValue();  
      posY2 = theOscMessage.get(1).floatValue();
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      //print("### received an osc message /trigger with typetag fff.");
      println("Position: " + posX2 + ", " + posY2);
      return;
    }  
  } 
  //println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
}
