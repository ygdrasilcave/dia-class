//https://processing.org/tutorials/pixels
//https://processing.org/examples  => Image

PImage img;
PImage img_w;

float theta = 0.0;

void setup() {
  size(1024, 575);
  img = loadImage("face.jpg");
  background(255);
}

void draw() {
  //background(0);
  noStroke();
  int _x = int(random(width));
  int _y = int(random(height));
  
  int index = _x + _y*img.width; 
  
  color c = img.pixels[index];
  float br = brightness(c);
  float ellipseSize = map(br, 0, 255, 1, 30);
  
  fill(c);
  ellipse(_x, _y, ellipseSize, ellipseSize);
}
