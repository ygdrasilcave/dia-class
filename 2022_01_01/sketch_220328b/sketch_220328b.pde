// Example 4-2: Using variables
// Declare and initialize two integer variables at the top of the code.

int circleX = 0;
int circleY = 100;

void setup() {
  size(480, 270);
}

void draw() {
  //int circleX = 0;
  //int circleY = 100;
  background(255);
  stroke(0);
  fill(175);
  // Use the variables to specify the location of an ellipse.
  ellipse(circleX, circleY, 50, 50);

  circleX = circleX + 1;
}

void mousePressed() {
  //println(circleX);
}
