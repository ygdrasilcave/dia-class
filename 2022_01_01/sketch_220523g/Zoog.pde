class Zoog {

  float x, y, w, h, eyeSize;

  float t = 0.0;
  float dirT = 1.0;
  
  float t2 = 0.0;
  float t2_speed = 0.0;

  Zoog(float tempX, float tempY, float tempW, float tempH, float tempEyeSize) {
    x = tempX;
    y = tempY;
    w = tempW;
    h = tempH;
    eyeSize = tempEyeSize;
  }

  void update(float _x, float _y) {
    x = _x;
    y = _y;

    /*t = t + dirT*0.08;
     if(t > PI/2){
     dirT *= -1.0;
     }
     if(t < 0.0){
     dirT *= -1.0;
     }*/

    t = map(_y, 0, height, PI/2+1.25, -1.25);
    
    t2 = cos(t2_speed)*0.5;
    t2_speed += 0.45;
  }

  void display() {
    ellipseMode(CENTER);
    rectMode(CENTER);
    for (float i = y - h/3; i < y + h/2; i += 10) {
      stroke(0);
      line(x-w/4, i, x + w/4, i);
    }
    // Draw Zoog's body
    stroke(0);
    fill(175);
    rect(x, y, w/6, h);
    // Draw Zoog's head
    stroke(0);
    fill(255);
    ellipse(x, y-h, w, h);
    // Draw Zoog's eyes
    fill(0);
    ellipse(x-w/3, y-h, eyeSize, eyeSize*2);
    ellipse(x + w/3, y - h, eyeSize, eyeSize*2);
    // Draw Zoog's legs
    stroke(0);

    pushMatrix();
    translate(x - w/12, y + h/2);
    rotate(-t);
    line(0, 0, -w/2, 0);
    pushMatrix();
    translate(-w/2, 0);
    rotate(PI/4 - t2);
    line(0, 0, -w/4, 0);
    popMatrix();
    popMatrix();

    pushMatrix();
    translate(x + w/12, y + h/2);
    rotate(t);
    line(0, 0, w/2, 0);
    pushMatrix();
    translate(w/2, 0);
    rotate(-PI/4 + t2);
    line(0, 0, w/4, 0);
    popMatrix();
    popMatrix();
  }
}
