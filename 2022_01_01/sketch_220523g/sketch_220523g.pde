Zoog zoog;
float t = 0.0;

void setup(){
  size(400, 400);
  zoog = new Zoog(width/2, height/2, 60, 60, 16);
}

void draw(){
  background(255);
  
  float _y = map(sin(t), -1, 1, 100, height-100);
  zoog.update(mouseX, _y);
  zoog.display();
  
  t += 0.05;
}
