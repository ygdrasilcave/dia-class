//https://processing.org/tutorials/pixels
//https://processing.org/examples  => Image

PImage img;
int num = 3;
int prevTime = 0;

void setup() {
  size(1920, 1920);
  img = loadImage("tree.png");
  prevTime = second();
}

void draw() {
  background(0);
  for (int i=0; i<num; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians((360/num)*i));
    image(img, -(img.width*0.4)*0.5, -(img.height*0.4), img.width*0.4, img.height*0.4);
    popMatrix();
  }
  
  if(second() != prevTime){
    //num++;
    //if(num > 32){
    //  num = 3;
    //}
    num = int(random(3, 32));
    prevTime = second();
  }
}
