//this code is based on Nalim\

float position_x = 0;
float position_y = 0;
float speed_x = 8;
float speed_y = 9;

float snowman_faceDia = 200;

void setup() {
  size(1200, 800);
  
  position_x = width/2;
  position_y = height/2;
}

void draw() {
  background(#9CEFFC);
  rectMode(CENTER);
  ellipseMode(CENTER);

  //mountain
  noStroke();
  fill(255);
  circle(300, 998, 800);
  circle(900, 998, 900);

  noStroke();
  fill(255);
  circle(120, 400, 50);
  circle(600, 270, 20);
  circle(400, 100, 70);
  circle(700, 400, 20);
  circle(200, 100, 20);
  circle(1000, 200, 40);
  circle(1100, 300, 50);
  circle(800, 400, 10);
  circle(800, 100, 20);
  
  snowman(300, height/2+120, 180);
  snowman(width/2, height/2+100, 170);
  snowman(width/2+250, height/2+110, 190);

  snowman(position_x, position_y, snowman_faceDia);

  position_x = position_x + speed_x;
  position_y = position_y + speed_y;
  
  if(position_y > height - (snowman_faceDia + snowman_faceDia*1.4)*0.5){
    position_y = height - (snowman_faceDia + snowman_faceDia*1.4)*0.5;
    speed_y = speed_y * -1;
  }
  if(position_y < (snowman_faceDia + snowman_faceDia*1.4)*0.5){
    position_y = (snowman_faceDia + snowman_faceDia*1.4)*0.5;
    speed_y = speed_y * -1;
  }
  
  if(position_x > width - (snowman_faceDia*1.4)*0.5){
    position_x = width - (snowman_faceDia*1.4)*0.5;
    speed_x = speed_x * -1;
  }
  if(position_x < (snowman_faceDia*1.4)*0.5){
    position_x = (snowman_faceDia*1.4)*0.5;
    speed_x = speed_x * -1;
  }
  
  
}

void snowman(float x, float y, float faceSize) {
  pushMatrix();
  translate(x, y);


  pushMatrix();
  translate(0, -(faceSize*1.4)*0.5);
  
  //body
  stroke(#9CEFFC);
  strokeWeight(10);
  fill(255);
  ellipse(0, faceSize*0.5 + (faceSize*1.4)*0.5, faceSize*1.4, faceSize*1.4);
  noStroke();

  //face
  ellipse(0, 0, faceSize, faceSize);

  //eyes
  fill(0);
  ellipse(-faceSize*0.22, -faceSize*0.12, faceSize*0.15, faceSize*0.15);
  ellipse(faceSize*0.22, -faceSize*0.12, faceSize*0.15, faceSize*0.15);

  //cheek
  fill(#FF95D0);
  ellipse(-faceSize*0.26, faceSize*0.12, faceSize*0.30, faceSize*0.30);
  ellipse(faceSize*0.26, faceSize*0.12, faceSize*0.30, faceSize*0.30);

  //mouth
  fill(255, 0, 0);
  triangle(-faceSize*0.16, faceSize*0.28, faceSize*0.16, faceSize*0.28, 0, faceSize*0.45);

  //danchu
  fill(#764B54);
  rectMode(CENTER);
  rect(0, 0+224-55, 10, 10);
  rect(0, 0+224-11, 20, 20);
  rect(0, 0+224+70-20, 30, 30);

  //pal1
  stroke(#624C50);
  strokeWeight(10);
  line(0+100, 0+224-40, 0+200, 0+120);
  //finger1
  line(0+200, 0+120, 0+200, 0+100);
  line(0+200, 0+120, 0+230, 0+120);

  //pal2
  line(0-100, 0+224-40, 0-200, 0+120);
  //finger2
  line(0-200, 0+120, 0-200, 0+100);
  line(0-200, 0+120, 0-230, 0+120);
  
  popMatrix();
  
  //strokeWeight(1);
  //stroke(255, 0, 0);
  //noFill();
  //rect(0, 0, faceSize*1.4, faceSize + faceSize*1.4);
  
  popMatrix();
}
