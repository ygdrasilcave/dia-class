float ellipseSize = 20;

void setup() {
  size(800, 600);
}

void draw() {
  
  background(map(mouseY, 0, height, 0, 255));
  /*for (int y=0; y<height; y+=50) {
   for (int x=0; x<width; x+=50) {
   fill(255, 255, 0);
   ellipse(25+x, 25+y, 50, 50);
   }
   }*/
  
  ellipseSize = map(mouseX, 0, width, 20, 100);
  
  int col = int(width/ellipseSize);
  int row = int(height/ellipseSize);
   
  for (int y=0; y<row+1; y++) {
    for (int x=0; x<col+1; x++) {
      if (y%2 == 0) {
        if (x%2 == 0) {
          fill(255, 255, 0);
        } else {
          fill(255, 0, 255);
        }
      }else{
        if (x%2 == 0) {
          fill(255, 0, 255);
        } else {
          fill(255, 255, 0);
        }
      }
      ellipse(ellipseSize*0.5+x*ellipseSize, ellipseSize*0.5+y*ellipseSize, ellipseSize, ellipseSize);
    }
  }
}
