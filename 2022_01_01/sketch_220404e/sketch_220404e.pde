int y = 20;       // Vertical location of each line
int x = 50;       // Initial horizontal location for first line
int spacing = 10; // How far apart is each line
int len = 20;     // Length of each line

void setup() {
  size(480, 270);
  background(255);
}

void draw() {
  background(255);

  len = int(map(mouseY, 0, height, y, height-y*2));

  // Legs
  stroke(0);
  // Draw the first leg.

  for (x=10; x < width; x+=spacing) {
    line(x, y, x, y + len);
  }
}
