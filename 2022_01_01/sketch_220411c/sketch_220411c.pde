void setup(){
  size(800, 600);
}

void draw(){
  background(255);
  drawBlackCircle(width/2, height/2);
}

void drawBlackCircle(float _x, float _y) {
  fill(0);
  ellipse(_x, _y, 20, 20);
}
