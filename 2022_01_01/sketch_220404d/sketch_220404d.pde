/*size(480, 270);  
background(255);  
// Legs  
stroke(0);  
line(50, 60, 50, 80);  
line(60, 60, 60, 80);  
line(70, 60, 70, 80);  
line(80, 60, 80, 80);  
line(90, 60, 90, 80);  
line(100, 60, 100, 80);  
line(110, 60, 110, 80);  
line(120, 60, 120, 80);  
line(130, 60, 130, 80);  
line(140, 60, 140, 80);  
line(150, 60, 150, 80);*/

size(480, 270);
background(255);

// Legs
stroke(0);
int y = 80;       // Vertical location of each line
int x = 50;       // Initial horizontal location for first line
int spacing = 10; // How far apart is each line
int len = 20;     // Length of each line

// Draw the first leg.
line(x, y, x, y + len); 
// Add spacing so the next leg appears 10 pixels to the right.
x = x + spacing; 

// Continue this process for each leg, repeating it over and over.
line(x, y, x, y + len); 
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
x = x + spacing;
line(x, y, x, y + len);
