//https://processing.org/tutorials/pixels
//https://processing.org/examples  => Image

PImage img;

void setup(){
  size(1024, 575);
  img = loadImage("face.jpg");
}

void draw(){
  background(255);
  noStroke();
  
  float threshold = map(mouseX, 0, width, 0, 255);
  
  for(int x=0; x<img.width; x++){
    for(int y=0; y<img.height; y++){
      int index = x + (y*img.width);
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      if(br > 160){
        fill(255);
      }else if(br <= 160 && br > 80){
        fill(128);
      }else{
        fill(0);
      }
      
      //fill(br);
      rect(x, y, 1, 1);
    }
  }
}
