void setup() {
  size(800, 600);
}

void draw() {
  background(0);
  for (int y=0; y<height; y+=100) {
    for (int x=0; x<width; x+=100) {
      myShape(50+x, 50+y, 100);
    }
  }
}

void myShape(float _x, float _y, float _size) {
  //heart shadow
  fill(#AF2074);
  noStroke();
  arc(_x + _size*0.08, _y + _size*0.08, _size, _size, 0, PI);
  arc(_x+_size*0.25 + _size*0.08, _y + _size*0.08, _size*0.5, _size*0.5, PI, TWO_PI);
  arc(_x-_size*0.25 + _size*0.08, _y + _size*0.08, _size*0.5, _size*0.5, PI, TWO_PI);

  //heart
  fill(#FF4687);
  noStroke();
  arc(_x, _y, _size, _size, 0, PI);
  arc(_x+_size*0.25, _y, _size*0.5, _size*0.5, PI, TWO_PI);
  arc(_x-_size*0.25, _y, _size*0.5, _size*0.5, PI, TWO_PI);

  stroke(#FF4687);
  float _space = (_size*0.5)/8;
  for (int i=0; i < 8; i++) {
    line(_x + i*_space, _y, _x + i*_space, _y - _size*0.3);
  }
  for (int i=0; i < 8; i++) {
    line(_x - i*_space, _y, _x - i*_space, _y - _size*0.3);
  }

  //eyes&mouth
  fill(255);
  stroke(0);
  strokeWeight(1);
  ellipse(_x+_size*0.25, _y, _size*0.085, _size*0.085);
  ellipse(_x-_size*0.25, _y, _size*0.085, _size*0.085);
  arc(_x, _y+_size*0.2, _size*0.4, map(mouseX, 0, width, 0, _size*0.4), 0, PI, CHORD);
  fill(0);
  noStroke();
  ellipse(_x+_size*0.25, _y, _size*0.04, _size*0.04);
  ellipse(_x-_size*0.25, _y, _size*0.04, _size*0.04);
}
