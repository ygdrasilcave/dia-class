//https://processing.org/tutorials/pixels
//https://processing.org/examples  => Image

PImage img;

void setup(){
  size(1920, 1080);
  img = loadImage("face.jpg");
}

void draw(){
  background(255);
  //image(img, 0, 0, width/2, height);
  //image(img, width/2, 0, width/2, height);
  //image(img, 0, 0, width, height);
  
  for(int x=0; x<width; x+=100){
    for(int y=0; y<height; y+=100){
      //noFill();
      //stroke(0);
      //rect(x, y, 100, 100);
      image(img, x, y, 100, 100);
    }
  }
}
