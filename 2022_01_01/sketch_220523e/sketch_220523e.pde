void setup() {
  size(200, 200);
}

void draw() {
  background(255);
  stroke(0);
  fill(175);

  // Grab mouse coordinates, constrained to window
  int mx = constrain(mouseX, 0, width);
  int my = constrain(mouseY, 0, height);

  // Translate to the mouse location
  translate(mx, my);
  fill(255, 0, 0);
  ellipse(0, 0, 8, 8);

  // Translate 100 pixels to the right
  translate(100, 0);
  fill(0, 0, 255);
  ellipse(0, 0, 8, 8);

  // Translate 100 pixels down
  translate(0, 100);
  fill(0, 255, 0);
  ellipse(0, 0, 8, 8);

  // Translate 100 pixels left
  translate(-100, 0);
  fill(255, 255, 0);
  ellipse(0, 0, 8, 8);
}
