Ball b1;
Ball b2;

void setup(){
  size(600, 400);
  b1 = new Ball();
  b2 = new Ball();
}

void draw(){
  background(255);
  b1.display(true);
  b2.display(false);
}
