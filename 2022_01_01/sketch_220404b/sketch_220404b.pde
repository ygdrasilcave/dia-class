float y = 0;
float velocity = 0;
float acceleration = 0.98; 

void setup(){
  size(800, 600);
  background(0);
}

void draw(){
  background(0);
  rect(width/2-25, y-25, 50, 50);
  
  velocity = velocity + acceleration;
  if(velocity > 30){
    velocity = 30;
  }
  y = y + velocity;
  
  if(y > height-25){
    velocity = velocity * -0.82;
    y = height-25;
  }
  
  println("Y: " + y + ", velocity: " + velocity);
}
