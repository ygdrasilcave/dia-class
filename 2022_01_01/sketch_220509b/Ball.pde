class Ball {
  float x;
  float y;
  float speedX, speedY;
  float r;
  float margin;

  Ball() {
    x = width/2;
    y = height/2;
    speedX = random(-5, 5);
    speedY = random(-5, 5);
    r = random(10, 50);
    margin = 50;
  }

  void update() {
    x = x + speedX;
      y = y + speedY;

      if (x > width-margin) {
        x = width-margin;
        speedX = speedX * -1;
      }
      if (x < margin) {
        x = margin;
        speedX = speedX * -1;
      }

      if (y > height-margin) {
        y = height-margin;
        speedY = speedY * -1;
      }
      if (y < margin) {
        y = margin;
        speedY *= -1;
      }

      margin = map(mouseX, 0, width, 0, 150);
  }

  void display() {
    noStroke();
    fill(120);
    ellipse(x, y, r, r);

    stroke(255, 0, 0);
    line(0, margin, width, margin);
    line(margin, 0, margin, height);
    line(0, height-margin, width, height-margin);
    line(width-margin, 0, width-margin, height);
  }
}
