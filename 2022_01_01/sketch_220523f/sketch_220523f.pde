// Example 14-17: Nested push and pop

// Global angle for rotation
float theta = 0;

void setup() {
  size(480, 270);
}

void draw() {
  background(255);
  stroke(0);

  translate(width/2, height/2);

  for (float i = 0; i < TWO_PI; i += 0.2) {
    pushMatrix(); 
    rotate(theta + i);
    line(0, 0, 100, 0);

    for (float j = 0; j < TWO_PI; j += 0.5) {
      pushMatrix();
      translate(100, 0);
      rotate(-theta-j);
      line(0, 0, 50, 0);
      popMatrix();
    }
    //pushMatrix();
    //ellipse(100, 0, 10, 10);
    //popMatrix();
    
    popMatrix();
  }

  theta += 0.01;
}
