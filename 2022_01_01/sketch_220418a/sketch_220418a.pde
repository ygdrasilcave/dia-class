int num = 5;

int[] x;
//int[] x = new int[num];
int[] y;
float[] tx;
float[] speedTx;
float[] ty;
float[] speedTy;
float[] ellipseSize;

int[] test = {2, 5, 6, 8, 98, 1849, 74364};

int[] dice = {4,5,2,1,3,6};

void setup() {
  size(800, 800);
  x = new int[num];
  y = new int[num];
  tx = new float[num];
  speedTx = new float[num];
  ty = new float[num];
  speedTy = new float[num];
  ellipseSize = new float[num];
  /*
  x[0] = int(random(width));
   x[1] = int(random(width));
   x[2] = int(random(width));
   x[3] = int(random(width));
   x[4] = int(random(width));
   
   y[0] = int(random(height));
   y[1] = int(random(height));
   y[2] = int(random(height));
   y[3] = int(random(height));
   y[4] = int(random(height));
   */
   
   print(test.length);
   
  for (int i=0; i < x.length; i++) {
  //for (int i=0; i<num; i++) {
    x[i] = int(random(width));
    y[i] = int(random(height));
    tx[i] = random(1);
    speedTx[i] = random(0.005, 0.03);
    ty[i] = random(1);
    speedTy[i] = random(0.005, 0.03);
    ellipseSize[i] = random(5, 45);
  }
}

void draw() {
  background(255);
  fill(0);
  //for (int i=0; i<width; i+=100) {
  //  ellipse(i+50, height/2, 100, 100);
  //}
  stroke(0);
  for (int i=0; i<num; i++) {
    x[i] += map(noise(tx[i]), 0, 1, -3, 3);
    y[i] += map(noise(ty[i]), 0, 1, -3, 3);
    ellipse(x[i], y[i], ellipseSize[i], ellipseSize[i]);
    if (i<num-1) {
      line(x[i], y[i], x[i+1], y[i+1]);
    }
    tx[i] += speedTx[i];
    ty[i] += speedTy[i];
  }


  //for (int i=0; i<4; i++) {
  //  line(x[i], y[i], x[i+1], y[i+1]);
  //}
}

void keyPressed() {
  for (int i=0; i<num; i++) {
    x[i] = int(random(width));
    y[i] = int(random(height));
    tx[i] = random(1);
    speedTx[i] = random(0.005, 0.03);
    ty[i] = random(1);
    speedTy[i] = random(0.005, 0.03);
    ellipseSize[i] = random(5, 45);
  }
}
