Ball b1;
Ball b2;

void setup(){
  size(600, 400);
  b1 = new Ball();
  b2 = new Ball();
}

void draw(){
  //println(b1.intersect(b2));
  if(b1.intersect(b2) == true){
    background(0);
    b1.changeColor(true);
    b2.changeColor(true);
  }else{
    background(255);
    b1.changeColor(false);
    b2.changeColor(false);
  }
  b1.update();
  b2.update();
  b1.display();
  b2.display();
}
