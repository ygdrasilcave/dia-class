class Ball {
  float x;
  float y;
  float speedX, speedY;
  float r;
  float margin;

  color c;

  Ball() {
    x = width/2;
    y = height/2;
    speedX = random(-5, 5);
    speedY = random(-5, 5);
    r = random(10, 50);
    margin = r;
    c = color(120, 120, 120);
  }

  boolean intersect(Ball _b) {
    float _dist = dist(x, y, _b.x, _b.y);

    if (_dist < (r+_b.r)) {
      return true;
    } else {
      return false;
    }
  }

  void changeColor(boolean _c) {
    if (_c == true) {
      c = color(255, 0, 0);
    } else {
      c = color(120, 120, 120);
    }
  }

  void update() {
    x = x + speedX;
    y = y + speedY;

    if (x > width-margin) {
      x = width-margin;
      speedX = speedX * -1;
    }
    if (x < margin) {
      x = margin;
      speedX = speedX * -1;
    }

    if (y > height-margin) {
      y = height-margin;
      speedY = speedY * -1;
    }
    if (y < margin) {
      y = margin;
      speedY *= -1;
    }
  }

  void display() {
    noStroke();
    fill(c);
    ellipse(x, y, r*2, r*2);
  }
}
