//https://processing.org/tutorials/pixels
//https://processing.org/examples  => Image

PImage img;
int num = 3;
int prevTime = 0;

float theta = PI;
float t = 0.0;

void setup() {
  size(1920, 1920);
  img = loadImage("banana.png");
  prevTime = second();
}

void draw() {
  background(0);
  pushMatrix();
  translate(150, 120);

  for (int x=0; x<(width/200); x+=1) {
    for (int y=0; y<(height/140); y+=1) {
      pushMatrix();
      translate(x*200, y*140);
      if (y%2 == 0) {
        if (x%2 == 0) {
          rotate(0);
        } else {
          rotate(theta);
        }
      } else if (y%2 == 1) {
        if (x%2 == 1) {
          rotate(0);
        } else {
          rotate(theta);
        }
      }
      image(img, -100, -70, 200, 140);
      popMatrix();
    }
  }
  popMatrix();

  theta = map(sin(t), -1, 1, 0, TWO_PI);
  t += 0.058;
}
