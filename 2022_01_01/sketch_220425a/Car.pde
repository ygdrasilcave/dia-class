class Car{
  
  float x;
  float y;
  float speedX;
  
  float r, g, b;
  
  Car(){
    x = -100;
    y = random(height);
    speedX = random(2, 8);
    r = random(255);
    g = random(255);
    b = random(255);
  }
  
  Car(float yPosition){
    x = -100;
    y = yPosition;
    speedX = random(2, 8);
    r = random(255);
    g = random(255);
    b = random(255);
  }
  
  void update(){
    x += speedX;
    if(x > width+100){
      x = -100;
    }
  }
  
  void display(){
    fill(r, g, b);
    rect(x-50, y-20, 100, 40);
  }
  
}
