//Example 8-1: A Car class and a Car object

Car myCar1;
Car myCar2;

void setup() {
  size(800, 600);
  myCar1 = new Car(height/2);
  myCar2 = new Car(150);
}

void draw() {
  background(0);
  myCar1.update();
  myCar1.display();
  
  myCar2.update();
  myCar2.display();
}
