float ellipseSize = 80;
int col;
int row;

void setup() {
  size(800, 800);
  background(0);
  col = int(width/ellipseSize);
  row = int(height/ellipseSize);
}

void draw() {
  background(0);

  noFill();
  stroke(255);
  /*for (float x=ellipseSize*0.5; x<width; x+=ellipseSize) {
   for (float y=ellipseSize*0.5; y<height; y+=ellipseSize) {
   for (int step=0; step<6; step++) {
   float _space = ellipseSize/6.0;
   ellipse(x+_space*0.5*step, y, ellipseSize - _space*step, ellipseSize - _space*step);
   }
   }
   }*/

  for (int x=0; x<col; x+=1) {
    for (int y=0; y<row; y+=1) {
      for (int step=0; step<6; step++) {
        float _space = ellipseSize/6.0;
        if (x%2 == 0) {
          ellipse((ellipseSize*0.5 + x*ellipseSize) + _space*0.5*step, ellipseSize*0.5 + y*ellipseSize, ellipseSize - _space*step, ellipseSize - _space*step);
        } else {
          ellipse((ellipseSize*0.5 + x*ellipseSize), (ellipseSize*0.5 + y*ellipseSize) + _space*0.5*step, ellipseSize - _space*step, ellipseSize - _space*step);
        }
      }
    }
  }
}
