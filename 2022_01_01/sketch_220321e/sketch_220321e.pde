void setup() {
  size(800, 800);
}

int center_x = 0;

void draw() {
  background(20, 5, 70);

  // +, -, *, /
  /*
int x = 9;
   int y = 2;
   int z = x / y;
   float x = 9;
   float y = 2;
   int z = x / y;
   print(z);
   */

  //int center_x = width/2;
  //int center_y = height/2;
  
  int center_y = height/2;

  float circle_radius = (width/2.0)*0.25;

  float dia = (sqrt(width*width + height*height) / 2.0) - (circle_radius/2.0);
  println(dia);

  float dia_dist = (dia/5.0)/sqrt(2);
  println(dia_dist);

  //spreaddddddd0
  fill(100, 100, 150, 150);
  noStroke();
  ellipse(center_x + dia_dist, center_y - dia_dist, circle_radius*0.2, circle_radius*0.2);
  ellipse(center_x + dia_dist*2, center_y - dia_dist*2, circle_radius*0.4, circle_radius*0.4);
  ellipse(center_x + dia_dist*3, center_y - dia_dist*3, circle_radius*0.6, circle_radius*0.6);
  ellipse(center_x + dia_dist*4, center_y - dia_dist*4, circle_radius*0.8, circle_radius*0.8);
  ellipse(center_x + dia_dist*5, center_y - dia_dist*5, circle_radius, circle_radius);

  ellipse(center_x + dia_dist, center_y + dia_dist, circle_radius*0.2, circle_radius*0.2);
  ellipse(center_x + dia_dist*2, center_y + dia_dist*2, circle_radius*0.6, circle_radius*0.6);
  ellipse(center_x + dia_dist*3, center_y + dia_dist*3, circle_radius, circle_radius);

  ellipse(center_x - dia_dist, center_y - dia_dist, circle_radius*0.2, circle_radius*0.2);
  ellipse(center_x - dia_dist*2, center_y - dia_dist*2, circle_radius*0.6, circle_radius*0.6);
  ellipse(center_x - dia_dist*3, center_y - dia_dist*3, circle_radius, circle_radius);

  ellipse(center_x - dia_dist, center_y + dia_dist, circle_radius*0.2, circle_radius*0.2);
  ellipse(center_x - dia_dist*2, center_y + dia_dist*2, circle_radius*0.6, circle_radius*0.6);
  ellipse(center_x - dia_dist*3, center_y + dia_dist*3, circle_radius, circle_radius);

  //0
  noStroke();
  fill(100, 100, 150, 100);
  ellipse(center_x, center_y, 150, 150);

  fill(255, 255, 255, 100);
  ellipse(center_x, center_y, 10, 10);
  ellipse(center_x, center_y, 50, 50);
  ellipse(center_x, center_y, 80, 80);

  //spreadline
  strokeWeight(5);
  stroke(100, 100, 150, 100);
  noFill();
  ellipse(center_x, center_y, 150, 70);
  ellipse(center_x, center_y, 300, 70);
  ellipse(center_x, center_y, 500, 70);
  ellipse(center_x, center_y, 900, 70);

  ellipse(center_x, center_y, 70, 150);
  ellipse(center_x, center_y, 70, 300);
  ellipse(center_x, center_y, 70, 500);
  ellipse(center_x, center_y, 70, 900);
  
  center_x = center_x + 1;
}
