Catcher c;

void setup(){
  size(600, 400);
  c = new Catcher();
}

void draw(){
  background(255);
  c.update(mouseX, height/2);
  c.display();
}
