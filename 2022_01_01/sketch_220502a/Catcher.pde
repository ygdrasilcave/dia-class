class Catcher{
  float x;
  float y;
  
  Catcher(){
    x = 0;
    y = 0;
    println("new class: Catcher");
  }
  
  void update(float _x, float _y){
    x = _x;
    y = _y;
  }
  
  void display(){
    fill(0);
    ellipse(x, y, 100, 100);
  }
}
