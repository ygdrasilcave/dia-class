//https://processing.org/tutorials/pixels
//https://processing.org/examples  => Image

PImage img;
PImage img_w;

float theta = 0.0;

void setup() {
  size(1024, 575);
  img = loadImage("face.jpg");
  img_w = loadImage("face_W.jpg");
}

void draw() {
  background(0);
  noStroke();
  
  strokeWeight(3);

  int pixelSize = int(map(mouseX, 0, width, 1, 50));

  for (int x=0; x<img.width; x+=pixelSize) {
    for (int y=0; y<img.height; y+=pixelSize) {
      int index = x + (y*img.width);
      color c1 = img.pixels[index];
      color c2 = img_w.pixels[index];
      float r1 = red(c1);
      float g1 = green(c1);
      float b1 = blue(c1);
      float br1 = brightness(c1);
      float r2 = red(c2);
      float g2 = green(c2);
      float b2 = blue(c2);
      float br2 = brightness(c2);
      //fill(r1, g1, b1);
      //ellipse(x, y, pixelSize, pixelSize);
      //textSize(pixelSize*1.25);
      //text("@", x, y);
      pushMatrix();
      stroke(r1, g1, b1);
      translate(x, y);
      rotate(map(br1, 0, 255, 0, HALF_PI) + theta);
      line(0, 0, pixelSize, 0);
      popMatrix();
      
      pushMatrix();
      stroke(r2, g2, b2);
      translate(x, y);
      rotate(map(br2, 0, 255, HALF_PI, 0) + theta);
      line(0, 0, pixelSize, 0);
      popMatrix();
    }
  }
  
  //theta += 0.05;
}
