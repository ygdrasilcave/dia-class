int x = 0;
int y = 0;

int count = 0;
int rectSize = 30;
int speed = 10;

void setup() {
  size(800, 600);
  background(0);
}

void draw() {
  background(0);

  rect(x, y, rectSize, rectSize);

  if (count == 0) {
    x = x + speed;
    y = 0;
    if(x > width-rectSize){
      count = 1;
      x = width-rectSize;
      println(count);
    }
  }
  else if (count == 1) {
    x = width-rectSize;
    y = y + speed;
    if(y > height-rectSize){
      count = 2;
      y = height-rectSize;
      println(count);
    }
  }
  else if (count == 2) {
    x = x - speed;
    y = height-rectSize;
    if(x < 0){
      count = 3;
      x = 0;
      println(count);
    }
  }
  else if (count == 3) {
    x = 0;
    y = y - speed;
    if(y < 0){
      count = 0;
      y = 0;
      println(count);
    }
  }
}
