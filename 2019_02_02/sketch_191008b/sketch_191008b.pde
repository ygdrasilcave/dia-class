Ball[] b;

void setup() {
  size(1000, 1000);
  background(0);
  b = new Ball[100];
  for (int i=0; i<100; i++) {
    b[i] = new Ball(random(1, 30));
  }
}

void draw() {
  background(0);
  PVector gravity = new PVector(0, 0.1);
  PVector wind = new PVector(0.089, 0);

  for (int i=0; i<100; i++) {
    b[i].updateForce(wind);
    float m = b[i].mass;
    PVector g = PVector.mult(gravity, m);
    b[i].updateForce(g);
    b[i].update();
    b[i].display();
  }
}
