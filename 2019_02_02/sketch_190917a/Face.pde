class Face{
  float x, y, w, h;
  float eyesOffsetX, eyesOffsetY, eyesW, eyesH;
  float noseOffsetX, noseOffsetY, noseW, noseH;
  float lipsOffsetX, lipsOffsetY, lipsW, lipsH;
  
  Face(float _x, float _y, float _w, float _h){
    x = _x;
    y = _y;
    w = _w;
    h = _h;
    
    eyesOffsetX = random(10, w/2-w*0.2);
    eyesOffsetY = random(10, h/3);
    eyesW = random(w*0.1, w/2);
    eyesH = eyesW*random(0.1, 0.8);
    
    noseOffsetX = random(-w*0.1, w*0.1);
    noseOffsetY = random(-h*0.1, h*0.2);
    noseW = random(w*0.1, w*0.25);
    noseH = random(h*0.1, h*0.35);
    
    lipsOffsetX = random(-w*0.1, w*0.1);
    lipsOffsetY = random(noseOffsetY + noseH/2 , h/5);
    lipsW = random(w*0.1, w*0.8);
    lipsH = w*random(0.25, 1.0);
    
  }
  
  void eyes(float _ex, float _ey, float _ew, float _eh, float _eAngle){
    pushMatrix();
    translate(x-_ex, y-_ey);
    rotate(radians(_eAngle));
    noFill();
    stroke(0);
    strokeWeight(5);
    arc(0, 0, _ew, _eh, radians(180), radians(360));
    popMatrix();
    
    pushMatrix();
    translate(x+_ex, y-_ey);
    rotate(radians(_eAngle*-1));
    noFill();
    stroke(0);
    strokeWeight(5);
    arc(0, 0, _ew, _eh, radians(180), radians(360));
    popMatrix();
  }
  
  void nose(float _nx, float _ny, float _nw, float _nh){
    ellipse(x+_nx, y+_ny, _nw, _nh);
  }
  
  void lips(float _lx, float _ly, float _lw, float _lh){
    noFill();
    stroke(0);
    strokeWeight(5);
    arc(x+_lx, y+_ly, _lw, _lh, radians(30), radians(180-30));
  }
  
  void display(float _eAngle){
    fill(255);
    ellipse(x, y, w, h);
    eyes(eyesOffsetX, eyesOffsetY, eyesW, eyesH, _eAngle);
    nose(noseOffsetX, noseOffsetY, noseW, noseH);
    lips(lipsOffsetX, lipsOffsetY, lipsW, lipsH);   
  }

}
