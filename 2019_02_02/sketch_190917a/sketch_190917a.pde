Face[] f;
float tx = 0;
float t = 0;
float tSpeed = 0.135;

void setup() {
  size(3000, 2000);
  f = new Face[150];
  
  for (int x=0; x<15; x++) {
    for (int y=0; y<10; y++) {
      int index = x + y*15;
      f[index] = new Face(100+x*200, 100+y*200, random(100, 200), random(100, 200));
    }
  }
}

void draw() {
  background(0);
  for (int i=0; i<150; i++) {
    f[i].display(map(tx, -1, 1, -45, 45));
  }
  
  tx = sin(t);
  t += tSpeed;
}
