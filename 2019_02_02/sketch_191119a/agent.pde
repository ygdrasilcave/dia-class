class Agent {

  PVector pos;
  PVector vel;
  PVector acc;
  float r;
  float maxForce;
  float maxSpeed;

  PVector[] tail;
  float[] tailAngle;
  int tailLength;
  int currTime;
  int count = 0;
  int tailTime;

  boolean shape;
  boolean fillType;

  Agent(float _x, float _y) {
    pos = new PVector(_x, _y);
    acc = new PVector(0, 0);
    vel = new PVector(0, 0);
    maxSpeed = 8;
    maxForce = 0.25;
    r = random(30, 100);

    tailLength = int(random(5, 30));
    tailTime = int(random(50, 100));

    tail = new PVector[tailLength];
    tailAngle = new float[tailLength];
    for (int i=0; i<tailLength; i++) {
      tail[i] = pos.copy();
    }    
    currTime = millis();

    if (random(10) > 5) {
      shape = true;
    } else {
      shape = false;
    }
    if (random(10) > 5) {
      fillType = true;
    } else {
      fillType = false;
    }
  }

  void update() {
    vel.add(acc);
    vel.limit(maxSpeed);
    pos.add(vel);
    acc.mult(0);

    if (pos.x < -100) {
      pos.x = width+100;
    }
    if (pos.x > width+100) {
      pos.x = -100;
    }
    if (pos.y < -100) {
      pos.y = height+100;
    }
    if (pos.y > height+100) {
      pos.y = -100;
    }

    if (currTime + tailTime < millis()) {
      tail[tailLength-1] = pos.copy();
      tailAngle[tailLength-1] = vel.heading();
      for (int i=0; i<tailLength-1; i++) {
        tail[i] = tail[i+1];
        tailAngle[i] = tailAngle[i+1];
      }
      currTime = millis();
    }
  }

  void addForce(PVector _f) {
    acc.add(_f);
  }

  void steering(ForceField _f) {
    PVector newTargetVel = _f.lookup(pos);
    newTargetVel.setMag(maxSpeed);
    PVector steer = PVector.sub(newTargetVel, vel);
    steer.limit(maxForce);
    addForce(steer);
  }

  void display() {
    if (fillType == true) {
      noStroke();
      fill(255);
    } else {
      stroke(255);
      noFill();
    }
    strokeWeight(2);

    for (int i=tailLength-1; i>=0; i--) {
      float _r = r * (float(i)/(tailLength-1));
      pushMatrix();
      translate(tail[i].x, tail[i].y);
      rotate(tailAngle[i]);
      if (shape == true) {
        ellipse(0, 0, _r, _r);
      } else {
        rect(0, 0, _r, _r);
      }
      if (i == tailLength-1) {
        ellipse(_r/2+10, -_r/3, _r/3, _r/3);
        ellipse(_r/2+10, _r/3, _r/3, _r/3);
      }
      popMatrix();
    }
  }
}
