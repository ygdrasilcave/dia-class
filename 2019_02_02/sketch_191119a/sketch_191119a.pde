ArrayList<Agent> a;
ForceField field;

void setup() {
  size(1600, 1600);
  a = new ArrayList<Agent>();
  field = new ForceField();
  rectMode(CENTER);
}

void draw() {
  background(0);

field.update();
  field.display();

  for (int i=0; i<a.size(); i++) {
    Agent _a = a.get(i);
    _a.steering(field);
    _a.update();
    _a.display();
  }
}

void mousePressed() {
  a.add(new Agent(mouseX, mouseY));
}
