PVector a;
PVector b;

void setup(){
  size(1000, 1000);
  a = new PVector(500, 500);
  b = new PVector(300, 100);
}

void draw(){
  background(0);
  a.set(mouseX, mouseY);
  
  strokeWeight(5);
  stroke(255);
  line(0, 0, a.x, a.y);
  
  noFill();
  stroke(255, 0, 0);
  rect(0, 0, a.mag(), 100);
  
  strokeWeight(15);
  stroke(0, 0, 255);
  //a.limit(300);
  //a.normalize();
  //a.mult(300);
  a.setMag(300);
  line(0, 0, a.x, a.y);
  
}
