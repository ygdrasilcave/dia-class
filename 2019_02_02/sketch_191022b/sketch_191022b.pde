ArrayList<Animal> ani;

void setup() {
  size(1600, 1600);
  ani = new ArrayList<Animal>();
  rectMode(CENTER);
}

void draw() {
  background(0);

  int removeIndex = 0;
  boolean remove = false;

  for (int i=ani.size()-1; i>=0; i--) {
    Animal me = ani.get(i);

    me.update();
    me.display();

    for (int j=ani.size()-1; j>=0; j--) {
      if (i != j) {
        Animal friend = ani.get(j);
        int myID = me.id;
        int friendID = friend.id;
        float dist = PVector.dist(me.pos, friend.pos);
        if (dist < 50) {
          if (myID < friendID) {
            remove = true;
            removeIndex = j;
          }
        }

        if (remove == true) {
          ani.remove(removeIndex);
          remove = false;
        }
      }
    }
  }
}

void mousePressed() {
  ani.add(new Quad(mouseX, mouseY));
}

void keyPressed() {
  if (key == ' ') {
    ani.add(new Animal(width/2, height/2));
  }
  if (key == 'b') {
    ani.add(new Bird(width/2, height/2));
  }
}
