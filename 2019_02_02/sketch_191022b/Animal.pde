class Animal{
  PVector pos;
  PVector vel;
  int id;
  
  Animal(float _x, float _y){
    pos = new PVector(_x, _y);
    vel = new PVector(random(3), random(5));
    id = 0;
  }
  
  void update(){
    pos.add(vel);
    
    if(pos.x > width){
      pos.x = width;
      vel.x *= -1;
    }
    if(pos.x < 0){
      pos.x = 0;
      vel.x *= -1;
    }
    if(pos.y > height){
      pos.y = height;
      vel.y *= -1;
    }
    if(pos.y < 0){
      pos.y = 0;
      vel.y *= -1;
    }
  }
  
  void display(){
    noFill();
    strokeWeight(5);
    stroke(255);
    ellipse(pos.x, pos.y, 100, 100);
  }
}
