class Quad extends Animal{
  
  Quad(float _x, float _y){
    super(_x, _y);
    vel.x = random(1, 3);
    vel.y = random(1, 3);
    id = 1;
  }
  
  void display(){
    fill(255);
    noStroke();
    pushMatrix();  
    translate(pos.x, pos.y);
    rotate(vel.heading());
    rect(50, -50, 5, 50);
    rect(-50, -50, 5, 50);
    rect(50, 50, 5, 50);
    rect(-50, 50, 5, 50);
    ellipse(0, 0, 100, 100);
    popMatrix();
  }
}

class Bird extends Animal{
  
  Bird(float _x, float _y){
    super(_x, _y);
    vel.x = random(3, 8);
    vel.y = random(3, 8);
    id = 2;
  }
  
  void display(){
    fill(255);
    noStroke();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(vel.heading());
    ellipse(0, -50, 10, 30);
    ellipse(0, 50, 10, 30);
    rect(0, 0, 50, 50);
    popMatrix();
  }
}
