int x, y = 0;
int pixelSize = 100;

int currTime = 0;
int duration = 100;

void setup() {
  size(1200, 1200);
  background(0);
  currTime = millis();
}

void draw() {
  if (currTime + duration < millis()) {
    x += pixelSize;
    if (x >= width) {
      x = 0;
      y += pixelSize;
      if (y >= height) {
        y = 0;
        x = 0;
      }
    }
    noStroke();
    fill(random(255), random(255), random(255));
    rect(x, y, pixelSize, pixelSize);   
    currTime = millis();
  }
}
