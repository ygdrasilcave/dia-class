class Ball {
  float x, y;
  float speedX, speedY;
  float dirX, dirY;
  float w, h;

  Ball() {
    w = random(10, 80);
    h = w;
    x = random(w/2, width-w/2);
    y = random(h/2, height-h/2);
    speedX = random(5, 10);
    speedY = random(6, 12);
    dirX = 1;
    dirY = 1;
  }

  Ball(float _x, float _y, float _sx, float _sy) {
    w = random(10, 80);
    h = w;
    x = _x;
    y = _y;
    speedX = _sx;
    speedY = _sy;
    dirX = 1;
    dirY = 1;
  }

  void update() {
    x += dirX*speedX;
    y += dirY*speedY; 

    if (x > width-w/2) {
      dirX *= -1;
    } else if (x < w/2) {
      dirX *= -1;
    }

    if (y > height-h/2) {
      dirY *= -1;
    } else if (y < h/2) {
      dirY *= -1;
    }
  }

  void display(boolean _c) {
    if (_c == true) {
      fill(random(255),random(255),random(255));
    } else {
      fill(255, 255, 255);
    }
    ellipse(x, y, w, h);
  }
}
