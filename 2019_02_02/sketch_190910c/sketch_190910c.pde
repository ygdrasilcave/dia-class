Ball[] b;

boolean c = false;

void setup() {
  size(2000, 2000);
  background(0);
  b = new Ball[200];
  for (int i=0; i<200; i++) {
    if (i%2 == 0) {
      b[i] = new Ball();
    } else {
      b[i] = new Ball(random(width/3, 2*(width/3)), random(height/3, 2*(height/3)), 0, 0);
    }
  }
}

void draw() {
  background(0);
  for (int i=0; i<200; i++) {
    b[i].update();
    b[i].display(c);
  }
}

void keyPressed() {
  if (key == ' ') {
    if (c == false) {
      c = true;
    }else{
      c = false;
    }
  }
}
