class Tile {
  float w;

  Tile(float _w) {
    w = _w;
  }

  void display(float _x, float _y, float _r, float _w) {
    w = _w;
    strokeWeight(12);
    stroke(255);
    noFill();
    
    pushMatrix();
    
    translate(w/2 + _x, w/2 + _y);
    rotate(radians(_r));

    arc(-w/2, -w/2, w, w, 0, HALF_PI, OPEN);
    arc(-w/2+w, -w/2+w, w, w, PI, PI+HALF_PI, OPEN);

    strokeWeight(1);
    rect(-w/2, -w/2, w, w);
    popMatrix();
  }
}
