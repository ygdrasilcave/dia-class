import de.looksgood.ani.*;
Ani rotateAni;
Ani diaAni;

Tile[] tiles;
int cols = 20;
int rows = 20;
float tileSize;
float tileSizeAni;
int[] rotateVal;
int[] rotateSample = {0, 90, 180, 270};
float rotateOffset = 0;

boolean tick = true;

void setup() {
  size(1800, 1800);
  tileSize = width/cols;
  tileSizeAni = tileSize;
  
  tiles = new Tile[cols*rows];
  rotateVal = new int[cols*rows];
  for (int i=0; i<cols*rows; i++) {
    tiles[i] = new Tile(tileSize);
    int index = int(random(4));
    rotateVal[i] = rotateSample[index];
  }
  strokeCap(SQUARE);
  Ani.init(this);
  rotateAni = new Ani(this, 1.5, "rotateOffset", rotateOffset+90, Ani.ELASTIC_IN_OUT);
  diaAni = new Ani(this, 2.0, "tileSizeAni", tileSizeAni, Ani.ELASTIC_IN_OUT);
}

void draw() {
  background(0);
  pushMatrix();
  translate((tileSize-tileSizeAni)/2, (tileSize-tileSizeAni)/2);
  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = x + y*cols;
      tiles[index].display(x*tileSize, y*tileSize, rotateVal[index]+rotateOffset, tileSizeAni);
    }
  }
  popMatrix();

  int s = second();
  if (s%5 == 0 && tick == true) {
    rotateAni.setBegin(rotateOffset);
    rotateAni.setEnd(rotateOffset+90);
    rotateAni.start();
    tick = false;
  } else if (s%5 == 1) {
    tick = true;
  }
}

void keyPressed() {
  diaAni.setPlayMode(Ani.YOYO);
  diaAni.repeat(2);
  diaAni.setBegin(tileSize);
  diaAni.setEnd(tileSize+100);
  diaAni.start();
}

void mouseReleased() {
  rotateAni.setBegin(rotateOffset);
  rotateAni.setEnd(rotateOffset+90);
  rotateAni.start();
}
