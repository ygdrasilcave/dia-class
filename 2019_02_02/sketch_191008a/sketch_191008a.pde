Ball b1, b2;

void setup(){
  size(1000, 1000);
  background(0);
  b1 = new Ball(50);
  b2 = new Ball(5);
}

void draw(){
  background(0);
  PVector gravity = new PVector(0, 0.1);
  PVector wind = new PVector(0.089, 0);
  b1.updateForce(wind);
  b2.updateForce(wind);
  float m1 = b1.mass;
  float m2 = b2.mass;
  PVector g1 = PVector.mult(gravity, m1);
  PVector g2 = PVector.mult(gravity, m2);
  b1.updateForce(g1);
  b1.update();
  b1.display();
  b2.updateForce(g2);
  b2.update();
  b2.display();
}
