import controlP5.*;
ControlP5 cp5;

Textlabel myTextlabelA;
Slider2D slider;

String s = "";

float speedX, speedY;
float dirX, dirY;
float posX, posY;

boolean v = true; 

void setup() {
  size(1000, 1000);
  noStroke();
  cp5 = new ControlP5(this);

  // create a new button with name 'buttonA'
  cp5.addButton("colorA")
    .setValue(0)
    .setPosition(100, 100)
    .setSize(200, 50)
    .setLabelVisible(false);
  ;

  cp5.addButton("colorB")
    .setValue(0)
    .setPosition(100, 150)
    .setSize(200, 50)
    .setLabelVisible(true);
  ;

  myTextlabelA = cp5.addTextlabel("label_colorA")
    .setText("colorA")
    .setPosition(150, 100)
    .setColorValue(0xFF80FFFF)
    .setFont(createFont("Georgia", 30))
    ;

  slider = cp5.addSlider2D("wave")
    .setPosition(500, 100)
    .setSize(300, 300)
    .setMinMax(0, 0, 20, 20)
    .setValue(3, 5)
    //.disableCrosshair()
    ;

  textSize(120);

  dirX = 1;
  dirY = 1;
  speedX = slider.getArrayValue()[0];
  speedY = slider.getArrayValue()[1];
}

void draw() {
  background(0);
  fill(255);
  noStroke();
  text(s, posX, posY);
  
  speedX = slider.getArrayValue()[0];
  speedY = slider.getArrayValue()[1];

  posX += dirX*speedX;
  posY += dirY*speedY;
  if (posX > width) {
    posX = width;
    dirX *= -1;
  }
  if (posX < 0) {
    posX = 0;
    dirX *= -1;
  }
  if (posY > height) {
    posY = height;
    dirY *= -1;
  }
  if (posY < 0) {
    posY = 0;
    dirY *= -1;
  }
}

public void controlEvent(ControlEvent theEvent) {
  println(theEvent.getController().getName());
}

public void colorA(int theValue) {
  println("a button event from colorA: "+theValue);
  s = "A";
}

public void colorB(int theValue) {
  println("a button event from colorB: "+theValue);
  s = "B";
}

void keyPressed(){
  if(key == ' '){
    v = !v;
    slider.setVisible(v);
  }
}
