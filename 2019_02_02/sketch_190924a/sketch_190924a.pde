PVector a;
PVector b;

void setup() {
  size(1000, 1000);
  a = new PVector(500, 500);
  b = new PVector(300, 100);
}

void draw() {
  background(0);
  strokeWeight(5);
  stroke(255);
  line(0, 0, a.x, a.y);
  line(0, 0, b.x, b.y);
  
  stroke(255, 0, 0);
  PVector c = PVector.add(a, b);
  line(0, 0, c.x, c.y);
  
  stroke(0, 255, 0);
  PVector d = PVector.sub(a, b);
  line(0, 0, d.x, d.y);
}
