ArrayList<Agent> a;

void setup() {
  size(1000, 1000);
  a = new ArrayList<Agent>();
  a.add(new Agent(width/2, height/2));
}

void draw() {
  background(0);

  PVector target = new PVector(mouseX, mouseY);

  for (int i=0; i<a.size(); i++) {
    Agent _a = a.get(i);
    _a.steering(target);
    _a.update();
    _a.display();
  }
}

void mousePressed() {
  a.add(new Agent(mouseX, mouseY));
}
