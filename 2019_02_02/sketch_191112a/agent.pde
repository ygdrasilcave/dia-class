class Agent {

  PVector pos;
  PVector vel;
  PVector acc;
  float r;
  float maxForce;
  float maxSpeed;

  Agent(float _x, float _y) {
    pos = new PVector(_x, _y);
    acc = new PVector(0, 0);
    vel = new PVector(0, 0);
    maxSpeed = 3;
    maxForce = 0.15;
    r = 50;
  }

  void update() {
    vel.add(acc);
    vel.limit(maxSpeed);
    pos.add(vel);
    acc.mult(0);
  }

  void addForce(PVector _f) {
    acc.add(_f);
  }

  void steering(PVector _target) {
    PVector newTargetVel = PVector.sub(_target, pos);
    float d = newTargetVel.mag();
    if (d < 100) {
      float m = map(d, 0, 100, 0, maxSpeed);
      newTargetVel.setMag(m);
    } else {
      newTargetVel.setMag(maxSpeed);
    }
    PVector steer = PVector.sub(newTargetVel, vel);
    steer.limit(maxForce);
    addForce(steer);
  }

  void display() {
    fill(255);
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(vel.heading());
    ellipse(0, 0, r, r);
    ellipse(35, 0, r/3, r/3);
    popMatrix();
  }
}
