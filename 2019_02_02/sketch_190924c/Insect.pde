class Insect{
  
  PVector pos;
  PVector speed;
  PVector acc;
  float t = 0;
  float wingRotVal = 0;
  float accNoiseTx = 0;
  float accNoiseTy = 0;
  float accNoiseTspeedX = 0;
  float accNoiseTspeedY = 0;
  
  Insect(){
    pos = new PVector(random(width), random(height));
    speed = new PVector(0, 0);
    acc = PVector.random2D();
    accNoiseTspeedX = random(0.005, 0.03);
    accNoiseTspeedY = random(0.005, 0.03);
  }
  
  
  void update(){
    acc.set(map(noise(accNoiseTx), 0, 1, -0.5, 0.5), map(noise(accNoiseTy), 0, 1, -0.5, 0.5));
    speed.add(acc);
    pos.add(speed);
    speed.limit(8);
    
    if(pos.x > width+300){
      pos.x = -300;
    }
    if(pos.x < -300){
      pos.x = width+300;
    }
    if(pos.y > height+300){
      pos.y = -300;
    }
    if(pos.y < -300){
      pos.y = height+300;
    }
    
    t += 0.65;
    wingRotVal = radians(map(sin(t), -1, 1, 20, -20));
    
    accNoiseTx += accNoiseTspeedX;
    accNoiseTy += accNoiseTspeedY;
  }
  
  
  void display(){
    fill(255);
    noStroke();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(speed.heading());
    rect(-50, -25, 100, 50);
    ellipse(80, -15, 30, 30);
    ellipse(80, 15, 30, 30);
    pushMatrix();
    translate(10, -30);
    rotate(wingRotVal);
    ellipse(0, -30, 30, 60);
    popMatrix();
    pushMatrix();
    translate(10, 30);
    rotate(-wingRotVal);
    ellipse(0, 30, 30, 60);
    popMatrix();
    popMatrix();
    
    //stroke(255);
    //strokeWeight(5);
    //line(0, 0, pos.x, pos.y);
  }
  
}
