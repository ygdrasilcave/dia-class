Insect[] a;

void setup() {
  size(2000, 2000);

  a = new Insect[50];

  for (int i=0; i<50; i++) {
    a[i] = new Insect();
  }
}

void draw() {
  background(0);
  for (int i=0; i<50; i++) {
    a[i].update();
    a[i].display();
  }
}
