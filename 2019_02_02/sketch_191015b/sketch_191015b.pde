ArrayList<Pig> p;

boolean enableGravity = false;
boolean enableWind = false;

import processing.sound.*;
//TriOsc triOsc;
SinOsc sine;
Env env; 
Reverb reverb;
// Times and levels for the ASR envelope
float attackTime = 0.001;
float sustainTime = 0.12;
float sustainLevel = 0.5;
float releaseTime = 0.2;
int noteValue = 60;

int interval = 300;
int currTime = 0;
int ampT = 0;

void setup() {
  size(2000, 2000);
  p = new ArrayList<Pig>();

  for (int i=0; i<25; i++) {
    p.add(new Pig());
  }

  //triOsc = new TriOsc(this);
  sine = new SinOsc(this);
  env = new Env(this);
  //reverb = new Reverb(this);
  //reverb.process(sine);
  //reverb.room(0.8);
  //reverb.damp(0.2);
  //reverb.wet(1.0);

  currTime = millis();

  textSize(60);
  textAlign(CENTER, CENTER);

  //sine.play(60, 1.0);
}

void draw() {
  background(0);
  PVector gravity = new PVector(0.0, 0.98);
  PVector wind = new PVector(-0.5, 0.0);

  int removeIndex = 0;
  boolean remove = false;

  for (int i=p.size()-1; i>=0; i--) {
    Pig _p = p.get(i);

    for (int j=p.size()-1; j>=0; j--) {
      if (j != i) {
        Pig _p2 = p.get(j);

        float _dia = _p.w*0.3;
        float _dia2 = _p2.w*0.3;
        float _dist = PVector.dist(_p.pos, _p2.pos);
        if (_dist < _dia+_dia2) {
          if (_dia > _dia2) {
            remove = true;
            removeIndex = j;
            noteValue = int(map(_dia2, 100*0.3, 300*0.3, 52, 76));
            playTone(_dia2*0.015);
            _p.eatCounter++;
          } else if (_dia < _dia2) {
            remove = true;
            removeIndex = i;
            noteValue = int(map(_dia, 100*0.3, 300*0.3, 52, 76));
            playTone(_dia*0.015);
            _p2.eatCounter++;
          }
        }
      }
    }
    _p.updateAcc();
    if (enableGravity == true) {
      _p.updateForce(gravity);
    }
    if (enableWind == true) {
      _p.updateForce(wind);
    }
    _p.update();
    _p.display();

    if (remove == true) {
      p.remove(removeIndex);
      remove = false;
    }
  }

  if (millis() > currTime + interval) {
    p.add(new Pig());
    currTime = millis();
  }

  //sine.amp(map(sin(ampT), -1.0, 1.0, 0.1, 1.0));
  //ampT += 0.25;
}

void playTone(float _sustainTime) {
  //triOsc.play(midiToFreq(noteValue), 0.5);  
  //env.play(triOsc, attackTime, _sustainTime, sustainLevel, releaseTime);
  sine.play(midiToFreq(noteValue), 0.6);
  env.play(sine, attackTime, _sustainTime, sustainLevel, releaseTime); 

  //sine.freq(midiToFreq(noteValue));
}

float midiToFreq(int note) {
  return (pow(2, ((note-69)/12.0))) * 440;
}

void keyPressed() {
  if (key == 'g') {
    enableGravity = !enableGravity;
    println(enableGravity);
  }
  if (key == 'w') {
    enableWind = !enableWind;
    println(enableWind);
  }
}

void mousePressed() {
  p.add(new Pig(mouseX, mouseY));
}
