ArrayList<Insect> fly;

void setup() {
  size(2000, 2000);
  fly = new ArrayList<Insect>();
  println(fly.size());
}

void draw() {
  background(0);
  for (int i=0; i<fly.size(); i++) {
    fly.get(i).update();
    fly.get(i).display();
    //int age = fly.get(i).age;
    int age = fly.get(i).getAge();
    if(age < 0){
      fly.remove(i);
      println(i + " is removed");
    }
  }
}

void mousePressed(){
  fly.add(new Insect(mouseX, mouseY));
  println(fly.size());
}
