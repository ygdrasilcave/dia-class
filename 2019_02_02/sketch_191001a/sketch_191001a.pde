ArrayList<Pig> p;

boolean enableGravity = false;
boolean enableWind = false;

void setup() {
  size(2000, 2000);
  p = new ArrayList<Pig>();

  for (int i=0; i<50; i++) {
    p.add(new Pig());
  }
}

void draw() {
  background(0);
  PVector gravity = new PVector(0.0, 0.98);
  PVector wind = new PVector(-0.5, 0.0);
  for (int i=0; i<p.size(); i++) {
    p.get(i).updateAcc();
    if (enableGravity == true) {
      p.get(i).updateForce(gravity);
    }
    if (enableWind == true) {
      p.get(i).updateForce(wind);
    }
    p.get(i).update();
    p.get(i).display();
  }
}

void keyPressed() {
  if (key == 'g') {
    enableGravity = !enableGravity;
    println(enableGravity);
  }
  if (key == 'w') {
    enableWind = !enableWind;
    println(enableWind);
  }
}
