class Pig {
  PVector pos;
  PVector acc;
  PVector vel;
  float topSpeed;
  float w, h;
  float t1, t2;
  float t1s, t2s;
  float tTail;
  float tTailSpeed;
  float tNose;
  float tNoseSpeed;
  float tEyeball;
  float tailLengthFactor;

  Pig() {
    pos = new PVector(random(width), random(height));
    vel = new PVector(0, 0);
    acc = new PVector(0.5, 0.3);
    topSpeed = random(3, 10);
    w = random(100, 300);
    h = w;
    t1s = random(0.005, 0.03);
    t2s = random(0.005, 0.03);
    tTailSpeed = random(0.002, 0.005);
    tNoseSpeed = random(0.05, 0.085);
    tailLengthFactor = random(0.0008, 0.008);
  }

  void updateAcc() {
    acc.set(map(noise(t1), 0, 1, -0.5, 0.5), map(noise(t2), 0, 1, -0.5, 0.5));
  }

  void updateForce(PVector _f) {
    acc.add(_f);
  }

  void update() {   
    vel.add(acc);
    pos.add(vel);
    vel.limit(topSpeed);

    //acc.mult(0);

    if (pos.x > width + w) {
      pos.x = -w;
    }
    if (pos.x < -w) {
      pos.x = width+w;
    }
    if (pos.y > height+h) {
      pos.y = -h;
    }
    if (pos.y < -h) {
      pos.y = height+h;
    }

    t1 += t1s;
    t2 += t2s;
  }

  void tail(float _x, float _y) {
    fill(255);
    float tailLength = _x;
    for (int i=0; i<150; i++) {
      float dia = w*map(i, 0, 149, 0.15, 0.01);
      float x = tailLength + cos(0.1*i)*dia;
      float y = _y + sin(0.1*i)*dia;
      ellipse(x, y, w*0.05, w*0.05);
      tailLength-=(w*tailLengthFactor)*((noise(tTail)+0.5)*1);
      tTail+=tTailSpeed;
    }
  }

  void nose(float _x, float _y) {
    float _h = h*0.4;
    fill(255);
    ellipse(_x, _y, (w*0.3)*((noise(tNose)+0.5)*1), _h);
    fill(0);
    ellipse(_x, _y+_h*0.25, _h*0.3, _h*0.2);
    ellipse(_x, _y-_h*0.25, _h*0.3, _h*0.2);
    tNose+=tNoseSpeed;
  }

  void eyes() {
    fill(255, 0, 0);
    ellipse(w/3, h/4, w*0.2, h*0.2);
    ellipse(w/3, -h/4, w*0.2, h*0.2);
    fill(0);
    ellipse(w/3, h/4, w*0.1*sin(tEyeball), h*0.1);
    ellipse(w/3, -h/4, w*0.1*sin(tEyeball), h*0.1);
    tEyeball += 0.3;
  }
  
  void ears(){
    fill(255);
    ellipse(w/3, h/2, w*0.3, h*0.4);
    ellipse(w/3, -h/2, w*0.3, h*0.4);
  }

  void display() {
    noStroke();
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(vel.heading());
    fill(255);
    ellipse(0, 0, w, h);
    //ears();
    nose(w/2, 0);
    tail(-w/2, 0);
    eyes();
    popMatrix();
  }
}
