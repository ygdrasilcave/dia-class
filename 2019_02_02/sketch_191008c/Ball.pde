class Ball{
  PVector pos;
  PVector vel;
  PVector acc;
  float maxVel;
  float w, h;
  float mass;
  
  Ball(float _m){
    pos = new PVector(random(width), 0);
    vel = new PVector(0, 0);
    acc = new PVector(0, 0);  
    maxVel = random(3, 10);
    
    mass = _m;
    w = mass*5;
    h = w;
  }
  
  void updateForce(PVector _f){
    PVector force = PVector.div(_f, mass);
    acc.add(force);
  }
  
  void update(){
    vel.add(acc);
    pos.add(vel);
    acc.mult(0);
    
    if(pos.x > width){
      pos.x = width;
      vel.x *= -1;
    }
    if(pos.x < 0){
      pos.x = 0;
      vel.x *= -1;
    }
    if(pos.y > height){
      pos.y = height;
      vel.y *= -1;
    }
    if(pos.y < 0){
      pos.y = 0;
      vel.y *= -1;
    }
  }
  
  void display(){
    noFill();
    stroke(255);
    strokeWeight(5);
    ellipse(pos.x, pos.y, w, h);
  }
}
