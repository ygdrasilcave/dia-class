Ball[] b;
int numBalls = 10;

void setup() {
  size(1000, 1000);
  background(0);
  b = new Ball[numBalls];
  for (int i=0; i<numBalls; i++) {
    b[i] = new Ball(random(1, 30));
  }
}

float x;
float y;
float t = 0;
float tSpeed = 0.034;

void draw() {
  background(0);
  
  x = width/2 + cos(t)*400;
  y = height/2 + sin(t)*400;
  
  PVector gravity = new PVector(0, map(y, 0, height, -0.1, 0.1));
  PVector wind = new PVector(map(x, 0, width, 0.12, -0.12), 0);
  
  fill(255, 0, 0);
  noStroke();
  ellipse(x, y, 50, 50);
  t += tSpeed;

  for (int i=0; i<numBalls; i++) {
    PVector friction = PVector.mult(b[i].vel, -1);
    friction.normalize();
    friction.mult(0.05);
    b[i].updateForce(friction);
    
    b[i].updateForce(wind);
    float m = b[i].mass;
    PVector g = PVector.mult(gravity, m);
    b[i].updateForce(g);
    //b[i].updateForce(gravity);
    b[i].update();
    b[i].display();
  }
}
