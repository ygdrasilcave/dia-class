int numBalls = 200;

Ball[] b;

void setup() {
  size(2000, 2000);
  background(0);
  b = new Ball[numBalls];
  for (int i=0; i<numBalls; i++) {
    if (i%2 == 0) {
      b[i] = new Ball();
    } else {
      b[i] = new Ball(random(width), random(height), 0, 0);
    }
  }
}

void draw() {
  background(0);
  for (int i=0; i<numBalls; i++) {
    b[i].update();
    b[i].display();
  }
}
