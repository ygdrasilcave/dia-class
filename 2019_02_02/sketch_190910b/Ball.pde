class Ball {
  float x, y;
  float speedX, speedY;
  float dirX, dirY;
  float w, h;
  float t, tSpeed;
  float sDist;
  float sDistSpeed;
  float sDistDir;

  Ball() {
    w = random(5, 50);
    h = w;
    x = random(w/2, width-w/2);
    y = random(h/2, height-h/2);
    speedX = random(2, 5);
    speedY = random(3, 8);
    dirX = 1;
    dirY = 1;
    t = 0;
    tSpeed = random(0.02, 0.089);
    sDist = w;
    sDistSpeed = random(1, 3);
    sDistDir = 1;
  }

  Ball(float _x, float _y, float _sx, float _sy) {
    w = random(5, 50);
    h = w;
    x = _x;
    y = _y;
    speedX = _sx;
    speedY = _sy;
    dirX = 1;
    dirY = 1;
    t = 0;
    tSpeed = random(0.02, 0.089);
    sDist = w;
    sDistSpeed = random(1, 3);
    sDistDir = 1;
  }

  void update() {
    x = x + dirX*speedX;
    y = y + dirY*speedY;   

    if (x > width-w/2) {
      dirX = dirX*-1;
    } else if (x < w/2) {
      dirX = dirX*-1;
    }
    if (y > height-h/2) {
      dirY = dirY*-1;
    } else if (y < h/2) {
      dirY = dirY*-1;
    }
  }

  float[] updateSatellite() {
    float[] val;
    val = new float[2];
    val[0] = x + cos(t)*sDist;
    val[1] = y + sin(t)*sDist;   
    t += tSpeed;
    sDist = sDist + sDistDir*sDistSpeed;
    if (sDist > w*2) {
      sDistDir *= -1;
    }
    if (sDist < 0) {
      sDistDir *= -1;
    }
    return(val);
  }

  void display() {
    fill(255, 255, 255);
    noStroke();
    ellipse(x, y, w, h);
    float _tempX = updateSatellite()[0];
    float _tempY = updateSatellite()[1];
    ellipse(_tempX, _tempY, w/3, w/3);
    stroke(255);
    strokeWeight(1);
    line(x, y, _tempX, _tempY);
  }
}
