import oscP5.*;
import netP5.*;
OscP5 oscP5;
NetAddress myRemoteLocation;

int xpos = 0;
int ypos = 0;
int currentTime = 0;
int duration = 10;

PImage img;

int rectX = 0;
int rectY = 0;
int colorR = 0;
int colorG = 0;
int colorB = 0;

int pixelSize = 20;

boolean start = false;
boolean finish = false;

void setup() {
  size(800, 800);
  oscP5 = new OscP5(this, 12001);
  myRemoteLocation = new NetAddress("localhost", 12000);
  img = loadImage("flower2.jpg");
  currentTime = millis();
  background(0);  
  image(img, 0, 0);
}

void draw() {
if (start == true) {
    if (finish == false) {
      if (millis() > currentTime + duration) {
        xpos+=pixelSize;
        if (xpos >= width) {
          xpos = 0;
          ypos+=pixelSize;
          if (ypos >= height) {
            ypos = 0;
            xpos = 0;
            finish = true;
          }
        }
        int index = xpos + ypos*width;
        color c = img.pixels[index];
        int r = int(red(c));
        int g = int(green(c));
        int b = int(blue(c));
        sendOSCMsg(xpos, ypos, r, g, b);
        currentTime = millis();
      }
    }else{
      loadPixels();
      if (millis() > currentTime + duration) {
        xpos+=pixelSize;
        if (xpos >= width) {
          xpos = 0;
          ypos+=pixelSize;
          if (ypos >= height) {
            ypos = 0;
            xpos = 0;
          }
        }
        int index = xpos + ypos*width;
        color c = pixels[index];
        int r = int(red(c));
        int g = int(green(c));
        int b = int(blue(c));
        sendOSCMsg(xpos, ypos, r, g, b);
        currentTime = millis();
      }
      updatePixels();
    }
    noStroke();
    fill(colorR, colorG, colorB);
    rect(rectX, rectY, pixelSize, pixelSize);
  }
}

void sendOSCMsg(int _x, int _y, int _r, int _g, int _b) {
  OscMessage myMessage = new OscMessage("/image"); 
  myMessage.add(_x);
  myMessage.add(_y);
  myMessage.add(_r);
  myMessage.add(_g);
  myMessage.add(_b);
  oscP5.send(myMessage, myRemoteLocation);
}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/image")==true) {
    if (theOscMessage.checkTypetag("iiiii")) {
      rectX = theOscMessage.get(0).intValue();  
      rectY = theOscMessage.get(1).intValue();
      colorR = theOscMessage.get(2).intValue();
      colorG = theOscMessage.get(3).intValue();
      colorB = theOscMessage.get(4).intValue();
    }else if(theOscMessage.checkTypetag("s")){
      String s = theOscMessage.get(0).stringValue(); 
      s = s.trim();
      if(s.equals("start")){
        start = true;
        currentTime = millis();
      }
      
    }
  }
}
