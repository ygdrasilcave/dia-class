ArrayList<Animal> ani;

void setup() {
  size(1000, 1000);
  ani = new ArrayList<Animal>();
  rectMode(CENTER);
}

void draw() {
  background(0);

  for (int i=0; i<ani.size(); i++) {
    ani.get(i).update();
    ani.get(i).display();
  }
}

void mousePressed() {
  ani.add(new Quad(mouseX, mouseY));
}

void keyPressed() {
  if (key == ' ') {
    ani.add(new Animal(width/2, height/2));
  }
  if (key == 'b') {
    ani.add(new Bird(width/2, height/2));
  }
}
