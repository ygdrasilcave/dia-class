class Quad extends Animal{
  
  Quad(float _x, float _y){
    super(_x, _y);
  }
  
  void display(){
    fill(255);
    noStroke();
    rect(pos.x + 20, pos.y - 20, 5, 20);
    rect(pos.x - 20, pos.y - 20, 5, 20);
    rect(pos.x + 20, pos.y + 20, 5, 20);
    rect(pos.x - 20, pos.y + 20, 5, 20);
    ellipse(pos.x, pos.y, 50, 50);
  }
}

class Bird extends Animal{
  
  Bird(float _x, float _y){
    super(_x, _y);
  }
  
  void display(){
    fill(255);
    noStroke();
    ellipse(pos.x, pos.y - 50, 10, 30);
    ellipse(pos.x, pos.y + 50, 10, 30);
    rect(pos.x, pos.y, 50, 50);
  }
}
