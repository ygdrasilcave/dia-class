void setup() {
  size(1000, 1000);
  background(0);
}

void draw() {
  background(0);
  drawColorRect("RED", 0, 0, substract(100, 50), 300);
  drawColorRect("YELLOW", 300, 0, 300, 300);
  drawBlueRect();
  println(substract(10, 6));
}


void drawColorRect(String _color, float _x, float _y, float _w, float _h) {
  if (_color.equals("red") || _color.equals("RED") || _color.equals("Red")) {
    fill(255, 0, 0);
  }else if(_color.equals("yellow") || _color.equals("Yellow") || _color.equals("YELLOW")) {
    fill(255, 255, 0);
  }else{
    fill(255, 255, 255);
  }
  rect(_x, _h, _w, _h);
}

void drawBlueRect() {
  fill(0, 0, 255);
  rect(width/2-150, height/2-150, 300, 300);
}

float substract(float _a, float _b){
  return (_a - _b);
}
