class Face{
  float x, y, w, h;
  
  Face(){
    x = width/2;
    y = height/2;
    w = 1000;
    h = 1500;
  }
  
  void eyes(float _ex, float _ey, float _ew, float _eh){
    pushMatrix();
    translate(x-_ex, y+_ey);
    rotate(radians(25));
    noFill();
    stroke(0);
    strokeWeight(5);
    arc(0, 0, _ew, _eh, radians(180), radians(360));
    popMatrix();
    
    pushMatrix();
    translate(x+_ex, y+_ey);
    rotate(radians(-25));
    noFill();
    stroke(0);
    strokeWeight(5);
    arc(0, 0, _ew, _eh, radians(180), radians(360));
    popMatrix();
  }
  
  void nose(float _nx, float _ny, float _nw, float _nh){
    ellipse(x+_nx, y+_ny, _nw, _nh);
  }
  
  void lips(float _lx, float _ly, float _lw, float _lh){
    noFill();
    stroke(0);
    strokeWeight(5);
    arc(x+_lx, y+_ly, _lw, _lh, radians(0), radians(360));
  }
  
  void display(){
    fill(255);
    ellipse(x, y, w, h);
    eyes(200, -200, 300, 100);
    nose(0,0,50, 200);
    lips(0, 300, 500, 100);
    
  }

}
