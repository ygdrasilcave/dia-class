class Bigbang {
  float w = 0;
  float h = 0;
  float whiteVal = 0;
  float rotateVal = 0.0;

  Ani ani_w;
  Ani ani_h;
  Ani ani_white;
  Ani ani_rotate;

  boolean b_type = true;
  int easingW = 0;
  int easingH = 0;

  boolean bwend = false;
  boolean bhend = false;
  boolean bw = true;
  boolean bh = true;

  Bigbang() {
    ani_w = new Ani(this, 2.5, "w", width, easings[easingW], "onEnd:ani_w_end");
    ani_h = new Ani(this, 2.5, "h", height, easings[easingH], "onEnd:ani_h_end");
    ani_white = new Ani(this, 2.5, "whiteVal", 255, easings[0]);
    ani_rotate = new Ani(this, 2.5, "rotateVal", TWO_PI, easings[0]);
  }

  void display() {
    if (bwend == true && bhend == true) {
      //saveFrame("myWork_####.jpg");
      /*easing_index++;    
      if (easing_index > easings.length-1) {
        easing_index = 0;
      }*/
      fill(0);
      noStroke();
      rect(width/2, height/2, width, height);
      bwend = false;
      bhend = false;

      easingW = int(random(0, 31));
      ani_w.setEasing(easings[easingW]);
      float wDuration = random(3, 10);
      float hDuration = random(3, 10);
      ani_w.setDuration(wDuration);
      if (bw == true) {
        ani_w.setBegin(0);
        ani_w.setEnd(width);
        ani_w.start();
      } else {
        ani_w.setBegin(width);
        ani_w.setEnd(0);
        ani_w.start();
      }
      easingH = int(random(0, 31));
      ani_h.setEasing(easings[easingH]);
      ani_h.setDuration(hDuration);
      if (bh == true) {
        ani_h.setBegin(0);
        ani_h.setEnd(height);
        ani_h.start();
      } else {
        ani_h.setBegin(height);
        ani_h.setEnd(0);
        ani_h.start();
      }
      if (wDuration > hDuration) {
        ani_white.setDuration(wDuration);
        ani_rotate.setDuration(wDuration);
      } else {
        ani_white.setDuration(hDuration);
        ani_rotate.setDuration(hDuration);
      }
      ani_white.start();
      ani_rotate.setBegin(random(TWO_PI));
      ani_rotate.setEnd(random(TWO_PI));
      ani_rotate.start();
      
    }

    noFill();
    stroke(whiteVal);
    strokeWeight(1.5);
    
    pushMatrix();
    translate(width/2, height/2);
    rotate(rotateVal);
    ellipse(0, 0, w, h); 
    rect(0, 0, w-200, h-200);
    
    //text("hello", 0, 0);
    
    popMatrix();

    textAlign(LEFT, CENTER);
    textSize(48);
    noStroke();
    fill(255);
    text(easingsVariableNames[easingH], 100, 100);
    text(easingsVariableNames[easingW], 100, 150);
    text(ani_w.getDuration()+": "+bw, 100, 200);
    text(ani_h.getDuration()+": "+bh, 100, 250);
    text(nf(ani_rotate.getBegin(), 1, 3)+" -> "+nf(ani_rotate.getEnd(), 1, 3), 100, 300);
  }

  void ani_w_end() {
    float _b = random(0, 1);
    if (_b > 0.5) {
      bw = true;
    } else {
      bw = false;
    }
    bwend = true;
  }

  void ani_h_end() {
    float _b = random(0, 1);
    if (_b > 0.5) {
      bh = true;
    } else {
      bh = false;
    }
    bhend = true;
  }
}
