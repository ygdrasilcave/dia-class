import processing.sound.*;

TriOsc triOsc;
SinOsc sine;

Env env; 

// Times and levels for the ASR envelope
float attackTime = 0.001;
float sustainTime = 0.12;
float sustainLevel = 0.5;
float releaseTime = 0.2;

// This is an octave in MIDI notes.
int[] midiSequence = {67, 67, 69, 69, 67, 67, 64, 67, 67, 64, 64, 62};

int noteValue = 60;

// Play a new note every 200ms
int duration = 200;

// This variable stores the point in time when the next note should be triggered
int trigger = millis(); 

// An index to count up the notes
int note = 0; 

void setup() {
  size(640, 360);
  background(255);

  //midiSequence = new int[50];
  /*for(int i=0; i<50; i++){
   midiSequence[i] = int(random(60, 80));
   }*/


  // Create triangle wave and start it
  triOsc = new TriOsc(this);
  sine = new SinOsc(this);

  // Create the envelope 
  env = new Env(this);
}

void draw() {
  
  
} 

void playTone() {
  triOsc.play(midiToFreq(noteValue), 0.5);
  sine.play(midiToFreq(noteValue - 12), 0.6);
  env.play(triOsc, attackTime, sustainTime, sustainLevel, releaseTime);
  env.play(sine, attackTime, sustainTime, sustainLevel, releaseTime);
}

// This helper function calculates the respective frequency of a MIDI note
float midiToFreq(int note) {
  return (pow(2, ((note-69)/12.0))) * 440;
}

void keyPressed() {
  if (key == 'a') {
    noteValue = 82;
    playTone();
  }
  if (key == 's') {
    noteValue = 62;
    playTone();
  }
}
