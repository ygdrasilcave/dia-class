/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/

let myVoice = new p5.Speech();
let listButton;

let words = ["processing", "five", "맑아요", "안녕하세요", "반가워요", "즐거워요"];

function setup(){
  createCanvas(500, 500);
  background(0, 0, 0);
  listButton = createButton('List Voices');
  listButton.style("width", "200px");
  listButton.position(width/2-100, height-50);
  listButton.mousePressed(doList);

  myVoice.setVoice(0);
  textAlign(CENTER, CENTER);
  textSize(60);
}

function draw(){
}

function doList(){
  myVoice.listVoices();
}

function keyPressed(){
  if(key == 's' || key == 'S'){
    background(0);
    var index = int(random(words.length));
    console.log(index);
    var word = words[index];
    fill(255);
    text(word, width/2, height/2);
    myVoice.speak(word);
  }
}


function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

