/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/
let bee;
let spider;
let centipede;

let insects = [];
let ix = [];
let iy = [];
let ir = [];

/*let t = 10;
let tSpeed = 1;
let tSpeedDir = 1;*/

function Insect(_body, _leg, _tMin, _tMax, _type){
  this.body = _body;
  this.leg = _leg;
  this.size = random(5, 15);
  this.t = 5;
  this.tSpeed = random(0, 0.5);
  this.tSpeedDir = 1;
  this.tMin = _tMin;
  this.tMax = _tMax;
  this.color = random(255);
  this.type = _type;

  this.display = function(_x, _y, _r){
    push();

    translate(_x, _y);
    rotate(_r);
    
    stroke(0);
    strokeWeight(1);
    for(var i=0; i<this.leg/2; i++){
      var theta = this.t*(i-int((this.leg/2)/2));
      var x1 = cos(PI*0.5 + radians(theta))*((this.size/2)*this.body) + this.size*int(this.body/2);
      var y1 = sin(PI*0.5 + radians(theta))*(this.size*0.5);
      var x2 = cos(PI*0.5 + radians(theta))*((this.size/2)*this.body*1.1) + this.size*int(this.body/2);
      var y2 = sin(PI*0.5 + radians(theta))*(this.size*1.8);
      line(x1, y1, x2, y2);
      if(this.type == true){
        ellipse(x2, y2, this.size*0.3, this.size*0.3);
      }
    }
    for(var i=0; i<this.leg/2; i++){
      var theta = this.t*(i-int((this.leg/2)/2));
      var x1 = cos(PI*1.5 + radians(theta))*((this.size/2)*this.body) + this.size*int(this.body/2);
      var y1 = sin(PI*1.5 + radians(theta))*(this.size*0.5);
      var x2 = cos(PI*1.5 + radians(theta))*((this.size/2)*this.body*1.1) + this.size*int(this.body/2);
      var y2 = sin(PI*1.5 + radians(theta))*(this.size*1.8);
      line(x1, y1, x2, y2);
      if(this.type == true){
        ellipse(x2, y2, this.size*0.3, this.size*0.3);
      }
    }

    stroke(0);
    strokeWeight(1);
    fill(this.color);
    for(var i=0; i<this.body; i++){
      ellipse(i*this.size, 0, this.size, this.size);
    }
    if(this.type == true){
      fill(255, 0, 0);
      noStroke();
      for(var i=0; i<this.body; i++){
        ellipse(i*this.size, 0, this.size*0.3, this.size*0.3);
      }
    }

    pop();

    this.t = this.t + this.tSpeed*this.tSpeedDir;
    if(this.t > this.tMax){
      this.tSpeedDir *= -1;
    }
    if(this.t < this.tMin){
      this.tSpeedDir *= -1;
    }
  }
}

/*function ant(_x, _y, _r){
  push();
  translate(_x, _y);
  rotate(_r);
  noStroke();
  fill(0);
  for(var i=0; i<3; i++){
    ellipse(i*50, 0, 50, 50);
  }
  
  stroke(0);
  strokeWeight(2);
  for(var i=-1; i<2; i++){
    var x1 = cos(PI*0.5 + radians(t*i))*40 + 50;
    var y1 = sin(PI*0.5 + radians(t*i))*30;
    var x2 = cos(PI*0.5 + radians(t*i))*80 + 50;
    var y2 = sin(PI*0.5 + radians(t*i))*70;
    line(x1, y1, x2, y2);
  }
  for(var i=-1; i<2; i++){
    var x1 = cos(PI*1.5 + radians(t*i))*40 + 50;
    var y1 = sin(PI*1.5 + radians(t*i))*30;
    var x2 = cos(PI*1.5 + radians(t*i))*80 + 50;
    var y2 = sin(PI*1.5 + radians(t*i))*70;
    line(x1, y1, x2, y2);
  }
  pop();

  t = t + tSpeed*tSpeedDir;
  if(t > 45){
    tSpeedDir *= -1;
  }
  if(t < 10){
    tSpeedDir *= -1;
  }
}*/

let x1, x2, x3;
let y1, y2, y3;
let r1, r2, r3;

function setup(){
  createCanvas(1000, 1000);
  background(255);
  bee = new Insect(3, 6, 5, 30, true);
  spider = new Insect(5, 14, 2, 15, true);
  centipede = new Insect(15, 42, 4, 8, true);

  var margin = 50;

  for(var i=0; i<80; i++){
    var bd = int(random(3, 22));
    if(bd%2 == 0){
      bd = bd+1;
    }else{
      bd = bd;
    }
    var lg = int(random(6, 62));
    if((lg/2)%2==0){
      lg = ((lg/2)+1)*2;
    }else{
      lg = lg;
    }
    var mn = random(2, 5);
    var mx = mn + random(20);

    var tp;
    if(random(10) > 5){
      tp = true;
    }else{
      tp = false;
    }

    insects[i] = new Insect(bd, lg, mn, mx, tp);
    ix[i] = random(margin, width-margin);
    iy[i] = random(margin, height-margin);
    ir[i] = radians(random(360));
  }

  x1 = random(margin, width-margin);
  x2 = random(margin, width-margin);
  x3 = random(margin, width-margin);
  y1 = random(margin, height-margin);
  y2 = random(margin, height-margin);
  y3 = random(margin, height-margin);
  r1 = radians(random(360));
  r2 = radians(random(360));
  r3 = radians(random(360));
}

function draw(){
  background(220);
  /*ant(width/2, height/2, radians(map(mouseX, 0, width, 0, 360)));
  ant(width/2+300, height/2, radians(map(mouseX, 0, width, 0, 360)));
  ant(width/2-300, height/2, radians(map(mouseX, 0, width, 0, 360)));*/
  bee.display(x1, y1, r1);
  spider.display(x2, y2, r2);
  centipede.display(x3, y3, r3);

  for(var i=0; i<insects.length; i++){
    insects[i].display(ix[i], iy[i], ir[i]);
  }
}


function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

