var myVoice = new p5.Speech(); // new P5.Speech object

//myVoice.onLoad = speechLoaded; // could do it this way
myVoice.onStart = speechStarted;
//myVoice.onPause = speechPaused; // not working
//myVoice.onResume = speechResumed; // not working
myVoice.onEnd = speechEnded;

//var lyric = "안녕 하세요, 반갑습니다.";
var lyric = "The history of textbooks dates back to ancient civilizations. For example, Ancient Greeks wrote educational texts. The modern textbook has its roots in the mass production made possible by the printing press. Johannes Gutenberg himself may have printed editions of Ars Minor, a schoolbook on Latin grammar by Aelius Donatus. Early textbooks were used by tutors and teachers (e.g. alphabet books), as well as by individuals who taught themselves. The Greek philosopher Socrates lamented the loss of knowledge because the media of transmission were changing. Before the invention of the Greek alphabet 2,500 years ago, knowledge and stories were recited aloud, much like Homer's epic poems. The new technology of writing meant stories no longer needed to be memorized, a development Socrates feared would weaken the Greeks' mental capacities for memorizing and retelling. (Ironically, we know about Socrates' concerns only because they were written down by his student Plato in his famous Dialogues.) The next revolution in the field of books came with the 15th-century invention of printing with changeable type. The invention is attributed to German metalsmith Johannes Gutenberg, who cast type in molds using a melted metal alloy and constructed a wooden-screw printing press to transfer the image onto paper. Gutenberg's first and only large-scale printing effort was the now iconic Gutenberg Bible in the 1450s — a Latin translation from the Hebrew Old Testament and the Greek New Testament. Gutenberg's invention made mass production of texts possible for the first time. Although the Gutenberg Bible itself was expensive, printed books began to spread widely over European trade routes during the next 50 years, and by the 16th century, printed books had become more widely accessible and less costly. While many textbooks were already in use, compulsory education and the resulting growth of schooling in Europe led to the printing of many more textbooks for children. Textbooks have been the primary teaching instrument for most children since the 19th century. Two textbooks of historical significance in United States schooling were the 18th century New England Primer and the 19th century McGuffey Readers. Recent technological advances have changed the way people interact with textbooks. Online and digital materials are making it increasingly easy for students to access materials other than the traditional print textbook. Students now have access to electronic books, online tutoring systems and video lectures. An example of an e-book is Principles of Biology from Nature Publishing. Most notably, an increasing number of authors are avoiding commercial publishers and instead offering their textbooks under a creative commons or other open license.";

var speakbutton; // UI

function setup()
{
  createCanvas(400, 400);

  // button:
  speakbutton = createButton('Speak');
  speakbutton.position(180, 200);
  speakbutton.mousePressed(buttonClicked);

  myVoice.listVoices();
  myVoice.setVoice(1);

  console.log(speakbutton.elt.innerHTML);
}

function draw()
{
  // why draw when you can click?
}

function buttonClicked()
{
  if(speakbutton.elt.innerHTML=='Speak') myVoice.speak(lyric);
  //else if(speakbutton.elt.innerHTML=='Pause') myVoice.pause(); // not working
  //else if(speakbutton.elt.innerHTML=='Resume') myVoice.resume(); // not working
  else if(speakbutton.elt.innerHTML=='Stop') myVoice.stop();
}

function speechLoaded()
{
  // say cheers:
  myVoice.speak("testing one two three!!!");
}

function speechStarted()
{
  console.log("started");
  background(0, 255, 0);
  //speakbutton.elt.innerHTML = 'Pause';
  speakbutton.elt.innerHTML = 'Stop';
}

/*
// not working...
function speechPaused()
{
  console.log("paused");
  background(0, 255, 0);
  speakbutton.elt.innerHTML = 'Resume';
}

// not working...
function speechResumed()
{
  console.log("resumed");
  background(0, 255, 0);
  speakbutton.elt.innerHTML = 'Pause';
}
*/

function speechEnded()
{
  console.log("stoped");
  background(255, 0, 0);
  speakbutton.elt.innerHTML = 'Speak';
}