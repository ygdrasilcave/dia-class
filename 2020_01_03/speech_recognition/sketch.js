//'en-US'
let myRec = new p5.SpeechRec('ko-KR'); // new P5.SpeechRec object
myRec.continuous = false; // turn continuous off!
myRec.onError = doError;
myRec.onEnd = doEnd;

let startButton;

function setup()
{
  // graphics stuff:
  createCanvas(800, 400);
  background(255, 255, 255);
  fill(0, 0, 0, 255);

  startButton = createButton('start');
  startButton.position(width/2, height-100);
  startButton.mousePressed(doStart);

  // instructions:
  textSize(32);
  textAlign(CENTER);
  text("say something", width/2, height/2);
  myRec.onResult = showResult;
  //myRec.start();
}

function draw()
{
  // why draw when you can talk?
}

function showResult()
{
  if(myRec.resultValue==true) {
    background(192, 255, 192);
    text(myRec.resultString, width/2, height/2);
    console.log(myRec.resultString);
  }
}

function restart(){
  myRec.start();
}

function doError(){
  background(192, 255, 192);
  text('something wrong!', width/2, height/2);
  console.log('error.....');
  restart();
}

function doStart(){
  restart();
}

function doEnd(){
  console.log('done!');
}