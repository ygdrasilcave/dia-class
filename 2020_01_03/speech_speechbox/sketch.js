var myVoice = new p5.Speech(); // new P5.Speech object

var menuLoaded = 0;
var label, input, checkbox, speakbutton, vslider, rslider, pslider; // UI

var Binterrupt = 0;

function setup()
{
  // input dialog:
  input = createInput("안녕하세요? 반갑습니다.");
  input.style("width", "500px");
  input.position(20, 65);
  // checkbox:
  checkbox = createCheckbox("interrupt?", false);
  checkbox.changed(checkboxEvent);
  //checkbox.style("width", "15px");
  //checkbox.style("height", "15px");
  checkbox.position(200, 100);
  // button:
  speakbutton = createButton('Speak');
  speakbutton.style("width", "150px");
  speakbutton.position(20, 100);
  speakbutton.mousePressed(doSpeak);
  // sliders:
  vslider = createSlider(0., 100., 100.);
  vslider.position(20, 140);
  vslider.mouseReleased(setVolume);
  rslider = createSlider(10., 200., 100.);
  rslider.position(20, 160);
  rslider.mouseReleased(setRate);
  pslider = createSlider(1., 200., 100.);
  pslider.position(20, 180);
  pslider.mouseReleased(setPitch);
  // labels:
  label = createDiv("say something:");
  label.position(20, 40);
  //label = createDiv("interrupt?");
  //label.position(125, 100);
  label = createDiv("volume");
  label.position(160, 140);
  label = createDiv("rate");
  label.position(160, 160);
  label = createDiv("pitch");
  label.position(160, 180);

    // say hello:
  myVoice.setVoice(0);
  myVoice.speak(input.value());

}

function setVolume()
{
  myVoice.setVolume(vslider.value()/100.);
}
function setRate()
{
  myVoice.setRate(rslider.value()/100.);
}
function setPitch()
{
  myVoice.setPitch(pslider.value()/100.);
}

function draw()
{
  // why draw when you can click?
}

function doSpeak()
{
  myVoice.interrupt = Binterrupt;
  myVoice.speak(input.value()); // debug printer for voice options
}

function checkboxEvent(){
  if (this.checked()) {
    Binterrupt = 1;
  } else {
    Binterrupt = 0;
  }
}