let counter = 0;
let counterSpeed = 1;

function setup() {
  createCanvas(1200, 1200);
  background(0);
}

function draw() {
  counter = counter + counterSpeed;
  if(counter >= 180){
    textSize(50);
    fill(255);
    text("NEXT", 400, 700);
    conterSpeed = 0;
  }
}