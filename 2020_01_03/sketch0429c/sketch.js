/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/

let assets = '../assets/playSource/'

//Changing the sprites' animations
//position and transformations: rotation, scale, mirror
//move the mouse and click
//press and hold the up and down keys

let ghost;

function setup() {
  createCanvas(800, 800);

  //create a sprite and add the 3 animations
  ghost = createSprite(400, 150, 50, 100);

  //label, first frame, last frame
  //the addAnimation method returns the added animation
  //that can be store in a temporary variable to change parameters
  var myAnimation = ghost.addAnimation('floating', assets+'ghost_standing0001.png', assets+'ghost_standing0007.png');
  //offX and offY is the distance of animation from the center of the sprite
  //in this case since the animations have different heights i want to adjust
  //the vertical offset to make the transition between floating and moving look better
  myAnimation.offY = 18;

  ghost.addAnimation('moving', assets+'ghost_walk0001.png', assets+'ghost_walk0004.png');

  ghost.addAnimation('spinning', assets+'ghost_spin0001.png', assets+'ghost_spin0003.png');

}

function draw() {
  background(255, 255, 0);

  var currX = ghost.position.x;
  var currY = ghost.position.y;
  var dist = sqrt((mouseX-currX)*(mouseX-currX) + (mouseY-currY)*(mouseY-currY));

  if(dist > 20){
    ghost.changeAnimation('moving');
    if(mouseX-currX > 0){
      ghost.mirrorX(1);
    }else{
      ghost.mirrorX(-1);
    }
    ghost.velocity.x = (mouseX-currX)*0.05;
    ghost.velocity.y = (mouseY-currY)*0.05;
  }else{
    ghost.changeAnimation('floating');
    ghost.velocity.x = 0;
    ghost.velocity.y = 0;
  }

  if(mouseIsPressed) {
    //the rotation is not part of the spinning animation
    ghost.rotation -= 10;
    ghost.changeAnimation('spinning');
  }
  else{
    ghost.rotation = 0;
  }

  var scaleF = map(mouseY, 0, height, 0.7, 2.0);
  ghost.scale = scaleF;

  //up and down keys to change the scale
  //note that scaling the image quality deteriorates
  //and scaling to a negative value flips the image
  /*if(keyIsDown(UP_ARROW)){
    ghost.scale += 0.05;
  }
  if(keyIsDown(DOWN_ARROW)){
    ghost.scale -= 0.05;
  }*/

  //draw the sprite
  drawSprites();
}


function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}