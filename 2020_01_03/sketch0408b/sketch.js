function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 750);
  linkN.position(400, 750);  
}

function setup() {
  // put setup code here
  createCanvas(800, 800);
  background(0);
  hyperlink();
}

function draw() {
  // put drawing code here
  background(0);

  push();
  translate(mouseX, mouseY);
  rotate(HALF_PI/2.0);
  fill(255);
  rect(-150, -150, 300, 300);
  fill(255, 255, 0);
  ellipse(0, 0, 150, 150);
  fill(0,255,0);
  rect(-25,-25,50,50);
  pop();


  //console.log(x1);
  fill(255, 0, 0);
  rect(600, 600, 150, 150);
  if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    cursor(HAND);
  }else{
    cursor(ARROW);
  }
}

function mousePressed(){
  if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }
}