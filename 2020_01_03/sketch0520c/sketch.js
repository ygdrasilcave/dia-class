/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/

let myVoice = new p5.Speech();

let y, m, d, wd, h, ms, s;
let time;
let ps;
let weekday = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
let name = ["원중아", "태영아", "민중아", "유림아", "진영아", "해든아", "유경아", "다현아", "성민아", "상범아", "제하야", "지은아"];
let doit = ["과제를 하세요", "밥을 먹어요", "잠을 자요", "영양제를 먹어요", "커피 한잔 하세요", "음악을 틀어요"];

function setup(){
  createCanvas(500, 500);
  background(0);
  myVoice.setVoice(0);
  s = second();
  ps = s;

  getTime();
  textSize(24);
  textAlign(CENTER, CENTER);
  fill(255);
  text(time, width/2, height/2);
}

function draw(){
  s = second();
  if(s%15 == 0 && s != ps){
    getTime();
    var nameIndex = int(random(12));
    var doitIndex = int(random(6));
    myVoice.speak(name[nameIndex] + "\n" + time + "\n" + doit[doitIndex]);
    console.log(name[nameIndex] + "\n" + time + "\n" + doit[doitIndex]);

    background(0);
    fill(255);
    text(name[nameIndex] + "\n" + time + "\n" + doit[doitIndex], width/2, height/2);
  }
  console.log(second());
  ps = s;
}

function getTime(){
  let now = new Date();
  y = year();
  m = month();
  d = day();
  //0 - 일요일, 1 - 월요일 ,....., 6 - 토요일
  wd = weekday[now.getDay()];
  h = hour();
  ms = minute();
  s = second();
  time = y + "년 " + m + "월 " + d + "일 " + wd + "\n" + h + "시 " + ms + "분 " + s + " 초 입니다.";
}

function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

