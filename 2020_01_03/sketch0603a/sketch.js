// Declare a "SerialPort" object
let serial;
let latestData = "waiting for data";  // you'll use this to write incoming data to the canvas

let w = 0;
let h = 0;
let b0 = 1;
let b1 = 1;
let redSlider, greenSlider, blueSlider;
let t = 0;

function setup() {
  createCanvas(600, 600);

  redSlider = createSlider(0, 255, 0);
  redSlider.position(70, 50);
  redSlider.style('width', '100px');
  redLabel = createDiv("RED");
  redLabel.position(10, 50);
  redLabel.style('color', 'white');

  greenSlider = createSlider(0, 255, 0);
  greenSlider.position(70, 75);
  greenSlider.style('width', '100px');
  greenLabel = createDiv("GREEN");
  greenLabel.position(10, 75);
  greenLabel.style('color', 'white');

  blueSlider = createSlider(0, 255, 0);
  blueSlider.position(70, 100);
  blueSlider.style('width', '100px');
  blueLabel = createDiv("BLUE");
  blueLabel.position(10, 100);
  blueLabel.style('color', 'white');


  // Instantiate our SerialPort object
  serial = new p5.SerialPort();

  // Get a list the ports available
  // You should have a callback defined to see the results
  serial.list();

  // Assuming our Arduino is connected, let's open the connection to it
  // Change this to the name of your arduino's serial port
  //serial.open("/dev/tty.usbmodem141101");
  serial.open("COM6");

  // Here are the callbacks that you can register
  // When we connect to the underlying server
  serial.on('connected', serverConnected);

  // When we get a list of serial ports that are available
  serial.on('list', gotList);
  // OR
  //serial.onList(gotList);

  // When we some data from the serial port
  serial.on('data', gotData);
  // OR
  //serial.onData(gotData);

  // When or if we get an error
  serial.on('error', gotError);
  // OR
  //serial.onError(gotError);

  // When our serial port is opened and ready for read/write
  serial.on('open', gotOpen);
  // OR
  //serial.onOpen(gotOpen);

  serial.on('close', gotClose);

  // Callback to get the raw data, as it comes in for handling yourself
  //serial.on('rawdata', gotRawData);
  // OR
  //serial.onRawData(gotRawData);
}

// We are connected and ready to go
function serverConnected() {
  print("Connected to Server");
}

// Got the list of ports
function gotList(thelist) {
  print("List of Serial Ports:");
  // theList is an array of their names
  for (let i = 0; i < thelist.length; i++) {
    // Display in the console
    print(i + " " + thelist[i]);
  }
}

// Connected to our serial device
function gotOpen() {
  print("Serial Port is Open");
}

function gotClose(){
    print("Serial Port is Closed");
    latestData = "Serial Port is Closed";
}

// Ut oh, here is an error, let's log it
function gotError(theerror) {
  print(theerror);
}

// There is data available to work with from the serial port
function gotData() {
  let currentString = serial.readLine();  // read the incoming string
  trim(currentString);                    // remove any trailing whitespace
  if (!currentString) return;             // if the string is empty, do no more
  //console.log(currentString);             // print the string
  latestData = currentString;            // save it for the draw method
}

// We got raw from the serial port
function gotRawData(thedata) {
  print("gotRawData" + thedata);
}

// Methods available
// serial.read() returns a single byte of data (first in the buffer)
// serial.readChar() returns a single char 'A', 'a'
// serial.readBytes() returns all of the data available as an array of bytes
// serial.readBytesUntil('\n') returns all of the data available until a '\n' (line break) is encountered
// serial.readString() retunrs all of the data available as a string
// serial.readStringUntil('\n') returns all of the data available as a string until a specific string is encountered
// serial.readLine() calls readStringUntil with "\r\n" typical linebreak carriage return combination
// serial.last() returns the last byte of data from the buffer
// serial.lastChar() returns the last byte of data from the buffer as a char
// serial.clear() clears the underlying serial buffer
// serial.available() returns the number of bytes available in the buffer
// serial.write(somevar) writes out the value of somevar to the serial device

function draw() {
  background(0);

  noStroke();
  fill(255);
  text(latestData, 10, 20);

  var data = split(latestData, ",");
  //console.log(data[0]);
  if(data[0] == "H" && data.length > 4){
    //for(var i=1; i<data.length; i++){
    //  text(data[i], 100, 100*i);
    //}
    w = int(data[1]);
    h = int(data[2]);
    b0 = int(data[3]);
    b1 = int(data[4]);
  }

  var r = int(redSlider.value());
  var g = int(greenSlider.value());
  var b = int(blueSlider.value());

  if(b0 == 1){
    stroke(r, g, b);
    noFill();
  }else{
    noStroke();
    fill(r, g, b);
  }
  if(b1 == 1){
    ellipse(width/2, height/2, w, h);
  }else{
    rect(width/2-w/2, height/2-h/2, w, h);
  }

  g = map(sin(t), -1, 1, 0, 1);
  g *= 255;
  t += 0.036;

  g = int(g);

  var v = r + " " + g + " " + b + "\n";
  serial.write(v);
}

/*
const int pot0 = A0;
const int pot1 = A1;
const int rPin = 9;
const int gPin = 5;
const int bPin = 6;
const int sw0 = 7;
const int sw1 = 8;

const int dataNum = 3;
int index = 0;
int rawData[dataNum];
const int minVal = 0;
const int maxVal = 255;

void setup() {
  pinMode(rPin, OUTPUT);
  pinMode(gPin, OUTPUT);
  pinMode(bPin, OUTPUT);
  pinMode(sw0, INPUT_PULLUP);
  pinMode(sw1, INPUT_PULLUP);

  for (int i = 0; i < dataNum; i++) {
    rawData[i] = 0;
  }
  Serial.begin(9600);

  analogWrite(rPin, 0);
  analogWrite(rPin, 0);
  analogWrite(rPin, 0);
  analogWrite(rPin, 255);
  delay(1000);
  analogWrite(rPin, 0);
  analogWrite(gPin, 255);
  delay(1000);
  analogWrite(gPin, 0);
  analogWrite(bPin, 255);
  delay(1000);
  analogWrite(bPin, 0);
}

void loop() {
  int potVal0 = analogRead(pot0) / 4;
  int potVal1 = analogRead(pot1) / 4;
  int swVal0 = digitalRead(sw0);
  int swVal1 = digitalRead(sw1);

  Serial.print("H");
  Serial.print(",");
  Serial.print(potVal0);
  Serial.print(",");
  Serial.print(potVal1);
  Serial.print(",");
  Serial.print(swVal0);
  Serial.print(",");
  Serial.print(swVal1);
  Serial.println();

  analogWrite(rPin, rawData[0]);
  analogWrite(gPin, rawData[1]);
  analogWrite(bPin, rawData[2]);
}

void serialEvent() {
  if (index == 0) {
    for (index; index < dataNum; index ++)
    {
      rawData[index] = Serial.parseInt();
      rawData[index] = constrain(rawData[index], minVal, maxVal);
    }
  }
  if (Serial.read() == '\n' && index == dataNum) {
    index = 0;
    //delay(10);
  }
}
*/