/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/

let myVoice = new p5.Speech();

let input;
let speakButton;
let volumeSlider, pitchSlider, rateSlider;
let checkBox, interruptVal;

function setup(){
  createCanvas(500, 500);
  background(160);

  input = createInput("메세지를 남기세요");
  input.style("width", "400px");
  input.position(50, 50);

  speakButton = createButton("speak");
  speakButton.style("width", "150px");
  speakButton.position(50, 100);
  speakButton.mousePressed(doSpeak);

  volumeSlider = createSlider(0.0, 1.0, 1.0, 0.05);
  volumeSlider.position(50, 150);
  volumeSlider.mouseReleased(setVolume);

  pitchSlider = createSlider(0.01, 2.0, 1.0, 0.01);
  pitchSlider.position(50, 200);
  pitchSlider.mouseReleased(setPitch);

  rateSlider = createSlider(0.1, 2.0, 1.0, 0.01);
  rateSlider.position(50, 250);
  rateSlider.mouseReleased(setRate);

  checkBox = createCheckbox("interrupt", false);
  checkBox.position(250, 100);
  checkBox.changed(changeInterrupt);

  myVoice.setVoice(0);

  textSize(12);
  fill(0);
  text("volume", 200, 165);
  text("pitch", 200, 215);
  text("rate", 200, 265);
}

function doSpeak(){
  myVoice.interrupt = interruptVal;
  myVoice.speak(input.value());
}

function setVolume(){
  //console.log(volumeSlider.value());
  myVoice.setVolume(volumeSlider.value());
}

function setPitch(){
  //console.log(pitchSlider.value());
  myVoice.setPitch(pitchSlider.value());
}

function setRate(){
  //console.log(rateSlider.value());
  myVoice.setRate(rateSlider.value());
}

function changeInterrupt(){
  if(this.checked() == true){
    interruptVal = true;
  }else{
    interruptVal = false;
  }
}

function draw(){
  //console.log(volumeSlider.value());
}


function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

