/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/

//'en-US'
let myRec = new p5.SpeechRec('ko-KR');

let startButton;

function setup(){
  createCanvas(500, 500);
  background(0);

  startButton = createButton('start');
  startButton.style("width", "100px")
  startButton.position(width/2-50, height-100);
  startButton.mousePressed(doStart);

  textSize(32);
  textAlign(CENTER, CENTER);

  fill(255);
  text("말씀하세요", width/2, height/2);

  myRec.onResult = showResult;
  myRec.onError = doError;
  myRec.onEnd = doEnd;
}

function draw(){

}

function showResult(){
  if(myRec.resultValue == true){
    background(0);
    fill(255);

    var saySomething = myRec.resultString;

    text(saySomething + "\n라구요?", width/2, height/2);
    console.log(saySomething + "\n라구요?");

    if("밥 먹을까" == saySomething){
      console.log("좋습니다.");
    }
  }
}

function doStart(){
  myRec.start();
}

function doError(){
  background(0);
  fill(255);
  text("못알아 듣겠어요.", width/2, height/2);
  console.log("error.....");
}

function doEnd(){
  console.log("done!");
}

function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

