let myVoice = new p5.Speech();
let time;
let y, m, d, wd, h, ms, s;
let weekday = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];

let ps;
let toggle = true;

function setup() {
  createCanvas(720, 400);
  myVoice.setVoice(0);
  //myVoice.speak("안녕?");
  getTime();
  ps = s;
}

function draw() {
  background(0);

  s = second();

  /*if(ps != s){
    getTime();
    ps = s;
  }*/

  if(s%30 == 0 && toggle == true){
    getTime();
    myVoice.speak(time);
    toggle = false;
  }else if(s%30 == 15 && toggle == false){
    toggle = true;
  }

  fill(255);
}

function getTime(){
  let now = new Date();
  y = year();
  m = month();
  d = day();
  wd = weekday[now.getDay()];
  h = hour();
  ms = minute();
  s = second();
  time = y + "년 " + m + "월 " + d + "일 " + wd + " " + h + "시 " + ms + "분 " + str(s) + "초 입니다.";
  console.log("Now: " + time);
}