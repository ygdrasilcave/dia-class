let img, bee1, bee2, bee3;
let currentTime, count;
let showBee;
let slider;
let bee_buzz;

function preload(){
  img = loadImage('../assets/flower.jpg');
  bee1 = loadImage('../assets/bee_front.png');
  bee2 = loadImage('../assets/bee_front2.png');
  bee3 = loadImage('../assets/bee_front3.png');
  bee_buzz = loadSound('../assets/bee_buzz.wav');
}

function setup() {
  // put setup code here
  createCanvas(1000, 750);
  background(0);
  currentTime = millis();
  count = 0;
  showBee = 0;

  slider = createSlider(100, 500, 250);
  slider.position(20, 20);
  slider.style('width', '300px');

  getAudioContext().suspend();
  bee_buzz.loop();
  bee_buzz.pause();
}


function draw() {
  // put drawing code here
  //background(0);
  var w = width/5;
  var h = height/5;
  image(img, 0, 0, width, height);

  //if(frameCount % 2 == 0){
  var frameDuratnion = slider.value();
  if(currentTime + frameDuratnion < millis()){
    showBee++;
    if(showBee > 2){
      showBee = 0;
    }
    currentTime = millis();
  }

  if(showBee == 0){
    image(bee1, mouseX-bee1.width/2, mouseY-bee1.height/2, bee1.width, bee1.height);
  }else if(showBee == 1){
    image(bee2, mouseX-bee2.width/2, mouseY-bee2.height/2, bee2.width, bee2.height);
  }else if(showBee == 2){
    image(bee3, mouseX-bee3.width/2, mouseY-bee3.height/2, bee3.width, bee3.height);
  }
}

function mousePressed(){

  if (getAudioContext().state !== 'running') {
    userStartAudio();
  }

  if(bee_buzz.isPlaying() == true){
    bee_buzz.pause();
  }else{
    bee_buzz.play();
  }


}