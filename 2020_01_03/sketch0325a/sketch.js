function setup() {
  // put setup code here
  createCanvas(800, 800);
  background(110);
}

function draw() {
  // put drawing code here
  fill(255, 204, 0);
  noStroke();
  ellipse(400, 400, 200, 200);

  stroke(0);
  strokeWeight(5);
  noFill();
  rect(400, 400, 200, 200);

  stroke(0, 255, 100);
  strokeWeight(1);
  line(255, 0, 255, 800);

  console.log(mouseX);
}