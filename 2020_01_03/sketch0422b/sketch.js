function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}

let ghost, asterisk;
let assets = '../assets/playSource/'

function preload() {
  ghost = loadAnimation(assets+'ghost_standing0001.png', assets+'ghost_standing0007.png');
  asterisk = loadAnimation(assets+'asterisk.png', assets+'triangle.png', assets+'square.png', assets+'cloud.png', assets+'star.png', assets+'mess.png', assets+'monster.png');
}

function setup() {
  createCanvas(800, 300);
  console.log(asterisk.frameDelay);
  asterisk.frameDelay = 30;
  ghost.frameDelay = 4;
}

function draw() {
  background(255, 255, 0);
  animation(ghost, mouseX, mouseY);
  animation(asterisk, 500, 150);
}


function mousePressed(){
  if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }
}