/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/
let x1 = -10;
let x2 = 10;
let x3 = 35;
let x4 = 18;
let x5 = 30;

let mouth = 0;
let mouthT = 0;

function setup() {
  createCanvas(800, 800);
  noStroke();
}

function draw() {
  background(0, 0, 0);
  x1 += 1;
  x2 += 2;
  x3 += 1.5;
  x4 += 2;
  x5 += 0.5;
  if(x1 > width){
    x1 = 0;
  }
  if(x2 > width){
    x2 = 0;
  }
  if(x3 > width){
    x3 = 0;
  }
  if(x4 > width){
    x4 = 0;
  }
  if(x5 > width){
    x5 = 0;
  }
  mouth = abs(sin(mouthT));
  mouthT += 0.132;
  let start = radians(mouth*45);
  let end = radians(360-mouth*45);
  arc(x1, 20, 20, 20, start, end);
  arc(x2, 40, 20, 20, start, end);
  arc(x3, 60, 20, 20, start, end);
  arc(x4, 80, 20, 20, start, end);
  arc(x5, 100, 20, 20, start, end);
}


function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

