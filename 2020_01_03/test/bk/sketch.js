var ball = {};
var soundFile;

function preload() {
  soundFormats('mp3', 'ogg');
  soundFile = loadSound('../assets/beatbox.ogg');
}

function setup() {
  createCanvas(710, 100);
}

function draw() {
  background(0);
  ball.x = constrain(mouseX, 0, width);
  fill(255, 255, 0);
  ellipse(ball.x, height / 2, 100, 100);
}

function mousePressed() {
  // map the ball's x location to a panning degree
  // between -1.0 (left) and 1.0 (right)
  let panning = map(ball.x, 0, width, -1.0, 1.0);
  soundFile.pan(panning);
  soundFile.play();
  console.log(ball.x);
}