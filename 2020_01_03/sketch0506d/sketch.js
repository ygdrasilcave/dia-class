/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/
let x = [];
let speedX = [];

let mouth = [];
let mouthT = [];
let mouthTSpeed = [];
let mouthMax = [];

function setup() {
  createCanvas(800, 800);
  noStroke();
  
  for(var i=0; i<40; i++){
    x[i] = random(-20, 0);
    speedX[i] = random(0.5, 4);
    mouth[i] = 0;
    mouthT[i] = 0;
    mouthTSpeed[i] = random(0.023, 0.18);
    mouthMax[i] = random(10, 90);
  }
  console.log(x);
}

function draw() {
  background(0, 0, 0);

  for(var i=0; i<x.length; i++){
    mouth[i] = abs(sin(mouthT[i]));
    mouthT[i] += mouthTSpeed[i];
    let start = radians(mouth[i]*mouthMax[i]);
    let end = radians(360-mouth[i]*mouthMax[i]);

    x[i] = x[i] + speedX[i];
    if(x[i] > width){
      x[i] = random(-20, 0);
      speedX[i] = random(0.5, 4);
    }
    arc(x[i], i*20+20, 20, 20, start, end);
  }
}


function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/
}

