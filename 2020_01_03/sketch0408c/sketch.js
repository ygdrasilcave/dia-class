function setup() {
  // put setup code here
  createCanvas(800, 800);
  background(0);
}

let angle = 0;
let sf = 0;
let sft = 0;

function draw() {
  // put drawing code here
  //background(0);
  fill(0, 15);
  noStroke();
  rect(0,0,width,height);

  push();
  translate(mouseX, mouseY);
  rotate(angle);
  scale(mouseX/400);
  fill(255);
  rect(-150, -150, 300, 300);
  pop();

  push();
  translate(width/2, height/2);
  rotate(-angle);
  scale(sf);
  noFill();
  stroke(255);
  rect(-150, -150, 300, 300);
  pop();

  angle += radians(1);
  sf = noise(sft)+0.5;
  sft += 0.025;

}