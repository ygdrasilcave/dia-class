let x1 = -300;
let x2 = 1100;
let y3 = 1000;

let eyeY1 = 0;
let eyeYDir1 = 1;
let eyeY2 = 0;
let eyeY3 = 0;

function setup() {
  // put setup code here
  createCanvas(800, 800);
  background(0);
}

function draw() {
  // put drawing code here
  background(0);

  x1 = x1 + 1;
  if(x1 > width+300){
    x1 = -300;
  }

  x2 = x2 - 5;
  if(x2 < -300){
    x2 = width+300;
  }

  y3 = y3 - 2;
  if(y3 < -200){
    y3 = width+200;
  }

  eyeY1 = eyeY1 + 3*eyeYDir1;
  if(eyeY1 > 20){
    eyeYDir1 = eyeYDir1*-1;
  }
  if(eyeY1 < -20){
    eyeYDir1 = eyeYDir1*-1;
  }

  cloud(x1, 200, eyeY1);
  cloud(x2, 350, 10);
  cloud(400, y3, -20);

  //console.log(x1);
}

function cloud(_x, _y, _ey){
  fill(255);
  noStroke();
  ellipse(_x, _y, 200, 200);
  ellipse(_x-100, _y, 200, 100);
  ellipse(_x+100, _y+50, 150, 120);

  fill(0);
  ellipse(_x-40, _y-40, 30, 50);
  ellipse(_x+40, _y-40, 30, 50);

  fill(255);
  ellipse(_x-40, _y-40+_ey, 15, 15);
  ellipse(_x+40, _y-40+_ey, 15, 15);
}