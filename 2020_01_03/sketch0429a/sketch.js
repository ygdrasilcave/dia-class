/*function hyperlink(){
  let linkP;
  let linkN;
  linkP = createA('../clock/', 'previous', '_self');
  linkN = createA('http://p5js.org/', 'next', '_blank');
  linkP.position(100, 700);
  linkN.position(400, 700);  
}*/

let assets = '../assets/playSource/'

//Controlling animations
//click, press and hold to see different behaviors
var circle, explode, sleep, glitch;

function preload() {

  sleep = loadAnimation(assets+'asterisk_stretching0001.png', assets+'asterisk_stretching0008.png');

  circle = loadAnimation(assets+'asterisk_circle0000.png', assets+'asterisk_circle0008.png');
  //by default animations loop but it can be changed
  circle.looping = false;

  explode = loadAnimation(assets+'asterisk_explode0001.png', assets+'asterisk_explode0011.png');

  glitch = loadAnimation(assets+'asterisk.png', assets+'triangle.png', assets+'square.png', assets+'cloud.png', assets+'star.png', assets+'mess.png', assets+'monster.png');
  //by default an animation plays but you may not want that
  glitch.playing = false;
}

function setup() {
  createCanvas(800, 300);
}

function draw() {
  background(120, 120, 120);

  //playing an pausing an animation
  if(mouseIsPressed){
    sleep.play();
  }else{
    sleep.stop();
  }

  //reading and changing the current frame
  if(explode.getFrame()==explode.getLastFrame()){
    //changeFrame starts from 0
    explode.changeFrame(7);
  }

  //playing backward or forward toward a specific frame
  //returns to the initial frame if click and hold
  if(mouseIsPressed){
    circle.goToFrame(0);
  }else{
    circle.goToFrame(circle.getLastFrame());
  }

  animation(sleep, 100, 150);
  animation(explode, 300, 150);
  animation(glitch, 500, 150);
  animation(circle, 700, 150);

}

function mousePressed(){
  /*if (mouseX > 600 && mouseX < 750 && mouseY > 600 && mouseY < 750){ 
    //window.open("http://www.google.com", '_blank');
    window.open("../sketch0325a/", '_blank');
  }*/

  //rewind on mouse pressed - change frame to 0
  explode.rewind();

  //move ahead one frame
  glitch.nextFrame();
  //glitch.previousFrame();
}