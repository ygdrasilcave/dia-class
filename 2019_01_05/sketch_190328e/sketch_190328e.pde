int x = 7;
int y = 19;

void setup() {
  size(400, 300);
  updateValue();
  textSize(50);
}

void draw() {
  background(0);
  fill(255);
  noStroke();
  text("x = " + x, 20, 60);
  text("y = " + y, 20, 160);
  text("x + y = " + addValue(), 20, 260);
}

void updateValue() {
  x = int(random(100));
  y = int(random(100));
}

int addValue(){
  return x+y;
}

int addValue2(int _x, int _y){
  return _x + _y;
}


void keyPressed() {
  if (key == 'r') {
    updateValue();
    int sum = addValue2(int(random(100)), int(random(100)));
    println(sum);
  }
}
