int num = 600;
String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
float[] x = new float[num];
float[] y = new float[num];
float[] speedX = new float[num];
float[] speedY = new float[num];
float[] hueValue = new float[num];
float[] sizeValue = new float[num];

void setup() {
  size(800, 800);
  colorMode(HSB, 360, 255, 255);
  for (int i=0; i<num; i++) {
    x[i] = random(width);
    y[i] = random(height);
    speedX[i] = random(1, 3);
    speedY[i] = random(0.5, 2.5);
    hueValue[i] = random(360);
    sizeValue[i] = random(10, 50);
  }
  //printArray(x);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0, 0, 255);
  noStroke();
  for (int i=0; i<num; i++) {
    fill(hueValue[i], 255, 255);
    x[i] += speedX[i];
    y[i] += speedY[i];
    textSize(sizeValue[i]);
    text(s.charAt(i%26), x[i], y[i]);
    if (x[i] > width) {
      x[i] = width;
      speedX[i] *= -1;
    }
    if (x[i] < 0) {
      x[i] = 0;
      speedX[i] *= -1;
    }
    if (y[i] > height) {
      y[i] = height;
      speedY[i] *= -1;
    }
    if (y[i] < 0) {
      y[i] = 0;
      speedY[i] *= -1;
    }
  }
}
