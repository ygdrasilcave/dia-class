float x = 0;
float y = 0;
float t = 0;
float tSpeed = 0.065;

void setup(){
  size(1200, 300);
  background(0);
}

void draw(){
  y = sin(t)*120 + 150;
  x = x + 1;
  fill(255);
  noStroke();
  ellipse(x, y, 10, 10);
  t+=tSpeed;
  
  tSpeed = map(mouseX, 0, width, 0.012, 0.125);
  
  if(x > width){
    x = 0;
    background(0);
  }
}

void keyPressed(){
  if(key == ' '){
    background(0);
    x = 0;
  }
}
