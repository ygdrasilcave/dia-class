float x = 0;
float y = 0;
float t = 0;
float r = 250;
float angle = 0;

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
  t = radians(angle);
  x = cos(t)*r + width/2;
  y = sin(t)*r + height/2;
  fill(255);
  noStroke();
  rect(x-25, y-25, 50, 50);
  stroke(255);
  strokeWeight(3);
  line(width/2, height/2, x, y);
  angle++;
}
