float x = 0;
float y = 0;
float r = 350;
float t = 0;
float tSpeed = 0.025;
float t2 = 0;

int pattern = 0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  //background(0);
  translate(width/2, height/2);
  x = cos(t)*r;
  y = sin(t)*r;
  t += tSpeed;
  noStroke();
  fill(255);
  ellipse(x, y, 5, 5);

  if (pattern == 0) {
    r = width/2 - 50;
  } else if (pattern == 1) {
    r = noise(t2)*(width/2);
    t2 += 0.012;
  } else if (pattern == 2) {
    r = sin(t2)*(width/2);
    t2 += 0.012;
  } else if (pattern == 3) {
    r = cos(t2)*(width/2);
    t2 += 0.036;
  }
  
  tSpeed = map(mouseX, 0, width, 0.0089, 0.29);
}

void keyPressed() {
  if (key == ' ') {
    background(0);
  }
  if (key == '0') {
    pattern = 0;
  }
  if (key == '1') {
    pattern = 1;
  }
  if (key == '2') {
    pattern = 2;
  }
  if (key == '3') {
    pattern = 3;
  }
}
