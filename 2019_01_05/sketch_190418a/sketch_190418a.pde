int sec;
int min;
int hour;
float centerX;
float centerY;
int type = 0;

float wt = 0;
int secPrev = 0;
boolean start = false;

void setup() {
  size(800, 800);
  background(255);
  centerX = width/2;
  centerY = height/2;
  secPrev = second();
}

void draw() {
  background(255);
  clock_bk(type);

  sec = second();
  min = minute();
  hour = hour();

  //second_needle(width/2-30-30);
  minute_needle(width/2-30-100);
  hour_needle(width/2-30-150); 
  //println(hour);
}

void hour_needle(float _r){
  float aOffset = map(min, 0, 59, 0, 360/12.0);
  float a = radians((((360/12.0) * (hour-12))-90)+aOffset);  
  float sx = cos(a)*_r + centerX;
  float sy = sin(a)*_r + centerY;
  float sx1 = cos(a-PI)*(_r*0.3) + centerX;
  float sy1 = sin(a-PI)*(_r*0.3) + centerY;
  stroke(0);
  strokeWeight(12);
  line(centerX, centerY, sx, sy);
  line(centerX, centerY, sx1, sy1);
}

void minute_needle(float _r){
  float aOffset = map(sec, 0, 59, 0, 360/60.0);
  float a = radians((((360/60.0) * min)-90)+aOffset);
  float sx = cos(a)*_r + centerX;
  float sy = sin(a)*_r + centerY;
  float sx1 = cos(a-PI)*(_r*0.3) + centerX;
  float sy1 = sin(a-PI)*(_r*0.3) + centerY;
  stroke(0);
  strokeWeight(8);
  line(centerX, centerY, sx, sy);
  line(centerX, centerY, sx1, sy1);
}

void second_needle(float _r){
  float a = radians(((360/60.0) * sec)-90);
  float sx = cos(a)*_r + centerX;
  float sy = sin(a)*_r + centerY;
  float sx1 = cos(a-PI)*(_r*0.3) + centerX;
  float sy1 = sin(a-PI)*(_r*0.3) + centerY;
  stroke(0);
  strokeWeight(3);
  line(centerX, centerY, sx, sy);
  line(centerX, centerY, sx1, sy1);
}

void clock_bk(int _type) {
  float w = map(sin(wt), -1, 1, 10, 100);
  
  noStroke();
  if (_type == 0) {
    for (int i=0; i<12; i++) {
      fill(0);
      float a = radians(i*(360/12));
      float x = cos(a)*(width/2-30-w) + centerX;
      float y = sin(a)*(height/2-30-w) + centerY;
      ellipse(x, y, 35, 35);
    }
    for (int i=0; i<60; i++) {
      float a = radians(i*(360/60));
      float x = cos(a)*(width/2-30-w) + centerX;
      float y = sin(a)*(height/2-30-w) + centerY;
      if(i%5 == 0){
        fill(255);
      }else{
        fill(0);
      }
      ellipse(x, y, 20, 20);
    }
  } else if (_type == 1) {
    for (int i=0; i<60; i++) {
      pushMatrix();
      translate(centerX, centerY);
      float a = radians(i*(360/60.0));
      rotate(a);
      if(i%5==0){
        rect(width/2-30-15-w, -5, 30, 10);
      }else{
        rect(width/2-30-10-w, -2.5, 20, 5);
      }
      popMatrix();
    }
  }
  
  if(secPrev != sec){
    wt = 0;
    secPrev = sec;
    start = true;
  }
  if(start == true){
    wt += 0.122;
    if(wt > PI){
      start = false;
    }
  }
  
  
  noStroke();
  fill(100);
  ellipse(centerX, centerY, w, w);
  noFill();
  stroke(120);
  strokeWeight(2);
  ellipse(centerX, centerY, w+10, w+10);
  ellipse(centerX, centerY, width - (w+10), height - (w+10));
  
  noStroke();
  fill(0);
  ellipse(centerX, centerY, 40, 40);
}

void keyPressed() {
  if (key == '1') {
    type = 1;
  }
  if (key == '0') {
    type = 0;
  }
}
