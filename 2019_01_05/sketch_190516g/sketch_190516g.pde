PImage img1;

float[] r;
float[] g;
float[] b;

float t = 0;

void setup() {
  size(800, 775);
  img1 = loadImage("face-2-1.jpg");
  r = new float[img1.width*img1.height];
  g = new float[img1.width*img1.height];
  b = new float[img1.width*img1.height];

  for (int i=0; i<r.length; i++) {
    color c = img1.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  background(0);
}

void draw() {
  //image(img2, 0, 0);
  background(255);
  noStroke();
  
  for(int y = 0; y<height; y+=10){
    for(int x = 0; x<width; x+=10){
      int index = x + y*width;
      float _r = r[index];
      float _g = g[index];
      float _b = b[index];
      
      float _s = noise(x*0.01, y*0.01, t)*20;
      
      fill(_r, _g, _b);
      ellipse(x, y, _s, _s);
    }
  }
  
  t+=0.065;

}
