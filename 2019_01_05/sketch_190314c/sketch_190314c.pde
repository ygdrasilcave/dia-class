float w = 300; 
float h = 300;

void setup() {
  size(1000, 1000);
  background(0);
}

void draw() {
  background(0);

  noStroke();
  fill(255, 255, 0);
  rect(width/2-200-w/2, height/2-h/2, w, h);

  fill(0, 255, 255);
  rect(width/2+200-w/2, height/2-h/2, w, h);

  fill(255, 0, 255);
  rect(width/2-w/2, height/2-200-h/2, w, h);

  fill(255);
  rect(width/2-w/2, height/2+200-h/2, w, h);

  stroke(255);
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);
}
