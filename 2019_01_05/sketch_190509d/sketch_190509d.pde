int[] r;
int[] g;
int[] b;

void setup() {
  size(720, 480);
  r = new int[720*480];
  g = new int[720*480];
  b = new int[720*480];
  for (int i=0; i<720*480; i++) {
    r[i] = int(random(255));
    g[i] = int(random(255));
    b[i] = int(random(255));
  }
}

void draw() {
  noStroke();
  for (int x=0; x<720; x++) {
    for (int y=0; y<480; y++) {
      int i = x + y*720;
      fill(r[i], g[i], b[i]);
      rect(x, y, 1, 1);
    }
  }
}
