float radius = 200;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  
  fill(255);
  
  float x1 = cos(radians(270))*radius + width/2;
  float y1 = sin(radians(270))*radius + height/2;  
  ellipse(x1, y1, 20, 20);
  
  float x2 = cos(radians(10))*(radius+100) + width/2;
  float y2 = sin(radians(10))*(radius+100) + height/2;  
  ellipse(x2, y2, 20, 20);
  
  float x3 = cos(radians(150))*radius + width/2;
  float y3 = sin(radians(150))*radius + height/2;  
  ellipse(x3, y3, 20, 20);
  
  stroke(255);
  line(x1, y1, x2, y2);
  line(x2, y2, x3, y3);
  line(x1, y1, x3, y3);
  
  line(width/2, height/2, x1, y1);
  line(width/2, height/2, x2, y2);
  line(width/2, height/2, x3, y3);
  
}
