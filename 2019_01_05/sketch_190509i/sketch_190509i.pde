PImage[] imgs;
int index = 0;

import processing.sound.*;
AudioIn micInput;
Amplitude loudness;

void setup() {
  size(800, 800);
  background(0);
  imgs = new PImage[12];
  for (int i=0; i<imgs.length; i++) {
    //PT_anim0001.gif
    String _fileName = "PT_anim00" + nf(i, 2) + ".gif";
    imgs[i] = loadImage(_fileName);
  }
  micInput = new AudioIn(this, 0);
  micInput.start();
  loudness = new Amplitude(this);
  loudness.input(micInput);
}

void draw() {
  for (int x=0; x<4; x++) {
    for (int y=0; y<4; y++) {
      image(imgs[index], x*200, y*200);
    }
  }

  float inputLevel = map(mouseY, 0, height, 1.0, 0.0);
  micInput.amp(inputLevel);
  float volume = loudness.analyze();
  //int size = int(map(volume, 0, 0.5, 1, 350));
  if (volume > 0.05) {
    index++;
    if (index > imgs.length-1) {
      index = 0;
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    index++;
    if (index > imgs.length-1) {
      index = 0;
    }
    println(index);
  }
}
