void setup() {
  size(600, 850);
}

void draw() {
  background(255);
  
  noStroke();
  fill(6, 26, 147);
  rect(0, 0, width/4, (height/3)/2);
  fill(255, 204, 15);
  rect(2*(width/4), 0, width/4, ((height/3)/2)/2);
  fill(209, 6, 23);
  rect(width/4, (height/3)/2, (width/4)*2, (height/3)/2);
  fill(6, 26, 147);
  rect(3*(width/4), height/3, width/4, (height/3)/2);
  fill(6, 26, 147);
  rect(0, (2*(height/3)), width/4, (height/3)/2);
  fill(255, 204, 15);
  rect(width/2, 2*(height/3), width/2, height/3);
  
  strokeCap(SQUARE);
  stroke(0);
  strokeWeight(14);

  line(width/4, 0, width/4, 2*(height/3)+(height/3)/2);
  line(3*(width/4), 0, 3*(width/4), 2*(height/3));
  line(0, height/3, width, height/3);  
  line(0, 2*(height/3), width, 2*(height/3));

  line(0, (height/3)/2, 3*(width/4), (height/3)/2);
  line(0, 2*(height/3)+(height/3)/2, width/2, 2*(height/3)+(height/3)/2);
  line(width/2, 2*(height/3), width/2, height);
  line(3*(width/4), height/2, width, height/2);
  line(width/2, 0, width/2, (height/3)/2);
  line(width/2, ((height/3)/2)/2, 3*(width/4), ((height/3)/2)/2);
}
