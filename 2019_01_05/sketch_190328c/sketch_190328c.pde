float x = 0.0;
float xSpeed = 5;

float y = 0.0;
float ySpeed = 5;

float xMinBoundary = 0;
float xMaxBoundary = 0;
float yMinBoundary = 0;
float yMaxBoundary = 0;

float spaceX = 0;
float spaceY = 0;

float tx = 0.0;
float ty = 0.0;

boolean toggle = true;
boolean showLine = true;

void setup() {
  size(800, 800);
  background(80);
  xSpeed = random(1, 7);
  ySpeed = random(2, 9);

  updateBoundary();
}

void draw() {
  background(80);

  updateBoundary();

  noStroke();
  fill(255);
  ellipse(x, y, 50, 50);

  x += xSpeed;
  y += ySpeed;

  if (x > xMaxBoundary) {
    x = xMaxBoundary;
    xSpeed = -xSpeed;
  }

  if (x < xMinBoundary) {
    x = xMinBoundary;
    xSpeed = -xSpeed;
  }

  if (y > yMaxBoundary) {
    y = yMaxBoundary;
    ySpeed = -ySpeed;
  }

  if (y < yMinBoundary) {
    y = yMinBoundary;
    ySpeed = -ySpeed;
  }

  if (showLine == true) {
    stroke(255);
    line(xMinBoundary, 0, xMinBoundary, height);
    line(xMaxBoundary, 0, xMaxBoundary, height);
    line(0, yMinBoundary, width, yMinBoundary);
    line(0, yMaxBoundary, width, yMaxBoundary);
  }
  //println(x);
}

void updateBoundary() {
  if (toggle == true) {
    spaceX = map(mouseX, 0, width, 0, width/2-10);
    spaceY = map(mouseY, 0, height, 0, height/2-10);
  } else {
    spaceX = map(noise(tx), 0, 1, 0, width/2-10);
    spaceY = map(noise(ty), 0, 1, 0, height/2-10);
  }

  xMinBoundary = spaceX;
  xMaxBoundary = width - spaceX;
  yMinBoundary = spaceY;
  yMaxBoundary = height - spaceY; 

  tx += 0.0165;
  ty += 0.027;
}

void keyPressed() {
  if (key == ' ') {
    toggle = !toggle;
  }

  if (key == 'h') {
    showLine = !showLine;
  }
}
