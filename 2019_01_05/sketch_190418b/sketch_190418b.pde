float x = 0;
float y = 0;
float speedX;
float speedY;
float dirX = 1;
float dirY = 1;
String s;
String s0 = "To";
String s1 = "be";
String s2 = "or";
String s3 = "not";
String s4 = "to";
String s5 = "be";
String s6 = "that";
String s7 = "is";
String s8 = "the";
String s9 = "question";
int counter = 0;

boolean ani = false;
float t = 0;

void setup() {
  size(800, 800);
  background(0);
  speedX = random(2, 8);
  speedY = random(3, 9);
  textAlign(CENTER, CENTER);
  textSize(100);
}

void draw() {
  background(50);
  fill(255);
  noStroke();
  ellipse(x, y, 10, 10);
  
  float windX = map(mouseX, 0, width, 0, 20);
  float windY = map(mouseY, 0, height, 0, 20);
  x += (speedX+windX)*dirX;
  y += (speedY+windY)*dirY;
  if (x > width-10) {
    x = width-10;
    dirX *= -1;
    speedX = random(2, 8);
    counter++;
  }
  if (x < 10) {
    x = 10;
    dirX *= -1;
    speedX = random(2, 8);
    counter++;
  }
  if (y > height-10) {
    y = height-10;
    dirY *= -1;
    speedY = random(3, 9);
    counter++;
  }
  if (y < 10) {
    y = 10;
    dirY *= -1;
    speedY = random(3, 9);
    counter++;
  }

  counter = counter%11;
  textSize(180);
  if (counter == 0) {
    s = s0;
    ani = false;
  } else if (counter == 1) {
    s = s1;
    ani = false;
  } else if (counter == 2) {
    s = s2;
    ani = false;
  } else if (counter == 3) {
    s = s3;
    ani = false;
  } else if (counter == 4) {
    s = s4;
    ani = false;
  } else if (counter == 5) {
    s = s5;
    ani = false;
  } else if (counter == 6) {
    s = s6;
    ani = false;
  } else if (counter == 7) {
    s = s7;
    ani = false;
  } else if (counter == 8) {
    s = s8;
    ani = false;
  } else if (counter == 9) {
    s = s9;
    ani = true;
  } else {
    textSize(80);
    s = "To be, or not to be,\nthat is the question";
    ani = false;
  }
  
  if(ani == true){
    t = random(-HALF_PI/4, HALF_PI/4);
  }else{
    t = 0;
  }

  pushMatrix();
  translate(width/2, height/2);
  rotate(t);
  text(s, 0, 0);
  popMatrix();
}
