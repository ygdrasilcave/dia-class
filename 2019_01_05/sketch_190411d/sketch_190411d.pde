float x = 0;
float y = 0;
float r = 350;
float t = 0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);
  x = cos(t)*(r*0.5) + width/2;
  y = sin(t)*r + height/2;
  t += 0.012;
  noStroke();
  fill(255, 255, 0);
  ellipse(x, y, 30, 30);
  fill(255, 0, 0);
  ellipse(x, 20, 30, 30);
  fill(0, 255, 0);
  ellipse(20, y, 30, 30);

  stroke(255);
  noFill();
  line(x, 20, x, y);
  line(20, y, x, y);
  line(width/2, height/2, x, y);
  ellipse(width/2, height/2, r, r*2);
}
