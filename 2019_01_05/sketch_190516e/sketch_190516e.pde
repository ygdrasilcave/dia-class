PImage img1;

float[] r;
float[] g;
float[] b;

float t = 0;
int index = 0;
float rad = 0;
float s = 1;

void setup() {
  size(800, 775);
  img1 = loadImage("face-2-1.jpg");
  r = new float[img1.width*img1.height];
  g = new float[img1.width*img1.height];
  b = new float[img1.width*img1.height];

  for (int i=0; i<r.length; i++) {
    color c = img1.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  background(0);
}

void draw() {
  //image(img2, 0, 0);
  //background(0);
  noStroke();
  float x = cos(t)*rad + width/2;
  float y = sin(t)*rad + height/2;
  float _r = r[index];
  float _g = g[index];
  float _b = b[index];
  fill(_r, _g, _b);
  ellipse(x, y, s, s);
  t += 0.025;
  rad += 0.0125;
  index += 1;
  s += 0.001;
  if(index > r.length){
    index = 0;
  }
  println(index);
}
