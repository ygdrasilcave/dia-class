float t = 0.0;
float tSpeed = 0.017;
boolean type = true;
int count = 0;

void setup() {
  size(800, 500);
  frameRate(1);
}

void draw() {
  background(0);
  
  if (type == false) {
    tSpeed = random(0.005, 0.03);
  }else{
    tSpeed = 0.0;
  }
  
  for (int i=0; i<width; i+=5) {
    float h = 0;
    if (type == true) {
      h = random(height);
    } else {
      h = noise(t);
      h = h*height;
    }
    stroke(255);
    strokeWeight(3);
    line(i, height, i, height-h);
    t += tSpeed;
  }

  count++;
  if (count%5 == 0) {
    type = !type;
  }
  
  fill(255);
  noStroke();
  textSize(42);
  text(tSpeed, 10, 40);
}

void keyPressed() {
  if (key == ' ') {
    saveFrame("myWorks_######.jpg");
  }
}
