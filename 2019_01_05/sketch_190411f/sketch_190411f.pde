void setup(){
  size(800, 800);
  background(0);
  textSize(40);
  textAlign(CENTER, CENTER);
}


void draw(){
  background(0);
  int sec = second();
  int min = minute();
  int hour = hour();
  fill(255);
  noStroke();
  text(hour+ " : " +min + " : " +sec, width/2, height/2);
}
