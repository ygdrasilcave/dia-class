float t1 = 0.0;
float t2 = 0.0;
float t3 = 0.0;

void setup(){
  size(800, 800);
  background(0);
  colorMode(HSB);
}

void draw(){
  //background(0);
  noStroke();
  fill(0, 15);
  rect(0,0,width,height);
  
  float w = noise(t1)*width;
  float h = noise(t2)*height;
  noFill();
  //strokeWeight(3);
  stroke(noise(t3)*255, 255, 255);  
  ellipse(w, h, 50, 50);
  ellipse(width/2, height/2, w, h);
  t1+=0.0145;
  t2+=0.035;
  t3+=0.0095;
}

void keyPressed(){
  if(key == ' '){
    saveFrame("myWorks_####.jpg");
  }
}
