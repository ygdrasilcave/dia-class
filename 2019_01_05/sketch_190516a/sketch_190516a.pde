PImage img1, img2;

void setup() {
  size(800, 775);
  img1 = loadImage("face-2-1.jpg");
  img2 = loadImage("face-5-1.jpg");
}

void draw() {
  //image(img2, 0, 0);
  background(0);
  noStroke();
  for (int y = 0; y < height; y++) {
    for (int x = 0; x < width; x++) {
      int index = x + y*width;
      color c;
      if(y%2 == 0){
        c = img1.pixels[index];
      }else{
        c = img2.pixels[index];
      }
      fill(c);
      rect(x, y, 1, 1);
    }
  }
}
