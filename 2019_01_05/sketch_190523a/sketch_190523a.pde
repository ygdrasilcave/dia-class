boolean sc0 = true;
boolean sc1 = false;
boolean sc2 = false;

int currTime = 0;
int duration0 = 5000;
int duration1 = 2000;
int duration2 = 3000;
int count = 0;

void setup() {
  size(800, 800);
  background(0);
  currTime = millis();
  println(currTime);
}

void draw() {
  background(0);
  if (sc0 == true) {
    scene0(mouseX, mouseY);
    if (currTime + duration0 < millis()) {
      next();
      currTime = millis();
      println("next: " + count);
      count++;
    }
  } else if (sc1 == true) {
    scene1(mouseX, mouseY);
    if (currTime + duration1 < millis()) {
      next();
      currTime = millis();
      println("next: " + count);
      count++;
    }
  } else if (sc2 == true) {
    scene2(mouseX, mouseY);
    if (currTime + duration2 < millis()) {
      next();
      currTime = millis();
      println("next: " + count);
      count++;
    }
  }
}

void next() {
  if (sc0 == true && sc1 == false && sc2 == false) {
    sc0 = false;
    sc1 = true;
    sc2 = false;
  } else if (sc0 == false && sc1 == true && sc2 == false) {
    sc0 = false;
    sc1 = false;
    sc2 = true;
  } else if (sc0 == false && sc1 == false && sc2 == true) {
    sc0 = true;
    sc1 = false;
    sc2 = false;
  }
}

void scene0(float _x, float _y) {
  fill(255, 0, 255);
  rect(_x-50, _y-50, 100, 100);
}

void scene1(float _x, float _y) {
  fill(0, 255, 255);
  ellipse(_x, _y, 100, 100);
}

void scene2(float _x, float _y) {
  fill(255, 255, 0);
  beginShape();
  for (int i = 0; i<8; i++) {
    float angle = radians((360/8)*i);
    float pointX = cos(angle)*50 + _x;
    float pointY = sin(angle)*50 + _y;
    vertex(pointX, pointY);
  }
  endShape(CLOSE);
}

void keyPressed() {
  if (key == '1') {
    sc0 = true;
    sc1 = false;
    sc2 = false;
    currTime = millis();
  }
  if (key == '2') {
    sc0 = false;
    sc1 = true;
    sc2 = false;
    currTime = millis();
  }
  if (key == '3') {
    sc0 = false;
    sc1 = false;
    sc2 = true;
    currTime = millis();
  }
}
