float x = 0;
float y = 0;
float angle = 0;

void setup(){
  size(1000, 600);
  background(0);
  
  x = 600;
  y = 450;
}

void draw(){
  background(0);
  
  x = width/2;
  y = height/2;
  
  translate(x, y);
  rotate(radians(angle));
  angle++;
  
  stroke(255);
  strokeWeight(3);
  fill(255);
  ellipse(98, 0, 100, 100);
  rect(-50, -50, 100, 100);
  line(0, 0, 0, 0-100);

  translate(200, 200);
  rotate(radians(-angle));
  rect(-50, -50, 100, 100);

}
