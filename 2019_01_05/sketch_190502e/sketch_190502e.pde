int num = 100;
String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
int sLen = s.length();
float theta = 0;
float[] dist = new float[num];
float[] distSpeed = new float[num];
float[] distSpeedT = new float[num];
float lineOffset = 15;

void setup() {
  size(800, 800);
  textAlign(CENTER, CENTER);
  textSize(20);
  for (int i = 0; i<num; i++) {
    dist[i] = 300;
    distSpeed[i] = 0;
    distSpeedT[i] = random(0.012, 0.035);
  }
}

void draw() {
  background(0);

  for (int i=0; i<num; i++) {
    noStroke();
    fill(255);
    pushMatrix();
    translate(width/2, height/2);
    float _angle = TWO_PI/float(num) * float(i);
    float _distOff = sin(distSpeed[i])*100;
    rotate(_angle);
    text(s.charAt(i%sLen), 0, -dist[i] + _distOff);
    popMatrix();

    noFill();
    stroke(255);
    float _x = cos(_angle-HALF_PI)*(dist[i]- lineOffset - _distOff) + width/2;
    float _y = sin(_angle-HALF_PI)*(dist[i]- lineOffset - _distOff) + height/2;
    float _x2 = 0;
    float _y2 = 0;
    if (i < num-1) {
      float _angle2 = TWO_PI/float(num) * float(i+1);
      float _distOff2 = sin(distSpeed[i+1])*100;
      _x2 = cos(_angle2-HALF_PI)*(dist[i+1]- lineOffset - _distOff2) + width/2;
      _y2 = sin(_angle2-HALF_PI)*(dist[i+1]- lineOffset - _distOff2) + height/2;
    }else{
      float _angle2 = TWO_PI/float(num) * float(0);
      float _distOff2 = sin(distSpeed[0])*100;
      _x2 = cos(_angle2-HALF_PI)*(dist[0]- lineOffset - _distOff2) + width/2;
      _y2 = sin(_angle2-HALF_PI)*(dist[0]- lineOffset - _distOff2) + height/2;
    }
    line(_x, _y, _x2, _y2);
    point(_x, _y);

    distSpeed[i] += distSpeedT[i];
  }
}
