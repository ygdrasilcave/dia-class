int num = 600;
String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
float[] x = new float[num];
float[] y = new float[num];
float[] speedX = new float[num];
float[] speedY = new float[num];
float[] hueValue = new float[num];
float[] sizeValue = new float[num];
float[] t = new float[num];
float[] speedT = new float[num];
float[] scaleFactor = new float[num];

void setup() {
  size(800, 800);
  colorMode(HSB, 360, 255, 255);
  for (int i=0; i<num; i++) {
    x[i] = random(width);
    y[i] = random(height);
    speedX[i] = 0;
    speedY[i] = -random(0.5, 2.5);
    hueValue[i] = random(360);
    sizeValue[i] = random(10, 50);
    t[i] = 0;
    speedT[i] = random(0.015, 0.065);
    scaleFactor[i] = random(2, 5);
  }
  //printArray(x);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0, 0, 255);
  noStroke();
  for (int i=0; i<num; i++) {
    fill(hueValue[i], 255, 255);
    float _nv = (noise(t[i])*-2.0)+1.0;
    x[i] += speedX[i] + _nv*scaleFactor[i];
    y[i] += speedY[i];
    textSize(sizeValue[i]);
    text(s.charAt(i%26), x[i], y[i]);

    if (y[i] < -50) {
      y[i] = height+50;
      x[i] = random(width);
    }
    t[i] += speedT[i];
  }
}
