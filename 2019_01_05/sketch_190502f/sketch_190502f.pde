String[] dice = {"one", "two", "three", "four", "five", "six", "seven"};
boolean rolling = false;
int index = 0;

void setup(){
  size(600, 600);
  background(0);
  textAlign(CENTER, CENTER);
  textSize(80);
}

void draw(){
  background(0);
  fill(255);
  text(dice[index], width/2, height/2);
  if(rolling == true){
    index = int(random(dice.length));
  }
}

void keyPressed(){
  if(key == ' '){
    rolling = !rolling;
  }
}
