PImage img;

void setup() {
  size(720, 480);
  img = loadImage("flower.jpg");
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw() {
  background(255);
  //image(img, mouseX, mouseY, 100, 100);
  image(img, 0, 0);
  color c = img.get(mouseX, mouseY);
  float r = red(c);
  float g = green(c);
  float b = blue(c);
  noStroke();
  fill(r, g, b);
  ellipse(mouseX, mouseY, 50, 50);
  fill(0);
  text("red: " + r, mouseX, mouseY+30);
  text("green: " + g, mouseX, mouseY+50);
  text("blue: " + b, mouseX, mouseY+70);
}
