PImage img;

void setup() {
  size(720, 480);
  img = loadImage("flower.jpg");
}

void draw() {
  background(255);
  noStroke();
  int s = int(map(mouseX, 0, width, 1, 20));
  for (int x=0; x<720; x+=s) {
    for (int y=0; y<480; y+=s) {
      int i = x + y*720;
      color c = img.pixels[i];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      
      if(br >= 230){
        br = 255;
      }else if(br > 120 && br < 230){
        br = 128;
      }else{
        br = 0;
      }
      
      fill(br);
      rect(x, y, s, s);
    }
  }
}
