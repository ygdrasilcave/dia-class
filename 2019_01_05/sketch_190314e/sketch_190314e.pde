int x1_1 = 1;
int y1_1 = 0;
int y1_2 = 10;

int x2_1 = 2;
int y2_1 = 0;
int y2_2 = 2;

int x3_1 = 3;
int y3_1 = 0;
int y3_2 = 8;

int x4_1 = 2;
int y4_1 = 8;
int y4_2 = 12;

int x5_1 = 0;
int x5_2 = 3;
int y5_1 = 2;

int x6_1 = 0;
int x6_2 = 4;
int y6_1 = 4;

int x7_1 = 0;
int x7_2 = 4;
int y7_1 = 8;

int x8_1 = 0;
int x8_2 = 2;
int y8_1 = 10;

int x9_1 = 2;
int x9_2 = 3;
int y9_1 = 1;

int x10_1 = 3;
int x10_2 = 4;
int y10_1 = 6;


void setup() {
  size(600, 800);
}

void draw() {
  background(255);
  /*
  line(0, 0*height/10, width, 0*height/10);
   line(0, 1*height/10, width, 1*height/10);
   line(0, 2*height/10, width, 2*height/10);
   line(0, 3*height/10, width, 3*height/10);
   line(0, 4*height/10, width, 4*height/10);
   line(0, 5*height/10, width, 5*height/10);
   line(0, 6*height/10, width, 6*height/10);
   line(0, 7*height/10, width, 7*height/10);
   line(0, 8*height/10, width, 8*height/10);
   line(0, 9*height/10, width, 9*height/10);
   */

  for (int i=0; i<12; i=i+1) {
    if (i == y5_1) {
      line(x5_1*(width/4), i*(height/12), x5_2*(width/4), i*(height/12));
    }
    if (i == y6_1) {
      line(x6_1*(width/4), i*(height/12), x6_2*(width/4), i*(height/12));
    }
    if (i == y7_1) {
      line(x7_1*(width/4), i*(height/12), x7_2*(width/4), i*(height/12));
    }
    if (i == y8_1) {
      line(x8_1*(width/4), i*(height/12), x8_2*(width/4), i*(height/12));
    }
    if (i == y9_1) {
      line(x9_1*(width/4), i*(height/12), x9_2*(width/4), i*(height/12));
    }
    if (i == y10_1) {
      line(x10_1*(width/4), i*(height/12), x10_2*(width/4), i*(height/12));
    }
  }
  stroke(255, 0, 0);
  strokeWeight(14);
  for (int j=0; j<4; j++) {
    if (j == x1_1) {
      line(j*(width/4), y1_1*(height/12), j*(width/4), y1_2*(height/12));
    }
    if (j == x2_1) {
      line(j*(width/4), y2_1*(height/12), j*(width/4), y2_2*(height/12));
    }
    if (j == x3_1) {
      line(j*(width/4), y3_1*(height/12), j*(width/4), y3_2*(height/12));
    }
    if (j == x4_1) {
      line(j*(width/4), y4_1*(height/12), j*(width/4), y4_2*(height/12));
    }
  }
}
