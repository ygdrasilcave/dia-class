PImage img1, img2;
int scaleX = 4;
int scaleY = 1;

void setup() {
  size(800, 775);
  img1 = loadImage("face-2-1.jpg");
}

void draw() {
  //image(img2, 0, 0);
  background(0);
  
  scaleX = int(map(mouseX, 0, width, 1, 10));
  scaleY = int(map(mouseY, 0, height, 1, 10));
  
  noStroke();
  for (int y = 0; y < height; y+=scaleY) {
    for (int x = 0; x < width; x+=scaleX) {
      int index = x + y*width;
      color c = 0;
      c = img1.pixels[index];
      fill(c);
      rect(x/scaleX, y/scaleY, 1, 1);
    }
  }
}
