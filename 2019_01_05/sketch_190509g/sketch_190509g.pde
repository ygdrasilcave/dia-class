PImage img;
float t = 0;

void setup() {
  size(720, 480);
  img = loadImage("flower.jpg");
  background(255);
}

void draw() {
  background(255);
  strokeWeight(6);
  
  for (int x=0; x<width; x+=15) {
    for (int y=0; y<height; y+=15) {
      int i = x + y*width;
      color c = img.pixels[i];
      float x2 = cos(t)*12 + x;
      float y2 = sin(t)*12 + y;
      stroke(c);
      line(x, y, x2, y2);
    }
  }
  //t = map(mouseX, 0, width, 0, TWO_PI);
  t+=0.035;
}
