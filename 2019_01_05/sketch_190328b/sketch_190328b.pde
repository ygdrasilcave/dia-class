float x = 0.0;
float xSpeed = 5;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  
  noStroke();
  fill(255);
  ellipse(x, height/2, 50, 50);
  
  x += xSpeed;
  
  if(x > width){
    x = width;
    xSpeed = -xSpeed;
  }
  
  if(x < 0){
    x = 0;
    xSpeed = -xSpeed;
  }
  
  //println(x);
}
