float t = 0.0;
float tSpeed = 0.032;

float x = 0.0;

void setup(){
  size(800, 800);
  background(0);
  textSize(40);
  text(tSpeed, 20, 40);
}

void draw(){
  //background(0);
    
  float h = noise(t)*height;
  t += tSpeed;
  
  noStroke();
  fill(map(h, 0, height, 0, 255));
  rect(x, height-h, 2, h);
  
  x += 2;
  
  if(x > width){
    x = 0;
    background(0);
    tSpeed = random(0.005, 0.03);
    fill(255);
    text(tSpeed, 20, 40);
  }
}
