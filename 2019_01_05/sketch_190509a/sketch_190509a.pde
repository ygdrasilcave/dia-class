float[] r1;
int num = 60;

void setup() {
  size(800, 800);
  r1 = new float[num];
  for (int i=0; i<r1.length; i++) {
    r1[i] = (TWO_PI/float(num)) * i;
  }
  printArray(r1);
}

void draw() {
  background(255);
  fill(0);
  noStroke();
  float x = 0;
  float y = 0;
  for (int i=0; i<r1.length; i++) {
    x = cos(r1[i])*300 + width/2;
    y = sin(r1[i])*300 + height/2;
    ellipse(x, y, 30, 30);
  }
}
