float angle = 0;

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
   
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angle));
  fill(255);
  noStroke();
  rect(250-25, -25, 50, 50);
  stroke(255);
  strokeWeight(3);
  line(0, 0, 250, 0);
  popMatrix();
  angle++;
}
