int num = 600;
float[] x = new float[num];
float[] y = new float[num];
float[] speedX = new float[num];
float[] speedY = new float[num];
float[] hueValue = new float[num];
float[] sizeValue = new float[num];

void setup() {
  size(800, 800);
  colorMode(HSB, 360, 255, 255);
  for (int i=0; i<num; i++) {
    x[i] = random(width);
    y[i] = random(height);
    speedX[i] = random(1, 3);
    speedY[i] = random(0.5, 2.5);
    hueValue[i] = random(360);
    sizeValue[i] = random(5, 30);
  }
  //printArray(x);
}

void draw() {
  background(0, 0, 255);
  noStroke();
  for (int i=0; i<num; i++) {
    fill(hueValue[i], 255, 255);
    x[i] += speedX[i];
    y[i] += speedY[i];
    if (i%2 == 0) {
      ellipse(x[i], y[i], sizeValue[i], sizeValue[i]);
    } else {
      rect(x[i]-sizeValue[i]/2, y[i]-sizeValue[i]/2, sizeValue[i], sizeValue[i]);
    }
    if (x[i] > width) {
      x[i] = width;
      speedX[i] *= -1;
    }
    if (x[i] < 0) {
      x[i] = 0;
      speedX[i] *= -1;
    }
    if (y[i] > height) {
      y[i] = height;
      speedY[i] *= -1;
    }
    if (y[i] < 0) {
      y[i] = 0;
      speedY[i] *= -1;
    }
  }
}
