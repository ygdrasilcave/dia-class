PImage img;
float x = 0;
float y = 0;
float speedX;
float speedY;
float tx = 0;
float ty = 0;
float txSpeed = 0.065;
float tySpeed = 0.045;
float s = 0;
float ts = 0;
float tsSpeed = 0.08;

void setup() {
  size(720, 960);
  img = loadImage("flower.jpg");
  background(255);
  speedX = random(2, 5);
  speedY = random(3, 6);
}

void draw() {  
  noStroke();
  int i = int(x) + int(y)*720;
  color c = img.pixels[i];
  fill(c);
  ellipse(x, y, s, s);
  
  tx += txSpeed;
  ty += tySpeed;
  ts += tsSpeed;
  s = map(noise(ts), 0, 1, 10, 50);
  
  x += speedX + map(noise(tx), 0, 1, -5, 5);
  y += speedY + map(noise(ty), 0, 1, -5, 5);
  
  if (x > width) {
    x = width-1;
    speedX *= -1;
  }
  if (x < 0) {
    x = 0;
    speedX *= -1;
  }
  if (y > img.height) {
    y = img.height-1;
    speedY *= -1;
  }
  if (y < 0) {
    y = 0;
    speedY *= -1;
  }
  image(img, 0, height/2);
  
}
