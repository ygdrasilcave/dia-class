float w=100;
float h=100;

void setup(){
  size(600, 600);
}

void draw(){
  background(0);
  fill(255);
  ellipse(width/2, height/2, w, h);
}

void keyPressed(){
  if(key == 'o'){
    w+=5;
  }
  if(key == 'y'){
    w-=5;
  }
  if(key == 'u'){
    h+=5;
  }
  if(key == 'i'){
    h-=5;
  }
}
