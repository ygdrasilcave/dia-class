float t = 0;
float r = 0;
float t2 = 0;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  //background(0);
  noStroke();
  fill(0, 5);
  rect(0, 0, width, height);
  
  fill(255);
  float x = cos(t)*r + width/2;
  float y = sin(t)*r + height/2;
  ellipse(x, y, 20, 20);
  t+=0.025;
  r = map(sin(t2), -1, 1, 200, 350);
  t2+=0.125;
  stroke(255);
  line(width/2, height/2, x, y);
}
