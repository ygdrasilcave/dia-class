void setup() {
  size(800, 500);
}

void draw() {
  background(0);
  
  float space = map(mouseY, 0, height, 50, 5);

  for (int i=0; i<mouseX/space; i=i+1) {
    if (i%5==0) {
      stroke(255);
      strokeWeight(3);
      line(i*space, height, i*space, height-300);
      noStroke();
      fill(255);
      if (i%2==0) {
        ellipse(i*space, height-300, 15, 15);
      } else {
        rect(i*space-15/2, height-300-15/2, 15, 15);
      }
    } else {
      stroke(255);
      strokeWeight(3);
      line(i*space, height, i*space, height-200);
    }
  }
}
