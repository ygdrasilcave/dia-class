PImage img1;

float[] r;
float[] g;
float[] b;

void setup() {
  size(800, 775);
  img1 = loadImage("face-2-1.jpg");
  r = new float[img1.width*img1.height];
  g = new float[img1.width*img1.height];
  b = new float[img1.width*img1.height];
  
  for(int i=0; i<r.length; i++){
    color c = img1.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  
  r = sort(r);
  g = sort(g);
  b = sort(b);
}

void draw() {
  //image(img2, 0, 0);
  background(0);
  
  noStroke();
  for (int y = 0; y < height; y+=1) {
    for (int x = 0; x < width; x+=1) {
      int index = x + y*width;
      float _r = r[index];
      float _g = g[index];
      float _b = b[index];
      fill(_r, _g, _b);
      rect(x, y, 1, 1);
    }
  }
}
