boolean scene1 = true;
boolean scene2 = false;
boolean scene3 = false;
boolean scene4 = false;

void setup() {
  size(800, 800);
}

void draw() {
  background(0);
  if (scene1 == true) {
    sc1();
  }else if(scene2 == true){
    sc2();
  }else if(scene3 == true){
    sc3();
  }else if(scene4 == true){
    sc4();
  }
}

void sc1() {
  fill(255, 255, 0);
  rect(width/2-200, height/2-200, 400, 400);
}

void sc2() {
  fill(0, 255, 255);
  ellipse(width/2, height/2, 400, 400);
}

void sc3() {
  fill(255, 0, 255);
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(45));
  rect(-200, -200, 400, 400);
  popMatrix();
}

void sc4() {
  fill(255, 255, 255);
  beginShape();
  float t = 0;
  for (int i=0; i<30; i++) {
    float x = cos(t)*200 + width/2;
    float y = sin(t)*200 + height/2;
    vertex(x, y);
    t += (TWO_PI/30.0)*i;
  }  
  endShape(CLOSE);
}


void keyPressed() {
  if (key == 's') {
    scene1 = true;
    scene2 = false;
    scene3 = false;
    scene4 = false;
  }
  if (key == 'd') {
    scene1 = false;
    scene2 = true;
    scene3 = false;
    scene4 = false;
  }
  if (key == 'f') {
    scene1 = false;
    scene2 = false;
    scene3 = true;
    scene4 = false;
  }
  if (key == 'e') {
    scene1 = false;
    scene2 = false;
    scene3 = false;
    scene4 = true;
  }
}
