float radius1 = 200;
float radius2 = 100;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  fill(255);
  stroke(255);

  int pointNum = int(map(mouseX, 0, width, 3, 20));

  for (int i=0; i<pointNum; i++) {
    
    if(i%2 == 0){
      radius1 = 200;
      radius2 = 100;
    }else{
      radius1 = 100;
      radius2 = 200;
    }
    
    float x = cos(radians((360/pointNum)*i))*radius1 + width/2;
    float y = sin(radians((360/pointNum)*i))*radius1 + height/2; 
    float x1 = 0;
    float y1 = 0;
    if (i < pointNum-1) {
      x1 = cos(radians((360/pointNum)*(i+1)))*radius2 + width/2;
      y1 = sin(radians((360/pointNum)*(i+1)))*radius2 + height/2;
    }else{
      x1 = cos(radians((360/pointNum)*0))*radius2 + width/2;
      y1 = sin(radians((360/pointNum)*0))*radius2 + height/2; 
    }
    
    ellipse(x, y, 20, 20);   
    line(x, y, x1, y1);
  }
}
