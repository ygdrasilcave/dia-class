PImage img;

void setup() {
  size(720, 480);
  img = loadImage("flower.jpg");
  background(255);
}

void draw() {  
  noStroke();
  int x = int(random(width));
  int y = int(random(height));
  int i = x + y*720;
  color c = img.pixels[i];
  fill(c);
  ellipse(x, y, 20, 20);
}
