float x = 500.0;
float y = 500.0;
float w = 200.0;
float h = 200.0;

void setup() {
  size(1000, 1000);
  background(0, 0, 0);
}

void draw() {
  background(0);
  
  noStroke();
  fill(255, 255, 0);
  ellipse(x+200, y, w, h);
  
  fill(255, 255, 255);
  ellipse(x-200, y, w, h);
  
  fill(0, 255, 255);
  ellipse(x, y-200, w, h);
  
  fill(255, 0, 255);
  ellipse(x, y+200, w, h);
  
  stroke(255);
  line(x-400, y, x+400, y);
  line(x, y-300, x, y+300);
}
