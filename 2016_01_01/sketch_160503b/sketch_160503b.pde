int n = 26;
float angOffset = 0.0;
float radOffset = 0.0;
float[] noiseVal;
float[] t;
float[] tAmp;

void setup() {
  size(800, 800);
  background(0);
  noiseVal = new float[n];
  t = new float[n];
  tAmp = new float[n];
  for(int i=0; i<n; i++){
    noiseVal[i] = 0.0;
    t[i] = 0.0;
    tAmp[i] = random(0.012, 0.045);
  }
}

void draw() {
  //background(255);
  noStroke();
  fill(0, 15);
  rect(0, 0, width, height);
  
  noFill();
  for (int i=0; i<n; i++) {
    float t1 = (TWO_PI/n)*i;
    float t2;
    float nextNoiseVal;
    if (i >= n-1) {
      t2 = 0.0;
      nextNoiseVal = noiseVal[0];
    } else {
      t2 = (TWO_PI/n)*(i+1);
      nextNoiseVal = noiseVal[i+1];
    }
    float offset = radians(angOffset);
    float cx1 = width/2 + cos(t1-offset)*radOffset;
    float cy1 = height/2 + sin(t1-offset)*radOffset;
    float ax1 = width/2 + cos(t1)*300*noiseVal[i];
    float ay1 = height/2 + sin(t1)*300*noiseVal[i];

    float ax2 = width/2 + cos(t2)*300*nextNoiseVal;
    float ay2 = height/2 + sin(t2)*300*nextNoiseVal;
    float cx2 = width/2 + cos(t2+offset)*radOffset;
    float cy2 = height/2 + sin(t2+offset)*radOffset;
    strokeWeight(3);
    stroke(255);
    bezier(ax1, ay1, cx1, cy1, cx2, cy2, ax2, ay2);
    /*if (i==0) {
      stroke(255, 0, 0);
    } else {
      stroke(0, 255, 0);
    }
    line(ax1, ay1, cx1, cy1);
    line(ax2, ay2, cx2, cy2);*/
    noiseVal[i] = noise(t[i]);
    t[i] += tAmp[i];
  }
  angOffset = map(mouseX, 0, width, 0, 45);
  radOffset = map(mouseY, 0, height, 0, height/2);
}