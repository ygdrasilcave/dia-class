float w = 20;
float t = 0.0;
int brightnessVal = 0;
int prevSecond = 0;

void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
  prevSecond = second();
  //frameRate(12);
}

void draw() {
  background(255, 0, 50);
  float hue = 215;
  for (int y=0; y<int(width/w)*2; y++) {
    for (int x=0; x<int(width/w); x++) {
      float centerX = x * (cos(radians(30))*w) * 2;
      float centerY = y * w*3;
      drawShape(centerX, centerY, hue);
    }
  }
  for (int y=0; y<int(width/w)*2; y++) {
    for (int x=0; x<int(width/w); x++) {
      float centerX = cos(radians(30))*w + x * (cos(radians(30))*w) * 2;
      float centerY = w + w/2 + y * w*3;
      drawShape(centerX, centerY, hue+100);
    }
  }
  //drawShape(width/2, height/2);

  w = map(mouseX, 0, width, 10, 300);
  t+=0.016;
  
  if(second() != prevSecond){
    brightnessVal  = brightnessVal + 1;
    if(brightnessVal > 2){
      brightnessVal = 0;
    }
    prevSecond = second();
  }
}

void drawShape(float cx, float cy, float hueVal) {
  noStroke();
  //stroke(0);
  if (brightnessVal == 0) {
    fill(hueVal, 100, 70);
  } else if (brightnessVal == 1) {
    fill(hueVal, 100, 100);
  } else {
    fill(hueVal, 100, 20);
  }
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cx + cos(radians(60*i-30))*w;
    float y = cy + sin(radians(60*i-30))*w;
    vertex(x, y);
  }
  vertex(cx, cy);
  endShape(CLOSE);

  //stroke(0);
  if (brightnessVal == 0) {
    fill(hueVal, 100, 20);
  } else if (brightnessVal == 1) {
    fill(hueVal, 100, 70);
  } else {
    fill(hueVal, 100, 100);
  }
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cx + cos(radians(60*i+90))*w;
    float y = cy + sin(radians(60*i+90))*w;
    vertex(x, y);
  }
  vertex(cx, cy);
  endShape(CLOSE);

  //stroke(0);
  if (brightnessVal == 0) {
    fill(hueVal, 100, 100);
  } else if (brightnessVal == 1) {
    fill(hueVal, 100, 20);
  } else {
    fill(hueVal, 100, 70);
  }
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cx + cos(radians(60*i+210))*w;
    float y = cy + sin(radians(60*i+210))*w;
    vertex(x, y);
  }
  vertex(cx, cy);
  endShape(CLOSE);
  
  fill(120, 0, 100);
  ellipse(cx, cy, 20, 20);
}