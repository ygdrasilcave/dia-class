float t = 0.0;
float t1 = 0.0;

void setup(){
  size(600, 600, P3D);
}

void draw(){
  background(0);
  pushMatrix();
  translate(width/2, height/2, 0);
  rotateY(map(mouseY, 0, height, 0, PI));
  for(int z=-50; z<50; z++){
    pushMatrix();
    translate(0, sin(t)*50, z*map(mouseX, 0, width, 0, 50));
    noFill();
    stroke(255);
    //rect(-30, -30, 60, 60);
    float s = noise(t1)*60+10;
    ellipse(0, 0, s, s);
    popMatrix();
    t+=0.065;
    t1+=0.045;
  }
  popMatrix();
  
  /*translate(width/2, height/2, map(mouseX, 0, width, 0, 300));
  rotateY(map(mouseY, 0, height, 0, TWO_PI));
  box(30, 30, 30);*/
}