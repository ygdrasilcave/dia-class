int num = 50;
float dia;
float lineSize;
float t = 0.0;

void setup() {
  size(800, 800);
  background(255);
  dia = width/num;
}

void draw() {
  background(255);

  for (int y=0; y<num; y++) {
    for (int x=0; x<num; x++) {
      //ellipse(x*dia+dia/2, y*dia+dia/2, dia, dia);
      //line(x*dia+dia/2-lineSize, y*dia+dia/2, x*dia+dia/2+lineSize, y*dia+dia/2);
      //line(x*dia+dia/2, y*dia+dia/2-lineSize, x*dia+dia/2, y*dia+dia/2+lineSize);
      if (y%2==0) {
        if (x%2==0) {
          fill(0);
          noStroke();
          ellipse(x*dia+dia/2-lineSize, y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2+lineSize, y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2-lineSize, 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2+lineSize, 10, 10);
        } else {
          noFill();
          stroke(0);
          ellipse(x*dia+dia/2-(dia/2-lineSize), y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2+(dia/2-lineSize), y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2-(dia/2-lineSize), 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2+(dia/2-lineSize), 10, 10);
        }
      } else {
        if (x%2==0) {
          noFill();
          stroke(0);
          ellipse(x*dia+dia/2-(dia/2-lineSize), y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2+(dia/2-lineSize), y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2-(dia/2-lineSize), 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2+(dia/2-lineSize), 10, 10);
        } else {
          fill(0);
          noStroke();
          ellipse(x*dia+dia/2-lineSize, y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2+lineSize, y*dia+dia/2, 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2-lineSize, 10, 10);
          ellipse(x*dia+dia/2, y*dia+dia/2+lineSize, 10, 10);
        }
      }
    }
  }
  lineSize = abs(sin(t))*dia/2;
  t += 0.058;
  println(lineSize);
  num = int(map(mouseX, 0, width, 2, 30));
  dia = width/num;
}