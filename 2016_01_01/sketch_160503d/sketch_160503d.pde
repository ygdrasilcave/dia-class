void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  strokeWeight(3);
  float ax1 = 100;
  float ay1 = height/2;
  float cx1 = map(mouseX, 0, width, 0, width/2);
  float cy1 = map(mouseY, 0, height, height/2, 0);
  
  float ax2 = 700;
  float ay2 = height/2;
  float cx2 = map(mouseX, 0, width, width, width/2);
  float cy2 = map(mouseY, 0, height, height/2, 0);
  
  fill(255);
  stroke(255);
  ellipse(ax1, ay1, 10, 10);
  ellipse(ax2, ay2, 10, 10);
  line(ax1, ay1, ax2, ay2);
  
  fill(0, 255, 0);
  ellipse(cx1, cy1, 10, 10);
  ellipse(cx2, cy2, 10, 10);
  
  stroke(255, 255, 0);
  noFill();
  bezier(ax1, ay1, cx1, cy1, cx2, cy2, ax2, ay2);
  bezier(cx1, cy1, ax1, ay1, ax2, ay2, cx2, cy2);
}