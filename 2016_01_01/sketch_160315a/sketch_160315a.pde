int r = 0;
int g = 0;
int b = 0;

void setup() {
  size(300, 600);
}

void draw() {
  background(r, g, b);
  b = b+10;
  if (b > 255) {
    b = 0;
  }
  
  g = g+5;
  if (g > 255) {
    g = 0;
  }
}

void keyPressed() {
  println("key pressed");
  b = b+10;

  if (b > 255) {
    b = 0;
  }

  println(b);
}