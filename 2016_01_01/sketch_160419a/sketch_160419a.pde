void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  background(255);
  
  stroke(0);
  fill(255, 255, 0);
  beginShape();
  for(int i=0; i<6; i++){
    float x = width/2 + cos(radians(i*60))*200;
    float y = height/2 + sin(radians(i*60))*200;
    vertex(x, y);
  }
  endShape(CLOSE);
}