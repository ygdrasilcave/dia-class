float w = 50;
float t = 0.0;

void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
}

void draw() {
  background(255, 0, 50);
  for (int y=0; y<20; y++) {
    for (int x=0; x<10; x++) {
      float centerX = x * (cos(radians(30))*w) * 2;
      float centerY = y * w*3;
      drawShape(centerX, centerY);
      //ellipse(centerX, centerY, w, w);
    }
  }
  
  for (int y=0; y<20; y++) {
    for (int x=0; x<10; x++) {
      float centerX = cos(radians(30))*w + x * (cos(radians(30))*w) * 2;
      float centerY = w + w/2 + y * w*3;
      //float centerY = sin(t)*height/2 + y*w*3;
      drawShape(centerX, centerY);
    }
  }
  t+=0.015;
}

void drawShape(float cx, float cy) {
  stroke(0);
  fill(156, 100, 70);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cx + cos(radians(60*i-30))*w;
    float y = cy + sin(radians(60*i-30))*w;
    vertex(x, y);
  }
  vertex(cx, cy);
  endShape(CLOSE);

  stroke(0);
  fill(156, 100, 20);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cx + cos(radians(60*i+90))*w;
    float y = cy + sin(radians(60*i+90))*w;
    vertex(x, y);
  }
  vertex(cx, cy);
  endShape(CLOSE);

  stroke(0);
  fill(156, 100, 100);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cx + cos(radians(60*i+210))*w;
    float y = cy + sin(radians(60*i+210))*w;
    vertex(x, y);
  }
  vertex(cx, cy);
  endShape(CLOSE);
}