int rectNumX = 8;
int rectNumY = 8;
float diaX = 0.0;
float diaY = 0.0;
int step = 8;
int stepSize = 15;
int dirX = 1;
int dirY = 1;
int pSecond = 0;

void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, rectNumX, rectNumY, step);
  diaX = float(width/rectNumX);
  diaY = float(height/rectNumY);
  pSecond = second();
}

void draw() {
  colorMode(HSB, rectNumX, rectNumY, step);
  background(rectNumX, 0, step);
  //stroke(rectNumX, rectNumY, 0);
  noStroke();
  for (int j=0; j<rectNumY; j=j+1) {
    for (int i=0; i<rectNumX; i=i+1) {  
      if (diaX > step*stepSize && diaY > step*stepSize) {
        for (int k=0; k<step; k++) {
          //noStroke();
          //fill(i, j, k);
          noFill();
          stroke(0);
          ellipse(i*diaX+diaX/2+dirX*k*stepSize, j*diaY+diaY/2+dirY*k*stepSize, diaX-k*stepSize, diaY-k*stepSize);
        }
      } else {
        noFill();
        stroke(0);
        ellipse(i*diaX+diaX/2, j*diaY+diaY/2, diaX, diaY);
      }
    }
  }
  if (pSecond != second()) {
    newDir();
    newSet();
    pSecond = second();
  }
}

void newDir() {
  if (random(10) > 5) {
    dirX = 1;
  } else {
    dirX = -1;
  }
  if (random(10) > 5) {
    dirY = 1;
  } else {
    dirY = -1;
  }
}

void newSet() {
  step = int(random(2, 16));
  stepSize = int(random(2, 15));
  println("step: " + step + "  stepSize: " + stepSize);
}

void keyPressed() {
  newDir();
  newSet();
}