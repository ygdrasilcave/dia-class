int num = 80;
float w;
boolean mode = true;

void setup() {
  size(800, 800);
  background(255);
  w = 60;
}

void draw() {
  background(255);

  for (int cy=0; cy<num; cy+=1) {
    for (int cx=0; cx<num; cx+=1) {
      if (mode == true) {
        noFill();
        stroke(0);
      } else {
        fill(random(255), random(255), random(255));
        stroke(0);
      }
      beginShape();
      for (int i=0; i<6; i++) {
        float theta = radians(i*60);
        float x = cx*(w+w/2)+(cy%2)*(w+w/2)/2 + w/2*cos(theta);
        float y = cy*tan(radians(60))*w/2 -(cy*tan(radians(60))*w/2)/2 + w/2*sin(theta);
        vertex(x, y);
      }
      endShape(CLOSE);
      //point(cx*(w+w/2)+(cy%2)*(w+w/2)/2, cy*tan(radians(60))*w/2);
    }
  }
  w = map(mouseX, 0, width, 10, 60);
}

void keyPressed() {
  mode = !mode;
}