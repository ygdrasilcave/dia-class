float t = 0.0;

void setup() {
  size(800, 300);
  background(0);
}

void draw() {
  background(0);
  stroke(255);
  strokeWeight(3);
  for (int x=0; x<width; x+= 5) {
    line(x, height, x, height-noise(t)*height);
    t+=0.016;
  }
  noLoop();
}