import de.looksgood.ani.*;

float x = 256;
float y = 256;

void setup() {
  size(512, 512);
  smooth();
  noStroke();

  Ani.init(this);
}

void draw() {
  background(255);
  fill(0);
  ellipse(x, y, 120, 120);
}

void mouseReleased() {
  Ani.to(this, 3.5, "x", mouseX);
  Ani.to(this, 3.5, "y", mouseY);
}