int cellSize = 5;
int cellNums;

int[] cellColor;

void setup() {
  size(1000, 1000);
  background(255);
  cellNums = floor(width/cellSize);
  cellColor = new int[(cellNums/2)*cellNums];
  for (int i = 0; i<(cellNums/2)*cellNums; i++) {
    cellColor[i] = int(map(random(10), 0, 10, 0, 1.5));
  }
}

void draw() {
  background(255);

  noStroke();
  for (int y=0; y<cellNums; y++) {
    for (int x=0; x<cellNums/2; x++) {
      int index = x + y*(cellNums/2);
      if (cellColor[index] == 0) {
        fill(255);
      } else {
        fill(0);
      }
      rect(x*cellSize, y*cellSize, cellSize, cellSize);
      rect((width-cellSize) - x*cellSize, y*cellSize, cellSize, cellSize);
    }
  }

  stroke(0);
  line(width/2, 0, width/2, height);
}

void keyPressed() {
  if (key == ' ') {
    for (int i = 0; i<(cellNums/2)*cellNums; i++) {
      cellColor[i] = 0;
    }
  }
}

void mouseDragged() {
  if (mouseX > 0 && mouseX < width/2 && mouseY > 0 && mouseY < height) {
    int index = floor(mouseX/cellSize + (mouseY/cellSize)*(cellNums/2));
    cellColor[index] = 1;
  }
}