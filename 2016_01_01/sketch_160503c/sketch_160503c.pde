int n = 15;
float angOffset = 0.0;
float radOffset = 0.0;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  background(255);
  fill(0);
  beginShape();
  noStroke();
  for (int i=0; i<n; i++) {
    float t1 = (TWO_PI/n)*i;
    float t2;
    if (i >= n-1) {
      t2 = 0.0;
    } else {
      t2 = (TWO_PI/n)*(i+1);
    }
    float offset = radians(angOffset);
    float cx1 = width/2 + cos(t1-offset)*radOffset;
    float cy1 = height/2 + sin(t1-offset)*radOffset;
    float ax1 = width/2 + cos(t1)*300;
    float ay1 = height/2 + sin(t1)*300;

    float ax2 = width/2 + cos(t2)*300;
    float ay2 = height/2 + sin(t2)*300;
    float cx2 = width/2 + cos(t2+offset)*radOffset;
    float cy2 = height/2 + sin(t2+offset)*radOffset;
    
    if(i == 0){
      vertex(ax1, ay1);
      bezierVertex(cx1, cy1, cx2, cy2, ax2, ay2);
    }else{
      bezierVertex(cx1, cy1, cx2, cy2, ax2, ay2);
    }
  }
  endShape();
  angOffset = map(mouseX, 0, width, 0, 45);
  radOffset = map(mouseY, 0, height, 0, height/2);
}

void keyPressed(){
  if(key == '3'){
    n = 3;
  }else if(key == '4'){
    n = 4;
  }else if(key == '5'){
    n = 5;
  }else if(key == '6'){
    n = 6;
  }else if(key == '7'){
    n = 7;
  }else if(key == '8'){
    n = 8;
  }else if(key == '9'){
    n = 9;
  }else if(key == '0'){
    n = 28;
  }
}