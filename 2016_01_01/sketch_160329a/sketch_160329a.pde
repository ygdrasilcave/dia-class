int num = 50;
float dia;
float lineSize;
float t = 0.0;

void setup() {
  size(800, 800);
  background(255);
  dia = width/num;
}

void draw() {
  background(255);
  noStroke();
  for (int y=0; y<num; y++) {
    for (int x=0; x<num; x++) {
      fill(0);
      //ellipse(x*dia+dia/2, y*dia+dia/2, dia, dia);
      //line(x*dia+dia/2-lineSize, y*dia+dia/2, x*dia+dia/2+lineSize, y*dia+dia/2);
      //line(x*dia+dia/2, y*dia+dia/2-lineSize, x*dia+dia/2, y*dia+dia/2+lineSize);
      ellipse(x*dia+dia/2-lineSize, y*dia+dia/2, 10, 10);
      ellipse(x*dia+dia/2+lineSize, y*dia+dia/2, 10, 10);
      ellipse(x*dia+dia/2, y*dia+dia/2-lineSize, 10, 10);
      ellipse(x*dia+dia/2, y*dia+dia/2+lineSize, 10, 10);
      /*beginShape(QUAD);  
      vertex(x*dia+dia/2+lineSize, y*dia+dia/2);
      vertex(x*dia+dia/2, y*dia+dia/2-lineSize);
      vertex(x*dia+dia/2-lineSize, y*dia+dia/2);
      vertex(x*dia+dia/2, y*dia+dia/2+lineSize);     
      endShape();
      beginShape(QUAD);  
      vertex(x*dia+dia/2+lineSize+10, y*dia+dia/2);
      vertex(x*dia+dia/2+10, y*dia+dia/2-lineSize);
      vertex(x*dia+dia/2-lineSize-10, y*dia+dia/2);
      vertex(x*dia+dia/2-10, y*dia+dia/2+lineSize);     
      endShape();*/
    }
  }
  lineSize = abs(sin(t))*dia/2;
  t += 0.058;
  println(lineSize);
  num = int(map(mouseX, 0, width, 2, 30));
  dia = width/num;
}