int num = 50;
float dia;
float lineSizeX;
float lineSizeY;
float t = 0.0;

void setup() {
  size(800, 800);
  background(255);
  dia = width/num;
}

void draw() {
  background(255);
  noStroke();
  for (int y=0; y<num; y++) {
    for (int x=0; x<num; x++) {
      fill(0);
      //ellipse(x*dia+dia/2, y*dia+dia/2, dia, dia);
      //line(x*dia+dia/2-lineSize, y*dia+dia/2, x*dia+dia/2+lineSize, y*dia+dia/2);
      //line(x*dia+dia/2, y*dia+dia/2-lineSize, x*dia+dia/2, y*dia+dia/2+lineSize);
      fill(0);
      noStroke();
      ellipse(x*dia+dia/2-lineSizeX, y*dia+dia/2, 10, 10);
      ellipse(x*dia+dia/2, y*dia+dia/2+lineSizeY, 10, 10);
      noFill();
      stroke(0);
      ellipse(x*dia+dia/2+lineSizeX, y*dia+dia/2, 10, 10);
      ellipse(x*dia+dia/2, y*dia+dia/2-lineSizeY, 10, 10);
      
      /*beginShape(QUAD);  
       vertex(x*dia+dia/2+lineSize, y*dia+dia/2);
       vertex(x*dia+dia/2, y*dia+dia/2-lineSize);
       vertex(x*dia+dia/2-lineSize, y*dia+dia/2);
       vertex(x*dia+dia/2, y*dia+dia/2+lineSize);     
       endShape();
       beginShape(QUAD);  
       vertex(x*dia+dia/2+lineSize+10, y*dia+dia/2);
       vertex(x*dia+dia/2+10, y*dia+dia/2-lineSize);
       vertex(x*dia+dia/2-lineSize-10, y*dia+dia/2);
       vertex(x*dia+dia/2-10, y*dia+dia/2+lineSize);     
       endShape();*/
    }
  }
  lineSizeX = abs(sin(t))*dia/2;
  lineSizeY = abs(cos(t))*dia/2;
  t += 0.058;
  println(lineSizeX);
  num = int(map(mouseX, 0, width, 2, 30));
  dia = width/num;
}