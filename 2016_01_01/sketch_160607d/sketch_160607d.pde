int num = 10;
float w = 100;

float t = 1.0;
float t1 = 0.0;

void setup() {
  size(1600, 800);
  background(0);
  textAlign(CENTER, CENTER);
}

void draw() {
  fill(0, 120);
  rect(0, 0, width, height);
  background(0);

  pushMatrix();

  strokeWeight(2);

  stroke(255);
  noFill();
  translate(width/4, height/2);

  ellipse(0, 0, 20, 20);

  float px=0, py=0;
  float startX=0, startY=0;
  for (int i=0; i<num; i++) {
    w = abs(sin(t))*120+60;
    float x = cos(i*(TWO_PI/num))*w;
    float y = sin(i*(TWO_PI/num))*w;
    if (i == 0) {
      startX = x;
      startY = y;
    }
    if (i>0) {
      line(px, py, x, y);
    }

    ellipse(x, y, 10, 10);
    line(0, 0, x, y);

    px = x;
    py = y;
    t+=0.002;
  }
  line(px, py, startX, startY);


  for (int i=0; i<num; i++) {
    pushMatrix();
    rotate(i*(TWO_PI/num));

    for (int j=0; j<30; j++) {   
      float x = j*8;
      //float y = cos(t1)*30;
      float y = map(noise(t1), 0, 1, -1, 1)*40;
      //ellipse(x, y, 10, 10);
      fill(255);
      noStroke();
      textSize(20);
      text("!", x, y);
      t1 += 0.065;
    }
    popMatrix();
  }

  popMatrix();


  pushMatrix();

  strokeWeight(2);

  stroke(255);
  noFill();
  translate(3*(width/4), height/2);

  ellipse(0, 0, 20, 20);

  float px1=0, py1=0;
  float startX1=0, startY1=0;
  for (int i=0; i<num; i++) {
    w = abs(sin(t))*120+60;
    float x = cos(i*(TWO_PI/num))*w;
    float y = sin(i*(TWO_PI/num))*w;
    if (i == 0) {
      startX1 = x;
      startY1 = y;
    }
    if (i>0) {
      line(px1, py1, x, y);
    }

    ellipse(x, y, 10, 10);
    line(0, 0, x, y);

    px1 = x;
    py1 = y;
    t+=0.002;
  }
  line(px1, py1, startX1, startY1);


  for (int i=0; i<num; i++) {
    pushMatrix();
    rotate(i*(TWO_PI/num));

    for (int j=0; j<30; j++) {   
      float x = j*8;
      float y = cos(t1)*30;
      //float y = map(noise(t1), 0, 1, -1, 1)*40;
      //ellipse(x, y, 10, 10);
      fill(255);
      noStroke();
      textSize(15);
      text("***", x, y);
      t1 += 0.2;
    }
    popMatrix();
  }

  popMatrix();

  fill(255);
  noStroke();
  textSize(40);
  
  text("sick", width/4, height-100);
  text("and", width/2, height-100);
  text("tired", 3*(width/4), height-100);
}