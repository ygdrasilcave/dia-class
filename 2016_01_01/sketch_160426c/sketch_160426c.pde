int n = 120;
float[] r;
float[] t;
float[] tAmp;

void setup() {
  size(800, 800);
  background(0);
  r = new float[n];
  t = new float[n];
  tAmp = new float[n];
  for (int i=0; i<n; i++) {
    r[i] = 0.0;
    t[i] = 0.0;
    //tAmp[i] = random(0.012, 0.045);
    tAmp[i] = 0.012;
  }
}


void draw() {
  background(0);
  beginShape();
  noFill();
  stroke(255);
  strokeWeight(3);
  float st = 0.0;
  for (int i=0; i<n; i++) {
    float tn = (TWO_PI/n)*i;
    float x = 0;
    float y = 0;
    x = width/2 + cos(tn)*r[i];
    y = height/2 + sin(tn)*r[i];
    vertex(x, y);
    //line(width/2, height/2, x, y);

    r[i] = noise(st+t[i])*width/2;
    t[i] += tAmp[i];
    st+=0.023;
  }
  endShape(CLOSE);
}

/*void keyPressed(){
 for(int i=0; i<n; i++){
 r[i] = random(20, 300);
 }
 }*/