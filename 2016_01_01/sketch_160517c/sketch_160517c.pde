import de.looksgood.ani.*;

float w = 0;
int n = 5;
float r = 0;
float g = 0;
float b = 0;
boolean br = true;
boolean bg = true;
boolean bb = true;

void setup() {
  size(800, 800);
  smooth();
  noStroke();

  Ani.init(this);
}

void draw() {
  background(255);
  noStroke();
  fill(r, g, b);
  rect(0, 0, width, height);

  fill(0); 
  stroke(110);
  for (int i=0; i<n; i++) {
    float angleStep = TWO_PI/n;
    float x = width/2 + cos(angleStep*i)*(w);
    float y = height/2 - sin(angleStep*i)*(w);
    float x2, y2;
    if (i < n-1) {
      x2 = width/2 + cos(angleStep*(i+1))*(w);
      y2 = height/2 - sin(angleStep*(i+1))*(w);
    } else {
      x2 = width/2 + cos(0)*(w);
      y2 = height/2 - sin(0)*(w);
    }
    ellipse(x, y, 20, 20);
    line(x, y, x2, y2);
  }
}

void keyPressed() {
  if (key == 'a') {
    Ani.to(this, 1.5, "w", width/2-20, Ani.BACK_IN_OUT);
  }
  if (key == 's') {
    Ani.to(this, 3.5, "w", 0, Ani.BACK_IN_OUT);
  }
  if (key == 'r') {
    if (br == true) {
      Ani.to(this, 1, "r", 0, Ani.BOUNCE_IN_OUT);
      br = false;
    } else {
      Ani.to(this, 1, "r", 255, Ani.BOUNCE_IN_OUT);
      br = true;
    }
  }
  if (key == 'g') {
    if (bg == true) {
      Ani.to(this, 1, "g", 0, Ani.BOUNCE_IN_OUT);
      bg = false;
    } else {
      Ani.to(this, 1, "g", 255, Ani.BOUNCE_IN_OUT);
      bg = true;
    }
  }
  if (key == 'b') {
    if (bb == true) {
      Ani.to(this, 1, "b", 0, Ani.BOUNCE_IN_OUT);
      bb = false;
    } else {
      Ani.to(this, 1, "b", 255, Ani.BOUNCE_IN_OUT);
      bb = true;
    }
  }
}