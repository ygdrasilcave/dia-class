float w = 0;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  background(255);
  stroke(0);
  fill(255, 255, 0);
  beginShape();
  for (int i=0; i<6; i++) {
    float x = width/2 + cos(radians(60*i+30))*w;
    float y = height/2 + sin(radians(60*i+30))*w;
    vertex(x, y);
    if(i == 0){
      ellipse(x, y, 30, 30);
    }
  }
  endShape(CLOSE);
  w = map(mouseX, 0, width, 10, width/2);
}