float t = 0.0;

void setup() {
  size(800, 200);
  background(0);
}

void draw() {
  background(0);
  stroke(255);
  strokeWeight(3);
  line(width/2, height/3, width/2+sin(t)*width/2, height/3);
  line(width/2, 2*(height/3), width/2+cos(t)*width/2, 2*(height/3));
  t += 0.0089;
}