int r = 0;
int g = 0;
int b = 0;
int dia = 5;

void setup() {
  size(800, 600);
  background(255);
}

void draw() {
  fill(r, g, b);
  noStroke();
  ellipse(mouseX, mouseY, dia, dia);

  if (mousePressed == true) {
    r = 255;
    g = 255;
    b = 255;
    dia = 30;
  } else {
    g = g+10;
    if (g > 255) {
      g = 0;
    }
    b = b+5;
    if (b > 255) {
      b = 0;
    }
    dia = 5;
  }
}

void keyPressed() {
  saveFrame("myWork.jpg");
}