int n = 3;
float angleOffset = 0.0;
float controlRadOffset = 0.0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  fill(255);
  stroke(0);
  beginShape();
  for (int i=0; i<n; i++) {
    float t1 = (TWO_PI/n)*i;
    float t2 = (TWO_PI/n)*(i+1);

    float ax1 = width/2 + cos(t1)*350;
    float ay1 = height/2 + sin(t1)*350;
    float cx1 = width/2 + cos(t1-angleOffset)*(350+controlRadOffset);
    float cy1 = height/2 + sin(t1-angleOffset)*(350+controlRadOffset);

    float ax2 = width/2 + cos(t2)*350;
    float ay2 = height/2 + sin(t2)*350;
    float cx2 = width/2 + cos(t2+angleOffset)*(350+controlRadOffset);
    float cy2 = height/2 + sin(t2+angleOffset)*(350+controlRadOffset);
    if (i == 0) {
      vertex(ax1, ay1);
      bezierVertex(cx1, cy1, cx2, cy2, ax2, ay2);
    } else {
      bezierVertex(cx1, cy1, cx2, cy2, ax2, ay2);
    }
  }
  endShape();
  angleOffset = map(mouseX, 0, width, -PI/2, PI/2);
  controlRadOffset = map(mouseY, 0, height, -150, 150);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      n++;
      if (n >= 128) {
        n = 128;
      }
    } else if (keyCode == DOWN) {
      n--;
      if (n <= 3) {
        n = 3;
      }
    }
  }
}