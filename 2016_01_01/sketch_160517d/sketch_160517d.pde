import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;
Easing[] easings = { Ani.LINEAR, 
  Ani.QUAD_IN, Ani.QUAD_OUT, 
  Ani.QUAD_IN_OUT, Ani.CUBIC_IN, 
  Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, 
  Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, 
  Ani.QUINT_OUT, Ani.QUINT_IN_OUT, 
  Ani.SINE_IN, Ani.SINE_OUT, 
  Ani.SINE_IN_OUT, Ani.CIRC_IN, 
  Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, 
  Ani.EXPO_IN_OUT, Ani.BACK_IN, 
  Ani.BACK_OUT, Ani.BACK_IN_OUT, 
  Ani.BOUNCE_IN, Ani.BOUNCE_OUT, 
  Ani.BOUNCE_IN_OUT, Ani.ELASTIC_IN, 
  Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT};
String[] easingsVariableNames = {"Ani.LINEAR",
"Ani.QUAD_IN", "Ani.QUAD_OUT",
"Ani.QUAD_IN_OUT", "Ani.CUBIC_IN",
"Ani.CUBIC_IN_OUT", "Ani.CUBIC_OUT",
"Ani.QUART_IN", "Ani.QUART_OUT",
"Ani.QUART_IN_OUT", "Ani.QUINT_IN",
"Ani.QUINT_OUT", "Ani.QUINT_IN_OUT",
"Ani.SINE_IN", "Ani.SINE_OUT",
"Ani.SINE_IN_OUT", "Ani.CIRC_IN",
"Ani.CIRC_OUT", "Ani.CIRC_IN_OUT",
"Ani.EXPO_IN", "Ani.EXPO_OUT",
"Ani.EXPO_IN_OUT", "Ani.BACK_IN",
"Ani.BACK_OUT", "Ani.BACK_IN_OUT",
"Ani.BOUNCE_IN", "Ani.BOUNCE_OUT",
"Ani.BOUNCE_IN_OUT", "Ani.ELASTIC_IN",
"Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT"};

int counter = 0;
float beginW = 0;
float endW = 0;
float durationW = 0;

float w = 0;
int n = 16;
float r = 0;
float g = 0;
float b = 0;
boolean br = false;
boolean bg = false;
boolean bb = false;

Ani ani_w;
Ani ani_r;
Ani ani_g;
Ani ani_b;

void setup() {
  size(800, 800);
  smooth();
  noStroke();

  Ani.init(this);
  ani_w = new Ani(this, 1.5, "w", width/2-20, Ani.BACK_IN_OUT);
  ani_r = new Ani(this, 1, "r", 0, Ani.BOUNCE_IN_OUT);
  ani_g = new Ani(this, 1, "g", 0, Ani.BOUNCE_IN_OUT);
  ani_b = new Ani(this, 1, "b", 0, Ani.BOUNCE_IN_OUT);
  
  textAlign(CENTER, CENTER);
}

void draw() {
  background(255);
  noStroke();
  fill(r, g, b);
  rect(0, 0, width, height);

  fill(255); 
  stroke(255);
  for (int i=0; i<n; i++) {
    float angleStep = TWO_PI/n;
    float x = width/2 + cos(angleStep*i)*(w);
    float y = height/2 - sin(angleStep*i)*(w);
    float x2, y2;
    if (i < n-1) {
      x2 = width/2 + cos(angleStep*(i+1))*(w);
      y2 = height/2 - sin(angleStep*(i+1))*(w);
    } else {
      x2 = width/2 + cos(0)*(w);
      y2 = height/2 - sin(0)*(w);
    }
    ellipse(x, y, 20, 20);
    line(x, y, x2, y2);
  }
  
  fill(255, 0, 0);
  ellipse(w, height/2, 30, 30);
  ellipse(width-w, height/2, 30, 30);
  printInfo(beginW, endW, durationW, easingsVariableNames[counter]);
}

void keyPressed() {
  if (key == 'a') {
    float begin = ani_w.getPosition();
    float end = width/2-100+random(-100, 100);
    float duration = random(1, 3);
    ani_w.setBegin(begin);
    ani_w.setEnd(end);
    ani_w.setDuration(duration);
    ani_w.setEasing(easings[counter]);
    ani_w.start();
    counter++;
    if (counter > easings.length-1) {
      counter = 0;
    }
    beginW = begin;
    endW = end;
    durationW = duration;
  }
  if (key == 's') {
    float begin = ani_w.getPosition();
    float end = random(0, 100);
    float duration = random(1, 3);
    ani_w.setBegin(begin);
    ani_w.setEnd(end);
    ani_w.setDuration(duration);
    ani_w.setEasing(easings[counter]);
    ani_w.start();
    counter++;
    if (counter > easings.length-1) {
      counter = 0;
    }
    beginW = begin;
    endW = end;
    durationW = duration;
  }
  if (key == 'r') {
    if (br == true) {
      ani_r.setBegin(ani_r.getPosition());
      ani_r.setEnd(0);
      ani_r.setDuration(random(1, 2));
      ani_r.start();
      br = false;
    } else {
      ani_r.setBegin(ani_r.getPosition());
      ani_r.setEnd(255);
      ani_r.setDuration(random(1, 2));
      ani_r.start();
      br = true;
    }
  }
  if (key == 'g') {
    if (bg == true) {
      ani_g.setBegin(ani_g.getPosition());
      ani_g.setEnd(0);
      ani_g.setDuration(random(1, 2));
      ani_g.start();
      bg = false;
    } else {
      ani_g.setBegin(ani_g.getPosition());
      ani_g.setEnd(255);
      ani_g.setDuration(random(1, 2));
      ani_g.start();
      bg = true;
    }
  }
  if (key == 'b') {
    if (bb == true) {
      ani_b.setBegin(ani_b.getPosition());
      ani_b.setEnd(0);
      ani_b.setDuration(random(1, 2));
      ani_b.start();
      bb = false;
    } else {
      ani_b.setBegin(ani_b.getPosition());
      ani_b.setEnd(255);
      ani_b.setDuration(random(1, 2));
      ani_b.start();
      bb = true;
    }
  }
}

void printInfo(float _begin, float _end, float _duration, String _easing) {
  fill(255);
  textSize(42);
  text(nf(_begin,0,2) + " to " + nf(_end,0,2) + " in " + nf(_duration,0,2) + "\nwith " + _easing, width/2, height-100);
}