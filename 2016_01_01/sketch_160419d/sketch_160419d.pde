void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
}

void draw() {
  background(255, 0, 50);
  
  stroke(0);
  fill(156, 100, 70);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = width/2 + cos(radians(60*i-30))*200;
    float y = height/2 + sin(radians(60*i-30))*200;
    vertex(x, y);
  }
  vertex(width/2, height/2);
  endShape(CLOSE);
 
  stroke(0);
  fill(156, 100, 20);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = width/2 + cos(radians(60*i+90))*200;
    float y = height/2 + sin(radians(60*i+90))*200;
    vertex(x, y);
  }
  vertex(width/2, height/2);
  endShape(CLOSE);
  
  stroke(0);
  fill(156, 100, 100);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = width/2 + cos(radians(60*i+210))*200;
    float y = height/2 + sin(radians(60*i+210))*200;
    vertex(x, y);
  }
  vertex(width/2, height/2);
  endShape(CLOSE);
}