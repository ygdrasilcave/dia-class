float w = 0;
int n = 3;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  background(255);
  stroke(0);
  fill(255, 255, 0);
  beginShape();
  for (int i=0; i<n; i++) {
    if (i%2 == 0) {
      float x = width/2 + cos(radians((360.0/n)*i))*(w);
      float y = height/2 + sin(radians((360.0/n)*i))*(w);
      vertex(x, y);
      ellipse(x, y, 20, 20);
    } else {
      float x = width/2 + cos(radians((360.0/n)*i))*(w-100);
      float y = height/2 + sin(radians((360.0/n)*i))*(w-100);
      vertex(x, y);
      ellipse(x, y, 20, 20);
    }
  }
  endShape(CLOSE);
  w = map(mouseX, 0, width, 10, width/2);
  n = int(map(mouseY, 0, height, 3, 64));
}