float w = 40;

void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
}

void draw() {
  background(255);

  for (int y1=0; y1<20; y1++) {
    for (int x1=0; x1<10; x1++) {
      stroke(0);
      fill(random(360), 100, 100);
      beginShape();
      for (int i=0; i<6; i++) {
        float x = (x1*w*3) + cos(radians(i*60))*w;
        float y = (y1*cos(radians(30))*w*2) + sin(radians(i*60))*w;
        vertex(x, y);
      }
      endShape(CLOSE);
    }
  }

  for (int y1=0; y1<20; y1++) {
    for (int x1=0; x1<10; x1++) {
      stroke(0);
      fill(random(360), 100, 100);
      beginShape();
      for (int i=0; i<6; i++) {
        float x = (w+w/2)+(x1*w*3) + cos(radians(i*60))*w;
        float y = (cos(radians(30))*w)+(y1*cos(radians(30))*w*2) + sin(radians(i*60))*w;
        vertex(x, y);
      }
      endShape(CLOSE);
    }
  }
}