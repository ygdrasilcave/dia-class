int cellNums = 100;
float cellSize;
int[] colorSet;
int[] nextSet;
int counter = 0;

void setup() {
  size(1000, 1000);
  cellSize = width/cellNums;
  colorSet = new int[cellNums];
  nextSet = new int[8];
  for (int i=0; i<cellNums; i++) {     
    /*float r = random(1);
    if (r > 0.5) {
      colorSet[i] = 1;
    } else {
      colorSet[i] = 0;
    }*/
    if(i == cellNums/2){
      colorSet[i] = 0;
    }else{
      colorSet[i] = 1;
    }
  }
  for (int i=0; i<8; i++) {
    float r = random(1);
    if (r > 0.5) {
      nextSet[i] = 1;
    } else {
      nextSet[i] = 0;
    }
  }
  background(255);
}

void draw() {
  for (int x=0; x<cellNums; x++) {
    //stroke(0);
    noStroke();
    fill(colorSet[x]*255);
    rect(x*cellSize, counter*cellSize, cellSize, cellSize);
  }
}

void keyPressed() {
  if (key == ' ') {
    counter++;
    if(counter > floor(height/cellSize)){
      counter = 0;
      background(255);
    }
    for (int i=1; i<cellNums-1; i++) {
      if (colorSet[i-1] == 1 && colorSet[i] == 1 && colorSet[i+1] == 1) {
        colorSet[i] = nextSet[0];
      } else if (colorSet[i-1] == 0 && colorSet[i] == 0 && colorSet[i+1] == 0) {
        colorSet[i] = nextSet[1];
      } else if (colorSet[i-1] == 1 && colorSet[i] == 1 && colorSet[i+1] == 0) {
        colorSet[i] = nextSet[2];
      } else if (colorSet[i-1] == 1 && colorSet[i] == 0 && colorSet[i+1] == 1) {
        colorSet[i] = nextSet[3];
      } else if (colorSet[i-1] == 0 && colorSet[i] == 1 && colorSet[i+1] == 1) {
        colorSet[i] = nextSet[4];
      } else if (colorSet[i-1] == 1 && colorSet[i] == 0 && colorSet[i+1] == 0) {
        colorSet[i] = nextSet[5];
      } else if (colorSet[i-1] == 0 && colorSet[i] == 1 && colorSet[i+1] == 0) {
        colorSet[i] = nextSet[6];
      } else if (colorSet[i-1] == 0 && colorSet[i] == 0 && colorSet[i+1] == 1) {
        colorSet[i] = nextSet[7];
      }
    }
  }
  if(key == 'r'){
    for (int i=0; i<8; i++) {
    float r = random(1);
    if (r > 0.5) {
      nextSet[i] = 1;
    } else {
      nextSet[i] = 0;
    }
  }
  }
}