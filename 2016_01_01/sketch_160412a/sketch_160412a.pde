int num = 20;
float w;
float over;
float t = 0.0;
boolean mode = true;

void setup() {
  size(800, 800);
  background(255);
  w = width/num;
  over = w*random(1);
}

void draw() {
  background(255);
  if (mode == true) {
    noFill();
    stroke(0);
  } else {
    fill(255);
    stroke(0);
  }
  for (int y=0; y<num; y++) {
    for (int x=0; x<num; x++) {
      if (y%2==0) {
        ellipse(x*w, y*w+w/2, w+over, w+over);
        point(x*w, y*w+w/2);
      } else {
        ellipse(x*w+w/2, y*w+w/2, w+over, w+over);
        point(x*w+w/2, y*w+w/2);
      }
    }
  }

  over = w*noise(t);
  t += 0.045;
}

void keyPressed() {
  mode = !mode;
}