int x = 0;
int y = 0;

int xSpeed = 2;
int ySpeed = 5;

int divX = 1;
int divY = 1;

void setup() {
  size(600, 600);
  background(255);
}

void draw() {
  noFill();
  stroke(0);
  rect(x, y, width/divX-x, height/divY-y);

  x = x+xSpeed;
  y = y+ySpeed;
  if (x > width && y > height) {
    saveFrame("myPattern_#####.jpg");
    x = 0;
    y = 0;
    background(255);
    xSpeed = int(random(1, 6));
    ySpeed = int(random(2, 8));
    divX = int(random(1, 4));
    divY = int(random(1, 4));
  }
}

void keyPressed() {
  x = x+xSpeed;
  y = y+ySpeed;
  if (x > width && y > height) {
    saveFrame("myPattern_#####.jpg");
    x = 0;
    y = 0;
    background(255);
    xSpeed = int(random(1, 6));
    ySpeed = int(random(2, 8));
    divX = int(random(1, 4));
    divY = int(random(1, 4));
  }
}