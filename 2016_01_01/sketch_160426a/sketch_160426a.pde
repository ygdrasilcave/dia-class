int n = 4;
float[] t;
float[] noiseR;
float[] tAmp;

void setup(){
  size(800, 800);
  background(0);
  noiseR = new float[n];
  t = new float[n];
  tAmp = new float[n];
  for(int i=0; i<n; i++){
    noiseR[i] = 0.0;
    t[i] = 0.0;
    tAmp[i] = random(0.012, 0.045);
  }
}

void draw(){
  background(0);
  beginShape();
  stroke(255);
  strokeWeight(3);
  noFill();
  for(int i=0; i<n; i++){
    float theta = TWO_PI/n;
    float x = width/2 + cos(theta*i)*width/2*noiseR[i];
    float y = height/2 + sin(theta*i)*height/2*noiseR[i];
    vertex(x, y);
    line(width/2, height/2, x, y);    
    noiseR[i] = noise(t[i]);
    t[i] += tAmp[i];
  }
  endShape(CLOSE);
  
}