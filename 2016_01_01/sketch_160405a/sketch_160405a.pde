float t = 0.0;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  int div = int(((cos(t)+1)*5)+1);
  for (int x=0; x<30; x++) {
    if (x%2 == 0) {
      noFill();
      stroke(0);
      drawMyShape(0, 30, x, 80, div);
    }else{
      fill(0);
      noStroke();
      drawMyShape(0, 30, x, 50, div);
    }
  }
  
  //drawMyShape(0, 30, 3, 50, 1);
  
  t+=0.035;
  //println(sum(349, 97487));
}

void drawMyShape(int _start, int _spaceX, int _step, int _num, int _div) {
  float x = _start;
  float spaceX = _spaceX;
  float y = height;
  float dia = 50;
  for (int i=0; i<_num; i++) {
    ellipse(x+spaceX*_step, y-(i*dia/_div*1), dia-map(i, 0, _num, 0, 48), dia-map(i, 0, _num, 0, 48));
  }
}

int sum(int _x, int _y) {
  int result = _x + _y;
  return result;
}