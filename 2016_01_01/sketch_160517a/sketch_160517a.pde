float x = 0.0;
float y = 0.0;
float tx;
float ty;

void setup() {
  size(600, 600);
}

void draw() {
  background(0);
  fill(255);
  ellipse(x, y, 50, 50);
  
  float diffX = tx - x;
  float diffY = ty - y;
  x = x + diffX*0.15;
  y = y + diffY*0.15;
  
  if(mousePressed == true){
    tx = mouseX;
    ty = mouseY;
  }
}