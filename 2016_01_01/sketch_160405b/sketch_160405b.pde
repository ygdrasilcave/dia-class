boolean f = true;
boolean b = false;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  //fill(0, 5);
  //noStroke();
  //rect(0,0,width,height);
  if (b == true) {
    background(0);
    b = false;
  }
  if (f == true) {
    stroke(255);
    fill(0);
  } else {
    stroke(255);
    noFill();
  }
  if (mousePressed) {
    drawMyShape(int(mouseX), 1, 1, int(random(2, 80)), int(random(5, 15)));
  }
}

void drawMyShape(int _start, int _spaceX, int _step, int _num, int _div) {
  float x = _start;
  float spaceX = _spaceX;
  float y = height;
  float dia = 50;
  for (int i=0; i<_num; i++) {
    ellipse(x+spaceX*_step, y-(i*dia/_div*1), dia-map(i, 0, _num, 0, 48), dia-map(i, 0, _num, 0, 48));
  }
}

void keyPressed() {
  if (key == 'f') {
    f = !f;
  } else if (key == ' ') {
    b = true;
  }
}