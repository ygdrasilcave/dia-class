float t=0;

void setup() {
  size(1000, 1000);
}

void draw() {
  background(0);

  translate(width/2, height/2);
  for (int j=0; j<360; j+=15) {
    float cx = cos(radians(j))*100;
    float cy = sin(radians(j))*map(mouseX, 0, width, 10, 200);
    for (int i=0; i<100; i+=5) {
      //float a = sin(map(mouseX, 0, width, -1, 1))*i*map(mouseY, 0, height, 0, 1);
      float a = sin(map(mouseX, 0, width, -1, 1))*i*noise(t);
      float x = cos(radians(a+j))*i*3;
      float y = sin(radians(a+j))*i*3;
      if (j%2 == 0) {
        fill(255, 40);
        stroke(0);
        strokeWeight(1);
      } else {
        fill(frameCount%255, 0, 0, 40);
        stroke(0);
        strokeWeight(1);
      }
      ellipse(cx+x, cy+y, 100-i, 100-i);
    }
    t+=0.06;
  }
  fill(0);
  strokeWeight(5);
  stroke(255, 0, 0);
  ellipse(cos(-QUARTER_PI)*100, sin(-QUARTER_PI)*100, 30, 20);
  ellipse(cos(-QUARTER_PI*3)*100, sin(-QUARTER_PI*3)*100, 30, 20);
}