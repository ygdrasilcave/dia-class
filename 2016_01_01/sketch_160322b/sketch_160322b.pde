int rectNum = 10;

void setup() {
  size(600, 600);
  background(255);
  colorMode(HSB, rectNum, rectNum, 100);
}

void draw() {
  noStroke();
  for (int j=0; j<rectNum; j=j+1) {
    for (int i=0; i<rectNum; i=i+1) {
      stroke(0);
      fill(i, j, 100);
      rect(i*width/rectNum, j*height/rectNum, width/rectNum, height/rectNum);
    }
  }
  //rectNum = int(map(mouseX, 0, width, 1, 100));
}

void keyPressed() {
}