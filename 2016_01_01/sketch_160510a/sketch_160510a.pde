import netP5.*;
import oscP5.*;
OscP5 server;
NetAddress sonicPi;

StringList myCode3;
StringList getlucky;

void setup(){
  size(600, 600);
  server = new OscP5(this, 12000);
  sonicPi = new NetAddress("localhost", 4557);
  myCode3 = new StringList();
  getlucky = new StringList();
  String lines[] = loadStrings("code3.txt");
  for(int i=0; i<lines.length; i++){
    myCode3.set(i, trim(lines[i]));
  }
  String lines2[] = loadStrings("Sonic_Pi_code_for_Get_Lucky_by_Daft_Punk.txt");
  for(int i=0; i<lines2.length; i++){
    getlucky.set(i, trim(lines2[i]));
  }
  println(myCode3);
}

void keyPressed(){
  if(key == '1'){
    OscMessage msg = new OscMessage("/run-code");
    msg.add("id");
    String code = "";
    code += "live_loop :loop do\n";
    code += "play 70\n";
    code += "sleep " + random(0.125, 1) + "\n";
    code += "end\n";
    msg.add(code);
    server.send(msg, sonicPi);
  }
  if(key == '2'){
    OscMessage msg = new OscMessage("/run-code");
    msg.add("id");
    String code = "";
    code += "use_synth :piano\n";
    code += "live_loop :pianoLoop do\n";
    code += "n = (scale :d3, :minor_pentatonic, num_octaves: 3).choose\n";
    code += "if rand > 0.5 then\n";
    code += "play (chord n, 5)\n";
    code += "end\n";
    code += "sleep 0.125\n";
    code += "end\n";
    msg.add(code);
    server.send(msg, sonicPi);
  }
  if(key == '3'){
    OscMessage msg = new OscMessage("/run-code");
    msg.add("id");
    String code = "";
    for(int i=0; i<myCode3.size(); i++){
      code += (myCode3.get(i) + "\n");
    }
    println(code);
    msg.add(code);
    server.send(msg, sonicPi);
  }
  if(key == '4'){
    OscMessage msg = new OscMessage("/run-code");
    msg.add("id");
    String code = "";
    for(int i=0; i<getlucky.size(); i++){
      code += (getlucky.get(i) + "\n");
    }
    println(code);
    msg.add(code);
    server.send(msg, sonicPi);
  }
  if(key == ' '){
    OscMessage msg = new OscMessage("/stop-all-jobs");
    msg.add("id");
    server.send(msg, sonicPi);
  }
}

void draw(){
}