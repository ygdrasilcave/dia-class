int n = 6;
float angOffset = 0.0;
float radOffset = 0.0;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  background(255);
  noFill();
  for (int i=0; i<n; i++) {
    float t1 = (TWO_PI/n)*i;
    float t2;
    if (i >= n-1) {
      t2 = 0.0;
    } else {
      t2 = (TWO_PI/n)*(i+1);
    }
    float offset = radians(angOffset);
    float cx1 = width/2 + cos(t1-offset)*radOffset;
    float cy1 = height/2 + sin(t1-offset)*radOffset;
    float ax1 = width/2 + cos(t1)*300;
    float ay1 = height/2 + sin(t1)*300;

    float ax2 = width/2 + cos(t2)*300;
    float ay2 = height/2 + sin(t2)*300;
    float cx2 = width/2 + cos(t2+offset)*radOffset;
    float cy2 = height/2 + sin(t2+offset)*radOffset;
    strokeWeight(3);
    stroke(0);
    bezier(ax1, ay1, cx1, cy1, cx2, cy2, ax2, ay2);
    if (i==0) {
      stroke(255, 0, 0);
    } else {
      stroke(0, 255, 0);
    }
    line(ax1, ay1, cx1, cy1);
    line(ax2, ay2, cx2, cy2);
  }
  angOffset = map(mouseX, 0, width, 0, 90);
  radOffset = map(mouseY, 0, height, 0, height/2);
}