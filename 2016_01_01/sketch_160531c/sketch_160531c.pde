//    http://mathworld.wolfram.com/ElementaryCellularAutomaton.html
//    https://en.wikipedia.org/wiki/Rule_30

int cellNums = 100;
int[] cellColor; 
float cellSize;
int[] nextColor;

int counter = 0;

int patternNums = 7;
int pattern = 0;
int[][] cellPattern = {{0, 0, 0, 1, 1, 1, 1, 0}, //rule 30
  {0, 1, 1, 1, 1, 1, 1, 0}, //rule 126
  {0, 0, 1, 1, 0, 1, 1, 0}, //rule 54
  {1, 0, 0, 1, 0, 1, 1, 0}, //rule 150
  {0, 0, 1, 1, 1, 1, 0, 0}, //rule 60
  {1, 0, 0, 1, 1, 1, 1, 0}, //rule 158
  {0, 0, 1, 1, 1, 1, 1, 0}   //rule 62
};

void setup() {
  size(1000, 1000); 
  cellColor = new int[cellNums];
  nextColor = new int[cellNums];
  for (int i=0; i<cellNums; i++) {
    float rand = random(1);
    if (rand > 0.5) {
      cellColor[i] = 1;
    } else {
      cellColor[i] = 0;
    }
    nextColor[i] = 0;
  }
  cellSize = width/cellNums;

  background(255);
}

void draw() {
  for (int i=0; i<cellNums; i++) {
    if (cellColor[i] == 1) {
      fill(0);
    } else {
      fill(255);
    }
    noStroke();
    ellipse(i*cellSize, counter*cellSize, cellSize, cellSize);
  }
}

void keyPressed() {
  if (key == ' ') {
    counter++;
    if (counter > floor(height/cellSize)) {
      background(255);
      for (int i=0; i<cellNums; i++) {
        float rand = random(1);
        if (rand > 0.5) {
          cellColor[i] = 1;
        } else {
          cellColor[i] = 0;
        }
        nextColor[i] = 0;
      }
      counter = 0;
      pattern++;
      if (pattern > patternNums-1) {
        pattern = 0;
      }
    }
    for (int i=1; i<cellNums-1; i++) {
      if (cellColor[i-1] == 1 && cellColor[i] == 1 && cellColor[i+1] == 1) {
        nextColor[i] = cellPattern[pattern][0];
      } else if (cellColor[i-1] == 1 && cellColor[i] == 1 && cellColor[i+1] == 0) {
        nextColor[i] = cellPattern[pattern][1];
      } else if (cellColor[i-1] == 1 && cellColor[i] == 0 && cellColor[i+1] == 1) {
        nextColor[i] = cellPattern[pattern][2];
      } else if (cellColor[i-1] == 1 && cellColor[i] == 0 && cellColor[i+1] == 0) {
        nextColor[i] = cellPattern[pattern][3];
      } else if (cellColor[i-1] == 0 && cellColor[i] == 1 && cellColor[i+1] == 1) {
        nextColor[i] = cellPattern[pattern][4];
      } else if (cellColor[i-1] == 0 && cellColor[i] == 1 && cellColor[i+1] == 0) {
        nextColor[i] = cellPattern[pattern][5];
      } else if (cellColor[i-1] == 0 && cellColor[i] == 0 && cellColor[i+1] == 1) {
        nextColor[i] = cellPattern[pattern][6];
      } else if (cellColor[i-1] == 0 && cellColor[i] == 0 && cellColor[i+1] == 0) {
        nextColor[i] = cellPattern[pattern][7];
      }
    }
    for (int i=0; i<cellNums; i++) {
      cellColor[i] = nextColor[i];
    }
  }
}