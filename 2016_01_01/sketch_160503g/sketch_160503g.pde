int n = 30;
float angleOffset = 0.0;
float controlRadOffset = 0.0;
float[] rad;

void setup() {
  size(800, 800);
  background(0);
  rad = new float[n];
  for (int i=0; i<n; i++) {
    rad[i] = random(10, 350);
  }
}

void draw() {
  background(0);

  fill(255);
  stroke(0);
  beginShape();
  for (int i=0; i<n; i++) {
    float t1 = (TWO_PI/n)*i;
    float t2 = (TWO_PI/n)*(i+1);

    float ax1 = width/2 + cos(t1)*rad[i];
    float ay1 = height/2 + sin(t1)*rad[i];
    float cx1 = width/2 + cos(t1-angleOffset)*(rad[i]+controlRadOffset);
    float cy1 = height/2 + sin(t1-angleOffset)*(rad[i]+controlRadOffset);
    
    float nextR = 0.0;
    if (i < n-1) {
      nextR = rad[i+1];
    } else {
      nextR = rad[0];
    }
    float ax2 = width/2 + cos(t2)*nextR;
    float ay2 = height/2 + sin(t2)*nextR;
    float cx2 = width/2 + cos(t2+angleOffset)*(nextR+controlRadOffset);
    float cy2 = height/2 + sin(t2+angleOffset)*(nextR+controlRadOffset);
    if (i == 0) {
      vertex(ax1, ay1);
      bezierVertex(cx1, cy1, cx2, cy2, ax2, ay2);
    } else {
      bezierVertex(cx1, cy1, cx2, cy2, ax2, ay2);
    }
  }
  endShape();
  angleOffset = map(mouseX, 0, width, -PI/2, PI/2);
  controlRadOffset = map(mouseY, 0, height, -150, 150);
}