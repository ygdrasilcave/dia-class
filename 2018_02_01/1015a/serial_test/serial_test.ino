const int dataNum = 3;
int index = 0;
int rawData[dataNum];

const int led_00 = 5;
const int led_01 = 6;
const int led_02 = 9;

const int minVal = 0;
const int maxVal = 255;

const int sw_00 = 7;
const int sw_01 = 8;

int sw_value0 = 0;
int sw_value1 = 0;

const int pot_00 = A0;
const int pot_01 = A1;

int pot_value0 = 0;
int pot_value1 = 1;

void setup()
{
  Serial.begin(115200);

  pinMode(sw_00, INPUT_PULLUP);
  pinMode(sw_01, INPUT_PULLUP);

  for (int i = 0; i < dataNum; i++) {
    rawData[i] = 0;
  }
}

void loop()
{
  if ( Serial.available()) {
    if (index == 0) {
      for (index; index < dataNum; index ++)
      {
        rawData[index] = Serial.parseInt();
        rawData[index] = constrain(rawData[index], minVal, maxVal);
      }

      analogWrite(led_00, rawData[0]);
      analogWrite(led_01, rawData[1]);
      analogWrite(led_02, rawData[2]);
    }
    if (Serial.read() == '\n' && index == dataNum) {
      index = 0;
      //delay(10);
      
      pot_value0 = analogRead(pot_00);
      pot_value1 = analogRead(pot_01);
      Serial.print('P'); //.......................................header
      Serial.print(",");
      Serial.print(pot_value0);
      Serial.print(',');
      Serial.println(pot_value1);
      sw_value0 = digitalRead(sw_00);
      sw_value1 = digitalRead(sw_01);
      Serial.print('S'); //.......................................header
      Serial.print(",");
      Serial.print(sw_value0);
      Serial.print(',');
      Serial.println(sw_value1);
      delay(10);
    }
  }
}

