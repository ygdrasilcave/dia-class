float xPosition = 0;
float yPosition = 0;
float w;
float h;

int position_01 = 0;

float r;
float g;
float b;

void setup(){
  size(600, 600);
  background(110);
  
  if(width > height){
    w = height * 0.5;
    h = w;
  }else{
    w = width * 0.5;
    h = w;
  }
  xPosition = width/2 - w/2;
  yPosition = height/2 - h/2;
  
  //w = 400;
  //h = w;
}

void draw(){ 
  fill(r, g, b);
  rect(xPosition, yPosition, w, h);
}
