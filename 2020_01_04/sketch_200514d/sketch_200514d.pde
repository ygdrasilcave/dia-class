PImage img;

boolean colorType = true;

void setup() {
  size(640, 639);
  background(0);
  img = loadImage("flower.jpg");
}

void draw() {
  background(0, 0, 0);

  float r = map(mouseX, 0, width, 2, 100);
  float w = 2*(cos(radians(30))*r);
  float h = r+r/2;
  int pixelNumX = int(width/w);
  int pixelNumY = int(height/h);
  /*
  println(pixelNumX + " : " + pixelNumY);
   //64 : 63
   println(img.pixels.length);
   //408960
   //640 + 630*640
   */

  pushMatrix();
  translate(w/2, h/2);

  for (int px=0; px<pixelNumX; px++) {
    for (int py=0; py<pixelNumY; py++) {

      int index = px*int(w) + py*int(h)*width;
      color c = img.pixels[index];
      float cr = red(c);
      float cg = green(c);
      float cb = blue(c);

      for (int j=0; j<3; j++) {
        pushMatrix();

        if (py%2 == 0) {
          translate(px*w, py*h);
        } else {
          translate(px*w + cos(radians(30))*r, py*h);
        }

        rotate(radians(120)*j);

        pushMatrix();
        rotate(-(HALF_PI+radians(60)));

        //fill(map(j,0,2,255,0));
        if (colorType == true) {
          if (j == 0) {
            fill(cr, cg, cb);
          } else if (j == 1) {
            fill(cr*0.6, cg*0.6, cb*0.6);
          } else if (j == 2) {
            fill(cr*0.1, cg*0.1, cb*0.1);
          }
        } else {
          if (j == 0) {
            fill(cr, 0, 0);
          } else if (j == 1) {
            fill(0, cg, 0);
          } else if (j == 2) {
            fill(0, 0, cb);
          }
        }
        noStroke();

        beginShape();
        vertex(0, 0);
        for (int i=0; i<3; i++) {
          float theta = radians((360/6)*i);        
          float x = cos(theta)*r;
          float y = sin(theta)*r;
          vertex(x, y);
        }
        endShape();
        popMatrix();

        popMatrix();
      }

      //fill(c);
      //rect(px*r, py*r, r, r);
    }
  }

  popMatrix();

  //image(img, 0, 0, width, height);
}

void keyPressed() {
  if (key == ' ') {
    colorType = !colorType;
  }
}
