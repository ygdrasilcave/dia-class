float w = 20;
float h = 20;

void setup() {
  size(800, 600);
  background(0);
}

void draw() {
  background(0);
  stroke(0);
  fill(255);
  
  
  for (float y = 0; y<height; y = y+h) {
    for (float x = 0; x<width; x = x+w) {
      rect(x, y, w, h);
    }
  }
}
