import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;
Easing[] easings = { Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, 
  Ani.QUAD_IN_OUT, Ani.CUBIC_IN, Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, 
  Ani.QUART_IN, Ani.QUART_OUT, Ani.QUART_IN_OUT, Ani.QUINT_IN, 
  Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, Ani.SINE_OUT, 
  Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, 
  Ani.BACK_OUT, Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, 
  Ani.BOUNCE_IN_OUT, Ani.ELASTIC_IN, Ani.ELASTIC_OUT, 
  Ani.ELASTIC_IN_OUT};
String[] easingsVariableNames = {"Ani.LINEAR", "Ani.QUAD_IN", 
  "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", "Ani.CUBIC_IN", 
  "Ani.CUBIC_IN_OUT", "Ani.CUBIC_OUT", "Ani.QUART_IN", 
  "Ani.QUART_OUT", "Ani.QUART_IN_OUT", "Ani.QUINT_IN", 
  "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", 
  "Ani.CIRC_IN_OUT", "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", 
  "Ani.BACK_IN", "Ani.BACK_OUT", "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", 
  "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", "Ani.ELASTIC_IN", 
  "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT"};

float t = 0;
Ani rotateAni;

float hue = 0;
Ani hueAni;

float wt = 0;
float wts = 0;

void setup() {
  size(800, 800);
  background(0);
  colorMode(HSB, 255, 255, 255);

  Ani.init(this);
  Ani.noAutostart();

  //this, duration, filed Name, end value, easing style
  //callback -> onStart, onEnd, onDelayEnd and onUpdate
  rotateAni = new Ani(this, 1, "t", TWO_PI, Ani.LINEAR, "onStart:startCB, onEnd:endCB");
  hueAni = new Ani(this, 1, "hue", 255, Ani.LINEAR);

  println(easings.length);
}

void draw() {
  pushMatrix();
  translate(width/2, height/2);
  rotate(t);

  noFill();
  stroke(hue, 255, 255);

  float w = abs(sin(wt))*100 + 200;
  ellipse(w/2, 0, w, 50);
  popMatrix();

  wt += wts;
}

void startCB(){
  println("animation started");
}

void endCB(){
  println("animation ended");
  wt = 0;
  wts = 0;
}

void keyPressed() {
  if (key == ' ') {
    if (!rotateAni.isPlaying()) {
      background(0);
      float duration = random(3, 8);
      int index = int(random(0, 31));
      rotateAni.setEasing(easings[index]);
      rotateAni.setDuration(duration);
      
      hueAni.setDuration(duration);
      
      rotateAni.start();
      hueAni.start();
      
      wts = 0.125;
      
      println(easingsVariableNames[index]);
    }
  }
}
