int xNum = 30;
int yNum = 20;

float w;
float h;

boolean b = true;

void setup() {
  size(800, 600);
  background(0);

  w = width/xNum;
  h = height/yNum;
}

void draw() {
  background(0);

  stroke(0);
  fill(255);
  
  xNum = int(map(mouseX, 0, width, 2, 100));
  yNum = int(map(mouseY, 0, height, 2, 100));
  
  w = width/xNum;
  h = height/yNum;

  for (int y=0; y <= yNum; y++) {
    for (int x=0; x <= xNum; x++) {
      
      if(b == true){
        rect(x*w, y*h, w, h);
      }
    }
  }
}

void keyPressed(){
  if(key == ' '){
    b = !b;
  }
}
