int numBalls = 45;
int[] x;
int[] speedX;
float w, h;

void setup() {
  size(800, 800);
  background(0);
  
  x = new int[numBalls];
  speedX = new int[numBalls];
  
  for(int i=0; i<numBalls; i++){
    x[i] = 0;
    speedX[i] = int(random(1, 10));
  }
  
  w = height/numBalls;
  h = w;
}

void draw() {
  background(0);
  fill(255);
  
  for (int i=0; i<numBalls; i++) {
    ellipse(x[i], w*i+w/2, w, h);
    x[i] += speedX[i];
    if(x[i] > width){
      x[i] = 0;
      speedX[i] = int(random(1, 10));
    }
  }
  
}
