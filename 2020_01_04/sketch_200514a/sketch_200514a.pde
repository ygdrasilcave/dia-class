void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(128);
  /*beginShape();
  float r = map(mouseX, 0, width, 10, width/2);
  float x = cos(radians(30)) * r;
  float y = sin(radians(30)) * r;
  vertex(x+width/2, y+height/2);
  x = cos(radians(150)) * r;
  y = sin(radians(150)) * r;
  vertex(x+width/2, y+height/2);
  x = cos(radians(270)) * r;
  y = sin(radians(270)) * r;
  vertex(x+width/2, y+height/2);
  endShape(CLOSE);
  */
  
  int numPoints = int(map(mouseY, 0, height, 3, 16));
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(-90));
  beginShape();
  float r = map(mouseX, 0, width, 10, width/2);
  for (int i=0; i<numPoints; i++) {
    float x = cos(radians((360/numPoints)*i)) * r;
    float y = sin(radians((360/numPoints)*i)) * r;
    vertex(x, y);
  }
  endShape(CLOSE);
  popMatrix();
}
