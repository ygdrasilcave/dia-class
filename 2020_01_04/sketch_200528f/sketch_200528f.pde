float tx = 0;
float ty = 0;

float t = 0;
float[] eyeT;
float[] eyeTS;

void setup() {
  size(800, 800);
  background(0);
  tx = width/2;
  ty = height/2;
  
  eyeT = new float[8*8];
  eyeTS = new float[8*8];
  for(int i=0; i<eyeT.length; i++){
    eyeT[i] = random(10);
    eyeTS[i] = random(0.01, 0.15);
  }
}

void draw() { 
  background(0);
  noStroke();

  for (int x=0; x<8; x++) {
    for (int y=0; y<8; y++) {
      tx = mouseX-(50+x*100);
      ty = mouseY-(50+y*100);
      t = atan2(ty, tx);
      
      int index = x + y*8;
      float eyeD = map(sin(eyeT[index]), -1, 1, 5, 30);
      //eyeT[index] += 0.08;
      eyeT[index] += eyeTS[index];
      
      fill(255);
      ellipse(50 + x*100, 50 + y*100, 50, 50);
      
      float dist = sqrt(tx*tx + ty*ty);
      if (dist < 25) {
        fill(0);
        ellipse(mouseX, mouseY, eyeD, eyeD); 
      } else {
        pushMatrix();
        translate(50 + x*100, 50 + y*100);
        rotate(t);      
        fill(0);
        ellipse(20, 0, eyeD, eyeD);      
        popMatrix();
      }      
    }
  }
  
  fill(255, 0, 0);
  ellipse(mouseX, mouseY, 20, 20);
}
