boolean color_mode_r = false;
boolean color_mode_g = true;
boolean color_mode_b = false;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  if (color_mode_r == true) {
    fill(255, 0, 0);
    noStroke();
  } 
  if (color_mode_g == true) {  
    fill(0, 255, 0);
    noStroke();
  }
  if (color_mode_b == true) {
    fill(0, 0, 255);
    noStroke();
  }

  for (int y=0; y<=height; y+=20) {
    for (int x=0; x<=width; x+=20) {      
      ellipse(x, y, 20, 20);
    }
  }
}

void keyPressed() {
  if (key == 'r' || key == 'R') {
    color_mode_r = true;
    color_mode_g = false;
    color_mode_b = false;
  }
  if (key == 'g' || key == 'G') {
    color_mode_r = false;
    color_mode_g = true;
    color_mode_b = false;
  }
  if (key == 'b' || key == 'B') {
    color_mode_r = false;
    color_mode_g = false;
    color_mode_b = true;
  }
}
