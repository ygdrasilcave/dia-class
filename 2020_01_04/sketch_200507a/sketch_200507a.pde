PImage img;
PImage img2;
float w, h;
float t=0;

void setup() {
  size(990, 659);
  img = loadImage("sunflowers.jpg");
  img2 = loadImage("Gerbera_Flower.png");
  background(0);
  w = width/10.0;
  h = height/10.0;
  for (int y=0; y<10; y++) {
    for (int x=0; x<10; x++) {
      image(img, x*w, y*h, w, h);
    }
  }
}

void draw() {
  //background(0);
  /*for (int y=0; y<10; y++) {
   for (int x=0; x<10; x++) {
   image(img, x*w, y*h, w, h);
   }
   } */

  /*for (int i=0; i<3; i++) {
   pushMatrix();
   translate(mouseX, mouseY);
   rotate(radians(120*i));
   image(img2, -100, -125, 200, 250);
   popMatrix();
   }*/

  pushMatrix();
  translate(mouseX, mouseY);
  rotate(t);
  image(img2, -100, -125, 200, 250);
  popMatrix();

  if (mousePressed == true) {
    t+=0.023;
  }
}
