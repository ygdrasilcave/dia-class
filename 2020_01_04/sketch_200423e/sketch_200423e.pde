int boxNum = 10;
float[] rotVal;
float[] rotValSpeed;
float[] dir;
float[] boxSize;
float[] boxDistance;
boolean[] type;

void setup() {
  size(1000, 1000);
  background(0);
  rotVal = new float[boxNum];
  rotValSpeed = new float[boxNum];
  boxSize = new float[boxNum];
  boxDistance = new float[boxNum];
  dir = new float[boxNum];
  type = new boolean[boxNum];

  for (int i=0; i<boxNum; i++) {
    rotVal[i] = random(0, TWO_PI);
    rotValSpeed[i] = random(0.002, 0.02);
    boxSize[i] = random(10, 80);
    boxDistance[i] = random(100, 450);
    float p = random(1);
    if (p > 0.5) {
      dir[i] = 1.0;
    } else {
      dir[i] = -1.0;
    }
    p = random(1);
    if (p > 0.5) {
      type[i] = true;
    } else {
      type[i] = false;
    }
  }
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 60);
  rect(0, 0, width, height);
   
  for (int i=0; i<boxNum; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(rotVal[i]*dir[i]);
    stroke(255);
    fill(255);
    if (type[i] == true) {
      rect(boxDistance[i]-boxSize[i]/2, -boxSize[i]/2, boxSize[i], boxSize[i]);
    } else {
      ellipse(boxDistance[i], 0, boxSize[i], boxSize[i]);
    }
    line(0, 0, boxDistance[i], 0);
    rotVal[i] += rotValSpeed[i];
    popMatrix();
  }
}
