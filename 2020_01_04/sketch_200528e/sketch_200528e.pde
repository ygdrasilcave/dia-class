float tx = 0;
float ty = 0;

float t = 0;

int xNum = 40;
int yNum = xNum;

float w;
float f = 1;

void setup() {
  size(800, 800);
  background(0);
  tx = width/2;
  ty = height/2;

  w = (width/xNum)/2;
}

void draw() { 
  background(0);
  noStroke();

  for (int x=0; x<xNum; x++) {
    for (int y=0; y<yNum; y++) {
      tx = mouseX-(w+x*(w*2));
      ty = mouseY-(w+y*(w*2));
      t = atan2(ty, tx);

      pushMatrix();
      translate(w + x*(w*2), w + y*(w*2));
      rotate(t);
      fill(0, 255, 0);
      ellipse(0, 0, w*0.5, w*0.5);
      rect(0, -(w*0.15)*0.5, w*f, w*0.15);
      popMatrix();
    }
  }


  fill(255, 0, 0);
  ellipse(mouseX, mouseY, 20, 20);
}


void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      xNum++;
      if (xNum > 80) {
        xNum = 80;
      }
      yNum = xNum;
      w = (width/xNum)/2;
    }
    if (keyCode == DOWN) {
      xNum--;
      if (xNum < 1) {
        xNum = 1;
      }
      yNum = xNum;
      w = (width/xNum)/2;
    }
    if(keyCode == LEFT){
      f -= 0.1;
    }
    if(keyCode == RIGHT){
      f += 0.1;
    }
  }
}
