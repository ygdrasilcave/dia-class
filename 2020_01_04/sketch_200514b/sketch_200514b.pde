float rf = 0;
float rfs = 0.025;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(128);

  //int numPoints = int(map(mouseY, 0, height, 3, 16));
  int numPoints = 12;

  for (int j=0; j<10; j++) {

    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(-90 + (360/10)*j));
    beginShape();
    noFill();
    stroke(0);
    float r = map(mouseX, 0, width, 10, width/2);
    for (int i=0; i<numPoints; i++) {
      float x = 0;
      float y = 0;
      if (i%2 == 0) {
        x = cos(radians((360/numPoints)*i)) * r;
        y = sin(radians((360/numPoints)*i)) * r;
      } else {
        x = cos(radians((360/numPoints)*i)) * (r*abs(sin(rf)));
        y = sin(radians((360/numPoints)*i)) * (r*abs(sin(rf)));
      }
      vertex(x, y);
    }
    endShape(CLOSE);
    popMatrix();
    
  }

  //rf += rfs;
  rf = map(mouseY, 0, width, 0, TWO_PI);
}
