float tx = 0;
float ty = 0;

float t = 0;

void setup() {
  size(800, 800);
  background(0);
  tx = width/2;
  ty = height/2;
}

void draw() { 
  background(0);
  noStroke();

  for (int x=0; x<8; x++) {
    for (int y=0; y<8; y++) {
      tx = mouseX-(50+x*100);
      ty = mouseY-(50+y*100);
      t = atan2(ty, tx);

      pushMatrix();
      translate(50 + x*100, 50 + y*100);
      rotate(t);
      fill(0, 255, 0);
      ellipse(0, 0, 50, 50);
      rect(0, -5, 50, 10);
      popMatrix();
    }
  }


  fill(255, 0, 0);
  ellipse(mouseX, mouseY, 20, 20);

}
