float x, y;
float speedX, speedY, dirX, dirY;
float t1, t2;
float dia;
float speedDia, dirDia;

void setup(){
  size(800, 800);
  background(0);
  x = random(width);
  y = random(height);
  speedX = 3;
  speedY = 3;
  dirX = 1;
  dirY = 1;
  speedDia = 1;
  dirDia = 1;
}

void draw(){
  fill(0, 20);
  noStroke();
  rect(0,0,width,height);
  
  speedX = noise(t1)*5;
  speedY = noise(t2)*5;
  x += speedX*dirX;
  y += speedY*dirY;
  
  dia += speedDia*dirDia;
  if(dia > 30){
    dia = 30;
    dirDia*=-1;
  }
  if(dia < 5){
    dia = 5;
    dirDia*=-1;
  }
  noFill();
  stroke(255);
  ellipse(x, y, dia, dia);
  t1 += 0.065;
  t2 += 0.082;
  if(x > width-dia/2){
    x = width-dia/2;
    dirX*=-1;
  }
  if(x < dia/2){
    x = dia/2;
    dirX*=-1;
  }
  if(y > height-dia/2){
    y = height-dia/2;
    dirY*=-1;
  }
  if(y < dia/20){
    y = dia/2;
    dirY*=-1;
  }
}
