PImage img;

void setup() {
  size(990, 659);
  img = loadImage("sunflowers.jpg");
  //background(80);
}

void draw() {
  float threshold = map(mouseX, 0, width, 0, 255);

  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      if (r>threshold) {
        r = 255;
      } else {
        r = 0;
      }
      if (g>threshold) {
        g = 255;
      } else {
        g = 0;
      }
      if (b>threshold) {
        b = 255;
      } else {
        b = 0;
      }
      fill(r, g, b);
      noStroke();
      rect(x, y, 1, 1);
    }
  }
}

void keyPressed() {
  if (key == 's') {
    saveFrame("myWork_####.jpg");
  }
}
