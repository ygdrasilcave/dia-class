PImage img;

void setup() {
  size(990, 659);
  img = loadImage("sunflowers.jpg");
}

void draw() {
  int x = mouseX;
  int y = mouseY;
  int index = x + y*width;
  color c = img.pixels[index];
  float r = red(c);
  float g = green(c);
  float b = blue(c);
  fill(r, g, b);
  noStroke();
  ellipse(x, y, 20, 20);
}
