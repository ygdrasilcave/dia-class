import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

float x;
float y;

int counterX = 0;
int counterY = 0;

Ani xAni, yAni;

void setup() {
  size(800, 800);
  background(0);

  Ani.init(this);
  Ani.noAutostart();

  x = 0;
  y = height-200;

  xAni = new Ani(this, 4, "x", width/2, Ani.LINEAR, "onEnd:endCB");
  yAni = new Ani(this, 4, "y", 200, Ani.BACK_IN_OUT, "onEnd:endCB");
}

void draw() {
  fill(255);
  noStroke();
  ellipse(x, y, 20, 20);
}

void endCB(Ani theAni) {
  if (theAni == xAni) {
    println("animation X done");
    if (counterX < 2) {
      float begin = xAni.getEnd();
      float end = width;
      xAni.setBegin(begin);
      xAni.setEnd(end);
      xAni.setDuration(4);
      xAni.start();
      counterX += 1;
    }
  } else if (theAni == yAni) {
    println("animation Y done");
    if (counterY < 2) {
      float begin = yAni.getEnd();
      float end = yAni.getBegin();
      yAni.setBegin(begin);
      yAni.setEnd(end);
      yAni.setDuration(4);
      //yAni.setEasing(Ani.BOUNCE_OUT);
      yAni.start();
      counterY += 1;
    }
  }
}

void keyPressed() {
  xAni.start();
  yAni.start();
  counterX += 1;
  counterY += 1;
}
