float x, y, w, h;

void setup(){
  size(800,800);
  background(255);
  x = random(width);
  y = random(height);
  w = random(3, 30);
  h = random(3, 30);
}

void draw(){
  x = random(width);
  y = random(height);
  w = random(3, 30);
  h = random(3, 30);
    
  fill(0, 20);
  ellipse(x, y, w, h);
}
