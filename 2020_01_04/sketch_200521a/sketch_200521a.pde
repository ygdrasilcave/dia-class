import processing.sound.*;

SoundFile sf;
Reverb reverb;
Delay delay;

void setup() {
  size(800, 800);
  background(0);

  sf = new SoundFile(this, "level-up-voice.wav");
  sf.stop();
  reverb = new Reverb(this);
  //reverb.process(sf);
  //reverb.room(0.5);
  
  delay = new Delay(this);
  delay.process(sf, 1);
  delay.time(0.5); 
  delay.feedback(0.5);
}

void draw() {
}

float voiceRate = 0.1;

void keyPressed() {
  if(key == 'q'){
    //reverb.room(map(mouseX, 0, width, 0.0, 1.0));
    //reverb.wet(map(mouseX, 0, width, 0.0, 1.0));
    sf.play();
  }
  
  
  if (key == 'a') {
    /*if (sf.isPlaying() == false) {
      sf.jump(2.5);
      //sf.play();
      //sf.rate(random(0.5, 2));
    }*/
    
    //sf.play();
    sf.jump(0.5);    
    sf.amp(map(mouseY, 0, height, 1.0, 0.0));
    sf.pan(map(mouseX, 0, width, -1.0, 1.0));
    sf.rate(voiceRate);
    
    voiceRate += 0.1;
    if(voiceRate > 2){
      voiceRate = 0.1;
    }
  }
  
  if(key == 's'){
    sf.jump(0.2);    
    sf.amp(map(mouseY, 0, height, 1.0, 0.0));
    sf.pan(map(mouseX, 0, width, -1.0, 1.0));
    sf.rate(random(0.5, 2));
  }
  
  if(key == 'd'){
    sf.jump(0.65);    
    sf.amp(map(mouseY, 0, height, 1.0, 0.0));
    sf.pan(map(mouseX, 0, width, -1.0, 1.0));
    sf.rate(random(0.5, 2));
  }
  
  if(key == 'p'){
    if (sf.isPlaying() == true) {
      sf.pause();
      //sf.stop();
    }
  }
  
  if(key == ' '){
    sf.stop();
  }
}
