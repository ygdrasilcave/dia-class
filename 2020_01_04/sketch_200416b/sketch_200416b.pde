float x = 0;
float y = 0;
float w = 0;
int currTime = 0;
float px, py;
float t = 0;

void setup() {
  size(800, 800);
  background(0);
  currTime = millis();
  px = x;
  py = y;
}

void draw() {
  if (currTime + 100 < millis()) {
    px = x;
    py = y;

    x += 50;
    y = noise(t)*height;
    
    if (x > width) {
      x = 0;
      background(0);
      px = x;
      py = y;
    }

    w = 10;
    currTime = millis();
    t += 0.362;
  }

  fill(255);
  noStroke();
  ellipse(x, y, w, w);

  stroke(255);
  line(px, py, x, y);
}
