void setup(){
  size(800, 800);
  background(0);
  colorMode(HSB);
}

float t = 0;
void draw(){
  background(0);
  //float r = random(255);
  float h = noise(t)*255;
  fill(h, 255, 255);
  rect(width/2-300, height/2-300, 600, 600);
  t += 0.012;
}
