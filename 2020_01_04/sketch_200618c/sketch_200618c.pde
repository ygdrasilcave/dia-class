import oscP5.*;
import netP5.*;

OscP5 oscP5;

float valueX = 0;
float valueY = 0;

float w = 0;

int sceneNum = 1;

void setup() {
  size(800, 800);
  oscP5 = new OscP5(this, 12001);
}

void draw() {
  background(0);  

  if (sceneNum == 1) {
    noStroke();
    fill(255, 255, 0);
    ellipse(valueX, valueY, w, w);
  } else if (sceneNum == 2) {
    noStroke();
    fill(0, 255, 255);
    rect(valueX-w/2, valueY-w/2, w, w);
  } else if (sceneNum == 3) {
    noStroke();
    fill(255, 0, 0);
    ellipse(valueX, valueY, w, w);
  } else if (sceneNum == 4) {
    noStroke();
    fill(255, 0, 255);
    rect(valueX-w/2, valueY-w/2, w, w);
  }
}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/curve")==true) {
    if (theOscMessage.checkTypetag("ff")) {
      valueX = theOscMessage.get(0).floatValue(); 
      valueY = theOscMessage.get(1).floatValue();

      valueX = map(valueX, 0, 1, 0, 800);
      valueY = map(valueY, 0, 1, 0, 800);
    }
  }
  if (theOscMessage.checkAddrPattern("/tr")==true) {
    if (theOscMessage.checkTypetag("f")) {
      sceneNum = int(theOscMessage.get(0).floatValue()); 
      //println(sceneNum);
    }
  }

  if (theOscMessage.checkAddrPattern("/size")==true) {
    if (theOscMessage.checkTypetag("f")) {
      w = theOscMessage.get(0).floatValue(); 
      w = map(w, 3, 4, 5, 100);
      //println(w);
    }
  }
  //println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
} 
