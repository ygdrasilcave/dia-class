float x;
float y;
float speedX = 5;
float speedY = 3;
float dirX = 1;
float dirY = 1;

float r = 255;
float g = 255;
float b = 255;

void setup() {
  size(1000, 1000);
  background(0);
  x = width/2;
  y = height/2;
}

void draw() {
  background(0);

  x = x + speedX*dirX;
  y = y + speedY*dirY;

  if (x > width-15) {
    dirX = dirX * -1;
    changeColor();
  }
  if (x < 15) {
    dirX = dirX * -1;
    changeColor();
  }
  if (y > height-15) {
    dirY = dirY * -1;
    changeColor();
  }
  if (y < 15) {
    dirY = dirY * -1;
    changeColor();
  }
  fill(r, g, b);
  ellipse(x, y, 30, 30);
}

void changeColor() {
  r = random(255);
  g = random(255);
  b = random(255);
}
