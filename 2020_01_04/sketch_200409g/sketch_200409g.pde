float x, y, speedX, speedY;
float dirX, dirY;
float w, h;

void setup(){
  size(800,800);
  background(255);
  x = random(width);
  y = random(height);
  speedX = random(3, 8);
  speedY = random(4, 8);
  dirX = 1;
  dirY = 1;
  w = random(5, 30);
  h = random(5, 30);
}

void draw(){
  //background(255);
  
  //speedX = random(3, 8);
  //speedY = random(4, 8);
  float scale = map(mouseX, 0, width, 0.1, 5);
  //w = random(5, 30) * scale;
  //h = random(5, 30) * scale;
  
  x += speedX*dirX;
  y += speedY*dirY;
  
  if(x > width){
    dirX *= -1;
    speedX = random(3, 8);
  }
  if(x < 0){
    dirX *= -1;
    speedX = random(3, 8);
  }
  if(y > height){
    dirY *= -1;
    speedY = random(4, 8);
  }
  if(y < 0){
    dirY *= -1;
    speedY = random(4, 8);
  }
  
  noFill();
  stroke(0);
  ellipse(x, y, w, h);
  
}

void keyPressed(){
  
}
