float x = 0;
float y = 0;

float t = 0;

void setup() {
  size(800, 800);
  background(0);
  x = width/2;
  y = height/2;
}

void draw() { 
  background(0);
  noStroke();

  x = mouseX-width/2;
  y = mouseY-height/2;
  t = atan2(y, x);

  pushMatrix();
  translate(width/2, height/2);
  rotate(t);
  fill(0, 255, 0);
  rect(0, -5, 200, 10);
  popMatrix();

  pushMatrix();
  translate(width/2, height/2);
  fill(255, 0, 0);
  ellipse(x, y, 50, 50);
  popMatrix();
}
