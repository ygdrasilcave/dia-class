void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(45));
  noStroke();
  fill(255);
  rect(-150, -150, 300, 300);
  ellipse(200, 0, 100, 100);
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(15));
  noFill();
  strokeWeight(5);
  stroke(255, 255, 0);
  rect(-125, -125, 250, 250);
  popMatrix();
  
  noFill();
  strokeWeight(5);
  stroke(255, 0, 0);
  rect(width/2-125, height/2-125, 250, 250);
}
