void setup() {
  size(1000, 1000);
  background(0);
}

void draw() {
  background(255,0,0);

  float r = map(mouseX, 0, width, 10, 100);
  r = 10;

  for (int px=0; px<width/r; px++) {
    for (int py=0; py<height/r; py++) {
      for (int j=0; j<3; j++) {
        pushMatrix();
        
        if(py%2 == 0){
          translate(px*(2*(cos(radians(30))*r)), py*(r+r/2));
        }else{
          translate(px*(2*(cos(radians(30))*r)) + cos(radians(30))*r, py*(r+r/2));
        }
        
        rotate(radians(120)*j);

        pushMatrix();
        rotate(-(HALF_PI+radians(60)));
        fill(map(j,0,2,255,0));
        beginShape();
        vertex(0, 0);
        for (int i=0; i<3; i++) {
          float theta = radians((360/6)*i);        
          float x = cos(theta)*r;
          float y = sin(theta)*r;
          vertex(x, y);
        }
        endShape();
        popMatrix();

        popMatrix();
      }
    }
  }
}
