int color_mode = 0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  if (color_mode == 0) {
    fill(255, 255, 0);
    noStroke();
  }else if (color_mode == 1) {
    fill(255, 0, 255);
    noStroke();
  }else if (color_mode == 2) {
    fill(255, 0, 0);
    noStroke();
  }else if (color_mode == 3) {
    fill(0, 255, 0);
    noStroke();
  }else if (color_mode == 4) {
    fill(0, 0, 255);
    noStroke();
  }else if (color_mode == 5) {
    fill(0, 255, 255);
    noStroke();
  }

  for (int y=0; y<=height; y+=20) {
    for (int x=0; x<=width; x+=20) {      
      ellipse(x, y, 20, 20);
    }
  }
}

void keyPressed(){
  if(key == CODED){
    if(keyCode == UP){
      color_mode += 1;
      if(color_mode > 5){
        //color_mode = 5;
        color_mode = 0;
      }
    }else if(keyCode == DOWN){
      color_mode -= 1;
      if(color_mode < 0){
        //color_mode = 0;
        color_mode = 5;
      }
    }
  } 
  println(color_mode);
}
