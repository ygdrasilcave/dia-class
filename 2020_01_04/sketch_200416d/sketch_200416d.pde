float h, w;
float t1 = 0;
float t2 = 0;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  fill(0, 12);
  noStroke();
  rect(0, 0, width, height);
  
  h = noise(t1)*width;
  w = noise(t2)*height;
  
  noFill();
  stroke(255);
  
  ellipse(width/2, height/2, h, w);
  
  t1 += 0.0126;
  t2 += 0.023;
}
