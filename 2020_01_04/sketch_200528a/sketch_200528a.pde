float x = 0;
float y = 0;

float t = 0;
float target = 0;

void setup() {
  size(800, 800);
  background(0);
  x = width/2;
  y = height/2;
  target = atan2(height/2, width/2);
}

void draw() { 
  background(0);
  noStroke();

  pushMatrix();
  translate(0, 0);
  rotate(t);
  fill(0, 255, 0);
  rect(0, -5, 200, 10);
  popMatrix();
  
  t += (target-t)*0.1;

  fill(255, 0, 0);
  ellipse(x, y, 50, 50);
}

void keyPressed() {
  if (key == ' ') {
    x = random(width);
    y = random(height);
    target = atan2(y, x);
  }
}
