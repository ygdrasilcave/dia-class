int currentTime = 0;

float r = 0;
float g = 0;
float b = 0;

void setup() {
  size(800, 800);
  background(255);  
  r = random(0, 255);
  g = random(0, 255);
  b = random(0, 255);  
  currentTime = millis();
}

void draw() {
  background(255);

  if (currentTime + 1000 < millis()) {
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
    currentTime = millis();
  }

  fill(r, g, b);

  ellipse(width/2, height/2, 400, 400);

  println(millis());
}

void keyPressed() {
  if (key == ' ') {
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
  }
}
