PImage img;

void setup() {
  size(990, 659);
  img = loadImage("sunflowers.jpg");
  //background(80);
}

void draw() {
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      fill(r, g, g);
      noStroke();
      rect(x, y, 1, 1);
    }
  }
}
