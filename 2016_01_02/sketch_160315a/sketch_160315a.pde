float x = 200.0;
float y = 100.0;

void setup(){
  size(800, 600);
  background(0, 0, 0);
}

void draw(){
  background(0);
  stroke(255, 255, 0);
  strokeWeight(5);
  line(width/2, 0, width/2, height);
  stroke(0, 255, 255);
  line(0, height/2, width, height/2);
  noStroke();
  fill(255, 255, 0);
  ellipse(width/2, y, 50, 50);
  fill(255, 0, 255);
  ellipse(x, height/2, 50, 50);
  x = x+5.2;
  y = y+8.6;
  
  if(y > height+50){
    y = -50;
  }
  if(x > width+50){
    x = -50;
  }
  println(y);
}