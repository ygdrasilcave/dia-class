PImage img;
float w = 30;

void setup(){
  size(900, 675);
  img = loadImage("yellow_flower.png");
  background(255);
}

void draw(){
  image(img, mouseX, mouseY, w, w);
}