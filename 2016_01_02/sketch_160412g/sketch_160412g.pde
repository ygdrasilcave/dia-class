int x[] = {450, 240, 120, 8, 300, 68, 159};
int[] y;

int j = 8;
int k;

int counter = 0;

void setup() {
  size(500, 500);  
  k = 24;
  y = new int[7];
  println(x.length);
  println(y.length);
  for (int i=0; i < y.length; i++) {
    y[i] = int(random(height));
  }
}

void draw() {
  background(255);
  ellipse(x[counter], y[counter], 80, 80);
}

void mouseReleased() {
  counter++;
  if (counter > x.length-1) {
    counter = 0;
    for (int i=0; i < y.length; i++) {
      x[i] = int(random(width));
      y[i] = int(random(height));
    }
  }
}