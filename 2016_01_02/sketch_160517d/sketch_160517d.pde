int[] data;
int i = 0;
int j = 0;
int pixelSize = 10;

void setup() {
  size(800, 800);
  data = new int[(width/pixelSize)*(height/pixelSize)];
  for (int i=0; i<data.length; i++) {
    float s = random(1);
    if (s < 0.5) {
      data[i] = 0;
    } else {
      data[i] = 1;
    }
  }
}

void draw() {
  background(120);
  noStroke();
  for (int y=0; y<int(height/pixelSize); y++) {
    for (int x=0; x<int(width/pixelSize); x++) {    
      int index = x + y*int(width/pixelSize);
      fill(data[index]*255);
      rect(x*pixelSize, y*pixelSize, pixelSize, pixelSize);
    }
  }
  loadPixels();
  color c = pixels[i + j*width];
  float br = brightness(c);
  
  noFill();
  stroke(255, 0, 0);
  strokeWeight(2);
  rect(i, j, pixelSize, pixelSize);
  
  if (frameCount%30 == 0) {
    println(br);
    i+=pixelSize;
    if (i > width) {
      i = 0;
      j+=pixelSize;
      if (j > height) {
        j = 0;
      }
    }
  }
}

void keyPressed() {
  for (int i=0; i<data.length; i++) {
    float s = random(1);
    if (s < 0.5) {
      data[i] = 0;
    } else {
      data[i] = 1;
    }
  }
}