float dia = 0.0;
float speed = 12;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  //background(0);
  fill(0, 20);
  noStroke();
  rect(0,0,width,height);
  
  stroke(255);
  strokeWeight(3);
  noFill();
  ellipse(width/2, height/2, dia, dia);
  dia = dia+speed;
  if(dia > width){
    dia = width;
    speed = speed*-1;
  }
  if(dia < 0){
    dia = 0;
    speed = speed*-1;
  }
}