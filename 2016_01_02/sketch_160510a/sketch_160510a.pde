import netP5.*;
import oscP5.*;
OscP5 server;
NetAddress iannix;

float posX0, posY0;
float posX1, posY1;

void setup(){
  size(800, 800);
  server = new OscP5(this, 12000);
  iannix = new NetAddress("localhost", 13000);
  posX0 = 0;
  posY0 = 0;
  posX1 = 0;
  posY1 = 0;
}

void draw(){
  background(0);
  fill(255);
  ellipse(posX0, posY0, 30, 30);
  ellipse(posX1, posY1, 50, 50);
}

void oscEvent(OscMessage msg){
  if(msg.checkAddrPattern("/left")==true){
    if(msg.checkTypetag("ff")==true){
      float x = msg.get(0).floatValue()*100;
      float y = msg.get(1).floatValue()*100;
      posX0 = x;
      posY0 = y;
      //println(x + " : " + y);
    }
  } 
  if(msg.checkAddrPattern("/right")==true){
    if(msg.checkTypetag("ff")==true){
      float x = msg.get(0).floatValue()*100;
      float y = msg.get(1).floatValue()*100;
      posX1 = x;
      posY1 = y;
      //println(x + " : " + y);
    }
  } 
}