import processing.video.*;

Movie m;
boolean p = true;

int sequence = 1;

void setup() {
  size(1280, 720, P3D);
  m = new Movie(this, "SampleVideo_1280x720_5mb.mp4");
  //m.loop();
  if (p == true) {
    m.play();
  } else {
    m.pause();
  }
  
  textureMode(NORMAL);
}

void movieEvent(Movie m) {
  m.read();
}

void draw() {
  background(0);
  image(m, 0, 0);
   
  /*beginShape();
  texture(m);
  vertex(100, 100, 0, 0);
  vertex(width-mouseX, 100, 1, 0);
  vertex(width-100, height-100, 1, 1);
  vertex(mouseX, height-100, 0, 1);
  endShape();*/
  
  if(sequence == 1){
    if(m.time()-0.5 > m.duration()/3){
      sequence = 0;
      m.pause();
    }
  }else if(sequence == 2){
    if(m.time()-0.5 > 2*m.duration()/3){
      sequence = 0;
      m.pause();
    }
  }else if(sequence == 3){
    if(m.time()-0.5 > m.duration()){
      sequence = 0;
      m.pause();
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    p = !p;
    if(p == true){
      m.play();
    }else{
      m.pause();
    }
  }
  if(key == '1'){
    sequence = 1;
    m.jump(0);
    m.play();
  }
  if(key == '2'){
    sequence = 2;
    m.jump(m.duration()/3);
    m.play();
  }
  if(key == '3'){
    sequence = 3;
    m.jump(2*(m.duration()/3));
    m.play();
  }
  
}