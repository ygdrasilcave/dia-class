float x = 100;
float y = 100;
float xSpeed;
float ySpeed;
float xDir = 1;
float yDir = 1;
float dia = 50;
float t = 0.0;

void setup(){
  size(800, 600);
  background(255);
  xSpeed = random(2, 10);
  ySpeed = random(2, 10);
}

void draw(){
  //background(255);
  fill(255, 5);
  noStroke();
  rect(0,0,width,height);
  dia = (noise(t)*100)+5;
  t += 0.065;
  x += xDir*xSpeed;
  y += yDir*ySpeed;
  stroke(0);
  noFill();
  ellipse(x, y, dia, dia);
  if(x > width-dia/2){
    x = width-dia/2;
    xSpeed = random(2, 10);
    xDir *= -1;
  }
  if(x < dia/2){
    x = dia/2;
    xSpeed = random(2, 10);
    xDir *= -1;
  }
  if(y > height-dia/2){
    y = height-dia/2;
    ySpeed = random(2, 10);
    yDir *= -1;
  }
  if(y < dia/2){
    y = dia/2;
    ySpeed = random(2, 10);
    yDir *= -1;
  }
}