PImage img;

float t = 0.0;

void setup() {
  size(600, 600);
  background(255);
  img = loadImage("yellow_flower.png");
  imageMode(CENTER);
}

void draw() {
  background(0);
  fill(0);
  noStroke();
  noTint();
  for (int i=0; i<12; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(30*i));
    //ellipse(width/2-50, 0, 20, 20);
    image(img, width/2-50, 0, 20, 20);
    popMatrix();
  }
  for (int i=0; i<60; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(6*i));
    //ellipse(width/2-50, 0, 5, 5);
    image(img, width/2-50, 0, 5, 5);
    popMatrix();
  }
  //ellipse(width/2, height/2, 30, 30);
  image(img, width/2, height/2, 30, 30);

  noFill();
  stroke(0);
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(second()*6-90));
  //ellipse(width/2-50, 0, 40, 40);
  image(img, width/2-50, 0, 40, 40);
  popMatrix();
  
  fill(0);
  noStroke();
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(minute()*6-90));
  float diaMinute = abs(sin(t)*30);
  //ellipse(width/2-100, 0, diaMinute, diaMinute);
  image(img, width/2-100, 0, diaMinute, diaMinute);
  popMatrix();
  
  fill(0);
  noStroke();
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(((hour()%12)*30-90) + map(minute(), 0, 59, 0, 30)));
  float diaHour = abs(cos(t)*50);
  //ellipse(width/2-160, 0, diaHour, diaHour);
  tint(0, 255, 255);
  image(img, width/2-160, 0, diaHour, diaHour);
  popMatrix();
  
  t+=0.035;
}