float x = 0.0;
float speedX = 5.0;
float dia1 = 2.0;
float dia2 = 2.0;
float amp = 300.0;
float t = 0.0;

void setup() {
  size(1600, 800);
  background(255);
}

void draw() {
  //background(255);
  
  pushMatrix();
  translate(width/4, height/2);
  rotate(radians(t));
  ellipse(x, 0, dia1, dia1);
  popMatrix();
  
  pushMatrix();
  translate(3*width/4, height/2);
  rotate(radians(t));
  ellipse(x, 0, dia2, dia2);
  popMatrix();
  
  
  t = t+0.2;

  if (x < amp/2) {
    dia1 = map(x, 0, amp/2, 2, 50);
    //dia1 = dia1 + 0.5;
  }else{
    dia1 = map(x, amp/2, amp, 50, 2);
    //dia1 = dia1 - 0.5;
  }
  
  dia2 = sin(map(x, 0, amp, 0, PI))*48 + 2;

  x = x + speedX;
  if (x > amp) {
    x = amp;
    speedX = speedX*-1;
  }
  if (x < 0) {
    x = 0;
    speedX = speedX*-1;
  }
}