float diaX = 0.0;
float speedX = 12;
float diaY = 0.0;
float speedY = 8;
float padX = 0;
float padY = 0;
boolean toggle = true;
int pSecond = 0;

void setup() {
  size(800, 800);
  background(0);
  pSecond = second();
}

void draw() {
  //background(0);
  fill(0, 30);
  noStroke();
  rect(0, 0, width, height);

  stroke(255);
  strokeWeight(6);
  noFill();
  ellipse(width/2, height/2, diaX, diaY);
  //ellipse(width/2, height/2, diaX, diaY/2);
  //ellipse(width/2, height/2, diaX/2, diaY);
  diaX = diaX+speedX;
  diaY = diaY+speedY;
  if (diaX > abs((padX-width/2)*2)) {
    diaX = abs((padX-width/2)*2);
    speedX= speedX*-1;
  }
  if (diaX < 0) {
    diaX = 0;
    speedX = speedX*-1;
  }
  if (diaY > abs((padY-height/2)*2)) {
    diaY = abs((padY-height/2)*2);
    speedY *= -1;
  }
  if (diaY < 0) {
    diaY = 0;
    speedY *= -1;
  }
  /*strokeWeight(1);
  stroke(0, 255, 0);
  line(padX, 0, padX, height);
  line(width-padX, 0, width-padX, height);
  line(0, padY, width, padY);
  line(0, height-padY, width, height-padY);*/

  if (pSecond != second()) {
    if (toggle == true) {
      padX = random(width);
      padY = random(height);
      toggle = false;
      pSecond = second();
    }
  }else{
    if(toggle == false){
      toggle = true;
    }
  }
}