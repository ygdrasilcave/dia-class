import netP5.*;
import oscP5.*;
OscP5 server;
NetAddress phone;

int sliderVal1 = 0;
int sliderVal2 = 0;
int sliderVal3 = 0;
float dia = 0;

void setup(){
  size(600, 600);
  server = new OscP5(this, 1337);
  //change the IP address 172.XX.XX.XX
  phone = new NetAddress("172.XX.XX.XX", 1337);
}

void draw(){
  background(sliderVal1, sliderVal2, sliderVal3);
  fill(255);
  ellipse(width/2, height/2, dia, dia);
}

void oscEvent(OscMessage msg){
  if(msg.checkAddrPattern("/mrmr/slider/horizontal/10/iPhone")==true){
    int val = msg.get(0).intValue();
    sliderVal1 = int(map(val, 0, 1000, 0, 255));
    println(val);
  }
  if(msg.checkAddrPattern("/mrmr/slider/horizontal/11/iPhone")==true){
    int val = msg.get(0).intValue();
    sliderVal2 = int(map(val, 0, 1000, 0, 255));
    println(val);
  }
  if(msg.checkAddrPattern("/mrmr/slider/horizontal/12/iPhone")==true){
    int val = msg.get(0).intValue();
    sliderVal3 = int(map(val, 0, 1000, 0, 255));
    println(val);
  }
  if(msg.checkAddrPattern("/mrmr/slider/horizontal/13/iPhone")==true){
    int val = msg.get(0).intValue();
    dia = map(val, 0, 1000, 0, width);
    println(val);
  }
  println(msg.addrPattern());
}