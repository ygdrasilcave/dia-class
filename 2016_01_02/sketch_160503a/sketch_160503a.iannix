/*
 *	IanniX Score File
 */


/*
 *	This method is called first.
 *	It is the good section for asking user for script global variables (parameters).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function askUserForParameters() {
	//title("The title of the parameter box");
	//ask("Group name of the parameter (only for display purposes)", "Parameter label", "myGlobalVar", "theDefaultValue");
}


/*
 *	This method stores all the operations made through IanniX scripts.
 *	You can add some commands here to make your own scripts!
 *	Scripts are written in Javascript but even with a limited knowledge of Javascript, many types of useful scripts can be created.
 *	
 *	Beyond the standard javascript commands, the run() function is used to send commands to IanniX.
 *	Commands must be provided to run() as a single string.
 *	For example, run("zoom 100"); sets the display zoom to 100%.
 *	
 *	To combine numeric parameters with text commands to produce a string, use the concatenation operator.
 *	In the following example center_x and center_y are in numeric variables and must be concatenated to the command string.
 *	Example: run("setPos current " + center_x + " " + center_y + " 0");
 *	
 *	To learn IanniX commands, perform an manipulation in IanniX graphical user interface, and see the Helper window.
 *	You'll see the syntax of the command-equivalent action.
 *	
 *	And finally, remember that most of commands must target an object.
 *	Global syntax is always run("<command name> <target> <arguments>");
 *	Targets can be an ID (number) or a Group ID (string name of group) (please see "Info" tab in Inspector panel).
 *	Special targets are "current" (last used ID), "all" (all the objects) and "lastCurve" (last used curve).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function makeWithScript() {
	//Clears the score
	run("clear");
	//Resets rotation
	run("rotate 0 0 0");
	//Resets score viewport center
	run("center 0 0");
	//Resets score zoom
	run("zoom 100");
}


/*
 *	When an incoming message is received, this method is called.
 *		- <protocol> tells information about the nature of message ("osc", "midi", "direct…)
 *		- <host> and <port> gives the origin of message, specially for IP protocols (for OpenSoundControl, UDP or TCP, it is the IP and port of the application that sends the message)
 *		- <destination> is the supposed destination of message (for OpenSoundControl it is the path, for MIDI it is Control Change or Note on/off…)
 *		- <values> are an array of arguments contained in the message
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function onIncomingMessage(protocol, host, port, destination, values) {
	//Logs a message in the console (open "Config" tab from Inspector panel and see "Message log")
	console("Received on '" + protocol + "' (" + host + ":" + port + ") to '" + destination + "', " + values.length + " values : ");
	
	//Browses all the arguments and displays them in log window
	for(var valueIndex = 0 ; valueIndex < values.length ; valueIndex++)
		console("- arg " + valueIndex + " = " + values[valueIndex]);
}


/*
 *	This method stores all the operations made through the graphical user interface.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughGUI() {
//GUI: NEVER EVER REMOVE THIS LINE
	run("center -1.11499 0.0223113");
	run("rotate 0 0 0");
	run("zoom 140");


	run("add curve 1");
	run("setpos current 0 0 0");
	var points1 = [
		{x: -5, y: 0, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 1.96392, y: -0.0426455, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
	];
	for(var i = 0 ; i < points1.length ; i++)
		run("setpointat current " + i + " " + points1[i].x + " " + points1[i].y + " " + points1[i].z + " " + points1[i].c1x + " " + points1[i].c1y + " " + points1[i].c1z + " " + points1[i].c2x + " " + points1[i].c2y + " " + points1[i].c2z);
	run("add cursor 2");
	run("setcurve current lastCurve");
	run("setpos current -3.93339 -0.0065317 0");
	run("setwidth current 5");
	run("setboundssourcemode current 1");
	run("setmessage current 20, osc://ip_out:port_out/curve collision_curve_id collision_curve_group_id collision_value_x collision_value_y 0 collision_xPos collision_yPos 0 , tcp:// curve collision_curve_id collision_curve_group_id collision_value_x collision_value_y 0 collision_xPos collision_yPos 0 ,");
	run("setpattern current 0 0 1");


	run("add trigger 3");
	run("setpos current -4.29974 0.665541 0");
	run("setmessage current 1, osc://ip_out:port_out/test 1.0 TEST#1 ,");

	run("add trigger 4");
	run("setpos current -3.89604 0.33406 0");
	run("setmessage current 1, osc://ip_out:port_out/test 2.0 test#2 ,");

	run("add trigger 5");
	run("setpos current -3.08371 0.759139 0");
	run("setmessage current 1, osc://ip_out:port_out/test 3.0 Blue ,");

	run("add trigger 6");
	run("setpos current -0.689424 0.240568 0");
	run("setmessage current 1, osc://ip_out:port_out/test 3.0 yellow ,");

	run("add trigger 7");
	run("setpos current -2.16545 0.815541 0");
	run("setmessage current 1, osc://ip_out:port_out/test 1.0 hello ,");

	run("add trigger 8");
	run("setpos current -1.6689 0.762631 0");
	run("setmessage current 1, osc://ip_out:port_out/test 2.0 green ,");

	run("add trigger 9");
	run("setpos current 0.711693 0.742684 0");
	run("setmessage current 1, osc://ip_out:port_out/test 1.0 monkey ,");

	run("add trigger 10");
	run("setpos current 1.00396 0.281202 0");
	run("setmessage current 1, osc://ip_out:port_out/test 2.0 elephant ,");

	run("add trigger 11");
	run("setpos current 1.33343 0.613425 0");
	run("setmessage current 1, osc://ip_out:port_out/test 3.0 cat ,");



//GUI: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method stores all the operations made by other softwares through one of the IanniX interfaces.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you or a third party software added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughInterfaces() {
//INTERFACES: NEVER EVER REMOVE THIS LINE

//INTERFACES: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method is called last.
 *	It allows you to modify your hand-drawn score (made through graphical user interface).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function alterateWithScript() {
	
}


/*
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 *	Made with IanniX 0.9.16
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 */

