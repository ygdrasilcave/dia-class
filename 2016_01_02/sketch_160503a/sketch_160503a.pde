import netP5.*;
import oscP5.*;

OscP5 oscServer;
NetAddress iannix;

float selector = 1.0;
String msg = "";

void setup(){
  size(600, 600);
  oscServer = new OscP5(this, 12000);
  iannix = new NetAddress("127.0.0.1", 13000);
  textAlign(CENTER, CENTER);
  textSize(48);
}

void draw(){
  noStroke();
  if(selector == 1.0){
    fill(255, 0, 0);
  }else if(selector == 2.0){
    fill(0, 255, 0);
  }else if(selector == 3.0){
    fill(0, 0, 255);
  }
  rect(0, 0, width, height);
  
  fill(255);
  text(msg, width/2, height/2);
}

void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/test")==true) {
    if(theOscMessage.checkTypetag("fs")) {
      float firstValue = theOscMessage.get(0).floatValue();  
      //float secondValue = theOscMessage.get(1).floatValue();
      String secondValue = theOscMessage.get(1).stringValue();
      //print("### received an osc message /test with typetag ffs.");
      //println(" values: "+firstValue+", "+secondValue+", "+thirdValue);
      selector = firstValue;
      msg = secondValue;
      return;
    }  
  } 
  println("address pattern "+theOscMessage.addrPattern());
}