PImage img;
int pixelSize = 1;

int[] r;
int[] g;
int[] b;
int maxR=0;
int maxG=0;
int maxB=0;
int maxNum;
int minR = 0;
int minG = 0;
int minB = 0;
int minNum;
float graphW;
float graphStep;

void setup() {
  size(1600, 300);
  img = loadImage("flower-07.jpg");
  r = new int[256];
  g = new int[256];
  b = new int[256];
  for (int i=0; i<256; i++) {
    r[i] = 0;
    g[i] = 0;
    b[i] = 0;
  }
  for (int i=0; i<img.width*img.height; i++) {
    color c = img.pixels[i];
    int rc = int(red(c));
    int gc = int(green(c));
    int bc = int(blue(c));
    r[rc] = r[rc]+1;
    g[gc] = g[gc]+1;
    b[bc] = b[bc]+1;
  }
  printArray(r);
  
  minR = img.width*img.height;
  minG = img.width*img.height;
  minB = img.width*img.height;
  for (int i=0; i<256; i++) {
    if (r[i] > maxR) {
      maxR = r[i];
    }
    if (g[i] > maxG) {
      maxG = g[i];
    }
    if (b[i] > maxB) {
      maxB = b[i];
    }
    if(r[i] < minR){
      minR = r[i];
    }
    if(g[i] < minG){
      minG = g[i];
    }
    if(b[i] < minB){
      minB = b[i];
    }
  }
  
  println(maxR + " : " + maxG + " : " + maxB);
  maxNum = max(maxR, maxG);
  maxNum = max(maxNum, maxB);
  
  minNum = min(minR, minG);
  minNum = min(minNum, minB);
  println(minNum + " : " + maxNum);
  
  background(255);
  graphStep = width/256.0;
  graphW = graphStep/3.0;
}

void draw() {
  background(255);
  noStroke();
  for (int i=0; i<256; i++) {
    fill(255, 0, 0);
    rect(i*graphStep, height-map(r[i], minR, maxR, 0, height), graphW, map(r[i], minR, maxR, 0, height));
    fill(0, 255, 0);
    rect(graphW+i*graphStep, height-map(g[i], minG, maxG, 0, height), graphW, map(g[i], minG, maxG, 0, height));
    fill(0, 0, 255);
    rect(2*graphW+i*graphStep, height-map(b[i], minB, maxB, 0, height), graphW, map(b[i], minB, maxB, 0, height));
  }
  image(img, 10, 10, 200, 150);
}