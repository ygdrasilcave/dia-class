int[] x;
int[] y;
float[] dia;
int k = 0;
int num = 100;

void setup() {
  size(800, 800);
  background(255);
  x = new int[num];
  y = new int[num];
  dia = new float[num];
  for (int i=0; i<num; i++) {
    x[i] = int(random(width));
    y[i] = int(random(height));
    dia[i] = random(5, 100);
    println(x[i] + "___" + y[i]);
  }
  textAlign(CENTER, CENTER);
  
}

void draw() {
  background(255);

  for (int i=0; i<num; i++) {
    fill(255);
    stroke(0);
    ellipse(x[i], y[i], dia[i], dia[i]);
    fill(0);
    noStroke();
    textSize(dia[i]);
    text(i, x[i], y[i]);
  }
  for(int j=0; j<num; j++){
    float d = random(2, 30);
    ellipse(random(width), random(height), d, d);
  }
}

void keyPressed() {
  for (int i=0; i<num; i++) {
    x[i] = int(random(width));
    y[i] = int(random(height));
    println(x[i] + "___" + y[i]);
  }
}