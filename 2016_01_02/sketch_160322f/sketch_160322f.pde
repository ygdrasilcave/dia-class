float x = 0.0;
float dia = 0.0;
float t = 0.0;

void setup(){
  size(1600, 300);
  background(255);
}

void draw(){
  x += 3;
  dia = noise(t)*150+10;
  ellipse(x, height/2, 0, dia);
  t += 0.035;
  if(x > width){
    background(255);
    x = 0;
  }
}