float x=0;
float y=0;
float w=0;
float h=0;
boolean pressToggle = true;
boolean releaseToggle = true;
boolean drawRect = false;

void setup() {
  size(800, 600);
  background(255);
}

void draw() {
  if (drawRect == true) {
    rect(x, y, w, h);
    drawRect = false;
  }
}

void mousePressed() {
  if (pressToggle == true) {
    x = mouseX;
    y = mouseY;
    pressToggle = false;
    releaseToggle = true;
  }
}

void mouseReleased() {
  if (releaseToggle == true) {
    w = mouseX-x;
    h = mouseY-y;
    releaseToggle = false;
    pressToggle = true;
    drawRect = true;
  }
}