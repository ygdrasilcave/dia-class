void setup(){
  size(600, 600);
}

void draw(){
  background(255);
  float w = width;
  float h = height;
  noFill();
  stroke(0);
  for(int i=0; i<60; i=i+1){
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(i*map(mouseX, 0, width, 0, 6)));
    rect(-(w/2), -(h/2), w, h);
    ellipse(width/2-50, 0, 20, 20);
    popMatrix();
    w = width-(i*10);
    h = height-(i*10);
  }
}