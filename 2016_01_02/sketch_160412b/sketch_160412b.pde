PImage img;
float w = 30;
int num = 20;
float t = 0.0;

void setup() {
  size(800, 800);
  img = loadImage("yellow_flower.png");
  background(255);
  w = width/num;
}

void draw() {
  background(random(255), random(255), 0);
  for (int y=0; y<num; y++) {
    for (int x=0; x<num; x++) {
      pushMatrix();
      translate(map(mouseX, 0, width, width/2, 0) + x*w, map(mouseX, 0, width, width/2, 0) + y*w);
      rotate(t);
      image(img, -w/2, -w/2, w, w);
      popMatrix();
    }
  }
  //num = int((abs(sin(t)))*30)+1;
  num = int(map(mouseX, 0, width, 1, 60));
  w = width/num;
  t+=0.032;
}