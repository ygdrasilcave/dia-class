import processing.video.*;

Movie m1;
Movie m2;
Movie m3;

int whichMovie = 1;

int pixelSize = 10;

float[] br;
float[] r;
float[] g;
float[] b;

void setup() {
  size(1280, 720);
  m1 = new Movie(this, "SampleVideo_1280x720_5mb.mp4");
  m2 = new Movie(this, "occultationgraphh264fullsize.mp4");
  m3 = new Movie(this, "technology_drives_exploration.mp4");

  m1.loop();
  m1.pause();
  m2.loop();
  m2.pause();
  m3.loop();
  m3.pause();
  
  br = new float[(width/pixelSize)*(height/pixelSize)];
  r = new float[(width/pixelSize)*(height/pixelSize)];
  g = new float[(width/pixelSize)*(height/pixelSize)];
  b = new float[(width/pixelSize)*(height/pixelSize)];
}

void movieEvent(Movie m) {
  if (m == m1) {
    m1.read();
  } else if (m == m2) {
    m2.read();
  } else if (m == m3) {
    m3.read();
  }
}

void draw() {
  background(0);

  if (whichMovie == 1) {
    image(m1, 0, 0, width, height);
    /*fill(0);
     rect(0, height-100, width, 50);
     fill(255);
     rect(0, height-95, map(m1.time(), 0, m1.duration(), 0, width), 40);*/
  } else if (whichMovie == 2) {
    image(m2, 0, 0, width, height);
    /*fill(0);
     rect(0, height-100, width, 50);
     fill(255);
     rect(0, height-95, map(m2.time(), 0, m2.duration(), 0, width), 40);*/
  } else if (whichMovie == 3) {
    image(m3, 0, 0, width, height);
    /*fill(0);
     rect(0, height-100, width, 50);
     fill(255);
     rect(0, height-95, map(m3.time(), 0, m3.duration(), 0, width), 40);*/
  }

  loadPixels();
  noStroke();
  for (int y=0; y<height/pixelSize; y++) {
    for (int x=0; x<width/pixelSize; x++) {
      color c = pixels[x*pixelSize + y*pixelSize*(width)];
      float brv = brightness(c);
      float rv = red(c);
      float gv = green(c);
      float bv = blue(c);
      br[x + y*(width/pixelSize)] = brv;
      r[x + y*(width/pixelSize)] = rv;
      g[x + y*(width/pixelSize)] = gv;
      b[x + y*(width/pixelSize)] = bv;
    }
  }
  br = sort(br);
  r = sort(r);
  g = sort(g);
  b = sort(b);
  for (int y=0; y<height/pixelSize; y++) {
    for (int x=0; x<width/pixelSize; x++) {
      fill(br[x + y*(width/pixelSize)]);
      //fill(r[x + y*(width/pixelSize)], 0, 0);
      rect(x*pixelSize, y*pixelSize, pixelSize, pixelSize);
    }
  }
  
  if (whichMovie == 1) {
    image(m1, 50, 50, width/5, height/5);
  } else if (whichMovie == 2) {
    image(m2, 50, 50, width/5, height/5);
  } else if (whichMovie == 3) {
    image(m3, 50, 50, width/5, height/5);
  }
}

void keyPressed() {
  if (key == '1') {
    whichMovie = 1;
    m1.play();
    m2.pause();
    m2.jump(0);
    m3.pause();
    m3.jump(0);
  }
  if (key == '2') {
    whichMovie = 2;
    m2.play();
    m1.pause();
    m1.jump(0);
    m3.pause();
    m3.jump(0);
  }
  if (key == '3') {
    whichMovie = 3;
    m3.play();
    m1.pause();
    m1.jump(0);
    m2.pause();
    m2.jump(0);
  }
}