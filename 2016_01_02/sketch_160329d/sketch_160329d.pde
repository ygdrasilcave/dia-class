float y = 0.0;
float ySpeed = 10.0;
float x;
float xSpeed = 2.0;

void setup(){
  size(800, 300);
  background(255);
  x = width/2;
}

void draw(){
  background(255);
  ellipse(x, y, 10, 10);
  
  y = y+ySpeed;
  x = x+xSpeed;
  
  if(y>height){
    y = height;
    ySpeed = ySpeed*-1;
  }
  if(y<0){
    y = 0;
    ySpeed = ySpeed*-1;
  }
  if(x > width){
    x = 0;
  }
}