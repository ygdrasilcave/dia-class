int x = 0;
int y = 0;
PImage img;

void setup() {
  size(900, 675);
  img = loadImage("rainbow_flower.jpg");
  frameRate(12);
}

void draw() {
  background(120);

  noStroke();
  for(int j = 0; j<height; j++){
    for(int i=0; i<width; i++){
      int pixelIndex = i + j*width;
      color c = img.pixels[pixelIndex];
      float br = brightness(c);
      if(br > map(mouseX, 0, width, 0, 255)){
        fill(255);
      }else{
        fill(0);
      }
      rect(i, j, 1, 1);
    }   
  }
  loadPixels();  
  int index = x + y*width; 
  color c = pixels[index]; 
  float br = brightness(c);
  println(br);
  fill(255, 0, 0);
  rect(x, y, 1, 1);
  x++;
  if(x > width-1){
    x = 0;
    y++;
    if(y > height-1){
      y = 0;
    }
  }
}