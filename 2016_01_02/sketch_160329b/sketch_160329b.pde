float angleValue = 0.0;
float angleSpeed = 5;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  background(255);
  
  noFill();
  stroke(0);
  
  pushMatrix();
  translate(mouseX, mouseY);
  rotate(radians(angleValue));
  line(0, 0, 150, 0);
  ellipse(100, 0, 50, 50);
  ellipse(150, 0, 20, 20);
  //rect(0, 0, 200, 100);
  popMatrix();
  
  pushMatrix();
  translate(2*(width/3), height/2);
  rotate(radians(angleValue));
  rect(-100, -50, 200, 100);
  popMatrix();
  
  angleValue += angleSpeed;
  
  if(angleValue > 135){
    angleValue = 135;
    angleSpeed *= -1;
  }
  if(angleValue < 45){
    angleValue = 45;
    angleSpeed *= -1;
  }
}