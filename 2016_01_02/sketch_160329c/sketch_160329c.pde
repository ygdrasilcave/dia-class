float angleValue = 0.0;
float angleSpeed = 1;

float rad = 0;
float radSpeed = 15;

boolean bRotate = false;

void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
}

void draw() {
  //background(255);

  fill(angleValue, 100, 100);
  noStroke();

  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angleValue));
  //line(0, 0, rad, 0);
  //ellipse(rad, 0, 50, 50);
  ellipse(rad, 0, 5, 5);
  //rect(0, 0, 200, 100);
  popMatrix();
  
  if(angleValue > 360){
    angleValue = 0;
  }

  if (bRotate == true) {
    angleValue += angleSpeed;
  }
  rad += radSpeed;

  if (rad > width/2) {
    rad = width/2;
    radSpeed *= -1;
  }
  if (rad < 250) {
    rad = 250;
    radSpeed *= -1;
  }
}

void keyPressed(){
  bRotate = !bRotate;
}