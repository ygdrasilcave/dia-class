void setup(){
  size(800, 600);
  background(255);
}

void draw(){
  if(frameCount % 600 == 0){
    background(255);
  }
  noFill();
  strokeWeight(1);
  //stroke(255, 255, 0);
  stroke(random(255));
  rect(random(-50, width), random(-50, height), 30, 30);  
  rect(random(-50, width), random(-50, height), 50, 50); 
  rect(random(-50, width), random(-50, height), 100, 100); 
}

void keyPressed(){
  if(key == 's'){
    saveFrame("myWork_#####.jpg");
  }
}