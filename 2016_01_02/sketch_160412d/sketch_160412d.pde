PImage img1, img2, img3, img4, img5, img6, img7;
PImage bk;
int sequence = 0;
float x = 0;
float y = 0;
float speedX = 10;
int j = 0;

void setup() {
  size(620, 880);
  background(255);

  img1 = loadImage("1.png");
  img2 = loadImage("2.png");
  img3 = loadImage("3.png");
  img4 = loadImage("4.png");
  img5 = loadImage("5.png");
  img6 = loadImage("6.png");
  img7 = loadImage("7.png");
  bk = loadImage("yellow_flower.png");
  x = width+300;
  y = random(height)-50;
  speedX = -15;
}

void draw() {
  background(255);
  image(bk, x-50, y, 100, 100);
  x += speedX;
  if(x < -150){
    x = width+300;
    y = random(height)-50;
  }
  if (sequence == 0) {
    image(img1, 0, 0, width, height);
  } else if (sequence == 1) {
    image(img2, 0, 0, width, height);
  } else if (sequence == 2) {
    image(img3, 0, 0, width, height);
  } else if (sequence == 3) {
    image(img4, 0, 0, width, height);
  } else if (sequence == 4) {
    image(img5, 0, 0, width, height);
  } else if (sequence == 5) {
    image(img6, 0, 0, width, height);
  } else if (sequence == 6) {
    image(img7, 0, 0, width, height);
  }
  if (frameCount % int(map(mouseX, 0, width, 1, 20)) == 0) {
    sequence++;
    if (sequence >= 6) {
      sequence = 0;
    }
  }
}