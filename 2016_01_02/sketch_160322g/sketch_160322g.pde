float x = 100.0;
float y = 100.0;
float xDir = 1;
float yDir = 1;
float dia = 50;
float t = 0.0;
float xSpeed;
float ySpeed;

void setup(){
  size(800, 600);
  background(255);
  xSpeed = random(2, 10);
  ySpeed = random(2, 10);
}

void draw(){
  dia = noise(t)*80 + 10;
  //dia = 50;
  t = t+0.036;
  x = x + xDir*xSpeed;
  y = y + yDir*ySpeed;
  ellipse(x, y, dia, dia);
  if(x > width-dia/2){
    x = width-dia/2;
    xDir = xDir*(-1);
  }
  if(x < dia/2){
    x = dia/2;
    xDir = xDir*(-1);
  }
  if(y > height-dia/2){
    y = height-dia/2;
    yDir = yDir*(-1);
  }
  if(y < dia/2){
    y = dia/2;
    yDir = yDir*(-1);
  }
}

void keyPressed(){
  if(key == 'x'){
    xSpeed = random(2, 10);
  }
  if(key == 'y'){
    ySpeed = random(2, 10);
  }
}