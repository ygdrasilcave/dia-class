int choice = 0;

void setup(){
  size(500, 500);
  background(0);
}

void draw(){
  noStroke();
  if(choice == 0){
    fill(255,0,0);
  }else if(choice == 1){
    fill(0,255,0);
  }else if(choice == 2){
    fill(0,0,255);
  }else if(choice == 3){
    fill(255,255,0);
  }else if(choice == 4){
    fill(0,255,255);
  }else if(choice == 5){
    fill(255,0,255);
  }else{
    fill(255,255,255);
  }
  rect(0,0,width,height);
}

void keyPressed(){
  choice = int(random(0, 7));
  println(choice);
}