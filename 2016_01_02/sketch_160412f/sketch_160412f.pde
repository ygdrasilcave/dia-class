PImage[] img;
int numImage = 7;
int sequence = 0;

void setup(){
  size(620, 880);
  img = new PImage[numImage];
  for(int i=0; i<numImage; i++){
    img[i] = loadImage((i+1) + ".png");
    //println((i+1) + ".png");
  }
  println(i);
}

void draw(){
  background(255);
  image(img[sequence], 0, 0, width, height);
  sequence++;
  if(sequence >= numImage-1){
    sequence = 0;
  }
}