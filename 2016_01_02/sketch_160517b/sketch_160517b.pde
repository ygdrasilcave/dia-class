int[] data;
int x = 0;
int y = 0;

void setup() {
  size(700, 700);
  data = new int[49];
  for (int i=0; i<49; i++) {
    float s = random(1);
    if (s < 0.5) {
      data[i] = 0;
    } else {
      data[i] = 1;
    }
  }
  frameRate(1);
}

void draw() {
  background(120);
  stroke(0);
  //for (int y=0; y<7; y++) {
  //  for (int x=0; x<7; x++) {    
  int index = x + y*7;
  if (data[index] == 1) {
    fill(255, 0, 0);
  } else {
    fill(0);
  }
  rect(x*100, y*100, 100, 100);
  //  }
  //}
  
  x++;
  if(x > 6){
    x = 0;
    y++;
    if(y > 6){
      y = 0;
    }
  }
}

void keyPressed() {
  for (int i=0; i<49; i++) {
    float s = random(1);
    if (s < 0.5) {
      data[i] = 0;
    } else {
      data[i] = 1;
    }
  }
}