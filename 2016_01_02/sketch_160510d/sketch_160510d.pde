import netP5.*;
import oscP5.*;
OscP5 server;
NetAddress client;

float r, g, b;
int x, y;

void setup(){
  size(900, 675);
  server = new OscP5(this, 13000);
  client = new NetAddress("localhost", 12000);
  background(255);
}

void draw(){
  noStroke();
  fill(r, g, b);
  ellipse(y, x, 10, 10);
}

void oscEvent(OscMessage msg){
  if(msg.checkAddrPattern("/color")==true){
    if(msg.checkTypetag("iifff")==true){
      int p1 = msg.get(0).intValue();
      int p2 = msg.get(1).intValue();
      float c1 = msg.get(2).floatValue();
      float c2 = msg.get(3).floatValue();
      float c3 = msg.get(4).floatValue();
      x = p1;
      y = p2;
      r = c1;
      g = c2;
      b = c3;
    }
  }
}