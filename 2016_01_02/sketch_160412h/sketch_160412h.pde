PImage img;

void setup() {
  img = loadImage("rainbow_flower.jpg");
  size(900, 675);
  background(255);
}

void draw() {
  background(0);
  //image(img, 0, 0);
  noStroke();
  int w = int(map(mouseX, 0, width, 1, 50));
  for (int y=0; y<img.height; y = y+w) {
    for (int x=0; x<img.width; x = x+w) {
      int index = x + y*img.width;
      color c = img.pixels[index];
      //color c = img.get(x, y);
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      fill(r, g, b);
      
      ellipse(x, y, w, w);
    }
  }
  /*int x = int(random(width));
   int y = int(random(height));
   
   color c = img.get(x, y);
   float r = red(c);
   float g = green(c);
   float b = blue(c);
   
   fill(r, g, b);
   text("&&", x, y);*/
}