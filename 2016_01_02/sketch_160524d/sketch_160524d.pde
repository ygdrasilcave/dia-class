PImage img;
int pixelSize = 1;

float[] r;
float[] g;
float[] b;

void setup() {
  size(1024, 1024);
  img = loadImage("house.jpg");
  r = new float[width*height];
  g = new float[width*height];
  b = new float[width*height];
  for (int i=0; i<width*height; i++) {
    color c = img.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  background(255);
}

void draw() {
  background(255);
  noStroke();
  float j = 0;
  for (int i=0; i<width*height; i+=pixelSize) {
    fill(r[width*height-1-i], g[width*height-1-i], b[width*height-1-i]);
    float y = cos(map(i%width, 0, width-1, HALF_PI, -HALF_PI))*j;
    rect(i%width, y, pixelSize, pixelSize);
    if(i%width == 0){
      j+=pixelSize;
    }
  }
}