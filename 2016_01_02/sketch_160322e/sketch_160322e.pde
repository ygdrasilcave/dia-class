float dia1 = 0.0;
float dia2 = 0.0;
float t = 0.0;

void setup(){
  size(800, 600);
  background(255);
}

void draw(){
  background(255);
  dia1 = random(0, 250);
  dia2 = noise(t)*250;
  ellipse(width/3, height/2, dia1, dia1);
  ellipse(2*width/3, height/2, dia2, dia2);
  t += 0.065;
}