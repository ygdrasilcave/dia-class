PImage img;
int pixelSize = 1;

float[] r;
float[] g;
float[] b;

void setup() {
  size(1024, 1024);
  img = loadImage("house.jpg");
  r = new float[width*height];
  g = new float[width*height];
  b = new float[width*height];
  for (int i=0; i<width*height; i++) {
    color c = img.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  background(255);
}

void draw() {
  background(255);
  noStroke();
  int j = 0;
  for (int i=0; i<width*height; i+=pixelSize) {
    fill(r[i], g[i], b[i]);
    rect(i%width, j, pixelSize, pixelSize);
    if(i%width == 0){
      j+=pixelSize;
    }
  }
}