float x;
float dir = 1;

void setup(){
  size(800, 800);
  x = width/2;
}

void draw(){
  background(255);
  line(mouseX, mouseY, width/2+x, height/2);
  
  x = x+dir*5;
  
  if(x > width/2-100){
    x = width/2-100;
    dir =  dir*-1;
  }
  if(x < -(width/2-100)){
    x = -(width/2-100);
    dir = dir*-1;
  }
}