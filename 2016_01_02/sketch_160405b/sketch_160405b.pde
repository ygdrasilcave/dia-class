float x = 0.0;
float speedX = 5.0;
float dia = 2.0;
float amp = 300.0;
float t = 0.0;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  translate(width/2, height/2);
  //rotate(radians(t));
  ellipse(x, 0, dia, dia);
  
  t = t+0.2;

  /*if (x < amp/2) {
    dia = map(x, 0, amp/2, 2, 50);
  }else{
    dia = map(x, amp/2, amp, 50, 2);
  }*/
  dia = abs(sin(x))*50;

  x = x + speedX;
  if (x > amp) {
    x = amp;
    speedX = speedX*-1;
  }
  if (x < 0) {
    x = 0;
    speedX = speedX*-1;
  }
}