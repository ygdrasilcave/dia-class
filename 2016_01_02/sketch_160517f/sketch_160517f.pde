import netP5.*;
import oscP5.*;

OscP5 server;
NetAddress sonicpi;

int[] data;
int i = 0;
int j = 0;
int pixelSize = 80;

void setup() {
  size(800, 800);
  data = new int[(width/pixelSize)*(height/pixelSize)];
  for (int i=0; i<data.length; i++) {
    data[i] = 0;
  }
  server = new OscP5(this, 13000);
  sonicpi = new NetAddress("localhost", 4557);
}

void draw() {
  background(120);
  noStroke();
  for (int y=0; y<int(height/pixelSize); y++) {
    for (int x=0; x<int(width/pixelSize); x++) {    
      int index = x + y*int(width/pixelSize);
      fill(data[index]*255);
      rect(x*pixelSize, y*pixelSize, pixelSize, pixelSize);
    }
  }
  //loadPixels();
  //color c = pixels[i + j*width];
  //float br = brightness(c);

  if (mousePressed == true) {
    if (mouseX >= 0 && mouseX < width-1 && mouseY >= 0 && mouseY < height-1) {
      int index = int(mouseX/pixelSize) + int(mouseY/pixelSize)*int(width/pixelSize);
      if (mouseButton == LEFT) {
        data[index] = 1;
      } else if (mouseButton == RIGHT) {
        data[index] = 0;
      }
    }
  }

  if (frameCount%10 == 0) {
    //println(br);

    i++;
    if (i > int(width/pixelSize)-1) {
      i = 0;
      j++;
      if (j > int(height/pixelSize)-1) {
        j = 0;
      }
    }
    println(data[i+j*int(width/pixelSize)]);
    if (data[i+j*int(width/pixelSize)]==1) {
      sendToSonicpi();
    }
  }
  noFill();
  stroke(255, 0, 0);
  strokeWeight(2);
  rect(i*pixelSize, j*pixelSize, pixelSize, pixelSize);
}

String sample = "C:/Users/ygdrasil/Documents/Processing/GIT_YonSei/2016_01_02/sketch_160517e/boom-uh-ah.wav";
void sendToSonicpi() {
  OscMessage msg = new OscMessage("/run-code");
  msg.add("id");
  String code = "";
  //code += "use_synth :fm\n play " + int(random(50, 90)) + ", amp: 0.3\n";
  code += "sample '" + sample + "', rate: " + 1;
  msg.add(code);
  server.send(msg, sonicpi);
}

void keyPressed() {
  if (key == '0') {
    for (int i=0; i<data.length; i++) {
      data[i] = 0;
    }
  }
}