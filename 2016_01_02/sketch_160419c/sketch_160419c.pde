PImage img;

void setup() {
  img = loadImage("rainbow_flower.jpg");
  size(900, 675);
  background(255);
}

void draw() {
  //background(0);
  noStroke();
  //image(img, 0, 0, width, height);
  
  int x = int(random(width));
  int y = int(random(height));
  
  color c = img.pixels[x + y*width];
  
  //stroke(0);
  fill(c);
  float w = random(10, 20);
  ellipse(x, y, w, w);
}