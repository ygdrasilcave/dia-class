void setup() {
  size(600, 600);
  background(0);
}

void draw() {
  background(0);

  noStroke();
  fill(255);
  for (int i=0; i<60; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(i*6));
    if (i%5 == 0) {
      ellipse(width/2-50, 0, 20, 20);
    }else{
      ellipse(width/2-50, 0, 10, 10);
    }
    popMatrix();
  }
  ellipse(width/2, height/2, 20, 20);

  noFill();
  stroke(255);
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(second()*6-90));
  ellipse(width/2-50, 0, 30, 30);
  popMatrix();
  
  fill(255);
  noStroke();
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(minute()*6-90));
  ellipse(width/2-100, 0, 30, 30);
  popMatrix();
  
  fill(255);
  noStroke();
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians((hour()%12)*30-90 + minute()*0.5));
  ellipse(width/2-150, 0, 30, 30);
  popMatrix();
  
  println(hour());
  println(hour()/12);
  println(hour()%12);
}