PShape eyeup, eyedown, nose, mouth;
float padX = 250;
float eyeLevel = 250;
float eyeW = 200;
float eyeH = 0.0;
float speedEye = 5;
float eyeHMax = 50;
float noseH = 0.0;
float t = 0.0;
int pSecond;

void setup() {
  size(800, 800);
  background(255);

  eyeup = loadShape("eyeup.svg");
  eyedown = loadShape("eyedown.svg");
  nose = loadShape("nose.svg");
  mouth = loadShape("mouth.svg");
  shapeMode(CENTER);
  noseH = random(150, 250);
  //println(nose.height);
  pSecond = second();
}

void draw() {
  background(255);

  noFill();
  stroke(0);
  //ellipse(width/2-padX, eyeLevel, eyeW, eyeH);
  //ellipse(width/2+padX, eyeLevel, eyeW, eyeH);

  fill(0);
  noStroke();
  ellipse(width/2-padX, eyeLevel, eyeHMax, eyeHMax);
  ellipse(width/2+padX, eyeLevel, eyeHMax, eyeHMax);

  fill(255);
  noStroke();
  ellipse(width/2-padX, eyeLevel, 20, 20);
  ellipse(width/2+padX, eyeLevel, 20, 20);

  shape(eyedown, width/2+padX, eyeLevel+map(eyeH, 0, eyeHMax, 0, eyeHMax-50)+80);
  shape(eyedown, width/2-padX, eyeLevel+map(eyeH, 0, eyeHMax, 0, eyeHMax-50)+80);

  shape(eyeup, width/2+padX, eyeLevel-eyeH-20);
  shape(eyeup, width/2-padX, eyeLevel-eyeH-20);
  //fill(255, 0,0);
  //ellipse(width/2+padX, eyeLevel-eyeH-20, 10, 10);

  shape(nose, width/2, height/2, nose.width+sin(t)*10, noseH);
  shape(mouth, width/2, height/2+noseH/2+100, mouth.width+cos(t)*20, mouth.height);

  t+=0.325;

  eyeH += speedEye;
  if (eyeH > eyeHMax) {
    eyeH = eyeHMax;
    speedEye *= -1;
  } else if (eyeH < 0) {
    eyeH = 0;
    speedEye *= -1;
  }

  if (pSecond != second()) {
    padX = random(110, 280);
    eyeLevel = random(200, 300);
    speedEye = random(1, 5);
    eyeHMax = random(30, 80);
    noseH = random(100, 250);
    pSecond = second();
  }
}

void keyPressed() {
  padX = random(110, 280);
  eyeLevel = random(200, 300);
  speedEye = random(1, 5);
  eyeHMax = random(30, 80);
  noseH = random(150, 250);
}