import netP5.*;
import oscP5.*;

OscP5 oscServer;
NetAddress iannix;

float posX1 = 0.0;
float posY1 = 0.0;
float posX2 = 0.0;
float posY2 = 0.0;

int colorChange1 = 0;
int colorChange2 = 0;

void setup() {
  size(600, 600);
  oscServer = new OscP5(this, 12000);
  iannix = new NetAddress("127.0.0.1", 13000);
}

void draw() {
  background(0);
  
  if(colorChange1 == 1){
    fill(255);
  }else if(colorChange1 == 2){
    fill(255, 0, 0);
  }else if(colorChange1 == 3){
    fill(0, 255, 0);
  }
  ellipse(width/2+posX1*50, height/2+posY1*50, 30, 30);
  if(colorChange2 == 1){
    fill(255);
  }else if(colorChange2 == 2){
    fill(255, 0, 0);
  }else if(colorChange2 == 3){
    fill(0, 255, 0);
  }
  ellipse(width/2+posX2*50, height/2+posY2*50, 30, 30);
}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/pos")==true) {
    if (theOscMessage.checkTypetag("ff")) {
      float firstValue = theOscMessage.get(0).floatValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      posX1 = firstValue;
      posY1 = secondValue;
      return;
    }
  } else if (theOscMessage.checkAddrPattern("/cursor")==true) {
    if (theOscMessage.checkTypetag("ff")) {
      float firstValue = theOscMessage.get(0).floatValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      posX2 = firstValue;
      posY2 = secondValue;
      return;
    }
  } else if(theOscMessage.checkAddrPattern("/small")==true){
    if (theOscMessage.checkTypetag("f")) {
      float firstValue = theOscMessage.get(0).floatValue();  
      colorChange1 = int(firstValue);
      return;
    }
  } else if(theOscMessage.checkAddrPattern("/big")==true){
    if (theOscMessage.checkTypetag("f")) {
      float firstValue = theOscMessage.get(0).floatValue(); 
      colorChange2 = int(firstValue);
      return;
    }
  } 
  //println("address pattern "+theOscMessage.addrPattern());
}