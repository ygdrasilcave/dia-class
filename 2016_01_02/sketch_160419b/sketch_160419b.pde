PImage img;

void setup() {
  img = loadImage("rainbow_flower.jpg");
  size(900, 675);
  background(255);
}

void draw() {
  background(0);
  noStroke();
  image(img, 0, 0, width, height);
  
  color c = img.pixels[mouseX + mouseY*width];
  
  stroke(0);
  fill(c);
  ellipse(mouseX, mouseY, 100, 100);
}