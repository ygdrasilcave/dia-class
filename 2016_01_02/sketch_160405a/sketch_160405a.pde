float x = 0.0;
float speedX = 2.0;
float d = 0.0;
float ph = 200.0;
float c = 0.0;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  
  fill(255);
  stroke(c);
  translate(width/2, height/2);
  rotate(radians(d));
  rect(x, 0, 20, 20);
  
  d = d + 0.2;
  if(d > 360 ){
    d = 0;
    c = c + 50;
    ph = ph - 20;
    if(ph < 0){
      ph = 200.0;
    }
  }
  
  x = x + speedX;
  if(x > ph){
    x = ph;
    speedX = speedX*-1;
  }
  if(x < 0){
    x = 0;
    speedX = speedX*-1;
  }
}