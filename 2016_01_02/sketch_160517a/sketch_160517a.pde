import netP5.*;
import oscP5.*;
OscP5 server;
NetAddress client;
NetAddress client2;

PImage img;
int pSecond = 0;
boolean toggle = true;
int x, y;

void setup() {
  size(900, 675);
  server = new OscP5(this, 12000);
  client = new NetAddress("localhost", 13000);
  client = new NetAddress("localhost", 13001);
  img = loadImage("rainbow_flower.jpg");
  pSecond = second();
  x = 0;
  y = 0;
}

void draw() {
  image(img, 0, 0);
  //if (second() != pSecond) {
  //if(frameCount%5 == 0){
    //if (toggle == true) {
      int index = x + y*img.width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);

      sendOSCMsg(x, y, r, g, b);
      
      if(br > 128){
        sendOSCSound();
      }

      x+=10;
      if (x>width-1) {
        x = 0;
        y+=10;
        if (y>height-1) {
          y = 0;
        }
      }
    //}
    //toggle = false;
    //pSecond = second();
  //}
}

void sendOSCMsg(int _x, int _y, float _r, float _g, float _b) {
  OscMessage msg = new OscMessage("/color");
  msg.add(_x);
  msg.add(_y);
  msg.add(_r);
  msg.add(_g);
  msg.add(_b);
  server.send(msg, client);
}

void sendOSCSound(){
  OscMessage msg = new OscMessage("/sound");
  msg.add(1);
  server.send(msg, client2);
}