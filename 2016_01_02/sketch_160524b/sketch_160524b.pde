PImage img;
int pixelSize = 20;

float[] r;
float[] g;
float[] b;

void setup() {
  size(900, 675);
  img = loadImage("flower-07.jpg");
  r = new float[width*height];
  g = new float[width*height];
  b = new float[width*height];
  for(int i=0; i<width*height; i++){
    color c = img.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  background(255);
  r = sort(r);
  g = sort(g);
  b = sort(b);
}

void draw() {
  background(255);
  noStroke();
  for(int y=0; y<height; y++){
    for(int x=0; x<width; x++){
      int index = x + y*width;
      fill(r[index], g[index], b[index]);
      rect(x, y, 1, 1);
    }
  }
}