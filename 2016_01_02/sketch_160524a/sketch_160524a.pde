PImage img;
boolean mode = true;
int pixelSize = 20;

void setup() {
  size(1024, 1024);
  img = loadImage("house.jpg");
}

void draw() {
  background(255);
  noStroke();
  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      if (mode == true) {
        fill(r, g, b);
        rect(x, y, pixelSize, pixelSize);
      } else {
        if (x%2 == 0) {
          fill(r, 0, 0);
          rect(x, height-1-y, pixelSize, pixelSize);
        } else {
          fill(0, g, 0);
          rect(x, y, pixelSize, pixelSize);
        }
      }
    }
  }
  //image(img, 0, 0);
}

void keyPressed(){
  mode = !mode;
}