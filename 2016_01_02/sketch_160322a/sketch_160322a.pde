int counter = 0;
boolean toggle = true;

void setup() {
  size(800, 800);
  background(255);
  textSize(60);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(255);
  fill(0);
  text("bang! - " + counter, width/2, 200);
  text(hour() + ":" + minute() + ":" + second(), width/2, 300);
  if (second()%5 == 0) {
    if (toggle == true) {
      println("bang! - " + counter);
      counter = counter+1;
      toggle = false;
    }
  } else if (second()%5 == 1) {
    if (toggle == false) {
      toggle = true;
    }
  }
  //println(second());
  //println(frameRate);
}