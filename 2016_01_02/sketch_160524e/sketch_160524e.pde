PImage img;
int pixelSize = 1;

float[] r;
float[] g;
float[] b;
float tStep;

void setup() {
  size(1024, 1024);
  img = loadImage("house.jpg");
  r = new float[width*height];
  g = new float[width*height];
  b = new float[width*height];
  for (int i=0; i<width*height; i++) {
    color c = img.pixels[i];
    r[i] = red(c);
    g[i] = green(c);
    b[i] = blue(c);
  }
  background(255);
  tStep = PI/width;
}

void draw() {
  background(255);
  noStroke();
  for(int y=0; y<height/2; y++){
    float t = 0.0;
    for(int x=0; x<width; x++){
      int index = x + y*width;
      fill(r[index], g[index], b[index]);
      float x1 = width/2 + cos(PI+t)*(width/2-y);
      float y1 = height/2 + sin(PI+t)*(width/2-y);
      rect(x1, y1, 1, 1);
      t+=tStep;
    }
  }
  for(int y=height/2; y<height; y++){
    float t = 0.0;
    for(int x=0; x<width; x++){
      int index = x + y*width;
      fill(r[index], g[index], b[index]);
      float x1 = width/2 + cos(PI+t)*(width/2-y);
      float y1 = height/2 + sin(PI+t)*(width/2-y);
      rect(x1, y1, 1, 1);
      t+=tStep;
    }
  }
  
}