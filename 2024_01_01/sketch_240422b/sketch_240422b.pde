float st = 0;
float T60 = TWO_PI/60.0;
float T12 = TWO_PI/12.0;

void setup() {
  size(800, 800);
  background(255);
  textAlign(CENTER, CENTER);
  textSize(23);
}

void draw() {
  background(255);
  noFill();
  stroke(0);
  strokeWeight(1);
  ellipse(width/2, height/2, 600, 600);
  drawNumbers(width/2, height/2, 300);
  
  float C_HOUR = hour();
  float C_MINUTE = minute();
  //C_MINUTE = 59;
  float C_SECOND = second();
  println(C_HOUR+":"+C_MINUTE+":"+C_SECOND);

  float sec = T60 * C_SECOND - HALF_PI;
  float sx = width/2 + cos(sec)*280;
  float sy = height/2 + sin(sec)*280;
  stroke(255, 0, 0);
  strokeWeight(1);
  line(width/2, height/2, sx, sy);
  pushMatrix();
  translate(width/2, height/2);
  rotate(sec);
  rect(0, -5, 200, 10);
  popMatrix();

  float min = T60 * C_MINUTE - HALF_PI;
  float mx = width/2 + cos(min)*250;
  float my = height/2 + sin(min)*250;
  stroke(0, 0, 0);
  strokeWeight(3);
  line(width/2, height/2, mx, my);

  //float hour = (T12 * C_HOUR - HALF_PI) + map(C_MINUTE, 0, 59, 0, T12);
  float hour = (T12 * C_HOUR - HALF_PI) + C_MINUTE*((HALF_PI/3.0)/60.0);
  float hx = width/2 + cos(hour)*200;
  float hy = height/2 + sin(hour)*200;
  stroke(0, 0, 0);
  strokeWeight(5);
  line(width/2, height/2, hx, hy);
  
  
  for(int i=0; i<60; i++){
    float dx = width/2 + cos(i*T60)*300;
    float dy = height/2 + sin(i*T60)*300;
    fill(0);
    noStroke();
    ellipse(dx, dy, 10, 10);
    if(i%5 == 0){
      noFill();
      stroke(0);
      strokeWeight(1);
      ellipse(dx, dy, 16, 16);
    }
  }
}


void drawNumbers(float _tx, float _ty, float _r) {
  for (int i=0; i<12; i++) {
    float _t = TWO_PI/12.0;
    float _x = _tx + cos(_t*i - (HALF_PI/3.0)*2.0)*(_r+25);
    float _y = _ty +sin(_t*i - (HALF_PI/3.0)*2.0)*(_r+25);
    fill(0);
    noStroke();
    pushMatrix();
    translate(_x, _y);
    //rotate(T60 * second());
    text(i+1, 0, 0);
    popMatrix();
  }
  
  for(int i=0; i<60; i++){
    float _t = TWO_PI/60.0;
    float _x = _tx + cos(_t*i)*_r;
    float _y = _ty +sin(_t*i)*_r;
    stroke(0);
    strokeWeight(1);
    pushMatrix();
    translate(_x, _y);
    rotate(_t*i);
    line(-5, 0, 5, 0);
    popMatrix();
  }
}
