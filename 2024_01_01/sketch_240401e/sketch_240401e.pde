PImage img;
float t = 0.0;

void setup() {
  size(800, 800);
  background(0);
  img = loadImage("tree.png");
}

void draw() {
  background(0);
  float imgW = img.width/5;
  float imgH = img.height/5;
  
  translate(mouseX, mouseY);
  /*if(mousePressed == true){
    rotate(radians(90));
  }else{
    rotate(radians(0));
  }*/
  rotate(sin(t));
  image(img, -imgW/2+5, -imgH, imgW, imgH);
  fill(255, 0, 0);
  ellipse(0, 0, 10, 10);
  t+=0.055;
}
