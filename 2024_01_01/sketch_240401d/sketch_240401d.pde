PImage img;
PImage img2;

void setup() {
  size(800, 800);
  background(0);
  //colorMode(HSB, 360, 100, 100);
  noStroke();
  img = loadImage("tree.png");
  img2 = loadImage("cloud1.png");
  /*for (int x = 0; x<360; x++) {
    float w = width/360.0;
    fill(x, 100, 100);
    rect(x*w, 0, w, height);
  }*/
}

void draw() {
  float imgW2 = img2.width/3;
  float imgH2 = img2.height/3;
  for (int y=0; y<height/imgH2; y++) {
    for (int x=0; x<width/imgW2; x++) {
      image(img2, x*imgW2, y*imgH2, imgW2, imgH2);
    }
  }
  
  float imgW = img.width/4;
  float imgH = img.height/4;
  image(img, mouseX-imgW/2, mouseY-imgH/2, imgW, imgH);
}
