PImage img;
int pixelSize = 20;

void setup(){
  size(800, 800);
  img = loadImage("flower.jpg");
}

void draw(){
  background(255);
  //image(img, 0, 0);
  
  pixelSize = int(map(mouseX, 0, width, 1, 50));
  
  for(int y=0; y<height; y+=pixelSize){
    for(int x=0; x<width; x+=pixelSize){
      //color c = img.get(x, y);
      int index = x + y*img.width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      noStroke();
      fill(c);
      rect(x, y, pixelSize, pixelSize);
      //ellipse(x+pixelSize*0.5, y+pixelSize*0.5, pixelSize, pixelSize);
    }
  }
}
