float x1 = 0;
float y1 = 0;
float y2 = 0;
float y3 = 0;
float t3 = 0;
float y4 = 0;
float t4 = 0;

void setup(){
  size(800, 200);
  background(255);
  y1 = height/2;
}

void draw(){
  fill(0);
  noStroke();
  ellipse(x1, y1, 5, 5);
  x1 += 1;
  
  fill(255, 0, 0);
  ellipse(x1, y2, 5, 5);
  y2 = random(height);
  
  fill(0, 255, 0);
  ellipse(x1, y3, 5, 5);
  y3 = noise(t3)*height;
  t3 += 0.02;
  
  fill(0, 0, 255);
  ellipse(x1, y4, 5, 5);
  y4 = map(cos(t4), -1, 1, 0, height);
  t4 += 0.065;
}
