float x = 0;
float y = 0;

void setup(){
  size(800, 800);
  background(255);
  x = width/2;
  y = height/2;
}

void draw(){
  background(255);
  ellipse(x, y, 50, 50);
  
  if(mousePressed == true){
    x = mouseX;
    y = mouseY;
  }
}
