float x = 100;
float speed = 2;
float direction = 1;

void setup(){
  size(800, 800);
  //rectMode(CENTER);
}

void draw(){
  //background(0);
  noStroke();
  fill(0, 15);
  rect(0, 0, width, height);
  fill(255);
  noFill();
  stroke(255);
  //ellipse(x, height/2, 50, 50);
  rect(x-25, height/2-10, 50, 20);
  ellipse(x+25, height/2, 20, 20);
  x += speed*direction;
  if((x > width-25) || (x < 25)){
    direction *= -1;
  }
}
