PImage img;

void setup(){
  size(800, 800);
  img = loadImage("flower.jpg");
}

void draw(){
  background(255);
  //image(img, 0, 0);
  
  for(int y=0; y<height; y++){
    for(int x=0; x<width; x++){
      //color c = img.get(x, y);
      int index = x + y*img.width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      noStroke();
      if(br > 200){
        fill(255);
      }else if(br <= 200 && br > 120){
        fill(160);
      }else if(br <= 120 && br > 60){
        fill(80);
      }else{
        fill(0);
      }
      rect(x, y, 1, 1);
    }
  }
}
