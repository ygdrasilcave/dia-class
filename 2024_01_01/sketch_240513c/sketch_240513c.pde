float[] x;
float[] y;

void setup() {
  size(800, 800);
  x = new float[80];
  y = new float[80];

  make_shape();
}

void draw() {
  background(0);
  //noFill();
  //stroke(255);
  /*fill(255);
  beginShape();
  // Reads one array element every time through the for()
  for (int i = 0; i < x.length; i++) {
    vertex(x[i], y[i]);
  }
  endShape(CLOSE);*/

  noFill();
  stroke(255);
  for (int i = 0; i < x.length; i++) {
    ellipse(x[i], y[i], 200, 200);
  }
}
void make_shape(){
  float radius2 = random(50, 250);
  for (int i=0; i<x.length; i++) {
    float angle = radians((360.0/(x.length))*i-90);
    float radius = 300;    
    if (i%2 == 0) {
       radius = 300;
    } else {
       radius = radius2;
    }
    x[i] = width/2 + cos(angle)*radius;
    y[i] = height/2 + sin(angle)*radius;
  }
}

void keyPressed(){
  if(key == ' '){
    make_shape();
  }
}
