float x = 0.0;
float y = 0.0;
float t = 0.0;
float tSpeed = 0.015;
float radius = 0.0;

void setup(){
  size(800, 800);
  radius = width/2 - 100;
}

void draw(){
  //background(0);
  noStroke();
  fill(0, 4);
  rect(0, 0, width, height);
  
  //t = HALF_PI * 0.5;
  x = width/2 + cos(t)*radius;
  y = height/2 + sin(t)*(radius*0.75);
  fill(255);
  ellipse(x, y, 30, 30);
  
  
  float x2 = x + cos(t*5.0)*(radius*0.25);
  float y2 = y + sin(t*5.0)*(radius*0.25);
  ellipse(x2, y2, 15, 15);
  
  //stroke(255);
  //line(width/2, height/2, x, y);
  //line(x, y, x2, y2);
  
  t += tSpeed;
}
