PImage img;

void setup(){
  size(800, 800);
  img = loadImage("flower.jpg");
}

void draw(){
  background(255);
  image(img, 0, 0);
  
  color c = img.get(mouseX, mouseY);
  float r = red(c);
  float g = green(c);
  float b = blue(c);

  //fill(r, g, b);
  //noStroke();
  //ellipse(mouseX, mouseY, 100, 100);
  
  noStroke();
  fill(0);
  rect(mouseX-52, mouseY-52, 104, 104);
  fill(c);
  rect(mouseX-50, mouseY-50, 100, 50);
  fill(r, 0, 0);
  rect(mouseX-50, mouseY, 100/3.0, 50);
  fill(0, g, 0);
  rect(mouseX-(100/3.0)/2.0, mouseY, 100/3.0, 50);
  fill(0, 0, b);
  rect(mouseX+(100/3.0)/2.0, mouseY, 100/3.0, 50);
  
}
