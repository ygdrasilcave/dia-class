import processing.sound.*;

AudioIn mic;
Amplitude loudness;

void setup(){
  size(800, 800);
  background(255);  
  mic = new AudioIn(this, 0);
  mic.start();
  loudness = new Amplitude(this);
  loudness.input(mic);
}

int lastSample = 0;
float alpha = 0.2;

void draw(){
  background(255);
  float volume = loudness.analyze();
  println(volume);
  int val = int(map(volume, 0, 0.5, 1, 750));
  int val_filter = int(val*alpha + lastSample*(1.0-alpha));
  lastSample = val_filter;
  
  //println(val_filter);
  fill(0);
  ellipse(val_filter, 300, 100, 100);
  ellipse(val, 600, 100, 100);
}
