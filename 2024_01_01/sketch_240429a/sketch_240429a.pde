float x = 0; // X-coordinate
float y = 0; // Y-coordinate

void setup() {
  size(800, 800);
  randomSeed(0); // Force the same random values
  background(0);
  stroke(255);
  x = width/2;
  y = height/2;
}

void draw() {
  x += random(-2, 2); // Assign new x-coordinate
  y += random(-2, 2); // Assign new y-coordinate
  point(x, y);
}
