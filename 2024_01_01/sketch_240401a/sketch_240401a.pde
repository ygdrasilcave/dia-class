void setup(){
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
}

void draw(){
  noStroke();
  float h = random(360);
  float s = 60;
  float b = 100;
  fill(h, s, b);
  float x = random(width);
  float y = random(height);
  float w = random(10, 30);
  ellipse(x, y, w, w);
}
