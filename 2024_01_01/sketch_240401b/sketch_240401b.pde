void setup(){
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
  noStroke();
}

void draw(){
  for(int x = 0; x<360; x++){
    float w = width/360.0;
    fill(x, 100, 100);
    rect(x*w, 0, w, height);
  }
}
