float dia = 0;
float t = 0.0;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  background(255);
  //noFill();
  //stroke(0);
  float sineValue = sin(t);
  fill(sineValue*128 + 128, 0, 0);
  //dia = abs(sin(t)) * width;
  dia = sineValue * 400 + 400;
  ellipse(width/2, height/2, dia, dia);
  t += 0.03559;
}
