PImage img, img2;
int num = 20;
float hVal = 0;
float t = 0.0;
boolean select = true;

void setup() {
  size(800, 800);
  background(0);
  img = loadImage("tree.png");
  img2 = loadImage("cloud1.png");
}

void draw() {
  background(0);

  num = int(map(cos(t), -1, 1, 3, 30));
  float rotIntv = TWO_PI/float(num);

  float imgW = img.width/5;
  float imgH = img.height/5;
  hVal = map(mouseY, 0, height, -imgH*1.5, imgH);

  for (int i=0; i<num; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(i*rotIntv);
    if (select == true) {
      image(img, -imgW/2+5, hVal, imgW, imgH);
    } else {
      image(img2, -imgW/2+5, hVal, imgW, imgH);
    }
    fill(255, 0, 0);
    ellipse(0, 0, 10, 10);
    popMatrix();
  }
  t+=0.025;
}

void keyPressed() {
  if (key == '1') {
    select = true;
  }
  if (key == '2') {
    select = false;
  }
}
