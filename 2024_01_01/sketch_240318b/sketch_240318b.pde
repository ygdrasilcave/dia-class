float r = 30;

void setup() {
  size(800, 600);
  background(255);
}

void draw() {
  //background(255);
  //float x = random(-100, width+100);
  //float y = random(-100, height+100);
  float x = mouseX;
  float y = mouseY;
  if (keyPressed == true) {
    if (key == '1') {
      line(x-r*sqrt(2), y, x+r*sqrt(2), y);
    } else if (key == '2') {
      line(x, y-r*sqrt(2), x, y+r*sqrt(2));
    } else if (key == '3') {
      line(x-r, y-r, x+r, y+r);
    } else if (key == '4') {
      line(x-r, y+r, x+r, y-r);
    }
  }
}
