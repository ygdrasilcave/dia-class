float angle = 0;
float rt = 0;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  //background(255);
  //float t = radians(angle);
  float t = angle;
  float r = map(sin(rt), -1, 1, 100, 400); 
  float x = width/2 + cos(t)*r;
  float y = height/2 + sin(t)*r;  
  fill(0);
  //rect(x-25, y-25, 50, 50);
  ellipse(x, y, 10, 10);
  //line(width/2, height/2, x, y);
  angle += 0.005;
  rt += 0.05;
}
