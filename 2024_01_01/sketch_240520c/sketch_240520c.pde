PImage img;

void setup(){
  size(800, 800);
  img = loadImage("man.jpg");
}

void draw(){
  background(255);
  //image(img, 0, 0);
  
  for(int y=0; y<height; y++){
    for(int x=0; x<width; x++){
      //color c = img.get(x, y);
      int index = x + y*img.width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      noStroke();
      if(br > map(mouseX, 0, width, 0, 255)){
        fill(255);
      }else{
        fill(0);
      }
      rect(x, y, 1, 1);
    }
  }
}
