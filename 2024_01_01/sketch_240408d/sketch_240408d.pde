String[] txt;
PFont font;
int index = 0;
int fontIndex = 0;
float a = 0;

void setup(){
  size(800, 800);
  background(0);
  for(int i=0; i<PFont.list().length; i++){
    println(i + " : " + PFont.list()[i]);
  }
  //218, 327, 215
  font = createFont(PFont.list()[218], 90);
  textFont(font);
  textSize(45);
  textAlign(CENTER, CENTER);
  txt = loadStrings("hangul2.txt");
}

void draw(){
  //background(0);
  fill(0, 5);
  noStroke();
  rect(0, 0, width, height);
  float g = height/txt.length;
  fill(255);
  for(int i=0; i<txt.length; i++){
    pushMatrix();
    translate(g*i, height/2);
    if(i%2==0){
      rotate(a);
    }else{
      rotate(-a); 
    }
    text(txt[i], 0, 0);
    popMatrix();
  }
  a += 0.022;
}

void keyPressed(){
}
