float t = 0;
String txt = "?";

void setup(){
  size(800, 800);
  background(0);
  textSize(90);
  textAlign(CENTER, CENTER);
}

void draw(){
  background(0);
  noStroke();
  fill(255);
  float gap = map(mouseX, 0, width, 10, 120);
  for(float y=0; y<height; y += gap){
    for(float x=0; x<width; x += gap){
      pushMatrix();
      translate(x, y);
      rotate(t);
      text(txt, 0, 0);
      popMatrix();
    }
  }
  //stroke(0, 255, 0);
  //line(0, height/2, width, height/2);
  //line(width/2, 0, width/2, height);
  t+=0.055;
}

void keyPressed(){
  txt = str(key);
}
