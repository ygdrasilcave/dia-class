void setup(){
  size(600, 800);
}

void draw(){
  background(255);
  
  stroke(0);
  strokeWeight(1);
  point(width/2, height/2);
  line(0, 0, width, height);
  line(width, 0, 0, height);
  
  fill(0, 255, 255);
  noStroke();
  ellipse(width/2, height/2, 200, 200);
  rect(width/2, height/2, 100, 100);
  
  stroke(0);
  //strokeWeight(10);
  noFill();
  beginShape();
  vertex(mouseX, mouseY);
  vertex(300, 300);
  vertex(250, 500);
  vertex(50, 400);
  vertex(150, 200);
  endShape(CLOSE);
  
  println(mouseX + "," + mouseY);
}
