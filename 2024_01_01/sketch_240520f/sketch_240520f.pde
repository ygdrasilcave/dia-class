PImage img;

void setup() {
  size(800, 800);
  img = loadImage("man.jpg");
  background(255);
}

void draw() {
  //background(255);
  for (int i=0; i<10; i++) {
    int x = int(random(width));
    int y = int(random(height));
    int index = x + y * img.width;
    color c = img.pixels[index];
    fill(c);
    noStroke();
    float d = random(5, 10);
    ellipse(x, y, d, d);
  }
}
