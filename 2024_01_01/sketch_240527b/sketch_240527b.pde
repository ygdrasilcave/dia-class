/**
 * http://www.looksgood.de/libraries/Ani/Ani_Cheat_Sheet.pdf
 *
 * Ani.LINEAR
 * Ani.QUAD_IN
 * Ani.QUAD_OUT
 * Ani.QUAD_IN_OUT
 * Ani.CUBIC_IN
 * Ani.CUBIC_IN_OUT
 * Ani.CUBIC_OUT
 * Ani.QUART_IN
 * Ani.QUART_OUT
 * Ani.QUART_IN_OUT
 * Ani.QUINT_IN
 * Ani.QUINT_OUT
 * Ani.QUINT_IN_OUT
 * Ani.SINE_IN
 * Ani.SINE_OUT
 * Ani.SINE_IN_OUT
 * Ani.CIRC_IN
 * Ani.CIRC_OUT
 * Ani.CIRC_IN_OUT
 * Ani.EXPO_IN
 * Ani.EXPO_OUT
 * Ani.EXPO_IN_OUT
 * Ani.BACK_IN
 * Ani.BACK_OUT
 * Ani.BACK_IN_OUT
 * Ani.BOUNCE_IN
 * Ani.BOUNCE_OUT
 * Ani.BOUNCE_IN_OUT
 * Ani.ELASTIC_IN
 * Ani.ELASTIC_OUT
 * Ani.ELASTIC_IN_OUT
 */

import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, Ani.CUBIC_IN, Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, Ani.QUART_IN, Ani.QUART_OUT, Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT};
String[] easingsVariableNames = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", "Ani.CUBIC_IN", "Ani.CUBIC_IN_OUT", "Ani.CUBIC_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT"};

float x = 0;
float y = 0;
float diameterW = 10;
float diameterH = 50;

Ani ani_diameterW;
Ani ani_diameterH;

void setup() {
  size(800, 800);  
  background(255);
  x = width/2;
  y = height/2;
  
  // you have to call always Ani.init() first!
  Ani.init(this);
  
  float _secondW = random(1, 5);
  float _secondH = random(1, 5);
  float _targetW = random(width*0.5, width);
  float _targetH = random(width*0.5, width);
  ani_diameterW = new Ani(this, _secondW, "diameterW", _targetW, Ani.ELASTIC_OUT);
  ani_diameterH = new Ani(this, _secondH, "diameterH", _targetH, Ani.LINEAR);
  //Ani.to(this, _secondW, "diameterW", _targetW, Ani.ELASTIC_OUT);
  //Ani.to(this, _secondH, "diameterH", _targetH, Ani.LINEAR);
  
  //print(easings.length);
}

void draw() {
  noFill();
  stroke(0);
  strokeWeight(0.25);
  ellipse(x, y, diameterW, diameterH);
}

void mousePressed(){
  background(255);
  float _secondW = random(1, 5);
  float _secondH = random(1, 5);
  float _beginW = 20;
  float _beginH = 20;
  float _targetW = random(width*0.5, width);
  float _targetH = random(width*0.5, width);
  int indexW = int(random(0, easings.length));
  int indexH = int(random(0, easings.length));
  ani_diameterW.setDuration(_secondW);
  ani_diameterH.setDuration(_secondH);  
  ani_diameterW.setEnd(_targetW);
  ani_diameterH.setEnd(_targetH);
  ani_diameterW.setBegin(_beginW);
  ani_diameterH.setBegin(_beginH);  
  ani_diameterW.setEasing(easings[indexW]);
  ani_diameterH.setEasing(easings[indexH]);
  
  ani_diameterW.start();
  ani_diameterH.start();
  
  //Ani.to(this, _secondW, "diameterW", _targetW, easings[indexW]);
  //Ani.to(this, _secondH, "diameterH", _targetH, easings[indexH]);
  println(easingsVariableNames[indexW]);
  println(easingsVariableNames[indexH]);
  println("___________________________");
}
