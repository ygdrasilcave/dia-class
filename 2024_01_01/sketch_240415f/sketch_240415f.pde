float x = 0;
float y = 0;
float targetX = 0;
float targetY = 0;
float diffX = 0;
float diffY = 0;
float startX = 0;
float startY = 0;
float pct = 0;
float step = 0.02;
boolean startMove = false;

void setup() {
  size(800, 800);
  background(255);
  x = width/2;
  y = height/2;
}

void draw() {
  //background(255);

  if (startMove == true) {
    pct += step;
    if (pct < 1.0) {
      x = startX + (pct * diffX);
      y = startY + (pct * diffY);
    }else{
      startMove = false;
    }
  }
  ellipse(x, y, 50, 50);
}

void mouseReleased() {
  targetX = mouseX;
  targetY = mouseY;
  startX = x;
  startY = y;
  diffX = targetX - x;
  diffY = targetY - y;
  pct = 0.0;
  startMove = true;
}
