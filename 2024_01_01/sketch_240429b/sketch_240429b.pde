float x = 0.0; // X-coordinate
float y = 0.0; // Y-coordinate
float angle = 0.0; // Direction of motion
float speed = 3; // Speed of motion

void setup() {
  size(800, 800);
  background(0);
  stroke(255, 130);
  randomSeed(121); // Force the same random values
  y = height/2;
  x = width/2;
}

void draw() {
  angle += random(-0.3, 0.3);
  //angle += 0.01;
  x += cos(angle) * speed; // Update x-coordinate
  y += sin(angle) * speed; // Update y-coordinate
  translate(x, y);
  rotate(angle);
  line(0, -30, 0, 30);
  if(x > width){
    x = 0;  
  }else if(x < 0){
    x = width;
  }
  
  if(y > height){
    y = 0;
  }else if(y < 0){
    y = height;
  }
}
