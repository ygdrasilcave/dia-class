PImage[] img;
int index = 0;

void setup() {
  size(800, 800);
  img = new PImage[12];

  /*img[0] = loadImage("PT_anim0000.gif");
  img[1] = loadImage("PT_anim0001.gif");
  img[2] = loadImage("PT_anim0002.gif");
  img[3] = loadImage("PT_anim0003.gif");
  img[4] = loadImage("PT_anim0004.gif");
  img[5] = loadImage("PT_anim0005.gif");
  img[6] = loadImage("PT_anim0006.gif");
  img[7] = loadImage("PT_anim0007.gif");
  img[8] = loadImage("PT_anim0008.gif");
  img[9] = loadImage("PT_anim0009.gif");
  img[10] = loadImage("PT_anim0010.gif");
  img[11] = loadImage("PT_anim0011.gif");*/
  
  for(int i=0; i<img.length; i++){
    img[i] = loadImage("PT_anim00" + nf(i, 2) + ".gif");
  }
}

void draw() {
  background(255);
  image(img[index], width/2-img[index].width/2, height/2-img[index].height/2);
  if (frameCount%2 == 0) {
    index = index + 1;
    if (index > img.length-1) {
      index = 0;
    }
  }
}
