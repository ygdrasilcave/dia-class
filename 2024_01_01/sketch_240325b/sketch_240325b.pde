int x = 25;

void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  noFill();
  stroke(0);
  ellipse(x, height/2, 50, 50);
  x = x+2;
  if(x >= width-25){
    x = 25;
  }
}
