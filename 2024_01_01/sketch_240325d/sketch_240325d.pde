int dia = 0;
int speed = 10;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  //background(255);
  noFill();
  stroke(0);
  ellipse(width/2, height/2, dia, dia);
  dia = dia+speed;
  if(dia >= width){
    speed = speed*-1;
  }
  if(dia <= 0){
    speed = speed*-1;
  }
}
