void setup() {
  size(800, 600);
  background(255);
}

void draw() {
  background(255);
  noFill();
  //stroke(0);
  noStroke();
  for(int y=0; y<height/40; y++){
    for(int x=0; x<width/40; x++){
      if(y%2==0){
        if(x%2==0){ 
          fill(0);
        }else{
          fill(255);
        }
      }else if(y%2==1){
        if(x%2==0){ 
          fill(255);
        }else{
          fill(0);
        }
      }
      /*if((x+y)%2==0){
        fill(0);
      }else{
        fill(255);
      }*/
      rect(x*40, y*40, 40, 40);
    }
  }
}
