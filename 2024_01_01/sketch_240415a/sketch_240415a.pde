PImage img;
float y = 50.0;
float speed = 5.0;
float imgSize = 150.0;
float t = 0;

void setup(){
  size(800, 800);
  img = loadImage("asterisk.png");
  //ellipseMode(RADIUS);
  //rectMode(CENTER);
}

void draw(){
  background(255);
  
  //speed = map(mouseX, 0, width, 0.2, 8);
  //speed = random(0.2, 8);
  /*speed = map(noise(t), 0, 1, 0.2, 8);
  t += 0.02;*/
  speed = map(cos(t), -1, 1, 0.2, 8);
  t += 0.065;
  
  fill(255, 0, 0);
  noStroke();  
  image(img, 300-imgSize*0.5, y-imgSize*0.5, imgSize, imgSize);
  ellipse(300, y, 10, 10);
  y = y + speed; 
  if(y > height + imgSize){
    y = -imgSize;
  }
}
