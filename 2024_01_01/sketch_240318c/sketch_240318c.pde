void setup() {
  size(800, 600);
  background(255);
}

void draw() {
  background(255);
  noFill();
  stroke(0);
  for(int i=0; i<5; i=i+1){
    ellipse(width/2, height/2, (i+1)*100, (i+1)*100);
  }
  //float d = 20;
  for(int i=0; i<int(width/20)+1; i=i+1){
    ellipse(i*20, height/2, 20, 20);
  }
  for(int i=0; i<width; i=i+20){
    ellipse(i, height/2 + 100, 20, 20);
  }
}
