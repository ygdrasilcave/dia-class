int w = 0;
void setup() {
  size(600, 800);
  background(255);
  frameRate(1);
  textSize(60);
}

void draw() {
  //background(255);
  stroke(0);
  noFill();
  //w = int(random(10, 150));
  ellipse(random(0, width), random(0, height), w, w);
  
  fill(255);
  noStroke();
  rect(0, 0, 110, 60); 
  fill(0);
  text(w, 10, 55);
  println(w);
  
  getNewSensorValue();
}

void getNewSensorValue(){
  w = int(random(10, 200));
}
