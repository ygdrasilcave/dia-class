int x = 25;
int speed = 6;
int dir = 1;

void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  noFill();
  stroke(0);
  ellipse(x, height/2, 50, 50);
  x = x + dir*speed;
  if(x >= width-25){
    dir = -1;
  }else if(x <= 25){
    dir = 1;
  }

}
