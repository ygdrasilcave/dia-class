int s = 2;

void setup() {
  size(800, 600);
  background(255);
}

void draw() {
  //background(255);
  float x = random(-100, width+100);
  float y = random(-100, height+100);

  if (s == 0) {
    line(x, y, x+100*sqrt(2), y);
  } else if (s == 1) {
    line(x, y, x, y+100*sqrt(2));
  } else if (s == 2) {
    line(x, y, x+100, y+100);
  } else if (s == 3) {
    line(x, y, x+100, y-100);
  }
}

void keyPressed() {
  if (key == 'q') {
    s = 0;
  }
  if (key == 'w') {
    s = 1;
  }
  if (key == 'e') {
    s = 2;
  }
  if (key == 'r') {
    s = 3;
  }
}
