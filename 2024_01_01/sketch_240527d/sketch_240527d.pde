/**
 * http://www.looksgood.de/libraries/Ani/Ani_Cheat_Sheet.pdf
 *
 * Ani.LINEAR
 * Ani.QUAD_IN
 * Ani.QUAD_OUT
 * Ani.QUAD_IN_OUT
 * Ani.CUBIC_IN
 * Ani.CUBIC_IN_OUT
 * Ani.CUBIC_OUT
 * Ani.QUART_IN
 * Ani.QUART_OUT
 * Ani.QUART_IN_OUT
 * Ani.QUINT_IN
 * Ani.QUINT_OUT
 * Ani.QUINT_IN_OUT
 * Ani.SINE_IN
 * Ani.SINE_OUT
 * Ani.SINE_IN_OUT
 * Ani.CIRC_IN
 * Ani.CIRC_OUT
 * Ani.CIRC_IN_OUT
 * Ani.EXPO_IN
 * Ani.EXPO_OUT
 * Ani.EXPO_IN_OUT
 * Ani.BACK_IN
 * Ani.BACK_OUT
 * Ani.BACK_IN_OUT
 * Ani.BOUNCE_IN
 * Ani.BOUNCE_OUT
 * Ani.BOUNCE_IN_OUT
 * Ani.ELASTIC_IN
 * Ani.ELASTIC_OUT
 * Ani.ELASTIC_IN_OUT
 */

import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;
import processing.sound.*;

Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, Ani.CUBIC_IN, Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, Ani.QUART_IN, Ani.QUART_OUT, Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT};
String[] easingsVariableNames = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", "Ani.CUBIC_IN", "Ani.CUBIC_IN_OUT", "Ani.CUBIC_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT"};

float x = 0;
float y = 0;
float diameterW = 10;
float diameterH = 50;
float rotateValue = 0;

Ani ani_diameterW;
Ani ani_diameterH;
Ani ani_rotate;

boolean[] max = {false, false, false};

SoundFile mySound;

void setup() {
  size(800, 800);  
  background(255);
  
  mySound = new SoundFile(this, "1.aif");
  
  x = width/2;
  y = height/2;
  
  // you have to call always Ani.init() first!
  Ani.init(this);
  
  float _secondW = random(1, 5);
  float _secondH = random(1, 5);
  float _targetW = random(width*0.5, width);
  float _targetH = random(width*0.5, width);
  ani_diameterW = new Ani(this, _secondW, "diameterW", _targetW, Ani.ELASTIC_OUT, "onStart:diaWStarted, onEnd:diaWEnd");
  ani_diameterH = new Ani(this, _secondH, "diameterH", _targetH, Ani.LINEAR, "onStart:diaHStarted, onEnd:diaHEnd");
  ani_rotate = new Ani(this, 5, "rotateValue", TWO_PI, Ani.LINEAR, "onStart:rotStarted, onEnd:rotEnd");
  
  //print(easings.length);
}

void draw() {
  noFill();
  stroke(0);
  strokeWeight(0.25);
  
  pushMatrix();
  translate(x, y);
  rotate(rotateValue);
  ellipse(0, 0, diameterW, diameterH);
  //rect(-diameterW*0.5, -diameterH*0.5, diameterW, diameterH);
  popMatrix();
}

void diaWStarted(){
  println("animation started: diameter W");
}
void diaHStarted(){
  println("animation started: diameter H");
}
void rotStarted(){
  println("animation started: rotation");
}
void diaWEnd(){
  println("animation done: diameter W");
  if(max[0] == true){
    saveFrame("myImage_####.jpg");
    mySound.play(1.0, 1.0);
  }
}
void diaHEnd(){
  println("animation done: diameter H");
  if(max[1] == true){
    saveFrame("myImage_####.jpg");
    mySound.play(1.0, 1.0);
  }
}
void rotEnd(){
  println("animation done: rotation");
  if(max[2] == true){
    saveFrame("myImage_####.jpg");
    mySound.play(1.0, 1.0);
  }
}


void mousePressed(){
  background(255);
  float _diaW_duration = random(1, 5);
  float _diaH_duration = random(1, 5);
  float _beginW = 20;
  float _beginH = 20;
  float _targetW = random(width*0.5, width);
  float _targetH = random(width*0.5, width);
  int _indexW = int(random(0, easings.length));
  int _indexH = int(random(0, easings.length));
  ani_diameterW.setDuration(_diaW_duration);
  ani_diameterH.setDuration(_diaH_duration);  
  ani_diameterW.setEnd(_targetW);
  ani_diameterH.setEnd(_targetH);
  ani_diameterW.setBegin(_beginW);
  ani_diameterH.setBegin(_beginH);  
  ani_diameterW.setEasing(easings[_indexW]);
  ani_diameterH.setEasing(easings[_indexH]);
  
  float _rotate_duration = random(1, 5);
  float _rotate_begin = 0;
  float _rotate_end = random(HALF_PI, TWO_PI);
  int _rotate_index = int(random(0, easings.length));
  ani_rotate.setDuration(_rotate_duration);
  ani_rotate.setBegin(_rotate_begin);
  ani_rotate.setEnd(_rotate_end);
  ani_rotate.setEasing(easings[_rotate_index]);
  
  ani_diameterW.start();
  ani_diameterH.start();
  ani_rotate.start();
  
  for(int i=0; i<max.length; i++){
    max[i] = false;
  }
  float worldRecord = 0;
  int index = 0;
  float[] durations = {_diaW_duration, _diaH_duration, _rotate_duration};  
  for(int i=0; i<durations.length; i++){
    if(durations[i] > worldRecord){
      index = i;
      worldRecord = durations[i];
    }
  }
  max[index] = true;
  println("___________________________");
  println(_diaW_duration + " : " + _diaH_duration + " : " + _rotate_duration);
  println("worldRecord index:" + index);
  println(easingsVariableNames[_indexW]);
  println(easingsVariableNames[_indexH]);
  println("___________________________");
}
