PImage img;

void setup() {
  size(800, 800);
  background(255);
  colorMode(HSB, 360, 100, 100);
  noStroke();
  img = loadImage("cloud1.png");
}

void draw() {
  for (int x = 0; x<360; x++) {
    float w = width/360.0;
    fill(x, 100, 100);
    rect(x*w, 0, w, height);
  }
  float imgW = img.width/3;
  float imgH = img.height/3;
  for (int y=0; y<height/imgH; y++) {
    for (int x=0; x<width/imgW; x++) {
      image(img, x*imgW, y*imgH, imgW, imgH);
    }
  }
}
