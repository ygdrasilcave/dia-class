int w = 0;
boolean reset = false;

void setup() {
  size(600, 800);
  background(255);
  //frameRate(12);
  textSize(60);
}

void draw() {
  if(reset == true){
    background(255);
    reset = false;
  }
  
  stroke(random(255), random(255), random(255));
  noFill();
  w = int(random(10, 200));
  ellipse(random(0, width), random(0, height), w, w);
  
  fill(255);
  noStroke();
  rect(0, 0, 110, 60); 
  fill(0);
  text(w, 10, 55);
  println(w);
}

void keyPressed(){
  if(key == 's'){
    saveFrame("myWork_####.jpg");
  }
  if((key == 'r' || key == 'R') && reset == false){
    reset = true;   
  }
}
