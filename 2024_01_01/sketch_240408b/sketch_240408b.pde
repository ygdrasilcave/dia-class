float t = 0;
String[] txt;
PFont font;
PFont font2;
int index = 0;
int fontIndex = 0;

void setup(){
  size(800, 800);
  background(0);
  for(int i=0; i<PFont.list().length; i++){
    println(i + " : " + PFont.list()[i]);
  }
  //218, 327, 215
  font = createFont(PFont.list()[327], 90);
  font2 = createFont(PFont.list()[218], 90);
  textFont(font);
  textSize(90);
  textAlign(CENTER, CENTER);
  txt = loadStrings("hangul2.txt");
  //println(txt.length);
}

void draw(){
  background(0);
  noStroke();
  fill(255);
  float gap = map(mouseX, 0, width, 10, 120);
  for(float y=0; y<height; y += gap){
    for(float x=0; x<width; x += gap){
      pushMatrix();
      translate(x, y);
      rotate(t);
      text(txt[index], 0, 0);
      popMatrix();
    }
  }
  //stroke(0, 255, 0);
  //line(0, height/2, width, height/2);
  //line(width/2, 0, width/2, height);
  t+=0.055;
}

void keyPressed(){
  //txt = str(key);
  if(key == ' '){
    index += 1;
    if(index > txt.length-1){
      index = 0;
    }
  }
  if(key == 'f'){
    fontIndex++;
    fontIndex = fontIndex%2;
    if(fontIndex == 0){
      textFont(font);
    }else if(fontIndex ==1){
      textFont(font2);
    }
  }
}
