float inc = 0.0;
int[] posX;
int[] segment;
float[] factor;

void setup() {
  size(800, 800);
  stroke(255, 204);
  smooth();
  posX = new int[100];
  segment = new int[100];
  factor = new float[100];
  for(int i=0; i<100; i++){
    posX[i] = int(random(width));
    segment[i] = int(random(5, 15));
    factor[i] = random(0.1, 0.6);
  }
}

void draw() {
  background(0);
  inc += 0.01;
  float angle = sin(inc)/10.0 + sin(inc*2.2)/20.0;
  //float angle = sin(inc);
  //float angle = 0;
  for(int i=0; i<100; i++){
    tail(posX[i], segment[i], angle*factor[i]);
  }
  //tail(18, 9, angle/1.3);
  //tail(33, 12, angle);
  //tail(44, 10, angle/1.3);
  //tail(62, 5, angle);
  //tail(88, 7, angle*2);
}

void tail(int x, int units, float angle) {
  pushMatrix();
  translate(x, height);
  for (int i = units; i > 0; i--) {
    strokeWeight(i);
    line(0, 0, 0, -30);
    translate(0, -30);
    rotate(angle);
  }
  popMatrix();
}
