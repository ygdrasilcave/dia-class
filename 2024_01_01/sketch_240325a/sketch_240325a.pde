void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  background(255);

  for (int y=0; y<height; y+=15) {
    for (int x=0; x<y; x+=15) {
      line(x, y, x+5, y+5);
    }
  }
}
