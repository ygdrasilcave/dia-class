float x = 100;
float speedX = 2;
float directionX = 1;
float y = 100;
float speedY = 2;
float directionY = 1;

float tx = 0;
float ty = 0;

float dia = 10;
float diaT = 0;

void setup() {
  size(800, 800);
  //rectMode(CENTER);
  speedX = random(1, 4);
  speedY = random(1, 3);
  background(0);
}

void draw() {
  //background(0);
  /*noStroke();
   fill(0, 5);
   rect(0, 0, width, height);*/

  //fill(255);
  noFill();
  stroke(255);
  strokeWeight(0.5);
  ellipse(x, y, dia, dia);
  x += speedX*directionX;
  float half_dia = dia*0.5;
  if ((x > width-half_dia) || (x < half_dia)) {
    directionX *= -1;
  }
  y += speedY*directionY;
  if ((y > height-half_dia) || (y < half_dia)) {
    directionY *= -1;
  }

  speedX = noise(tx)*6;
  speedY = noise(ty)*6;
  tx += 0.035;
  ty += 0.065;

  dia = map(cos(diaT), -1, 1, 10, 50);
  diaT += 0.25;
}
