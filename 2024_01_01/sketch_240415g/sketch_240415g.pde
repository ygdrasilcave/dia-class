float x = 0;
float y = 0;
float targetX = 0;
float targetY = 0;
float diffX = 0;
float diffY = 0;
float pct = 0.02;

int count = 0;

void setup() {
  size(800, 800);
  background(255);
  smooth(64);
  x = width/2;
  y = height/2;
  targetX = x;
  targetY = y;
}

void draw() {
  //background(255);
  float distMax = dist(0, 0, 800, 800);
  float d = dist(x, y, targetX, targetY);
  if (d > 1.0) {
    diffX = targetX - x;
    diffY = targetY - y;
    x += diffX * pct;
    y += diffY * pct;
  }
  stroke(0);
  strokeWeight(0.5);
  fill(255);
  ellipse(x, y, 30, 30);
  
  pushMatrix();
  translate(x, y);
  rotate(map(d, 0, distMax, 0, TWO_PI));
  line(0, -25, 0, 25);
  popMatrix();
  
  if(frameCount%120 == 0){
    newTarget();
    count++;
    if(count >= 100){
      saveFrame("myWork_####.jpg");
      background(255);
      count = 0;
    }
  }
}

void mouseReleased() {
  targetX = mouseX;
  targetY = mouseY;
}

void newTarget(){
  targetX = random(15, width-15);
  targetY = random(15, height-15);
}
