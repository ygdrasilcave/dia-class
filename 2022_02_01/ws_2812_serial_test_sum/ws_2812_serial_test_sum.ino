#include <Adafruit_NeoPixel.h>
#define PIN        6 // On Trinket or Gemma, suggest changing this to 1
#define NUMPIXELS 64 // Popular NeoPixel ring size
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

char messageBuffer[193];

void setup() {
  Serial.begin(115200);
  pixels.begin();
  pixels.clear();
}

int error_counter = 0;

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'

  int raw_data = Serial.read();

  if(raw_data == '*'){
    int read_count = Serial.readBytes(messageBuffer, 193);
    //Serial.println(read_count);
    long sum = 0;
    if(read_count == 193){
      //Serial.println("good data");
      //Serial.println(messageBuffer);
      for(int i=0; i<64; i++){
        int red = messageBuffer[i*3];
        int green = messageBuffer[i*3+1];
        int blue = messageBuffer[i*3+2];
        pixels.setPixelColor(i, pixels.Color(red, green, blue));
        int pixel_sum = red + green + blue;
        sum = sum + pixel_sum;
      }
      sum = sum & 0xFF;
      if(sum == int(messageBuffer[192])){
        pixels.show();
        Serial.print("GOOD: ");
        Serial.print(int(messageBuffer[192]));
        Serial.print(", ");
        Serial.println(sum);
      }else{
        error_counter++;
        Serial.print("bad data: ");
        Serial.println(error_counter);
      }
    }
  }
}
