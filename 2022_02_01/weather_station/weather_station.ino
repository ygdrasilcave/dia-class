#define uint  unsigned int
#define ulong unsigned long

#define PIN_ANEMOMETER  2     // Digital 2
#define PIN_VANE        5     // Analog 5

// How often we want to calculate wind speed or direction
#define MSECS_CALC_WIND_SPEED 5000
#define MSECS_CALC_WIND_DIR   100

volatile int numRevsAnemometer = 0; // Incremented in the interrupt
ulong nextCalcSpeed;                // When we next calc the wind speed
ulong nextCalcDir;                  // When we next calc the direction
ulong time;                         // Millis() at each start of loop().

// ADC readings:
#define NUMDIRS 16
float adc[NUMDIRS] = {
  0.35, 0.428, 0.535, 0.76, 1.045, 1.295, 1.69,
  2.115, 2.59, 3.005, 3.255, 3.635, 3.94, 4.15, 4.45, 4.8};
float directions[NUMDIRS] = {
  112.5, 67.5, 90, 157.5, 135, 202.5, 180, 22.5,
  45, 247.5, 225, 337.5, 0, 292.5, 315, 270};
//float expectedVoltages[NUMDIRS] = {
//  0.32, 0.41, 0.45, 0.62, 0.9, 1.19, 1.4,
//  1.98, 2.25, 2.93, 3.08, 3.43, 3.84, 4.04, 4.34, 4.62};
char *strVals[NUMDIRS] = {
  "ESE","ENE", "E","SSE", "SE","SSW", "S", "NNE",
  "NE","WSW","SW","NNW","N","WNW","NW", "W"};


// These directions match 1-for-1 with the values in adc, but
// will have to be adjusted as noted above. Modify 'dirOffset'
// to which direction is 'away' (it's West here).
//char *strVals[NUMDIRS] = {"W","NW","N","SW","NE","S","SE","E"};
//byte dirOffset=0;

//=======================================================
// Initialize
//=======================================================

const int ledPin = 7;
boolean status = false;

void setup() {
  Serial.begin(9600);
  pinMode(PIN_ANEMOMETER, INPUT);
  digitalWrite(PIN_ANEMOMETER, HIGH);  // Turn on the internal Pull Up Resistor
  attachInterrupt(0, countAnemometer, FALLING);
  nextCalcSpeed = millis() + MSECS_CALC_WIND_SPEED;
  nextCalcDir   = millis() + MSECS_CALC_WIND_DIR;

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
}

//=======================================================
// Main loop.
//=======================================================
void loop() {
  time = millis();

  if (time >= nextCalcSpeed) {
    calcWindSpeed();
    nextCalcSpeed = time + MSECS_CALC_WIND_SPEED;
  }

  if (time >= nextCalcDir) {
    calcWindDir();
    nextCalcDir = time + MSECS_CALC_WIND_DIR;
    status = !status;
    digitalWrite(ledPin, status);
  }
}

//=======================================================
// Interrupt handler for anemometer. Called each time the reed
// switch triggers (one revolution).
//=======================================================
void countAnemometer() {
  numRevsAnemometer++;
}

//=======================================================
// Find vane direction.
//=======================================================
void calcWindDir() {
  unsigned int val;
  float voltage;
  byte x;

  val = analogRead(PIN_VANE);
  voltage = (float) ((val/1024.0)*5.0);
  for (x=0; x < NUMDIRS; x++){
    if(adc[x] >= voltage){
    //if(expectedVoltages[x] >= voltage){
      break;
    }
  }
  Serial.print("Dir: ");
  Serial.print(directions[x]);
  Serial.print(", ");  
  Serial.println(strVals[x]);
}


//=======================================================
// Calculate the wind speed, and display it (or log it, whatever).
// 1 rev/sec = 1.492 mph
//=======================================================
void calcWindSpeed() {
  int x, iSpeed;
  // This will produce mph * 10   (2.4km/h)
  // (didn't calc right when done as one statement)
  //long speed = 14920;
  long speed = 24000;
  speed *= numRevsAnemometer;
  speed /= MSECS_CALC_WIND_SPEED;

  iSpeed = speed;         // Need this for formatting below

  Serial.print("Wind speed: ");
  x = iSpeed / 10;
  Serial.print(x);
  Serial.print('.');
  x = iSpeed % 10;
  Serial.println(x);

  numRevsAnemometer = 0;        // Reset counter
}


/*
def onReceive(dat, rowIndex, message, bytes):

	#print(message)
	
	data = message.split('    ')
	print(data)
	
	wind_speed = data[0].split(':')
	print(float(wind_speed[1].strip()))
	
	wind_dir = data[1].split(':')
	#print(wind_dir[1])
	
	wind_dir_degrees = wind_dir[1].split(',')[0]
	wind_dir_string = wind_dir[1].split(',')[1]
	print(wind_dir_degrees.strip())
	print(wind_dir_string.strip())


	return

*/
