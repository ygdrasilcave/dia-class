#include <Adafruit_NeoPixel.h>
#define PIN        6 // On Trinket or Gemma, suggest changing this to 1
#define NUMPIXELS 64 // Popular NeoPixel ring size
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

char messageBuffer[192];

void setup() {
  Serial.begin(115200);
  pixels.begin();
  pixels.clear();
}

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'

  int raw_data = Serial.read();

  if(raw_data == '*'){
    int read_count = Serial.readBytes(messageBuffer, 192);
    //Serial.println(read_count);
    if(read_count == 192){
      Serial.println("good data");
      //Serial.println(messageBuffer);
      for(int i=0; i<64; i++){
        int red = messageBuffer[i*3];
        int green = messageBuffer[i*3+1];
        int blue = messageBuffer[i*3+2];
        pixels.setPixelColor(i, pixels.Color(red, green, blue));
      }
      pixels.show();
    }
  }
}
