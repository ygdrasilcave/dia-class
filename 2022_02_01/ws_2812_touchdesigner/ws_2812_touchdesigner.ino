#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 6  // On Trinket or Gemma, suggest changing this to 1
// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 64  // Popular NeoPixel ring size

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

char messageBuffer[192]; //64*3
bool messageReceived;

void setup() {
  // put your setup code here, to run once:
  pixels.begin();
  pixels.show();
  
  Serial.begin(115200);
  messageReceived = false;
}

void loop() {
  pixels.clear();
  // put your main code here, to run repeatedly:
  pixels.setBrightness(10);

  int startChar = Serial.read();

  if (startChar == '*') {
    int count = Serial.readBytes(messageBuffer, 192);
    if (count == 192) {
      //Serial.print("message received: ");
      //Serial.println(messageBuffer);
      for (int i = 0; i < 64; i++) {
        int r = messageBuffer[i * 3];
        int g = messageBuffer[i * 3 + 1];
        int b = messageBuffer[i * 3 + 2];
        pixels.setPixelColor(i, pixels.Color(r, g, b));
      }
      pixels.show();
      Serial.println('!');
    }
    //Serial.print("bytes received: ");
    //Serial.println(count);
  }
}
