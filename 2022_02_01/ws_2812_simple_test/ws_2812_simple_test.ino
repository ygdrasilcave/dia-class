// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// Released under the GPLv3 license to match the rest of the
// Adafruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        6 // On Trinket or Gemma, suggest changing this to 1

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 64 // Popular NeoPixel ring size

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 100

int x = 0;
int y = 0;

void setup() {
  pixels.begin();
  pixels.clear();
}

void loop() {
  pixels.clear(); // Set all pixel colors to 'off'
  int i = x + y*8;
  pixels.setPixelColor(i, pixels.Color(150, 0, 0));
  pixels.show();   // Send the updated pixel colors to the hardware.
  delay(DELAYVAL);
  y++;
  if(y > 7){
    y = 0;
    x++;
    if(x > 7){
      x = 0;
      y = 0;
    }
  }
}
