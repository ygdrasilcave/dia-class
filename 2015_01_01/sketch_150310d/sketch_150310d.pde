boolean[] bits = new boolean[8];
String val = "00000000 : 0";

void setup() {
  size(800, 200);
  background(80);
  bits[0] = false;
  bits[1] = false;
  bits[2] = false;
  bits[3] = false;
  bits[4] = false;
  bits[5] = false;
  bits[6] = false;
  bits[7] = false;
  textSize(30);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(80);
  stroke(80);
  fill(int(bits[0])*255);
  rect(0, 0, 100, 100);
  fill(int(bits[1])*255);
  rect(100, 0, 100, 100);
  fill(int(bits[2])*255);
  rect(200, 0, 100, 100);
  fill(int(bits[3])*255);
  rect(300, 0, 100, 100);
  fill(int(bits[4])*255);
  rect(400, 0, 100, 100);
  fill(int(bits[5])*255);
  rect(500, 0, 100, 100);
  fill(int(bits[6])*255);
  rect(600, 0, 100, 100);
  fill(int(bits[7])*255);
  rect(700, 0, 100, 100);
  
  fill(255);
  noStroke();
  text(val, width/2, 150);
}

void mouseReleased() {
  if (mouseX >= 0 && mouseX < 100 && mouseY >= 0 && mouseY < 100) {
    bits[0] = !bits[0];
  }
  if (mouseX >= 100 && mouseX < 200 && mouseY >= 0 && mouseY < 100) {
    bits[1] = !bits[1];
  }
  if (mouseX >= 200 && mouseX < 300 && mouseY >= 0 && mouseY < 100) {
    bits[2] = !bits[2];
  }
  if (mouseX >= 300 && mouseX < 400 && mouseY >= 0 && mouseY < 100) {
    bits[3] = !bits[3];
  }
  if (mouseX >= 400 && mouseX < 500 && mouseY >= 0 && mouseY < 100) {
    bits[4] = !bits[4];
  }
  if (mouseX >= 500 && mouseX < 600 && mouseY >= 0 && mouseY < 100) {
    bits[5] = !bits[5];
  }
  if (mouseX >= 600 && mouseX < 700 && mouseY >= 0 && mouseY < 100) {
    bits[6] = !bits[6];
  }
  if (mouseX >= 700 && mouseX < 800 && mouseY >= 0 && mouseY < 100) {
    bits[7] = !bits[7];
  }
  //println(bits);
  String value = str(int(bits[0]))+str(int(bits[1]))+str(int(bits[2]))
    +str(int(bits[3]))+str(int(bits[4]))+str(int(bits[5]))+str(int(bits[6]))+str(int(bits[7]));
  val = value + " : " + unbinary(value);
}

