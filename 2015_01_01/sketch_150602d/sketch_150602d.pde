float j=0.0;

void setup() {
  size(1000, 1000, P3D);
  colorMode(HSB, 360, 100, 100);
}


void draw() {
  background(0);
  translate(width/2, height/2);
  rotateX(radians(map(mouseX, 0, width, 0, 360)));
  //noStroke();
  for (int i=0; i<3500; i++) {
    rotateY(PI*map(mouseY, 0, height, 0, 8.3));
    for (int k=0; k<5; k++) {
      stroke(240, 0, map(i, 0, 3500, 100, 0));
      strokeWeight(3);
      pushMatrix();
      translate(0, 0, k*50);
      rotateZ(j);
      point(0.1*i*cos(radians(j*i)), 0.1*i*sin(radians(j*i)));
      popMatrix();
    }
  }
  j+=0.005;
}

