boolean b0 = false;
boolean b1 = false;
boolean b2 = false;
boolean b3 = false;
boolean b4 = false;
boolean b5 = false;
boolean b6 = false;
boolean b7 = false;
String value = "00000000 : 0";

void setup(){
  size(800, 200);
  background(80);
  textAlign(CENTER, CENTER);
  textSize(36);
}

void draw(){
  background(80);
  stroke(80);
  fill(int(b0)*255);
  rect(0,0,100,100);
  fill(int(b1)*255);
  rect(100,0,100,100);
  fill(int(b2)*255);
  rect(200,0,100,100);
  fill(int(b3)*255);
  rect(300,0,100,100);
  fill(int(b4)*255);
  rect(400,0,100,100);
  fill(int(b5)*255);
  rect(500,0,100,100);
  fill(int(b6)*255);
  rect(600,0,100,100);
  fill(int(b7)*255);
  rect(700,0,100,100);
  fill(255);
  text(value, width/2, 150);
}

void mouseReleased(){
  if(mouseX >= 0 && mouseX < 100 && mouseY >= 0 && mouseY < 100){
    b0 = !b0;
  }
  if(mouseX >= 100 && mouseX < 200 && mouseY >= 0 && mouseY < 100){
    b1 = !b1;
  }
  if(mouseX >= 200 && mouseX < 300 && mouseY >= 0 && mouseY < 100){
    b2 = !b2;
  }
  if(mouseX >= 300 && mouseX < 400 && mouseY >= 0 && mouseY < 100){
    b3 = !b3;
  }
  if(mouseX >= 400 && mouseX < 500 && mouseY >= 0 && mouseY < 100){
    b4 = !b4;
  }
  if(mouseX >= 500 && mouseX < 600 && mouseY >= 0 && mouseY < 100){
    b5 = !b5;
  }
  if(mouseX >= 600 && mouseX < 700 && mouseY >= 0 && mouseY < 100){
    b6 = !b6;
  }
  if(mouseX >= 700 && mouseX < 800 && mouseY >= 0 && mouseY < 100){
    b7 = !b7;
  }
  
  String val = str(int(b0)) + str(int(b1)) + str(int(b2)) + str(int(b3)) + 
  str(int(b4)) + str(int(b5)) + str(int(b6)) + str(int(b7));
  value = val + " : " + unbinary(val);
  println(val + " : " + unbinary(val));
}
