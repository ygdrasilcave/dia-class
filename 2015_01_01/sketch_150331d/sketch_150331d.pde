void setup() {
  size(600, 600);
  background(0);
}

int num = 6;
float w = 200;
float t = 0.0;

float tension = 100;
float diff = 50;

void draw() {
  background(0);
  
  strokeWeight(3);

  translate(width/2, height/2);
  float px=0, py=0;
  float startX=0, startY=0;

  stroke(255);
  fill(255);
  for (int i=0; i<num; i++) {
    //w = abs(sin(t))*120+60;
    float x = cos(i*(TWO_PI/num))*w;
    float y = sin(i*(TWO_PI/num))*w;
    if (i == 0) {
      startX = x;
      startY = y;
    }
    noFill();
    if (i>0) {
      bezier(px, py, cos((i-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), sin((i-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), x, y, cos((i)*(TWO_PI/num)+atan(tension/w))*(w+diff), sin((i)*(TWO_PI/num)+atan(tension/w))*(w+diff));
      fill(255, 0, 0);
      ellipse(cos((i-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), sin((i-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), 10, 10);
      fill(0, 255, 0);
      ellipse(cos((i)*(TWO_PI/num)+atan(tension/w))*(w+diff), sin((i)*(TWO_PI/num)+atan(tension/w))*(w+diff), 10, 10);
    }
    fill(255);
    //ellipse(x, y, 10, 10);
    //line(0, 0, x, y);

    px = x;
    py = y;
    t+=0.002;
  }
  noFill();
  bezier(px, py, cos((num-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), sin((num-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), startX, startY, cos(atan(tension/w))*(w+diff), sin(atan(tension/w))*(w+diff));
  fill(255, 0, 0);
  ellipse(cos((num-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), sin((num-1)*(TWO_PI/num)-atan(tension/w))*(w+diff), 10, 10);
  fill(0, 255, 0);
  ellipse(cos(atan(tension/w))*(w+diff), sin(atan(tension/w))*(w+diff), 10, 10);
  
  tension = map(mouseX, 0, width, -200, 200);
  diff = map(mouseY, 0, height, -200, 200);
}

