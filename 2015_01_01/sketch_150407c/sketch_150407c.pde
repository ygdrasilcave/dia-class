void setup(){
  size(600, 600);
}

void draw(){
  background(0);
  translate(width/2, height/2);
  for(int i=0; i<360; i+=15){
    float x = cos(radians(i))*map(mouseX, 0, width, 10, 200);
    float y = sin(radians(i))*map(mouseX, 0, width, 10, 200);
    ellipse(x, y, 30, 30);
  }
}
