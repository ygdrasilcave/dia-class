float t = 0.0;

void setup() {
  size(1000, 1000);
  background(0);
}

void draw() {
  background(0);
  translate(width/2, height/2);
  rotate(t);
  stroke(255);
  strokeWeight(3);
  for (int i=0; i<3500; i++) {
    point(0.1*i*cos(radians(137.5*i)), 0.1*i*sin(radians(137.5*i)));
  }
  t+=0.01;
}

