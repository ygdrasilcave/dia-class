float t = 0.0;
float px = 0.0;
float py = 0.0;
float dirX = 1;
float dirY = 1;
boolean b = true;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  if (b == true) {
    px+=dirX*2;
    py+=dirY*3;
  }
  noStroke();
  fill(255);
  background(0);
  pushMatrix();
  translate(width/2, height/2);
  rotate(atan2(py-height/2, px-width/2));
  rect(0, -5, 100, 10);
  ellipse(0, 0, 30, 30);
  popMatrix();
  t+=0.06;
  if (px > width || px < 0) {
    dirX*=-1;
  }
  if (py > height || py < 0) {
    dirY*=-1;
  }
  fill(255, 0, 0);
  ellipse(px, py, 30, 30);
}

void keyPressed() {
  b = !b;
}

