float t = 0.0;

void setup() {
  size(600, 600, P3D);
  background(0);
}

void draw() {
  background(0);
  pushMatrix();
  translate(width/2, height/2, 0);
  rotateX(t);
  for (int i=0; i<50; i++) {
    pushMatrix();
    rotateY(map(i, 0, 49, 0, TWO_PI));
    stroke(255);
    strokeWeight(map(mouseY, 0, height, 1, 10));
    line(100, -100, 100, 100);
    popMatrix();
  }
  popMatrix();
  t+=0.05;
}

