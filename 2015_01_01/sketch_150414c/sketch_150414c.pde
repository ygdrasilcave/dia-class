float eyeH = 50;
float eyeW = 50;
boolean b = true;

float px = 0;
float py = 0;
float speedX, speedY;
float t = 0.0;

void setup() {
  size(800, 800);
  background(0);
  speedX = random(3, 5);
  speedY = random(4, 6);
}

void draw() {
  background(0);
  for (int y=0; y<height/eyeH+1; y++) {
    for (int x=0; x<width/eyeW+1; x++) {
      pushMatrix();
      translate(x*eyeW, y*eyeH);      
      float t = atan2(px-y*eyeH, py-x*eyeW);
      float px1 = cos(t)*((eyeW/2)-eyeW*50/100/2);
      float py1 = sin(t)*((eyeH/2)-eyeH*50/100/2);
      float px2 = cos(t)*((eyeW/2)-eyeW*20/100/2);
      float py2 = sin(t)*((eyeH/2)-eyeH*20/100/2);
      fill(255);
      noStroke();
      ellipse(px1, py1, eyeW*50/100, eyeW*50/100);
      fill(0);
      if (b == true) {
        ellipse(px2, py2, eyeW*20/100, eyeW*20/100);
      } else {
        ellipse(px1, py1, eyeW*20/100, eyeW*20/100);
      }
      stroke(255);
      strokeWeight(3);
      noFill();
      ellipse(0, 0, eyeW, eyeH);
      popMatrix();
    }
  }
  
  px = width/2 + cos(t)*(width/2-100);
  py = height/2 + sin(t)*(height/2-100);
  t+=0.02;
  
  /*if(px > width){
    px = 0;
  }
  if(px < 0){
    px = width;
  }
  if(py > height){
    py = 0;
  }
  if(py < 0){
    py = height;
  }*/
  
  if(frameCount%30 == 0){
    b = !b;
  }
}
void keyPressed() {
  b = !b;
}

