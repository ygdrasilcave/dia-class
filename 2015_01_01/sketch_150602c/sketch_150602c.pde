int j = 0;
float n = 0.0;

void setup() {
  size(720, 480);
  background(0);
}

void draw() {
  background(0);

  translate(width/2, height/2);
  for (int i=0; i<3800; i++) {
    stroke(255-i/18);
    point( 0.1*i*cos(radians(n*i)), 0.1*i*sin(radians(n*i)) );
    fill(0);
    noStroke();
    ellipse(0, 0, 20, 20);
  }
  n+=0.1;
  j+=2;
}

