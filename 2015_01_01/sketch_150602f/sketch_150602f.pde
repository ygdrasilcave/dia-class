void setup(){
  size(500, 250);
}

void draw(){
  noStroke();
  colorMode(RGB, 255, 255, 255);
  fill(random(255), random(255), random(255));
  rect(0,0,width/2, height);
  colorMode(HSB, 360, 100, 100);
  fill(random(360), 100, 100);
  rect(width/2, 0, width/2, height);
}
