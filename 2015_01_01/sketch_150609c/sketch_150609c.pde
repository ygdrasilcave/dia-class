float t = 0.0;
float t1 = 0.0;

PShape bot;

void setup() {
  size(600, 600, P3D);
  bot = loadShape("bot1.svg");
  smooth(8);
}

void draw() {
  background(0);
  pushMatrix();
  translate(width/2, height/2, 0);
  rotateY(map(mouseY, 0, height, 0, PI));
  for (int z=-50; z<50; z++) {
    pushMatrix();
    //translate(0, sin(t)*50, z*map(mouseX, 0, width, 0, 50));
    translate(sin(t)*50, 0, z*map(mouseX, 0, width, 0, 50));    
    //rect(-30, -30, 60, 60);
    float s = noise(t1)*60+10;
    //ellipse(0, 0, s, s);
    bot.disableStyle();
    noFill();
    stroke(255);
    strokeWeight(1);

    shape(bot, 0, 0, 80, 80);
    popMatrix();
    t+=0.065;
    t1+=0.045;
  }
  popMatrix();

  /*translate(width/2, height/2, map(mouseX, 0, width, 0, 300));
   rotateY(map(mouseY, 0, height, 0, TWO_PI));
   box(30, 30, 30);*/
}

