float t = 0.0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);
  fill(255, 20);
  stroke(0);
  pushMatrix();
  translate(width/2, height/2);
  for (int j=0; j<360; j+=15) {
    float cx = cos(radians(j)) * 100;
    float cy = sin(radians(j)) * 100;
    for (int i=0; i<100; i+=2) {
      float a = cos(t)*i;
      float x = cos(radians(j+a))*(i*3);
      float y = sin(radians(j+a))*(i*3);
      ellipse(cx+x, cy+y, 100-i, 100-i);
    }
    t+=0.004;
  }
  popMatrix();
}

