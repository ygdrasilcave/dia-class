float px = 0.0;
float py = 0.0;
float speedX;
float speedY;
float dirX = 1;
float dirY = 1;

float t = 0.0;

boolean b = true;
float t2 = 0.0;

void setup() {
  size(800, 800);
  background(0);
  speedX = random(5, 9);
  speedY = random(4, 8);
}

float tx=0.0;
float ty=0.0;

void draw() {
  background(0);
  noStroke();
  ty = 0.0;
  for (int y=0; y<height+50; y+=50) {
    tx = 0.0;
    for (int x=0; x<width+50; x+=50) {
      float d = dist(px, py, x, y);
      if (d < width/2) {
        d = map(d, 0, width/2, 0, 255);
      } else {
        d = 255;
      }
      float noiseAngle = noise(tx, ty);
      fill(d);
      pushMatrix();
      translate(x, y);
      rotate(atan2(py-y, px-x));
      //rotate(noiseAngle*TWO_PI + atan2(py-y, px-x));
      //rect(0, -3, 50, 6); 
      stroke(255);
      strokeWeight(5);
      if (b == true) {
        line(0, 0, 50, sin(t2)*50);
      } else {
        line(0, 0, 50, 0);
      }
      ellipse(0, 0, 20, 20);
      popMatrix();
      tx+=0.08;
    }
    ty+=0.095;
  }
  px+=dirX*speedX;
  py+=dirY*speedY;
  
  t2+=0.065;

  if (px > width || px < 0) {
    dirX*=-1;
  }
  if (py > height || py < 0) {
    dirY*=-1;
  }

  if (frameCount%30 == 0) {
    //b = !b;
  }
}

