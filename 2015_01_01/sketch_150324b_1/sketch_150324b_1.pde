float x;
float y;
float t;
float ts;
int n = 5;

float r;

void setup() {
  size(600, 600);
  background(0);
}

void draw() {
  background(0);
  ts = TWO_PI/n;
  t = 0;
  for (int i=0; i<n; i++) {
    if(i%2 == 0){
      r = 250;
    }else{
      r = map(mouseY, 0, height, 20, 200);
    }
    x = width/2 + cos(t)*r;
    y = height/2 + sin(t)*r;
    noStroke();
    fill(255);
    ellipse(x, y, 10, 10);
    stroke(255);
    strokeWeight(3);
    if(i%2 == 1){
      r = 250;
    }else{
      r = map(mouseY, 0, height, 20, 200);
    }
    line(x, y, width/2 + cos(t+ts)*r, height/2 + sin(t+ts)*r);
    t = t + ts;
  }  
  n = int(map(mouseX, 0, width, 3, 60));
  //r = map(mouseY, 0, height, 20, 200);
}

