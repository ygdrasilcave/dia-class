float x;
float y;
float r;
float t;
float rt;

void setup(){
  size(600, 600);
  background(0);
  r = 200;
}

void draw(){
  //background(0);
  
  x = width/2 + cos(t)*r;
  y = height/2 + sin(t)*r;
  
  ellipse(x, y, 5, 5);
  t = t+0.008;
  
  r = 100+(sin(rt))*160;
  rt = rt+0.06;
}
