int cellNum = 80;
int cellSize;
int[] bitColor;
int[] newSet;

void setup() {
  size(800, 800);
  background(255);
  cellSize = width/cellNum;
  bitColor = new int[(cellNum/2)*cellNum];
  initColor();
  newSet = new int[8];
  initSet();
}

void initSet() {
  for (int i=0; i<newSet.length; i++) {
    float col = random(1);
    if (col > 0.5) {
      newSet[i] = 1;
    } else {
      newSet[i] = 0;
    }
  }
}

void initColor() {
  for (int i=0; i<bitColor.length; i++) {
    float col = random(1);
    if (col > 0.5) {
      bitColor[i] = 1;
    } else {
      bitColor[i] = 0;
    }
  }
}

void draw() {
  background(255);
  noStroke();

  for (int y=0; y<cellNum; y++) {
    int xCount = cellNum/2-1;
    for (int x = 0; x<cellNum/2; x++) {
      fill(bitColor[x + y*cellNum/2]*255);
      rect(x*cellSize, y*cellSize, cellSize, cellSize);
      fill(bitColor[xCount + y*cellNum/2]*255);
      rect(x*cellSize+width/2, y*cellSize, cellSize, cellSize);
      xCount--;
    }
  }
}

void nextSet() {
  for (int i=1; i<bitColor.length-1; i++) {
    if (bitColor[i-1] == 1 && bitColor[i] == 1 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[0];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 1 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[1];
    } else if (bitColor[i-1] == 1 && bitColor[i] == 1 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[2];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 1 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[3];
    } else if (bitColor[i-1] == 1 && bitColor[i] == 0 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[4];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 0 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[5];
    } else if (bitColor[i-1] == 1 && bitColor[i] == 0 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[6];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 0 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[7];
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    initColor();
  }
  if (key == 'r') {
    initSet();
    println(newSet);
  }
  if (key == 'n') {
    nextSet();
  }
}

