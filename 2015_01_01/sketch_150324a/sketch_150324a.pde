int d = 0;
float r = 0;

void setup(){
  size(600,600);
  background(0);  
}

void draw(){
  //background(0);
  //fill(0, 10);
  //noStroke();
  //rect(0,0,width,height);
  
  fill(255);
  noStroke();
  ellipse(width/2, height/2, 20, 20);
  float x = width/2+cos(radians(d))*r;
  float y = height/2+sin(radians(d))*r;
  fill(255, 255, 0);
  ellipse(x, y, 10, 10);
  d += 1;
  r += 0.2;
}
