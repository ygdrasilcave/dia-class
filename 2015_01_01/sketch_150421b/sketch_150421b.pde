int cellNum = 200;
int cellSize;
int[] bitColor;
int[] newSet;

int count = 0;

void setup() {
  size(1000, 1000);
  background(255);
  cellSize = width/cellNum;
  bitColor = new int[cellNum];
  initColor();
  newSet = new int[8];
  initSet();
}

void initSet() {
  for (int i=0; i<newSet.length; i++) {
    float col = random(1);
    if (col > 0.5) {
      newSet[i] = 1;
    } else {
      newSet[i] = 0;
    }
  }
}

void initColor() {
  for (int i=0; i<bitColor.length; i++) {
    /*float col = random(1);
    if (col > 0.5) {
      bitColor[i] = 1;
    } else {
      bitColor[i] = 0;
    }*/
    if(i == int(bitColor.length/2)){
      bitColor[i] = 0;
    }else{
      bitColor[i] = 1;
    }
  }
}

void draw() {
  //background(255);
  noStroke();
  for (int x = 0; x<cellNum; x++) {
    fill(bitColor[x]*255);
    rect(x*cellSize, count*cellSize, cellSize, cellSize);
  }
  if(count > cellNum-1){
    background(255);
    count = 0;
  }
}

void nextSet() {
  for (int i=1; i<bitColor.length-1; i++) {
    if (bitColor[i-1] == 1 && bitColor[i] == 1 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[0];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 1 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[1];
    } else if (bitColor[i-1] == 1 && bitColor[i] == 1 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[2];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 1 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[3];
    } else if (bitColor[i-1] == 1 && bitColor[i] == 0 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[4];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 0 && bitColor[i+1] == 1) {
      bitColor[i] = newSet[5];
    } else if (bitColor[i-1] == 1 && bitColor[i] == 0 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[6];
    } else if (bitColor[i-1] == 0 && bitColor[i] == 0 && bitColor[i+1] == 0) {
      bitColor[i] = newSet[7];
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    initColor();
  }
  if (key == 'r') {
    initSet();
    println(newSet);
  }
  if (key == 'n') {
    nextSet();
    count++;
  }
}

