float t = 0.0;
float t2 = 0.0;
void setup() {
  size(600, 600);
}

float mx;
float my;
float speedX = 15;
float speedY = 9;
float dirX = 1;
float dirY = 1;

void draw() {
  background(0);
  fill(255, 40);
  stroke(0);

  for (int j=1; j<width+30; j+=30) {
    pushMatrix();
    translate(j, height);
    for (int i=0; i<100; i+=5) {
      float a = sin(map(mx, 0, width, -1, 1))*i*map(my, 0, height, 0, 1);
      float x = cos(radians(-90)+radians(a))*i*3;
      float y = sin(radians(-90)+radians(a))*i*3;
      ellipse(x, y, 100-i, 100-i);
    }
    popMatrix();
  }
  t+=0.06;
  t2+=0.12;

  mx+=dirX*speedX;
  my+=dirY*speedY;
  if (mx>width) {
    mx = width;
    dirX*=-1;
    speedX = random(15, 30);
  }
  if (mx<0) {
    mx = 0;
    dirX*=-1;
    speedX = random(15, 30);
  }
  if (my>height) {
    my = height;
    dirY*=-1;
    speedY = random(15, 30);
  }
  if (my<0) {
    my = 0;
    dirY*=-1;
    speedY = random(15, 30);
  }
  fill(255, 0, 0);
  ellipse(mx, my, 10, 10);
}

