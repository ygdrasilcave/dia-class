float x;
float y;
float t;
float p;
float r;

float k;

void setup() {
  size(600, 600);
  background(0);
}

void draw() {  
  background(0);
  p = 0.0;
  beginShape();
  fill(255, 255, 0);
  for (t=0.0; t<TWO_PI; t+=0.01) {
    x = width/2 + cos(t)*r;
    y = height/2 + sin(t)*r;
    
    //noStroke();
    //fill(255);
    //ellipse(x, y, 5, 5);
    vertex(x, y);
    //t += 0.01;
    r = 30+abs(sin(p))*k;
    p += 0.08;
  }
  endShape(CLOSE);
  
  k+=1;
  if(k > 200){
    k = 0;
  }
}

