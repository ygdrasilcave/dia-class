float t = 0.0;
PFont font;

void setup(){
  size(600, 600);
  background(0);
  textAlign(CENTER, CENTER);
  font = loadFont("Aparajita-BoldItalic-48.vlw");
  textFont(font);
}

void draw(){
  background(0);
  fill(255);
  noStroke();
  textSize(60);
  translate(width/2, height/2);
  rotate(t);
  text("my name is ***", 0, 0);
  t+= 0.006;
}
