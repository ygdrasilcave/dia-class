float x;
float y;
float t = 0.0;
float ts;

int n = 3;
int ns = 1;

float radMin = 20;
float radMinSpeed = 10;

void setup() {
  size(800, 800);
  background(0);
  ts = TWO_PI/80;
  frameRate(6);
}

void draw() {
  background(0);
  //ts = TWO_PI/int(map(mouseX, 0, width, 3, 160));
  ts = TWO_PI/n;
  t = 0.0;
  for (int i=0; i<n; i++) {
    float rad;
    if (i%2 == 0) {
      rad = 300;
    } else {
      //rad = map(mouseX, 0, width, 20, 350);
      rad = radMin;
    }
    x = width/2+cos(t)*rad;
    y = height/2+sin(t)*rad;  
    if (i%2 == 0) {
      //rad = map(mouseX, 0, width, 20, 350);
      rad = radMin;
    } else {
      rad = 300;
    }  
    noStroke();
    fill(255);
    ellipse(x, y, 20, 20);
    stroke(255);
    strokeWeight(3);
    line(width/2, height/2, x, y);
    line(x, y, width/2+cos(t+ts)*rad, height/2+sin(t+ts)*rad);
    t+=ts;
  }
  n = n+ns;
  if (n > 30) {
    ns *= -1;
  }
  if (n < 3) {
    ns *= -1;
  }
  
  radMin += radMinSpeed;
  if(radMin > 350){
    radMinSpeed *= -1;
  }
  if(radMin < 20){
    radMinSpeed *= -1;
  }
  
  println(n);
}

