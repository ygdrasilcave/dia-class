float j=0.0;

void setup() {
  size(1000, 1000);
  frameRate(5);
}


void draw() {
  background(0);
  translate(width/2, height/2);
  for (int i=0; i<3500; i++) {
    stroke(255);
    strokeWeight(3);
    point(0.1*i*cos(radians(j*i)), 0.1*i*sin(radians(j*i)));
  }

  j+=0.17;
}

