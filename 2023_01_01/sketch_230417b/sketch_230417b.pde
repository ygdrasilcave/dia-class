PImage img;

void setup(){
  size(800, 800);
  img = loadImage("man.png");
  background(255);
}

void draw(){
  //image(img, 0, 0);
  int x = int(random(width));
  int y = int(random(height));
  int index = x + (img.width * y);
  color c = img.pixels[index];
  
  noStroke();
  fill(c);
  ellipse(x, y, 10, 10);
  
  //strokeWeight(5);
  //stroke(c);
  //line(x, y, x+20, y-20);
}
