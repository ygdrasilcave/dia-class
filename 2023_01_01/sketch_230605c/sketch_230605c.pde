/**
 * http://www.looksgood.de/libraries/Ani/Ani_Cheat_Sheet.pdf
 *
 * Ani.LINEAR
 * Ani.QUAD_IN
 * Ani.QUAD_OUT
 * Ani.QUAD_IN_OUT
 * Ani.CUBIC_IN
 * Ani.CUBIC_IN_OUT
 * Ani.CUBIC_OUT
 * Ani.QUART_IN
 * Ani.QUART_OUT
 * Ani.QUART_IN_OUT
 * Ani.QUINT_IN
 * Ani.QUINT_OUT
 * Ani.QUINT_IN_OUT
 * Ani.SINE_IN
 * Ani.SINE_OUT
 * Ani.SINE_IN_OUT
 * Ani.CIRC_IN
 * Ani.CIRC_OUT
 * Ani.CIRC_IN_OUT
 * Ani.EXPO_IN
 * Ani.EXPO_OUT
 * Ani.EXPO_IN_OUT
 * Ani.BACK_IN
 * Ani.BACK_OUT
 * Ani.BACK_IN_OUT
 * Ani.BOUNCE_IN
 * Ani.BOUNCE_OUT
 * Ani.BOUNCE_IN_OUT
 * Ani.ELASTIC_IN
 * Ani.ELASTIC_OUT
 * Ani.ELASTIC_IN_OUT
 */

import de.looksgood.ani.*;

float x = 0;
float y = 0;
float diameterW = 50;
float diameterH = 50;

void setup() {
  size(800, 800);
  // you have to call always Ani.init() first!
  Ani.init(this);
  background(255);
  x = width/2;
  y = height/2;
}

void draw() {
  noFill();
  stroke(0);
  strokeWeight(0.25);
  ellipse(x, y, diameterW, diameterH);
}

void mouseReleased() {
  //Ani.to(this, 2.5, "x", width, Ani.LINEAR);
  //Ani.to(this, 1.0, "y", mouseY, Ani.ELASTIC_OUT);
  Ani.to(this, 3.0, "diameterW", width*0.75, Ani.ELASTIC_OUT);
  Ani.to(this, 3.0, "diameterH", width, Ani.LINEAR);
}
