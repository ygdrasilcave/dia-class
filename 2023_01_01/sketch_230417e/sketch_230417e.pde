PImage img;
int brushSize = 5;

void setup() {
  size(800, 800);
  img = loadImage("man.png");
  background(255);
}

void draw() {
  //image(img, 0, 0);
  background(255);
  int pixelSize = 15;
  int threshold = int(map(mouseX, 0, width, 5, 250));
  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width; x+=pixelSize) {
      int index = x + (img.width * y);
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      //fill(255-br);
      //noStroke();
      //rect(x, y, pixelSize, pixelSize);
      
      float rotVal = map(br, 0, 255, 45, 0);
      pushMatrix();
      translate(x, y);
      rotate(radians(rotVal));
      strokeWeight(6);
      stroke(r, g, b);
      line(0, 0, 0, -pixelSize);
      popMatrix();
    }
  }
}
