float x = 200;
float y = 0;

float theta = 0;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  //background(255);
  
  noFill();
  stroke(0);
  strokeWeight(0.25);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(theta);
  //line(0, 0, x, y);
  rect(x-40, y-20, 80, 40);  
  popMatrix();
  
  theta += 0.05;
}
