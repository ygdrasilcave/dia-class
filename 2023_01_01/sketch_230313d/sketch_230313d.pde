void setup() {
  size(1000, 800);
  background(255);
  textAlign(RIGHT, BOTTOM);
  textSize(36);
}

void draw() {
  //background(255);

  noFill();
  stroke(0);
  float x1 = random(width);
  float x2 = random(width);
  float y = random(height/2);
  line(x1, y, x1, height/2);
  line(x2, height/2, x2, height - y);
  
  noStroke();
  fill(0);
  String position = mouseX + ":" + mouseY;
  //text(position, mouseX, mouseY);
}
