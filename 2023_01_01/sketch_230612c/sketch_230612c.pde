import processing.sound.*;

AudioIn mic;
Amplitude loudness;

void setup(){
  size(800, 800);
  background(255);  
  mic = new AudioIn(this, 0);
  mic.start();
  loudness = new Amplitude(this);
  loudness.input(mic);
}

int sample_00, sample_01, sample_02, sample_03, sample_04;

void draw(){
  background(255);
  float volume = loudness.analyze();
  int val = int(map(volume, 0, 0.5, 1, 750));
  sample_00 = sample_01;
  sample_01 = sample_02;
  sample_02 = sample_03;
  sample_03 = sample_04;
  sample_04 = val;
  int val_filter = (sample_00 + sample_01 + sample_02 + sample_03 +sample_04) / 5;  
  //println(val_filter);
  
  /*fill(0);
  ellipse(val_filter, 300, 100, 100);
  ellipse(val, 600, 100, 100);*/
  
  noFill();
  stroke(0);
  strokeWeight(0.25);
  ellipse(width/2, height/2, val_filter, val_filter);
}
