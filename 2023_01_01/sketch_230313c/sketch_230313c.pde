void setup() {
  size(1000, 800);
  background(255);
  textAlign(RIGHT, BOTTOM);
  textSize(36);
}

void draw() {
  //background(255);

  noFill();
  stroke(0);
  float x = random(width);
  float y = random(0, height/2-10);
  line(x, y, x, height-y);
  
  noStroke();
  fill(0);
  String position = mouseX + ":" + mouseY;
  //text(position, mouseX, mouseY);
}
