void setup() {
  size(1000, 800);
  background(5, 255, 149);
  //noLoop();
  textAlign(RIGHT, BOTTOM);
  textSize(36);
}

void draw() {
  background(5, 255, 149);
  
  fill(255, 0, 255);
  noStroke();
  ellipse(300, 600, 200, 200);
  
  noFill();
  stroke(0,0,255);
  strokeWeight(5);
  rect(300-(mouseX/2), 600-50, mouseX, 100);
  
  fill(0, 255, 255, 120);
  noStroke();
  rect(300-100, 600-(mouseY/2), 200, mouseY);
  
  //println(numAdd(300, 4));
  
  //exit();
  
  noStroke();
  fill(0);
  String position = mouseX + ":" + mouseY;
  text(position, mouseX, mouseY);
}

int numAdd(int a, int b){
  return a+b;
}
