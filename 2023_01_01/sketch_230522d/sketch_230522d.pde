float r = 250;
float t = 0;
int n = 6;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  //background(255);
  stroke(0);
  strokeWeight(0.2);
  noFill();
  beginShape();
  for (int i=0; i<n; i++) {
    float theta = radians(i * (360.0/n));
    if (i%2 == 0) {
      r = cos(t)*250;
    } else {
      r = sin(t)*250;
    }
    float x = width/2 + cos(theta)*r;
    float y = height/2 + sin(theta)*r;
    vertex(x, y);
  }
  endShape(CLOSE);

  t += 0.01;
  if (t >= TWO_PI) {
    t = 0.0;
    n++;
  }
}

void keyPressed() {
  if (key == ' ') {
    noLoop();
  }
  if (key == 'l') {
    loop();
  }
}
