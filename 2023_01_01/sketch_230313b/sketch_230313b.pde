void setup() {
  size(1000, 800);
  background(255);
  textAlign(RIGHT, BOTTOM);
  textSize(36);
}

void draw() {
  //background(255);

  noFill();
  stroke(0);
  rect(width/2-150, height/2-150, 300, 300);
  rect(width/2-140, height/2-140, 280, 280);
  rect(width/2-130, height/2-130, 260, 260);
  rect(width/2-120, height/2-120, 240, 240);
  rect(width/2-110, height/2-110, 220, 220);
  rect(width/2-100, height/2-100, 200, 200);
  rect(width/2-90, height/2-90, 180, 180);
  rect(width/2-80, height/2-80, 160, 160);
  rect(width/2-70, height/2-70, 140, 140);
  rect(width/2-60, height/2-60, 120, 120);
  rect(width/2-50, height/2-50, 100, 100);
  
  ellipse(random(width), random(height), random(10, 100), random(10, 100));
  
  noStroke();
  fill(0);
  String position = mouseX + ":" + mouseY;
  text(position, mouseX, mouseY);
}
