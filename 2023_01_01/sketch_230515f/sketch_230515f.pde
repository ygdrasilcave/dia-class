void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  
  int numPoints = int(map(mouseX, 0, width, 3, 60));
  fill(255, 0, 0);
  noStroke();
  beginShape();
  for(int i=0; i<numPoints; i++){
    float degree = i*(TWO_PI/float(numPoints));
    //float r = random(30, 250);
    float r = 200;
    float x = width/2 + cos(degree)*r;
    float y = height/2 + sin(degree)*r;
    vertex(x, y);
    //ellipse(x, y, 10, 10);
  }
  endShape(CLOSE);
}
