void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  background(255);
  
  float w = 600;
  rect(width/2-w/2.0, height/2-w/2.0, w, w);
  
  rect(width/2-(w-20)/2.0, height/2-(w-20)/2.0, w-20, w-20);
  
  rect(width/2-(w-40)/2.0, height/2-(w-40)/2.0, w-40, w-40);
  
  rect(width/2-(w-60)/2.0, height/2-(w-60)/2.0, w-60, w-60);
  
  rect(width/2-(w-80)/2.0, height/2-(w-80)/2.0, w-80, w-80);
  
  rect(width/2-(w-100)/2.0, height/2-(w-100)/2.0, w-100, w-100);
  
}
