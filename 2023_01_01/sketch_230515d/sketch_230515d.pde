float x = 0;
float y = 0;

float radius = 0;
float degrees = 0;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  //background(255);
  rect(width/2-15, height/2-15, 30, 30);
  
  noFill();
  stroke(0);
  
  radius = sqrt(2)*(width/2);
  degrees = degrees + 15;
  x = cos(radians(degrees)) * radius;
  y = sin(radians(degrees)) * radius;
  rect(x-15, y-15, 30, 30);
  
  line(0, 0, x, y);
  
  
}
