void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  background(255);
  noFill();
  stroke(0);
  for(int i=0; i<100; i++){
    ellipse(i*8, i*8, 10, 10);
  }
}
