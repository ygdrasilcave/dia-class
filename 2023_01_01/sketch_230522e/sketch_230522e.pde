float r = 250;
float t = 0;
int n = 200;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  stroke(0);
  noFill();
  beginShape();
  for (int i=0; i<n; i++) {
    float theta = radians(i * (360.0/n));
    r = map(noise(t), 0, 1, 50, 250);
    float x = width/2 + cos(theta)*r;
    float y = height/2 + sin(theta)*r;
    vertex(x, y);
    t += 0.5;
  }
  endShape(CLOSE);
  noLoop();
}

void keyPressed() {
  if (key == ' ') {
    noLoop();
  }
  if (key == 'l') {
    loop();
  }
}
