PImage img;

void setup(){
  size(800, 800);
  img = loadImage("man.png");
  background(255);
}

void draw(){
  //image(img, 0, 0);
  
  int index = mouseX + (img.width * mouseY);
  color c = img.pixels[index];
  
  //noStroke();
  //fill(c);
  //ellipse(mouseX, mouseY, 10, 10);
  
  strokeWeight(5);
  stroke(c);
  
  line(mouseX, mouseY, mouseX+20, mouseY-20);
}
