float t = 0.0;

void setup() {
  size(800, 400);
  background(0);
}

void draw() {

  float x = random(width);
  stroke(255);
  line(x, 0, x, height/2.0);
  
  float x2 = noise(t)*width;
  line(x2, height/2, x2, height); 
  t+=0.045;
}
