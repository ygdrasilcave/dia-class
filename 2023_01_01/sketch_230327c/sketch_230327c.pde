import processing.pdf.*;
boolean record = false;

int d = 50;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  if (record == true) {
    beginRecord(PDF, "frame-####.pdf");
  }

  background(255);
  stroke(0);
  strokeWeight(1);
  /*for (int x=0; x<width; x+=d) {
   line(x, 0, x, height);
   }
   for (int y=0; y<height; y+=d) {
   line(0, y, width, y);
   }*/
  
  float offsetX = map(mouseX, 0, width, 0, d);
  float offsetY = map(mouseY, 0, height, -0.5, 0.5);
   
  noFill();
  for (int y=0; y<height/d+1; y++) {
    for (int x=0; x<width/d+1; x++) {
      if (y%2 == 0) {
        if (x%2 == 0) {
          for (int k=5; k<=d; k+=10) {
            ellipse(x*d + d*0.5, y*d + d*0.5, k, k);
            //ellipse(x*d + d*0.5 - k*offsetY, y*d + d*0.5 - k*offsetY, k, k);
          }
        } else {
          for (int k=5; k<=d; k+=2) {
            //rect(x*d, y*d, k, k);
            ellipse(x*d + d*0.5, y*d + d*0.5 + offsetX, k, k);
          }
        }
      } else {
        if (x%2 == 0) {
          for (int k=5; k<=d; k+=10) {
            //rect(x*d, y*d, k, k);
            ellipse(x*d + d*0.5, y*d + d*0.5 + offsetX, k, k);
          }
        } else {
          for (int k=5; k<=d; k+=10) {
            ellipse(x*d + d*0.5 + offsetX, y*d + d*0.5, k, k);
          }
        }
      }
    }
  }

  if (record == true) {
    endRecord();
    record = false;
    //exit();
  }
}

void keyPressed() {
  if ((key == 's' || key == 'S' || key == ' ') && record == false) {
    record = true;
  }
}
