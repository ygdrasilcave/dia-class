import processing.pdf.*;
PFont font1;
PFont font2;

String art = "Art is a broad term that encompasses a wide range of human creative expression, including visual arts, literature, music, theater, film, dance, and more. At its core, art is a form of human communication that uses various mediums and techniques to express ideas, emotions, and experiences. Art can be representational, abstract, or symbolic, and it can take many different forms, such as painting, sculpture, photography, performance art, installation art, and more. It can be created for a variety of purposes, such as to entertain, inspire, educate, or provoke thought. Ultimately, the definition of art is subjective and can vary depending on cultural, historical, and personal contexts. What one person considers to be art may not be seen as such by another, and the boundaries between art and other forms of creative expression can be blurry.";
float s;
float t;

void setup(){
  // A3(297x420mm) 842x1191 pixels, 72dpi
  //size(842, 1191);
  //0.9068
  size(764, 1080);
  
  printArray(PFont.list());
  font1 = createFont("Verdana Italic", 120);
  font2 = createFont(PFont.list()[266], 24);
  //font = loadFont("LetterGothicStd.otf");
  
  
  String date = nf(year(),4)+"_"+nf(month(),2)+"_"+nf(day(),2)+"_"+nf(hour(),2)+"_"+nf(minute(),2)+"_"+nf(second(),2);
  beginRecord(PDF, "myWork_"+date+".pdf");
  
  background(255);
  
  fill(0);
  noStroke();
  textAlign(CENTER, CENTER);
  textSize(120);
  textFont(font1);
  text("hello world", width/2, height/2);
  
  textSize(24);
  textFont(font2);
  text("my name is processing", width/2, height/2+100);
  //text(art, 100, 100, 300, 400);
  
  noFill();
  stroke(0);
  rect(10, 10, width-20, height-20);
}

void draw(){
  //background(255);
  s = noise(t);
  s = map(s, 0, 1, 10, 120);
  noFill();
  stroke(0, s);
  strokeWeight(0.25);
  ellipse(mouseX, mouseY, s, s);
  t += 0.045;
}

void keyPressed(){
  if(key == ' '){
    
    String date = nf(year(),4)+"/"+nf(month(),2)+"/"+nf(day(),2)+", "+nf(hour(),2)+":"+nf(minute(),2)+":"+nf(second(),2);
    textAlign(LEFT, CENTER);
    fill(0);
    noStroke();
    text(date, 60, height-60);
    
    endRecord();
    println("DONE");
    exit();
  }
}
