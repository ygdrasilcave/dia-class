void setup() {
  size(1000, 800);
  background(255);
  textAlign(RIGHT, BOTTOM);
  textSize(36);
}

void draw() {
  //background(255);

  noFill();
  stroke(0);
  float x = random(width);
  //float y = random(height/2);
  float y = mouseY/2;
  line(x, y, x, height-y);
  
  noStroke();
  fill(0);
  String position = mouseX + ":" + mouseY;
  //text(position, mouseX, mouseY);
}

void keyPressed(){
  if(key == 's'){
    saveFrame("myArtWork-####.jpg");
  }
}
