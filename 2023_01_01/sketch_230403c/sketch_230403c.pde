import processing.svg.*;
boolean record = false;

void setup() {
  size(800, 800);
  background(255);
  rectMode(CENTER);
}

void draw() {
  if (record == true) {
    beginRecord(SVG, "frame-####.svg");
  }

  background(255);
  stroke(0);

  float len = map(mouseX, 0, width, 0, 400);
  float ratio = map(mouseY, 0, height, 0, 1);

  float t = 0;
  for (int x = 0; x<width; x+=5) {
    pushMatrix();
    translate(x, height/2);
    rotate(t);
    stroke(0);
    noFill();
    line(0, -len*ratio, 0, len*(1-ratio));

    //fill(255, 0, 0);
    //noStroke();
    //rect(0, len*(1-ratio), 5, 5);

    //noStroke();
    //fill(0);
    //ellipse(0, 0, 5, 5);
    popMatrix();
    t+=0.085;
  }

  noFill();
  stroke(0);
  int maxDegree = int(map(mouseX, 0, width, 0, 360));
  for (int i = 0; i<maxDegree; i+=5) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(i));
    ellipse(100, 0, 400, 50);
    popMatrix();
  }

  //ellipse(0,0,width,width);
  if (record == true) {
    endRecord();
    record = false;
    //exit();
  }
}

void keyPressed() {
  if ((key == 's' || key == 'S' || key == ' ') && record == false) {
    record = true;
  }
}
