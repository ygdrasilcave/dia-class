//float r = 250;
//float t = 0;
int n = 200;

float[] r;
float[] t;
float[] tSpeed;

float targetTheta = 0;
float radius = 0;

void setup() {
  size(800, 800);
  r = new float[n];
  t = new float[n];
  tSpeed = new float[n];
  for(int i=0; i<n; i++){
    t[i] = noise(i*0.05);
    tSpeed[i] = 0.038;
  }
  background(255);
}

void draw() {
  //background(255);
  stroke(0);
  noFill();
  strokeWeight(0.25);
  
  float tx = width/2 + cos(targetTheta)*radius;
  float ty = height/2+ sin(targetTheta)*radius;

  beginShape();
  for (int i=0; i<n; i++) {
    float theta = radians(i * (360.0/n));
    r[i] = map(noise(t[i]), 0, 1, 10, 65);
    float x = tx + cos(theta)*r[i];
    float y = ty + sin(theta)*r[i];
    vertex(x, y);
    //line(width/2, height/2, x, y);
    //ellipse(x, y, 10, 10);
    t[i] += tSpeed[i];
  }
  endShape(CLOSE);
  
  targetTheta += 0.01;
  radius += 0.1;
}

void keyPressed() {
  if (key == ' ') {
    noLoop();
  }
  if (key == 'l') {
    loop();
  }
}
