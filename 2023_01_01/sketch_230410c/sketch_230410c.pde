float t = 0;

void setup() {
  size(800, 800);
  background(255);
  smooth();
  rectMode(CENTER);
}

boolean toggle = true;
float arm_dir = 1;

void draw() {
  background(255);
  
  float xoff = 0.0;
  
  for (int x=0; x<20; x++) {
    xoff += 0.01;   // Increment xoff 
    float yoff = 0.0;    
    for (int y=0; y<20; y++) {
      yoff += 0.01;
      drawRobot(x*45, y*50, map(noise(xoff, yoff, t), 0, 1, 0, PI), -1);

    }
  }

  t+=0.065;
}

void drawRobot(float _posX, float _posY, float _angle, int _dir) {
  pushMatrix();
  translate(_posX, _posY);
  scale(0.4, 0.4);
  //rotate(cos(t)*0.2);
  noStroke();
  fill(38, 38, 200);
  rect(0, -42, 38, 30); // head
  rect(0, 0, 50, 50); // body

  pushMatrix();
  translate(-33, -37/2.0);
  rotate(_angle);
  rect(0, 37/2.0, 12, 37); // left arm
  popMatrix();

  pushMatrix();
  translate(33, -37/2.0);
  rotate(_dir * _angle);
  rect(0, 37/2.0, 12, 37); // right arm
  popMatrix();

  float left_leg_height = map(cos(t), -1, 1, 10, 50);
  float right_leg_height = map(-cos(t), -1, 1, 10, 50);
  rect(-9, -((50-left_leg_height)*0.5)+52, 16, left_leg_height); // left leg
  rect(9, -((50-right_leg_height)*0.5)+52, 16, right_leg_height); // right leg

  fill(222, 222, 249);
  ellipse(-8, -42, 12, map(sin(t), -1, 1, 3, 12)); // left eye
  ellipse(8, -42, 12, map(sin(t), -1, 1, 3, 12)); // right eye
  popMatrix();
}

void mousePressed() {
  //drawRobot(mouseX, mouseY);
}
