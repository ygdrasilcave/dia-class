import processing.sound.*;

AudioIn mic;
Amplitude loudness;

void setup(){
  size(800, 800);
  background(255);  
  mic = new AudioIn(this, 0);
  mic.start();
  loudness = new Amplitude(this);
  loudness.input(mic);
}

void draw(){
  float volume = loudness.analyze();
  volume = map(volume, 0, 0.5, 1, 750);
  //println(volume);
  stroke(0);
  strokeWeight(0.25);
  noFill();
  ellipse(width/2, height/2, volume, volume);
}
