import processing.sound.*;

AudioIn mic;
Amplitude loudness;

//int sample_00, sample_01, sample_02, sample_03, sample_04;
int[] sample;

void setup(){
  size(800, 800);
  background(255);  
  mic = new AudioIn(this, 0);
  mic.start();
  loudness = new Amplitude(this);
  loudness.input(mic);
  
  sample = new int[8];
}

void draw(){
  //background(255);
  float volume = loudness.analyze();
  int val = int(map(volume, 0, 0.5, 1, 750));
  for(int i=0; i<sample.length-1; i++){
    sample[i] = sample[i+1];
  }
  sample[sample.length-1] = val;
  
  int val_filter = 0;
  for(int i=0; i<sample.length; i++){
    val_filter += sample[i];
  }
  val_filter = val_filter / 5;  
  //println(val_filter);
  
  noFill();
  stroke(0);
  strokeWeight(0.25);
  ellipse(width/2, height/2, val_filter, val_filter);
}
