float x = 0;
float y = 0;

void setup(){
  size(800, 800);
  background(255);
  y = height/2;
}

void draw(){
  ellipse(x, y, 100, 100);
  x += 5;
  if(x > width){
    x = width;
  }
}
