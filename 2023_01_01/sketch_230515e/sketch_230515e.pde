float x = 0;
float y = 0;

float radius = 0;
float degrees = 0;
float t = 0;
float t2 = 0;

void setup() {
  size(800, 800);
  colorMode(HSB, 360, 100, 100);
  background(0, 0, 100);
}

void draw() {
  //background(255);
  //ellipse(width/2, height/2, 30, 30);

  //fill(degrees % 360, 100, 100);
  fill(degrees % 360, 100, 100);
  noStroke();
  
  noFill();
  stroke(0, 0, 0);
  strokeWeight(0.25);

  radius = map(cos(t), -1, 1, 150, 200);
  degrees += 1;
  x = mouseX + cos(radians(degrees)) * radius;
  y = mouseY + sin(radians(degrees)) * radius;
  ellipse(x, y, map(cos(t2), -1, 1, 80, 180), 80);

  t += 0.1;
  t2 += 0.085;

  //strokeWeight(0.25);
  //stroke(degrees % 360, 100, 100);
  //line(width/2, height/2, x, y);
}
