PImage img;
PImage img2;
PImage arm;
int i;

float t = 0.0;

void setup() {
  size(800, 800);
  i = 10;
  img = loadImage("mantis.png");
  img2 = loadImage("putto.png");
  arm = loadImage("arm.png");
  background(255);
  //imageMode(CENTER);
}

void draw() {
  background(255, 255, 0);
  //image(img, mouseX, mouseY, 200, 200);
  //image(img2, mouseX, mouseY, 200, 200);
  pushMatrix();
  translate(width/2, height/2);
  rotate(sin(t)*1.5);
  image(arm, -arm.width*0.5+50, -50, arm.width*0.5, arm.height*0.5);
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(-sin(t)*1.5);
  scale(-1, 1);
  image(arm, -arm.width*0.5+50, -50, arm.width*0.5, arm.height*0.5);
  popMatrix();
  
  t+=0.05;
}

void keyPressed() {
  if (key == 'r') {
    background(255);
  }
}
