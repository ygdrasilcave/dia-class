PImage img;

float t = 0.0;
float y_speed = 0.0;

void setup(){
  size(800, 800);
  img = loadImage("putto.png");
  imageMode(CENTER);
  rectMode(CENTER);
}

void draw(){
  background(0);
  float posY = map(sin(y_speed), -1, 1, height-100, 100);
  drawRobot(mouseX, posY, map(posY, 100, height-100, 0, PI), -1);
  y_speed += 0.065;
  t += 0.085;
}

void drawRobot(float _posX, float _posY, float _angle, int _dir){
  pushMatrix();
  translate(_posX, _posY);
  scale(1.5, 1.5);
  rotate(cos(t)*0.2);
  noStroke();
  fill(38, 38, 200);
  image(img, 0, -42, img.width*0.25, img.height*0.25);
  //rect(0, -42, 38, 30); // head
  rect(0, 0, 50, 50); // body

  pushMatrix();
  translate(-33, -37/2.0);
  rotate(_angle);
  rect(0, 37/2.0, 12, 37); // left arm
  popMatrix();

  pushMatrix();
  translate(33, -37/2.0);
  rotate(_dir * _angle);
  rect(0, 37/2.0, 12, 37); // right arm
  popMatrix();

  float left_leg_height = map(cos(t), -1, 1, 10, 50);
  float right_leg_height = map(-cos(t), -1, 1, 10, 50);
  rect(-9, -((50-left_leg_height)*0.5)+52, 16, left_leg_height); // left leg
  rect(9, -((50-right_leg_height)*0.5)+52, 16, right_leg_height); // right leg

  fill(222, 222, 249);
  //ellipse(-8, -42, 12, map(sin(t), -1, 1, 3, 12)); // left eye
  //ellipse(8, -42, 12, map(sin(t), -1, 1, 3, 12)); // right eye
  popMatrix();
}
