import processing.pdf.*;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  background(255);
  stroke(0);
  strokeWeight(1);
  for(int x=0; x<width; x+=10){
    line(x, 0, x, height);
  }
  for(int y=0; y<height; y+=10){
    line(0, y, width, y);
  }
}

void keyPressed(){
  if(key == 's' || key == 'S'){
    saveFrame("myWork.jpg");
  }
}
