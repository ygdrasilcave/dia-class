float x = 0;
float y = 0;
float theta = 0;
float r = 250;

float t = 0;

void setup(){
  size(800, 800);
  background(255);
}

void draw(){
  //background(255);
  
  x = cos(theta)*r;
  y = sin(theta)*r;
  
  noFill();
  stroke(0);
  strokeWeight(0.25);
  
  pushMatrix();
  translate(width/2+x, height/2+y);
  rotate(theta);
  rect(-40, -5, 80, 10);
  popMatrix();
  
  //line(width/2, height/2, width/2+x, height/2+y);
  r = map(sin(t), -1, 1, 150, 250);
  theta += 0.02;
  t += 0.25;
}

void keyPressed(){
  if(key == ' '){
    noLoop();
  }
  if(key == 'l'){
    loop();
  }
  if(key == 'e'){
    background(255);
  }
}
