PImage arm;
PImage mantis;
PImage putto;
float t = 0.0;
int num = 20;

void setup() {
  size(800, 800);
  arm = loadImage("arm.png");
  mantis = loadImage("mantis.png");
  putto = loadImage("putto.png");
  background(255);
}

void draw() {
  background(255, 255, 0);
  for (int i=0; i<num; i++) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(i*(360.0/num)));
    //image(arm, -arm.width*0.5+50, -50, arm.width*0.5, arm.height*0.5);
    image(arm, -mouseX, -mouseY, arm.width*0.5, arm.height*0.5);
    //image(mantis, -mouseX, -mouseY, mantis.width*0.5, mantis.height*0.5);
    image(putto, -mouseX, -mouseY, putto.width*0.5, putto.height*0.5);
    popMatrix();
  }
}

void keyPressed() {
  if (key == 'r') {
    background(255);
  }
}
