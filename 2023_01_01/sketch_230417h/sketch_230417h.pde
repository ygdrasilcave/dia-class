float t = 0.0;
float x = 0;

void setup() {
  size(800, 200);
  background(0);
}

void draw() {
  float y = random(height);
  fill(255);
  noStroke();
  ellipse(x, y, 5, 5);
  
  float y2 = noise(t)*height;
  fill(255, 0, 0);
  ellipse(x, y2, 5, 5);
  t+=0.036;
  
  x += 2;
}
