import processing.sound.*;

AudioIn mic;
Amplitude loudness;

//int sample_00, sample_01, sample_02, sample_03, sample_04;
int[] sample;

float x = 0;
float y = 0;

void setup(){
  size(800, 800);
  background(255);  
  mic = new AudioIn(this, 0);
  mic.start();
  loudness = new Amplitude(this);
  loudness.input(mic);
  
  sample = new int[8];
  
  x = width/2;
  y = height/2;
}

void draw(){
  //background(255);
  float volume = loudness.analyze();
  int val = int(map(volume, 0, 0.5, 1, 600));
  for(int i=0; i<sample.length-1; i++){
    sample[i] = sample[i+1];
  }
  sample[sample.length-1] = val;
  
  int val_filter = 0;
  for(int i=0; i<sample.length; i++){
    val_filter += sample[i];
  }
  val_filter = val_filter / 5;  
  //println(val_filter);
  
  noStroke();
  fill(0);
  ellipse(x, y, 3, 3);
  
  if(val_filter > 100 && val_filter < 200){
    x += 3;
  }
  if(val_filter >= 200 && val_filter < 300){
    y -= 3;
  }
  if(val_filter >= 300 && val_filter < 400){
    x -= 3;
  }
  if(val_filter >= 400 && val_filter < 500){
    y += 3;
  }
  
  if(x > width){
    x = 0;
  }
  if(x < 0){
    x = width;
  }
  if(y > height){
    y = 0;
  }
  if(y < 0){
    y = height;
  }
}
