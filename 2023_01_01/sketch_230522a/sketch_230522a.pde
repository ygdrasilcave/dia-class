float x = 0;
float y = 0;

float radius = 0;
float degrees = 0;
float t = 0;
float t2 = 0;

void setup() {
  size(800, 800);
  colorMode(HSB, 360, 100, 100);
  background(0, 0, 100);
}

void draw() {
  //background(255);
  //ellipse(width/2, height/2, 30, 30);

  //fill(degrees % 360, 100, 100);
  fill(degrees % 360, 100, 100);
  noStroke();
  
  noFill();
  stroke(0, 0, 0);
  strokeWeight(0.25);

  //radius = map(cos(t), -1, 1, 200, 250);
  radius = 250;
  degrees += 1;
  x = width/2 + cos(radians(degrees)) * radius;
  y = height/2 + sin(radians(degrees)) * radius;
  float w = map(cos(t2), -1, 1, 80, 180);
  //rect(x-w/2.0, y-40, w, 80);
  rect(x-40, y-40, 80, 80);

  t += 0.1;
  t2 += 0.085;

  //strokeWeight(0.25);
  //stroke(degrees % 360, 100, 100);
  //line(width/2, height/2, x, y);
}


void keyPressed(){
  if(key == ' '){
    noLoop();
  }
  if(key == 'l'){
    loop();
  }
}
