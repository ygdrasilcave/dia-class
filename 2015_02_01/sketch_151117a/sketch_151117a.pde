import processing.serial.*;
Serial myPort;

int currTime;
boolean toggle = true;

void setup() {
  size(500, 500);
  myPort  = new Serial(this, "COM13", 9600);
  currTime = millis();
}

void draw() {
  if (currTime + int(map(mouseX, 0, width, 10, 1000)) < millis()) {
    if (toggle == true) {
      myPort.write('a');
      toggle = !toggle;
    }else{
      myPort.write('b');
      toggle = !toggle;
    }
    currTime = millis();
  }
  
}



/*

int msg = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    int data = Serial.read();
    if (data == 'a') {
      digitalWrite(13, HIGH);
      Serial.println("ON");
    } else {
      digitalWrite(13, LOW);
      Serial.println("OFF");
    }
  }
}

*/