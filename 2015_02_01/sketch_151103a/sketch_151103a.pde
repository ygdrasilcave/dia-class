int numFrames = 7;
PImage[] images = new PImage[numFrames];

int currTime;
int interval = 100;
int imageCounter = 0;

void setup() {
  size(800, 800);

  for (int i = 0; i < numFrames; i++) {
    String imageName = "ballarina_" + nf(i, 3) + ".png";
    images[i] = loadImage(imageName);
  }

  currTime = millis();
} 

void draw() { 
  background(0);
  if (currTime+interval < millis()) {
    currTime = millis();
    imageCounter++;
    if (imageCounter > numFrames-1) {
      imageCounter = 0;
    }
  }
  image(images[imageCounter], 0, 0, width, height);
}