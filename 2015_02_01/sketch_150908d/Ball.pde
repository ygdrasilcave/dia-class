class Ball {

  float x, y;
  float speedX, speedY;
  int dirX = 1;
  int dirY = 1;
  float w, h;
  float r, g, b;
  float xpad, ypad, xpadMax, ypadMax;
  float t = 0.0;
  float tspeed;

  Ball() {
    x = random(width);
    y = random(height);
    speedX = random(2, 6);
    speedY = random(3, 8);
    w = random(10, 30);
    h = w;
    r = random(255);
    g = random(255);
    b = random(255);
    xpad = 0;
    ypad = 0;
    xpadMax = random(20, 100);
    ypadMax = random(20, 100);
    tspeed = random(0.01, 0.1);
  }

  void update(float _s) {
    x+=speedX*dirX;
    y+=speedY*dirY;
    if (x > width || x < 0) {
      dirX*=-1;
    }
    if (y > height || y < 0) {
      dirY*=-1;
    }
    xpad = abs(cos(t))*xpadMax+_s;
    ypad = abs(cos(t))*ypadMax+_s;
    t+=tspeed;
  }

  void display(int _type) {
    if (_type == 1) {
      fill(r, g, b);
      ellipse(x, y, w, h);
    }else if(_type == 2){
      fill(r, g, b);
      rect(x-w/2, y-h/2, w, h);
    }else{
      //noFill();
      //stroke(255);
      fill(255);
      ellipse(x, y, w, h);
      
      stroke(255);
      noFill();
      ellipse(x+xpad, y, w, h);
      ellipse(x-xpad, y, w, h);
      ellipse(x, y+ypad, w, h);
      ellipse(x, y-ypad, w, h);      
      line(x-xpad, y, x+xpad, y);
      line(x, y-ypad, x, y+ypad);
    }
  }
}