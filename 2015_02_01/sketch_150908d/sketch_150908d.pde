import oscP5.*;
import netP5.*;
OscP5 oscP5;

Ball[] b;
int type = 0;
int numBalls = 50;

void setup() {
  size(1200, 1000);
  oscP5 = new OscP5(this, 12000);
  b = new Ball[numBalls];
  for (int i=0; i<numBalls; i++) {
    b[i] = new Ball();
  }
}

void draw() {
  //background(0);
  fill(0, 25);
  noStroke();
  rect(0,0,width,height);
  for (int i=0; i<numBalls; i++) {
    //if (i%2==0) {
      b[i].update(map(mouseX, 0, width, 0, 200));
    //}
    b[i].display(type);
  }
}

void keyPressed() {
  if (key == '1') {
    type = 1;
  }
  if (key == '2') {
    type = 2;
  }
  if (key == '0') {
    type = 0;
  }
}


void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/test")==true) {
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      type = int(value);
      println(value);
    }
  } 
}