Ball[] b;
int num = 100;

float bx, by;
float mx, my, t;

void setup() {
  size(800, 800);
  b = new Ball[num];
  for (int i=0; i<num; i++) {
    if (i == 0) {
      b[i] = new Ball(random(10, 15), random(13, 17));
    } else {
      b[i] = new Ball(random(5, 7), random(6, 8));
    }
  }
}

void draw() {
  background(0); 
  mx = width/2+cos(t)*300;
  my = height/2+sin(t)*300;
  t+=0.02;
  if (mx > width/2+50) {
    bx = mx;
  }
  if (my > height/2+50) {
    by = my;
  }
  for (int i=0; i<num; i++) {
    if (i == 0) {
      b[i].update(0, width, 0, height);
      b[i].display(true);
    } else {
      b[i].update(map(bx, width/2+50, width, width/2-50, 0), bx, map(by, height/2+50, height, width/2-50, 0), by);
      b[i].display(false);
    }
  }
  /*stroke(0, 255, 0);
  line(bx, 0, bx, height);
  line(0, by, width, by);
  line(map(bx, width/2+50, width, width/2-50, 0), 0, map(bx, width/2+50, width, width/2-50, 0), height);
  line(0, map(by, height/2+50, height, width/2-50, 0), width, map(by, height/2+50, height, width/2-50, 0));
  */
}