class Ball {
  float x, y;
  float speedX, speedY;
  float dirX, dirY;

  Ball(float _sx, float _sy) {
    x = width/2;
    y = height/2;
    speedX = _sx;
    speedY = _sy;
    dirX = 1;
    dirY = 1;
  }

  void update(float _bxmin, float _bxmax, float _bymin, float _bymax) {
    x += speedX*dirX;
    y += speedY*dirY;

    if (x > _bxmax) {
      x = _bxmax;
      dirX *= -1;
    }
    if (x < _bxmin) {
      x = _bxmin;
      dirX *= -1;
    }
    if (y > _bymax) {
      y = _bymax;
      dirY *= -1;
    }
    if (y < _bymin) {
      y = _bymin;
      dirY *= -1;
    }
  }
  void display(boolean _type) {
    if (_type == true) {
      fill(229, 0, 255);
      noStroke();
      ellipse(x, y, 100, 100);
    } else {
      fill(255);
      noStroke();
      ellipse(x, y, 30, 30);
    }
  }
}