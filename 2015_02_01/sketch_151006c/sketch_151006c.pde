PVector center;

void setup(){
  size(600, 600);
  center = new PVector(width/2, height/2);
}

void draw(){
  background(0);
  PVector mouse = new PVector(mouseX, mouseY);
  mouse.sub(center);
  //mouse.setMag(100);
  mouse.normalize();
  mouse.mult(100);
  
  pushMatrix();
  stroke(255);
  strokeWeight(5);
  translate(width/2, height/2);
  //rotate(mouse.heading());
  line(0, 0, mouse.x, mouse.y);
  popMatrix();
  
}