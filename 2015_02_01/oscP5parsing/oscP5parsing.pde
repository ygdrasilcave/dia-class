import oscP5.*;
import netP5.*;

OscP5 oscP5;
//NetAddress myRemoteLocation;

void setup() {
  size(400,400);
  frameRate(25);
  oscP5 = new OscP5(this, 12000);
  //myRemoteLocation = new NetAddress("127.0.0.1",12000);
}

void draw() {
  background(0);  
}

/*void mousePressed() {
  OscMessage myMessage = new OscMessage("/test");
  myMessage.add(123);
  myMessage.add(12.34);
  myMessage.add("some text");
  oscP5.send(myMessage, myRemoteLocation); 
}*/


void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/test")==true) {
    /*if(theOscMessage.checkTypetag("ifs")) {
      int firstValue = theOscMessage.get(0).intValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      String thirdValue = theOscMessage.get(2).stringValue();
      print("### received an osc message /test with typetag ifs.");
      println(" values: "+firstValue+", "+secondValue+", "+thirdValue);
      return;
    }*/
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      println(value);
    }
  } 
}