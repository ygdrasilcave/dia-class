class Imgs {
  int frames = 7;
  PImage[] imgs;

  int currTime;
  int interval;
  int imageCounter = 0;
  
  int imgX, imgY;
  int imageSize;

  Imgs(PImage[] _imgs, int _imgX, int _imgY) {
    imgs = new PImage[frames];
    imgs = _imgs;
    currTime = millis();
    interval = int(random(100, 1000));
    imgX = _imgX;
    imgY = _imgY;
    imageSize = int(random(100, 200));
  }

  void display() {
    if (currTime+interval < millis()) {
      currTime = millis();
      imageCounter++;
      if (imageCounter > numFrames-1) {
        imageCounter = 0;
      }
    }
    image(imgs[imageCounter], imgX-imageSize/2, imgY-imageSize/2, imageSize, imageSize);
  }
}