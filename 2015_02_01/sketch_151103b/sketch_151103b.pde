int numFrames = 7;
PImage[] images = new PImage[numFrames];

ArrayList<Imgs> imageInstance;

void setup() {
  size(1000, 1000);

  for (int i = 0; i < numFrames; i++) {
    String imageName = "ballarina_" + nf(i, 3) + ".png";
    images[i] = loadImage(imageName);
  }
  
  imageInstance = new ArrayList();
} 

void draw() { 
  background(0);
  for(int i=0; i<imageInstance.size(); i++){
    Imgs _img = imageInstance.get(i);
    _img.display();
  }
}

void mouseReleased(){
  imageInstance.add(new Imgs(images, int(mouseX), int(mouseY)));
}