import oscP5.*;
import netP5.*;
OscP5 oscP5;
float seq = 0;
float valx;
float valy;
float valz;
float valx1;
float valy1;
float valz1;

void setup(){
  size(600,600);
  background(0);
  oscP5 = new OscP5(this, 12000);
}

void draw(){
  background(0);
  noStroke();
  if(seq == 1.0){
    fill(255, 0, 0);
  }else if(seq == 2.0){
    fill(0, 255, 0);
  }else if(seq == 3.0){
    fill(0, 0, 255);
  }else{
    fill(0);
  }
  rect(0,0,width,height);
  
  fill(255);
  stroke(0);
  pushMatrix();
  translate(width/2, height/2);
  ellipse((valx*50), -(valy*50), 20, 20);
  rect(valx1*50, -(valy1*50), 30, 30);
  popMatrix();
  
  stroke(255);
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);
}

void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/test")==true) {
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      seq = value;
      println(value);
    }
  }else if(theOscMessage.checkAddrPattern("/cursor")==true){
    if(theOscMessage.checkTypetag("ffff")) {
      valx = theOscMessage.get(0).floatValue();
      valy = theOscMessage.get(1).floatValue();
      valz = theOscMessage.get(2).floatValue();
      float time = theOscMessage.get(3).floatValue();
      //float valy1 = theOscMessage.get(4).floatValue();
      //float valz1 = theOscMessage.get(5).floatValue();
      println(valx+" "+valy+" "+valz+" "+time);
    }
  }else if(theOscMessage.checkAddrPattern("/wiggle")==true){
    if(theOscMessage.checkTypetag("fff")) {
      valx1 = theOscMessage.get(0).floatValue();
      valy1 = theOscMessage.get(1).floatValue();
      valz1 = theOscMessage.get(2).floatValue();
      println(valx1+" "+valy1+" "+valz1);
    }
  }
}