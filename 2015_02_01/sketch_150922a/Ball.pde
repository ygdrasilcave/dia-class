class Ball{
  float speedX, speedY;
  float posX, posY;
  float accX, accY;
  int dirX, dirY;
  float dia;
  float maxSpeed;
  int lifeCount;
  int maxLife;
  
  Ball(float _x, float _y){
    posX = _x;
    posY = _y;
    speedX = 0;
    speedY = 0;
    dirX = 1;
    dirY = 1;
    dia = random(10, 30);
    accX = random(-0.5, 0.5);
    accY = random(-0.5, 0.5);
    maxSpeed = random(6, 8);
    lifeCount = 0;
    maxLife = int(random(20, 100));
  }
  
  void update(){
    speedX += accX;
    speedY += accY;
    posX += speedX*dirX;
    posY += speedY*dirY;
    if(speedX > maxSpeed){
      speedX = maxSpeed;
    }
    if(speedX < -maxSpeed){
      speedX = -maxSpeed;
    }
    if(speedY > maxSpeed){
      speedY = maxSpeed;
    }
    if(speedY < -maxSpeed){
      speedY = -maxSpeed;
    }
    
    if(posX > width-dia/2){
      posX = width-dia/2;
      speedX *= -1;
      //posX = dia/2;
    }
    if(posX < dia/2){
      posX = dia/2;
      speedX *= -1;
      //posX = width-dia/2;
    }
    if(posY > height-dia/2){
      posY = height-dia/2;
      speedY *= -1;
      //posY = dia/2;
    }
    if(posY < dia/2){
      posY = dia/2;
      speedY *= -1;
      //posY = height-dia/2;
    }
  }
  
  void display(){
    fill(255);
    //noFill();
    //stroke(255);
    //ellipse(posX, posY, dia, dia);
    rect(posX, posY, dia/2, dia);
  }
  
  boolean isDead(){
    lifeCount++;
    if(lifeCount > maxLife){
      return true;
    }else{
      return false;
    }
  }
}