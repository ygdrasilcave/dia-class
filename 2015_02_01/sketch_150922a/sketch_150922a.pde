ArrayList<Ball> b;

float x, y;
float t = 0.0;

void setup() {
  size(1600, 800);
  background(0);
  b = new ArrayList<Ball>();
  rectMode(CENTER);
}

void draw() {
  //background(0);
  fill(0, 24);
  noStroke();
  rect(width/2,height/2,width,height);
  for (int i=b.size()-1; i>=0; i--) {
    Ball _b = b.get(i);
    _b.accX = random(-0.5, 0.5);
    _b.accY = random(-0.5, 0.5);
    _b.update();
    _b.display();
    if(_b.isDead() == true){
      b.remove(i);
    }
  }
  
  x = x + 5;
  y = height/2 + sin(t)*(height/2-100);
  b.add(new Ball(x, y));
  t+=0.065;
  if(x > width){
    x = 0;
  }
}

void mouseDragged() {
  b.add(new Ball(mouseX, mouseY));
}