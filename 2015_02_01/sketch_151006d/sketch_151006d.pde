ArrayList<Ball> b;
boolean gravitySwitch = false;
boolean windSwitch = false;

int time;
int interval = 1000;

int maxBallNum = 300;
int ballCounter = 0;

float gt, wt;
float t;

boolean brake;

void setup() {
  size(800, 600);
  background(0);
  b = new ArrayList<Ball>();
  b.add(new Ball(new PVector(random(width), random(height)), random(4, 12)));
  time = millis();
  ballCounter++;
  brake = false;
}

void createNewBall() {  
  if (time+interval < millis()) {
    b.add(new Ball(new PVector(random(width), random(height)), random(4, 12)));
    time = millis();
    interval = int(random(10, 200));
    ballCounter++;
  }
}

void draw() {
  if (b.size() < maxBallNum) {
    createNewBall();
  }
  //background(0);
  fill(0, 25);
  noStroke();
  rect(0, 0, width, height);
  for (int i = b.size()-1; i>=0; i--) {
    Ball _b = b.get(i);
    PVector gravity = new PVector(0, map(noise(gt), 0, 1, -0.65, 0.65));
    if (gravitySwitch == true) {
      if (i%2 == 0) {
        _b.addForce(gravity);
      } else {
        _b.addForce(gravity.mult(-1));
      }
    }
    PVector wind = new PVector(map(noise(wt), 0, 1, -0.34, 0.34), 0);
    if (windSwitch == true) {
      if (i%2 == 0) {
        _b.addForce(wind);
      }else{
        _b.addForce(wind.mult(-1));
      }
    }
    PVector circle = new PVector(cos(t), sin(t));
    _b.addForce(circle);
    
    
    _b.update(brake);
    _b.display();
  }
  /*if (b.size() > maxBallNum) {
   b.remove(0);
   }*/
  fill(80);
  noStroke();
  rect(80, 40, 135, 80);
  textSize(48);
  fill(255);
  noStroke();
  text(ballCounter, 100, 100);

  gt += 0.038;
  wt += 0.018;
  t += 0.25;
}

void keyPressed() {
  if (key == 'g') {
    gravitySwitch = !gravitySwitch;
  }
  if (key == 'w') {
    windSwitch = !windSwitch;
  }
  if (key == ' ') {
    brake = !brake;
  }
}