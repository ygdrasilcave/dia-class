ArrayList<Ball> b;
boolean gravitySwitch = false;
boolean windSwitch = false;

int time;
int interval = 1000;

int maxBallNum = 100;
int ballCounter = 0;

void setup() {
  size(800, 600);
  background(0);
  b = new ArrayList<Ball>();
  b.add(new Ball(new PVector(random(width), random(height)), random(4, 12)));
  time = millis();
  ballCounter++;
}

void createNewBall() {  
  if (time+interval < millis()) {
    b.add(new Ball(new PVector(random(width), random(height)), random(4, 12)));
    time = millis();
    interval = int(random(10, 200));
    ballCounter++;
  }
}

void draw() {
  if (b.size() < maxBallNum) {
    createNewBall();
  }
  //background(0);
  fill(0, 25);
  noStroke();
  rect(0, 0, width, height);
  for (int i = b.size()-1; i>=0; i--) {
    Ball _b = b.get(i);
    PVector gravity = new PVector(0, 0.65);
    if (gravitySwitch == true) {
      _b.addForce(gravity);
    }
    PVector wind = new PVector(0.5, 0);
    if (windSwitch == true) {
      _b.addForce(wind);
    }

    _b.update();
    _b.display();
  }
  /*if (b.size() > maxBallNum) {
   b.remove(0);
   }*/
  fill(80);
  noStroke();
  rect(80, 40, 100, 80);
  textSize(48);
  fill(255);
  noStroke();
  text(ballCounter, 100, 100);
}

void keyPressed() {
  if (key == 'g') {
    gravitySwitch = !gravitySwitch;
  }
  if (key == 'w') {
    windSwitch = !windSwitch;
  }
}