class Ball {

  PVector pos;
  PVector speed;
  PVector acc;
  float maxSpeed;
  float ballSize;

  Ball(PVector _p, float _ms) {
    pos = _p;
    speed = new PVector(0, 0);
    acc = new PVector(0, 0);
    maxSpeed = _ms;
    ballSize = random(6, 25);
  }

  void addForce(PVector _f) {
    acc.add(_f);
  }

  void update() {
    speed.add(acc);
    pos.add(speed);
    acc.mult(0);
    speed.limit(maxSpeed);

    if (pos.y > height) {
      pos.y = 0;
    }
    if (pos.y < 0) {
      pos.y = height;
    }
    if (pos.x > width) {
      pos.x = 0;
    }
    if (pos.x < 0) {
      pos.x = width;
    }
  }

  void display() {
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(speed.heading());
    fill(255);
    noStroke();
    ellipse(0, 0, ballSize, ballSize);
    fill(255, 0, 0);
    noStroke();
    ellipse(ballSize*2, 0, ballSize*0.5, ballSize*0.5);
    stroke(255);
    line(0, 0, ballSize*2, 0);
    popMatrix();
  }
}