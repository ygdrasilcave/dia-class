PShape bot;
ArrayList<Bot> bots;
int counter = 0;
int currTime;

void setup() {
  size(800, 800);
  shapeMode(CENTER);
  bots = new ArrayList();
  bot = loadShape("bot1.svg");
  for (int i=0; i<20; i++) {
    bots.add(new Bot(bot, random(-width/2, width/2), random(-height/2, height/2)));
  }
} 

void draw() {
  background(102);
  for (int i = 0; i < bots.size(); i++) {
    Bot _b = bots.get(i);
    _b.display();
  }
}