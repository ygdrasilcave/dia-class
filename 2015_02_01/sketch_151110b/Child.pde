class Bot{
  
  PShape p;
  int currTime = 0;
  int counter = 0;
  float posX, posY;
  float sc = 0;
  
  Bot(PShape _p, float _x, float _y){
    p = _p;
    currTime = millis();
    posX = _x;
    posY = _y;
    sc = random(20, 100);
  }
  
  void display(){
    pushMatrix();
    translate(width/2+posX, height/2+posY);
    
    for(int i = 0; i <= counter; i++){
      PShape ch = p.getChild(i);
      shape(ch, 0, 0);    
    }
    if(currTime + 100 < millis()){
      counter++;
      currTime = millis();
      if(counter > p.getChildCount()-1){
        counter = 0;
      }
    }
    popMatrix();
  }
  
  
}