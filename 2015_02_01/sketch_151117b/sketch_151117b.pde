import processing.serial.*;
Serial myPort;

float x, y;
float t = 0.0;
boolean toggle = true;

void setup() {
  size(500, 500);
  myPort  = new Serial(this, "COM13", 9600);
}

void draw() {
  background(0);
  x = width/2 + cos(radians(t))*200;
  y = height/2 + sin(radians(t))*200;
  t+=map(mouseY, 0, height, 0, 30);
  
  float duration = map(mouseX, 0, width, 0, 360);

  if(t%360 >= 0 && t%360 < duration){
    myPort.write('a');
    fill(255, 0, 0);
  }else{
    myPort.write('b');
    fill(125);
  }
  arc(width/2, height/2, 400, 400, radians(0), radians(duration));
  
  fill(255);
  noStroke();
  ellipse(x, y, 10, 10);
  stroke(255);
  line(width/2, height/2, x, y);
  
}



/*

int msg = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    int data = Serial.read();
    if (data == 'a') {
      digitalWrite(13, HIGH);
      Serial.println("ON");
    } else {
      digitalWrite(13, LOW);
      Serial.println("OFF");
    }
  }
}

*/