class Imgs {
  int frames = 7;
  PImage[] imgs;

  int currTime;
  int interval;
  int imageCounter = 0;

  float imgX1, imgY1, imgX2, imgY2;
  float imageSize;
  
  float rotateVal = 0.0;
  
  int myID;

  Imgs(PImage[] _imgs, float _cx, float _cy, float _rad, int _id) {
    imgs = new PImage[frames];
    imgs = _imgs;
    currTime = millis();
    //interval = int(random(100, 1000));
    interval = 100;
    
    imageSize = 150;

    myID = _id;
    
    rotateVal = (TWO_PI/30)*myID;
    
    imgX1 = _cx + cos(rotateVal)*_rad;
    imgY1 = _cy + sin(rotateVal)*_rad;
    imgX2 = _cx + cos(rotateVal)*(_rad-imageSize);
    imgY2 = _cy + sin(rotateVal)*(_rad-imageSize);
  }


  void display(int _frame) {
    if (currTime+interval < millis()) {
      currTime = millis();
      imageCounter++;
      if (imageCounter > numFrames-1) {
        imageCounter = 0;
      }
    }
    pushMatrix();
    translate(imgX1, imgY1);
    rotate(HALF_PI+rotateVal);
    tint(130);
    image(imgs[_frame], -imageSize/2, -imageSize/2, imageSize, imageSize);
    popMatrix();
    pushMatrix();
    translate(imgX2, imgY2);
    rotate(-HALF_PI+rotateVal);
    tint(255);
    image(imgs[_frame], -imageSize/2, -imageSize/2, imageSize, imageSize);
    popMatrix();
  }
}