int numFrames = 7;
PImage[] images = new PImage[numFrames];

ArrayList<Imgs> imageInstance;
int id = 0;

float dummyX = 0;
float dummyXSpeed = 0.5;

void setup() {
  size(1000, 1000);

  for (int i = 0; i < numFrames; i++) {
    String imageName = "ballarina_" + nf(i, 3) + ".png";
    images[i] = loadImage(imageName);
  }
  
  imageInstance = new ArrayList();
} 

void draw() { 
  background(0);
  for(int i=0; i<imageInstance.size(); i++){
    Imgs _img = imageInstance.get(i);
    //_img.display(int(map(mouseX, 0, width, 0, numFrames)));
    _img.display(int(dummyX)%numFrames);
  }
  dummyX += dummyXSpeed;
}

void mouseReleased(){
  imageInstance.add(new Imgs(images, width/2, height/2, 350, id));
  id++;
}