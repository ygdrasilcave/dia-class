class Ball {
  float x, y;
  float speedX, speedY;
  float dirX, dirY;
  int lifeCount = 0;
  int myMaxLife;

  Ball(float _px, float _py, float _sx, float _sy) {
    x = _px;
    y = _py;
    speedX = _sx;
    speedY = _sy;
    dirX = 1;
    dirY = 1;
    myMaxLife = int(random(1, 15));
  }

  void update(float _bxmin, float _bxmax, float _bymin, float _bymax) {
    x += speedX*dirX;
    y += speedY*dirY;

    if (x > _bxmax) {
      x = _bxmax;
      dirX *= -1;
      lifeCount++;
    }
    if (x < _bxmin) {
      x = _bxmin;
      dirX *= -1;
      lifeCount++;
    }
    if (y > _bymax) {
      y = _bymax;
      dirY *= -1;
      lifeCount++;
    }
    if (y < _bymin) {
      y = _bymin;
      dirY *= -1;
      lifeCount++;     
    }
  }
  void display(boolean _type) {
    if (_type == true) {
      fill(229, 0, 255);
      noStroke();
      ellipse(x, y, 100, 100);
    } else {
      fill(255-110*lifeCount);
      noStroke();
      ellipse(x, y, 30, 30);
    }
    fill(0);
    text(lifeCount, x, y);
  }
  
  boolean getLife(){
    boolean b;
    if(lifeCount >= myMaxLife){
      b = true;
    }else{
      b = false;
    }   
    return b;
  }
}