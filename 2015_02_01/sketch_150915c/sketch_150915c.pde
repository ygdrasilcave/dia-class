ArrayList<Ball> b;

void setup() {
  size(800, 800);
  background(0);
  b = new ArrayList<Ball>();
  /*for(int i=0; i<10; i++){
   b.add(new Ball(random(2, 5), random(3, 7)));
   println(b.size());
   }*/
  textAlign(CENTER, CENTER);
  textSize(24);
}

void draw() {
  background(0);
  for (int i=b.size()-1; i>=0; i--) {
    Ball _b = b.get(i);
    _b.update(0, width, 0, height);
    if (i%2 ==0) {
      _b.display(false);
    } else {
      _b.display(false);
    }

    if (_b.getLife() == true) {
      b.remove(i);
      println(b.size());
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    b.clear();
    println(b.size());
  }
}

void mouseDragged() {
  b.add(new Ball(mouseX, mouseY, random(1, 10), random(1, 12)));
  println(b.size());
}