Ball[] bE;
Ball[] bR;

void setup() {
  size(600, 600);
  bE = new Ball[100];
  bR = new Ball[100];
  for (int i=0; i<100; i++) {
    bE[i] = new Ball();
    bR[i] = new Ball();
  }
}

void draw() {
  background(0);
  for (int i=0; i<100; i++) {
    bE[i].update();
    bE[i].displayE();
    bR[i].update();
    bR[i].displayR();
  }
}