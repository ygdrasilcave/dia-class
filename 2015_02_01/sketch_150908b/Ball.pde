class Ball {

  float x, y;
  float speedX, speedY;
  int dirX = 1;
  int dirY = 1;
  float w, h;
  float r, g, b;
  
  Ball() {
    x = random(width);
    y = random(height);
    speedX = random(2, 6);
    speedY = random(3, 8);
    w = random(10, 30);
    h = w;
    r = random(255);
    g = random(255);
    b = random(255);
  }

  void update() {
    x+=speedX*dirX;
    y+=speedY*dirY;
    if (x > width || x < 0) {
      dirX*=-1;
    }
    if (y > height || y < 0) {
      dirY*=-1;
    }
  }

  void display() {
    fill(r, g, b);
    ellipse(x, y, w, h);
  }
}