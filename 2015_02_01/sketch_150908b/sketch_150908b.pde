Ball[] b;

void setup() {
  size(600, 600);
  b = new Ball[200];
  for (int i=0; i<200; i++) {
    b[i] = new Ball();
  }
}

void draw() {
  background(0);
  for (int i=0; i<200; i++) {
    b[i].update();
    b[i].display();
  }
}