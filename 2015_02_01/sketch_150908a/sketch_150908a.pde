float x, y;
float speedX, speedY;
int dirX = 1;
int dirY = 1;

void setup(){
  size(600, 600);
  background(0);
  x = width/2;
  y = height/2;
  speedX = random(2, 6);
  speedY = random(3, 8);
}

void draw(){
  background(0);
  x+=speedX*dirX;
  y+=speedY*dirY;
  fill(255);
  ellipse(x, y, 30, 30);
  if(x > width || x < 0){
    dirX*=-1;
  }
  if(y > height || y < 0){
    dirY*=-1;
  }
}