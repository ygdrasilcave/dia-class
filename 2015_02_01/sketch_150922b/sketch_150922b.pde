PVector pos;
PVector speed;
PVector acc;

void setup(){
  size(600, 600);
  background(0);
  pos = new PVector(width/2, 20);
  speed = new PVector(0, 0);
  acc = new PVector(0, 0);
}

void draw(){
  background(0);
  
  if(mousePressed == true){
    PVector wind = new PVector(0.2, 0);
    acc.add(wind);
  }
  PVector gravity = new PVector(0, 0.3);
  acc.add(gravity);
     
  if(pos.x > width){
    pos.x = width;
    speed.x *= -1;
  }
  if(pos.x < 0){
    pos.x = 0;
    speed.x *= -1;
  }
  if(pos.y > height){
    pos.y = height;
    PVector fraction = new PVector(0, 3.85);
    acc.add(fraction);
    speed.y *= -1;    
  }
  if(pos.y < 0){
    pos.y = 0;
    speed.y *= -1;
  }
  
  if(pos.y > height-200){
    PVector fraction = new PVector(0, -0.95);
    acc.add(fraction);
  }
  
  stroke(255);
  line(0, height-200, width, height-200);
  
  speed.add(acc);
  pos.add(speed);
  fill(255);
  ellipse(pos.x, pos.y, 20, 20);
  
  //speed.limit(10);
  acc.mult(0);
}