PShape bot;
int children;
int counter = 0;
int currTime;

void setup() {
  size(640, 360);
  bot = loadShape("bot1.svg");
  children = bot.getChildCount();
  currTime = millis();
} 

void draw() {
  background(102);
  translate(width/2, height/2);
  float zoom = map(mouseX, 0, width, 0.1, 4.5);
  scale(zoom);
  bot.disableStyle();
  noFill();
  shape(bot, -140, -140);
  
  PShape b = bot.getChild(counter);  
  b.enableStyle();
  //b.setFill(color(random(255), random(255), random(255)));
  shape(b, -140, -140);
  if (currTime + 100 < millis()) {
    counter++;
    currTime = millis();
  }
  if (counter > children-1) {
    counter = 0;
  }
}