class Ball{
  
  PVector pos;
  PVector speed;
  PVector acc;
  float maxSpeed;
  float ballSize;
  
  Ball(PVector _p, float _ms){
    pos = _p;
    speed = new PVector(0, 0);
    acc = new PVector(0, 0);
    maxSpeed = _ms;
    ballSize = random(6, 25);
  }
  
  void addForce(PVector _f){
    acc.add(_f);
  }
  
  void update(){
    speed.add(acc);
    pos.add(speed);
    acc.mult(0);
    speed.limit(maxSpeed);
    
    if(pos.y > height){
      pos.y = 0;
    }
    if(pos.y < 0){
      pos.y = height;
    }
    if(pos.x > width){
      pos.x = 0;
    }
    if(pos.x < 0){
      pos.x = width;
    }
  }
  
  void display(){
    fill(255);
    ellipse(pos.x, pos.y, ballSize, ballSize);
    stroke(255);
    line(pos.x, pos.y, pos.x+ballSize*2, pos.y);
    fill(255, 0,0);
    ellipse(pos.x+ballSize*2, pos.y, ballSize*0.5, ballSize*0.5);
  }
}