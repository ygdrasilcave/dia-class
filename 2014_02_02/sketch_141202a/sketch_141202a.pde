import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

float x = 0;
float y = 0;

float sx, sy;

void setup() {
  size(400,400);
  frameRate(25);
  oscP5 = new OscP5(this,13000);

  myRemoteLocation = new NetAddress("127.0.0.1",9000);
  
  sx = random(5, 10);
  sy = random(5, 10);
}

void draw() {
  background(0);
 
 x += sx; 
 y += sy;
 
 if(x<0){
   x = 0;
   sx *= -1;
   sendOSC();
 }
 if(x > mouseX){
   x = mouseX;
   sx *= -1;
   sendOSC();
 }
 
 if(y<0){
   y = 0;
   sy *= -1;
   sendOSC();
 }
 if(y>mouseY){
   y = mouseY;
   sy *= -1;
   sendOSC();
 }
 stroke(255);
 line(mouseX, 0, mouseX, height);
 line(0, mouseY, width, mouseY);
 
 fill(255);
 ellipse(x, y, 10, 10);
}


void sendOSC() {
  OscMessage myMessage = new OscMessage("/pd");  
  myMessage.add(random(60, 800));
  oscP5.send(myMessage, myRemoteLocation); 
}


/*void oscEvent(OscMessage theOscMessage) {  
  if(theOscMessage.checkAddrPattern("/planet/camRotate")==true) {
    println("TYPE: " +theOscMessage.typetag());
    if(theOscMessage.checkTypetag("sf")) {
      String firstValue = theOscMessage.get(0).stringValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      println(" values: "+firstValue+", "+int(secondValue));
      return;
    }
  } 
}*/
