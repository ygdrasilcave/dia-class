float[][] kernel1 = {
  {
    -1, -1, -1
  }
  , {
    -1, 9, -1
  }
  , {
    -1, -1, -1
  }
};
float norm1 = 1.0;

float[][] kernel2 = {
  {
    1, 1, 1
  }
  , {
    1, -7, 1
  }
  , {
    1, 1, 1
  }
};
float norm2 = 1.0;


float[][] kernel3 = {
  {
    0, -2, 0
  }
  , {
    -2, 11, -2
  }
  , {
    0, -2, 0
  }
};
float norm3 = 3.0;

float[][] kernel4 = {
  {
    1, 2, 1
  }
  , {
    2, 4, 2
  }
  , {
    1, 2, 1
  }
};
float norm4 = 16.0;

float[][] kernel5 = {
  {
    1, 1, 1
  }
  , {
    1, 1, 1
  }
  , {
    1, 1, 1
  }
};
float norm5 = 9.0;

float[][] kernel6 = {
  {
    1, 0, 0
  }
  , {
    0, 1, 0
  }
  , {
    0, 0, 1
  }
};
float norm6 = 3.0;

float[][] kernel7 = {
  {
    1, 0, 0, 0, 0, 0, 0, 0, 0
  }
  , {
    0, 1, 0, 0, 0, 0, 0, 0, 0
  }
  , 
  {
    0, 0, 1, 0, 0, 0, 0, 0, 0
  }
  , {
    0, 0, 0, 1, 0, 0, 0, 0, 0
  }
  , {
    0, 0, 0, 0, 1, 0, 0, 0, 0
  }
  , 
  {
    0, 0, 0, 0, 0, 1, 0, 0, 0
  }
  , {
    0, 0, 0, 0, 0, 0, 1, 0, 0
  }
  , {
    0, 0, 0, 0, 0, 0, 0, 1, 0
  }
  , 
  {
    0, 0, 0, 0, 0, 0, 0, 0, 1
  }
};
float norm7 = 9.0;

float[][] kernel8 = {
  {
    1, 0, 0, 0, 0
  }
  , {
    0, 1, 0, 0, 0
  }
  , 
  {
    0, 0, 1, 0, 0
  }
  , {
    0, 0, 0, 1, 0
  }
  , {
    0, 0, 0, 0, 1
  }
};
float norm8 = 5.0;

float[][] kernel9 = {
  {
    1, 1, 0
  }
  , {
    1, 0, -1
  }
  , {
    0, -1, -1
  }
};
float norm9 = 1.0;

PImage img;
int offset = 0;

int count = 1;

void setup() {
  img = loadImage("face_w.jpg");
  size(img.width, img.height);
  background(0);
  offset = int(kernel1.length/2);
  println(offset);
}

void draw() {
  for (int y=offset; y<img.height-offset; y++) {
    for (int x=offset; x<img.width-offset; x++) {
      float r = 0;
      float g = 0;
      float b = 0;
      color c;
      for (int i=-offset; i<=offset; i++) {
        for (int j=-offset; j<=offset; j++) {
          int index = (x+j)+(y+i)*width;
          c = img.pixels[index];
          if (count == 1) {
            r = r + red(c)*(kernel1[i+offset][j+offset]/norm1);
            g = g + green(c)*(kernel1[i+offset][j+offset]/norm1);
            b = b + blue(c)*(kernel1[i+offset][j+offset]/norm1);
          } else if (count == 2) {
            r = r + red(c)*(kernel2[i+offset][j+offset]/norm2);
            g = g + green(c)*(kernel2[i+offset][j+offset]/norm2);
            b = b + blue(c)*(kernel2[i+offset][j+offset]/norm2);
          } else if (count == 3) {
            r = r + red(c)*(kernel3[i+offset][j+offset]/norm3);
            g = g + green(c)*(kernel3[i+offset][j+offset]/norm3);
            b = b + blue(c)*(kernel3[i+offset][j+offset]/norm3);
          } else if (count == 4) {
            r = r + red(c)*(kernel4[i+offset][j+offset]/norm4);
            g = g + green(c)*(kernel4[i+offset][j+offset]/norm4);
            b = b + blue(c)*(kernel4[i+offset][j+offset]/norm4);
          } else if (count == 5) {
            r = r + red(c)*(kernel5[i+offset][j+offset]/norm5);
            g = g + green(c)*(kernel5[i+offset][j+offset]/norm5);
            b = b + blue(c)*(kernel5[i+offset][j+offset]/norm5);
          } else if (count == 6) {
            r = r + red(c)*(kernel6[i+offset][j+offset]/norm6);
            g = g + green(c)*(kernel6[i+offset][j+offset]/norm6);
            b = b + blue(c)*(kernel6[i+offset][j+offset]/norm6);
          } else if (count == 7) {
            r = r + red(c)*(kernel7[i+offset][j+offset]/norm7);
            g = g + green(c)*(kernel7[i+offset][j+offset]/norm7);
            b = b + blue(c)*(kernel7[i+offset][j+offset]/norm7);
          } else if (count == 8) {
            r = r + red(c)*(kernel8[i+offset][j+offset]/norm8);
            g = g + green(c)*(kernel8[i+offset][j+offset]/norm8);
            b = b + blue(c)*(kernel8[i+offset][j+offset]/norm8);
          } else if (count == 9) {
            r = r + (red(c)*(kernel9[i+offset][j+offset]/norm9));
            g = g + (green(c)*(kernel9[i+offset][j+offset]/norm9));
            b = b + (blue(c)*(kernel9[i+offset][j+offset]/norm9));
          }
        }
      }
      if (count != 9) {
        fill(r, g, b);
      } else {
        fill(r+127, g+127, b+127);
      }
      noStroke();
      rect(x, y, 1, 1);
    }
  }
}

void keyPressed() {
  count++;
  if (count > 9) {
    count = 1;
  }
  if (count == 1) {
    offset = int(kernel1.length/2);
  } else if (count == 2) {
    offset = int(kernel2.length/2);
  } else if (count == 3) {
    offset = int(kernel3.length/2);
  } else if (count == 4) {
    offset = int(kernel4.length/2);
  } else if (count == 5) {
    offset = int(kernel5.length/2);
  } else if (count == 6) {
    offset = int(kernel6.length/2);
  } else if (count == 7) {
    offset = int(kernel7.length/2);
  } else if (count == 8) {
    offset = int(kernel8.length/2);
  } else if (count == 9) {
    offset = int(kernel9.length/2);
  }
}

