PImage img;

/*float[][] kernel = {{1,1,1}, {1,1,1}, {1,1,1}};
float norm = 9.0;*/

/*float[][] kernel = {{-1,-1,-1}, {-1,9,-1}, {-1,-1,-1}};
float norm = 1.0;*/

/*float[][] kernel = {{-1,-1,-1}, {-1,8,-1}, {-1,-1,-1}};
float norm = 1.0;*/

/*float[][] kernel = {{0,0,0}, {1,1,1}, {0,0,0}};
float norm = 3.0;*/

//float[][] kernel = {{1, 1, 1}, {0, 0, 0}, {-1, -1, -1}};
//float[][] kernel = {{-1, -1, -1}, {0, 0, 0}, {1, 1, 1}};
//float norm = 1.0;

/*float[][] kernel = {{0, 1, 0}, {1, -3, 1}, {0, 1, 0}};
float norm = 1.0;*/

/*float[][] kernel = {{0, 1, 0}, {1, -3, 1}, {0, 1, 0}};
float norm = 1.0;*/

float[][] kernel = {
  {
    1, 2, 1
  }
  , {
    2, 4, 2
  }
  , {
    1, 2, 1
  }
};
float norm = 16.0;


void setup(){
  img = loadImage("rose.jpg");
  size(img.width, img.height);
}

void draw(){
  for(int y=1; y<height-1; y++){
    for(int x=1; x<width-1; x++){
      color c;
      float r = 0;
      float g = 0;
      float b = 0;
      for(int j=-1; j<=1; j++){
        for(int i=-1; i<=1; i++){
          int index = (x+i)+(y+j)*width;
          c = img.pixels[index];
          r = r + red(c)*kernel[j+1][i+1]/norm;
          g = g + green(c)*kernel[j+1][i+1]/norm;
          b = b + blue(c)*kernel[j+1][i+1]/norm;
        }
      }     
      fill(r, g, b);
      noStroke();
      rect(x,y,1,1);
    }
  }
}
