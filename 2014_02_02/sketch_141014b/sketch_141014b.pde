color[] palette = new color[16*16];
int cellSize = 20;

int count = 0;

PImage img;

void setup() {
  img = loadImage("rose.jpg");
  size(img.width, img.height);
  for (int i=0; i<palette.length; i++) {
    palette[i] = color(81*i/255, 251*i/255, 255*i/255);
  }
}

void draw() {
  /*for (int y=0; y<16; y++) {
   for (int x=0; x<16; x++) {
   int index = x+y*16;
   color c = palette[index];
   fill(c);
   stroke(0);
   rect(x*cellSize, y*cellSize, cellSize, cellSize);
   }
   }*/
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      int index = x+y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      int newIndex = int(brightness(c));
      if (r > g && r > b) {
        fill(palette[newIndex]);
      } else {
        fill(c);
      }
      noStroke();
      rect(x, y, 1, 1);
    }
  }
}

void keyPressed() {
  count++;
  if (count > 3) {
    count = 0;
  }

  if (count == 0) {
    for (int i=0; i<palette.length; i++) {
      palette[i] = color(81*i/255, 251*i/255, 255*i/255);
    }
  } else if (count == 1) {
    for (int i=0; i<palette.length; i++) {
      palette[i] = color(255*i/255, 227*i/255, 82*i/255);
    }
  } else if (count == 2) {
    for (int i=0; i<palette.length; i++) {
      palette[i] = color(255*i/255, 57*i/255, 236*i/255);
    }
  } else if (count == 3) {
    for (int i=0; i<palette.length; i++) {
      palette[i] = color(i);
    }
  }
}

