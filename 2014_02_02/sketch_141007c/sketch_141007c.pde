int cellSize = 20;

int r = 239;
int g = 118;
int b = 255;

color[] palette = new color[16*16];
boolean t = true;

void setup(){
  size(cellSize*16, cellSize*16);
  switchPalette();
}

void draw(){
  for(int y=0; y<16; y++){
    for(int x=0; x<16; x++){
      int i = x + y * 16;
      fill(palette[i]);
      stroke(0);
      rect(x*cellSize, y*cellSize, cellSize, cellSize);
    }
  }
  
}

void keyPressed(){
  t = !t;
  switchPalette();
}

void switchPalette() {
  if (t == true) {
    for (int i=0; i<256; i++) {
      palette[i] = color(r*i/255, g*i/255, b*i/255);
    }
  } else {
    for (int i=0; i<256; i++) {
      if (i<180) {
        palette[i] = color(255*i/255, 0, 0);
      } else if (i>=180 && i<235) {
        palette[i] = color(0, 255*i/255, 0);
      } else {
        palette[i] = color(0, 0, 255*i/255);
      }
    }
  }
}
