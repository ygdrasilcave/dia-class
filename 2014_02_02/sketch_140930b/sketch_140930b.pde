PImage img;
int pixelSize = 1;

ArrayList<ColorList> ac = new ArrayList<ColorList>();

void setup() {
  size(900, 633);
  img = loadImage("festival.jpeg");
  for (int y=0; y<height; y+=pixelSize) {
    for (int x = 0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      ac.add(new ColorList(c));
    }
  }
  background(0);
}

int index = 0;
void draw() {
  index = 0;
  for (int x=0; x<width; x++) {
    for (int y=0; y<height; y++) {
      ColorList tc = ac.get(index);
      color c = tc.getColor();
      fill(c);
      noStroke();
      rect(x, y, pixelSize, pixelSize);
      index++;
    }
  }
}

