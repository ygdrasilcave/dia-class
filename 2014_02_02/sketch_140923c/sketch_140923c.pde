PImage img;
int pixelSize = 1;

float worldRecordR = 0;
float worldRecordG = 0;
float worldRecordB = 0;
boolean showImage = true;
int stw = 1;

void setup() {
  size(900, 633);
  img = loadImage("festival.jpeg");
}

void draw() {
  background(0);
  if (showImage == true) {
    for (int y=0; y<height; y+=pixelSize) {
      for (int x = 0; x<width; x+=pixelSize) {
        int index = x + y*width;
        color c = img.pixels[index];
        float r = red(c);
        float g = green(c);
        float b = blue(c);
        float br = brightness(c);
        fill(c);
        noStroke();
        rect(x, y, pixelSize, pixelSize);
        if (r >= worldRecordR) {
          worldRecordR = r;
        }
        if (g >= worldRecordG) {
          worldRecordG = g;
        }
        if (b >= worldRecordB) {
          worldRecordB = b;
        }
      }
    }
  }
  for (int y=0; y<height; y+=pixelSize) {
    for (int x = 0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      if (r == worldRecordR) {
        stroke(255,0,0);
        strokeWeight(stw);
        point(x, y);
      }
      if(g == worldRecordG){
        stroke(0,255,0);
        strokeWeight(stw);
        point(x, y);
      }
      if(b == worldRecordB){
        stroke(0,0,255);
        strokeWeight(stw);
        point(x, y);
      }
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    showImage = !showImage;
  }
  if(key == CODED){
    if(keyCode == UP){
      stw++;
      if(stw > 6){
        stw = 6;
      }
    }
    if(keyCode == DOWN){
      stw--;
      if(stw < 1){
        stw = 1;
      }
    }
  }
  
}

