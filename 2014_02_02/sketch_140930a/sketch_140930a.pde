PImage img;
int pixelSize = 1;

ArrayList<ColorList> ac = new ArrayList<ColorList>();

void setup() {
  size(900, 633);
  img = loadImage("festival.jpeg");
  for (int y=0; y<height; y+=pixelSize) {
    for (int x = 0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      ac.add(new ColorList(c));
    }
  }
}

float t = 0.0;

void draw() {
  background(0);
  //int xCount = int(random(-10, 10));
  //int xCount = 0;
  int xCount = int(sin(t)*10);
  int xOffSet = xCount;
  int yCount = 0;
  for (int i=0; i<ac.size (); i++) {
    ColorList tc = ac.get(i);
    color c = tc.getColor();
    fill(c);
    noStroke();
    rect(xCount, yCount, pixelSize, pixelSize);
    xCount++;
    if(xCount > (width-1)+xOffSet){
      //xCount = int(random(-10, 10));
      //xCount = 0;
      xCount = int(sin(t)*map(mouseY,0,height,1,25));
      t+=map(mouseX, 0, width, 0.001, 0.098);
      xOffSet = xCount;
      yCount++;
    }
  }
}

