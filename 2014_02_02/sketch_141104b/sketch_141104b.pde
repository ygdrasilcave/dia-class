XML xmlWonju;
XML xmlJeju;
String[] dataW;
float[] tempW;
float worldRecordW = 0;
String[] dataJ;
float[] tempJ;
float worldRecordJ = 0;

boolean toggle = true;

void setup() {
  size(1000, 200);
  loadData();
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0);
  for (int i=0; i<dataW.length; i++) {
    fill(0, 255, 0);
    rect(20+i*(width/dataW.length), height-map(tempW[i], 0, worldRecordW, 0, height-30), 10, map(tempW[i], 0, worldRecordW, 0, height-30));
    text(dataW[i], 20+i*(width/dataW.length), 20);
  }
  for (int i=0; i<dataJ.length; i++) {
    fill(255, 255, 0);
    rect(40+i*(width/dataJ.length), height-map(tempJ[i], 0, worldRecordJ, 0, height-30), 10, map(tempJ[i], 0, worldRecordJ, 0, height-30));
    text(dataJ[i], 40+i*(width/dataJ.length), 20);
  }
  if(minute()%5 == 0 && toggle == true){
    loadData();
    toggle = false;
  }else if(minute()%5 != 0 && toggle == false){
    toggle = true;
  }
}

void loadData() {
  if(xmlWonju != null){
    xmlWonju = null;
    dataW = null;
    tempW = null;
    worldRecordW = 0;
  }
  if(xmlJeju != null){
    xmlJeju = null;
    dataJ = null;
    tempJ = null;
    worldRecordJ = 0;
  }
  xmlWonju = loadXML("http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=4213037000");
  xmlJeju = loadXML("http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=5013025900");
  XML[] childrenW = xmlWonju.getChild("channel").getChild("item").getChild("description").getChild("body").getChildren("data");
  dataW = new String[childrenW.length];
  tempW = new float[childrenW.length];
  XML[] childrenJ = xmlJeju.getChild("channel").getChild("item").getChild("description").getChild("body").getChildren("data");
  dataJ = new String[childrenJ.length];
  tempJ = new float[childrenJ.length];
  //println(xml);
  //println(children.length);
  for (int i=0; i<childrenW.length; i++) {
    dataW[i] = childrenW[i].getChild("temp").getContent();
    tempW[i] = float(dataW[i]);
    if (worldRecordW < tempW[i]) {
      worldRecordW = tempW[i];
    }
  }
  for (int i=0; i<childrenJ.length; i++) {
    dataJ[i] = childrenJ[i].getChild("temp").getContent();
    tempJ[i] = float(dataJ[i]);
    if (worldRecordJ < tempJ[i]) {
      worldRecordJ = tempJ[i];
    }
  }
  //println(tempW);
}

