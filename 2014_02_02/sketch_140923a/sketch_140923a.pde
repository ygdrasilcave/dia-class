PImage img;
int pixelSize = 1;

float worldRecord = 0;
float worldRecord_pre = 0;
IntList wrX = new IntList();
IntList wrY = new IntList();

void setup() {
  size(800, 400);
  img = loadImage("Image-1.jpg");

  //image(img, 0, 0);
  //worldRecord = 0;
  for (int y=0; y<height; y=y+pixelSize) {
    for (int x=0; x<width; x=x+pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      if (b >= worldRecord) {
        worldRecord = b;
      }      
      fill(c);
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  for (int y=0; y<height; y=y+pixelSize) {
    for (int x=0; x<width; x=x+pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      if (b == worldRecord) {
        wrX.append(x);
        wrY.append(y);
      }
    }
  }
  noFill();
  for (int i=0; i < wrX.size (); i++) {
    stroke(255, 0, 0);
    ellipse(wrX.get(i), wrY.get(i), 50, 50);
    stroke(255);
  }
  println(wrX.size());
  println(worldRecord);
}

void draw() {
}

