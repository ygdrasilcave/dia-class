PImage img;
int pixelSize = 1;

ArrayList<ColorList> cl;
FloatDict brightnessDict;
int[] indexArray;

void setup() {
  size(900, 633);
  img = loadImage("output_000026.jpg");
  cl = new ArrayList<ColorList>();
  brightnessDict = new FloatDict();
  for (int y=0; y<height; y+=pixelSize) {
    for (int x = 0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      cl.add(new ColorList(c));
      brightnessDict.set(str(index), brightness(c));
    }
  }
  background(0);
  brightnessDict.sortValues();
  indexArray = int(brightnessDict.keyArray());
  /*for(int i=0; i<20; i++){
   println(indexArray[i]);
   }*/
}

void draw() {
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {

      int index = x+y*width;

      if (mode == true) {
        ColorList _cl = cl.get(index);
        fill(_cl.getColor());
      } else {
        ColorList _cl = cl.get(indexArray[index]);
        fill(_cl.getColor());
      }
      noStroke();
      rect(x, y, 1, 1);
    }
  }
}

boolean mode = true; 
void keyPressed() {
  if (key == ' ') {
    mode = !mode;
  }
  if(key == 's'){
    saveFrame("output_######.jpg");
  }
}

