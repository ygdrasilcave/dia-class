PImage img, img1, img2;
float x=0;
float y=0;
float step = 20;

int changeImage = 0;

PGraphics gr;

import oscP5.*;
import netP5.*;
OscP5 oscP5;
NetAddress myRemoteLocation;

void setup() {
  img = loadImage("face_w.jpg");
  img1 = loadImage("Checkerboard.jpg"); 
  img2= loadImage("grad.jpg"); 
  //size(img.width, img.height);
  size(665, 602);
  gr = createGraphics(width, height);
  oscP5 = new OscP5(this, 13000);
  myRemoteLocation = new NetAddress("127.0.0.1", 9000);
  background(255);
}

void draw() {
  int index = int(x) + int(y)*width;
  color c = color(0, 0, 0);

  if (changeImage == 0) {
    gr.beginDraw();
    //gr.background(255);
    gr.fill(0);
    if (mousePressed) {
      gr.rect(mouseX, mouseY, 20, 20);
    }
    gr.endDraw();
    image(gr, 0, 0, width, height);
    c = gr.pixels[index];
  } else if (changeImage == 1) {
    image(img1, 0, 0, width, height);
    c = img1.pixels[index];
  } else if (changeImage == 2) {
    image(img, 0, 0, width, height);
    c = img.pixels[index];
  } else if (changeImage == 3) {
    image(img2, 0, 0, width, height);
    c = img2.pixels[index];
  }
  x += step;
  if (x > width-step) {
    x = 0;
    y += step;
    if (y>height-step) {
      x = 0;
      y = 0;
    }
  }
  float br = brightness(c);
  fill(br);
  stroke(0);
  rect(x, y, step, step);
  sendOSC(br);
}

void sendOSC(float _msg) {
  OscMessage myMessage = new OscMessage("/pd");  
  myMessage.add(_msg);
  oscP5.send(myMessage, myRemoteLocation);
}

void keyPressed() {
  if (key == ' ') {
    changeImage++;
    changeImage = changeImage%4;
    background(255);
    gr.background(255);
  }
}
