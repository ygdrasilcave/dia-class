PImage img;
boolean r = true;

void setup() {
  img = loadImage("face_w.jpg");
  size(img.width, img.height);
  image(img, 0, 0);
}

void draw() {
  loadPixels();
  for (int y=1; y<img.height-1; y++) {
    for (int x=1; x<img.width-1; x++) {      
      int nX = int(random(x-1, x+2));
      int nY = int(random(y-1, y+2));
      nX = constrain(nX, x-1, x+1);
      nY = constrain(nY, y-1, y+1);
      int index = nX + nY*width;
      pixels[x+y*width] = pixels[index];
    }
  }
  updatePixels();
  
  if(second()%10 == 0 && r == true){
    //reset();
    r = false;
  }else if(second()%10 != 0 && r == false){
    r = true;
  }
}

void keyPressed(){
  reset();
}

void reset(){
  loadPixels();
  for (int y=0; y<img.height; y++) {
    for (int x=0; x<img.width; x++) {      
      int index = x + y*width;
      pixels[index] = img.pixels[index];
    }
  }
  updatePixels();
}

