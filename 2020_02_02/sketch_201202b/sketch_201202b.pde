import oscP5.*;
import netP5.*;

OscP5 osc;
NetAddress padIP;

ArrayList<Boid> boids;

float maxSpeed = 4;
float maxForce = 0.065;
float sFactor = 1.5;
float aFactor = 1.0;
float cFactor = 0.9;

int type = 0;


void setup() {
  size(1000, 800);
  boids = new ArrayList<Boid>();

  //pc port
  osc = new OscP5(this, 8080);
  //ipad ip address and port
  padIP = new NetAddress("192.168.1.108", 8000);
}

void draw() {
  background(255);

  for (Boid _b : boids) {
    _b.ms = maxSpeed;
    _b.mf = maxForce;
    _b.flock(boids);
    _b.update();
    _b.display();
  }

  //sFactor = map(mouseX, 0, width, 0, 1.5);
  //cFactor = map(mouseY, 0, height, 0, 1.0);
}

void mouseDragged() {
  boids.add(new Boid(mouseX, mouseY));
}


String cleanOSC = "/clean__project_1__";
String toggle1 = cleanOSC + "toggle_1";
String button1 = cleanOSC + "button_1";
String slider1 = cleanOSC + "slider_1";
String slider2 = cleanOSC + "slider_2";
String slider3 = cleanOSC + "slider_3";
String slider4 = cleanOSC + "slider_4";
String slider5 = cleanOSC + "slider_5";
String picker = cleanOSC + "picker_1";

void oscEvent(OscMessage msg) {
  //print(msg.addrPattern());

  //type tag: i(int) f(float) s(string)
  if (msg.checkAddrPattern(toggle1) == true) {
    //println("toggle");
    //println(msg.typetag());
    if (msg.checkTypetag("i")) {
      int toggleValue = msg.get(0).intValue();
      type = toggleValue;
      println("Type: " + type);
    }
  } else if (msg.checkAddrPattern(button1) == true) {
    //println("button");
    //println(msg.typetag());
    if (msg.checkTypetag("i")) {
      int buttonValue = msg.get(0).intValue();
      println(buttonValue);
    }
  } else if (msg.checkAddrPattern(slider1) == true) {
    //println("slider1");
    //println(msg.typetag());
    if (msg.checkTypetag("f")) {
      float slider1Value = msg.get(0).floatValue();
      maxSpeed = slider1Value*4;
      println("Max Speed: " + maxSpeed);
      
    }
  } else if (msg.checkAddrPattern(slider2) == true) {
    //println("slider2");
    //println(msg.typetag());
    if (msg.checkTypetag("f")) {
      float slider2Value = msg.get(0).floatValue();
      maxForce = slider2Value*0.065;
      println("Max Force: " + maxForce);
    }
  } else if (msg.checkAddrPattern(slider3) == true) {
    //println("slider2");
    //println(msg.typetag());
    if (msg.checkTypetag("f")) {
      float slider3Value = msg.get(0).floatValue();
      sFactor = slider3Value*1.5;
      println("Separate: " + sFactor);
    }
  } else if (msg.checkAddrPattern(slider4) == true) {
    //println("slider2");
    //println(msg.typetag());
    if (msg.checkTypetag("f")) {
      float slider4Value = msg.get(0).floatValue();
      aFactor = slider4Value*1.5;
      println("Align: " + aFactor);
    }
  } else if (msg.checkAddrPattern(slider5) == true) {
    //println("slider2");
    //println(msg.typetag());
    if (msg.checkTypetag("f")) {
      float slider5Value = msg.get(0).floatValue();
      cFactor = slider5Value*1.5;
      println("Cohesion: " + cFactor);
    }
  }
  
  else if (msg.checkAddrPattern(picker) == true) {
    println(msg.typetag());
    if (msg.checkTypetag("i")) {
      int pickerValue = msg.get(0).intValue();
      type = pickerValue;
      println("Type: " + type);
    }
  }
}
