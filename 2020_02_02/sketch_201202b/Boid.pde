class Boid {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float ms;
  float mf;

  Boid(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(random(-1, 1), random(-1, 1));
    acceleration = new PVector(0, 0);
    ms = 4;
    mf = 0.065;
  }

  void applyForce(PVector _f) {
    acceleration.add(_f);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(ms);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x < -50) {
      position.x = width+50;
    }
    if (position.x > width+50) {
      position.x = -50;
    }
    if (position.y < -50) {
      position.y = height+50;
    }
    if (position.y > height+50) {
      position.y = -50;
    }
  }

  PVector separate(ArrayList<Boid> _b) {
    float maxDist = 20;
    PVector sumDistance = new PVector(0, 0);
    int counter = 0;

    for (Boid other : _b) {
      float dist = PVector.dist(position, other.position);
      if (dist > 0 && dist < maxDist) {
        PVector diff = PVector.sub(position, other.position);
        diff.normalize();
        diff.div(dist);
        sumDistance.add(diff);
        counter++;
      }
    }

    PVector steer = new PVector(0, 0);
    if (counter > 0) {
      sumDistance.div(counter);
      sumDistance.normalize();
      sumDistance.mult(ms);
      steer = PVector.sub(sumDistance, velocity); 
      steer.limit(mf);
    }
    return steer;
  }

  PVector align(ArrayList<Boid> _b) {
    float maxDist = 50;
    PVector sumVelocity = new PVector(0, 0);
    int counter = 0;
    for (Boid other : _b) {
      float dist = PVector.dist(position, other.position);
      if (dist > 0 && dist < maxDist) {
        sumVelocity.add(other.velocity);
        counter++;
      }
    }

    PVector steer = new PVector(0, 0);
    if (counter > 0) {
      sumVelocity.div(counter);
      sumVelocity.normalize();
      sumVelocity.mult(ms);
      steer = PVector.sub(sumVelocity, velocity);
      steer.limit(mf);
    }    
    return steer;
  }

  PVector cohesion(ArrayList<Boid> _b) {
    float maxDist = 50;
    PVector sumPosition = new PVector(0, 0);
    int counter = 0;
    for (Boid other : _b) {
      float dist = PVector.dist(position, other.position);
      if (dist > 0 && dist < maxDist) {
        sumPosition.add(other.position);
        counter++;
      }
    }

    PVector steer = new PVector(0, 0);
    if (counter > 0) {
      //TARGET
      sumPosition.div(counter);
      //SEEK FUNCTION
      PVector desiredVelocity = PVector.sub(sumPosition, position);
      desiredVelocity.normalize();
      desiredVelocity.mult(ms);
      steer = PVector.sub(desiredVelocity, velocity);
      steer.limit(mf);
    }
    return steer;
  }

  void flock(ArrayList<Boid> _b) {
    PVector separate = separate(_b);
    PVector align = align(_b);
    PVector cohesion = cohesion(_b);

    separate.mult(sFactor);
    align.mult(aFactor);
    cohesion.mult(cFactor);

    applyForce(separate);
    applyForce(align);
    applyForce(cohesion);
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    if (type == 0) {
      fill(0);
      noStroke();
      beginShape();
      vertex(5, 0);
      vertex(-5, 2.5);
      vertex(-5, -2.5);
      endShape(CLOSE);
    } else if (type == 1) {
      fill(255, 0, 0);
      ellipse(0, 0, 10, 5);
    } else if (type == 2) {
      fill(0, 255, 0);
      ellipse(0, 0, 10, 5);
    }
    popMatrix();
  }
}
