class FlowField {
  int cols;
  int rows;
  int res;
  float[][] angleValue;
  float zoff = 0;

  FlowField() {
    res = 20;
    cols = width/res;
    rows = height/res;
    angleValue = new float[cols][rows];
  }

  void processNoise(float _zs) {
    float xoff = 0;
    for (int x=0; x<cols; x+=1) {
      float yoff = 0;
      for (int y=0; y<rows; y+=1) {
        angleValue[x][y] = noise(xoff, yoff, zoff)*TWO_PI + filedDirection;
        yoff += 0.15;
      }
      xoff += 0.15;
    }
    zoff += _zs;
  }

  void processTarget(float _tx, float _ty) {
    for (int x=0; x<cols; x++) {
      for (int y=0; y<rows; y+=1) {
        angleValue[x][y] = 0;
      }
    }
  }

  void display() {
    for (int x=0; x<cols; x+=1) {
      for (int y=0; y<rows; y+=1) {
        //fill((angleValue[x][y]/TWO_PI)*255);
        //noStroke();
        //rect(x*res, y*res, res, res);

        float r = res*0.5;
        float cx = x*res + r;
        float cy = y*res + r;
        float theta = angleValue[x][y];
        float px = cx + cos(theta)*r;
        float py = cy + sin(theta)*r;

        stroke(0);
        line(cx, cy, px, py);
        fill(0);
        noStroke();
        ellipse(cx, cy, 5, 5);
      }
    }
  }

  PVector getDesired(PVector _pos) {
    int col = int(constrain(_pos.x/res, 0, cols-1));
    int row = int(constrain(_pos.y/res, 0, rows-1));
    float theta = angleValue[col][row];
    float x = cos(theta);
    float y = sin(theta);
    PVector desired = new PVector(x, y);
    return desired;
  }
}
