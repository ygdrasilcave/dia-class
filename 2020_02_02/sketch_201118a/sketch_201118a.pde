FlowField f;
float filedDirection;
ArrayList<Vehicle> v;

void setup() {
  size(1000, 1000);
  f = new FlowField();
  v = new ArrayList<Vehicle>();
  createVehicles();
}

void createVehicles() {
  for (int i=0; i<50; i++) {
    v.add(new Vehicle(random(width), random(height)));
  }
}

void draw() {
  background(255);
  filedDirection = map(mouseY, 0, height, 0, TWO_PI);
  if (mousePressed==true) {
    f.processTarget(mouseX, mouseY);
  } else {
    f.processNoise(map(mouseX, 0, width, 0, 0.06));
  }
  f.display();

  for (Vehicle _v : v) {  
    _v.follow(f);
    _v.update();
    _v.display();
  }
  /*for(int i=0; i<v.size(); i++){  
   v.get(i).follow(f);
   v.get(i).update();
   v.get(i).display();
   }*/
}
