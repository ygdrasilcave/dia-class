class Vehicle {
  PVector position;
  PVector velocity;
  PVector acceleration;

  float ms;
  float mf;
  float r;

  Vehicle(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);  
    ms = 4;
    mf = 0.1;
    r = 25;
  }

  void follow(FlowField _f) {
    PVector desired = _f.getDesired(position);
    desired.normalize();
    desired.mult(ms);
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(mf);
    applyForce(steer);
  }

  void applyForce(PVector _f) {
    acceleration.add(_f);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(ms);
    position.add(velocity);
    acceleration.mult(0);
  }

  void display() {
    fill(0);
    noStroke();

    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    beginShape();
    vertex(r, 0);
    vertex(-r, r*0.5);
    vertex(-r, -r*0.5);    
    endShape();
    popMatrix();


    if (position.x < 0) {
      position.x = width;
    }
    if (position.x > width) {
      position.x = 0;
    }
    if (position.y < 0) {
      position.y = height;
    }
    if (position.y > height) {
      position.y = 0;
    }
  }
}
