Pendulum[] p;

void setup() {
  size(1000, 1000);
  p = new Pendulum[100];
  for (int y=0; y<10; y++) {
    for (int x=0; x<10; x++) {
      int i = x + y*10;
      p[i] = new Pendulum(new PVector(x*100+50, y*100), random(25, 100));
    }
  }
}

void draw() {
  background(255);
  for (int i=0; i<p.length; i++) {
    p[i].update();
    p[i].display();
  }
  
  PVector newOrigin = new PVector(mouseX, mouseY);
  p[10].origin = newOrigin.copy();
  
  //println(p.location);
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<p.length; i++) {
      p[i].angle = PI/4;
    }
  }
}
