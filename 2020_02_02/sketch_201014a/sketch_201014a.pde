Mover[] m = new Mover[100];

void setup() {
  size(1200, 800);
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(random(0.2, 4), random(width), 100);
  }

  background(255);
}

void draw() {
  background(255);

  PVector wind = new PVector(map(mouseX, 0, width, 0.025, -0.025), 0);
  PVector gravity = new PVector(0, 0.1);

  for (int i=0; i<m.length; i++) {
    m[i].applyForce(wind);
    m[i].applyForce(gravity);

    //friction = -1 * v.normalize() * c * N
    float c = 0.0065; // coefficient of friction
    float N = 1; //normal force: the force perpendicular to the object’s motion along a surface
    PVector friction = m[i].velocity.copy();
    friction.normalize();
    friction.mult(-1);
    friction.mult(c*N);
    m[i].applyForce(friction);

    m[i].update();
    m[i].display();
  }
}
