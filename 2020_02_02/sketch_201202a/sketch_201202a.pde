ArrayList<Boid> boids;

float sFactor = 1.5;
float aFactor = 1.0;
float cFactor = 0.9;

void setup(){
  size(1000, 800);
  boids = new ArrayList<Boid>();
}

void draw(){
  background(255);
  
  for(Boid _b : boids){
    //_b.ms = map(mouseX, 0, width, 0, 8);
    //_b.mf = map(mouseY, 0, height, 0, 0.085);
    _b.flock(boids);
    _b.update();
    _b.display();
  }
  
  //sFactor = map(mouseX, 0, width, 0, 1.5);
  //cFactor = map(mouseY, 0, height, 0, 1.0);
}

void mouseDragged(){
  boids.add(new Boid(mouseX, mouseY));
}
