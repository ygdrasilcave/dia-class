PVector location = new PVector(100, 100);
PVector velocity = new PVector(1, 3.3);

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  
  location.add(velocity);
  
  if(location.x < 0 || location.x > width){
    velocity.x = velocity.x*-1;
  }
  if(location.y < 0 || location.y > height){
    velocity.y = velocity.y*-1;
  }
  
  ellipse(location.x, location.y, 50, 50);
}
