void setup() {
  size(600, 400);
}


void draw() {
  background(255);

  PVector a = new PVector(20, 300);
  PVector b = new PVector(500, 250);
  PVector v = new PVector(mouseX, mouseY);
  
  PVector ab = PVector.sub(b, a);
  PVector av = PVector.sub(v, a);
  
  ab.normalize();
  ab.mult(av.dot(ab));
  
  PVector normalPoint = PVector.add(a, ab);
  
  stroke(0);
  line(a.x, a.y, b.x, b.y);
  line(a.x, a.y, v.x, v.y);
  ellipse(normalPoint.x, normalPoint.y, 10, 10);
}
