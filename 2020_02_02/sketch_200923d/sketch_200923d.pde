PVector location;
PVector velocity;
PVector acc;
float maxVel = 10;
float ballSize = 100;

PVector target;
int currentTime = 0;
int interval;

void setup() {
  size(1200, 800);
  location = new PVector(width/2, height/2);
  velocity = new PVector(0, 0);
  acc = new PVector(0, 0);
  target = new PVector(random(width), random(height));
  currentTime = millis();
  interval = int(random(500, 2000));
}

void draw() {
  background(255);

  //PVector mouse = new PVector(mouseX, mouseY);
  //PVector dir = PVector.sub(mouse, location);
  PVector dir = PVector.sub(target, location);

  if (dir.mag() < ballSize/5) {
    dir.normalize();
    dir.mult(0);
    acc = dir;
    velocity.add(acc);
    velocity.mult(0.85);
  } else {
    dir.normalize();
    dir.mult(1.5);
    acc = dir;
    velocity.add(acc);
  }

  velocity.limit(maxVel);
  location.add(velocity);

  if (location.x > width + ballSize/2) {
    location.x = -ballSize/2;
  } else if (location.x < -ballSize/2) {
    location.x = width + ballSize/2;
  }

  if (location.y > height + ballSize/2) {
    location.y = -ballSize/2;
  } else if (location.y < -ballSize/2) {
    location.y = height + ballSize/2;
  }

  fill(0);
  ellipse(location.x, location.y, ballSize, ballSize);
  
  fill(255, 0, 0);
  ellipse(target.x, target.y, 20, 20);
  
  if(currentTime + interval < millis()){
    target.set(random(width), random(height));
    currentTime = millis();
    interval = int(random(200, 800));
  }
}
