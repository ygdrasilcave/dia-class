class Bob{
  PVector position;
  PVector velocity;
  PVector acceleration;
  float damping = 0.98;
    
  Bob(float _x, float _y){
    position = new PVector(_x, _y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.mult(damping);
    position.add(velocity);
    acceleration.mult(0);
  }
  
  void applyForce(PVector _force){
    PVector f = _force.copy();
    acceleration.add(f);
  }
  
  void display(){
    fill(0);
    noStroke();
    ellipse(position.x, position.y, 50, 50);
  }
}
