Bob b;

PVector springAnchor;
float springLength;
float springK;

void setup() {
  size(1000, 1000);
  springAnchor = new PVector(width/2, 20);
  springLength = 200;
  springK = 0.025;
  b = new Bob(width/2, 800);
}

void draw() {
  background(255);
  fill(255, 0, 0);
  noStroke();
  ellipse(springAnchor.x, springAnchor.y, 20, 20);

  stroke(0);
  strokeWeight(1);
  line(springAnchor.x, springAnchor.y, b.position.x, b.position.y);

  PVector gravity = new PVector(0, 0.9);

  PVector wind = new PVector(0, 0);
  if (mousePressed == true) {
    PVector _w = new PVector(10, 4);
    wind = _w.copy();
  }

  PVector springForce = PVector.sub(b.position, springAnchor);
  float dist = springForce.mag();
  float stretch = dist - springLength;
  springForce.normalize();
  springForce.mult(-1*springK*stretch);

  b.applyForce(gravity);
  b.applyForce(wind);
  b.applyForce(springForce);
  b.update();
  b.display();
}
