PVector location;
PVector velocity;
float ballSize = 200;

void setup(){
  size(1200, 800);
  location = new PVector(width/2, height/2);
  velocity = new PVector(random(0.5, 3), random(0.3, 3));
}


void draw(){
  background(255);
  
  location.add(velocity);
  
  if(location.x > width + ballSize/2){
    location.x = -ballSize/2;
  }else if(location.x < -ballSize/2){
    location.x = width + ballSize/2;
  }
  
  if(location.y > height + ballSize/2){
    location.y = -ballSize/2;
  }else if(location.y < -ballSize/2){
    location.y = height+ballSize/2;
  }
  
  fill(0);
  ellipse(location.x, location.y, ballSize, ballSize);
}
