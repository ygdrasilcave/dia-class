class Vehicle {

  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  float targetAngle;

  Vehicle() {
    position = new PVector(random(width), random(height));
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, 0);
    maxSpeed = random(1, 3);
    maxForce = random(0.02, 0.09);
    targetAngle = 0;
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x < 0) {
      position.x = width;
    }
    if (position.x > width) {
      position.x = 0;
    }
    if (position.y < 0) {
      position.y = height;
    }
    if (position.y > height) {
      position.y = 0;
    }
  }

  void applyForce(PVector force) {
    acceleration.add(force);
  }

  void wander() {
    float wanderRad = 50;
    float wanderDist = 150;

    PVector circlePos = velocity.copy();
    circlePos.normalize();
    circlePos.mult(wanderDist);
    circlePos.add(position);

    fill(0, 255, 0);
    noStroke();
    ellipse(circlePos.x, circlePos.y, 5, 5);
    noFill();
    stroke(0, 255, 0);
    ellipse(circlePos.x, circlePos.y, wanderRad*2, wanderRad*2);
    line(position.x, position.y, circlePos.x, circlePos.y);

    targetAngle = targetAngle + random(-0.3, 0.3);
    float tx = circlePos.x + cos(targetAngle)*wanderRad; 
    float ty = circlePos.y + sin(targetAngle)*wanderRad;

    noStroke();
    fill(255, 0, 0);
    ellipse(tx, ty, 5, 5);
    stroke(255, 0, 0);
    line(circlePos.x, circlePos.y, tx, ty);

    PVector wander_target = new PVector(tx, ty);
    seek(wander_target);
  }

  void seek(PVector _target) {
    PVector desired = PVector.sub(_target, position);
    //desired.normalize();
    //desired.mult(maxSpeed);
    if (b_target == true) {
      float dist = desired.mag();
      desired.normalize();
      if (dist < 100) {
        float m = map(dist, 0, 100, 0, maxSpeed);
        desired.mult(m);
      } else {
        desired.mult(maxSpeed);
      }
    } else {
      desired.setMag(maxSpeed);
    } 
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }

  void display() {
    fill(0);
    noStroke();
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-25, -12.5, 50, 25);
    ellipse(30, -10, 10, 10);
    ellipse(30, 10, 10, 10);
    popMatrix();
  }
}
