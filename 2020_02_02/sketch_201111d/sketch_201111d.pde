Vehicle[] v;
boolean b_target = false;

void setup() {
  size(800, 800);
  v = new Vehicle[20];
  for (int i=0; i<v.length; i++) {
    v[i] = new Vehicle();
  }
}

void draw() {
  background(255);

  if (mousePressed == true) {
    b_target = true;
  }else{
    b_target = false;
  }
  PVector target = new PVector(mouseX, mouseY);
  for (int i=0; i<v.length; i++) {
    if (b_target == true) {
      v[i].seek(target);
    } else {
      v[i].wander();
    }
    v[i].update();
    v[i].display();
  }
}
