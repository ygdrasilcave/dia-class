class Vehicle {

  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  float targetAngle;
  float wanderDist;


  ArrayList<PVector> tail = new ArrayList<PVector>();
  FloatList tailAngle = new FloatList();
  int time;

  Vehicle() {
    position = new PVector(random(width), random(height));
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, 0);
    maxSpeed = random(1, 3);
    maxForce = random(0.02, 0.09);
    targetAngle = 0;
    time = millis();
    
    wanderDist = random(50, 180);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (time + 100 < millis()) {
      tail.add(position.copy());
      tailAngle.append(velocity.heading());
      time = millis();
    }

    if (tail.size() > 10) {
      tail.remove(0);
      tailAngle.remove(0);
    }

    if (position.x < 0) {
      position.x = width;
    }
    if (position.x > width) {
      position.x = 0;
    }
    if (position.y < 0) {
      position.y = height;
    }
    if (position.y > height) {
      position.y = 0;
    }
  }

  void applyForce(PVector force) {
    acceleration.add(force);
  }

  void wander() {
    float wanderRad = 50;
    

    PVector circlePos = velocity.copy();
    circlePos.normalize();
    circlePos.mult(wanderDist);
    circlePos.add(position);

    fill(0, 255, 0);
    noStroke();
    ellipse(circlePos.x, circlePos.y, 5, 5);
    noFill();
    stroke(0, 255, 0);
    ellipse(circlePos.x, circlePos.y, wanderRad*2, wanderRad*2);
    line(position.x, position.y, circlePos.x, circlePos.y);

    targetAngle = targetAngle + random(-0.3, 0.3);
    float tx = circlePos.x + cos(targetAngle)*wanderRad; 
    float ty = circlePos.y + sin(targetAngle)*wanderRad;

    noStroke();
    fill(255, 0, 0);
    ellipse(tx, ty, 5, 5);
    stroke(255, 0, 0);
    line(circlePos.x, circlePos.y, tx, ty);

    PVector wander_target = new PVector(tx, ty);
    seek(wander_target);
  }

  void seek(PVector _target) {
    PVector desired = PVector.sub(_target, position);
    //desired.normalize();
    //desired.mult(maxSpeed);
    if (b_target == true) {
      float dist = desired.mag();
      desired.normalize();
      if (dist < 100) {
        float m = map(dist, 0, 100, 0, maxSpeed);
        desired.mult(m);
      } else {
        desired.mult(maxSpeed);
      }
    } else {
      desired.setMag(maxSpeed);
    } 
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }

  void display() {
    //fill(0);
    //noStroke();
    noFill();
    stroke(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-12.5, -12.5, 25, 25);
    ellipse(20, -10, 10, 10);
    ellipse(20, 10, 10, 10);
    popMatrix();

    for (int i = tail.size()-1; i>=0; i--) {
      //ellipse(tail.get(i).x, tail.get(i).y, 25, 25);
      pushMatrix();
      translate(tail.get(i).x, tail.get(i).y);
      rotate(tailAngle.get(i));
      rect(-12.5, -12.5, 25, 25);
      popMatrix();
    }
  }
}
