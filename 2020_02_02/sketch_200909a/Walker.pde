class Walker{
  int x;
  int y;
  
  Walker(){
    x = width/2;
    y = height/2;
  }
  Walker(int _x, int _y){
    x = _x;
    y = _y;
  }
  
  void step(){
    /*int choice = int(random(4));
    if(choice == 0){
      x++;
    }else if(choice == 1){
      x--;
    }else if(choice == 2){
      y++;
    }else{
      y--;
    }*/
    int stepX = int(random(3))-1;
    int stepY = int(random(3))-1;
    x += stepX;
    y += stepY;
    println(x);
  }
  
  void display(){
    stroke(0);
    point(x, y);
  }
}
