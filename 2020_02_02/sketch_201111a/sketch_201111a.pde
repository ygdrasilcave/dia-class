Vehicle[] v;

void setup() {
  size(800, 800);
  v = new Vehicle[20];
  for (int i=0; i<v.length; i++) {
    v[i] = new Vehicle();
  }
}

void draw() {
  background(255);

  PVector target = new PVector(mouseX, mouseY);
  for (int i=0; i<v.length; i++) {
    v[i].seek(target);
    v[i].update();
    v[i].display();
  }
}
