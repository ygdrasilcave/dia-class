class Vehicle{
  
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  
  Vehicle(){
    position = new PVector(random(width), random(height));
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, 2);
    maxSpeed = random(2, 8);
    maxForce = random(0.05, 0.5);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }
  
  void applyForce(PVector force){
    acceleration.add(force);
  }
  
  void seek(PVector _target){
    PVector desired = PVector.sub(_target, position);
    //method 1
    //desired.normalize();
    //desired.mult(maxSpeed);
    //desired.setMag(maxSpeed);
    //method 2 arrive
    //desired.mult(0.5);
    //method 3 arrive
    float dist = desired.mag();
    desired.normalize();
    if(dist < 100){
      float m = map(dist, 0, 100, 0, maxSpeed);
      desired.mult(m);
    }else{
      desired.mult(maxSpeed);
    }
    
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }
  
  void display(){
    fill(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-25, -12.5, 50, 25);
    ellipse(30, -10, 10, 10);
    ellipse(30, 10, 10, 10);
    popMatrix();
  }
}
