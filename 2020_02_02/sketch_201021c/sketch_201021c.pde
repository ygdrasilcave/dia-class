float t = 0;
float ts = 0.025;

float r1 = 0;
float r2 = 0;

void setup(){
  size(800, 800);
}

void draw(){
  //background(255);
  t = t + ts;
  //r1 = 100 + 200 * abs(cos(t));
  r1 = 100 + 200 * cos(t)*sin(t*1.3);
  float x1 = width/2 + cos(t)*r1;
  float y1 = height/2 + sin(t)*r1;
  pushMatrix();
  translate(x1, y1);
  rotate(t);
  fill(255, 0, 0);
  rect(-50, -25, 100, 50);
  popMatrix();
  /*
  r2 = 350;
  float x2 = width/2 + cos(t*1.5)*r2;
  float y2 = height/2 + sin(t*1.5)*r2; 
  pushMatrix();
  translate(x2, y2);
  rotate(t*1.5);
  fill(0, 255, 0);
  rect(-50, -25, 100, 50);
  popMatrix();
  */
  stroke(0);
  line(width/2, height/2, x1, y1);
  //line(width/2, height/2, x2, y2);
}
