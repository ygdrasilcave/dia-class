Mover[] m;
int moverNum = 50;

void setup() {
  size(1200, 800);
  m = new Mover[moverNum];
  for (int i=0; i<moverNum; i++) {
    m[i] = new Mover();
  }
}

void draw() {
  background(255);
  for (int i=0; i<moverNum; i++) {
    m[i].update();
    m[i].checkEdges();
    m[i].display();
  }
}
