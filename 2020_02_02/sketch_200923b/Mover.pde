class Mover {

  PVector location;
  PVector velocity;
  float ballSize;
  float ballSizeScale;
  float t = 0;
  float ts;

  Mover() {
    location = new PVector(width/2, height/2);
    velocity = new PVector(random(-3, 3), random(-3, 3));   
    ballSize = random(30, 160);
    ballSizeScale = 1;
    ts = random(0.065, 0.15);
  }

  void update() {
    location.add(velocity);
    t += ts;
    ballSizeScale = map(sin(t), -1, 1, 0.5, 1);
  }

  void checkEdges() {
    float bs = (ballSize*ballSizeScale)/2;
    if (location.x > width + bs) {
      location.x = -bs;
    } else if (location.x < -bs) {
      location.x = width + bs;
    }
    if (location.y > height + bs) {
      location.y = -bs;
    } else if (location.y < -bs) {
      location.y = height+bs;
    }
  }

  void display() {
    noFill();
    stroke(0);
    ellipse(location.x, location.y, ballSize*ballSizeScale, ballSize*ballSizeScale);
  }
}
