class Bob {
  PVector position;
  PVector velocity;
  PVector acceleration;

  PVector springAnchor;
  float springLength;
  float springK;

  float damping = 0.98;
  float dia;

  Bob(float _sx, float _sy, float _x, float _y, float _dia) {
    position = new PVector(_x, _y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);

    springAnchor = new PVector(_sx, _sy);
    springLength = random(150, 250);
    springK = random(0.0065, 0.025);
    dia = _dia;
  }

  void springForce() {
    PVector springForce = PVector.sub(position, springAnchor);
    float dist = springForce.mag();
    float stretch = dist - springLength;
    springForce.normalize();
    springForce.mult(-1*springK*stretch);
    applyForce(springForce);
  }

  void update() {
    velocity.add(acceleration);
    velocity.mult(damping);
    position.add(velocity);
    acceleration.mult(0);
  }

  void applyForce(PVector _force) {
    PVector f = _force.copy();
    acceleration.add(f);
  }

  void display() {
    fill(255, 0, 0);
    noStroke();
    ellipse(springAnchor.x, springAnchor.y, dia*0.3, dia*0.3);

    stroke(0);
    strokeWeight(1);
    line(springAnchor.x, springAnchor.y, position.x, position.y);

    fill(0);
    noStroke();
    ellipse(position.x, position.y, dia, dia);
  }
}
