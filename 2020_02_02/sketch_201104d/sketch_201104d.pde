Bob[] b;

void setup() {
  size(1000, 500);
  b = new Bob[100];
  for (int i=0; i<b.length; i++) {
    b[i] = new Bob(10*i+5, 10, 10*i+5, random(100, 400), random(10, 30));
  }
}

void draw() {
  background(255);
  PVector gravity = new PVector(0, 0.9);

  PVector wind = new PVector(0, 0);
  if (mousePressed == true) {
    if (mouseX <width/2) {
      PVector _w = new PVector(1, 0.2);
      wind = _w.copy();
    }else{
      PVector _w = new PVector(-1, 0.2);
      wind = _w.copy();
    }
  }
  for (int i=0; i<b.length; i++) {
    b[i].applyForce(gravity);
    b[i].applyForce(wind);
    b[i].springForce();
    b[i].update();
    b[i].display();
  }
}
