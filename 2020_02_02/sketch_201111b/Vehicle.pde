class Vehicle{
  
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  
  Vehicle(){
    position = new PVector(random(width), random(height));
    acceleration = new PVector(0, 0);
    velocity = new PVector(0, 0);
    maxSpeed = random(2, 8);
    maxForce = random(0.05, 0.5);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }
  
  void applyForce(PVector force){
    acceleration.add(force);
  }
  
  void wander(){
    float wanderRad = 50;
    float wanderDist = 100;
    
    PVector circlePos = velocity.copy();
    circlePos.normalize();
    circlePos.mult(wanderDist);
    circlePos.add(position);
    
    fill(0, 255, 0);
    noStroke();
    ellipse(circlePos.x, circlePos.y, 5, 5);
    noFill();
    stroke(0, 255, 0);
    ellipse(circlePos.x, circlePos.y, wanderRad*2, wanderRad*2);
    
    float t = random(TWO_PI);
    float tx = circlePos.x + cos(t)*wanderRad; 
    float ty = circlePos.y + sin(t)*wanderRad;
    
    noStroke();
    fill(255, 0, 0);
    ellipse(tx, ty, 5, 5);
    
    PVector target = new PVector(tx, ty);
    seek(target);
    
  }
  
  void seek(PVector _target){
    PVector desired = PVector.sub(_target, position);
    //desired.normalize();
    //desired.mult(maxSpeed);
    desired.setMag(maxSpeed);
 
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }
  
  void display(){
    fill(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-25, -12.5, 50, 25);
    ellipse(30, -10, 10, 10);
    ellipse(30, 10, 10, 10);
    popMatrix();
  }
}
