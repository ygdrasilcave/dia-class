class Vehicle {
  PVector position;
  PVector velocity;
  PVector acceleration;

  float maxSpeed;
  float maxForce;
  float r;

  Vehicle(PVector _pos, float _ms, float _mf) {
    maxSpeed = _ms;
    maxForce = _mf;
    position = _pos;
    velocity = new PVector(maxSpeed, 0);
    acceleration = new PVector(0, 0);
    r = 12;
  }

  void applyForce(PVector _f) {
    acceleration.add(_f);
  }

  void seek(PVector _target) {
    PVector desired = PVector.sub(_target, position);
    desired.normalize();
    desired.mult(maxSpeed);

    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }

  void follow(Path _p) {
    //path point a, b
    PVector a = _p.points.get(0);
    PVector b = _p.points.get(1);
    //predict point
    PVector v = velocity.copy();
    v.normalize();
    v.mult(50);
    //v.setMag(50);
    v.add(position);
    //projection vector
    PVector ab = PVector.sub(b, a);
    PVector av = PVector.sub(v, a);
    ab.normalize();
    ab.mult(av.dot(ab));
    PVector normalPoint = PVector.add(a, ab);

    PVector target = PVector.sub(b, a);
    target.normalize();
    target.mult(25);
    target.add(normalPoint);
    seek(target);
    //}


    if (showPath == true) {
      fill(0);
      noStroke();
      ellipse(v.x, v.y, 5, 5);
      ellipse(normalPoint.x, normalPoint.y, 5, 5);
      fill(255, 0, 0);
      ellipse(target.x, target.y, 8, 8);
      stroke(0);
      line(position.x, position.y, v.x, v.y);
    }
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x > width+10) {
      position.x = -10;
    } else if (position.x < -10) {
      position.x = 0;
      velocity.set(maxSpeed, 0);
    }
  }

  void display() {
    fill(255);
    stroke(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    beginShape();
    vertex(r, 0);
    vertex(-r, r/2);
    vertex(-r, -r/2);
    endShape(CLOSE);  
    popMatrix();
  }
}
