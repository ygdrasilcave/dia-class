ArrayList<Vehicle> v;
Path p;

boolean showPath = true;

void setup() {
  size(1000, 600);
  p = new Path();
  p.addPoint(0, random(height));
  p.addPoint(width, random(height));

  v = new ArrayList<Vehicle>();
  v.add(new Vehicle(new PVector(0, height/2), 4, 0.25));
}

void draw() {
  background(255);
  if (showPath == true) {
    p.display();
  }

  //PVector target = new PVector(mouseX, mouseY);
  //v.seek(target);
  for (Vehicle _v : v) {
    _v.follow(p);
    _v.update();
    _v.display();
  }

  if (mousePressed) {
    v.add(new Vehicle(new PVector(mouseX, mouseY), random(1, 4), random(0.03, 0.25)));
  }
}

void keyPressed() {
  if (key == ' ') {
    p.clearPoints();
    p.addPoint(0, random(height));
    p.addPoint(width, random(height));
  }

  if (key == 'n') {
    v.add(new Vehicle(new PVector(random(width), random(height)), random(1, 4), random(0.03, 0.25)));
  }
  
  if(key == 'p'){
    showPath = !showPath;
  }
}
