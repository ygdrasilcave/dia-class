class Path {
  ArrayList<PVector> points;
  float r;

  Path() {
    points = new ArrayList<PVector>();
    r = 30;
  }

  void addPoint(float _x, float _y) {
    PVector p = new PVector(_x, _y);
    points.add(p);
  }

  void clearPoints() {
    points.clear();
  }

  void display() {

    noFill();
    stroke(150);
    strokeWeight(r*2);
    beginShape();
    for (PVector _p : points) {
      vertex(_p.x, _p.y);
    }    
    endShape();

    stroke(0);
    strokeWeight(1);
    beginShape();
    for (PVector _p : points) {
      vertex(_p.x, _p.y);
    }    
    endShape();
  }
}
