PVector location;
PVector velocity;
PVector acceleration;
float rectSize = 200;
float rectSizeScale = 1;
float maxVelocity = 10;

void setup() {
  size(1200, 800);
  location = new PVector(width/2, height/2);
  velocity = new PVector(0, 0);
  acceleration = new PVector(random(0.05), random(0.05));
}

void draw() {
  background(255);
  
  acceleration = PVector.random2D();
  float r = random(10);
  if(r > 2){
    acceleration.mult(0.2);
  }else{
    acceleration.mult(0.8);
  }

  velocity.add(acceleration);
  velocity.limit(maxVelocity);
  println(velocity.mag());
  location.add(velocity);

  float bs = (rectSize*rectSizeScale)/2;
  if (location.x > width + bs) {
    location.x = -bs;
  } else if (location.x < -bs) {
    location.x = width + bs;
  }
  if (location.y > height + bs) {
    location.y = -bs;
  } else if (location.y < -bs) {
    location.y = height+bs;
  }

  fill(0);
  rect(location.x-rectSize/2, location.y-rectSize/2, rectSize, rectSize);
}
