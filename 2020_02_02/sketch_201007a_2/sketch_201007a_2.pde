Mover[] movers = new Mover[100];

void setup() {
  size(1200, 800);
  for (int i = 0; i < movers.length; i++) {
    movers[i] = new Mover(random(0.1, 5), 0, 0);
  }
}

void draw() {
  background(255);

  PVector wind = new PVector(0.01, 0);

  for (int i = 0; i < movers.length; i++) {
    
    //Gravity on Earth
    float m = movers[i].mass;
    PVector gravity = new PVector(0, 0.1*m);

    movers[i].applyForce(wind);
    movers[i].applyForce(gravity);

    movers[i].update();
    movers[i].display();
    movers[i].checkEdges();
  }
}
