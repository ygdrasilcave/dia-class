import oscP5.*;
import netP5.*;

OscP5 osc;
NetAddress padIP;

void setup(){
  size(800, 800);
  //pc port
  osc = new OscP5(this, 8080);
  //ipad ip address and port
  padIP = new NetAddress("192.168.1.108", 8000);
}

void draw(){
}

String cleanOSC = "/clean__project_1__";
String toggle1 = cleanOSC + "toggle_1";
String button1 = cleanOSC + "button_1";
String slider1 = cleanOSC + "slider_1";
String slider2 = cleanOSC + "slider_2";


void oscEvent(OscMessage msg){
  //print(msg.addrPattern());
  
  
  //type tag: i(int) f(float) s(string)
  if(msg.checkAddrPattern(toggle1) == true){
    //println("toggle");
    //println(msg.typetag());
    if(msg.checkTypetag("i")){
      int toggleValue = msg.get(0).intValue();
      println(toggleValue);
    }
  }else if(msg.checkAddrPattern(button1) == true){
    //println("button");
    //println(msg.typetag());
    if(msg.checkTypetag("i")){
      int buttonValue = msg.get(0).intValue();
      println(buttonValue);
    }
  }else if(msg.checkAddrPattern(slider1) == true){
    //println("slider1");
    //println(msg.typetag());
    if(msg.checkTypetag("f")){
      float slider1Value = msg.get(0).floatValue();
      println(slider1Value);
    }
  }else if(msg.checkAddrPattern(slider2) == true){
    //println("slider2");
    //println(msg.typetag());
    if(msg.checkTypetag("f")){
      float slider2Value = msg.get(0).floatValue();
      println(slider2Value);
    }
  }
  
  
}
