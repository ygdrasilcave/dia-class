class Pendulum{
  PVector location;
  PVector origin;
  float r;
  float angle;
  float aVelocity;
  float aAcceleration;
  float damping;
  
  Pendulum(PVector _o, float _r){
    origin = _o.copy();
    location = new PVector();
    r = _r;
    angle = PI/4;
    
    aVelocity = 0.0;
    aAcceleration = 0.0;
    
    damping = 0.998;
    //damping = 1;
  }
  
  void update(){
    float gravity = 0.8;
    aAcceleration = sin(-1*angle)*gravity/(r);
    
    aVelocity += aAcceleration;
    angle += aVelocity;
    
    aVelocity *= damping;
  }
  
  void display(){    
    location.set(sin(angle)*r, cos(angle)*r);
    location.add(origin); 
    
    //PVector diff = PVector.sub(location, origin);
    //diff.normalize();
    
    fill(0);
    ellipse(origin.x, origin.y, 10, 10);
    
    stroke(0);
    line(origin.x, origin.y, location.x, location.y); 
    
    fill(0);
    pushMatrix();
    translate(location.x, location.y);
    rotate(-1*angle);
    //rotate(diff.heading());
    rect(-15, -15, 30, 30);
    popMatrix();
  }
  
}
