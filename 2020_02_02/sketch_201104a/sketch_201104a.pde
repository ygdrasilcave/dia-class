Pendulum p1;
Pendulum p2;

void setup(){
  size(1000, 1000);
  p1 = new Pendulum(new PVector(width/2, 20), 100);
  p2 = new Pendulum(new PVector(width/2, 200), 600);
}

void draw(){
  background(255);
  p1.update();
  p2.update();
  p1.display();
  p2.display();
  //println(p.location);
}
