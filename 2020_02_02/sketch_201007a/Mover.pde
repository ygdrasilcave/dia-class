class Mover {
  PVector position;
  PVector velocity;
  PVector acceleration;

  float mass;

  Mover(float _mass) {
    position = new PVector(width/2, height/2);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    mass = _mass;
  }

  void applyForce(PVector _force) {
    PVector f = PVector.div(_force, mass);
    acceleration.add(f);
  }

  void update() {
    velocity.add(acceleration);
    position.add(velocity);
    acceleration.mult(0);
  }

  void display(boolean _i) {
    if (_i == true) {
      fill(0);
      ellipse(position.x, position.y, 100, 100);
    } else {
      fill(0);
      rect(position.x-50, position.y-50, 100, 100);
    }
  }
}
