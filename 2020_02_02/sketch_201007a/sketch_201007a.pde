Mover m1;
Mover m2;

void setup() {
  size(1200, 800);
  m1 = new Mover(1);
  m2 = new Mover(100);
}

void draw() {
  background(255);
  PVector wind = new PVector(-0.01, 0);
  PVector gravity = new PVector(0, 0.098);

  if (mousePressed) {
    m1.applyForce(wind);
    m1.applyForce(gravity);
    m2.applyForce(wind);
    m2.applyForce(gravity);
  }
  m1.update();
  m1.display(true);
  m2.update();
  m2.display(false);
}
