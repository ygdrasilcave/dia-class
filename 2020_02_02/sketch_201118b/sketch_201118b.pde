void setup(){
  PVector xAxis = new PVector(100, 25);
  PVector yAxis = new PVector(30, 100);
  PVector v = new PVector(50, 50);
  
  println(xAxis.dot(v));
  println(yAxis.dot(v));
  
  xAxis.normalize();
  yAxis.normalize();
  
  println(xAxis.dot(v));
  println(yAxis.dot(v));
  
  println(degrees(acos(xAxis.dot(v)/v.mag())));
  println(degrees(acos(yAxis.dot(v)/v.mag())));
  
  println(degrees(PVector.angleBetween(xAxis, v)));
  println(degrees(PVector.angleBetween(yAxis, v)));
}
