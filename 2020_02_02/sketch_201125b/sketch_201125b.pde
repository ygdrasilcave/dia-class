ArrayList<Vehicle> v;
Path p;

boolean showPath = true;

void makePath() {
  float t = 0;
  noiseSeed(millis());
  for (int i=0; i<=10; i++) {
    p.addPoint(i*width/10, noise(t)*height);
    t += 0.25;
  }
}

void setup() {
  size(1000, 600);
  p = new Path();
  makePath();

  v = new ArrayList<Vehicle>();
  v.add(new Vehicle(new PVector(0, height/2), 4, 0.25));
}

void draw() {
  background(255);
  if (showPath == true) {
    p.display();
  }

  //PVector target = new PVector(mouseX, mouseY);
  //v.seek(target);
  for (Vehicle _v : v) {
    _v.follow(p);
    _v.update();
    _v.display();
  }

  if (mousePressed) {
    v.add(new Vehicle(new PVector(mouseX, mouseY), random(1, 4), random(0.03, 0.25)));
  }
}

void keyPressed() {
  if (key == ' ') {
    p.clearPoints();
    makePath();
  }

  if (key == 'n') {
    v.add(new Vehicle(new PVector(random(width), random(height)), random(1, 4), random(0.03, 0.25)));
  }

  if (key == 'p') {
    showPath = !showPath;
  }
}
