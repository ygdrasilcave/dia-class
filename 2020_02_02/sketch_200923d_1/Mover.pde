class Mover {
  PVector location;
  PVector velocity;
  PVector acc;
  float maxVel;
  float ballSize;

  PVector target;
  int currentTime;
  int interval;
  float legLength;
  float legLengthMax;
  int legNum;
  float t = 0;
  float tSpeed;

  Mover() {
    location = new PVector(width/2, height/2);
    velocity = new PVector(0, 0);
    acc = new PVector(0, 0);
    target = new PVector(random(width), random(height));
    currentTime = millis();
    interval = int(random(200, 1000));

    ballSize = random(10, 40);
    maxVel = random(1, 4);

    currentTime = 0;
    
    legLengthMax = random(3, 15);
    legNum = int(random(3, 16));
    tSpeed = random(0.025, 0.16);
  }

  void newTarget() {
    if (currentTime + interval < millis()) {
      target.set(random(-200, width+200), random(-200, height+200));
      currentTime = millis();
      interval = int(random(200, 1000));
    }
  }

  void update() {
    PVector dir = PVector.sub(target, location);

    if (dir.mag() < ballSize/5) {
      /*dir.normalize();
      dir.mult(0);
      acc = dir;
      velocity.add(acc);*/
      velocity.mult(0.85);
      
      //velocity.set(dir.mult(0.85));
    } else {
      dir.normalize();
      dir.mult(1.5);
      acc = dir;
      velocity.add(acc);
    }

    velocity.limit(maxVel);
    location.add(velocity);

    /*if (location.x > width + ballSize/2) {
      location.x = -ballSize/2;
    } else if (location.x < -ballSize/2) {
      location.x = width + ballSize/2;
    }

    if (location.y > height + ballSize/2) {
      location.y = -ballSize/2;
    } else if (location.y < -ballSize/2) {
      location.y = height + ballSize/2;
    }*/
  }

  void display(boolean _showTarget) {
    noFill();
    stroke(0);
    ellipse(location.x, location.y, ballSize, ballSize);
    for(float i=0; i<TWO_PI; i+=(TWO_PI/legNum)){
      float x1 = location.x + cos(i)*(ballSize/2);
      float y1 = location.y + sin(i)*(ballSize/2);
      float x2 = location.x + cos(i)*(ballSize/2+legLength);
      float y2 = location.y + sin(i)*(ballSize/2+legLength);
      stroke(0);
      line(x1, y1, x2, y2);
      fill(0);
      noStroke();
      ellipse(x2, y2, ballSize*0.15, ballSize*0.15);
    }
    legLength = map(cos(t), -1, 1, 0, 1)*legLengthMax;
    t += tSpeed;

    if (_showTarget == true) {
      fill(255, 0, 0);
      noStroke();
      ellipse(target.x, target.y, ballSize*0.25, ballSize*0.25);
    }
  }
}
