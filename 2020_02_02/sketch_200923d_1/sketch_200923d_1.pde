Mover[] m;
boolean showTarget = true;
boolean move = true;

void setup() {
  size(1200, 800);
  m = new Mover[200];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover();
  }
}

void draw() {
  background(255);
  for (int i=0; i<m.length; i++) {
    if (move == true) {
      m[i].newTarget();
    }
    m[i].update();
    m[i].display(showTarget);
  }
}

void keyPressed() {
  if (key == 's') {
    showTarget = !showTarget;
  }
  if(key == 'm'){
    move = !move;
  }
}
