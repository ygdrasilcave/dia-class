float angle = 0.0;
float angleSpeed = 1;

void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angle));
  line(200, 0, -200, 0);
  ellipse(200, 0, 30, 30);
  ellipse(-200, 0, 30, 30);
  popMatrix();
  
  angle += angleSpeed;
}
