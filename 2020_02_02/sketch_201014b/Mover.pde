class Mover{
  PVector location;
  PVector velocity;
  PVector acceleration;
  
  float mass;
  
  Mover(float _mass, float _x, float _y){
    location = new PVector(_x, _y);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    mass = _mass;
  }
  
  void applyForce(PVector force){
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }
  
  void update(){
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
    
    if(location.x > width){
      location.x = width;
      velocity.x = velocity.x * -1;
    }else if(location.x < 0){
      location.x = 0;
      velocity.x = velocity.x * -1;
    }
    
    if(location.y > height){
      location.y = height;
      velocity.y = velocity.y * -1;
    }else if(location.y < 0){
      location.y = 0;
      velocity.y = velocity.y * -1;
    }
  }
  
  void display(int _i){
    //fill(0);
    noFill();
    stroke(0);
    ellipse(location.x, location.y, mass*20, mass*20);
    
    fill(0);
    noStroke();
    text(_i, location.x, location.y);
  }
}
