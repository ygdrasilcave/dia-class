Mover[] m = new Mover[10];

float liquid_x;
float liquid_y;
float liquid_w;
float liquid_h;

void setup() {
  size(1200, 800);
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(2, random(width), 100);
  }

  background(255);
  textAlign(CENTER, CENTER);
  textSize(20);

  liquid_x = 400;
  liquid_y = 550;
  liquid_w = 300;
  liquid_h = height - liquid_y;
}

void draw() {
  background(255);

  fill(0, 30);
  rect(liquid_x, liquid_y, liquid_w, liquid_h);

  PVector gravity = new PVector(0, 0.125);

  for (int i=0; i<m.length; i++) {

    m[i].applyForce(gravity);

    PVector pos = m[i].location.copy();
    if (pos.y > liquid_y && pos.y < (liquid_y+liquid_h) && pos.x > liquid_x && pos.x < (liquid_x+liquid_w)) {
      //float c = 0.068;
      float c = 0.5;
      float speed = m[i].velocity.mag();
      float dragMag = speed*speed*c;    
      PVector dragForce = m[i].velocity.copy();
      dragForce.normalize();
      dragForce.mult(-1);    
      dragForce.mult(dragMag);
      m[i].applyForce(dragForce);
    }

    m[i].update();
    m[i].display(i);
  }
}

void mouseReleased() {
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(2, random(width), 100);
  }
}
