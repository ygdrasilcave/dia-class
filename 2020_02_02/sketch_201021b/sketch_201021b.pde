Mover[] m;

void setup() {
  size(800, 800);
  m = new Mover[20];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(random(width), random(height), random(0.1, 0.5));
  }
}

void draw() {
  background(255);
  for (int i=0; i<m.length; i++) {
    m[i].update();
    m[i].display();
  }
}
