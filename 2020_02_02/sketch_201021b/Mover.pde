class Mover{
  PVector position;
  PVector acc;
  PVector vel;
  float size;
  float maxSpeed;
  
  Mover(float _x, float _y, float _s){    
    position = new PVector(_x, _y);
    acc = new PVector(0, 0);
    vel = new PVector(0, 0);   
    size = _s;
    maxSpeed = 8*size;
  }
  
  void update(){
    PVector target = new PVector(mouseX, mouseY);
    PVector dist = PVector.sub(target, position);
    dist.normalize();
    dist.mult(0.05);
    acc = dist;
    vel.add(acc);
    vel.limit(maxSpeed);
    position.add(vel);
    
    if(position.x > width){
      position.x = 0;
    }else if(position.x < 0){
      position.x = width;
    }
    if(position.y > height){
      position.y = 0;
    }else if(position.y < 0){
      position.y = height;
    }
  }
  
  void display(){
    //float angle = vel.heading();
    float angle = atan2(vel.y, vel.x);
    
    pushMatrix();
    translate(position.x, position.y);
    rotate(angle);
    fill(0);
    ellipse((100*size)*0.5 + (20*size)*0.5, -(50*size)*0.5, 20*size, 20*size);
    ellipse((100*size)*0.5 + (20*size)*0.5, (50*size)*0.5, 20*size, 20*size);
    rect(-(100*size)*0.5, -(50*size)*0.5, 100*size, 50*size);
    popMatrix();
  }
  
}
