String[] data;
int[] bus;
int maxVal = 0;
float w;

PFont font;
String[] monthString;
String[] index;
boolean toggle = true;
int counter = 2;

void setup() {
  size(1600, 600);
  font = createFont("SeoulHangangEB.ttf", 32);
  data = loadStrings("trans.csv");
  bus = new int[12];
  monthString = trim(data[0].split(","));
  index = new String[3]; 
  update();
  textFont(font);
  textSize(30);
  w = width/bus.length;
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0);
  
  textSize(28);
  for (int i=0; i<bus.length; i++) {
    float scaleVal = map(bus[i], 0, maxVal, 0, TWO_PI);
    float scaleVal2 = map(bus[i], 0, maxVal, 0, w);
    
    fill(255);
    arc(i*w+w/2, 2*height/3, w, w, 0, scaleVal, PIE);
    noFill();
    stroke(255);
    strokeWeight(3);
    ellipse(i*w+w/2, height/3, scaleVal2, scaleVal2);

    fill(255);
    text(monthString[i+3], i*w+w/2, height-100);
  }
  
  text(index[0], width/2, 30);
  text(index[1], width/2, 70);
  text(index[2], width/2, 110);

  if (frameCount%10 == 0) {
    if (toggle == true) {
      maxVal = 0;
      counter++;
      if (counter > data.length-1) {
        counter = 2;
      }
      update();
      toggle = false;
    }
  } else {
    toggle = true;
  }
}

void update() {
  println(data[counter]);
  String[] busDataString = trim(data[counter].split(","));
  for (int i=0; i<bus.length; i++) {
    bus[i] = int(busDataString[i+3]);
    if (maxVal < bus[i]) {
      maxVal = bus[i];
    }
  }
  for (int i=0; i<index.length; i++) {
    index[i] = busDataString[i];
  }
}

void keyPressed() {
  if (key == ' ') {
    maxVal = 0;
    counter++;
    if (counter > data.length-1) {
      counter = 2;
    }
    update();
  }
}