XML xml;
PFont kr;
StringList city;
IntList tempLow0, tempLow1, tempLow2;
IntList tempHigh0, tempHigh1, tempHigh2;
StringList condition0, condition1, condition2;
StringList date0, date1, date2;
String today = "";
int min, max;
color lowCol0, lowCol1, lowCol2, highCol0, highCol1, highCol2;

float topMargin = 200;
float bottomMargin = 100;

boolean toggle0 = true;
boolean toggle1 = true;
boolean toggle2 = true;

import themidibus.*;
MidiBus myBus;
int channel = 0;
int pitch = 60;
int velocity = 127;
import oscP5.*;
import netP5.*;
OscP5 oscP5;
NetAddress myRemoteLocation;

int step = 0;
int currTime;
int onTime = 200;
boolean noteOff = false;
int offTime;
int whichData = 0;

int freq = 0;
boolean lowOrHigh = false;

void setup() {
  size(1600, 800);
  kr = createFont("SeoulHangangEB.ttf", 42);

  MidiBus.list();
  myBus = new MidiBus(this, -1, "LoopBe Internal MIDI");
  oscP5 = new OscP5(this, 12000);
  myRemoteLocation = new NetAddress("127.0.0.1", 9000);

  currTime = millis();

  city = new StringList();
  tempLow0 = new IntList();
  tempHigh0 = new IntList();
  condition0 = new StringList();
  date0 = new StringList();
  tempLow1 = new IntList();
  tempHigh1 = new IntList();
  condition1 = new StringList();
  date1 = new StringList();
  tempLow2 = new IntList();
  tempHigh2 = new IntList();
  condition2 = new StringList();
  date2 = new StringList();

  lowCol0 = color(0, 0, 255, 200);
  lowCol1 = color(26, 255, 253, 200);
  lowCol2 = color(255, 219, 13, 200);
  highCol0 = color(255, 0, 0, 200);
  highCol1 = color(214, 13, 255, 200);
  highCol2 = color(219, 94, 99, 200);

  parsingXML("mid-term-rss3.xml");
}

void draw() {
  background(0);

  fill(255);
  noStroke();
  textSize(20);
  text(today, width/2, 50);
  text(city.get(0), 100, 25*1);
  text(city.get(1), 100, 25*2);
  text(city.get(2), 100, 25*3);

  textSize(16);
  for (int i=0; i<date0.size(); i++) {
    text(date0.get(i), width/date0.size()/2 + width/date0.size()*i, height-50);
  }

  fill(lowCol0);
  ellipse(150, 25*1, 20, 20);
  fill(highCol0);
  ellipse(180, 25*1, 20, 20);

  fill(lowCol1);
  ellipse(150, 25*2, 20, 20);
  fill(highCol1);
  ellipse(180, 25*2, 20, 20);

  fill(lowCol2);
  ellipse(150, 25*3, 20, 20);
  fill(highCol2);
  ellipse(180, 25*3, 20, 20);

  if (toggle0 == true) {
    drawLineChart(tempLow0, tempHigh0, condition0, lowCol0, highCol0, 3);
  }
  if (toggle1 == true) {
    drawLineChart(tempLow1, tempHigh1, condition1, lowCol1, highCol1, 2);
  }
  if (toggle2 == true) {
    drawLineChart(tempLow2, tempHigh2, condition2, lowCol2, highCol2, 1);
  }

  int dataNum = tempLow0.size();
  float startX = width/dataNum/2;
  float cellW = width/dataNum;
  strokeWeight(5);
  stroke(0, 255, 255);
  line(startX+cellW*step, 0, startX+cellW*step, height);
  onTime = int(map(mouseX, 0, width, 60, 1200));

  if (currTime+onTime < millis()) {
    if (whichData == 0) {
      pitch = int(map(tempLow0.get(step), min, max, 66, 96));
      freq = int(map(tempHigh0.get(step), min, max, 66, 400));
    } else if (whichData == 1) {
      pitch = int(map(tempLow1.get(step), min, max, 66, 96));
      freq = int(map(tempHigh1.get(step), min, max, 66, 400));
    } else if (whichData == 2) {
      pitch = int(map(tempLow2.get(step), min, max, 66, 96));
      freq = int(map(tempHigh2.get(step), min, max, 66, 400));
    }
    velocity = int(map(mouseY, 0, height, 127, 0));
    if (lowOrHigh == true) {
      myBus.sendNoteOn(channel, pitch, velocity);
    } else {
      myBus.sendNoteOn(channel, freq, velocity);
    }
    sendOSCMsg(freq);
    currTime = millis();
    noteOff = true;
  }
  if (noteOff == true) {
    if (currTime+(onTime-50) < millis()) {
      myBus.sendNoteOff(channel, pitch, velocity);
      noteOff = false;
      step++;
      if (step > dataNum-1) {
        step = 0;
        whichData = int(random(3));
        lowOrHigh = !lowOrHigh;
      }
    }
  }
}

void sendOSCMsg(int _freq) {
  OscMessage myMessage = new OscMessage("/test");  
  myMessage.add(_freq);
  oscP5.send(myMessage, myRemoteLocation);
}

void keyPressed() {
  if (key == '1') {
    toggle0 = !toggle0;
  }
  if (key == '2') {
    toggle1 = !toggle1;
  }
  if (key == '3') {
    toggle2 = !toggle2;
  }
  if (key == 'q') {
    whichData = 0;
  } else if (key == 'w') {
    whichData = 1;
  } else if (key == 'e') {
    whichData = 2;
  }
}