void drawLineChart(IntList _low, IntList _high, StringList _condition, color _rowCol, color _highCol, int _j) {
  textSize(16);
  int dataNum = _low.size();
  float startX = width/dataNum/2;
  float cellW = width/dataNum;

  for (int i=0; i<dataNum; i++) {
    float y = map(_low.get(i), min, max, height-bottomMargin, topMargin);
    fill(_rowCol);
    noStroke();
    ellipse(startX + cellW*i, y, 10, 10);
    fill(255);
    text(_low.get(i), startX + cellW*i, y-20);
    stroke(_rowCol);
    if (i<_low.size()-1) {
      line(startX + cellW*i, y, 
        startX + cellW*(i+1), map(_low.get(i+1), min, max, height-bottomMargin, topMargin));
    }

    y = map(_high.get(i), min, max, height-bottomMargin, topMargin);
    fill(_highCol);
    noStroke();
    ellipse(startX + cellW*i, y, 10, 10);
    fill(255);
    text(_high.get(i), startX + cellW*i, y-20);
    stroke(_highCol);
    if (i<_high.size()-1) {
      line(startX + cellW*i, y, 
        startX + cellW*(i+1), map(_high.get(i+1), min, max, height-bottomMargin, topMargin));
    }
    
    fill(255);
    noStroke();
    text(_condition.get(i), startX + cellW*i, topMargin - 25*_j -20);
  }
}

void parsingXML(String _xmlFile) {
  xml = loadXML(_xmlFile);
  XML[] weather = xml.getChild("channel").getChild("item").getChild("description").getChild("body").getChildren();
  today = xml.getChild("channel").getChild("pubDate").getContent();

  city.set(0, weather[1].getChild("city").getContent());
  city.set(1, weather[3].getChild("city").getContent());
  city.set(2, weather[5].getChild("city").getContent());
  textFont(kr);
  textAlign(CENTER, CENTER);
  XML[] data0 = weather[1].getChildren("data");
  XML[] data1 = weather[3].getChildren("data");
  XML[] data2 = weather[5].getChildren("data");
  if (data0.length == data1.length && data1.length ==  data2.length) {
    for (int i=0; i<data0.length; i++) {
      tempLow0.set(i, int(data0[i].getChild("tmn").getContent()));
      tempHigh0.set(i, int(data0[i].getChild("tmx").getContent()));
      condition0.set(i, data0[i].getChild("wf").getContent());
      date0.set(i, data0[i].getChild("tmEf").getContent());
      
      tempLow1.set(i, int(data1[i].getChild("tmn").getContent()));
      tempHigh1.set(i, int(data1[i].getChild("tmx").getContent()));
      condition1.set(i, data1[i].getChild("wf").getContent());
      date1.set(i, data1[i].getChild("tmEf").getContent());
      
      tempLow2.set(i, int(data2[i].getChild("tmn").getContent()));
      tempHigh2.set(i, int(data2[i].getChild("tmx").getContent()));
      condition2.set(i, data2[i].getChild("wf").getContent());
      date2.set(i, data2[i].getChild("tmEf").getContent());
    }
  }else{
    noLoop();
  }

  min = 100;
  max = 0;
  for (int i=0; i<tempLow0.size(); i++) {
    if (tempLow0.get(i) < min) {
      min = tempLow0.get(i);
    }
    if (tempLow1.get(i) < min) {
      min = tempLow1.get(i);
    }
    if (tempLow2.get(i) < min) {
      min = tempLow2.get(i);
    }
    if (tempHigh0.get(i) > max) {
      max = tempHigh0.get(i);
    }
    if (tempHigh1.get(i) > max) {
      max = tempHigh1.get(i);
    }
    if (tempHigh2.get(i) > max) {
      max = tempHigh2.get(i);
    }
  }
  println(min+"  :  "+max);
}