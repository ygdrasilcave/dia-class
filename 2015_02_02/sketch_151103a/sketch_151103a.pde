XML xml;
PFont kr;
StringList city;
IntList tempLow0, tempLow1, tempLow2;
IntList tempHigh0, tempHigh1, tempHigh2;
StringList condition0, condition1, condition2;
StringList date0, date1, date2;
String today = "";
int min, max;

void setup() {
  size(1600, 800);
  kr = createFont("SeoulHangangEB.ttf", 42);
  xml = loadXML("mid-term-rss3.xml");
  XML[] weather = xml.getChild("channel").getChild("item").getChild("description").getChild("body").getChildren();
  today = xml.getChild("channel").getChild("pubDate").getContent();

  //print(weather[1].getChild("city"));
  city = new StringList();
  //println(weather.length);
  city.set(0, weather[1].getChild("city").getContent());
  city.set(1, weather[3].getChild("city").getContent());
  city.set(2, weather[5].getChild("city").getContent());
  textFont(kr);
  textAlign(CENTER, CENTER);
  XML[] data0 = weather[1].getChildren("data");
  XML[] data1 = weather[3].getChildren("data");
  XML[] data2 = weather[5].getChildren("data");
  //println(data0[5].getChild("tmn"));
  tempLow0 = new IntList();
  tempHigh0 = new IntList();
  condition0 = new StringList();
  date0 = new StringList();
  for (int i=0; i<data0.length; i++) {
    tempLow0.set(i, int(data0[i].getChild("tmn").getContent()));
    tempHigh0.set(i, int(data0[i].getChild("tmx").getContent()));
    condition0.set(i, data0[i].getChild("wf").getContent());
    date0.set(i, data0[i].getChild("tmEf").getContent());
  }
  tempLow1 = new IntList();
  tempHigh1 = new IntList();
  condition1 = new StringList();
  date1 = new StringList();
  for (int i=0; i<data1.length; i++) {
    tempLow1.set(i, int(data1[i].getChild("tmn").getContent()));
    tempHigh1.set(i, int(data1[i].getChild("tmx").getContent()));
    condition1.set(i, data1[i].getChild("wf").getContent());
    date1.set(i, data1[i].getChild("tmEf").getContent());
  }
  tempLow2 = new IntList();
  tempHigh2 = new IntList();
  condition2 = new StringList();
  date2 = new StringList();
  for (int i=0; i<data2.length; i++) {
    tempLow2.set(i, int(data2[i].getChild("tmn").getContent()));
    tempHigh2.set(i, int(data2[i].getChild("tmx").getContent()));
    condition2.set(i, data2[i].getChild("wf").getContent());
    date2.set(i, data2[i].getChild("tmEf").getContent());
  }
  //println(today);
  min = 100;
  max = 0;
  for (int i=0; i<tempLow0.size(); i++) {
    if (tempLow0.get(i) < min) {
      min = tempLow0.get(i);
    }
    if (tempLow1.get(i) < min) {
      min = tempLow1.get(i);
    }
    if (tempLow2.get(i) < min) {
      min = tempLow2.get(i);
    }
    if (tempHigh0.get(i) > max) {
      max = tempHigh0.get(i);
    }
    if (tempHigh1.get(i) > max) {
      max = tempHigh1.get(i);
    }
    if (tempHigh2.get(i) > max) {
      max = tempHigh2.get(i);
    }
  }
  println(min+"  :  "+max);
}

void draw() {
  background(0);

  fill(255);
  noStroke();
  textSize(20);
  text(today, width/2, 50);
  text(city.get(0), 100, 50*1);
  text(city.get(1), 100, 50*2);
  text(city.get(2), 100, 50*3);

  textSize(16);
  for (int i=0; i<date0.size(); i++) {
    text(date0.get(i), width/date0.size()/2 + width/date0.size()*i, height-50);
  }

  fill(0, 0, 255, 200);
  ellipse(150, 50*1, 20, 20);
  fill(255, 0, 0, 200);
  ellipse(200, 50*1, 20, 20);
  
  fill(26, 255, 253, 200);
  ellipse(150, 50*2, 20, 20);
  fill(214, 13, 255, 200);
  ellipse(200, 50*2, 20, 20);
  
  fill(255, 219, 13, 200);
  ellipse(150, 50*3, 20, 20);
  fill(219, 94, 99, 200);
  ellipse(200, 50*3, 20, 20);

  textSize(16);
  for (int i=0; i<tempLow0.size(); i++) {
    fill(0, 0, 255, 200);
    noStroke();
    ellipse(width/tempLow0.size()/2 + width/tempLow0.size()*i, map(tempLow0.get(i), min, max, height-80, 150), 10, 10);
    fill(255);
    text(tempLow0.get(i), width/tempLow0.size()/2 + width/tempLow0.size()*i, map(tempLow0.get(i), min, max, height-80, 150)-20);
    stroke(0, 0, 255, 200);
    if (i<tempLow0.size()-1) {
      line(width/tempLow0.size()/2 + width/tempLow0.size()*i, map(tempLow0.get(i), min, max, height-80, 150), 
        width/tempLow0.size()/2 + width/tempLow0.size()*(i+1), map(tempLow0.get(i+1), min, max, height-80, 150));
    }
    fill(255, 0, 0, 200);
    noStroke();
    ellipse(width/tempHigh0.size()/2 + width/tempHigh0.size()*i, map(tempHigh0.get(i), min, max, height-80, 150), 10, 10);
    fill(255);
    text(tempHigh0.get(i), width/tempHigh0.size()/2 + width/tempHigh0.size()*i, map(tempHigh0.get(i), min, max, height-80, 150)-20);
    
    stroke(255, 0, 0, 200);
    if (i<tempHigh0.size()-1) {
      line(width/tempHigh0.size()/2 + width/tempHigh0.size()*i, map(tempHigh0.get(i), min, max, height-80, 150), 
        width/tempHigh0.size()/2 + width/tempHigh0.size()*(i+1), map(tempHigh0.get(i+1), min, max, height-80, 150));
    }
  }

  for (int i=0; i<tempLow1.size(); i++) {
    fill(26, 255, 253, 200);
    noStroke();
    ellipse(width/tempLow1.size()/2 + width/tempLow1.size()*i, map(tempLow1.get(i), min, max, height-80, 150), 10, 10);
    fill(255);
    text(tempLow1.get(i), width/tempLow1.size()/2 + width/tempLow1.size()*i, map(tempLow1.get(i), min, max, height-80, 150)-20);
    
    stroke(26, 255, 253, 200);
    if (i<tempLow1.size()-1) {
      line(width/tempLow1.size()/2 + width/tempLow1.size()*i, map(tempLow1.get(i), min, max, height-80, 150), 
        width/tempLow1.size()/2 + width/tempLow1.size()*(i+1), map(tempLow1.get(i+1), min, max, height-80, 150));
    }
    fill(214, 13, 255, 200);
    noStroke();
    ellipse(width/tempHigh1.size()/2 + width/tempHigh1.size()*i, map(tempHigh1.get(i), min, max, height-80, 150), 10, 10);
    fill(255);
    text(tempHigh1.get(i), width/tempHigh1.size()/2 + width/tempHigh1.size()*i, map(tempHigh1.get(i), min, max, height-80, 150)-20);
    
    stroke(214, 13, 255, 200);
    if (i<tempHigh1.size()-1) {
      line(width/tempHigh1.size()/2 + width/tempHigh1.size()*i, map(tempHigh1.get(i), min, max, height-80, 150), 
        width/tempHigh1.size()/2 + width/tempHigh1.size()*(i+1), map(tempHigh1.get(i+1), min, max, height-80, 150));
    }
  }

  for (int i=0; i<tempLow2.size(); i++) {
    fill(255, 219, 13, 200);
    noStroke();
    ellipse(width/tempLow2.size()/2 + width/tempLow2.size()*i, map(tempLow2.get(i), min, max, height-80, 150), 10, 10);
    fill(255);
    text(tempLow2.get(i), width/tempLow2.size()/2 + width/tempLow2.size()*i, map(tempLow2.get(i), min, max, height-80, 150)-20);
    
    stroke(255, 219, 13, 200);
    if (i<tempLow1.size()-1) {
      line(width/tempLow2.size()/2 + width/tempLow2.size()*i, map(tempLow2.get(i), min, max, height-80, 150), 
        width/tempLow2.size()/2 + width/tempLow2.size()*(i+1), map(tempLow2.get(i+1), min, max, height-80, 150));
    }
    fill(219, 94, 99, 200);
    noStroke();
    ellipse(width/tempHigh2.size()/2 + width/tempHigh2.size()*i, map(tempHigh2.get(i), min, max, height-80, 150), 10, 10);
    fill(255);
    text(tempHigh2.get(i), width/tempHigh2.size()/2 + width/tempHigh2.size()*i, map(tempHigh2.get(i), min, max, height-80, 150)-20);
    
    stroke(219, 94, 99, 200);
    if (i<tempHigh2.size()-1) {
      line(width/tempHigh2.size()/2 + width/tempHigh2.size()*i, map(tempHigh2.get(i), min, max, height-80, 150), 
        width/tempHigh2.size()/2 + width/tempHigh2.size()*(i+1), map(tempHigh2.get(i+1), min, max, height-80, 150));
    }
  }
}