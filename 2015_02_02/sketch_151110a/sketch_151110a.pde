import themidibus.*;
MidiBus myBus;
int channel = 0;
int pitch = 60;
int velocity = 127;
int time;
int interval = 100;
int offTime = 20;

boolean off = false;

import oscP5.*;
import netP5.*;
  
OscP5 oscP5;
NetAddress myRemoteLocation;

void setup() {
  size(400, 400);
  background(0);

  MidiBus.list();
  myBus = new MidiBus(this, -1, "Microsoft GS Wavetable Synth");
  time = millis();
  
  oscP5 = new OscP5(this,12000);
  myRemoteLocation = new NetAddress("127.0.0.1",9000);
}

void sendOSCMsg(){
  OscMessage myMessage = new OscMessage("/test");  
  myMessage.add(int(mouseX));
  myMessage.add(int(mouseY));
  oscP5.send(myMessage, myRemoteLocation);
}

void draw() {
  if (time+interval < millis()) {
    myBus.sendNoteOn(channel, int(random(60, 96)), int(random(20, 127)));
    time = millis();
    off = true;
    //println("on");
  }
  if (off == true) {
    if (time+offTime < millis()) {
      myBus.sendNoteOff(channel, pitch, velocity);
      //println("off");
      pitch++;
      if(pitch > 96){
        pitch = 60;
      }
      off = false;
    }
  }
  sendOSCMsg();
}