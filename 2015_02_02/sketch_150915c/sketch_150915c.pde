String[] data;
int[] bus;
int maxVal = 0;
float w;

PFont font;
String[] monthString;
String[] index;
boolean toggle = true;
int counter = 2;

void setup() {
  size(1000, 1000);
  font = createFont("SeoulHangangEB.ttf", 32);
  data = loadStrings("trans.csv");
  bus = new int[12];
  monthString = trim(data[0].split(","));
  index = new String[3]; 
  update();
  textFont(font);
  textSize(30);
  w = width/bus.length;
  textAlign(CENTER, CENTER);
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 20);
  rect(0, 0, width, height);
  
  textSize(28);
  textAlign(CENTER, CENTER);
  
  float step = TWO_PI/bus.length;
  for (int i=0; i<bus.length; i++) {
    float x2 = width/2 + cos(i*step)*(width/2-30);
    float y2 = height/2 + sin(i*step)*(width/2-30);
    stroke(110);
    strokeWeight(1);
    line(width/2, height/2, x2, y2);
    noStroke();
    fill(255);
    text(monthString[i+3], x2, y2);

    float scaleVal = map(bus[i], 0, maxVal, 0, width/2-80);

    float x = width/2 + cos(i*step)*scaleVal;
    float y = height/2 + sin(i*step)*scaleVal;
    
    fill(255);
    noStroke();
    ellipse(x, y, 10, 10);
    stroke(255);
    strokeWeight(3);
    line(width/2, height/2, x, y);

    if (i>0) {
      float scaleVal3 = map(bus[i-1], 0, maxVal, 0, width/2-80);
      float x3 = width/2 + cos((i-1)*step)*scaleVal3;
      float y3 = height/2 + sin((i-1)*step)*scaleVal3;
      strokeWeight(1);
      line(x3, y3, x, y);
    } else {
      float scaleVal3 = map(bus[11], 0, maxVal, 0, width/2-80);
      float x3 = width/2 + cos((11)*step)*scaleVal3;
      float y3 = height/2 + sin((11)*step)*scaleVal3;
      strokeWeight(1);
      line(x3, y3, x, y);
    }
  }

  textAlign(LEFT, CENTER);
  text(index[0], 20, 30);
  text(index[1], 20, 70);
  text(index[2], 20, 110);

  if (frameCount%10 == 0) {
    if (toggle == true) {
      maxVal = 0;
      counter++;
      if (counter > data.length-1) {
        counter = 2;
      }
      update();
      toggle = false;
    }
  } else {
    toggle = true;
  }
}

void update() {
  println(data[counter]);
  String[] busDataString = trim(data[counter].split(","));
  for (int i=0; i<bus.length; i++) {
    bus[i] = int(busDataString[i+3]);
    if (maxVal < bus[i]) {
      maxVal = bus[i];
    }
  }
  for (int i=0; i<index.length; i++) {
    index[i] = busDataString[i];
  }
}

void keyPressed() {
  if (key == ' ') {
    maxVal = 0;
    counter++;
    if (counter > data.length-1) {
      counter = 2;
    }
    update();
  }
}