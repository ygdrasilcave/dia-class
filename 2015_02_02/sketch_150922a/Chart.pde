class Chart {
  int[] bus;
  int maxVal = 0;
  String[] index;
  int myMaxVal = 0;
  boolean myStyle;

  Chart(String _data, boolean _style) {
    String[] busDataString = trim(_data.split(","));

    if (_style == true) {
      bus = new int[12];
      index = new String[3];
      for (int i=0; i<bus.length; i++) {
        bus[i] = int(busDataString[i+3]);
        if (maxVal < bus[i]) {
          maxVal = bus[i];
        }
      }
      for (int i=0; i<index.length; i++) {
        index[i] = busDataString[i];
      }
      myMaxVal = maxVal;
    } else {
      bus = new int[12];
      index = new String[12];
      for (int i=0; i<index.length; i++) {
        index[i] = busDataString[i+3];
      }
    }
    myStyle = _style;
  }

  void update(int _mval) {
    maxVal = _mval;
  }

  void update() {
    maxVal = myMaxVal;
  }

  void display(float _cx, float _cy, float _w, float _h) {
    float step = TWO_PI/bus.length;
    strokeWeight(0.85);
    stroke(125);
    for (int i=0; i<bus.length; i++) {
      float x = _cx + cos(i*step)*(_w/2);
      float y = _cy + sin(i*step)*(_w/2);
      line(_cx, _cy, x, y);
    }
    if (myStyle == true) {
      noFill();
      stroke(255);
      strokeWeight(2);
      beginShape();
      for (int i=0; i<bus.length; i++) {
        float scaleVal = map(bus[i], 0, maxVal, 0, _w/2);
        float x = _cx + cos(i*step)*scaleVal;
        float y = _cy + sin(i*step)*scaleVal;
        vertex(x, y);
      }
      endShape(CLOSE);

      textSize(30);
      fill(255);
      noStroke();
      text(index[0], _cx, _cy-35);
      text(index[1], _cx, _cy);
      text(index[2], _cx, _cy+35);
    } else {
      fill(255);
      textSize(16);
      for (int i=0; i<index.length; i++) {
        float x = _cx + cos(i*step)*(_w/2-20);
        float y = _cy + sin(i*step)*(_w/2-20);
        text(index[i], x, y);
      }
    }
  }
}