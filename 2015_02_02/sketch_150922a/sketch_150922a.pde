String[] data;
PFont font;
boolean toggle = true;
float chartSize;
float pad = 10;
int numCols = 6;
int numRows;
float margin;
int gmaxVal = 0;

Chart[] transChart;
Chart indexChart;

int counter = 0;

boolean view = true;

void setup() {
  size(1600, 1000);
  font = createFont("SeoulHangangEB.ttf", 32);
  data = loadStrings("trans.csv");  
  indexChart = new Chart(data[0], false);
  transChart = new Chart[data.length-2];
  for (int i=0; i<transChart.length; i++) {
    transChart[i] = new Chart(data[i+2], true);
    if (gmaxVal < transChart[i].maxVal) {
      gmaxVal = transChart[i].maxVal;
    }
  }
  chartSize = width/numCols - pad*2;
  textFont(font);
  textSize(30);
  textAlign(CENTER, CENTER);
  numRows = int(transChart.length/numCols);
  if (transChart.length%numCols > 0) {
    numRows += 1;
  }
  margin = (width%chartSize)/2;
}

void draw() {
  background(0);
  if (view == true) {
    int counter = 0;
    for (int y=0; y<numRows; y++) {
      for (int x=0; x<numCols; x++) {
        if (counter < transChart.length) {
          transChart[counter].display(margin+chartSize/2+chartSize*x, chartSize/2+chartSize*y, chartSize-pad, chartSize-pad);
        } else if (counter == transChart.length) {
          indexChart.display(margin+chartSize/2+chartSize*x, chartSize/2+chartSize*y, chartSize-pad, chartSize-pad);
        }
        counter++;
      }
    }
  } else {
    transChart[counter].display(width/2, height/2, width/2, height/2);
  }
}

void keyPressed() {
  if (key == ' ') {
    toggle = !toggle;
    if (toggle == true) {
      for (int i=0; i<transChart.length; i++) {
        transChart[i].update(gmaxVal);
      }
    } else {
      for (int i=0; i<transChart.length; i++) {
        transChart[i].update();
      }
    }
  }
  if (key == 'n') {
    counter++;
    if (counter > transChart.length-1) {
      counter = 0;
    }
  }
  if(key == 'v'){
    view = !view;
  }
}