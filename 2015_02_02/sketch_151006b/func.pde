void parsingXML() {
  xml = loadXML("http://xml.weather.yahoo.com/forecastrss/KSXX00"+loc[locNum]+".xml");  
  location = xml.getChild("channel").getChild("yweather:location").getString("city");
  date = xml.getChild("channel").getChild("lastBuildDate").getContent();
  condition = xml.getChild("channel").getChild("item").getChild("yweather:condition").getString("text");
  temp = xml.getChild("channel").getChild("item").getChild("yweather:condition").getString("temp");
  float FtoC = (float(temp)-32.0)/1.8;
  temp = nf(FtoC, 2, 1);
  humidity = xml.getChild("channel").getChild("yweather:atmosphere").getString("humidity");
  sunrise = xml.getChild("channel").getChild("yweather:astronomy").getString("sunrise");
  sunset = xml.getChild("channel").getChild("yweather:astronomy").getString("sunset");

  XML[] forecast = xml.getChild("channel").getChild("item").getChildren("yweather:forecast");  
  tempLow = new StringList();
  tempHigh = new StringList();
  foreDate = new StringList();
  conditions = new StringList();
  forecastNum = forecast.length;
  for (int i=0; i<forecastNum; i++) {
    String low = forecast[i].getString("low");
    float FtoC_low = (float(low)-32.0)/1.8;
    low = nf(FtoC_low, 2, 1);
    String high = forecast[i].getString("high");
    float FtoC_high = (float(high)-32.0)/1.8;
    high = nf(FtoC_high, 2, 1);
    tempLow.set(i, low);
    tempHigh.set(i, high);
    foreDate.set(i, forecast[i].getString("day")+", "+forecast[i].getString("date"));
    conditions.set(i, forecast[i].getString("text"));
  }
}