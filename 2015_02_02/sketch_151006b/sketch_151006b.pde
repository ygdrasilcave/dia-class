XML xml;
//seoul, wonju, busan, jeju
String[] loc = {"37", "38", "50", "54"};
int locNum = 0;

String location = "";
String date = "";
String condition = "";
String temp = "";
String humidity = "";
String sunrise = "";
String sunset = "";
StringList tempLow;
StringList tempHigh;
StringList foreDate;
StringList conditions;

int forecastNum = 0;
PFont font;
float topMargin = 60;
float lineSpace = 50;
float txMaxLow = 0;
float txMaxHigh = 0;
float txMaxDate = 0;
float txMaxCondition = 0;
boolean reloadToggle = true;

void setup() {
  size(500, 500);
  background(0);
  parsingXML();
  font = createFont("SeoulHangangEB.ttf", 38);
  textFont(font);
  textAlign(CENTER, BOTTOM);
  strokeCap(SQUARE);
}

void draw() {
  background(0);
  
  stroke(20);
  noFill();
  String[] sunriseSP = splitTokens(sunrise, " :");
  String[] sunsetSP = splitTokens(sunset, " :");
  float sunriseTime = float(sunriseSP[0])*60+float(sunriseSP[1]);
  float sunsetTime = (float(sunsetSP[0])+12)*60+float(sunsetSP[1]);
  //println(sunriseTime+" "+sunsetTime+" "+(hour()*60+minute()));
  strokeWeight(16);
  arc(width/2, height/2, 200, 200, 0, PI);
  arc(width/2, height/2, 160, 160, 0, PI);
  arc(width/2, height/2, 200, 200, PI, TWO_PI);
  stroke(255, 188, 0);
  arc(width/2, height/2, 200, 200, PI, map(hour()*60+minute(), sunriseTime, sunsetTime, PI, TWO_PI));
  stroke(255, 72, 106);
  arc(width/2, height/2, 200, 200, 0, constrain(map(float(temp), float(tempLow.get(0)), float(tempHigh.get(0)), 0, PI), 0, PI));
  stroke(0, 216, 222);
  arc(width/2, height/2, 160, 160, 0, map(float(humidity), 0, 100, 0, PI));
  
  textAlign(CENTER, BOTTOM);
  noStroke();
  fill(255);
  textSize(36);
  text(location+"  "+hour() + " : " + nf(minute(), 2) + " : " + nf(second(), 2), width/2, topMargin);
  textSize(20);
  text(date, width/2, topMargin+lineSpace);
  textSize(30);
  text(condition+"  "+temp+"C  "+humidity+"%", width/2, topMargin+lineSpace*2);
  textSize(22);
  text("|^| "+sunrise+"  "+"|_| "+sunset, width/2, topMargin+lineSpace*3);
  
  textAlign(LEFT, BOTTOM);
  textSize(18);
  for (int i=0; i<forecastNum; i++) {
    if (textWidth(foreDate.get(i)) > txMaxDate) {
      txMaxDate = textWidth(foreDate.get(i));
    }
  }
  for (int i=0; i<forecastNum; i++) {
    if (textWidth(conditions.get(i)) > txMaxCondition) {
      txMaxCondition = textWidth(conditions.get(i));
    }
  }
  for (int i=0; i<forecastNum; i++) {
    if (textWidth(tempHigh.get(i)+"C") > txMaxHigh) {
      txMaxHigh = textWidth(tempHigh.get(i)+"C");
    }
  }
  for (int i=0; i<forecastNum; i++) {
    if (textWidth(tempLow.get(i)+"C") > txMaxLow) {
      txMaxLow = textWidth(tempLow.get(i)+"C");
    }
  }
  fill(255);
  noStroke();
  pushMatrix();
  translate(width/2, topMargin+lineSpace*4);
  pushMatrix();
  translate(-(txMaxDate+txMaxCondition+txMaxLow+txMaxHigh+60)/2, 0);
  for (int i=0; i<forecastNum; i++) {
    text(foreDate.get(i), 0, lineSpace*i);
    text(conditions.get(i), txMaxDate+20, lineSpace*i);
    text(tempLow.get(i)+"C", txMaxDate+txMaxCondition+40, lineSpace*i);
    text(tempHigh.get(i)+"C", txMaxDate+txMaxCondition+txMaxLow+60, lineSpace*i);
  }
  popMatrix();
  popMatrix();

  /*if (second() == 59) {
   if (reloadToggle == true) {
   parsingXML();
   reloadToggle = false;
   println("reload");
   }
   } else {
   if (reloadToggle == false) {
   reloadToggle = true;
   }
   }*/
  if (second()%10 == 0) {
    if (reloadToggle == true) {
      locNum++;
      if (locNum > loc.length-1) {
        locNum = 0;
      }
      parsingXML();
      reloadToggle = false;
      println("reload");
    }
  } else {
    if (reloadToggle == false) {
      reloadToggle = true;
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    locNum++;
    if (locNum > loc.length-1) {
      locNum = 0;
    }
    parsingXML();
  }
}