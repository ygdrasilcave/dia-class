String[] data;
int[] bus;
int maxVal = 0;
float w;

PFont font;

String[] monthString;
String[] index;
boolean toggle = true;
int counter = 2;

void setup() {
  size(900, 600);
  font = createFont("SeoulHangangEB.ttf", 32);
  data = loadStrings("trans.csv");
  bus = new int[12];
  w = (width-100)/bus.length;
  monthString = trim(data[0].split(","));
  index = new String[3]; 
  update();
  textFont(font);
  textSize(30);
}

void draw() {
  //background(0);
  fill(0, 10);
  noStroke();
  rect(100,0,width-100,height-50);
  fill(0);
  rect(0,0,100,height);
  rect(0,height-50,width,50);
  
  textSize(28);
  for (int i=0; i<12; i++) {
    //float scaleVal = map(bus[i], 0, maxVal, 0, height-50);
    float scaleVal = map(bus[i], 0, maxVal, height-50, 0);
    noStroke();
    //fill(0);
    //rect(100+i*w, height-scaleVal-50, w-10, scaleVal);
    fill(255);
    ellipse(100+i*w+w/2, scaleVal, 10, 10);
    strokeWeight(3);
    stroke(255);
    if (i<11) {
      float scaleVal1 = map(bus[i], 0, maxVal, height-50, 0);
      float scaleVal2 = map(bus[i+1], 0, maxVal, height-50, 0);
      line(100+i*w+w/2, scaleVal1, 100+(i+1)*w+w/2, scaleVal2);
    }
    if (scaleVal < 150) {
      fill(255);
    } else {
      fill(0);
    }
    fill(255);
    pushMatrix();
    translate(140+i*w, height-80);
    rotate(radians(-90));
    text(bus[i], 0, 0);
    popMatrix();

    fill(255);
    text(monthString[i+3], 100+i*w, height-20);
  }

  textSize(38);
  fill(255);
  pushMatrix();
  translate(50, height-50);
  rotate(radians(-90));
  text(index[0], 0, 0);
  popMatrix();
  pushMatrix();
  translate(50, height-150);
  rotate(radians(-90));
  text(index[1], 0, 0);
  popMatrix();
  pushMatrix();
  translate(50, height-300);
  rotate(radians(-90));
  text(index[2], 0, 0);
  popMatrix();

  if (frameCount%20 == 0) {
    if (toggle == true) {
      maxVal = 0;
      counter++;
      if (counter > data.length-1) {
        counter = 2;
      }
      update();
      toggle = false;
    }
  } else {
    toggle = true;
  }
}

void update() {
  println(data[counter]);
  String[] busDataString = trim(data[counter].split(","));
  for (int i=0; i<bus.length; i++) {
    bus[i] = int(busDataString[i+3]);
    if (maxVal < bus[i]) {
      maxVal = bus[i];
    }
  }
  for (int i=0; i<index.length; i++) {
    index[i] = busDataString[i];
  }
}

void keyPressed() {
  if (key == ' ') {
    maxVal = 0;
    counter++;
    if (counter > data.length-1) {
      counter = 2;
    }
    update();
  }
}