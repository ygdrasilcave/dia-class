String[] data;
int[] bus;
int maxVal = 0;
float w;

PFont font;
String[] monthString;
String[] index;
boolean toggle = true;
int counter = 2;

float wm, hm;

void setup() {
  size(1000, 1000);
  font = createFont("SeoulHangangEB.ttf", 32);
  data = loadStrings("trans.csv");
  bus = new int[12];
  monthString = trim(data[0].split(","));
  index = new String[3]; 
  update();
  textFont(font);
  textSize(30);
  w = width/bus.length;
  textAlign(CENTER, CENTER);
  wm = width/5;
  hm = height/5;
}

void draw() {
  background(0);
  
  textSize(28);
  textAlign(CENTER, CENTER);
  fill(255);  
  beginShape();
  float step = TWO_PI/bus.length;
  for (int i=0; i<bus.length; i++) {
    float x2 = wm/2 + cos(i*step)*(wm/2-30);
    float y2 = hm/2 + sin(i*step)*(hm/2-30);
    noStroke();
    fill(255);
    text(monthString[i+3], x2, y2);
    
    float scaleVal = map(bus[i], 0, maxVal, 0, wm/2-80);
    float x = wm/2 + cos(i*step)*scaleVal;
    float y = hm/2 + sin(i*step)*scaleVal;
    vertex(x, y);
  }
  endShape(CLOSE);

  textAlign(LEFT, CENTER);
  text(index[0], 20, 30);
  text(index[1], 20, 70);
  text(index[2], 20, 110);

  if (frameCount%10 == 0) {
    if (toggle == true) {
      maxVal = 0;
      counter++;
      if (counter > data.length-1) {
        counter = 2;
      }
      update();
      toggle = false;
    }
  } else {
    toggle = true;
  }
}

void update() {
  println(data[counter]);
  String[] busDataString = trim(data[counter].split(","));
  for (int i=0; i<bus.length; i++) {
    bus[i] = int(busDataString[i+3]);
    if (maxVal < bus[i]) {
      maxVal = bus[i];
    }
  }
  for (int i=0; i<index.length; i++) {
    index[i] = busDataString[i];
  }
}

void keyPressed() {
  if (key == ' ') {
    maxVal = 0;
    counter++;
    if (counter > data.length-1) {
      counter = 2;
    }
    update();
  }
}