String[] stringArray = {"Literature,", "in its broadest", "sense, is", 
  "any written work;", "etymologically", "the term derives", "writing formed", 
  "texts."};

ClassA[] a;

float lineSpace;
boolean rot = false;

void setup() {
  size(800, 800);
  background(0);
  println(stringArray.length);
  a = new ClassA[stringArray.length];
  for (int i=0; i<stringArray.length; i++) {
    a[i] = new ClassA(stringArray[i]);
  }
  textAlign(CENTER, CENTER);
  lineSpace = height/stringArray.length;
}

void draw() {
  background(0);
  for (int i=0; i<stringArray.length; i++) {
    if (rot == true) {
      a[i].update();
    }
    a[i].display(width/2, i*lineSpace+lineSpace/2);
  }
}

String in = "";
void keyPressed(){
  if(key == ' '){
    rot = !rot;
  }
  if(key >= 'a' && key <= 'z'){
    in = in+key;
    println(in);
  }  
  if(keyCode == ENTER || keyCode == RETURN){
      in = "";
  }
}