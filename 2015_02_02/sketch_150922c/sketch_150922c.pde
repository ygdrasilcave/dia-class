ArrayList<ClassA> a;

ClassB[] b;

float lineSpace;
boolean rot = false;

String in = "";

void setup() {
  size(800, 800);
  background(0);

  a = new ArrayList<ClassA>();
  b = new ClassB[12];

  textAlign(CENTER, CENTER);
  lineSpace = 0;

  for (int y=0; y<4; y++) {
    for (int x=0; x<3; x++) {
      int index = x + y*3;
      b[index] = new ClassB(x+1, y+1, 25);
    }
  }
}

void draw() {
  background(0);
  for (int i=a.size()-1; i>=0; i--) {
    ClassA _a = a.get(i);
    if (rot == true) {
      _a.update();
    } else {
      _a.update(0);
    }
    _a.display(width/2, i*lineSpace+lineSpace/2);
  }

  float step = TWO_PI/12;

  for (int i=0; i<b.length; i++) {
    float x = width/2 + cos(-(PI/2)+step*i)*(width/2-100);
    float y = height/2 + sin(-(PI/2)+step*i)*(height/2-100);
    b[i].update();
    b[i].display(x,y);
  }
}


void keyPressed() {
  if (key == ' ') {
    rot = !rot;
  }
  if (key >= 'a' && key <= 'z') {
    in = in+key;
    println(in);
  }  
  if (keyCode == ENTER || keyCode == RETURN) {
    a.add(new ClassA(in));
    lineSpace = height/a.size();
    in = "";
  }
}