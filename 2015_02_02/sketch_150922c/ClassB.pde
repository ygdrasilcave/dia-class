class ClassB {
  int cols;
  int rows;
  float dia;
  float t = 0.0;
  float maxDia;
  

  ClassB(int _cols, int _rows, int _dia) {
    cols = _cols;
    rows = _rows; 
    dia = _dia;
    maxDia = dia;
  }

  void update() {
    dia = cos(t)*maxDia;
    t+=0.065;
  }

  void display(float _mx, float _my) {
    pushMatrix();
    translate(_mx, _my);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        if (cols%2 == 0 && rows%2 == 0) {
          ellipse((x-(cols/2))*dia+dia/2, (y-(rows/2))*dia+dia/2, dia, dia);
        } else if (cols%2 == 1 && rows%2 == 1) {
          ellipse((x-(cols/2))*dia, (y-(rows/2))*dia, dia, dia);
        } else if (cols%2 == 0 && rows%2 == 1) {
          ellipse((x-(cols/2))*dia+dia/2, (y-(rows/2))*dia, dia, dia);
        } else if (cols%2 == 1 && rows%2 == 0) {
          ellipse((x-(cols/2))*dia, (y-(rows/2))*dia+dia/2, dia, dia);
        }
      }
    }
    popMatrix();
  }
}