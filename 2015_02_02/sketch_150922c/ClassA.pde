class ClassA {
  String s;
  float t;

  ClassA(String _s) {
    s = _s;
    t = 0.0;
  }

  ClassA() {
    s = "default";
    t = 0.0;
  }

  void update() {
    t += 0.2;
  }
  void update(float _a){
    t = _a;
  }

  void display(float _x, float _y) {
    pushMatrix();
    translate(_x, _y);
    rotate(t);
    fill(255);
    textSize(60);
    text(s, 0, 0);
    popMatrix();    
  }
}