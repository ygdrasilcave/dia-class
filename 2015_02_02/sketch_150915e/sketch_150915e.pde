Chart c1, c2;
float[] dataX;
float[] dataY;

void setup(){
  size(400,400);
  background(0);
  dataX = new float[20];
  dataY = new float[20];
  for(int i=0; i<20; i++){
    dataX[i] = random(width);
    dataY[i] = random(height);
  }
  c1 = new Chart(dataX, dataY);
  c2 = new Chart(dataY, dataX);
}

void draw(){
  background(0);
  c1.display(255, 0, 0);
  c2.display(0, 255, 0);
}