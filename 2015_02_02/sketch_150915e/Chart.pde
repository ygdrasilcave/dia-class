class Chart{
  float[] x;
  float[] y;
  
  Chart(float[] _x, float[] _y){
    x = _x;
    y = _y;
  }
  
  void update(){
    
  }
  
  void display(float r, float g, float b){
    for(int i=0; i<x.length; i++){
      fill(r, g, b);
      ellipse(x[i], y[i], 10, 10);
    }
  }
}