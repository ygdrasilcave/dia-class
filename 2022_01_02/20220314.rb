# Welcome to Sonic Pi v2.11.1

use_synth :saw

comment do
  play 60
  sleep 0.5
  
  play 54
  sleep 0.5
  
  play 53
  sleep 0.5
  
  play 64
  sleep 0.5
  
  play 63
  sleep 0.5
  
  play hz_to_midi(442)
  sleep 1
end


_note = [60, 64, 63, 72, 76, 79]
_duration = [1, 0.5, 0.25, 0.125]

live_loop :main do
  play _note.choose
  sleep _duration.choose
end
