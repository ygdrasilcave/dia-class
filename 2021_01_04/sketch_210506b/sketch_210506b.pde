PImage img;

int cols, rows;
int r;

float pixelDistX;
float pixelDistY;

float pixelScale = 1.0;

void setup() {
  size(1280, 720);
  img = loadImage("flower.jpg");

  r = 10;
  cols = int(width/(r*2));
  rows = int(height/(r*2));
  
  float _r = float(r);
  pixelDistX = sqrt(_r*_r - (_r*0.5)*(_r*0.5)) * 2.0;
  pixelDistY = _r + (_r*0.5);
}

void draw() {
  background(255);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = x*(r*2) + (y*(r*2))*img.width;
      color c = img.pixels[index];
      if(y%2==0){
        drawMyPixel2(x*(pixelDistX*pixelScale), y*(pixelDistY*pixelScale), r*pixelScale, c);
      }else{
        drawMyPixel2(x*pixelDistX*pixelScale + (pixelDistX*pixelScale)*0.5, y*(pixelDistY*pixelScale), r*pixelScale, c);
      }
    }
  }
  
  pixelScale = map(mouseX, 0, width, 0.5, 4.0);
}

void drawMyPixel(float _posX, float _posY, float _r, color _c) {
  fill(_c);
  noStroke();
  beginShape();
  for (int i=0; i<6; i++) {
    float px = _posX + cos(radians(-30)+radians(60*i))*_r;
    float py = _posY + sin(radians(-30)+radians(60*i))*_r;
    vertex(px, py);
  }
  endShape(CLOSE);
}

void drawMyPixel2(float _posX, float _posY, float _r, color _c) {
  fill(_c);
  noStroke();
  beginShape();
  for (int i=0; i<6; i++) {
    float px = _posX + cos(radians(-30)+radians(60*i))*_r;
    float py = _posY + sin(radians(-30)+radians(60*i))*_r;
    vertex(px, py);
  }
  endShape(CLOSE);
}
