float x=0;
float y=0;
float dirX = 1;
float dirY = 1;
float speedX = 4;
float speedY = 6;
float dia = 60;

void setup(){
  size(1000, 800);
  background(255);
}

void draw(){
  background(255);
  
  dia = map(mouseX, 0, width, 10, 300);
  
  fill(0);
  ellipse(x, y, dia, dia);
  
  x += dirX*speedX;
  y += dirY*speedY;
  
  if(x > width-dia/2){
    x = width-dia/2;
    dirX *= -1;
  }else if(x < dia/2){
    x = dia/2;
    dirX *= -1;
  }
  
  if(y > height-dia/2){
    y = height-dia/2;
    dirY *= -1;
  }else if(y < dia/2){
    y = dia/2;
    dirY *= -1;
  }
}
