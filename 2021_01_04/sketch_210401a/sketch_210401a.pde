float w = 100;
int cols;
int rows;

void setup() {
  size(1000, 800);
  rows = int(height/w);
  cols = int(width/w);
}

void draw() {
  background(255);

  noFill();
  stroke(0);
  /*for (int y=0; y<rows; y++) {
   for (int x=0; x<cols; x++) {
   ellipse(w/2 + x*w, w/2 + y*w, w, w);
   }
   }*/

  for (int y=0; y<height; y+=w) {
    for (int x=0; x<width; x+=w) {
      ellipse(w/2 + x, w/2 + y, w, w);
    }
  }
}
