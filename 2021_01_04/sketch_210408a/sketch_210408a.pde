PImage img_tree;
float degree = 0;

int treeNum = 16;

float offset_x;
float[] offset_y;
float scaleFactor;
float offset_y_dir = 1;
float t = 0;

void setup() {
  size(1000, 800);
  img_tree = loadImage("tree.png");
  scaleFactor = 0.3;
  offset_y = new float[treeNum];
  for (int i=0; i<treeNum; i++) {
    offset_y[i] = 505*scaleFactor;
  }
}

void draw() {
  background(255, 200, 0);

  float img_w = img_tree.width*scaleFactor;
  float img_h = img_tree.height*scaleFactor;

  offset_x = 250*scaleFactor;
  
  t+=0.153;
  
  //degree += 0.025;
  

  pushMatrix();
  translate(width/2, height/2);
  rotate(-degree);
  for (int i=0; i<treeNum; i++) {
    pushMatrix();
    rotate(i*(TWO_PI/treeNum));
    offset_y[i] = (505*scaleFactor+60) + sin(i*(HALF_PI*0.5)+t)*50;
    image(img_tree, -(img_w/2) + offset_x, -(img_h/2) - offset_y[i], img_w, img_h);
    stroke(255, 3, 197);
    strokeWeight(3);
    line(0, 0, 0, -offset_y[i]+(img_h/2));
    popMatrix();
  }
  popMatrix();
}
