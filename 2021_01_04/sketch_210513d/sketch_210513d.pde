PImage[] img;

int imageFrame = 0;

boolean play = true;

float currentPos = 0;
float velocity = 5;

void setup() {
  size(1600, 400);

  img = new PImage[7];

  for (int i=0; i<img.length; i++) {
    img[i] = loadImage("ballet_" + nf(i, 2) + ".png");
  }

  textSize(24);
}

void draw() {
  background(255, 255, 0);
  
  //currentPos = mouseX;
  
  velocity = map(mouseY, 0, height, 50, 5);
  currentPos += velocity;
  if(currentPos > width){
    currentPos = 0;
  }

  image(img[imageFrame], currentPos-height/2, 0, height, height);

  /*if(currentPos < 200){
    imageFrame = 0;
  }else if(currentPos >= 200 && currentPos < 400){
    imageFrame = 1;
  }else if(currentPos >= 400 && currentPos < 600){
    imageFrame = 2;
  }else if(currentPos >= 600 && currentPos < 1000){
    imageFrame = 3;
  }else if(currentPos >= 1000 && currentPos < 1200){
    imageFrame = 4;
  }else if(currentPos >= 1200 && currentPos < 1400){
    imageFrame = 5;
  }else if(currentPos >= 1400){
    imageFrame = 6;
  }*/
  float gap = width/7.0;
  imageFrame = int(floor(currentPos/gap));
  if(imageFrame > img.length-1){
    imageFrame = img.length-1;
  }
  //println(imageFrame);

  fill(0);
  text("current frame: " + imageFrame, 50, 50);
  text("play: " + play, 50, 100);
}

void keyPressed() {
  if (key == ' ') {
    play = !play;
  }
}
