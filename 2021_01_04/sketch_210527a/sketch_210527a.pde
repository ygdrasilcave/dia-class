import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};

Ani xAni;
Ani yAni;

float x, y;

//0:Right, 1:Left, 2:Up, 3:Down
int dir = 0;

int[] dirSeq = {0, 2, 1, 3, 0, 2};
float[] targetValue = {300, 200, 500, 600, 200, 400};

int seq = 0;

void setup() {
  size(1000, 1000);
  background(0);

  Ani.init(this);
  Ani.noAutostart();

  x = width/2;
  y = height/2;

  xAni = new Ani(this, 1.0, "x", x, Ani.LINEAR, "onEnd:xAniendEvent");
  yAni = new Ani(this, 1.0, "y", y, Ani.LINEAR, "onEnd:yAniendEvent");
}

void draw() {
  //background(0);

  fill(255);
  ellipse(x, y, 50, 50);
}

void keyPressed() {
  if (key == ' ') {
    dir = dirSeq[seq];
    aniSeq(dir, targetValue[seq]);
  }
}

void xAniendEvent() {
  seq++;
  if (seq > dirSeq.length-1) {
    seq = 0;
  }
  dir = dirSeq[seq];
  aniSeq(dir, targetValue[seq]);
}

void yAniendEvent() {
  seq++;
  if (seq > dirSeq.length-1) {
    seq = 0;
  }
  dir = dirSeq[seq];
  aniSeq(dir, targetValue[seq]);
}

//0:Right, 1:Left, 2:Up, 3:Down
void aniSeqRandom() {
  if (dir == 0) {
    float lastPosX = xAni.getEnd();
    float newPosX = random(lastPosX, width);
    xAni.setBegin(lastPosX);
    xAni.setEnd(newPosX);
    xAni.start();
  } else if (dir == 1) {
    float lastPosX = xAni.getEnd();
    float newPosX = random(0, lastPosX);
    xAni.setBegin(lastPosX);
    xAni.setEnd(newPosX);
    xAni.start();
  } else if (dir == 2) {
    float lastPosY = yAni.getEnd();
    float newPosY = random(0, lastPosY);
    yAni.setBegin(lastPosY);
    yAni.setEnd(newPosY);
    yAni.start();
  } else if (dir == 3) {
    float lastPosY = yAni.getEnd();
    float newPosY = random(lastPosY, height);
    yAni.setBegin(lastPosY);
    yAni.setEnd(newPosY);
    yAni.start();
  }
}
//0:Right, 1:Left, 2:Up, 3:Down
void aniSeq(int _dir, float _newTarget) {
  if (_dir == 0) {
    float lastPosX = xAni.getEnd();
    float newPosX = lastPosX + _newTarget;
    xAni.setBegin(lastPosX);
    xAni.setEnd(newPosX);
    xAni.start();
  } else if (_dir == 1) {
    float lastPosX = xAni.getEnd();
    float newPosX = lastPosX - _newTarget;
    xAni.setBegin(lastPosX);
    xAni.setEnd(newPosX);
    xAni.start();
  } else if (_dir == 2) {
    float lastPosY = yAni.getEnd();
    float newPosY = lastPosY - _newTarget;
    yAni.setBegin(lastPosY);
    yAni.setEnd(newPosY);
    yAni.start();
  } else if (_dir == 3) {
    float lastPosY = yAni.getEnd();
    float newPosY = lastPosY + _newTarget;
    yAni.setBegin(lastPosY);
    yAni.setEnd(newPosY);
    yAni.start();
  }
}
