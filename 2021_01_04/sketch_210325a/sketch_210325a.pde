int x = 0;
int speed = 10;
int dir = 1;

void setup(){
  size(1000, 800);
  background(0);
}

void draw(){
  background(0);
  
  stroke(255);
  noFill();
  float dia = map(x, 0, width, 0, height);
  ellipse(width/2, height/2, dia, dia);
  
  stroke(255);
  //noFill();
  fill(map(x, 0, width, 0, 255));
  ellipse(x, height/2, 200, 200);
  
  ellipse(width/2, dia, 100, 100);
  
  ellipse(x, dia, 50, 50);
  
  ellipse(map(x, 0, width, width, 0), dia, 20, 20);
  
  x += (dir * speed);
  if(x <= 0){
    x = 0;
    dir = dir*-1;
    speed = int(random(1, 20));
    println(speed);
  }
  if(x >= width){
    x = width;
    dir = dir*-1;
    speed = int(random(1, 20));
    println(speed);
  }

}
