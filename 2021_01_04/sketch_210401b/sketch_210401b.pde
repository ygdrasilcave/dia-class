PImage img_tree;
PImage img_bk;
float degree = 0;

void setup() {
  size(1000, 800);
  img_tree = loadImage("tree.png");
  img_bk = loadImage("Tulips.jpg");
}

void draw() {
  background(255, 200, 0);
  //image(img_bk, 0, 0, width, height);
  
  /*for (int y=0; y<2; y++) {
    for (int x=0; x<2; x++) {
      image(img_tree, x*(width/2), y*(height/2), width/2, height/2);
    }
  }*/
  
  float scaleFactor = map(mouseX, 0, width, 0.1, 1.0);
  float img_w = img_tree.width*0.25;
  float img_h = img_tree.height*0.25;
  
  //float degree = map(mouseY, 0, height, 0, TWO_PI);
  
  degree += 0.065;

  translate(mouseX, mouseY);
  rotate(degree);
  //image(img_tree, -(img_w/2), -(img_h/2), img_w, img_h);
  image(img_tree, -(img_w/2) + 130, -(img_h/2) - 260, img_w, img_h);
  fill(255, 0, 0);
  noStroke();
  rect(-10, -10, 20, 20);

}
