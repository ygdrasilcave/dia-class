import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

int easingIndex = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};

float w, h, rot, white;

Ani wAni;
Ani hAni;
Ani rotAni;
Ani whiteAni;

int columns = 10;
int rows = 6;

int endCounter = 0;

float cellWidth;
float cellHeight;
float maxSize;

String wEasing = "";
String hEasing = "";
float wStartValue = 0;
float wEndValue = 0;
float wDuration = 0;
float hStartValue = 0;
float hEndValue = 0;
float hDuration = 0;
float rStartValue = 0;
float rDuration = 0;
float rEndValue = 0;

int xStep = 0;
int yStep = 0;

int type = 3;
boolean drawGrid = true;

PShape s;
PShape bot;

void setup() {
  size(2000, 1200);
  background(0);

  cellWidth = width/columns;
  cellHeight = height/rows;

  if (cellWidth > cellHeight) {
    maxSize = cellHeight * 0.9;
  } else {
    maxSize = cellWidth * 0.9;
  }

  Ani.init(this);

  w = 0;
  h = 0;
  rot = 0;
  white = 0;

  wEasing = easingName[easingIndex];
  hEasing = easingName[easingIndex];
  wStartValue = w;
  wEndValue = maxSize;
  hStartValue = h;
  hEndValue = maxSize;
  rStartValue = rot;
  rEndValue = TWO_PI;

  wDuration = 5.0;
  hDuration = 2.0;
  rDuration = 5.0;

  wAni = new Ani(this, wDuration, "w", wEndValue, easings[easingIndex], "onEnd:wAniEndEvt");
  hAni = new Ani(this, hDuration, "h", hEndValue, easings[easingIndex], "onEnd:hAniEndEvt");
  rotAni = new Ani(this, rDuration, "rot", rEndValue, easings[0], "onEnd:rotAniEndEvt");
  whiteAni = new Ani(this, 6.0, "white", 255, easings[0], "onEnd:whiteAniEndEvt");

  
  textAlign(CENTER, CENTER);
  textSize(14);
  
  s = loadShape("Untitled-1.svg");
  bot = loadShape("bot1.svg");
  
  rectMode(CENTER);
  shapeMode(CENTER);

}

void draw() {
  if (drawGrid == true) {
    stroke(90);
    strokeWeight(0.25);
    for (int x=0; x<columns; x++) {
      line(x*cellWidth, 0, x*cellWidth, height);
    }
    for (int y=0; y<rows; y++) {
      line(0, y*cellHeight, width, y*cellHeight);
    }
  }

  pushMatrix();
  noFill();
  stroke(white);
  strokeWeight(1);
  translate(cellWidth/2 + cellWidth*xStep, cellHeight/2 + cellHeight*yStep);
  rotate(rot);
  if (type == 0) {
    rect(0, 0, w, h);
    rect(0, 0, h, w);
  } else if (type == 1) {
    rect(0, 0, w, h);
  } else if (type == 2) {
    ellipse(0, 0, w, h);
  } else if (type == 3) {
    //ellipse(0, 0, w, h);
    //ellipse(0, 0, h, w);
    s.disableStyle();
    stroke(white);
    noFill();
    shape(s, 0, 0, w, h);
  }
  popMatrix();

  textSize(14);
  pushMatrix();
  translate(cellWidth/2 + cellWidth*xStep, (cellHeight*3) + (cellHeight/2) + cellHeight*yStep);
  fill(0);
  noStroke();
  rect(0, 0, cellWidth-5, cellHeight-5);
  fill(255);
  text(trim(wEasing), 0, -70);
  text(trim(hEasing), 0, -50);
  text(trim(nf(wStartValue, 3, 3) + " >>> " + nf(wEndValue, 3, 3)), 0, -30);
  text(trim(nf(wDuration, 2, 3)) + " seconds", 0, -10);
  text(trim(nf(hStartValue, 3, 3) + " >>> " + nf(hEndValue, 3, 3)), 0, 10);
  text(trim(nf(hDuration, 2, 3)) + " seconds", 0, 30);
  text(trim(nf(rStartValue, 3, 3) + " >>> " + nf(rEndValue, 3, 3)), 0, 50);
  text(trim(nf(rDuration, 2, 3)) + " seconds", 0, 70);
  popMatrix();

  if (endCounter == 4) {

    easingIndex = int(random(0, 31));

    wStartValue = random(maxSize);
    wEndValue = random(maxSize);
    wDuration = random(1, 6);
    wAni.setBegin(wStartValue);
    wAni.setEnd(wEndValue);
    wAni.setDuration(wDuration);
    wAni.setEasing(easings[easingIndex]);
    wEasing = easingName[easingIndex];

    easingIndex = int(random(0, 31));

    hStartValue = random(maxSize);
    hEndValue = random(maxSize);
    hDuration = random(1, 6);
    hAni.setBegin(hStartValue);
    hAni.setEnd(hEndValue);
    hAni.setDuration(hDuration);
    hAni.setEasing(easings[easingIndex]);
    hEasing = easingName[easingIndex];

    rStartValue = random(TWO_PI);
    rEndValue = random(TWO_PI);
    rDuration = random(1, 6);
    rotAni.setBegin(rStartValue);
    rotAni.setEnd(rEndValue);
    rotAni.setDuration(rDuration);
    rotAni.setEasing(easings[0]);

    whiteAni.setBegin(0);
    whiteAni.setEnd(255);
    whiteAni.setDuration(random(1, 6));  
    whiteAni.setEasing(easings[0]);

    endCounter = 0;

    xStep++;
    if (xStep > columns-1) {
      xStep = 0;
      yStep++;
      if (yStep > (rows/2)-1) {
        yStep = 0;
        println("done");
        saveFrame("myWork-######.tiff");
        background(0);
      }
    }

    wAni.start();
    hAni.start();
    rotAni.start();
    whiteAni.start();
  }
}

void wAniEndEvt() {
  endCounter += 1;
}

void hAniEndEvt() {
  endCounter += 1;
}

void rotAniEndEvt() {
  endCounter += 1;
}

void whiteAniEndEvt() {
  endCounter += 1;
}

void keyPressed() {
  if (key == ' ') {
    type++;
    if (type > 3) {
      type = 0;
    }
  }
}
