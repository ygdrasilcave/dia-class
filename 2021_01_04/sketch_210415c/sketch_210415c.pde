PImage img;
PImage face;

void setup() {
  size(1200, 700);
  img = loadImage("Tulips.jpg");
  face = loadImage("face.png");
}

void draw() {
  background(255);

  image(img, 0, 0, width, height);

  //image(face, mouseX-face.width/2, mouseY-face.height/2);

  //BLEND, ADD, SUBTRACT, LIGHTEST, DARKEST, DIFFERENCE, EXCLUSION,
  //MULTIPLY, SCREEN, OVERLAY, HARD_LIGHT, SOFT_LIGHT, DODGE, BURN
  blend(face, 0, 0, face.width/2, face.height/2, 
  mouseX-face.width/2, mouseY-face.height/2, face.width/2, face.height/2, SOFT_LIGHT);
  
  blend(face, 0, face.height/2, face.width/2, face.height/2, 
  mouseX-face.width/2, mouseY, face.width/2, face.height/2, BURN);
  
  blend(face, face.width/2, 0, face.width/2, face.height/2, 
  mouseX, mouseY-face.height/2, face.width/2, face.height/2, EXCLUSION);
  
  blend(face, face.width/2, face.height/2, face.width/2, face.height/2, 
  mouseX, mouseY, face.width/2, face.height/2, DIFFERENCE);
}
