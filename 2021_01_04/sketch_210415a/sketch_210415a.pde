PImage img;
PImage tree;

void setup(){
  size(1200, 350);
  img = loadImage("Tulips.jpg");
  tree = loadImage("tree.png");
}

void draw(){
  background(255);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(HALF_PI);
  noTint();
  image(tree, -height/2, -width/2, height, width);
  popMatrix();
  
  noTint();
  image(img, 0, 0);
  
  float tintValue = map(mouseY, 0, height, 0, 255);
  /*tint(255, tintValue, tintValue, 100);
  image(img, width/2, 0);*/
  tint(255, 255, 0, tintValue);
  image(img, width/2, 0);
}
