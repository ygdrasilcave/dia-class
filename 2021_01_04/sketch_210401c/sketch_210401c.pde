float t = 0;

void setup(){
  size(1000, 800);
}

void draw(){
  background(255);
  
  t+=0.025;
  
  pushMatrix();
  translate(mouseX, mouseY);
  rotate(t);
  strokeWeight(5);
  stroke(0);
  line(0, 0, 300, 0);
  fill(0);
  noStroke();
  ellipse(300, 0, 30, 30);
  //ellipse(-100, 0, 30, 30);
  popMatrix();
  
  pushMatrix();
  translate(mouseX, mouseY);
  rotate(t * -2);
  strokeWeight(5);
  stroke(255,0,0);
  line(0, 0, 300, 0);
  fill(255,0,0);
  noStroke();
  ellipse(300, 0, 30, 30);
  popMatrix();
}
