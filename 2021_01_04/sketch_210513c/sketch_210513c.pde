PImage[] img;

int imageFrame = 0;
int currentTime = 0;

boolean play = true;

void setup() {
  size(1000, 1000);

  img = new PImage[7];

  for (int i=0; i<img.length; i++) {
    img[i] = loadImage("ballet_" + nf(i, 2) + ".png");
  }

  textSize(24);

  currentTime = millis();
  println(currentTime);
}

void draw() {
  background(255, 255, 0);

  image(img[imageFrame], 0, 0);

  int interval = int(map(mouseY, 0, height, 25, 250));

  if (currentTime + interval < millis()) {
    if (play == true) {
      imageFrame++;
      if (imageFrame > img.length-1) {
        imageFrame = 0;
      }
    }
    currentTime = millis();
  }

  fill(0);
  text("current frame: " + imageFrame, 50, 50);
  text("delay time: " + interval, 50, 75);
  text("play: " + play, 50, 100);
}

void keyPressed() {
  if (key == ' ') {
    play = !play;
  }
}
