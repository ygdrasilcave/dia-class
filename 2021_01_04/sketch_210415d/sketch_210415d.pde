PImage img;
float t = 0.0;

void setup() {
  size(1200, 700);
  img = loadImage("Tulips.jpg");
}

void draw() {
  background(255);

  image(img, 0, 0, width, height);

  //image(face, mouseX-face.width/2, mouseY-face.height/2);
  
  int offsetX = int(map(sin(t), -1, 1, 0, 200));
  
  t+=0.12;

  //BLEND, ADD, SUBTRACT, LIGHTEST, DARKEST, DIFFERENCE, EXCLUSION,
  //MULTIPLY, SCREEN, OVERLAY, HARD_LIGHT, SOFT_LIGHT, DODGE, BURN
  blend(img, 0, 0, width, height, offsetX, 0, width, height, DIFFERENCE);
}
