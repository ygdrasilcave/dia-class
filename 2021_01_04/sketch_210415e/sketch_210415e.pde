PImage img;
PImage gradiant;

/*
int num = 8;
float[] yStartPos;
float t = 0;
*/
void setup() {
  size(1200, 700);
  img = loadImage("Tulips.jpg"); //600x350
  gradiant = loadImage("gradiant.png"); //600x600
  
  /*yStartPos = new float[num];
  for(int i=0; i<num; i++){
    yStartPos[i] = sin(i*HALF_PI*0.5)*200;
  }*/
}

//blend mode
//BLEND, ADD, SUBTRACT, LIGHTEST, DARKEST, DIFFERENCE, EXCLUSION,
//MULTIPLY, SCREEN, OVERLAY, HARD_LIGHT, SOFT_LIGHT, DODGE, BURN

void draw() {
  background(255);
  
  image(gradiant, mouseX-gradiant.width/2, mouseY-gradiant.height/2);
  blend(img, 0, 0, gradiant.width, gradiant.height, 0, 0, width, height, ADD);
  
  //image(img, 0, 0, width, height);
  //blend(gradiant, 0, 0, gradiant.width, gradiant.height, mouseX, mouseY, gradiant.width, gradiant.height, SCREEN);
  
  /*for(int x = 0; x<num; x++){
    yStartPos[x] = sin(x*HALF_PI*0.5 + t)*200;
    image(gradiant, x*(width/num), height/2-100 + yStartPos[x], 200, 200);
  }  
  t+=0.25;
  
  blend(img, 0, 0, gradiant.width, gradiant.height, 0, 0, width, height, ADD);*/
}
