float w = 50;

float br = 0;
float dirBr = 1;
float speedBr = 0.5;

void setup() {
  size(800, 600);
}

void draw() {
  background(0);

  /*
  ellipse(0*w + w/2, height/2, w, w);
   rect(1*w, height/2-w/2, w, w);
   
   ellipse(2*w + w/2, height/2, w, w);
   rect(3*w, height/2-w/2, w, w);
   
   ellipse(4*w + w/2, height/2, w, w);
   rect(5*w, height/2-w/2, w, w);
   
   ellipse(6*w + w/2, height/2, w, w);
   rect(7*w, height/2-w/2, w, w);
   
   ellipse(8*w + w/2, height/2, w, w);
   rect(9*w, height/2-w/2, w, w);
   
   ellipse(10*w + w/2, height/2, w, w);
   rect(11*w, height/2-w/2, w, w);
   */

  /*for (int i=0; i<20; i+=2) {
   ellipse(i*w + w/2, height/2, w, w);
   }
   
   for (int i=1; i<20; i+=2) {
   rect(i*w, height/2-w/2, w, w);
   }*/


  /*
  for(int i=0; i<(width/w); i+=2){
   ellipse(i*w + w/2, w/2+0*w, w, w);
   rect((i+1)*w, w/2-w/2+0*w, w, w);
   }
   
   for(int i=0; i<(width/w); i+=2){
   ellipse(i*w + w/2, w/2+1*w, w, w);
   rect((i+1)*w, w/2-w/2+1*w, w, w);
   }
   
   for(int i=0; i<(width/w); i+=2){
   ellipse(i*w + w/2, w/2+2*w, w, w);
   rect((i+1)*w, w/2-w/2+2*w, w, w);
   }
   
   for(int i=0; i<(width/w); i+=2){
   ellipse(i*w + w/2, w/2+3*w, w, w);
   rect((i+1)*w, w/2-w/2+3*w, w, w);
   }
   */

  /*
  noFill();
   stroke(255);
   fill(255);
   noStroke();
   for (int y=0; y<height/w; y++) {
   for (int x=0; x<(width/(1*w)); x+=2) {
   ellipse(x*w + w/2, w/2+y*w, w, w);
   rect((x+1)*w, w/2-w/2+y*w, w, w);
   }
   }
   */

  br += dirBr * speedBr;
  if (br >= 255) {
    br = 255;
    dirBr *= -1;
  } else if (br <= 0) {
    br = 0;
    dirBr *= -1;
  }

  noStroke();
  for (int y=0; y<height/w; y++) {
    float yw = y*w;
    float tempScale = w*0.5;
    if (y%2==0) {
      for (int x=0; x<(width/(1*w)); x+=2) {
        float tempX = x*w + tempScale;
        float tempY = tempScale + yw;
        fill(255);
        ellipse(tempX, tempY, w, w);
        rect((x+1)*w, yw, w, w);
        fill(br);
        ellipse(tempX, tempY, tempScale, tempScale);
        ellipse((x+1)*w + tempScale, tempY, w, w);
      }
    } else {
      for (int x=0; x<(width/(1*w)); x+=2) {        
        float xw = x*w;
        float tempY = tempScale + yw;
        fill(255);
        rect(xw, yw, w, w);
        ellipse((x+1)*w + tempScale, tempY, w, w);
        fill(br);
        ellipse(xw + tempScale, tempY, w, w);
        ellipse((x+1)*w + tempScale, tempY, tempScale, tempScale);
      }
    }
  }

  println(frameRate);
}
