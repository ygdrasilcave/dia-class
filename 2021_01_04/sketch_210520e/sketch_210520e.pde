import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Ani xAni;
Ani yAni;
float x;
float y;

float newDuration = 1.0;

int counter=0;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};



void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(0);

  x = 0.0;
  y = height;

  Ani.init(this);
  Ani.noAutostart();

  println(easings.length);

  //Ani(Object Target, float Duration, String FieldName, float End)
  //Ani(Object Target, float Duration, float Delay, String FieldName, float End)
  xAni = new Ani(this, newDuration, "x", width/2, Ani.LINEAR);
  yAni = new Ani(this, newDuration, "y", 0, Ani.QUAD_IN);

  textAlign(CENTER, CENTER);
  textSize(24);
}

void draw() {

  fill(0, 0, 255);
  ellipse(x, y, 10, 10);

  fill(0, 0, 255);
  noStroke();
  //text("easing style: " + easingName[easingCounter], width/2, height-100);
  text("duration: " + newDuration, width/2, height-50);
}

void keyPressed() {
  if (key == ' ') {
    //newDuration = random(0.5, 3.0);
    if (counter%2 == 0) { 
      background(0);
      if (xAni.isPlaying() == false) {
        float newBegin = 0;
        float newEnd = width/2;
        xAni.setBegin(newBegin);
        xAni.setEnd(newEnd);
        xAni.setDuration(newDuration);
        xAni.start();
      }
      if (yAni.isPlaying() == false) {
        /*easingCounter++;
        if (easingCounter > easings.length-1) {
          easingCounter = 0;
        }
        yAni.setEasing(easings[easingCounter]);*/
        yAni.setEasing(Ani.QUAD_IN);
        float newBegin = height;
        float newEnd = 0;
        yAni.setBegin(newBegin);
        yAni.setEnd(newEnd);
        yAni.setDuration(newDuration);
        yAni.start();
      }
    }else{
      if (xAni.isPlaying() == false) {
        float newBegin = width/2;
        float newEnd = width;
        xAni.setBegin(newBegin);
        xAni.setEnd(newEnd);
        xAni.setDuration(newDuration);
        xAni.start();
      }
      if (yAni.isPlaying() == false) {
        float newBegin = 0;
        float newEnd = height;
        yAni.setBegin(newBegin);
        yAni.setEnd(newEnd);
        yAni.setDuration(newDuration);
        yAni.setEasing(Ani.QUAD_OUT);
        yAni.start();
      }
    }

    counter++;
  }
}
