PImage img;

int pixelSize = 15;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");
  background(255);
}

void draw() {
  //background(0);

  noStroke();
  
  //int x = mouseX;
  //int y = mouseY;
  
  int x = int(random(width));
  int y = int(random(height));
  
  int index = x + y*width;
  color c = img.pixels[index];
  fill(c);
  ellipse(x, y, pixelSize, pixelSize);
}
