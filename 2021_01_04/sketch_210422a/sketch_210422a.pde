PImage img;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");
}

void draw() {
  background(255);
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      float r = map(x, 0, width, 0, 255);
      float g = map(y, 0, height, 0, 255);      
      stroke(r, g, 0);
      point(x, y);
    }
  }
}
