PImage img;

int cols, rows;
int r;
float pixelRotate = HALF_PI/2;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");

  r = 10;
  cols = int(width/r);
  rows = int(height/r);
}

void draw() {
  //image(img, 0, 0);
  background(0);


  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = (x*r) + (y*r)*img.width;
      color c = img.pixels[index];
      //fill(c);
      //noStroke();
      //rect(x*r, y*r, r, r);

      stroke(c);
      strokeWeight(5);
      pushMatrix();
      translate(x*r, y*r);
      rotate(pixelRotate);
      line(0, 0, 5, 0);
      popMatrix();
    }
  }

  //pixelRotate += 0.01;
}
