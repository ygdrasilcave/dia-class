PImage img;

int pixelSize = 7;

float[] redValue;
float[] greenValue;
float[] blueValue;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");

  int arraySizeX = ceil(width/float(pixelSize));
  int arraySizeY = ceil(height/float(pixelSize));
  int arraySize = arraySizeX * arraySizeY;
  redValue = new float[arraySize];
  greenValue = new float[arraySize];
  blueValue = new float[arraySize];

  println(arraySizeX);
  println(arraySizeY);
  println(arraySize);

  /*
  //pixelSize == 1
   for(int i=0; i<arraySize; i++){
   color c = img.pixels[i*pixelSize];
   redValue[i] = red(c);
   greenValue[i] = green(c);
   blueValue[i] = blue(c);
   }
   */
  int i = 0;
  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      if (i < arraySize) {
        redValue[i] = red(c);
        greenValue[i] = green(c);
        blueValue[i] = blue(c);
      }
      i++;
    }
  }

  println(i);

  background(255);

  println(img.pixels.length);
  println(redValue.length);
}

void draw() {
  noStroke();
  for (int i=0; i<redValue.length; i++) {
    int x = i%(ceil((width)/float(pixelSize)));
    int y = (i/((ceil((width)/float(pixelSize)))));
    fill(redValue[i], greenValue[i], blueValue[i]);
    rect(x*pixelSize, y*pixelSize, pixelSize, pixelSize);
  }
}
