PImage img;

int cols, rows;
int r;

float pixelDistX;
float pixelDistY;

float pixelScale = 1.0;

int prevSecond = 0;
int lightDir = 0;

void setup() {
  size(1280, 720);
  img = loadImage("flower.jpg");

  r = 10;

  float _r = float(r);
  pixelDistX = sqrt(_r*_r - (_r*0.5)*(_r*0.5)) * 2.0;
  pixelDistY = _r + (_r*0.5);

  cols = int(width/int(pixelDistX));
  rows = int(height/int(pixelDistY));
  
  prevSecond = second();
}

void draw() {
  background(255);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = x*int(pixelDistX) + (y*int(pixelDistY))*img.width;
      color c = img.pixels[index];
      if (y%2==0) {
        drawMyPixel2(x*(pixelDistX*pixelScale), y*(pixelDistY*pixelScale), r*pixelScale, c);
      } else {
        drawMyPixel2(x*pixelDistX*pixelScale + (pixelDistX*pixelScale)*0.5, y*(pixelDistY*pixelScale), r*pixelScale, c);
      }
    }
  }

  pixelScale = map(mouseX, 0, width, 0.5, 4.0);
  
  if(prevSecond != second()){
    lightDir++;
    prevSecond = second();
  }
}

void drawMyPixel(float _posX, float _posY, float _r, color _c) {
  fill(_c);
  noStroke();
  beginShape();
  for (int i=0; i<6; i++) {
    float px = _posX + cos(radians(-30)+radians(60*i))*_r;
    float py = _posY + sin(radians(-30)+radians(60*i))*_r;
    vertex(px, py);
  }
  endShape(CLOSE);
}

void drawMyPixel2(float _posX, float _posY, float _r, color _c) {
  float cr = red(_c);
  float cg = green(_c);
  float cb = blue(_c);
  
  noStroke();
  for (int j=0; j<3; j++) {
    if (j==(lightDir%3)) {
      fill(cr*1.0, cg*1.0, cb*1.0);
    }else if(j==((lightDir%3)+1)%3){
      fill(cr*0.5, cg*0.5, cb*0.5);
    }else if(j==((lightDir%3)+2)%3){
      fill(cr*0.1, cg*0.1, cb*0.1);
    }
    beginShape();
    vertex(_posX, _posY);
    for (int i=0; i<3; i++) {
      float px = _posX + cos(radians(-30 + 120*j)+radians(60*i))*_r;
      float py = _posY + sin(radians(-30 + 120*j)+radians(60*i))*_r;
      vertex(px, py);
    }
    endShape(CLOSE);
  }
}
