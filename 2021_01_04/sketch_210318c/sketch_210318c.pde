int x = 0;
int speed = 10;
int dir = 1;

void setup(){
  size(1000, 800);
  background(0);
}

void draw(){
  background(0);
  
  stroke(255);
  noFill();
  ellipse(x, height/2, 200, 200);
  
  x += (dir * speed);
  if(x <= 0){
    x = 0;
    dir = dir*-1;
    speed = int(random(1, 20));
    println(speed);
  }
  if(x >= width){
    x = width;
    dir = dir*-1;
    speed = int(random(1, 20));
    println(speed);
  }
}
