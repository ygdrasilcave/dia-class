import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Ani xAni;
float newDuration = 1.0;
float dia;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};



void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(0);

  Ani.init(this);
  //Ani.noAutostart();

  println(easings.length);
  
  dia = 100;

  //Ani(Object Target, float Duration, String FieldName, float End)
  //Ani(Object Target, float Duration, float Delay, String FieldName, float End)
  //callback -> onStart, onEnd, onDelayEnd and onUpdate
  xAni = new Ani(this, newDuration, "dia", 600, easings[easingCounter], "onEnd:endEvent");

  textAlign(CENTER, CENTER);
  textSize(24);
}

void endEvent(){
  newDuration = random(0.5, 1.5);
  easingCounter = int(random(0, easings.length-1));
  //xAni.setEasing(easings[easingCounter]);
  xAni.setEasing(Ani.ELASTIC_IN_OUT);
  float newBegin = xAni.getEnd();
  float newEnd = xAni.getBegin();
  xAni.setBegin(newBegin);
  xAni.setEnd(newEnd);
  //xAni.setDelay(1.0);
  xAni.setDuration(newDuration);
  xAni.start();
}

void draw() {
  background(0);

  fill(0, 0, 255);
  ellipse(width/2, height/2, dia, dia);

  fill(0, 0, 255);
  noStroke();
  text("easing style: " + easingName[easingCounter], width/2, height-100);
  text("duration: " + newDuration, width/2, height-50);
}
