import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Ani rotateAni;
float rotateValue = 0.0;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT,
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT",
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};



void setup() {
  size(800, 800);
  background(0);

  Ani.init(this);
  Ani.noAutostart();
  
  println(easings.length);

  //Ani(Object Target, float Duration, String FieldName, float End)
  //Ani(Object Target, float Duration, float Delay, String FieldName, float End)
  rotateAni = new Ani(this, 2.5, "rotateValue", TWO_PI, easings[easingCounter]);
  
  textAlign(CENTER, CENTER);
  textSize(24);
}

void draw() {
  background(0);

  pushMatrix();
  fill(255);
  translate(width/2, height/2);
  rotate(rotateValue);
  rect(-100, -50, 200, 100);
  popMatrix();
  
  fill(255);
  noStroke();
  text("easing style: " + easingName[easingCounter], width/2, height-100);
}

void keyPressed() {
  if (key == ' ') {
    easingCounter++;
    if(easingCounter > easings.length-1){
      easingCounter = 0;
    }
    rotateAni.setEasing(easings[easingCounter]);
    //rotateAni.setEnd(random(PI, TWO_PI));
    rotateAni.start();
  }
}
