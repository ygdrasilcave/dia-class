import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Ani xAni;
Ani yAni;
float x;
float y;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};



void setup() {
  size(1200, 600);
  background(0);

  x = 10.0;
  y = 10.0;

  Ani.init(this);
  //Ani.noAutostart();

  println(easings.length);

  //Ani(Object Target, float Duration, String FieldName, float End)
  //Ani(Object Target, float Duration, float Delay, String FieldName, float End)
  xAni = new Ani(this, 1.0, "x", 300.0, Ani.LINEAR);
  yAni = new Ani(this, 1.0, "y", 300.0, easings[easingCounter]);

  textAlign(CENTER, CENTER);
  textSize(24);
}

void draw() {
  background(0);

  fill(255);
  ellipse(300, height/2, x, x);
  ellipse(900, height/2, y, y);

  fill(255);
  noStroke();
  text("easing style: " + easingName[easingCounter], width/2, height-100);
}

void keyPressed() {
  if (key == ' ') { 
    if (xAni.isPlaying() == false) {
      float newBegin = xAni.getEnd();
      float newEnd = xAni.getBegin();
      xAni.setBegin(newBegin);
      xAni.setEnd(newEnd);
      xAni.start();
    }
    if(yAni.isPlaying() == false){
      easingCounter++;
      if (easingCounter > easings.length-1) {
        easingCounter = 0;
      }
      yAni.setEasing(easings[easingCounter]);
      float newBegin = yAni.getEnd();
      float newEnd = yAni.getBegin();
      yAni.setBegin(newBegin);
      yAni.setEnd(newEnd);
      yAni.start();
    }
  }
}
