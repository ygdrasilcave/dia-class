int num = 20;
float[] yStartPos;
float t = 0;

void setup(){
  size(1000, 600);
  yStartPos = new float[num];
  for(int i=0; i<num; i++){
    yStartPos[i] = sin(i*HALF_PI*0.25)*100;
  }
}


void draw(){
  background(0);
  fill(255);
  noStroke();
  for(int x = 0; x<num; x++){
    yStartPos[x] = sin(x*HALF_PI*0.25 + t)*100;
    ellipse(x*(width/num), height/2 + yStartPos[x], 30, 30);
  }
  
  t+=0.013;
}
