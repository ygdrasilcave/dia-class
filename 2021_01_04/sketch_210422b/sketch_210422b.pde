PImage img;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");
}

void draw() {
  background(255);
  
  float threshold = map(mouseX, 0, width, 0, 255);
  
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      int index = x + y*width;
      color c = img.pixels[index];
      
      /*float r = 255-red(c);
      float g = 255-green(c);
      float b = 255-blue(c);
      stroke(r, g, b);*/
      
      /*float br = brightness(c);
      if(br > threshold){
        stroke(255);
      }else{
        stroke(0);
      }*/
      
      float br = brightness(c);
      if(br >= 200){
        stroke(255);
      }else if(br < 200 && br >= 150){
        stroke(180);
      }else{
        stroke(0);
      }
      
      point(x, y);
    }
  }
}
