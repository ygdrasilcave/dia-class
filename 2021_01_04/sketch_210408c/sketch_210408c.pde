PImage img_tree;

float scaleFactor;
float offset_x = 0;
float offset_y = 0;
float img_w;
float img_h;

int treeNum = 10;

float[] y = new float[treeNum];

void setup() {
  size(1000, 600);  
  img_tree = loadImage("tree.png");
  scaleFactor = 0.3;
  img_w = img_tree.width * scaleFactor;
  img_h = img_tree.height * scaleFactor;
  offset_x = -img_w/2 + 250*scaleFactor;
  offset_y = -img_h/2 - 505*scaleFactor;
  
  for(int i=0; i < y.length; i++){
    y[i] = random(150);
  }
}

void draw() {
  background(255);
  for (int x=0; x<treeNum; x+=1) {
    image(img_tree, x*(width/treeNum) + offset_x, ((height-100) + offset_y) - y[x], img_w, img_h);
    stroke(255, 3, 197);
    line(x*(width/treeNum) + offset_x, height-100, x*(width/treeNum) + offset_x, height-100 - y[x]);
  }
  
  stroke(0);
  strokeWeight(3);
  line(0, height-100, width, height-100);
}

void mousePressed(){
  for(int i=0; i < y.length; i++){
    y[i] = random(150);
  }
}
