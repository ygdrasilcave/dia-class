PImage img;

int pixelSize = 5;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");
}

void draw() {
  background(0);

  pixelSize = int(map(mouseX, 0, width, 1, 15));

  noStroke();

  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];

      float r = red(c);
      float g = green(c);
      float b = blue(c);

      fill(r, g, b);
      //rect(x, y, pixelSize, pixelSize);
      ellipse(x+pixelSize/2, y+pixelSize/2, pixelSize, pixelSize);
    }
  }
}
