PImage img_tree;
float degree = 0;

float offset_x;
float offset_y;
float scaleFactor;
float offset_y_dir = 1;

void setup() {
  size(1000, 800);
  img_tree = loadImage("tree.png");
  scaleFactor = 0.3;
  offset_y = 505*scaleFactor;
}

void draw() {
  background(255, 200, 0);

  float img_w = img_tree.width*scaleFactor;
  float img_h = img_tree.height*scaleFactor;
  
  offset_x = 250*scaleFactor;
    
  offset_y += offset_y_dir*3;
  if(offset_y > 250){
    offset_y = 250;
    offset_y_dir *= -1;
  }
  if(offset_y < 505*scaleFactor){
    offset_y = 505*scaleFactor;
    offset_y_dir *= -1;
  }
 
  //degree += 0.025;

  /*translate(mouseX, mouseY);
  rotate(degree);
  image(img_tree, -(img_w/2) + 130, -(img_h/2) - 260, img_w, img_h);
  fill(255, 0, 0);
  noStroke();
  rect(-10, -10, 20, 20);*/
  
  //translate(mouseX, mouseY);
  //image(img_tree, -(img_w/2) + offset_x, -(img_h/2) - offset_y, img_w, img_h);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(degree);
  for(int i=0; i<16; i++){
    pushMatrix();
    rotate(i*(TWO_PI/16));
    image(img_tree, -(img_w/2) + offset_x, -(img_h/2) - offset_y, img_w, img_h);
    popMatrix();
  }
  popMatrix();
}
