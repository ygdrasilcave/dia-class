PImage img0, img1, img2, img3, img4, img5, img6;

int imageFrame = 0;

void setup() {
  size(1000, 1000);
  img0 = loadImage("ballet_00.png");
  img1 = loadImage("ballet_01.png");
  img2 = loadImage("ballet_02.png");
  img3 = loadImage("ballet_03.png");
  img4 = loadImage("ballet_04.png");
  img5 = loadImage("ballet_05.png");
  img6 = loadImage("ballet_06.png");

  textSize(24);
}

void draw() {
  background(255, 255, 0);
  if (imageFrame == 0) {
    image(img0, 0, 0);
  }else if(imageFrame == 1) {
    image(img1, 0, 0);
  }else if(imageFrame == 2) {
    image(img2, 0, 0);
  }else if(imageFrame == 3) {
    image(img3, 0, 0);
  }else if(imageFrame == 4) {
    image(img4, 0, 0);
  }else if(imageFrame == 5) {
    image(img5, 0, 0);
  }else if(imageFrame == 6) {
    image(img6, 0, 0);
  }

  fill(0);
  text(imageFrame, 50, 50);
}

void keyPressed() {
  if (key == 'a' || key == 'A') {
    imageFrame++;
    if (imageFrame > 6) {
      imageFrame = 0;
    }
  }
}
