PImage img1;
PImage img2;

int pixelSize = 5;

int cols, rows;

void setup() {
  size(1024, 575);
  img1 = loadImage("face-1.jpg");
  img2 = loadImage("face-2.jpg");
  
  cols = int(width/pixelSize);
  rows = int(height/pixelSize);
}

void draw() {
  background(255);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = x*pixelSize + y*pixelSize*img1.width;
      color c1 = img1.pixels[index];
      color c2 = img2.pixels[index];
      noStroke();
      if (y%2==0) {
        if (x%2==0) {
          fill(c1);
        }else{
          fill(c2);
        }
      } else {
        if (x%2==0) {
          fill(c2);
        }else{
          fill(c1);
        }
      }
      rect(x*pixelSize, y*pixelSize, pixelSize, pixelSize);
    }
  }
  
  pixelSize = int(map(mouseX, 0, width, 1, 20));
  cols = int(width/pixelSize);
  rows = int(height/pixelSize);
}
