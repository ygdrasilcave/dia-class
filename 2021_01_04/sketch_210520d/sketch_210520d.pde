import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Ani xAni;
Ani yAni;
float x;
float y;

float newDuration = 1.0;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};



void setup() {
  size(800, 800);
  colorMode(HSB, 255);

  x = 0.0;
  y = 0.0;

  Ani.init(this);
  //Ani.noAutostart();

  println(easings.length);

  //Ani(Object Target, float Duration, String FieldName, float End)
  //Ani(Object Target, float Duration, float Delay, String FieldName, float End)
  xAni = new Ani(this, newDuration, "x", width, Ani.LINEAR);
  yAni = new Ani(this, newDuration, "y", height, easings[easingCounter]);

  textAlign(CENTER, CENTER);
  textSize(24);
}

void draw() {
  background(0);

  fill(0, 0, 255);
  ellipse(x, height/2, 30, 30);
  ellipse(width/2, y, 30, 30);
  
  float hue = map(y, 0, height, 0, 255);
  fill(hue, 255, 255);
  rect(10, 10, 200, 200);
  
  noFill();
  stroke(0, 0, 255);
  strokeWeight(3);
  ellipse(width-100-10, 100+10, 200, 200);
  
  float r = map(y, 0, height, 0, TWO_PI);
  pushMatrix();
  translate(width-100-10, 100+10);
  rotate(r);
  line(0, 0, 100, 0);
  popMatrix();
  
  float d = map(y, 0, height, 10, 300);
  ellipse(width/2, height/2, d, d);

  fill(0, 0, 255);
  noStroke();
  text("easing style: " + easingName[easingCounter], width/2, height-100);
  text("duration: " + newDuration, width/2, height-50);
}

void keyPressed() {
  if (key == ' ') { 
    newDuration = random(0.5, 3.0);
    if (xAni.isPlaying() == false) {
      float newBegin = xAni.getEnd();
      float newEnd = xAni.getBegin();
      xAni.setBegin(newBegin);
      xAni.setEnd(newEnd);
      xAni.setDuration(newDuration);
      xAni.start();
    }
    if(yAni.isPlaying() == false){
      easingCounter++;
      if (easingCounter > easings.length-1) {
        easingCounter = 0;
      }
      yAni.setEasing(easings[easingCounter]);
      float newBegin = yAni.getEnd();
      float newEnd = yAni.getBegin();
      yAni.setBegin(newBegin);
      yAni.setEnd(newEnd);
      yAni.setDuration(newDuration);
      yAni.start();
    }
  }
}
