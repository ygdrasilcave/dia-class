PImage img;

int cols, rows;
int r;

void setup() {
  size(1280, 720);
  img = loadImage("flower.jpg");

  r = 20;
  cols = int(width/(r*2));
  rows = int(height/(r*2));
}

void draw() {
  background(0);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = (x*(r*2)) + (y*(r*2))*img.width;
      color c = img.pixels[index];
      fill(c);
      noStroke();
      //rect(x*r, y*r, r, r);
      beginShape();
      for (int i=0; i<6; i++) {
        float px = x*(r*2) + cos(radians(-30)+radians(60*i))*r;
        float py = y*(r*2) + sin(radians(-30)+radians(60*i))*r;
        vertex(px, py);
      }
      endShape(CLOSE);
    }
  }

  /*float x = width/2 + cos(radians(270))*100;
   float y = height/2 + sin(radians(270))*100;
   fill(255);
   ellipse(x, y, 5, 5);
   stroke(255);
   line(width/2, height/2, x, y);*/
   
  /*fill(255);
   beginShape();
   for (int i=0; i<6; i++) {
   float x = width/2 + cos(radians(-30)+radians(60*i))*100;
   float y = height/2 + sin(radians(-30)+radians(60*i))*100;
   vertex(x, y);
   }
   endShape(CLOSE);*/
}
