float x = 0;
float s = 10;

void setup() {
  size(1000, 800);
  background(0);
}

void draw() {
  background(0);

  stroke(255);
  noFill();
  ellipse(x, height/2, 200, 200);

  x += s;

  if (x <= 0) {
    x = 0;
    s = resetVelocity(s);
  }
  if (x >= width) {
    x = width;
    s = resetVelocity(s);
  }
}

float resetVelocity(float currentVelocity) {
  float rs = random(0.1, 2);
  float newVelocity = 10*rs;
  if (currentVelocity > 0) {
    newVelocity = newVelocity * 1;
  } else if (currentVelocity < 0) {
    newVelocity = newVelocity * -1;
  }
  newVelocity = (newVelocity) * -1;
  println(newVelocity);
  return newVelocity;
}
