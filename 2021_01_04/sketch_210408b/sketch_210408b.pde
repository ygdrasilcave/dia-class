PImage img_tree;
float degree = 0;

int treeNum = 8;

float offset_x;

float offset_y0;
float offset_y1;
float offset_y2;
float offset_y3;
float offset_y4;
float offset_y5;
float offset_y6;
float offset_y7;

float scaleFactor;
float offset_y_dir = 1;

void setup() {
  size(1000, 800);
  img_tree = loadImage("tree.png");
  scaleFactor = 0.3;
  offset_x = 250*scaleFactor;
}

void draw() {
  background(255, 200, 0);

  float img_w = img_tree.width*scaleFactor;
  float img_h = img_tree.height*scaleFactor;

  //offset_y = -505*scaleFactor + map(mouseX, 0, width, -200, 200);

  offset_y0 = -505*scaleFactor - 0;
  offset_y1 = -505*scaleFactor - 10;
  offset_y2 = -505*scaleFactor - 20;
  offset_y3 = -505*scaleFactor - 30;
  offset_y4 = -505*scaleFactor - 40;
  offset_y5 = -505*scaleFactor - 50;
  offset_y6 = -505*scaleFactor - 60;
  offset_y7 = -505*scaleFactor - 70;

  pushMatrix();
  translate(width/2, height/2);
  rotate(degree);
  for (int i=0; i<treeNum; i++) {
    pushMatrix();
    rotate(i*(TWO_PI/treeNum));
    //image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_yi, img_w, img_h);
    if (i == 0) {
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y0, img_w, img_h);
    }else if(i == 1){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y1, img_w, img_h);
    }else if(i == 2){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y2, img_w, img_h);
    }else if(i == 3){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y3, img_w, img_h);
    }else if(i == 4){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y4, img_w, img_h);
    }else if(i == 5){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y5, img_w, img_h);
    }else if(i == 6){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y6, img_w, img_h);
    }else if(i == 7){
      image(img_tree, -(img_w/2) + offset_x, -(img_h/2) + offset_y7, img_w, img_h);
    }
    popMatrix();
  }
  popMatrix();
}
