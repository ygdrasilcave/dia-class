PImage img;
PImage tree;

PGraphics pg1, pg2, pg3, pg4, pg5, pg6, pg7, pg8;

void setup(){
  size(1200, 1400);
  img = loadImage("Tulips.jpg");
  tree = loadImage("tree.png");
  
  int img_w = img.width;
  int img_h = img.height;
  pg1 = createGraphics(img_w, img_h);
  pg2 = createGraphics(img_w, img_h);
  pg3 = createGraphics(img_w, img_h);
  pg4 = createGraphics(img_w, img_h);
  pg5 = createGraphics(img_w, img_h);
  pg6 = createGraphics(img_w, img_h);
  pg7 = createGraphics(img_w, img_h);
  pg8 = createGraphics(img_w, img_h);
}

void draw(){
  background(255);
  
  float thresholdValue = map(mouseX, 0, width, 0, 1.0);
  float posterizeValue = map(mouseX, 0, width, 2, 12);
  float blurValue = map(mouseX, 0, width, 1, 12);
  
  pg1.beginDraw();
  pg1.image(img, 0, 0);
  pg1.filter(THRESHOLD, thresholdValue);
  pg1.endDraw();  
  image(pg1, 0, 0);
  
  pg2.beginDraw();
  pg2.image(img, 0, 0);
  pg2.filter(GRAY);
  pg2.endDraw();  
  image(pg2, width/2, 0);
  
  pg3.beginDraw();
  pg3.image(img, 0, 0);
  pg3.filter(OPAQUE);
  pg3.endDraw();  
  image(pg3, 0, img.height);
  
  pg4.beginDraw();
  pg4.image(img, 0, 0);
  pg4.filter(INVERT);
  pg4.endDraw();  
  image(pg4, width/2, img.height);
  
  pg5.beginDraw();
  pg5.image(img, 0, 0);
  pg5.filter(POSTERIZE, posterizeValue);
  pg5.endDraw();  
  image(pg5, 0, img.height*2);
  
  pg6.beginDraw();
  pg6.image(img, 0, 0);
  pg6.filter(BLUR, blurValue);
  pg6.endDraw();  
  image(pg6, width/2, img.height*2);
  
  pg7.beginDraw();
  pg7.image(img, 0, 0);
  pg7.filter(ERODE);
  pg7.endDraw();  
  image(pg7, 0, img.height*3);
  
  pg8.beginDraw();
  pg8.image(img, 0, 0);
  pg8.filter(DILATE);
  pg8.endDraw();  
  image(pg8, width/2, img.height*3);
}
