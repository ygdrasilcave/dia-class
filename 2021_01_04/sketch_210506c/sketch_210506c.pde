float x;
float y;
int prevSecond;
boolean toggle = true;

void setup() {
  size(1200, 1200);
  x = width/2;
  y = height/2;
  prevSecond = second();
}

void draw() {
  background(255);
  color c = color(255, 0, 255);
  int vertexNum = int(map(mouseX, 0, width, 3, 36));
  drawMyPixel(x, y, 100, vertexNum, c);

  /*if (prevSecond != second()) {
    x = random(width);
    y = random(height);
    prevSecond = second();
  }*/
  
  //hour();
  //minute();
  //second();
  
  if(second()%3 == 0 && toggle == true){
    x = random(width);
    y = random(height);
    toggle = false;
  }else if(second()%3 == 1 && toggle == false){
    toggle = true;
  }
  
  //println(second());
}

void drawMyPixel(float _posX, float _posY, float _r, float _n, color _c) {
  fill(_c);
  noStroke();
  beginShape();
  for (int i=0; i<_n; i++) {
    float px = _posX + cos(radians(-(360/_n)/2)+radians((360/_n)*i))*_r;
    float py = _posY + sin(radians(-(360/_n)/2)+radians((360/_n)*i))*_r;
    vertex(px, py);
  }
  endShape(CLOSE);
}
