void setup() {
  size(800, 600);
  background(255, 52, 222);
  //println("setup");
}

void draw() {
  background(255, 52, 222);
  
  strokeWeight(20);
  stroke(0, 0, 255);
  point(width/2, height/2);

  //strokeWeight(3);
  //stroke(62, 252, 0);
  noStroke();
  fill(255, 255, 0);
  rect(width/2, height/2, 200, 100);

  strokeWeight(2);
  stroke(0);
  noFill();
  ellipse(width/2, height/2, 100, 150);
  
  strokeWeight(5);
  stroke(0, 50, 255);
  line(width/2, height/2, mouseX, mouseY);
  
  noFill();
  quad(width/2, height/2, width/2+100, height/2+120, width/2+250, height/2-20, width/2+150, height/2-200);
  
  rect(width/2-(width-100)/2, height/2-(height-100)/2, width-100, height-100);
  
  //println("draw");
}
