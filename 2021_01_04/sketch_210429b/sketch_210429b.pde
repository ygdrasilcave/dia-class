PImage img;

int cols, rows;
int r;
float t = 0.0;

void setup() {
  size(600, 350);
  img = loadImage("Tulips.jpg");

  r = 10;
  cols = int(width/r);
  rows = int(height/r);
}

void draw() {
  //image(img, 0, 0);
  background(0);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = (x*r) + (y*r)*img.width;
      color c = img.pixels[index];
      float br = brightness(c);
      float rotVal = map(br, 0, 255, 0, PI+HALF_PI);
      stroke(c);
      strokeWeight(5);
      pushMatrix();
      translate(x*r, y*r);
      rotate(rotVal + map(noise(t),0,1,-3,3));
      line(0, 0, (float(r)/255.0)*br, 0);
      popMatrix();
    }
  }
  
  t+=0.0065;
}
