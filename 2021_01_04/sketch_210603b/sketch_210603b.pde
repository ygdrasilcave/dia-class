int scene = 1;

void setup() {
  size(1000, 800);
  background(0);
}

void draw() {
  if (scene == 1) {
    pushMatrix();
    translate(0, 0);
    scene01();
    popMatrix();
  } else if (scene == 2) {
    pushMatrix();
    translate(0, 0);
    scene02();
    popMatrix();
  } else if (scene == 3) {
    pushMatrix();
    translate(0, 0);
    scene03();
    popMatrix();
  }
}


void scene01() {
  background(0);
  fill(255, 255, 0);
  rect(width/2-250, height/2-250, 500, 500);
}

void scene02() {
  background(0);
  fill(255, 255, 0);
  ellipse(width/2, height/2, 400, 400);
}

void scene03() {
  background(0);
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(45));
  fill(255, 0, 255);
  rect(-250, -250, 500, 500);
  popMatrix();
}

void keyPressed() {
  if (key == '1') {
    scene = 1;
  }
  if (key == '2') {
    scene = 2;
  }
  if (key == '3') {
    scene = 3;
  }
}
