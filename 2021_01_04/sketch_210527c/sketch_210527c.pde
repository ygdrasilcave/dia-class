import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

int easingCounter = 0;
Easing[] easings = {Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, 
  Ani.CUBIC_IN, Ani.CUBIC_OUT, Ani.CUBIC_IN_OUT, Ani.QUART_IN, Ani.QUART_OUT, 
  Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, 
  Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, 
  Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, 
  Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, 
  Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};

String[] easingName = {"Ani.LINEAR", "Ani.QUAD_IN", "Ani.QUAD_OUT", "Ani.QUAD_IN_OUT", 
  "Ani.CUBIC_IN", "Ani.CUBIC_OUT", "Ani.CUBIC_IN_OUT", "Ani.QUART_IN", "Ani.QUART_OUT", 
  "Ani.QUART_IN_OUT", "Ani.QUINT_IN", "Ani.QUINT_OUT", "Ani.QUINT_IN_OUT", "Ani.SINE_IN", 
  "Ani.SINE_OUT", "Ani.SINE_IN_OUT", "Ani.CIRC_IN", "Ani.CIRC_OUT", "Ani.CIRC_IN_OUT", 
  "Ani.EXPO_IN", "Ani.EXPO_OUT", "Ani.EXPO_IN_OUT", "Ani.BACK_IN", "Ani.BACK_OUT", 
  "Ani.BACK_IN_OUT", "Ani.BOUNCE_IN", "Ani.BOUNCE_OUT", "Ani.BOUNCE_IN_OUT", 
  "Ani.ELASTIC_IN", "Ani.ELASTIC_OUT", "Ani.ELASTIC_IN_OUT" 
};

float w, h, rot, white;

Ani wAni;
Ani hAni;
Ani rotAni;
Ani whiteAni;

boolean[] isDone = {false, false, false, false};

boolean done01 = false;
boolean done01 = false;

void setup() {
  size(800, 800);
  background(0);

  Ani.init(this);

  w = 0;
  h = 0;
  rot = 0;
  white = 0;

  wAni = new Ani(this, 5.0, "w", width/2, easings[easingCounter], "onEnd:wAniEndEvt");
  hAni = new Ani(this, 2.0, "h", width/2, easings[easingCounter], "onEnd:hAniEndEvt");
  rotAni = new Ani(this, 5.0, "rot", TWO_PI, easings[easingCounter], "onEnd:rotAniEndEvt");
  whiteAni = new Ani(this, 6.0, "white", 255, easings[easingCounter], "onEnd:whiteAniEndEvt");

  rectMode(CENTER);
}

void draw() {
  noFill();
  stroke(white);
  translate(width/2, height/2);
  rotate(rot);
  rect(0, 0, w, h);
  rect(0, 0, h, w);
  //ellipse(0, 0, w*2, h*2);

  int total = 0;
  for (int i=0; i<isDone.length; i++) {
    if (isDone[i] == true) {
      total++;
    }
  }

  if (total == 4) {
    println("done");
    saveFrame("myWork-######.tiff");
    
    int easingIndex = int(random(0, 31));
    
    wAni.setBegin(random(width/2));
    wAni.setEnd(random(width/2));
    wAni.setDuration(random(1, 6));
    wAni.setEasing(easings[easingIndex]);
    
    easingIndex = int(random(0, 31));
    
    hAni.setBegin(random(height/2));
    hAni.setEnd(random(height/2));
    hAni.setDuration(random(1, 6));
    hAni.setEasing(easings[easingIndex]);
    
    easingIndex = int(random(0, 31));
    
    rotAni.setBegin(random(TWO_PI));
    rotAni.setEnd(random(TWO_PI));
    rotAni.setDuration(random(1, 6));
    rotAni.setEasing(easings[0]);
    
    easingIndex = int(random(0, 31));
    
    whiteAni.setBegin(0);
    whiteAni.setEnd(255);
    whiteAni.setDuration(random(1, 6));  
    whiteAni.setEasing(easings[0]);
    
    wAni.start();
    hAni.start();
    rotAni.start();
    whiteAni.start();
    for (int i=0; i<isDone.length; i++) {
      isDone[i] = false;
    }
    background(0);
  }
}

void wAniEndEvt() {
  isDone[0] = true;
}

void hAniEndEvt() {
  isDone[1] = true;
}

void rotAniEndEvt() {
  isDone[2] = true;
}

void whiteAniEndEvt() {
  isDone[3] = true;
}
