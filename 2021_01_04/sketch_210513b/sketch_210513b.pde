PImage[] img;

int imageFrame = 0;

void setup() {
  size(1000, 1000);
  
  img = new PImage[7];
  
  for (int i=0; i<img.length; i++) {
    img[i] = loadImage("ballet_" + nf(i, 2) + ".png");
  }

  textSize(24);
}

void draw() {
  background(255, 255, 0);

  image(img[imageFrame], 0, 0);

  fill(0);
  text(imageFrame, 50, 50);
}

void keyPressed() {
  if (key == 'a' || key == 'A') {
    imageFrame++;
    if (imageFrame > img.length-1) {
      imageFrame = 0;
    }
  }
}
