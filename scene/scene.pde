IntList scene = new IntList();
int nextScene = 0;

void setup() {
  size(500, 500);
  for (int i=0; i<5; i++) {
    scene.set(i, i);
  }
  scene.shuffle();
  println(scene);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(255);
  sceneFunc(scene.get(nextScene));

  if (fadeStart == true) {
    transition(fade, next);
  }
}

void sceneFunc(int _num) {
  if (_num == 0) {
    background(0);
    fill(255);
    noStroke();
    rect(width/2-50, height/2-50, 100, 100);
  } else if (_num == 1) {
    background(0);
    fill(255);
    noStroke();
    ellipse(width/2, height/2, 100, 100);
  } else if (_num == 2) {
    background(0);
    stroke(255);
    noFill();
    rect(width/2-50, height/2-50, 100, 100);
  } else if (_num == 3) {
    background(0);
    noFill();
    stroke(255);
    ellipse(width/2, height/2, 100, 100);
  } else if (_num == 4) {
    background(0);
    fill(255);
    noStroke();
    text("END", width/2, height/2);
  }
}

int fadeVal = 0;
boolean fade = true;
boolean fadeStart = false;
int next = 0;
void transition(boolean _inOut, int _n) {  
  if (_inOut == true) {
    fadeVal+=20;
    if (fadeVal > 255) {
      fadeVal = 255;
      fade = false;
      nextScene += 1;          
      if (nextScene > scene.size()-1) {
        scene.shuffle();
        println(scene);
        nextScene = 0;
      }
      //nextScene = _n;
      println(nextScene);
    }
  } else {
    fadeVal-=20;
    if (fadeVal < 0) {
      fadeVal = 0;
      fade = true;
      fadeStart = false;
    }
  }
  fill(0, fadeVal);
  noStroke();
  rect(0, 0, width, height);
}

void keyPressed() {
  if (key == '1') {
    fadeStart = true;
    next = 0;
  } else if (key == '2') {
    fadeStart = true;
    next = 1;
  } else if (key == '3') {
    fadeStart = true;
    next = 2;
  } else if (key == '4') {
    fadeStart = true;
    next = 3;
  } else if (key == '5') {
    fadeStart = true;
    next = 4;
  }
}

