let sp_A;
let sp_B;

function setup() {
  new Canvas(600, 800);

  sp_A = new Sprite(width/2, height/2, 100);
  sp_B = new Sprite(width/2+50, height/2, 100, 100);
  
  sp_A.layer = 100;
  sp_B.layer = 1;
  sp_A.text = '100번';
  sp_A.textSize = 20;
  sp_B.text = '1번';
  sp_B.textSize = 20;
  sp_A.overlaps(sp_B);
  //sp_B.vel.x = 0.5;
}

function draw() {
  clear();
  background(255);

  //console.log(sp_A.overlapped(sp_B));
}