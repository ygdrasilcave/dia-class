let tileSize = 70;
let img_tree, img_fence, img_waterEdge, img_water, img_ground, img_grass, img_house1, img_house2;
let gr_trees, gr_fence, gr_waterEdge, gr_water, gr_ground, gr_grass, gr_house1, gr_house2;

let img_switchOff, img_switchOn, img_player, img_signExit;
let sp_switch, sp_player;

let switchValue = 0;

let imgFolder1 = '/assets/Platformer Art Complete Pack/Base pack/Tiles/';
let imgFolder2 = '/assets/Platformer Art Complete Pack/Candy expansion/Tiles/';
let imgFolder3 = '/assets/Platformer Art Complete Pack/Ice expansion/Tiles/';
let imgFolder4 = '/assets/Platformer Art Complete Pack/Request pack/Tiles/';

let mySound;

function preload() {
    img_tree = loadImage(imgFolder2 + 'lollipopFruitGreen.png');
    img_fence = loadImage(imgFolder1 + 'ladder_mid.png');
    img_waterEdge = loadImage(imgFolder1 + 'castle.png');
    img_water = loadImage(imgFolder1 + 'liquidWater.png');
    img_ground = loadImage(imgFolder1 + 'brickWall.png');
    img_grass = loadImage(imgFolder2 + 'hillCaneGreen.png');
    img_house1 = loadImage(imgFolder4 + 'beamHoles.png');
    img_house2 = loadImage(imgFolder1 + 'boxCoin.png');

    img_switchOff = loadImage(imgFolder4 + 'laserSwitchBlueOff.png');
    img_switchOn = loadImage(imgFolder4 + 'laserSwitchBlueOn.png');
    img_player = loadImage('/assets/image/questKid.png');
    img_signExit = loadImage(imgFolder1 + 'signExit.png');

    soundFormats('mp3', 'ogg');
    mySound = loadSound('/assets/sound/doorbell');
}

function setup() {
    new Canvas(700, 700);
    background(255);
    allSprites.rotationLock = true;

    gr_trees = new Group();
    gr_trees.w = tileSize;
    gr_trees.h = tileSize;
    gr_trees.tile = 't';
    gr_trees.collider = 's';
    gr_trees.image = img_tree;
    gr_trees.scale = 1.0;

    gr_fence = new Group();
    gr_fence.w = tileSize;
    gr_fence.h = tileSize;
    gr_fence.tile = 'f';
    gr_fence.collider = 's';
    gr_fence.image = img_fence;
    gr_fence.scale = 1.0;

    gr_fence2 = new Group();
    gr_fence2.w = tileSize;
    gr_fence2.h = tileSize;
    gr_fence2.tile = 'F';
    gr_fence2.collider = 's';
    gr_fence2.image = img_fence;
    gr_fence2.scale = 1.0;

    gr_waterEdge = new Group();
    gr_waterEdge.w = tileSize;
    gr_waterEdge.h = tileSize;
    gr_waterEdge.tile = 'e';
    gr_waterEdge.collider = 's';
    gr_waterEdge.image = img_waterEdge;
    gr_waterEdge.scale = 1.0;

    gr_water = new Group();
    gr_water.w = tileSize;
    gr_water.h = tileSize;
    gr_water.tile = 'w';
    gr_water.collider = 'n';
    gr_water.image = img_water;
    gr_water.scale = 1.0;

    gr_ground = new Group();
    gr_ground.w = tileSize;
    gr_ground.h = tileSize;
    gr_ground.tile = 'g';
    gr_ground.collider = 'n';
    gr_ground.image = img_ground;
    gr_ground.scale = 1.0;

    gr_grass = new Group();
    gr_grass.w = tileSize;
    gr_grass.h = tileSize;
    gr_grass.tile = '~';
    gr_grass.collider = 'n';
    gr_grass.image = img_grass;
    gr_grass.scale = 1.0;

    gr_house1 = new Group();
    gr_house1.w = tileSize;
    gr_house1.h = tileSize;
    gr_house1.tile = 'h';
    gr_house1.collider = 'n';
    gr_house1.image = img_house1;
    gr_house1.scale = 1.0;

    gr_house2 = new Group();
    gr_house2.w = tileSize;
    gr_house2.h = tileSize;
    gr_house2.tile = 'o';
    gr_house2.collider = 'n';
    gr_house2.image = img_house2;
    gr_house2.scale = 1.0;

    new Tiles(
        [
            'tttttttttt',
            'tgggggoogt',
            'tgggggoogt',
            'tggggggggt',
            'tgggggg~~t',
            'tgggggg~~t',
            'teeeegg~~t',
            'tewwegg~~t',
            'tewwegg~~t',
            'tffffFFfft',
        ],
        35, 35,
        tileSize, tileSize
        );

    sp_switch = new Sprite(140+35, 140+35, 'd');
    sp_switch.image = img_switchOff;
    sp_switch.w = tileSize;
    sp_switch.h = tileSize;
    sp_switch.scale = 1.0;

    sp_player = new Sprite();
    sp_player.spriteSheet = img_player;
    sp_player.w = 32;
    sp_player.h = 32;
    sp_player.x = width/2;
    sp_player.y = height/2;
    sp_player.collider = 'd';
    sp_player.anis.offset.x = 2;
    sp_player.anis.offset.y = 1;
    sp_player.anis.frameDelay = 2;
    sp_player.addAnis({
        up: { row: 5, col: 0, frames: 4 },
        down: { row: 7, frames: 8 },
        //run: { row: 2, frames: 5, frameDelay: 14 },
        run: { row: 0, col: 4, frames: 4, rotation: 0 },
        stand: { row: 3, frames: 1 }
        });
    sp_player.scale = 2.5;
    sp_player.changeAni('stand');
    sp_player.debug = true;
}

function draw() {
    clear();

    //camera.zoom = 2.0;
    //camera.x = sp_player.x;
    //camera.y = sp_player.y;

    if(kb.pressing('a')){
        //sp_player.move(tileSize*0.25, 'left', 1);
        sp_player.vel.x = -1;
        sp_player.changeAni('run');
        sp_player.mirror.x = true;
    }else if(kb.pressing('d')){
        //sp_player.move(tileSize*0.25, 'right', 1);
        sp_player.vel.x = 1;
        sp_player.changeAni('run');
        sp_player.mirror.x = false;
    }else if(kb.pressing('w')){
        //sp_player.move(tileSize*0.25, 'up', 1);
        sp_player.vel.y = -1;
        sp_player.changeAni('up');
    }else if(kb.pressing('s')){
        //sp_player.move(tileSize*0.25, 'down', 1);
        sp_player.vel.y = 1;
        sp_player.changeAni('down');
    }else{
        sp_player.vel.x = 0;
        sp_player.vel.y = 0;
        sp_player.changeAni('stand');
    }

    if (sp_player.overlapping(sp_switch)){
        sp_switch.image = img_switchOn;
        gr_fence2.image = img_signExit;
        gr_fence2.collider = 'd';
        switchValue = 1;
    }
    //else{
    //    sp_switch.image = img_switchOff;
    //}
    if(switchValue == 1){
        if (sp_player.overlaps(gr_fence2)){
            mySound.play();
            //window.open('/page01', '_self');
            //window.open('https://www.yonsei.ac.kr', '_blank');
            window.open('/page01', '_blank');
        }
    }
}
