let sp_player, sp_coin1, sp_coin2, sp_coin3;
let count = 0;
let mySound;
let isPlay = false;

function preload(){
  //soundFormats('mp3', 'ogg');
  mySound = loadSound("/assets/sound/Soni_Ventorum_Wind_Quintet.mp3");
}

function setup() {
  new Canvas(600, 800);

  sp_coin1 = new Sprite(random(width), random(height), 30);
  sp_coin2 = new Sprite(random(width), random(height), 30);
  sp_coin3 = new Sprite(random(width), random(height), 30);
  sp_coin1.color = 'yellow';
  sp_coin2.color = 'rec';
  sp_coin3.color = 'blue';
  sp_coin1.text = '1';
  sp_coin2.text = '2';
  sp_coin3.text = '3';
  sp_player = new Sprite(50, 50);

  textAlign(CENTER, CENTER);
  textSize(120);
}

function draw() {
  clear();
  background(255);
  sp_player.moveTowards(mouse);
    
  if (sp_player.collides(sp_coin1)){
    sp_coin1.remove();
    count++;
  }
  if (sp_player.overlaps(sp_coin2)){
    sp_coin2.remove();
    count++;
  }
  if (sp_player.overlaps(sp_coin3)){
    sp_coin3.remove();
    count++;
  }
  if(count >= 3){
    fill(0);
    noStroke();
    text("수업 끝", width/2, height/2);
    if(isPlay == false){
      mySound.play();
      isPlay = true;
    }
  }
  console.log(count);
}