let monsterImg, monsterMess;
let sp_monster;
let sp_player, sp_pillar;
let counter;

function preload(){
  monsterImg = loadImage('/assets/image/monster.png');
  monsterMess = loadImage('/assets/image/mess.png');
}

function setup() {
  new Canvas(600, 800);
  world.gravity.y = 10;
  
  // x, y, width, height, collider
  //sp_pillar = new Sprite(width/2, 550, 30, 500, 'static')
  // x, y, diameter
  sp_player = new Sprite(width/2, 0, 50);

  sp_monster = new Sprite();
  sp_monster.img = monsterImg;
  sp_monster.x = width/2;
  sp_monster.y = 550;
  sp_monster.diameter = 85;
  sp_monster.collider = 'static';
  sp_monster.debug = true;
  sp_monster.friction = 10;
  sp_monster.drag = 10;
  counter = 0;
}

function draw() {
  clear();
  background(255);

  if (sp_player.colliding(sp_monster)) {
    counter++;
    sp_player.vel.y = -5/counter;
    //sp_pillar.h -= 52;
    //if(sp_pillar.h < 10){
    //  sp_pillar.h = 10;
    //}
    //console.log(sp_pillar.h);
    //console.log(sp_player.vel.y);
    sp_monster.img = monsterMess;    
  }else{
    sp_monster.img = monsterImg;
  }
}