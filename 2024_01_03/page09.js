let monsterImg;
let sp_monster;
let sp_box;

function preload(){
  monsterImg = loadImage('/assets/image/monster.png');
}

function setup() {
  new Canvas(600, 600);

  sp_monster = new Sprite();
  sp_monster.img = monsterImg;
  sp_monster.diameter = 70;
  sp_monster.x = 100;
  sp_monster.y = 100;
  //dynamic, static, kinematic, none
  sp_monster.collider = 'kinematic';
  //sp_monster.collider = 'dynamic';

  // velocity
  //sp_monster.vel.x = 1;
  //sp_monster.vel.y = 1;

  // direction and speed
  //sp_monster.direction = 45;
  //sp_monster.speed = 1;

  sp_box = new Sprite();
  sp_box.x = width/2;
  sp_box.y = height/2;
}

function draw() {
  clear();
  background(255);
  if (kb.presses('2')) {
    sp_monster.scale = 2;
  }
  sp_monster.debug = mouse.pressing();

  //sp_monster.x = mouse.x;
  //sp_monster.y = mouse.y;

  /*if (kb.presses('left')) {
    //(distance, direction, speed)
    sp_monster.move(30, 'left', 3);
  }else if(kb.presses('right')) {
    //(distance, direction, speed)
    sp_monster.move(30, 'right', 3);
  }else if(kb.presses('up')) {
    //(distance, direction, speed)
    sp_monster.move(30, 'up', 3);
  }else if(kb.presses('down')) {
    //(distance, direction, speed)
    sp_monster.move(30, 'down', 3);
  }*/

  /*if(mouse.pressing()){
   // target x, y
   // (x, y, tracking)
   sp_monster.moveTowards(mouse.x, mouse.y, 0.1);
  }*/
  if(mouse.presses()){
    // (x, y, speed)
    sp_monster.moveTo(mouse.x, mouse.y, 8);
  }

  /*if (kb.pressing('left')){
    sp_monster.vel.x = -5;
  }else if (kb.pressing('right')){
    sp_monster.vel.x = 5;
  }else if (kb.pressing('up')){
    sp_monster.vel.y = -5;
  }else if (kb.pressing('down')){
    sp_monster.vel.y = 5;
  }else{
    sp_monster.vel.x = 0;
    sp_monster.vel.y = 0;
  }*/
}


/*
let smiley;
let smileText1 = `
..yyyyyy
.yybyybyy
yyyyyyyyyy
yybyyyybyy
.yybbbbyy
..yyyyyy`;

  let smileText2 = `
..yyyyyy
.yybyybyy
yyyyyyyyyy
yyybbbbyyy
.ybyyyyby
..yyyyyy`;

function setup() {
  new Canvas(500, 500);

  let palette = {
    y: color(60, 220, 255),
    b: color('#303060')
  };

  smiley = new Sprite();
  smiley.img = spriteArt(smileText1, 32, palette);
}

function draw() {
  background(255);

  if(kb.pressing('s')){
    let palette = {
      y: color(255, 255, 0),
      b: color('#303060')
    };
    smiley.img = spriteArt(smileText1, 32, palette);
  }else{
    let palette = {
      y: color(60, 220, 255),
      b: color('#303060')
    };
    smiley.img = spriteArt(smileText2, 32, palette);
  }
}*/