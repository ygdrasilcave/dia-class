let sp_big, sp_small;

function setup() {
  new Canvas(500, 200);
  sp_big = new Sprite(250, 100, 100, 100);
  sp_small = new Sprite(30, 30, 20, 20);
  camera.on();
}

function draw() {
  clear();
  background(255);

  camera.on();
  camera.x = sin(frameCount) * 200 + 250;

  if (sp_big.mouse.presses()) sp_big.rotation += 10;

  if (sp_big.mouse.hovering()) sp_big.color = '#ff0000';
  else sp_big.color = '#224477';

  sp_big.draw();

  camera.off();

  if (sp_small.mouse.presses()) sp_small.rotation += 10;

  if (sp_small.mouse.hovering()) sp_small.color = '#ff0000';
  else sp_small.color = '#224477';

  sp_small.draw();
}