let angle = 0.0;
let t = 0.0;

function setup() {
  createCanvas(800, 800);
  background(204);
}

function draw() {
  noFill();
  strokeWeight(0.15);
  translate(width/2, height/2);
  rotate(angle);  
  let offsetX;
  let offsetY;
  let w = 400;
  if(mouseIsPressed == true){
    stroke(0);
    offsetX = map(mouseX, 0, width, -200, 200);
    offsetY = map(mouseY, 0, height, -200, 200);
  }else{
    stroke(255);
    //w = map(mouseX, 0, width, 50, 600);
    w = map(sin(t), -1, 1, 5, 600);
    offsetX = -w*0.5;
    offsetY = -w*0.5;    
  }
  rect(offsetX, offsetY, w, w);
  angle += 1;
  t += 0.25;
}

function keyPressed(){
  if(key == ' '){
    background(204);
  }
}
