let t = 0.0;

function setup() {
  createCanvas(800, 800);
  background(204);
}

function draw() {
  background(204);
  noFill();
  stroke(0);
  strokeWeight(0.15);
  /*
  let offsetX = map(mouseX, 0, width, -200, 300);
  let offsetY = map(mouseY, 0, height, -200, 300);
  for(let i=0; i<360; i+=2){
    push();
    translate(width/2, height/2);
    rotate(i);  
    //rect(-200, -200, 400, 400);
    ellipse(offsetX, offsetY, 600, 20);
    //line(-10, -20, 350, 200);
    pop();
  }
  */
  /*for(let i=0; i<720; i+=2){
    push();
    translate(width/2, height/2);
    rotate(i);
    let w = map(i, 0, 720, 400, 20);
    rect(-w*0.5, -w*0.5, w, w);
    pop();
  }*/
  let a = map(sin(t), -1, 1, -360, 360);
  push();
  translate(mouseX, mouseY);
  for(let i=800; i>0; i-=2){
    push();
    //translate(width/2, height/2);
    rotate(map(i, 0, 800, a, 0));
    rect(-i*0.5, -i*0.5, i, i);
    ellipse(0, 0, i, i*0.3);
    pop();
  }
  pop();
  t += 0.25;
}