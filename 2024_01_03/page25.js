let gr_grass, gr_grassUnder;
let gr_water, gr_waterTop;
let tilesGroup;

let imgFolder = '/assets/Platformer Art Complete Pack/Base pack/Tiles/';
let grassImg, grassUnderImg;
let waterImg, waterTopImg;

function preload(){
  grassImg = loadImage(imgFolder + 'grassMid.png');
  grassUnderImg = loadImage(imgFolder + 'grassCenter.png');
  waterImg = loadImage(imgFolder + 'liquidWater.png');
  waterTopImg = loadImage(imgFolder + 'liquidWaterTop_mid.png');
}

function setup() {
  new Canvas(700, 800);

  world.gravity.y = 10;

  gr_grass = new Group();
  gr_grass.image = grassImg;
  gr_grass.w = 70;
  gr_grass.h = 70;
  gr_grass.tile = 'g';
  gr_grass.collider = 's';

  gr_grassUnder = new Group();
  gr_grassUnder.image = grassUnderImg;
  gr_grassUnder.w = 70;
  gr_grassUnder.h = 70;
  gr_grassUnder.tile = 'u';
  gr_grassUnder.collider = 's';

  gr_water = new Group();
  gr_water.image = waterImg;
  gr_water.w = 70;
  gr_water.h = 70;
  gr_water.tile = 'w';
  gr_water.collider = 's';

  gr_waterTop = new Group();
  gr_waterTop.image = waterTopImg;
  gr_waterTop.w = 70;
  gr_waterTop.h = 70;
  gr_waterTop.tile = 't';
  gr_waterTop.collider = 's';

  tilesGroup = new Tiles(
    [
      '..........',
      '..........',
      '..........',
      '..........',
      'ggggttgggg',
      'uuuuwwuuuu',
      'uuuuwwuuuu',
      'uuuuuuuuuu',
      'uuuuuuuuuu'
    ],
    // tile group w, y position
    35, 35,
    // each tile width, height
    gr_grass.w,
    gr_grass.h
  );

  new Sprite(200, 100, 50);
}

function draw() {
  clear();
  background(255);
}