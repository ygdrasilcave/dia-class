let gr_dots;

function setup() {
  new Canvas(600, 800);

  gr_dots = new Group();
  gr_dots.color = color(255, 255, 0);
  gr_dots.y = height/2;
  gr_dots.diameter = 20;
  //gr_dots.collider = 'n';

  console.log(gr_dots.length);
  
  // gr_dots.length = 0
  while (gr_dots.length < 10) {
    let sp_dot = new gr_dots.Sprite();
    // gr_dots.length = 1, 2, 3, .... 
    sp_dot.x = gr_dots.length * 30;

    if(gr_dots.length == 3){
      sp_dot.color = color(255, 0, 0);
      sp_dot.d = 25;
    }
  }

  gr_dots[4].color = color(0, 0, 255);
  gr_dots[4].d = 25;

  // gr_dots.length = 10
  console.log(gr_dots.length);
}

function draw() {
  clear();
  background(255);
  gr_dots.moveTowards(mouse);
}