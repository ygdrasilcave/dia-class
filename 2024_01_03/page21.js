let player, gr_gems;

function setup() {
  new Canvas(600, 800);

  world.gravity.y = 9;

  gr_gems = new Group();
  gr_gems.diameter = 15;
  //gr_gems.x = () => random(0, canvas.w);
  //gr_gems.y = () => random(0, canvas.h);
  gr_gems.x = (i) => (i%30)*20;
  gr_gems.y = (i) => int(i/30)*20;
  gr_gems.amount = 1500;
  gr_gems.color = color(0, 255, 255);
  gr_gems.stroke = color(255);
  //gr_gems.text = (i) => i;

  sp_player = new Sprite();

  let sp_floor = new Sprite(width/2, height-25, width, 50, 's');
  let sp_wall1 = new Sprite(5, height/2, 10, height, 's');
  let sp_wall2 = new Sprite(width-5, height/2, 10, height, 's');
}

function draw() {
  clear();
  background(255);
  sp_player.moveTowards(mouse);
}