function setup(){
  createCanvas(800, 800);
}

function draw(){
  background(0, 255, 125);
  //rect(mouseX-50, mouseY-50, 100, 100);
  //ellipse(650/2, 800/2+300/2, 100, 100);

  noStroke();
  fill(255);
  ellipse(width/2+100, height/2-130, 100, 100);
  ellipse(width/2-100, height/2-130, 100, 100);

  let diffX = mouseX - width/2;
  let diffY = mouseY - height/2;
  let dist = sqrt(diffX*diffX + diffY*diffY);
  
  if(dist < 150){
  //if((mouseX > width/2-150) && (mouseX < width/2+150) && (mouseY > height/2-150) && (mouseY < height/2+150)){
    fill(0, 255, 255);
  }else{
    fill(255);
  }
  ellipse(width/2, height/2, 300, 300);
  //rect(width/2-150, height/2-150, 300, 300);

  noFill();
  stroke(0);
  ellipse(width/2+100, height/2-130, 100, 100);
  ellipse(width/2-100, height/2-130, 100, 100);
  ellipse(width/2, height/2, 300, 300);
  //ellipse();

  if(dist < 150){
    arc(width/2, height/2, 200, 200, 0, radians(180));
  }else{
    push();
    translate(mouseX, mouseY);
    //rotate();
    for(let i=0; i<7; i=i+1){
      arc((i*30), 0, 30, 30, 0, radians(180));  
    }
    pop();
  }


  //arc(width/2+(0*30), height/2, 30, 30, 0, radians(180));
  //arc(width/2+(1*30), height/2, 30, 30, 0, radians(180));
  //arc(width/2+(2*30), height/2, 30, 30, 0, radians(180));
  //arc(width/2+(3*30), height/2, 30, 30, 0, radians(180));
  //arc(width/2+(4*30), height/2, 30, 30, 0, radians(180));
  //arc(width/2+(5*30), height/2, 30, 30, 0, radians(180));

  print("mouseX: " + mouseX);
  print("mouseY: " + mouseY);

  //line(0, 100, width, 100);
}