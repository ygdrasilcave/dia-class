let sp_s0, sp_s1, jt_j;
function setup() {
  new Canvas(500, 200);
  world.gravity.y = 10;

  sp_s0 = new Sprite(250, 100, 30, 30, 'k');
  sp_s1 = new Sprite(250, 100, 400, 20);

  // Group이 속성적 연결이라고 한다면
  // Joint는 물리적 연결

  // glue joint 물리적으로 하나의 물체가 된다.
  // jt_j = new GlueJoint(spriteA, spriteB);

  // distance joint 용수철, 고무줄, 끈, 탄성, 늘어나는 정도
  // springiness: 0.0 ~ 1.0 늘어나지 않음 ~ 많이 늘어남
  // damping: 힘이 줄어드는 정도, 원래의 길이로 복원되는 힘
  // jt_j.springiness = 0.0;
  // jt_j = new DistanceJoint(sA, sB);

  // wheel joint : 모터, 바퀴, 선풍기
  // 속성: speed, maxPower(기본값1000), springiness(0.0001~1.0, 기본값 0.1)
  // 속성: damping(바퀴충격흡수0.0~1.0, 기본값0.7), angle(연결될 때 각도, 기본값 90도)
  // sp_car = new Sprite(250, 120, 100, 30);
  // sp_wheelsFront = new Sprite(280, 140, 20);
  // sp_wheelsRear = new Sprite(220, 140, 20);
  // jt_axleFront = new WheelJoint(car, wheelsFront);
  // st_axleRear = new WheelJoint(car, wheelsRear);
  // jt_axleFront.motorEnabled = true;

  jt_j = new HingeJoint(sp_s0, sp_s1);
  jt_j.maxPower = 0.1;
  // try changing the limits!
  // 회전 조인트(hinge)의 최소, 최대 회전 각도
  jt_j.lowerLimit = -15;
  jt_j.upperLimit = 15;

  // slider joint: 직선 운동, 선형 운동
  // 속성: range(두 스프라이트가 얼마나 멀리 떨어질 수 있는가)
  // speed(직성운동체 필요한 모터의 속도, 기본값 0.0)
  // angle(두 스프라이트가 연결되는 각도)
  jt_j = new SliderJoint(sp_floor, sp_scale);
  jt_j.range = 200;
  jt_j.angle = 90;
}

function draw() {
  clear();
  background(255);
  if (mouse.presses()) new Sprite(mouse.x, mouse.y, 6, 6)
}