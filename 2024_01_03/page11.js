let monsterImg, monsterMess;
let sp_monster;
let sp_player, sp_pillar;
let counter;
let bool_collides = false;

function preload(){
  monsterImg = loadImage('/assets/image/monster.png');
  monsterMess = loadImage('/assets/image/mess.png');
}

function setup() {
  new Canvas(600, 800);
  world.gravity.y = 10;
  
  // x, y, width, height, collider
  //sp_pillar = new Sprite(width/2, 550, 30, 500, 'static')
  // x, y, diameter
  sp_player = new Sprite(width/2, 0, 50);
  sp_player.friction = 10;
  //sp_player.drag = 10;
  sp_player.bounciness = 0;

  sp_monster = new Sprite();
  sp_monster.img = monsterImg;
  sp_monster.x = width/2;
  sp_monster.y = 550;
  sp_monster.diameter = 85;
  sp_monster.collider = 'static';
  sp_monster.debug = true;
  
  counter = 0;
}

function draw() {
  clear();
  background(255);

  //collides, colliding, collided

  if(sp_player.collides(sp_monster)){
    if(bool_collides == false){
      sp_monster.img = monsterMess;
      bool_collides = true;
    }
  }

  if (sp_player.colliding(sp_monster) > 6) {    
    if(counter < 10 && bool_collides == true){
    //if(counter < 10){
      counter++;
      sp_player.vel.y = -5;
      console.log(counter);
      sp_monster.img = monsterImg;
      bool_collides = false;
    }
           
  }

}