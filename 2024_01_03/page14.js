let ani_cloud;
let img_cloud;

let ani_shapeShifter;

let ani_splat;

let sp_circle01;
let sp_circle02;

function preload(){
  //ani_cloud = loadAnimation('/assets/image/cloud_breathing1.png', 9);
  ani_cloud = loadAni('/assets/image/cloud_breathing1.png', 9);
  ani_cloud.frameDelay = 4;
  img_cloud = loadImage('/assets/image/cloud_breathing1.png');

  ani_shapeShifter = loadAnimation(
    '/assets/image/asterisk.png',
    '/assets/image/triangle.png',
    '/assets/image/square.png',
    '/assets/image/cloud.png',
    '/assets/image/star.png',
    '/assets/image/mess.png',
    '/assets/image/monster.png'
  );
  ani_shapeShifter.frameDelay = 8;

  ani_splat = loadAni('/assets/image/p1_spritesheet.png', { frameSize: [72, 97], frames: 7 });
  ani_splat.frameDelay = 6;
}

function setup() {
  new Canvas(800, 800);

  sp_circle01 = new Sprite(width/2-10, height/2-200, 50, 'd');
  sp_circle01.image = img_cloud;

  sp_circle02 = new Sprite(width/2, height/2-200, 50, 'd');
}

function draw() {
  clear();
  background(255);
  //p5play function
  animation(ani_cloud, 300, height/2);
  //p5js function
  image(img_cloud, 500-img_cloud.width/2, height/2-img_cloud.height/2);
  animation(ani_shapeShifter, width/2, height/2);

  animation(ani_splat, width/2, height/2 + 200);
}