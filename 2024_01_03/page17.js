let gr_splats;

function setup() {
  new Canvas(800, 800);

  gr_splats = new Group();
  gr_splats.addAni('/assets/image/asterisk_explode0001.png', 11);
}

function draw() {
  clear();
  background(255);

  if (mouse.presses()) {
    new gr_splats.Sprite(mouse.x, mouse.y);
  }
}