let sp_ball;
let sp_floor;

function setup() {
  new Canvas(500, 500);
  world.gravity.y = 10;
  //world.gravity.x = 2;

  sp_ball = new Sprite();
  sp_ball.color = color(255, 0, 255);
  sp_ball.diameter = 100;
  sp_ball.x = width/2;
  sp_ball.y = 100;

  sp_floor = new Sprite();
  sp_floor.color = color(0, 255, 0);
  sp_floor.x = width/2;
  sp_floor.y = height-10;
  sp_floor.w = width;
  sp_floor.h = 20;
  sp_floor.rotation = 0;
  sp_floor.collider = 'kinematic';
  //dynamic, static, kinematic, none
}

function draw() {
  background(255);
  //clear();
  let rotate_val = map(mouseX, 0, width, -15, 15);
  rotate_val = constrain(rotate_val, -15, 15);
  sp_floor.rotation = rotate_val;
}