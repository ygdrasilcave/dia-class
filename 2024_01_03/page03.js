function setup(){
  createCanvas(800, 800);
  background(0);
}

function draw(){
  background(0);

  stroke(255, 255, 0);
  strokeWeight(5);
  noFill();
  rect(-100, -100, 200, 200);

  push();
  translate(width/2, height/2)
  if(mouseIsPressed == true){
    rotate(45);
  }else{
    rotate(0);
  }
  noStroke();
  fill(255);
  rect(-100, -100, 200, 200);
  stroke(0, 255, 0);
  line(0, 0, width/2, height/2);
  pop();
  
  push();
  translate(width/2, 100);
  stroke(255, 0, 255);
  strokeWeight(5);
  noFill();
  rect(-100, -100, 200, 200);
  pop();
}