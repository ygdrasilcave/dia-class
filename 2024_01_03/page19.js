let sp_hero;
let sp_floor;

function preload(){
  sp_hero = new Sprite(62, 24, 32, 32);
  sp_hero.spriteSheet = '/assets/image/questKid.png';
  sp_hero.anis.offset.x = 2;
  sp_hero.anis.offset.y = 1;
  sp_hero.anis.frameDelay = 8;

  //sp_hero.addAnis({
  sp_hero.addAnimations({
    run: { row: 0, col: 4, frames: 4, rotation: 0},
    jump: { row: 1, frames: 6 },
    roll: { row: 2, frames: 5, frameDelay: 14 },
    turn: { row: 3, frames: 7 },
    stand: { row: 3, frames: 1 }
  });
  sp_hero.changeAni('run');
}

function setup(){
  new Canvas(124, 48, 'pixelated x6');
  //new Canvas(124, 48);
  allSprites.pixelPerfect = true;

  sp_floor = new Sprite(width/2, 48-8+4, 124, 8, 's');
}

function draw(){
  clear();
  background(255);

  if(kb.presses('up')){
    sp_hero.changeAni(['jump', 'stand']);
  }
  if(kb.presses('down')){
    sp_hero.changeAni(['roll', 'stand']);
  }
  
  if (kb.pressing('left')){
    sp_hero.vel.x = -1;
  }
  else if (kb.pressing('right')){
    sp_hero.vel.x = 1;
  }
  else{
    sp_hero.vel.x = 0;
  }
  
  if (sp_hero.ani.name != 'jump' && sp_hero.ani.name != 'roll') {
    if (kb.pressing('left')) {
      sp_hero.changeAni('run');
      sp_hero.mirror.x = true;
    } else if (kb.pressing('right')) {
      sp_hero.changeAni('run');
      sp_hero.mirror.x = false;
    } else if (sp_hero.ani.name != 'stand') {
      sp_hero.changeAni('stand');
    }
  }
}