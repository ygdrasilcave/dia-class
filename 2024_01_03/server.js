const path = require('path');
const port = 3000;

let express = require('express');
let socket = require('socket.io');

let app = express();
let server = app.listen(port, () => {
    console.log('http://localhost:' + port + ' 서버 실행')
})

let io = socket(server);
io.sockets.on('connection', newConnection);

function newConnection(socket){
	//console.log(socket);
	console.log('new client: ' + socket.id);
	socket.on('mouse', mouseMsg);
	function mouseMsg(data){
		//console.log(data);
		socket.broadcast.emit('mouse', data);
		//io.sockets.emit('mouse', data);
	}
}


//https://morian-kim.tistory.com/3

//console.log(path.join(__dirname, '/libraries'));
//app.use(express.static('public'));
//app.use('/js', express.static(path.join(__dirname, '/js')));
app.use('/libraries', express.static(path.join(__dirname, '/libraries')));
app.use('/assets', express.static(path.join(__dirname, '/assets')));
app.use('/pages', express.static(path.join(__dirname, '/pages')));

let pageName = 'main'
//app.use(express.static('pages/' + pageName));
////app.use(express.static(path.join(__dirname + '/pages', 'page01')));

app.get('/example01', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/examples/main.html'))
})
app.get('/example02', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/examples/page01.html'))
})
app.get('/example03', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/examples/page02.html'))
})
app.get('/example04', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/examples/page03.html'))
})
app.get('/example05', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/examples/page04.html'))
})


app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/main.html'))
})

app.get('/page01', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page01.html'))
})

app.get('/page02', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page02.html'))
})

app.get('/page03', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page03.html'))
})

app.get('/page04', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page04.html'))
})

app.get('/page05', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page05.html'))
})

app.get('/page06', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page06.html'))
})

app.get('/page07', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page07.html'))
})

app.get('/page08', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page08.html'))
})

app.get('/page09', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page09.html'))
})

app.get('/page10', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page10.html'))
})

app.get('/page11', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page11.html'))
})

app.get('/page12', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page12.html'))
})

app.get('/page13', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page13.html'))
})

app.get('/page14', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page14.html'))
})

app.get('/page15', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page15.html'))
})

app.get('/page16', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page16.html'))
})

app.get('/page17', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page17.html'))
})

app.get('/page18', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page18.html'))
})

app.get('/page19', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page19.html'))
})

app.get('/page20', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page20.html'))
})
app.get('/page21', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page21.html'))
})
app.get('/page22', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page22.html'))
})
app.get('/page23', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page23.html'))
})
app.get('/page24', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page24.html'))
})
app.get('/page25', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page25.html'))
})
app.get('/page26', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page26.html'))
})
app.get('/page27', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page27.html'))
})
app.get('/page28', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page28.html'))
})
app.get('/page29', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page29.html'))
})
app.get('/page30', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page30.html'))
})
app.get('/page31', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page31.html'))
})
app.get('/page32', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page32.html'))
})
app.get('/page33', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page33.html'))
})
app.get('/page34', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page34.html'))
})
app.get('/page35', (req, res) => {
  res.sendFile(path.join(__dirname + '/pages/page35.html'))
})