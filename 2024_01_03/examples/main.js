let i = 0;
let cnv;
let link1, link2, link3, link4, link_ys;

function setup() {
  cnv = createCanvas(800, 600);
  print("안녕하세요");

  textSize(42);
  textAlign(CENTER, CENTER);
  print("변수 let i 의 현재 값을 출력합니다:" + i)
  // setup 구문 안에서 변수 h는 없다!!!
  //print(h);

  link1 = createA('/page01', '다음 이야기는?', '_self');
  link_ys = createA('https://www.yonsei.ac.kr', '연세대학교', '_blank');
  link2 = createA('/page02', '페이지 투', '_blank');
  link3 = createA('/page03', '페이지 쓰리', '_blank');
  link4 = createA('/page04', '페이지 포', '_blank');
  reposLinks();
}

function draw() {
  // put drawing code here
  background(255,255,0);
  fill(255);
  ellipse(width/2, height/2, 500, 100);

  if(mouseX > 440 && mouseY > 370 && mouseX < 477 && mouseY < 414){
    fill(255, 0, 255);
    rect(440, 370, 37, 44);
  }

  fill(0);
  text("반갑습니다. " + i, width*0.5, height*0.5);
  let h = hour();
  let m = minute();
  let s = second();
  fill(0);
  text(nf(h,2) + ":" + nf(m,2) + ":" + nf(s,2), width/2, height/2 + 50);
  //print("반갑습니다." + i);
  i = i + 1; // i++

  //console.log('x: ' + mouseX + ', y: ' + mouseY);
}


function mousePressed(){
  console.log('마우스를 클릭했습니다.');
  if(mouseX > 440 && mouseY > 370 && mouseX < 477 && mouseY < 414){
    window.open('/page01', '_self');
  }
}

function reposLinks(){
  let x = (windowWidth - width) / 2;
  let y = (windowHeight - height) / 2;
  link1.position(x + 30, y + 30);
  link_ys.position(x + 30, y + 60);
  link2.position(x + 30, y + 90);
  link3.position(x + 30, y + 120);
  link4.position(x + 30, y + 150);
}

function windowResized() {
  reposLinks();
}