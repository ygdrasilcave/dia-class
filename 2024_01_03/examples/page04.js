let floor_sp, balls_gr;
let arc_sp;
let arc_points = [[]];

function makeArc(cx, cy, r){
  let i = 0;
  for(let d=-10; d<130; d+=10){
    let x = cx + cos(d)*r;
    let y = cy + sin(d)*r;
    arc_points[i] = [x, y];
    i++;
  }
}

function setup() {
  new Canvas(800, 600);
  world.gravity.y = 12;

  //                ([vertex0, vertex1, vertex2, ...])
  floor_sp = new Sprite([[20, 60], [200, 140], [450, 180]]);
  floor_sp.collider = 'static';

  makeArc(width/2+100, height/2, 250);
  //print(arc_points);
  //angleMode(RADIANS);
  arc_sp = new Sprite(arc_points);
  arc_sp.collider = 'static';
  //arc_sp.visible = false;

  balls_gr = new Group();
  balls_gr.d = 20;
  balls_gr.collider = 'd';
  balls_gr.x = (i) => (i%2)*(width/2+260) + 25;
  balls_gr.y = 0;
  //balls_gr.debug = true;

  new balls_gr.Sprite();
}

function draw() {
  clear();
  background(255);
  rect(floor_sp.x - 2, floor_sp.y - 2, 4, 4);

  if(frameCount%15 == 0){
    new balls_gr.Sprite();
    //print(balls_gr.length);
  }

  if(mouse.released()){
    new balls_gr.Sprite(mouse.x, mouse.y, 20, 20);
  }

  if(balls_gr.cull(50)){
    //print(balls_gr.length);
  }
}