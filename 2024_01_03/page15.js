let ani_asterisk;

function setup() {
  new Canvas(800, 800);
  ani_asterisk = loadAnimation('/assets/image/asterisk_explode0001.png', 11);
}

function draw() {
  clear();
  background(255);

  if (kb.presses('s')) ani_asterisk.stop();
  if (kb.presses('p')) ani_asterisk.play();
  if (kb.presses('o')) ani_asterisk.play(0);
  if (kb.presses('ArrowDown')) ani_asterisk.frameDelay--;
  if (kb.presses('ArrowUp')) ani_asterisk.frameDelay++;
  if (kb.presses('r')) ani_asterisk.rewind();
  if (kb.presses('l')) ani_asterisk.loop();
  if (kb.presses('n')) ani_asterisk.noLoop();
  if (kb.presses('ArrowLeft')) ani_asterisk.previousFrame();
  if (kb.presses('ArrowRight')) ani_asterisk.nextFrame();
  if (kb.presses('5')) ani_asterisk.frame = 5;
  if (kb.pressing('x')) ani_asterisk.scale.x = -1;
  if (kb.pressing('y')) ani_asterisk.scale.y = -1;
  if (kb.pressing('1')) ani_asterisk.scale = 1;
  if (kb.pressing('2')) ani_asterisk.scale = 2;

  animation(ani_asterisk, width/2, height/2);
}