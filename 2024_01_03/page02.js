/*
let leftEyeSize = 1;
let rightEyeSize = 1;
let t = 0;

function setup() {
  createCanvas(480, 120);
  //angleMode(RADIANS);
}

function draw() {
  background(204);
  // Left creature
  fill(255);
  beginShape();
  vertex(50, 120);
  vertex(100, 90);
  vertex(110, 60);
  vertex(80, 20);
  vertex(210, 60);
  vertex(160, 80);
  vertex(200, 90);
  vertex(140, 100);
  vertex(130, 120);
  endShape();
  fill(0);
  ellipse(155, 60, leftEyeSize, 8);

  leftEyeSize = sin(t) * 8;

  // Right creature
  fill(255);
  beginShape();
  vertex(370, 120);
  vertex(360, 90);
  vertex(290, 80);
  vertex(340, 70);
  vertex(280, 50);
  vertex(420, 10);
  vertex(390, 50);
  vertex(410, 90);
  vertex(460, 120);
  endShape();
  fill(0);
  ellipse(345, 50, 10, rightEyeSize);
  rightEyeSize = cos(t) * 10;

  t = t + 15;
}
*/
/*
function setup() {
  createCanvas(720, 480);
  strokeWeight(2);
  ellipseMode(RADIUS);
}
function draw() {
  background(204);
  robot();
}
function robot(){
  push();
  translate(mouseX-219-45, mouseY-257-60);
  // Neck
  stroke(102); // Set stroke to gray
  line(266, 257, 266, 162); // Left
  line(276, 257, 276, 162); // Middle
  line(286, 257, 286, 162); // Right
  // Antennae
  line(276, 155, 246, 112); // Small
  line(276, 155, 306, 56); // Tall
  line(276, 155, 342, 170); // Medium
  // Body
  noStroke(); // Disable stroke
  fill(102); // Set fill to gray
  ellipse(264, 377, 33, 33); // Antigravity orb
  fill(0); // Set fill to black
  rect(219, 257, 90, 120); // Main body
  fill(102); // Set fill to gray
  rect(219, 274, 90, 6); // Gray stripe
  // Head
  fill(0); // Set fill to black
  ellipse(276, 155, 45, 45); // Head
  fill(255); // Set fill to white
  ellipse(288, 150, 14, 14); // Large eye
  fill(0); // Set fill to black
  ellipse(288, 150, 3, 3); // Pupil
  fill(153); // Set fill to light gray
  ellipse(263, 148, 5, 5); // Small eye 1
  ellipse(296, 130, 4, 4); // Small eye 2
  ellipse(305, 162, 3, 3); // Small eye 3
  pop();
}*/

/*
let t = 0.0;
function setup() {
  createCanvas(800, 800);
  angleMode(DEGREES);
}

function draw() {
  background(0);
  noFill();
  stroke(255);
  let dia = sin(t) * 60 + 60;
  for(let y=0; y<=height; y+=40){
    for (let x = 0; x <= width; x += 40) {
      ellipse(x, y, dia, dia);
    }
  }
  t+=0.25;
}
*/
/*
let x = 60; // x coordinate
let y = 420; // y coordinate
let bodyHeight = 110; // Body height
let neckHeight = 140; // Neck height
let radius = 45;
let ny = y - bodyHeight - neckHeight - radius; // Neck Y
let t = 0.0;

function setup() {
 createCanvas(800, 800);
 strokeWeight(2);
 ellipseMode(RADIUS);
}

function draw() {
 background(204);

 x = mouseX;
 y = sin(t)*100 + 200 + ny;
 t += 5;

 // Neck
 stroke(102);
 line(x+2, y-bodyHeight, x+2, ny);
 line(x+12, y-bodyHeight, x+12, ny);
 line(x+22, y-bodyHeight, x+22, ny);
 // Antennae
 line(x+12, ny, x-18, ny-43);
 line(x+12, ny, x+42, ny-99);
 line(x+12, ny, x+78, ny+15);
 // Body
 noStroke();
 fill(102);
 ellipse(x, y-33, 33, 33);
 fill(0);
 rect(x-45, y-bodyHeight, 90, bodyHeight-33);
 fill(102);
 rect(x-45, y-bodyHeight+17, 90, 6);
 // Head
 fill(0);
 ellipse(x+12, ny, radius, radius);
 fill(255);
 ellipse(x+24, ny-6, 14, 14);
 fill(0);
 ellipse(x+24, ny-6, 3, 3);
 fill(153);
 ellipse(x, ny-8, 5, 5);
 ellipse(x+30, ny-26, 4, 4);
 ellipse(x+41, ny+6, 3, 3);
}
*/

function setup() {
 createCanvas(800, 800);
 stroke(0, 102);
 background(255, 255, 0);
}
function draw() {
 var weight = dist(mouseX, mouseY, pmouseX, pmouseY);
 strokeWeight(weight);
 line(mouseX, mouseY, pmouseX, pmouseY);
}

/*
var x = 0;
var easing = 0.01;
function setup() {
 createCanvas(220, 120);
 background(255);
}
function draw() {
 var targetX = mouseX;
 x += (targetX - x) * easing;
 ellipse(x, 40, 12, 12);
 print(targetX + " : " + x);
}
*/
