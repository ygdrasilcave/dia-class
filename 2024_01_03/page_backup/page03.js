// Initialize the Image Classifier method with MobileNet. A callback needs to be passed.
let classifier;

// A variable to hold the image we want to classify
let img;

let c_name;
let c_confidence;

function preload() {
  classifier = ml5.imageClassifier('MobileNet');
  img = loadImage('/assets/image/bird.png');
}

function setup() {
  createCanvas(400, 400);
  classifier.classify(img, gotResult);
  image(img, 0, 0, 400, 400);
  textAlign(LEFT, CENTER);
  textSize(18);
}

function draw(){
  image(img, 0, 0, 400, 400);
  fill(255, 160);
  noStroke();
  rect(0, height-70, width, 60);
  fill(0);
  text(c_name, 10, height-50);
  text(c_confidence, 10, height-25);
}

// A function to run when we get any errors and the results
function gotResult(error, results) {
  // Display error in the console
  if (error) {
    console.error(error);
  } else {
    // The results are in an array ordered by confidence.
    console.log(results);
    //createDiv(`Label: ${results[0].label}`);
    //createDiv(`Confidence: ${nf(results[0].confidence, 0, 2)}`);
    c_name = results[0].label;
    c_confidence = nf(results[0].confidence, 0, 2);
  }
}
