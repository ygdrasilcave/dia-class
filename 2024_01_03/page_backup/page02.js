//  <script type="text/javascript" src="/libraries/quicksettings.js"></script>
//  <script type="text/javascript" src="/libraries/p5.gui.js"></script>

// to control the slider range and step for individual variables.
// Just append 'min', 'max' or 'step' to your variable names...
// or call sliderRange(min, max, step) before gui.addGlobals('slider_varialble')

// gui params
var numShapes = 20;
var strokeWidth = 4;
var strokeColor = '#00ddff';
var fillColor = [0, 255, 255];
var drawStroke = true;
var drawFill = true;
var radius;
var shape = ['circle', 'triangle', 'square', 'pentagon', 'star'];
var label = 'label';

// gui
let visible = false;
let gui;

// dynamic parameters
var bigRadius;

function setup() {

  createCanvas(1200, 1000);
  colorMode(HSB, 255);

  // Calculate big radius
  bigRadius = height / 2.0 - 50;
  radius = 20;

  // Create Layout GUI
  gui = createGui('parameters').setPosition(100, 100);
  sliderRange(0, 100, 1);
  gui.addGlobals('bigRadius', 'numShapes', 'shape', 'label', 'radius',
  'drawFill', 'fillColor', 'drawStroke', 'strokeColor');
  sliderRange(0.2, 5, 0.1);
  gui.addGlobals('strokeWidth');

  gui.hide();

  angleMode(RADIANS);

}


function draw() {

  background(0, 0, 220);

  // set fill style
  if(drawFill == true) {
    fill(fillColor);
  } else {
    noFill();
  }

  // set stroke style
  if(drawStroke == true) {
    stroke(strokeColor);
    strokeWeight(strokeWidth);
  } else {
    noStroke();
  }

  // draw circles arranged in a circle
  for(let i = 0; i < numShapes; i++) {

    let angle = (TWO_PI / numShapes) * i;
    let x = (width / 2.0) + cos(angle) * bigRadius;
    let y = (height / 2.0) + sin(angle) * bigRadius;
    let d = 2 * radius;

    // pick a shape
    if(shape == 'circle') {
        ellipse(x, y, d, d);
    }else if(shape == 'square'){
        rectMode(CENTER);
        rect(x, y, d, d);
    }else if(shape == 'triangle'){
        ngon(3, x, y, d);
    }else if(shape == 'pentagon'){
        ngon(5, x, y, d);
    }else if(shape == 'star'){
        star(6, x, y, d/sqrt(3), d);
    }

    // draw a label below the shape
    push();
    noStroke();
    fill(0);
    textAlign(CENTER, CENTER);
    //text(label, x, y + radius + 15);
    text(label, x, y);
    pop();

  }

}


// check for keyboard events
function keyPressed() {
  if(key == 'g' || key == 'G'){
    visible = !visible;
    if(visible == true){
      gui.show();
    }else{
      gui.hide();
    }
  }
  /*if(key == 'f' || key == 'F'){
    drawFill = !drawFill;
    console.log('f pressed');
    console.log(drawFill);
  }*/
}


// draw a regular n-gon with n sides
function ngon(n, x, y, d) {
  beginShape();
  for(let i = 0; i < n; i++) {
    let angle = TWO_PI / n * i;
    let px = x + sin(angle) * d / 2;
    let py = y - cos(angle) * d / 2;
    vertex(px, py);
  }
  endShape(CLOSE);
}


// draw a regular n-pointed star
function star(n, x, y, d1, d2) {
  beginShape();
  for(let i = 0; i < 2 * n; i++) {
    let d = (i % 2 === 1) ? d1 : d2;
    let angle = PI / n * i;
    let px = x + sin(angle) * d / 2;
    let py = y - cos(angle) * d / 2;
    vertex(px, py);
  }
  endShape(CLOSE);
}
