let socket;
let cloudAni;
let mySound;

function preload() {
  soundFormats('mp3', 'ogg');
  mySound = loadSound('/assets/sound/doorbell');
}

let mySprite;

function setup() {
 // put setup code here
  createCanvas(800, 800);
  background(255, 255, 200);

  socket = io.connect('http://localhost:3000');
  socket.on('mouse', receiveData);

  mySprite = new Sprite();
  cloudAni = loadAni('/assets/image/cloud_breathing1.png', 9);

  rectMode(CENTER);
}

function receiveData(getData){
  noFill();
  stroke(0);
  ellipse(getData.x, getData.y, 30, 30);
}

function mouseDragged(){
  let data = {
    x: mouseX,
    y: mouseY
  }
  socket.emit('mouse', data);
  noFill();
  stroke(0);
  ellipse(data.x, data.y, 30, 30);
}

function draw() {
  // put drawing code here
  //clear();
  fill(255);
  stroke(255, 0, 0);
  rect(250, 80, 150, 150);
  animation(cloudAni, 250, 80);

  if (mouse.presses()) {
    mySound.play();
  }else if(mouse.pressing()){
    mySprite.color = 'green';
    mySprite.stroke = 'red';
  }else{
    mySprite.color = 'red';
    mySprite.stroke = 'green';
  }
}
