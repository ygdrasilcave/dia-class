let gr_bricks, tilesGroup;

function setup() {
  new Canvas(600, 800);

  gr_bricks = new Group();
  gr_bricks.w = 20;
  gr_bricks.h = 20;
  gr_bricks.tile = 'o';

  tilesGroup = new Tiles(
    [
      'o.o.o.o.o.o.o.o.o.o.o',
      '.o.o.o.o.o.o.o.o.o.o.',
      'o.o.o.o.o.o.o.o.o.o.o',
      '.o.o.o.o.o.o.o.o.o.o.',
      'o.o.o.o.o.o.o.o.o.o.o',
      '.o.o.o.o.o.o.o.o.o.o.',
      'o.o.o.o.o.o.o.o.o.o.o',
      '.o.o.o.o.o.o.o.o.o.o.',
    ],
    // tile group w, y position
    10, 10,
    // each tile width, height
    gr_bricks.w+4,
    gr_bricks.h+4
  );
}

function draw() {
  clear();

  background(255);

  for (let brick of gr_bricks) {
    if (brick.mouse.hovers()) {
      brick.color = '#AA4A44';
    }
  }
}