let angle = 0.0;

function setup() {
  createCanvas(800, 800);
  background(204);
}

function draw() {
  translate(mouseX, mouseY);
  rotate(angle);
  rect(-15, -15, 30, 30);
  angle += 3;
}

/*
var angle = 0.0;

function setup() {
  createCanvas(800, 800);
  background(204);
}

function draw() {
  rotate(angle);
  translate(mouseX, mouseY);
  rect(-15, -15, 30, 30);
  angle += 3;
}
*/