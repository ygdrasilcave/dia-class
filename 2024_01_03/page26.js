let gr_grass, gr_grassUnder;
let gr_water, gr_waterTop;
let tilesGroup;

let imgFolder = '/assets/Platformer Art Complete Pack/Base pack/Tiles/';
let spriteSheetImg;

function preload(){
  spriteSheetImg = loadImage(imgFolder + 'tiles_spritesheet.png');
}

function setup() {
  new Canvas(700, 800);

  world.gravity.y = 10;

  gr_grass = new Group();
  gr_grass.spriteSheet = spriteSheetImg;
  gr_grass.addAni({x:504, y:576, w:70, h:70});
  gr_grass.w = 70;
  gr_grass.h = 70;
  gr_grass.tile = 'g';
  gr_grass.collider = 's';

  gr_grassUnder = new Group();
  gr_grassUnder.spriteSheet = spriteSheetImg;
  gr_grassUnder.addAni({x:576, y:864, w:70, h:70});
  gr_grassUnder.w = 70;
  gr_grassUnder.h = 70;
  gr_grassUnder.tile = 'u';
  gr_grassUnder.collider = 's';

  gr_water = new Group();
  gr_water.spriteSheet = spriteSheetImg;
  gr_water.addAni({x:504, y:216, w:70, h:70});
  gr_water.w = 70;
  gr_water.h = 70;
  gr_water.tile = 'w';
  gr_water.collider = 's';

  gr_waterTop = new Group();
  gr_waterTop.spriteSheet = spriteSheetImg;
  gr_waterTop.addAni({x:432, y:576, w:70, h:70});
  gr_waterTop.w = 70;
  gr_waterTop.h = 70;
  gr_waterTop.tile = 't';
  gr_waterTop.collider = 's';

  tilesGroup = new Tiles(
    [
      '..........',
      '..........',
      '..........',
      '..........',
      'ggggttgggg',
      'uuuuwwuuuu',
      'uuuuwwuuuu',
      'uuuuuuuuuu',
      'uuuuuuuuuu'
    ],
    // tile group w, y position
    35, 35,
    // each tile width, height
    gr_grass.w,
    gr_grass.h
  );

  new Sprite(200, 100, 50);
}

function draw() {
  clear();
  background(255);
}