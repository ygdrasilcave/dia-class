let sp_hero;

function preload() {
  sp_hero = new Sprite(62, 24, 32, 32);
  sp_hero.spriteSheet = '/assets/image/questKid.png';
  sp_hero.anis.offset.x = 2;
  sp_hero.anis.frameDelay = 8;

  //sp_hero.addAnis({
  sp_hero.addAnimations({
    run: { row: 0, col: 4, frames: 4, rotation: 0},
    jump: { row: 1, frames: 6 },
    roll: { row: 2, frames: 5, frameDelay: 14 },
    turn: { row: 3, frames: 7 },
    stand: { row: 3, frames: 1 }
  });
  sp_hero.changeAni('run');
}

function setup() {
  new Canvas(124, 48, 'pixelated x6');
  //new Canvas(124, 48);
  allSprites.pixelPerfect = true;
}

function draw() {
  clear();
  background(255);
  if(kb.presses('r')){
    sp_hero.ani.play();
    sp_hero.changeAni('run');
  }
  if(kb.presses('j')){
    sp_hero.ani.play();
    sp_hero.changeAni('jump');
  }
  if(kb.presses('l')){
    // 'roll' -> 'jump' -> 'jump' -> 'jump' -> ...
    //sp_hero.changeAni(['roll', 'jump']);
    // 'roll' -> 'jump' -> 'roll' -> 'jump' -> ...
    //sp_hero.changeAni(['roll', 'jump', '**']);
    // 'roll' -> 'jump' and all ani stoped!
    sp_hero.changeAni(['roll', 'jump', 'turn', ';;']);
  }
  if(kb.presses('t')){
    sp_hero.ani.play();
    sp_hero.changeAni('turn');
  }
  if(kb.presses('s')){
    sp_hero.ani.play();
    sp_hero.changeAni('stand');
  }
}