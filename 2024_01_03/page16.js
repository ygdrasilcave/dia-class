let sp_ghost;
let sp_ball;

let ani_fly;
let ani_idle;

function preload(){
  ani_fly = loadAni('/assets/image/ghost_walk01.png', 3);
  ani_idle = loadAni('/assets/image/ghost_standing01.png', 7)
}

function setup() {
  new Canvas(800, 800);
  sp_ghost = new Sprite(width/2, height/2, 120);
  //sp_ghost.image = img;
  sp_ghost.addAni('fly', ani_fly);
  sp_ghost.addAni('idle', ani_idle);

  sp_ball = new Sprite(width/2 + 200, height/2, 150);
}

function draw() {
  clear();
  background(255);

  //animation(ani_fly, width/2-100, height/2);
  //animation(ani_idle, width/2+100, height/2);

  sp_ghost.debug = mouse.pressing();

  if (kb.pressing('left')) {
    sp_ghost.changeAni('fly');
    sp_ghost.vel.x = -2;
    sp_ghost.mirror.x = true;
  }
  else if (kb.pressing('right')) {
    sp_ghost.changeAni('fly');
    sp_ghost.vel.x = 2;
    sp_ghost.mirror.x = false;
  }
  else {
    sp_ghost.changeAni('idle');
    sp_ghost.vel.x = 0;
  }

  if (kb.pressing('space')) {
    sp_ghost.ani.stop();
  } else {
    sp_ghost.ani.play();
  }
}