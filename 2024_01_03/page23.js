let gr_balls;
let gr_water;

function setup() {
  new Canvas(600, 600);

  world.gravity.y = 10;

  gr_balls = new Group();
  gr_balls.d = 20;
  gr_balls.collider = 'none';
  gr_balls.direction = () => random(0, 360);
  gr_balls.speed = () => random(1, 5);
  gr_balls.amount = 10;

  gr_water = new Group();
  gr_water.color = 'blue';
  gr_water.mass = 3;
  gr_water.vel.y = -6;
  gr_water.life = 60;
  //gr_water.autoCull = false;
}
function draw() {
  clear();
  background(255);
  if (gr_balls.cull(-50)){
    new gr_balls.Sprite();
    if(gr_balls.length < 10){
      new gr_balls.Sprite();
    }
  }
  //console.log(gr_balls.length);

  new gr_water.Sprite(mouse.x, mouse.y, 10);
  if (mouse.presses()){
    new Sprite(mouse.x, -200, 40);
  }
  console.log(gr_water.length);
}