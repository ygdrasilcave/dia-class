void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.print("1023 >>> ");
  Serial.println(1023);

  Serial.print("100 >>> ");
  Serial.println(100);

  Serial.print("102 >>> ");
  Serial.write(102);
  Serial.println();
  
  Serial.print("2023 >>> ");
  Serial.write(2023);
  Serial.println();
  Serial.println();
  
  delay(5000);
}
