#include <Servo.h>
Servo servoMotor;
int angle = 0;

void setup() {
  Serial.begin(9600);
  servoMotor.attach(9);
}

void loop() {
  if (Serial.available()) {
    char ch = Serial.read();
    //ascii value 48 - 57
    if ( ch >= '0' && ch <= '9' ) {
      angle = (ch - '0');
      angle = angle * 20;
    }
  }
  servoMotor.write(angle);
}
