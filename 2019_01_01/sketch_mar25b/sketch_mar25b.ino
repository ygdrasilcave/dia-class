const int sw0 = 8;
const int led = 12;

boolean status0 = false;
int value_current = 0;
int value_prev = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(sw0, INPUT);
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  value_current = digitalRead(sw0);

  if(value_prev == HIGH && value_current == LOW){
    status0 = !status0;
  }

  if(status0 == true){
    blinkLED(100);
  }else{
    digitalWrite(led, LOW);
  }

  
  Serial.println(status0);

  value_prev = value_current;
}

void blinkLED(int _d){
  digitalWrite(led, HIGH);
  delay(_d);
  digitalWrite(led, LOW);
  delay(_d);
}
