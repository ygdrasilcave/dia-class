const int sw0 = 8;
const int sw1 = 9;

const int led = 12;

void setup() {
  // put your setup code here, to run once:
  pinMode(sw0, INPUT);
  pinMode(sw1, INPUT);
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int value0 = digitalRead(sw0);
  int value1 = digitalRead(sw1);

  if(value0 == 1 && value1 == 0){
    blinkLED(250);   
  }else if(value0 == 0 && value1 == 1){
    blinkLED(1000);
  }else if(value0 == 1 && value1 == 1){
    blinkLED(100);
  }else{
    digitalWrite(led, LOW);
  }
  
  Serial.print(value0);
  Serial.print(" : ");
  Serial.println(value1);
}

void blinkLED(int _d){
  digitalWrite(led, HIGH);
  delay(_d);
  digitalWrite(led, LOW);
  delay(_d);
}
