
const int ledPin0 = 12;
const int ledPin1 = 11;
const int ledPin2 = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin0, OUTPUT);
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  
  ledTest();
}

void loop() {
  // put your main code here, to run repeatedly:
  patternA(1000);
  patternA(900);
  patternA(800);
  patternA(700);
  patternA(600);
  patternA(500);
  patternA(400);
  patternA(300);
  patternA(200);
  patternA(100);

  patternB(200, 100);
  patternB(700, 500);
  patternB(1200, 1000);
  patternB(800, 200);
}

void ledTest(){
  digitalWrite(ledPin0, HIGH);
  digitalWrite(ledPin1, HIGH);
  digitalWrite(ledPin2, HIGH);
  delay(100);
  digitalWrite(ledPin0, LOW);
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  delay(100);

  digitalWrite(ledPin0, HIGH);
  digitalWrite(ledPin1, HIGH);
  digitalWrite(ledPin2, HIGH);
  delay(100);
  digitalWrite(ledPin0, LOW);
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  delay(100);

  digitalWrite(ledPin0, HIGH);
  digitalWrite(ledPin1, HIGH);
  digitalWrite(ledPin2, HIGH);
  delay(100);
  digitalWrite(ledPin0, LOW);
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  delay(100);
}

void patternA(int _duration){
  digitalWrite(ledPin0, HIGH);
  delay(_duration);
  digitalWrite(ledPin0, LOW);
  delay(_duration);
}

void patternB(int _onTime, int _offTime){
  digitalWrite(ledPin1, HIGH);
  delay(_onTime);
  digitalWrite(ledPin1, LOW);
  delay(_offTime);
}
