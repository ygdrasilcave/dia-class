const int switch_00 = 9;
const int switch_01 = 10;
int currState_00 = 0;
int prevState_00 = 0;
int currState_01 = 0;
int prevState_01 = 0;
int counter = 0;

void setup() {
  pinMode(switch_00, INPUT);
  pinMode(switch_01, INPUT);

  for(int i=0; i<7; i=i+1) {
    pinMode(i+2, OUTPUT);
  }

  Serial.begin(9600);

  digitalWrite(5, HIGH);
}

void loop() {
  currState_00 = digitalRead(switch_00);
  currState_01 = digitalRead(switch_01);
  if (prevState_00 == HIGH && currState_00 == LOW) {
    counter++;
  }
  if (prevState_01 == HIGH && currState_01 == LOW) {
    counter--;
  }
  Serial.println(counter);

  offAllLEDs();
  digitalWrite(counter + 5, HIGH);
  if (counter < -3) {
    counter = -3;
  }
  if (counter > 3) {
    counter = 3;
  }

  prevState_00 = currState_00;
  prevState_01 = currState_01;
}

void offAllLEDs() {
  for (int i = 0; i < 7; i++) {
    digitalWrite(i + 2, LOW);
  }
}
