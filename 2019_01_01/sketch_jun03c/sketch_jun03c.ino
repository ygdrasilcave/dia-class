const int ledPin = 4;
int rawData = 0;

int data = 0;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    rawData = Serial.read();
    if (rawData == 'H') {
      //data = 1;
      digitalWrite(ledPin, HIGH);
    } else if (rawData == 'L') {
      //data = 0;
      digitalWrite(ledPin, LOW);
    } else {
      Serial.println("Wrong message! enter H or L");
    }
  }

  /*if (data == 1) {
    digitalWrite(ledPin, HIGH);
  } else if (data == 0) {
    digitalWrite(ledPin, LOW);
  }*/
}

/*processing code
  import processing.serial.*;
  Serial myPort;

  boolean toggle = false;

  void setup() {
  size(400, 400);

  println(Serial.list());
  myPort = new Serial(this, Serial.list()[1], 9600);

  rectMode(CENTER);
  }

  void draw()
  {
  background(0);

  noStroke();
  if (mousePressed == true) {
   if (mouseX>width/2-50 && mouseX<width/2+50 && mouseY>height/2-50 && mouseY<height/2+50) {
     fill(0, 255, 0);
     myPort.write('H');
     //if (toggle == false) {
     //  myPort.write('H');
     //  toggle = true;
     //}
   }
  }
  else {
   fill(255);
   myPort.write('L');
   //if(toggle == true){
   //  myPort.write('L');
   //  toggle = false;
   //}
  }
  rect(width/2, height/2, 100, 100);
  }
*/
