int inPin_01_01 = 5;
int inPin_01_02 = 4;
int enPin_01 = 9;

int mSpeed_01 = 0;

int potPin = 0;
int switchPin = 3;

void setup(){
  pinMode(inPin_01_01, OUTPUT);
  pinMode(inPin_01_02, OUTPUT);
  pinMode(switchPin, INPUT);
}

void loop(){
  mSpeed_01 = analogRead(potPin);
  analogWrite(enPin_01, map(mSpeed_01, 0, 1023, 0, 255));
  
  if(digitalRead(switchPin) == HIGH){
    digitalWrite(inPin_01_01, LOW);
    digitalWrite(inPin_01_02, HIGH);
  }else{
    digitalWrite(inPin_01_01, HIGH);
    digitalWrite(inPin_01_02, LOW);
  }
}
