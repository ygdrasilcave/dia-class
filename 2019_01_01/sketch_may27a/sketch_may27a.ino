#include <Servo.h>

Servo servoMotor;
int anSensor = 0;
int sensorVal;

void setup() {
 servoMotor.attach(9);
 Serial.begin(9600);
}

void loop() {
 sensorVal = analogRead(anSensor);
 sensorVal = map(sensorVal, 0, 1023, 0, 180);
 servoMotor.write(sensorVal);
 Serial.println(sensorVal);
 delay(15);
}
