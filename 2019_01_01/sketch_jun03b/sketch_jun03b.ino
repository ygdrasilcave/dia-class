int value0_00, value0_01, value0_02;
int value1_00, value1_01, value1_02;

void setup(){
 Serial.begin(9600);
}

void loop(){
 value0_00 = 10;
 value0_01 = 100;
 value0_02 = 1000;
 value1_00 = 20;
 value1_01 = 200;
 value1_02 = 2000;
 Serial.print('H'); //.......................................header
 Serial.print(",");
 Serial.print(value0_00);
 Serial.print(",");
 Serial.print(value0_01);
 Serial.print(",");
 Serial.print(value0_02);
 Serial.println();
 delay(10);
 Serial.print('F'); //........................................footer
 Serial.print(",");
 Serial.print(value1_00);
 Serial.print(",");
 Serial.print(analogRead(0));
 Serial.print(",");
 Serial.print(analogRead(1));
 Serial.println();
 delay(10);
}

/* processing code
import processing.serial.*;
Serial myPort;
char header = 'H';
char footer = 'F';

float x = 0;
float y = 0;

void setup() {
 size(600, 600);
 
 //println(Serial.list());
 myPort = new Serial(this, Serial.list()[0], 9600);
 //myPort = new Serial(this, "COM3", 9600);
 myPort.bufferUntil('\n');
 background(0);
}

void draw() {
  ellipse(x, y, 20, 20);
}

void serialEvent(Serial myPort)
{
 String rawData = myPort.readStringUntil('\n');
 if (rawData != null)
 {
   rawData = trim(rawData);
   println(rawData);
   String [] data = rawData.split(",");
   println("size of data = " + data.length);
   if (data[0].charAt(0) == header && data.length > 3)
   {
     for ( int i = 1; i < data.length; i++)
     {
       println("Value " + i + " = " + data[i]);
     }
   }else if(data[0].charAt(0) == footer && data.length > 3){
     for ( int i = 1; i < data.length; i++)
     {
       println("Value " + i + " = " + data[i]);
       
       if(i == 2){
         x = float(trim(data[2]));
         x = map(x, 0, 1023, 0, width);
       }
       if(i == 3){
         y = float(trim(data[3]));
         y = map(y, 0, 1023, 0, height);
       }
     }
   }
 }
}
*/
