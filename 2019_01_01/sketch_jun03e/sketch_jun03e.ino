const int dataNum = 3;
int index = 0;
int rawData[dataNum];

const int led_00 = 9;
const int led_01 = 10;
const int led_02 = 11;

const int minVal = 0;
const int maxVal = 255;

void setup()
{
  Serial.begin(9600);
  for (int i = 0; i < dataNum; i++) {
    rawData[i] = 0;
  }
}

void loop()
{
  if ( Serial.available()) {
    if (index == 0) {
      for (index; index < dataNum; index ++)
      {
        rawData[index] = Serial.parseInt();
        rawData[index] = constrain(rawData[index], minVal, maxVal);
      }

      analogWrite(led_00, rawData[0]);
      analogWrite(led_01, rawData[1]);
      analogWrite(led_02, rawData[2]);

      Serial.print('H'); //.......................................header
      Serial.print(",");
      Serial.print(rawData[0]);
      Serial.print(',');
      Serial.print(rawData[1]);
      Serial.print(',');
      Serial.println(rawData[2]);
    }
    if (Serial.read() == '\n' && index == dataNum) {
      index = 0;
      delay(10);
    }
  }
}

/*processing code
  import processing.serial.*;
  Serial myPort;
  char header = 'H';
  int dataNum = 3;

  float x = 0;
  float speedX = 8;

  float w = 0;
  float speedW = 2;

  void setup() {
  size(600, 600);

  println(Serial.list());
  myPort = new Serial(this, Serial.list()[0], 9600);
  //myPort  = new Serial(this, "COM3", 9600);
  }

  void draw() {
  background(120);

  stroke(255);
  strokeWeight(3);
  //fill(map(mouseX, 0, width, 0, 255));
  noFill();
  ellipse(x, mouseY, w, w);

  x += speedX;
  if(x > width){
   x = width;
   speedX = -8;
  }
  if(x < 0){
   x = 0;
   speedX = 8;
  }

  w += speedW;
  if(w > 300){
   w = 0;
  }

  myPort.write(int(map(x, 0, width, 0, 255)) + " " + int(map(mouseY, 0, height, 0, 255)) + " " + int(map(w, 0, 300, 0, 255)) + "\n");
  }

  void serialEvent(Serial myPort )
  {
  String rawData = myPort.readStringUntil('\n');
  if (rawData != null)
  {
   rawData = trim(rawData);
   println(rawData);
   String [] data = rawData.split(",");
   println("size of data = " + data.length);
   if (data[0].charAt(0) == header && data.length > dataNum)
   {
     for ( int i = 1; i < data.length; i++)
     {
       println("Value " + i + " = " + data[i]);
     }
   }
  }
  }
*/
