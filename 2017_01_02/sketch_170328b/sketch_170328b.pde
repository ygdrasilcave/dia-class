void setup() {
  size(1000, 1000);
  background(0, 0);
}

void draw() {
  for (int y=0; y<height/50; y++) {
    for (int x=0; x<width/50; x++) {
      /*if (y%2==0) {
       if (x%2 == 0) {
       fill(255);
       } else {
       fill(0);
       }
       } else {
       if (x%2 == 0) {
       fill(0);
       } else {
       fill(255);
       }
       }*/
      if ((x+y)%2 == 0) {
        fill(255);
      } else {
        fill(0);
      }
      rect(x*50, y*50, 50, 50);
    }
  }
}