float t = 0.0;
float theta = 0.0;

void setup() {
  size(600, 600);
  background(0);
}

void draw() {
  //background(0);
  float x = width/2 + cos(theta)*300;
  float y = height/2 + sin(theta)*300;
  //float x = width/2 + cos(theta)*(abs(sin(t))*100+200);
  //float y = height/2 + sin(theta)*(abs(sin(t))*100+200);
  //float x = width/2 + cos(theta)*sin(theta)*200;
  //float y = height/2 + sin(theta)*cos(theta)*200;
  
  ellipse(x, y, 20, 20);
  t += 0.0368;
  theta += 0.0125;
}