import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress iannix;

import beads.*;
import java.util.Arrays; 
AudioContext ac;

SamplePlayer sp0;
SamplePlayer sp1;
SamplePlayer sp2;

Glide rateValue0;
Glide rateValue1;
Glide rateValue2;

Gain g;

int scene = 1;

void setup() {
  size(500, 500);
  frameRate(25);
  oscP5 = new OscP5(this, 9000);
  iannix = new NetAddress("127.0.0.1", 9001);
  ac = new AudioContext();
  String sourceFile = sketchPath("") + "13.wav";
  sp0 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sourceFile = sketchPath("") + "coin_2.wav";
  sp1 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sourceFile = sketchPath("") + "bits_piece_3.wav";
  sp2 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sp0.setKillOnEnd(false);
  sp1.setKillOnEnd(false);
  sp2.setKillOnEnd(false);
  rateValue0 = new Glide(ac, 1, 1);
  rateValue1 = new Glide(ac, 1, 1);
  rateValue2 = new Glide(ac, 1, 1);
  sp0.setRate(rateValue0);
  sp1.setRate(rateValue1);
  sp2.setRate(rateValue2);
  g = new Gain(ac, 2, 0.1);
  g.addInput(sp0);
  g.addInput(sp1);
  g.addInput(sp2);
  ac.out.addInput(g);
  sp0.pause(true);
  sp1.pause(true);
  sp2.pause(true);
  ac.start();
}

void draw() {
  background(0);
  noStroke();
  if (scene == 1) {
    fill(255, 0, 0);
  } else if (scene == 2) {
    fill(0, 255, 0);
  } else if (scene == 3) {
    fill(0, 0, 255);
  }
  ellipse(width/2, height/2, 200, 200);
}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/test")==true) {
    if (theOscMessage.checkTypetag("ff")) {
      float value0 = theOscMessage.get(0).floatValue();
      float value1 = theOscMessage.get(1).floatValue();
      println(value0);
      scene = int(value0);
      if (scene == 1) {
        sp0.setToLoopStart();
        rateValue0.setValue(value1);
        sp0.start();
        sp1.pause(true);
        sp2.pause(true);
      } else if (scene == 2) {
        sp1.setToLoopStart();
        rateValue1.setValue(value1);
        sp1.start();
        sp0.pause(true);
        sp2.pause(true);
      } else if (scene == 3) {
        sp2.setToLoopStart();
        rateValue2.setValue(value1);
        sp2.start();
        sp0.pause(true);
        sp1.pause(true);
      }
      return;
    }
  }
}