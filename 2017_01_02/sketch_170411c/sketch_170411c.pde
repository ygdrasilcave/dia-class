PImage img;
int pixelSize = 10;
float t = 0.0;

void setup() {
  size(600, 431);
  img = loadImage("flower2.jpg");
  background(0);
}

void draw() {
  for (int y=0; y<img.height; y+=pixelSize) {
    for (int x = 0; x<img.width; x+=pixelSize) {
      color c = img.pixels[x+y*img.width];
      fill(c);
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  
  //pixelSize = int(map(mouseX, 0, width, 1, 50));
  pixelSize = int(abs(sin(t))*50 + 1);
  t += 0.015;
}