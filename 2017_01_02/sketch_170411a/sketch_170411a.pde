PImage img;

int i = 90;
int[] j;

void setup(){
  size(600, 431);
  img = loadImage("flower2.jpg");
  
  i = 128;
  
  j = new int[10];
  for(int k=0; k<j.length; k++){
    j[k] = int(random(255));
  }
  
  println(i);
  println(j);
  println(j[7]);
  println(img.pixels.length);
}

void draw(){
  image(img, 0, 0);
  
  color c = img.pixels[mouseX + mouseY*img.width];
  
  fill(c);
  noStroke();
  rect(mouseX, mouseY, 50, 50);
}