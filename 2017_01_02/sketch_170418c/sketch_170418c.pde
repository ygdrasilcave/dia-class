float[] x;
float[] y;
float t = 0.0;

void setup(){
  size(600, 600);
  background(0);
  x = new float[12];
  y = new float[12];

  for(int i=0; i<x.length; i++){
    x[i] = width/2 + cos(radians(i*30-60))*250;
    y[i] = height/2 + sin(radians(i*30-60))*250;    
  }
  
  println(x);
}

void draw(){
  background(0);
  for(int i=0; i<x.length; i++){
    ellipse(x[i], y[i], 20, 20);
    //text(str(i+1), x[i], y[i]);
  }
  
  x[0] = width/2 + cos(radians(-60))*abs(sin(t))*250;
  y[0] = height/2 + sin(radians(-60))*abs(sin(t))*250;
  t+=0.0365;

}