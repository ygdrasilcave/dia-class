Flower[] flower;

void setup() {
  size(1000, 800);
  frameRate(6);
  
  colorMode(HSB, 255, 255, 255);
  
  flower = new Flower[25];

  for (int i=0; i<flower.length; i++) {
    flower[i] = new Flower();
  }

  for (int y=0; y<5; y++) {
    for (int x=0; x<5; x++) {
      int index = x + y*5;
      float xw = width/6 * x + width/6;
      float yh = height/6 * y + height/6;
      boolean type;
      if((x+y)%2 == 0){
        type = true;
      }else{
        type = false;
      }
      flower[index] = new Flower(xw, yh, random(20, (width/6)/3), type);
    }
  }
}

void draw() {
  background(0);
  for (int i=0; i<flower.length; i++) {
    flower[i].drawFlower();
  }
}