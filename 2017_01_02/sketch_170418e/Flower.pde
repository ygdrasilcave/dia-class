class Flower {

  float x, y;
  int num;
  float w, h, offset;
  float hue;
  int numSpeed = 1;
  int numDir = 1;
  boolean colorType;

  Flower() {
    x = random(width);
    y = random(height);
    num = int(random(6, 32));
    w = random(50, 150);
    h = w*random(0.1, 0.6);
    offset = w/2+random(-w/3, w/3);
    hue = random(255);
  }

  Flower(float _x, float _y, float _w, boolean _type) {
    x = _x;
    y = _y;
    num = int(random(6, 32));
    w = _w;
    h = w*random(0.1, 0.6);
    offset = w/2+random(-w/3, w/3);
    hue = random(255);
    colorType = _type;
  }

  void drawFlower() {
    if (colorType == true) {
      noStroke();
      fill(hue, 255, 255);
    } else {
      stroke(hue, 0, 255);
      noFill();
    }
    for (int i=0; i<num; i++) {
      pushMatrix();
      translate(x, y);
      rotate(radians(360.0/num*i));
      ellipse(offset, 0, w, h);
      popMatrix();
    }
    num += numSpeed*numDir;
    if (num > 64) {
      numDir = numDir*-1;
    }
    if (num < 2 ) {
      numDir = numDir*-1;
    }
  }
};