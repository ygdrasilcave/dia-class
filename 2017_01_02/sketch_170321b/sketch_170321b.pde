float rX = 0.0;
float speedX = 2.0;
float dirX = 1.0;
float rY = 0.0;
float speedY = 5.0;
float dirY = 1.0;
boolean shape = true;

void setup(){
  size(1000, 1000);
  background(0);
}

void draw(){
  background(0);
  if(shape == true){
  ellipse(width/2, height/2, rX, rY);
  }else{
    rect(width/2-rX/2.0, height/2-rY/2.0, rX, rY);
  }
  rX = rX + dirX*speedX;
  rY = rY + dirY*speedY;
  if(rX >= width || rX <= 0){
    dirX = dirX*(-1);
    shape = !shape;
  }
  if(rY >= height || rY <= 0){
    dirY = dirY*(-1);
    shape = !shape;
  }
}