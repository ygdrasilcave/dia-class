float rotVal = 0.0;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  translate(width/2, height/2);
  rotate(radians(rotVal));
  rect(-100, -100, 200, 200);
  rotVal += 1;
}