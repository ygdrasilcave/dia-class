float x, y;
float speedX, speedY;
float dirX, dirY;

void setup(){
  size(600, 600);
  background(0);
  x = width/2;
  y = height/2;
  speedX = random(1, 5);
  speedY = random(2, 8);
  dirX = 1.0;
  dirY = 1.0;
}

void draw(){
  background(0);
  x += speedX*dirX;
  y += speedY*dirY;
  ellipse(x, y, 20, 20);
  if(x > width){
    dirX = dirX*-1.0;
  }
  if(x < 0){
    dirX *= -1.0;
  }
  if(y > height){
    dirY *= -1.0;
  }
  if(y < 0){
    dirY *= -1.0;
  }
}