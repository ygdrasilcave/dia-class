float w = 100;
float h = 200;

void setup() {
  size(800, 600);
  background(0);
}

void draw() {
  //background(0);
  
  strokeWeight(1);
  stroke(255);
  fill(255, 120, 0);
  noFill();
  rect((width/2) - (w/2), (height/2) - (h/2), w, h);
  
  w = mouseX;
  h = mouseY;
}

void keyPressed(){
  if(key == 'a'){
    println("a");
    saveFrame("myWork_#####.jpg");
  }else{
    println("bang");
  }
}