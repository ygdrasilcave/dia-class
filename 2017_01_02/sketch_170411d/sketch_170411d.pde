PImage img;
int pixelSize = 20;

void setup() {
  size(600, 431);
  img = loadImage("flower2.jpg");
  background(0);
}

void draw() {
  for (int y=0; y<img.height; y+=pixelSize) {
    for (int x = 0; x<img.width; x+=pixelSize) {
      color c = img.pixels[x+y*img.width];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      
      //fill(255-r, 255-g, 255-b);
      fill(br);
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  
  pixelSize = int(map(mouseX, 0, width, 1, 60));
}