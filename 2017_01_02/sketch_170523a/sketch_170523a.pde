import de.looksgood.ani.*;

Ani animationX;
Ani animationY;

float xPos;
float yPos;

void setup() {
  size(800, 800);

  Ani.init(this);
  Ani.noAutostart();

  animationX = new Ani(this, 2.0, "xPos", width-100, Ani.BOUNCE_OUT);
  animationX.setBegin(100);
  animationX.setCallback("onEnd:aniX_end_callback");

  animationY = new Ani(this, 2.0, "yPos", 100, Ani.BOUNCE_OUT);
  animationY.setBegin(height-100);
  animationY.setCallback("onEnd:aniY_end_callback");

  xPos = 100;
  yPos = height-100;
}

void draw() {
  background(0);
  fill(255);
  ellipse(xPos, yPos, 50, 50);
}

void aniX_end_callback() {
  animationY.setDuration(random(0.5, 2.5));  
  if (yPos < height/2) {
    animationY.setEnd(height-100);
    animationY.setBegin(100);
  } else {
    animationY.setEnd(100);
    animationY.setBegin(height-100);
  }
  animationY.setDelay(0.5);
  animationY.start();
}

void aniY_end_callback(){
  if(xPos < width/2){
    animationX.setEnd(height-100);
    animationX.setBegin(100);
  }else{
    animationX.setEnd(100);
    animationX.setBegin(height-100);
  }
}

void keyPressed() {
  if (key == 'a') {
    animationX.setDuration(random(0.5, 2.5));
    animationX.start();
  } else if (key == 'b') {
    animationY.setDuration(random(0.5, 2.5));
    animationY.start();
  }
}