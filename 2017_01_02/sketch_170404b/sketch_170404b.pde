float rotVal = 0.0;
float rad = 0.0;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  ellipse(width/2, height/2, 50, 50);
  
  float x = 0.0;
  float y = 0.0;
  x = cos(radians(45)) * rad;
  y = sin(radians(45)) * rad;
  ellipse(x, y, 30, 30);
  
  rad += 1;
  if(rad > 800*sqrt(2)){
    rad = 0.0;
  }
}