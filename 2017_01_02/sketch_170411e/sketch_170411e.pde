PImage img;
int pixelSize = 20;
int threshold = 127;
float t = 0.0;

void setup() {
  size(600, 431);
  img = loadImage("flower2.jpg");
  background(0);
}

void draw() {
  //image(img, 0, 0);
  for (int y=0; y<img.height; y+=pixelSize) {
    for (int x = 0; x<img.width; x+=pixelSize) {
      color c = img.pixels[x + y*img.width];
      
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      
      /*if(br > 85*2){
        fill(255);
      }else if(br<= 85*2 && br > 85){
        fill(127);
      }else{
        fill(0);
      }*/
      
      if(br > threshold){
        fill(255);
      }else{
        fill(0);
      }
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  
  pixelSize = int(map(mouseX, 0, width, 1, 60));
  threshold = int(map(mouseY, 0, height, 1, 254));
  
  //threshold = int(map(cos(t), -1, 1, 1, 254));
  t+=0.0225;
}