float angle = 0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  //background(0);
  pushMatrix();
   //translate(width/3, 0);
   fill(0, 3);
   //stroke(255);
   //strokeWeight(10);
   rect(0, 0, width, height);
   popMatrix();

  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(angle-90));
  //fill(255);
  //noStroke();
  noFill();
  stroke(255);
  ellipse(200, 0, map(mouseX, 0, width, 20, 400), 50);
  popMatrix();
  //angle = second()*6;
  angle = angle + map(mouseY, 0, height, 1, 15);
}