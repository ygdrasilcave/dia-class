import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress iannix;

int scene = 1;

void setup() {
  size(500, 500);
  frameRate(25);
  oscP5 = new OscP5(this, 9000);
  iannix = new NetAddress("127.0.0.1", 8000);
}

void draw() {
  background(0);
  noStroke();
  if(scene == 1){
    fill(255, 0, 0);
  }else if(scene == 2){
    fill(0, 255, 0);
  }else if(scene == 3){
    fill(0, 0, 255);
  }
  ellipse(width/2, height/2, 200, 200);
}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/test")==true) {
    if (theOscMessage.checkTypetag("f")) {
      float value0 = theOscMessage.get(0).floatValue();
      println(value0);
      scene = int(value0);
      return;
    }
  }
}