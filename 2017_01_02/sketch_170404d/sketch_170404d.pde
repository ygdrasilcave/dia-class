float rotVal = 0.0;
float rad = 350.0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);
  fill(255);
  noStroke();
  ellipse(width/2, height/2, 30, 30);

  float x = 0.0;
  float y = 0.0;

  for (int i=0; i<12; i++) {
    x = width/2 + cos(radians(i*30-60)) * rad;
    y = height/2 + sin(radians(i*30-60)) * rad;
    fill(255, 255, 0);
    //ellipse(x, y, 30, 30);
    //fill(255, 0, 0);
    textAlign(CENTER, CENTER);
    textSize(32);
    text(str(i+1), x, y);
  }
  
  float secondX = width/2 + cos(radians(second()*6-90)) * (rad-50);
  float secondY = width/2 + sin(radians(second()*6-90)) * (rad-50);
  ellipse(secondX, secondY, 20, 20);
  
  float minuteX = width/2 + cos(radians(minute()*6-90)) * (rad-60);
  float minuteY = width/2 + sin(radians(minute()*6-90)) * (rad-60);
  stroke(255);
  strokeWeight(4);
  line(width/2, height/2, minuteX, minuteY);
  
  float hourAngle = (hour()*30-90) + map(minute(),0,59,0,29);
  float hourX = width/2 + cos(radians(hourAngle)) * (rad-120);
  float hourY = width/2 + sin(radians(hourAngle)) * (rad-120);
  stroke(255);
  strokeWeight(5);
  line(width/2, height/2, hourX, hourY);
}