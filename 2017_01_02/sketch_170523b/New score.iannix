/*
 *	IanniX Score File
 */


/*
 *	This method is called first.
 *	It is the good section for asking user for script global variables (parameters).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function askUserForParameters() {
	//title("The title of the parameter box");
	//ask("Group name of the parameter (only for display purposes)", "Parameter label", "myGlobalVar", "theDefaultValue");
}


/*
 *	This method stores all the operations made through IanniX scripts.
 *	You can add some commands here to make your own scripts!
 *	Scripts are written in Javascript but even with a limited knowledge of Javascript, many types of useful scripts can be created.
 *	
 *	Beyond the standard javascript commands, the run() function is used to send commands to IanniX.
 *	Commands must be provided to run() as a single string.
 *	For example, run("zoom 100"); sets the display zoom to 100%.
 *	
 *	To combine numeric parameters with text commands to produce a string, use the concatenation operator.
 *	In the following example center_x and center_y are in numeric variables and must be concatenated to the command string.
 *	Example: run("setPos current " + center_x + " " + center_y + " 0");
 *	
 *	To learn IanniX commands, perform an manipulation in IanniX graphical user interface, and see the Helper window.
 *	You'll see the syntax of the command-equivalent action.
 *	
 *	And finally, remember that most of commands must target an object.
 *	Global syntax is always run("<command name> <target> <arguments>");
 *	Targets can be an ID (number) or a Group ID (string name of group) (please see "Info" tab in Inspector panel).
 *	Special targets are "current" (last used ID), "all" (all the objects) and "lastCurve" (last used curve).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function makeWithScript() {
	//Clears the score
	run("clear");
	//Resets rotation
	run("rotate 0 0 0");
	//Resets score viewport center
	run("center 0 0");
	//Resets score zoom
	run("zoom 100");
}


/*
 *	When an incoming message is received, this method is called.
 *		- <protocol> tells information about the nature of message ("osc", "midi", "direct…)
 *		- <host> and <port> gives the origin of message, specially for IP protocols (for OpenSoundControl, UDP or TCP, it is the IP and port of the application that sends the message)
 *		- <destination> is the supposed destination of message (for OpenSoundControl it is the path, for MIDI it is Control Change or Note on/off…)
 *		- <values> are an array of arguments contained in the message
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function onIncomingMessage(protocol, host, port, destination, values) {
	//Logs a message in the console (open "Config" tab from Inspector panel and see "Message log")
	console("Received on '" + protocol + "' (" + host + ":" + port + ") to '" + destination + "', " + values.length + " values : ");
	
	//Browses all the arguments and displays them in log window
	for(var valueIndex = 0 ; valueIndex < values.length ; valueIndex++)
		console("- arg " + valueIndex + " = " + values[valueIndex]);
}


/*
 *	This method stores all the operations made through the graphical user interface.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughGUI() {
//GUI: NEVER EVER REMOVE THIS LINE
	run("center 0.22212 0.236564");
	run("zoom 36");
	run("rotate 0 0 0");
	run("speed 1.48");


	run("add curve 1");
	run("setpos current -1.66197 0.687228 0");
	var points1 = [
		{x: 0, y: 0, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 0, y: -0.0138335, z: 0, c1x: 0, c1y: -0.0027667, c1z: 0, c2x: 0, c2y: 0.0110668, c2z: 0},
		{x: 0, y: -0.0553342, z: 0, c1x: 0, c1y: -0.0110668, c1z: 0, c2x: -0.0027667, c2y: 0.0332005, c2z: 0},
		{x: 0.0138335, y: -0.179836, z: 0, c1x: 0.0027667, c1y: -0.0332005, c1z: 0, c2x: -0.0055334, c2y: 0.0332006, c2z: 0},
		{x: 0.027667, y: -0.221337, z: 0, c1x: 0.0055334, c1y: -0.0332006, c1z: 0, c2x: -0.0138335, c2y: 0.0304338, c2z: 0},
		{x: 0.0830011, y: -0.332005, z: 0, c1x: 0.0138335, c1y: -0.0304338, c1z: 0, c2x: -0.0166002, c2y: 0.0276672, c2z: 0},
		{x: 0.110668, y: -0.359673, z: 0, c1x: 0.0166002, c1y: -0.0276672, c1z: 0, c2x: -0.0442674, c2y: 0.0553344, c2z: 0},
		{x: 0.304338, y: -0.608677, z: 0, c1x: 0.0442674, c1y: -0.0553344, c1z: 0, c2x: -0.0664012, c2y: 0.0691678, c2z: 0},
		{x: 0.442674, y: -0.705512, z: 0, c1x: 0.0664012, c1y: -0.0691678, c1z: 0, c2x: -0.105135, c2y: 0.0913006, c2z: 0},
		{x: 0.830014, y: -1.06518, z: 0, c1x: 0.105135, c1y: -0.0913006, c1z: 0, c2x: -0.113435, c2y: 0.102368, c2z: 0},
		{x: 1.00985, y: -1.21735, z: 0, c1x: 0.113435, c1y: -0.102368, c1z: 0, c2x: -0.0719352, c2y: 0.055336, c2z: 0},
		{x: 1.18969, y: -1.34186, z: 0, c1x: 0.0719352, c1y: -0.055336, c1z: 0, c2x: -0.149402, c2y: 0.094068, c2z: 0},
		{x: 1.75686, y: -1.68769, z: 0, c1x: 0.149402, c1y: -0.094068, c1z: 0, c2x: -0.143868, c2y: 0.080234, c2z: 0},
		{x: 1.90903, y: -1.74303, z: 0, c1x: 0.143868, c1y: -0.080234, c1z: 0, c2x: -0.083002, c2y: 0.024902, c2z: 0},
		{x: 2.17187, y: -1.8122, z: 0, c1x: 0.083002, c1y: -0.024902, c1z: 0, c2x: -0.071934, c2y: 0.0166, c2z: 0},
		{x: 2.2687, y: -1.82603, z: 0, c1x: 0.071934, c1y: -0.0166, c1z: 0, c2x: -0.030434, c2y: 0.00830002, c2z: 0},
		{x: 2.32404, y: -1.8537, z: 0, c1x: 0.030434, c1y: -0.00830002, c1z: 0, c2x: -0.030434, c2y: 0.011066, c2z: 0},
		{x: 2.42087, y: -1.88136, z: 0, c1x: 0.030434, c1y: -0.011066, c1z: 0, c2x: -0.030434, c2y: 0.005532, c2z: 0},
		{x: 2.47621, y: -1.88136, z: 0, c1x: 0.030434, c1y: -0.005532, c1z: 0, c2x: -0.047034, c2y: 0, c2z: 0},
		{x: 2.65604, y: -1.88136, z: 0, c1x: 0.047034, c1y: 0, c1z: 0, c2x: -0.0498, c2y: -0.00276601, c2z: 0},
		{x: 2.72521, y: -1.86753, z: 0, c1x: 0.0498, c1y: 0.00276601, c1z: 0, c2x: -0.0681044, c2y: -0.0150378, c2z: 0},
		{x: 2.99656, y: -1.80617, z: 0, c1x: 0.0681044, c1y: 0.0150378, c1z: 0, c2x: -0.080234, c2y: -0.035968, c2z: 0},
		{x: 3.12638, y: -1.68769, z: 0, c1x: 0.080234, c1y: 0.035968, c1z: 0, c2x: -0.0397976, c2y: -0.0375283, c2z: 0},
		{x: 3.19555, y: -1.61853, z: 0, c1x: 0.0397976, c1y: 0.0375283, c1z: 0, c2x: -0.041502, c2y: -0.0332, c2z: 0},
		{x: 3.33389, y: -1.52169, z: 0, c1x: 0.041502, c1y: 0.0332, c1z: 0, c2x: -0.038734, c2y: -0.024902, c2z: 0},
		{x: 3.38922, y: -1.49402, z: 0, c1x: 0.038734, c1y: 0.024902, c1z: 0, c2x: -0.0415, c2y: -0.0415, c2z: 0},
		{x: 3.54139, y: -1.31419, z: 0, c1x: 0.0415, c1y: 0.0415, c1z: 0, c2x: -0.038734, c2y: -0.047034, c2z: 0},
		{x: 3.58289, y: -1.25885, z: 0, c1x: 0.038734, c1y: 0.047034, c1z: 0, c2x: -0.019368, c2y: -0.052568, c2z: 0},
		{x: 3.63823, y: -1.05135, z: 0, c1x: 0.019368, c1y: 0.052568, c1z: 0, c2x: -0.0166, c2y: -0.0608668, c2z: 0},
		{x: 3.66589, y: -0.954516, z: 0, c1x: 0.0166, c1y: 0.0608668, c1z: 0, c2x: -0.00829997, c2y: -0.0415006, c2z: 0},
		{x: 3.67973, y: -0.843847, z: 0, c1x: 0.00829997, c1y: 0.0415006, c1z: 0, c2x: -0.00276799, c2y: -0.110669, c2z: 0},
		{x: 3.67973, y: -0.401173, z: 0, c1x: 0.00276799, c1y: 0.110669, c1z: 0, c2x: 0.513914, c2y: 0.0563542, c2z: 0},
		{x: 3.67973, y: -0.290505, z: 0, c1x: 0, c1y: 0.110668, c1z: 0, c2x: 0.0166, c2y: -0.107902, c2z: 0},
		{x: 3.59673, y: 0.138336, z: 0, c1x: -0.0166, c1y: 0.107902, c1z: 0, c2x: 0.019368, c2y: -0.113435, c2z: 0},
		{x: 3.58289, y: 0.276671, z: 0, c1x: -0.019368, c1y: 0.113435, c1z: 0, c2x: 0.00830202, c2y: -0.0525674, c2z: 0},
		{x: 3.55522, y: 0.401173, z: 0, c1x: -0.00830202, c1y: 0.0525674, c1z: 0, c2x: 0.044266, c2y: -0.110669, c2z: 0},
		{x: 3.36156, y: 0.830014, z: 0, c1x: -0.044266, c1y: 0.110669, c1z: 0, c2x: 0.055334, c2y: -0.116202, c2z: 0},
		{x: 3.27855, y: 0.982183, z: 0, c1x: -0.055334, c1y: 0.116202, c1z: 0, c2x: 0.058102, c2y: -0.0774672, c2z: 0},
		{x: 3.07105, y: 1.21735, z: 0, c1x: -0.058102, c1y: 0.0774672, c1z: 0, c2x: 0.060866, c2y: -0.0664014, c2z: 0},
		{x: 2.97422, y: 1.31419, z: 0, c1x: -0.060866, c1y: 0.0664014, c1z: 0, c2x: 0.074702, c2y: -0.063636, c2z: 0},
		{x: 2.69754, y: 1.53553, z: 0, c1x: -0.074702, c1y: 0.063636, c1z: 0, c2x: 0.069168, c2y: -0.055334, c2z: 0},
		{x: 2.62838, y: 1.59086, z: 0, c1x: -0.069168, c1y: 0.055334, c1z: 0, c2x: 0.0332, c2y: -0.0249, c2z: 0},
		{x: 2.53154, y: 1.66003, z: 0, c1x: -0.0332, c1y: 0.0249, c1z: 0, c2x: 0.047034, c2y: -0.0332, c2z: 0},
		{x: 2.39321, y: 1.75686, z: 0, c1x: -0.047034, c1y: 0.0332, c1z: 0, c2x: 0.0332, c2y: -0.022134, c2z: 0},
		{x: 2.36554, y: 1.7707, z: 0, c1x: -0.0332, c1y: 0.022134, c1z: 0, c2x: 0.027668, c2y: -0.013834, c2z: 0},
		{x: 2.25487, y: 1.82603, z: 0, c1x: -0.027668, c1y: 0.013834, c1z: 0, c2x: 0.027668, c2y: -0.011066, c2z: 0},
		{x: 2.2272, y: 1.82603, z: 0, c1x: -0.027668, c1y: 0.011066, c1z: 0, c2x: 0.019366, c2y: 0, c2z: 0},
		{x: 2.15804, y: 1.82603, z: 0, c1x: -0.019366, c1y: 0, c1z: 0, c2x: 0.019366, c2y: 0, c2z: 0},
		{x: 2.13037, y: 1.82603, z: 0, c1x: -0.019366, c1y: 0, c1z: 0, c2x: 0.011068, c2y: 0, c2z: 0},
		{x: 2.1027, y: 1.82603, z: 0, c1x: -0.011068, c1y: 0, c1z: 0, c2x: 0.0332, c2y: 0.00276601, c2z: 0},
		{x: 1.96437, y: 1.8122, z: 0, c1x: -0.0332, c1y: -0.00276601, c1z: 0, c2x: 0.035968, c2y: 0.011066, c2z: 0},
		{x: 1.92286, y: 1.7707, z: 0, c1x: -0.035968, c1y: -0.011066, c1z: 0, c2x: 0.035968, c2y: 0.044268, c2z: 0},
		{x: 1.78453, y: 1.59086, z: 0, c1x: -0.035968, c1y: -0.044268, c1z: 0, c2x: 0.038732, c2y: 0.055336, c2z: 0},
		{x: 1.7292, y: 1.49402, z: 0, c1x: -0.038732, c1y: -0.055336, c1z: 0, c2x: 0.019368, c2y: 0.030434, c2z: 0},
		{x: 1.68769, y: 1.43869, z: 0, c1x: -0.019368, c1y: -0.030434, c1z: 0, c2x: 0.027668, c2y: 0.063634, c2z: 0},
		{x: 1.59086, y: 1.17585, z: 0, c1x: -0.027668, c1y: -0.063634, c1z: 0, c2x: 0.027666, c2y: 0.069168, c2z: 0},
		{x: 1.54936, y: 1.09285, z: 0, c1x: -0.027666, c1y: -0.069168, c1z: 0, c2x: 0.019368, c2y: 0.0664006, c2z: 0},
		{x: 1.49402, y: 0.843847, z: 0, c1x: -0.019368, c1y: -0.0664006, c1z: 0, c2x: 0.011068, c2y: 0.0608674, c2z: 0},
		{x: 1.49402, y: 0.788513, z: 0, c1x: -0.011068, c1y: -0.0608674, c1z: 0, c2x: 0.019366, c2y: 0.0498008, c2z: 0},
		{x: 1.39719, y: 0.594843, z: 0, c1x: -0.019366, c1y: -0.0498008, c1z: 0, c2x: 0.027666, c2y: 0.0608676, c2z: 0},
		{x: 1.35569, y: 0.484175, z: 0, c1x: -0.027666, c1y: -0.0608676, c1z: 0, c2x: 0.019368, c2y: 0.027667, c2z: 0},
		{x: 1.30035, y: 0.456508, z: 0, c1x: -0.019368, c1y: -0.027667, c1z: 0, c2x: 0.047034, c2y: 0.0470342, c2z: 0},
		{x: 1.12052, y: 0.249004, z: 0, c1x: -0.047034, c1y: -0.0470342, c1z: 0, c2x: 0.052566, c2y: 0.0553344, c2z: 0},
		{x: 1.03752, y: 0.179836, z: 0, c1x: -0.052566, c1y: -0.0553344, c1z: 0, c2x: 0.0498012, c2y: 0.0415007, c2z: 0},
		{x: 0.871514, y: 0.0415007, z: 0, c1x: -0.0498012, c1y: -0.0415007, c1z: 0, c2x: 0.0525682, c2y: 0.0387339, c2z: 0},
		{x: 0.774679, y: -0.0138335, z: 0, c1x: -0.0525682, c1y: -0.0387339, c1z: 0, c2x: 0.0608676, c2y: 0.0359673, c2z: 0},
		{x: 0.567176, y: -0.138336, z: 0, c1x: -0.0608676, c1y: -0.0359673, c1z: 0, c2x: 0.0498008, c2y: 0.0249005, c2z: 0},
		{x: 0.525675, y: -0.138336, z: 0, c1x: -0.0498008, c1y: -0.0249005, c1z: 0, c2x: 0.0110668, c2y: 0.0027666, c2z: 0},
		{x: 0.511842, y: -0.152169, z: 0, c1x: -0.0110668, c1y: -0.0027666, c1z: 0, c2x: 0.0110668, c2y: 0.0027666, c2z: 0},
		{x: 0.470341, y: -0.152169, z: 0, c1x: -0.0110668, c1y: -0.0027666, c1z: 0, c2x: 0.011067, c2y: 0, c2z: 0},
		{x: 0.456507, y: -0.152169, z: 0, c1x: -0.011067, c1y: 0, c1z: 0, c2x: 0.038734, c2y: -0.0110668, c2z: 0},
		{x: 0.276671, y: -0.0968349, z: 0, c1x: -0.038734, c1y: 0.0110668, c1z: 0, c2x: 0.066401, c2y: -0.0166003, c2z: 0},
		{x: 0.124502, y: -0.0691677, z: 0, c1x: -0.066401, c1y: 0.0166003, c1z: 0, c2x: 0.0940682, c2y: -0.0138336, c2z: 0},
		{x: -0.19367, y: -0.027667, z: 0, c1x: -0.0940682, c1y: 0.0138336, c1z: 0, c2x: 0.0802346, c2y: -0.0110668, c2z: 0},
		{x: -0.276671, y: -0.0138335, z: 0, c1x: -0.0802346, c1y: 0.0110668, c1z: 0, c2x: 0.0249004, c2y: -0.0055334, c2z: 0},
		{x: -0.318172, y: 0, z: 0, c1x: -0.0249004, c1y: 0.0055334, c1z: 0, c2x: 0.0138336, c2y: -0.0110668, c2z: 0},
		{x: -0.345839, y: 0.0415007, z: 0, c1x: -0.0138336, c1y: 0.0110668, c1z: 0, c2x: 0.0110668, c2y: -0.0138336, c2z: 0},
		{x: -0.373506, y: 0.0691679, z: 0, c1x: -0.0110668, c1y: 0.0138336, c1z: 0, c2x: 0.0055334, c2y: -0.0276671, c2z: 0},
		{x: -0.373506, y: 0.179836, z: 0, c1x: -0.0055334, c1y: 0.0276671, c1z: 0, c2x: 0, c2y: -0.0415006, c2z: 0},
		{x: -0.373506, y: 0.276671, z: 0, c1x: 0, c1y: 0.0415006, c1z: 0, c2x: 0, c2y: -0.0276672, c2z: 0},
		{x: -0.373506, y: 0.318172, z: 0, c1x: 0, c1y: 0.0276672, c1z: 0, c2x: -0.0027666, c2y: -0.0525676, c2z: 0},
		{x: -0.359673, y: 0.539509, z: 0, c1x: 0.0027666, c1y: 0.0525676, c1z: 0, c2x: -0.0055334, c2y: -0.0525674, c2z: 0},
		{x: -0.345839, y: 0.581009, z: 0, c1x: 0.0055334, c1y: 0.0525674, c1z: 0, c2x: -0.0221338, c2y: -0.027667, c2z: 0},
		{x: -0.249004, y: 0.677844, z: 0, c1x: 0.0221338, c1y: 0.027667, c1z: 0, c2x: -0.027667, c2y: -0.0221338, c2z: 0},
		{x: -0.207504, y: 0.691678, z: 0, c1x: 0.027667, c1y: 0.0221338, c1z: 0, c2x: -0.027667, c2y: -0.0055336, c2z: 0},
		{x: -0.110669, y: 0.705512, z: 0, c1x: 0.027667, c1y: 0.0055336, c1z: 0, c2x: -0.0276672, c2y: -0.0083002, c2z: 0},
		{x: -0.0691681, y: 0.733179, z: 0, c1x: 0.0276672, c1y: 0.0083002, c1z: 0, c2x: -0.0193671, c2y: -0.0055334, c2z: 0},
		{x: -0.0138335, y: 0.733179, z: 0, c1x: 0.0193671, c1y: 0.0055334, c1z: 0, c2x: -0.0304338, c2y: 0, c2z: 0},
		{x: 0.0830011, y: 0.733179, z: 0, c1x: 0.0304338, c1y: 0, c1z: 0, c2x: -0.0359673, c2y: 0, c2z: 0},
		{x: 0.166003, y: 0.733179, z: 0, c1x: 0.0359673, c1y: 0, c1z: 0, c2x: -0.038734, c2y: 0.0027668, c2z: 0},
		{x: 0.276671, y: 0.719345, z: 0, c1x: 0.038734, c1y: -0.0027668, c1z: 0, c2x: -0.0304338, c2y: 0.0055334, c2z: 0},
		{x: 0.318172, y: 0.705512, z: 0, c1x: 0.0304338, c1y: -0.0055334, c1z: 0, c2x: -0.0166004, c2y: 0.0138336, c2z: 0},
		{x: 0.359673, y: 0.650177, z: 0, c1x: 0.0166004, c1y: -0.0138336, c1z: 0, c2x: -0.0166002, c2y: 0.0166004, c2z: 0},
		{x: 0.401173, y: 0.62251, z: 0, c1x: 0.0166002, c1y: -0.0166004, c1z: 0, c2x: -0.0110668, c2y: 0.0110668, c2z: 0},
		{x: 0.415007, y: 0.594843, z: 0, c1x: 0.0110668, c1y: -0.0110668, c1z: 0, c2x: -0.0166004, c2y: 0.038734, c2z: 0},
		{x: 0.484175, y: 0.42884, z: 0, c1x: 0.0166004, c1y: -0.038734, c1z: 0, c2x: -0.0221336, c2y: 0.0442674, c2z: 0},
		{x: 0.525675, y: 0.373506, z: 0, c1x: 0.0221336, c1y: -0.0442674, c1z: 0, c2x: -0.0221336, c2y: 0.0747011, c2z: 0},
		{x: 0.594843, y: 0.0553343, z: 0, c1x: 0.0221336, c1y: -0.0747011, c1z: 0, c2x: -0.019367, c2y: 0.0774679, c2z: 0},
		{x: 0.62251, y: -0.0138335, z: 0, c1x: 0.019367, c1y: -0.0774679, c1z: 0, c2x: -0.0110668, c2y: 0.0664011, c2z: 0},
		{x: 0.650177, y: -0.276671, z: 0, c1x: 0.0110668, c1y: -0.0664011, c1z: 0, c2x: -0.011067, c2y: 0.0636343, c2z: 0},
		{x: 0.677845, y: -0.332005, z: 0, c1x: 0.011067, c1y: -0.0636343, c1z: 0, c2x: -0.0083002, c2y: 0.0304338, c2z: 0},
		{x: 0.691678, y: -0.42884, z: 0, c1x: 0.0083002, c1y: -0.0304338, c1z: 0, c2x: -0.0166002, c2y: 0.0719346, c2z: 0},
		{x: 0.760846, y: -0.691678, z: 0, c1x: 0.0166002, c1y: -0.0719346, c1z: 0, c2x: -0.0166002, c2y: 0.0636344, c2z: 0},
		{x: 0.774679, y: -0.747012, z: 0, c1x: 0.0166002, c1y: -0.0636344, c1z: 0, c2x: -0.0027666, c2y: 0.0415006, c2z: 0},
		{x: 0.774679, y: -0.899181, z: 0, c1x: 0.0027666, c1y: -0.0415006, c1z: 0, c2x: 0, c2y: 0.0498008, c2z: 0},
		{x: 0.774679, y: -0.996016, z: 0, c1x: 0, c1y: -0.0498008, c1z: 0, c2x: 0, c2y: 0.0331998, c2z: 0},
		{x: 0.774679, y: -1.06518, z: 0, c1x: 0, c1y: -0.0331998, c1z: 0, c2x: 0, c2y: 0.0636348, c2z: 0},
		{x: 0.774679, y: -1.31419, z: 0, c1x: 0, c1y: -0.0636348, c1z: 0, c2x: 0, c2y: 0.077468, c2z: 0},
		{x: 0.774679, y: -1.45252, z: 0, c1x: 0, c1y: -0.077468, c1z: 0, c2x: 0.0138334, c2y: 0.099602, c2z: 0},
		{x: 0.705512, y: -1.8122, z: 0, c1x: -0.0138334, c1y: -0.099602, c1z: 0, c2x: 0.0166002, c2y: 0.099602, c2z: 0},
		{x: 0.691678, y: -1.95053, z: 0, c1x: -0.0166002, c1y: -0.099602, c1z: 0, c2x: 0.0138338, c2y: 0.077468, c2z: 0},
		{x: 0.636343, y: -2.19954, z: 0, c1x: -0.0138338, c1y: -0.077468, c1z: 0, c2x: 0.0166004, c2y: 0.066402, c2z: 0},
		{x: 0.737155, y: -2.41102, z: 0, c1x: -0.0166004, c1y: -0.066402, c1z: 0, c2x: 0.0138334, c2y: 0.030434, c2z: 0},
		{x: 0.567176, y: -2.35171, z: 0, c1x: -0.0138334, c1y: -0.030434, c1z: 0, c2x: 0.0110668, c2y: 0.055334, c2z: 0},
		{x: 0.553342, y: -2.55921, z: 0, c1x: -0.0110668, c1y: -0.055334, c1z: 0, c2x: 0.0083002, c2y: 0.055334, c2z: 0},
		{x: 0.525675, y: -2.62838, z: 0, c1x: -0.0083002, c1y: -0.055334, c1z: 0, c2x: 0.0166002, c2y: 0.055334, c2z: 0},
		{x: 0.470341, y: -2.83588, z: 0, c1x: -0.0166002, c1y: -0.055334, c1z: 0, c2x: 0.0110668, c2y: 0.055334, c2z: 0},
		{x: 0.470341, y: -2.90505, z: 0, c1x: -0.0110668, c1y: -0.055334, c1z: 0, c2x: 0.0110668, c2y: 0.052568, c2z: 0},
		{x: 0.415007, y: -3.09872, z: 0, c1x: -0.0110668, c1y: -0.052568, c1z: 0, c2x: 0.0110668, c2y: 0.060868, c2z: 0},
		{x: 0.415007, y: -3.20939, z: 0, c1x: -0.0110668, c1y: -0.060868, c1z: 0, c2x: 0, c2y: 0.027666, c2z: 0},
		{x: 0.415007, y: -3.23705, z: 0, c1x: 0, c1y: -0.027666, c1z: 0, c2x: 0, c2y: 0.030434, c2z: 0},
		{x: 0.415007, y: -3.36156, z: 0, c1x: 0, c1y: -0.030434, c1z: 0, c2x: -0.0055334, c2y: 0.041502, c2z: 0},
		{x: 0.442674, y: -3.44456, z: 0, c1x: 0.0055334, c1y: -0.041502, c1z: 0, c2x: -0.0138336, c2y: 0.027666, c2z: 0},
		{x: 0.484175, y: -3.49989, z: 0, c1x: 0.0138336, c1y: -0.027666, c1z: 0, c2x: -0.0166002, c2y: 0.019366, c2z: 0},
		{x: 0.525675, y: -3.54139, z: 0, c1x: 0.0166002, c1y: -0.019366, c1z: 0, c2x: -0.0138334, c2y: 0.013834, c2z: 0},
		{x: 0.553342, y: -3.56906, z: 0, c1x: 0.0138334, c1y: -0.013834, c1z: 0, c2x: -0.019367, c2y: 0.019368, c2z: 0},
		{x: 0.62251, y: -3.63823, z: 0, c1x: 0.019367, c1y: -0.019368, c1z: 0, c2x: -0.0249006, c2y: 0.013834, c2z: 0},
		{x: 0.677845, y: -3.63823, z: 0, c1x: 0.0249006, c1y: -0.013834, c1z: 0, c2x: -0.0332006, c2y: 0, c2z: 0},
		{x: 0.788513, y: -3.63823, z: 0, c1x: 0.0332006, c1y: 0, c1z: 0, c2x: -0.0359672, c2y: 0, c2z: 0},
		{x: 0.857681, y: -3.63823, z: 0, c1x: 0.0359672, c1y: 0, c1z: 0, c2x: -0.0636334, c2y: -0.024902, c2z: 0},
		{x: 1.10668, y: -3.51372, z: 0, c1x: 0.0636334, c1y: 0.024902, c1z: 0, c2x: -0.0691678, c2y: -0.033202, c2z: 0},
		{x: 1.20352, y: -3.47222, z: 0, c1x: 0.0691678, c1y: 0.033202, c1z: 0, c2x: -0.030434, c2y: -0.019366, c2z: 0},
		{x: 1.25885, y: -3.41689, z: 0, c1x: 0.030434, c1y: 0.019366, c1z: 0, c2x: -0.066402, c2y: -0.0498, c2z: 0},
		{x: 0.124859, y: -3.1522, z: 0, c1x: 0.066402, c1y: 0.0498, c1z: 0, c2x: -0.071936, c2y: -0.047034, c2z: 0},
		{x: 1.61853, y: -3.18172, z: 0, c1x: 0.071936, c1y: 0.047034, c1z: 0, c2x: -0.063634, c2y: -0.060868, c2z: 0},
		{x: 1.8537, y: -2.91888, z: 0, c1x: 0.063634, c1y: 0.060868, c1z: 0, c2x: -0.0664, c2y: -0.071934, c2z: 0},
		{x: 1.95053, y: -2.82205, z: 0, c1x: 0.0664, c1y: 0.071934, c1z: 0, c2x: -0.063634, c2y: -0.083002, c2z: 0},
		{x: 2.17187, y: -2.50387, z: 0, c1x: 0.063634, c1y: 0.083002, c1z: 0, c2x: -0.055334, c2y: -0.091302, c2z: 0},
		{x: 2.2272, y: -2.36554, z: 0, c1x: 0.055334, c1y: 0.091302, c1z: 0, c2x: -0.022134, c2y: -0.0498, c2z: 0},
		{x: 2.28254, y: -2.25487, z: 0, c1x: 0.022134, c1y: 0.0498, c1z: 0, c2x: -0.035968, c2y: -0.096836, c2z: 0},
		{x: 2.40704, y: -1.88136, z: 0, c1x: 0.035968, c1y: 0.096836, c1z: 0, c2x: -0.027666, c2y: -0.105134, c2z: 0},
		{x: 2.42087, y: -1.7292, z: 0, c1x: 0.027666, c1y: 0.105134, c1z: 0, c2x: -0.011066, c2y: -0.094068, c2z: 0},
		{x: 2.46237, y: -1.41102, z: 0, c1x: 0.011066, c1y: 0.094068, c1z: 0, c2x: -0.011068, c2y: -0.074702, c2z: 0},
		{x: 2.47621, y: -1.35569, z: 0, c1x: 0.011068, c1y: 0.074702, c1z: 0, c2x: -0.024902, c2y: -0.063634, c2z: 0},
		{x: 2.58688, y: -1.09285, z: 0, c1x: 0.024902, c1y: 0.063634, c1z: 0, c2x: -0.0332, c2y: -0.0719348, c2z: 0},
		{x: 2.64221, y: -0.996016, z: 0, c1x: 0.0332, c1y: 0.0719348, c1z: 0, c2x: -0.019366, c2y: -0.0387338, c2z: 0},
		{x: 2.68371, y: -0.899181, z: 0, c1x: 0.019366, c1y: 0.0387338, c1z: 0, c2x: -0.030434, c2y: -0.0691678, c2z: 0},
		{x: 2.79438, y: -0.650177, z: 0, c1x: 0.030434, c1y: 0.0691678, c1z: 0, c2x: -0.0332, c2y: -0.066401, c2z: 0},
		{x: 2.84971, y: -0.567176, z: 0, c1x: 0.0332, c1y: 0.066401, c1z: 0, c2x: -0.060868, c2y: -0.0968348, c2z: 0},
		{x: 3.09872, y: -0.166003, z: 0, c1x: 0.060868, c1y: 0.0968348, c1z: 0, c2x: -0.055334, c2y: -0.0996017, c2z: 0},
		{x: 3.12638, y: -0.0691677, z: 0, c1x: 0.055334, c1y: 0.0996017, c1z: 0, c2x: -0.022134, c2y: -0.038734, c2z: 0},
		{x: 3.20939, y: 0.0276672, z: 0, c1x: 0.022134, c1y: 0.038734, c1z: 0, c2x: -0.047036, c2y: -0.0608677, c2z: 0},
		{x: 3.36156, y: 0.235171, z: 0, c1x: 0.047036, c1y: 0.0608677, c1z: 0, c2x: -0.038734, c2y: -0.0608678, c2z: 0},
		{x: 3.40306, y: 0.332006, z: 0, c1x: 0.038734, c1y: 0.0608678, c1z: 0, c2x: -0.027666, c2y: -0.0415006, c2z: 0},
		{x: 3.49989, y: 0.442674, z: 0, c1x: 0.027666, c1y: 0.0415006, c1z: 0, c2x: -0.027666, c2y: -0.027667, c2z: 0},
		{x: 3.54139, y: 0.470341, z: 0, c1x: 0.027666, c1y: 0.027667, c1z: 0, c2x: -0.019368, c2y: -0.0332006, c2z: 0},
		{x: 3.59673, y: 0.608677, z: 0, c1x: 0.019368, c1y: 0.0332006, c1z: 0, c2x: -0.0249, c2y: -0.0359672, c2z: 0},
		{x: 3.66589, y: 0.650177, z: 0, c1x: 0.0249, c1y: 0.0359672, c1z: 0, c2x: -0.022132, c2y: -0.0166002, c2z: 0},
		{x: 3.70739, y: 0.691678, z: 0, c1x: 0.022132, c1y: 0.0166002, c1z: 0, c2x: -0.027668, c2y: -0.0276672, c2z: 0},
		{x: 3.80423, y: 0.788513, z: 0, c1x: 0.027668, c1y: 0.0276672, c1z: 0, c2x: -0.030434, c2y: -0.0276672, c2z: 0},
		{x: 3.85956, y: 0.830014, z: 0, c1x: 0.030434, c1y: 0.0276672, c1z: 0, c2x: -0.030434, c2y: -0.027667, c2z: 0},
		{x: 3.9564, y: 0.926848, z: 0, c1x: 0.030434, c1y: 0.027667, c1z: 0, c2x: -0.035968, c2y: -0.0304338, c2z: 0},
		{x: 4.0394, y: 0.982183, z: 0, c1x: 0.035968, c1y: 0.0304338, c1z: 0, c2x: -0.027666, c2y: -0.0193664, c2z: 0},
		{x: 4.09473, y: 1.02368, z: 0, c1x: 0.027666, c1y: 0.0193664, c1z: 0, c2x: -0.019366, c2y: -0.0138334, c2z: 0},
		{x: 4.13623, y: 1.05135, z: 0, c1x: 0.019366, c1y: 0.0138334, c1z: 0, c2x: -0.00830002, c2y: -0.00829999, c2z: 0},
		{x: 4.13623, y: 1.06518, z: 0, c1x: 0.00830002, c1y: 0.00829999, c1z: 0, c2x: -0.00276804, c2y: -0.00553401, c2z: 0},
		{x: 4.15007, y: 1.07902, z: 0, c1x: 0.00276804, c1y: 0.00553401, c1z: 0, c2x: -0.024902, c2y: -0.00276802, c2z: 0},
		{x: 4.26074, y: 1.07902, z: 0, c1x: 0.024902, c1y: 0.00276802, c1z: 0, c2x: -0.0221339, c2y: 0, c2z: 0},
	];
	for(var i = 0 ; i < points1.length ; i++)
		run("setpointat current " + i + " " + points1[i].x + " " + points1[i].y + " " + points1[i].z + " " + points1[i].c1x + " " + points1[i].c1y + " " + points1[i].c1z + " " + points1[i].c2x + " " + points1[i].c2y + " " + points1[i].c2z);
	run("add cursor 2");
	run("setcurve current lastCurve");
	run("setpos current -1.66197 0.687228 0");
	run("settime current 0");
	run("settimepercent current 0");
	run("setmessage current 20, osc://ip_out:port_out/test cursor_xPos cursor_yPos cursor_angle ,");
	run("setpattern current 0 0 1 -1");




//GUI: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method stores all the operations made by other softwares through one of the IanniX interfaces.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you or a third party software added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughInterfaces() {
//INTERFACES: NEVER EVER REMOVE THIS LINE

//INTERFACES: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method is called last.
 *	It allows you to modify your hand-drawn score (made through graphical user interface).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function alterateWithScript() {
	
}


/*
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 *	Made with IanniX 0.9.17
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 */



/*
    This file is part of IanniX, a graphical real-time open-source sequencer for digital art
    Copyright (C) 2010-2015 — IanniX Association

    Project Manager: Thierry Coduys (http://www.le-hub.org)
    Development:     Guillaume Jacquemin (http://www.buzzinglight.com)

    This file was written by Guillaume Jacquemin.

    IanniX is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

