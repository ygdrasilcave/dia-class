import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress iannix;
NetAddress sierra1;

float x;
float y;
float rotVal;

void setup() {
  size(500, 500);
  frameRate(25);
  oscP5 = new OscP5(this, 9000);
  iannix = new NetAddress("127.0.0.1", 8000);
  sierra1 = new NetAddress("172.26.33.80", 9000);
  x = 0;
  y = 0;
}

void draw() {
  background(0);
  fill(255);
  pushMatrix();
  translate(x, y); 
  rotate(radians(rotVal));
  rect(-50, -15, 100, 30);
  ellipse(-70, 0, 30, 30);
  popMatrix();
}

/*
void mousePressed() {
 OscMessage myMessage = new OscMessage("/test");
 myMessage.add(123); //add an int to the osc message
 myMessage.add(12.34); //add a float to the osc message
 myMessage.add("some text"); //add a string to the osc message
 oscP5.send(myMessage, iannix);
 }
 */

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/test")==true) {
    //if (theOscMessage.checkTypetag("ifs")) {
    if (theOscMessage.checkTypetag("fff")) {
      //int firstValue = theOscMessage.get(0).intValue();  
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      float value0 = theOscMessage.get(0).floatValue();
      float value1 = theOscMessage.get(1).floatValue();
      float value2 = theOscMessage.get(2).floatValue();
      x = map(value0, -5, 5, 0, width);
      y = map(value1, -5, 5, height, 0);
      rotVal = value2*-1.0;
      //println(value2);
      //OscMessage myMessage = new OscMessage("/test");
      //myMessage.add(value0);
      //myMessage.add(value1);
      //myMessage.add(value2);
      //oscP5.send(myMessage, sierra1);
      return;
    }
  }
}