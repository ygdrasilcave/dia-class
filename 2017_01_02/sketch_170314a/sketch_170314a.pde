float positionX = 100;
float y = 35;
float colorG = 0;
float t = 0.0;

void setup() {
  size(600, 500);
  background(127, 255, 255);
}

void draw() {
  //background(127, colorG, 255);
  rect(100, 100, 100, 100);
  ellipse(positionX, y, 100, 100);
  
  colorG = abs(sin(t))*255;
  positionX = positionX + 2;
  y = y + 1;
  t = t + 0.06;
  
  //println(positionX/y);
  println(y);
}

void keyPressed(){
  if(key == 'a'){
    println("a");
    saveFrame("myWork_#####.jpg");
  }else{
    println("bang");
  }
}