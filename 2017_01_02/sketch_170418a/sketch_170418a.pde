PImage img_cat;
PImage img_flower;
int pixelSize = 1;
boolean toggle = true;

void setup() {
  size(700, 990);
  img_cat = loadImage("man.jpg");
  img_flower = loadImage("woman.jpg");
}

void draw() {
  background(0);
  noStroke();
  for (int y=0; y<img_cat.height; y+=pixelSize) {
    for (int x=0; x<img_cat.width; x+=pixelSize) {
      int index = x + y*img_cat.width;
      color c1 = img_cat.pixels[index];
      color c2 = img_flower.pixels[index];
      float br1 = brightness(c1);
      float br2 = brightness(c2);
      /*if ((x+y)%2 == 0) {
       fill(c1);
       } else {
       fill(c2);
       }*/
      if (toggle == true) {
        if (br1 > br2) {
          fill(c2);
        } else {
          fill(c1);
        }
      } else {
        if (br1 > br2) {
          fill(c1);
        } else {
          fill(c2);
        }
      }
      rect(x, y, pixelSize, pixelSize);
    }
  }
  //pixelSize = int(map(mouseX, 0, width, 0, 50)+2);
  println(pixelSize);
}

void keyPressed() {
  if (key == ' ') {
    toggle = !toggle;
  }
}