PImage img;
PImage img2;
float t = 0.0;
float t2 = 0.0;

void setup() {
  size(1000, 1000);
  background(0);
  img = loadImage("face.png");
  img2 = loadImage("male.png");
}

void draw() {
  background(0);

  pushMatrix();
  translate(width/2, height/3);
  scale(abs(cos(t)));
  rotate(sin(t));
  imageMaker(img, 0, 0, img.width/5, img.height/5, 45);
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/3*2);
  scale(1);
  rotate(t);
  imageMaker(img2, 0, 0, img.width/5, img.height/5, 45);
  popMatrix();

  println(add(5, 9));

  //t = t + 0.06;
  t += 0.06;
}

void imageMaker(PImage _image, float _x, float _y, float _w, float _h, int _angle) {
  for (int i=0; i<360; i=i+_angle) {
    pushMatrix();
    translate(_x, _y);
    rotate(radians(i));
    image(_image, -_w/2, 100-abs(sin(t2))*100, _w, _h);
    fill(255);
    ellipse(0, _h+10, 20, 20);
    popMatrix();
  }
  t2 += map(mouseX, 0, width, 0, 0.15);
}

float add(float a, float b) {
  return a+b;
}