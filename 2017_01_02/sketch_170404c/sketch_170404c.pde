float rotVal = 0.0;
float rad = 200.0;
float angle = 0.0;

void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);
  fill(255);
  ellipse(width/2, height/2, 50, 50);
  
  float x = 0.0;
  float y = 0.0;
  x = width/2 + cos(radians(angle)) * rad;
  y = height/2 + sin(radians(angle)) * rad;
  fill(0, 255, 255);
  ellipse(x, height/2, 30, 30);
  ellipse(width/2, y, 30, 30);
  fill(255, 255, 0);
  //ellipse(x, y, 30, 30);
  angle++;
  //fill(255, 0, 0);
  textAlign(CENTER, CENTER);
  textSize(32);
  int i = 10;
  text(str(i), x, y);
}