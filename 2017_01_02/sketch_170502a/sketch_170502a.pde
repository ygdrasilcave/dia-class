PImage img1, img2;
int maxSize = 25;
Brush[] brush;
int cols;
int rows;
boolean colorToggle = true;
boolean toggle = true;

void setup() {
  size(1200, 862);
  img1 = loadImage("flower2.jpg");
  img2 = loadImage("face.jpg");

  cols = int(width/maxSize);
  rows = int(height/maxSize);

  brush = new Brush[cols*rows];

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      int index = x*maxSize + (y*maxSize)*width;
      color c = img1.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float w = random(maxSize*0.2, maxSize);

      brush[x+y*cols] = new Brush(x*maxSize, y*maxSize, w, r, g, b);
    }
  }
}

void draw() {
  background(0);
  translate(maxSize/2, maxSize/2);
  for (int i=0; i<brush.length; i++) {
    brush[i].drawBrush();
  }

  if (second()%10 == 0) {
    if (toggle == true) {
      println("bang");
      colorToggle = !colorToggle;
      for (int y=0; y<rows; y++) {
        for (int x=0; x<cols; x++) {
          int index = x*maxSize + (y*maxSize)*width;

          color c;
          if (colorToggle == true) {
            c = img1.pixels[index];
          } else {
            c = img2.pixels[index];
          }
          float r = red(c);
          float g = green(c);
          float b = blue(c);
          brush[x + y*cols].changeColor(r, g, b);
        }
      }
      toggle = false;
    }
  }else{
    toggle = true;
  }
}

void keyPressed() {
  if (key == ' ') {
    saveFrame("work_####.jpg");
  }
}