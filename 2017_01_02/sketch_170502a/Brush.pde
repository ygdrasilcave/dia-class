class Brush{
  
  float x, y;
  float r, g, b;
  float w, h;
  int num;
  int dir = 1;
  float offset;
  
  Brush(float _x, float _y, float _w, float _r, float _g, float _b){
    x = _x;
    y = _y;
    w = _w;
    h = w*random(0.1, 0.6);
    r = _r;
    g = _g;
    b = _b;
    num = int(random(2, 12));
    offset = random(0, w*0.2);
  }
  
  void drawBrush(){
    fill(r, g, b);
    noStroke();
    //rect(x, y, w, h);
    
    for(int i=0; i<num; i++){
      pushMatrix();
      translate(x, y);
      rotate(TWO_PI/num * i);
      ellipse(w/2/2 + offset, 0, w/2, h/2);
      popMatrix();
    }
    
    num += dir*1;
    if(num > 16){
      dir *= -1;
    }
    if(num < 2){
      dir *= -1;
    }
  }
  
  void changeColor(float _r, float _g, float _b){
    r = _r;
    g = _g;
    b = _b;
  }
};