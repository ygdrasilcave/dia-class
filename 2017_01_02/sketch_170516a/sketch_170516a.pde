int scene = 0;

void setup() {
  size(600, 600);
}

void draw() {
  background(0);
  println("current scene: " + scene);
  if (scene == 0) {
    scene0();
  } else if (scene == 1) {
    scene1();
  } else if (scene == 2) {
    scene2();
  } else if (scene == 3) {
    scene3();
  } else if (scene == 4) {
    scene4();
  }
}

void scene0() {
  fill(255, 0, 0);
  noStroke();
  rect(width/2-50, height/2-50, 100, 100);
}

void scene1() {
  fill(0, 255, 0);
  noStroke();
  ellipse(width/2, height/2, 100, 100);
}

void scene2() {
  fill(0, 0, 255);
  noStroke();
  pushMatrix();
  translate(width/2, height/2);
  rotate(PI/4);
  rect(-50, -50, 100, 100);
  popMatrix();
}

void scene3() {
  noFill();
  stroke(255);
  strokeWeight(3);
  ellipse(width/2, height/2, 100, 100);
}

void scene4() {
  noFill();
  stroke(255);
  strokeWeight(3);
  rect(width/2-50, height/2-50, 100, 100);
}

void keyPressed() {
  if (key == 'a') {
    scene = 0;
  } else if (key == 'b') {
    scene = 1;
  } else if (key == 'c') {
    scene = 2;
  } else if (key == 'd') {
    scene = 3;
  } else if (key == 'e') {
    scene = 4;
  }
}