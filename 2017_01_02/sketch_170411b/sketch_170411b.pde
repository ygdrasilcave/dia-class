PImage img;

void setup(){
  size(600, 431);
  img = loadImage("flower2.jpg");
  background(0);
}

void draw(){
  int x = int(random(width));
  int y = int(random(height));
  
  color c = img.pixels[x + y*img.width];
  
  fill(c);
  noStroke();
  //rect(mouseX, mouseY, 10, 10);
  rect(x, y, 10, 10);
}