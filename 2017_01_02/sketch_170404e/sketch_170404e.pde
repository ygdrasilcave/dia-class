float rad = 350.0;

void setup() {
  size(800, 800);
  background(0);
  textAlign(CENTER, CENTER);
}

void draw() {
  background(0);
  fill(255);
  noStroke();
  ellipse(width/2, height/2, 20, 20);
  for (int i=0; i<12; i+=1) {
    ellipse(width/2+cos(radians(i*30))*rad, height/2+sin(radians(i*30))*rad, 20, 20);
    //textAlign(CENTER, CENTER);
    //textSize(32);
    //text(str(i+1), width/2+cos(radians(i*30-60))*rad, height/2+sin(radians(i*30-60))*rad);
  }
  
  noFill();
  stroke(255);
  strokeWeight(1);
  float secondX = width/2+cos(radians(second()*6-90))*(rad-30);
  float secondY = height/2+sin(radians(second()*6-90))*(rad-30);
  //line(width/2, height/2, secondX, secondY);
  fill(255);
  noStroke();
  ellipse(secondX, secondY, 30, 30);
  fill(0);
  textSize(20);
  text(second(), secondX, secondY);
  
  stroke(255);
  strokeWeight(5);
  float minuteX = width/2+cos(radians(minute()*6-90))*(rad-150);
  float minuteY = height/2+sin(radians(minute()*6-90))*(rad-150);
  //line(width/2, height/2, minuteX, minuteY);
  fill(255);
  noStroke();
  ellipse(minuteX, minuteY, 60, 60);
  fill(0);
  textSize(32);
  text(minute(), minuteX, minuteY);
  
  stroke(255);
  strokeWeight(9);
  float offsetAngle = minute()*0.5;
  float hourX = width/2+cos(radians(hour()*30-90+offsetAngle))*(rad-200);
  float hourY = height/2+sin(radians(hour()*30-90+offsetAngle))*(rad-200);
  //line(width/2, height/2, hourX, hourY);
  fill(255);
  noStroke();
  ellipse(hourX, hourY, 100, 100);
  fill(0);
  textSize(42);
  text(hour(), hourX, hourY);
}