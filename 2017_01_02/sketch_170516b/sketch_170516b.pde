import beads.*;
import java.util.Arrays; 
AudioContext ac;

SamplePlayer sp0;
SamplePlayer sp1;
SamplePlayer sp2;
SamplePlayer sp3;
SamplePlayer sp4;

Glide rateValue0;
Glide rateValue1;
Glide rateValue2;
Glide rateValue3;
Glide rateValue4;

Gain g;

int scene = 0;

boolean bf = true;

void setup() {
  size(600, 600);
  ac = new AudioContext();
  String sourceFile = sketchPath("") + "13.wav";
  println(sourceFile);
  sp0 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sourceFile = sketchPath("") + "bits_piece_1.wav";
  sp1 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sourceFile = sketchPath("") + "bits_piece_3.wav";
  sp2 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sourceFile = sketchPath("") + "coin_2.wav";
  sp3 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sourceFile = sketchPath("") + "dmx.wav";
  sp4 = new SamplePlayer(ac, SampleManager.sample(sourceFile));
  sp0.setKillOnEnd(false);
  sp1.setKillOnEnd(false);
  sp2.setKillOnEnd(false);
  sp3.setKillOnEnd(false);
  sp4.setKillOnEnd(false);
  
  rateValue0 = new Glide(ac, 1, 1);
  rateValue1 = new Glide(ac, 1, 1);
  rateValue2 = new Glide(ac, 1, 1);
  rateValue3 = new Glide(ac, 1, 1);
  rateValue4 = new Glide(ac, 1, 0.5);
  
  sp0.setRate(rateValue0);
  sp1.setRate(rateValue1);
  sp2.setRate(rateValue2);
  sp3.setRate(rateValue3);
  sp4.setRate(rateValue4);
  
  g = new Gain(ac, 2, 0.1);
  g.addInput(sp0);
  g.addInput(sp1);
  g.addInput(sp2);
  g.addInput(sp3);
  g.addInput(sp4);
  ac.out.addInput(g);
  
  sp0.pause(true);
  sp1.pause(true);
  sp2.pause(true);
  sp3.pause(true);
  sp4.pause(true);
  
  ac.start();
}

void draw() {
  background(0);
  //println("current scene: " + scene);
  if (scene == 0) {
    scene0();
  } else if (scene == 1) {
    scene1();
  } else if (scene == 2) {
    scene2();
  } else if (scene == 3) {
    scene3();
  } else if (scene == 4) {
    scene4();
    if(bf == true){
      rateValue4.setValue(-map(mouseX, 0, width, 0.1, 3.0));
    }else{
      rateValue4.setValue(map(mouseX, 0, width, 0.1, 3.0));
    }
  }
}

void scene0() {
  fill(255, 0, 0);
  noStroke();
  rect(width/2-50, height/2-50, 100, 100);
}

void scene1() {
  fill(0, 255, 0);
  noStroke();
  ellipse(width/2, height/2, 100, 100);
}

void scene2() {
  fill(0, 0, 255);
  noStroke();
  pushMatrix();
  translate(width/2, height/2);
  rotate(PI/4);
  rect(-50, -50, 100, 100);
  popMatrix();
}

void scene3() {
  noFill();
  stroke(255);
  strokeWeight(3);
  ellipse(width/2, height/2, 100, 100);
}

void scene4() {
  noFill();
  stroke(255);
  strokeWeight(3);
  rect(width/2-50, height/2-50, 100, 100);
}

void keyPressed() {
  if (key == 'a') {
    scene = 0;
    sp0.setToLoopStart();
    rateValue0.setValue(1.0);
    sp0.start();
    sp1.pause(true);
    sp2.pause(true);
    sp3.pause(true);
    sp4.pause(true);
  } else if (key == 'b') {
    scene = 1;
    sp1.setToLoopStart();
    rateValue1.setValue(1.0);
    sp1.start();
    sp0.pause(true);
    sp2.pause(true);
    sp3.pause(true);
    sp4.pause(true);
  } else if (key == 'c') {
    scene = 2;
    sp2.setToLoopStart();
    rateValue2.setValue(1.0);
    sp2.start();
    sp0.pause(true);
    sp1.pause(true);
    sp3.pause(true);
    sp4.pause(true);
  } else if (key == 'd') {
    scene = 3;
    sp3.setToLoopStart();
    rateValue3.setValue(1.0);
    sp3.start();
    sp0.pause(true);
    sp1.pause(true);
    sp2.pause(true);
    sp4.pause(true);
  } else if (key == 'e') {
    bf = !bf;
    scene = 4;
    if (bf == true) {
      sp4.setToEnd();
      rateValue4.setValue(-2.0);
    } else {
      sp4.setToLoopStart();
      rateValue4.setValue(2.0);
    }
    sp4.start();
    sp0.pause(true);
    sp1.pause(true);
    sp2.pause(true);
    sp3.pause(true);
  }
}