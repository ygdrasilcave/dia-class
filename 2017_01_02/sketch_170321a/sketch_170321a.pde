float x = 0.0;
float y = 0.0;
float speedX = 3.0;
float speedY = 5.0;
float dirX = 1.0;
float dirY = 1.0;

void setup() {
  size(800, 600);
  background(0);
  //println(speedY);
}

void draw() {
  //background(0);
  //fill(0, 0, 0, 5);
  //rect(0, 0, width, height);

  noStroke();
  fill(random(255), random(255), random(255));
  //noFill();
  //stroke(255, 255, 0);
  float w = 10;
  //println(w);
  ellipse(x, y, w, w);

  x = x + dirX*speedX;
  y = y + dirY*speedY;
  //println(x);

  if (x >= width || x <= 0) {
    dirX = dirX*(-1);
  }
  if (y >= height || y <= 0){
    dirY = dirY*(-1);
  }
  
  speedX = mouseX/80.0;
  speedY = mouseY/60.0;
}