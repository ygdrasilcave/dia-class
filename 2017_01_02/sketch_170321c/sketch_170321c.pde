float dia = 0.0;
float speed = 12.0;
float dir = 1.0;

int change = 0;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  if (change == 0) {
    fill(255);
    ellipse(width/2, height/2, 200, dia);
    speed = 12;
  } else if (change == 1) {
    fill(128);
    rect(width/2-100, height/2-dia/2, 200, dia);
    speed = 1;
  } else if (change == 2) {
    fill(255, 0, 0);
    ellipse(width/2, height/2, 200, dia);
    speed = 3;
  } else if (change == 3) {
    fill(0, 255, 0);
    rect(width/2-100, height/2-dia/2, 200, dia);
    speed = 8;
  } else if (change == 4) {
    fill(0, 0, 255);
    ellipse(width/2, height/2, 200, dia);
    speed = 15;
  } else if (change == 5) {
    fill(255, 255, 0);
    rect(width/2-100, height/2-dia/2, 200, dia);
    speed = 1;
  }

  dia = dia + dir*speed;

  if (dia >= 150 || dia <= 0.0) {
    dir = dir*-1;
  }
}

void keyPressed() {
  if (key == 'c') {
    change = change + 1;
    if (change >= 6) {
      change = 0;
    }
  }
}