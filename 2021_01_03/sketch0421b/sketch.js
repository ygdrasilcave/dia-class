let assets_folder = '../assets/playSource/';

let ani_floating;
let ani_moving;
let ani_spinning;
let sprite_ghost;

function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
  ani_moving = loadAnimation(assets_folder+'ghost_walk0001.png', assets_folder+'ghost_walk0004.png');
  ani_spinning = loadAnimation(assets_folder+'ghost_spin0001.png', assets_folder+'ghost_spin0003.png');
}

function setup(){
  createCanvas(800, 800);
  sprite_ghost = createSprite(width/2, height/2, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);
  sprite_ghost.addAnimation('moving', ani_moving);
  sprite_ghost.addAnimation('spinning', ani_spinning);
}

function draw(){
  background(255, 255, 0);

  if(mouseX < sprite_ghost.position.x-10){
    sprite_ghost.changeAnimation('moving');
    sprite_ghost.mirrorX(-1);
    sprite_ghost.velocity.x = -2;
  }
  else if(mouseX > sprite_ghost.position.x+10){
    sprite_ghost.changeAnimation('moving');
    sprite_ghost.mirrorX(1);
    sprite_ghost.velocity.x = 2;
  }
  else{
    sprite_ghost.changeAnimation('floating');
    sprite_ghost.velocity.x = 0;
  }

  if(mouseIsPressed == true){
    sprite_ghost.changeAnimation('spinning');
    sprite_ghost.rotation = sprite_ghost.rotation - 5;
  }else{
    sprite_ghost.rotation = 0;
  }

  if(keyIsDown(UP_ARROW)){
    sprite_ghost.scale = sprite_ghost.scale + 0.05;
    if(sprite_ghost.scale > 2.0){
      sprite_ghost.scale = 2.0;
    }
  }else if(keyIsDown(DOWN_ARROW)){
    sprite_ghost.scale = sprite_ghost.scale - 0.05;
    if(sprite_ghost.scale < 0.2){
      sprite_ghost.scale = 0.2;
    }
  }

  drawSprites();
}
