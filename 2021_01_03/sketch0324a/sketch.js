let w = 500;
let h = 500;

let rectW = 60;
let rectH = 60;

function setup() {
  // put setup code here
  createCanvas(900,900);
}

function draw() {
  // put drawing code here
  background(0);

  //console.log("start");
  //for(let i=0; i<10; i++){
  //  console.log(i);
  //}
  //console.log("done");

  let numEllipse = int(map(mouseY, 0, height, 2, 100));
  w = map(mouseX, 0, width, 10, 600);
  h = w;
  noFill();
  stroke(255, 100);
  for(let i=0; i<numEllipse; i++){    
    ellipse(w/2 + ((width-w)/(numEllipse-1))*i, height/2, w, h);
  }
  for(let i=0; i<numEllipse; i++){    
    ellipse(width/2, h/2 + ((height-h)/(numEllipse-1))*i, w, h);
  }
  for(let i=0; i<numEllipse; i++){    
    ellipse(w/2 + ((width-w)/(numEllipse-1))*i, h/2 + ((height-h)/(numEllipse-1))*i, w, h);
  }
  for(let i=0; i<numEllipse; i++){    
    ellipse(w/2 + ((width-w)/(numEllipse-1))*i, height - (h/2 + ((height-h)/(numEllipse-1))*i), w, h);
  }

  let numRect = int(map(mouseX, 0, width, 2, 100));
  for(let i=0; i<numRect; i++){
    rect(((width-rectW)/(numRect-1))*i, height/2+h/2, rectW, rectH);
  }

}