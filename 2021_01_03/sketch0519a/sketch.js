let words = ['안녕하세요', '디지털크래프트 수업입니다', '열심히 공부합시다'];

let myVoice = new p5.Speech();
let btn_list;
let sel_voice;
let toggle = true;

let currentVoice;
let currentWord;

function preload(){
}

function setup(){
  createCanvas(800, 800);

  btn_list = createButton('List Voices');
  btn_list.position(20 , 20);
  btn_list.mousePressed(returnVoiceList);

  sel_voice = createSelect();
  sel_voice.position(20, 80);
  for(let i=0; i<myVoice.voices.length; i++){
    sel_voice.option(myVoice.voices[i]['name']);
  }

  currentVoice = myVoice.voices[11]['name'];
  
  sel_voice.selected(currentVoice);
  sel_voice.changed(voiceSelectEvent);

  myVoice.setVoice(currentVoice);


  textAlign(CENTER, CENTER);
  textSize(42);

  //myVoice.onLoad = speechLoaded; // could do it this way
  //myVoice.onStart = speechStarted;
  //myVoice.onPause = speechPaused; // not working
  //myVoice.onResume = speechResumed; // not working
  //myVoice.onEnd = speechEnded;
}

function voiceSelectEvent(){
  let _item = sel_voice.value();
  currentVoice = _item;
  myVoice.setVoice(_item);
}

function returnVoiceList(){
  myVoice.listVoices();
  console.log(myVoice.voices.length);
}

function draw(){
  background(255, 255, 0);
  fill(0);
  noStroke();
  text(currentVoice, width/2, height/2);
  text(currentWord, width/2, height/2+100);
}

function keyPressed(){
  if(key == 's'){
    let index = int(random(words.length));
    currentWord = words[index];
    myVoice.speak(currentWord);
  }
}