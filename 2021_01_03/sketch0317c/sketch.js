let pointX = 150;
let pointY = 650;
let toggle = false;

function setup() {
  // put setup code here
  createCanvas(800,800);
}

function draw() {
  // put drawing code here
  background(250);

  strokeWeight(1);

  stroke(255, 0, 255);
  line(30, 500, 700, 100);

  stroke(255, 10, 0);
  noFill();
  strokeWeight(5);
  triangle(450, 180, 750, 300, 550, 500);

  strokeWeight(1);
  quad(400, 550, 600, 400, 780, 430, 630, 760);
  
  stroke(0, 200, 10);
  noFill();
  ellipse(200, 450, 300, 300);
  arc(100, 400, 450, 450, radians(265), radians(320));

  stroke(0);
  fill(255, 255, 0);
  rect(230, 550, 200, 100);

  stroke(0);
  fill(0, 25, 220, 100);
  beginShape();
  vertex(330, 700);
  vertex(600, 530);
  vertex(570, 680);
  vertex(700, 500);
  vertex(620, 795);
  endShape(CLOSE);

  stroke(0);
  strokeWeight(30);
  point(pointX, pointY);

  if(second()%2 == 0 && toggle == false){
    pointX = random(800);
    pointY = random(800);
    toggle = true;
  }else if(second()%2 == 1 && toggle == true){
    toggle = false;
  }

  stroke(0, 0, 0);
  strokeWeight(1);
  noFill();
  bezier(85, 20, 10, 10, 90, 90, 15, 80);
}