let assets_folder = '../assets/playSource/';

let ani_box, ani_asterisk1, ani_asterisk2, ani_treasure;
let sp_asterisk;
let group_box, group_treasure;

let debug_mode = false;

function preload(){
  ani_box = loadAnimation(assets_folder+'box0001.png', assets_folder+'box0003.png');
  ani_asterisk1 = loadAnimation(assets_folder+'asterisk_normal0001.png', assets_folder+'asterisk_normal0003.png');
  ani_asterisk2 = loadAnimation(assets_folder+'asterisk_stretching0001.png', assets_folder+'asterisk_stretching0008.png');
  ani_treasure = loadAnimation(assets_folder+'small_circle0001.png');
}

function setup(){
  createCanvas(800, 800);

  sp_asterisk = createSprite(random(width), random(height));
  sp_asterisk.addAnimation('normal', ani_asterisk1);
  sp_asterisk.addAnimation('eating', ani_asterisk2);

  group_box = new Group();
  group_treasure = new Group();

  for(let i=0; i<4; i++){
    let sp_box = createSprite(random(width), random(height));
    sp_box.addAnimation('normal', ani_box);
    group_box.add(sp_box);
  }

  for(let i=0; i<10; i++){
    let sp_treasure = createSprite(random(width), random(height));
    sp_treasure.addAnimation('noraml', ani_treasure);
    group_treasure.add(sp_treasure);
  }
  
}

//collide, overlap, displace

function draw(){
  background(255, 255, 0);

  sp_asterisk.velocity.x = (mouseX - sp_asterisk.position.x)*0.1;
  sp_asterisk.velocity.y = (mouseY - sp_asterisk.position.y)*0.1;

  sp_asterisk.collide(group_box);

  /*for(let i=0; i<group_treasure.length; i++){
    let _temp = group_treasure[i];
    if(sp_asterisk.overlap(_temp)==true){
      sp_asterisk.changeAnimation('eating');
      sp_asterisk.animation.changeFrame(0);
      _temp.remove();
    }else{
      sp_asterisk.changeAnimation('normal');
    }
  }*/
  //console.log(group_treasure.length);


  //I can define a function to be called upon collision, overlap, displace or bounce
  //see get_treasure() below
  sp_asterisk.overlap(group_treasure, get_treasure);
  if(sp_asterisk.getAnimationLabel()=='eating' && sp_asterisk.animation.getFrame()==sp_asterisk.animation.getLastFrame()){
    sp_asterisk.changeAnimation('normal');
  }

  boundDebug(debug_mode);

  drawSprites();

}

function get_treasure(_collector, _collected){
  _collector.changeAnimation('eating');
  _collector.animation.changeFrame(0);
  _collected.remove();
}

function boundDebug(_b){
  sp_asterisk.debug = _b;
  for(let i=0; i<group_treasure.length; i++){
    let _temp = group_treasure[i];
    _temp.debug = _b;
  }
  for(let i=0; i<group_box.length; i++){
    let _temp = group_box[i];
    _temp.debug = _b;
  }
  
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debug_mode = !debug_mode;
  }
}
