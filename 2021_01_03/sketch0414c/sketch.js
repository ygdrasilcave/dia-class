let assets_folder = '../assets/playSource/';

let explode;
let lastFrame;
let txt = "FORWARD";

let x, y;
let dirX, dirY;
let explode_w;
let explode_h;

function preload(){
  explode = loadAnimation(assets_folder+'asterisk_explode0001.png', assets_folder+'asterisk_explode0011.png');
}

function setup(){
  createCanvas(1000, 750);
  background(255, 255, 0);
  lastFrame = explode.getLastFrame();
  console.log(lastFrame);
  textAlign(CENTER, CENTER);
  textSize(30);

  explode.frameDelay = 30;
  x = width/2;
  y = height/2;
  dirX = 1;
  dirY = 1;

  explode_w = explode.getWidth();
  explode_h = explode.getHeight();
}

function draw(){
  background(255, 255, 0);

  if(explode.getFrame() == 0){
    explode.goToFrame(lastFrame);
    txt = "FORWARD";
    //console.log(txt);
  }
  if(explode.getFrame() == lastFrame){
    explode.goToFrame(0);
    txt = "BACKWARD";
    //console.log(txt);
  }

  animation(explode, x, y);
  text(txt, x, y+explode_h/2+15);
  text(explode.getFrame(), x, y+explode_h/2+45);

  explode.frameDelay = int(map(mouseX, 0, width, 4, 30));

  x += dirX*random(8);
  y += dirY*random(10);

  if(x > width-(explode_w/2)){
    x = width-(explode_w/2);
    dirX *= -1;
  }
  if(x < explode_w/2){
    x = explode_w/2;
    dirX *= -1;
  }
  if(y > height - (explode_h/2)){
    y = height - (explode_h/2);
    dirY *= -1;
  }
  if(y < explode_h/2 ){
    y = explode_h/2;
    dirY *= -1;
  }
}