function setup() {
  // put setup code here
  createCanvas(800,400);
  //console.log("setup");

  let link1;
  let link2;
  link1 = createA('sketch0303a/', '2nd page', '_self');
  link2 = createA('https://www.yonsei.ac.kr/', 'YONSEI UNIV', '_blank');
  link1.position(100, 10);
  link2.position(100, 35);
}

//let i=0;

let myMessage = "My first web page !";

function draw() {
  // put drawing code here
  background(255, 0, 0);

  noStroke();

  fill(0);
  textSize(60);
  textAlign(CENTER, CENTER);
  text(myMessage, width/2, height/2);

  fill(0, 255, 0);
  ellipse(mouseX, mouseY, 60, 30);

  fill(0, 0, 255);
  rect(700, 300, 50, 50);

  //console.log("draw " + i);
  //i = i+1;
}

function mousePressed(){
	if(mouseX > 700 && mouseX < 750 && mouseY > 300 && mouseY < 350){
		window.open('https://portal.yonsei.ac.kr', '_blank');
	}
	//if(mouseX > 700){
	//	window.open('https://portal.yonsei.ac.kr', '_blank');
	//}
}