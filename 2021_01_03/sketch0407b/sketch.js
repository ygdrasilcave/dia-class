let assets_folder = '../assets/';
let image_URL = assets_folder + 'flower.jpg';
//let image_URL = 'https://upload.wikimedia.org/wikipedia/commons/b/b6/Image_created_with_a_mobile_phone.png';
let img_background;

let bee1, bee2, bee3;

let curr_time = 0;
let image_counter = 0;

let slider;

let snd_bee;


function preload(){
  img_background = loadImage(image_URL);
  bee1 = loadImage(assets_folder + 'bee_front.png');
  bee2 = loadImage(assets_folder + 'bee_front2.png');
  bee3 = loadImage(assets_folder + 'bee_front3.png');
  snd_bee = loadSound(assets_folder + 'bee_buzz.wav');
}

function setup(){
  createCanvas(1000, 750);
  background(0);
  console.log(image_URL);

  curr_time = millis();
  console.log(curr_time);

  slider = createSlider(50, 250, 100);
  slider.position(50, 50);
  slider.style('width', '300px');

  getAudioContext().suspend();
  snd_bee.loop();
  snd_bee.pause();
  console.log('sound file loaded');
}

function draw(){
  image(img_background, 0, 0, width, height);

  let frame_interval = slider.value();

  if(curr_time+frame_interval < millis()){
    image_counter++;
    if(image_counter > 2){
      image_counter = 0;
    }
    curr_time = millis();
    //console.log(image_counter);
  }

  if(image_counter == 0){
    image(bee1, mouseX-bee1.width/2, mouseY-bee1.height/2);
  }else if(image_counter == 1){
    image(bee2, mouseX-bee1.width/2, mouseY-bee1.height/2);
  }else if(image_counter == 2){
    image(bee3, mouseX-bee1.width/2, mouseY-bee1.height/2);
  }


}

function mousePressed(){
  if(getAudioContext().state != 'running'){
    userStartAudio();
  }

  if(snd_bee.isPlaying() == true){
    snd_bee.pause();
  }else{
    snd_bee.play();
  }
}
