let myVoice = new p5.Speech();

myVoice.onLoad = speechLoaded;
myVoice.onStart = speechStarted;
myVoice.onEnd = speechEnded;

var lyric = "The history of textbooks dates back to ancient civilizations. For example, Ancient Greeks wrote educational texts. The modern textbook has its roots in the mass production made possible by the printing press.";

let btn_speak;

function setup(){
  createCanvas(800, 800);

  btn_speak = createButton('Speak');
  btn_speak.position(150, 150);
  btn_speak.mousePressed(speakButtonPressed);

  myVoice.listVoices();
  myVoice.setVoice(1);

  console.log(btn_speak.elt.innerHTML);
}

function speakButtonPressed(){
  if(btn_speak.elt.innerHTML == 'Speak'){
    myVoice.speak(lyric);
  }else if(btn_speak.elt.innerHTML == 'Stop'){
    myVoice.stop();
  }
}

function speechLoaded(){
  console.log("voice loaded");
}

function speechStarted(){
  console.log("started");
  btn_speak.elt.innerHTML = 'Stop';
}

function speechEnded(){
  console.log("ended");
  btn_speak.elt.innerHTML = 'Speak';
}


function draw(){
  background(255, 255, 0);
}
