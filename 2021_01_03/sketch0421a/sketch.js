let assets_folder = '../assets/playSource/';

let ani_floating;
let sprite_floating;

function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
}

function setup(){
  createCanvas(800, 800);

  sprite_floating = createSprite(width/2 - 200, height/2, 72, 158);
  sprite_floating.addAnimation('floating', ani_floating);
}

function draw(){
  background(255, 255, 0);

  if(mouseIsPressed == true){
    sprite_floating.animation.stop();
  }else{
    sprite_floating.animation.play();
  }

  /*let frame = int(map(mouseX, 0, width, 0, sprite_floating.animation.getLastFrame()));

  sprite_floating.animation.changeFrame(frame);*/

  drawSprites();
}

function mousePressed(){
  let s1 = createSprite(width/2, height/2, 70, 100);
  s1.shapeColor = color(255, 0, 255);
  let s2 = createSprite(width/2 + 200, height/2, 100, 50);
}