var words = ["i", "heart", "p", "five", "안녕 하세요", "누구?"]; // some words
var iptr = 0; // a counter for the words

var myVoice = new p5.Speech(); // new P5.Speech object

var listbutton; // button

function setup()
{
  // graphics stuff:
  createCanvas(400, 400);
  background(255, 0, 0);
  fill(255, 255, 255, 100);
  // instructions:
  textSize(24);
  textAlign(CENTER);
  text("click me", width/2, height/2);
  // button:
  listbutton = createButton('List Voices');
  listbutton.position(180, 430);
  listbutton.mousePressed(doList);
}

function draw()
{
  // why draw when you can click?
  
}

function doList()
{
  myVoice.listVoices(); // debug printer for voice options
  console.log(myVoice.voices.length);
}

function keyPressed()
{
   // clear screen
}

function mousePressed()
{
  // if in bounds:
  if(mouseX<width && mouseY<height) {
    background(255, 0, 0);
    ellipse(mouseX, mouseY, 50, 50); // circle
    // randomize voice and speak word:
    //myVoice.setVoice(Math.floor(random(myVoice.voices.length)));
    //myVoice.setVoice(13);
    let index = Math.floor(random(myVoice.voices.length));
    myVoice.setVoice(index);
    console.log(myVoice.voices[index]['name']);
    //myVoice.setVoice(1);
    myVoice.speak(words[iptr]);
    iptr = (iptr+1) % words.length; // increment
    fill(255);
    text(trim(myVoice.voices[index]['name']), width/2, height/2);
  }
}