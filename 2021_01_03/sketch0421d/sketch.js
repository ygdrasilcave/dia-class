let assets_folder = '../assets/playSource/';

let ani_floating;
let ani_circle;
let sprite_ghost;
let sprite_circle;

let direction = 0;

function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
  ani_circle = loadAnimation(assets_folder+'asterisk_circle0006.png', assets_folder+'asterisk_circle0008.png');
}
function setup(){
  createCanvas(800, 800);
  sprite_ghost = createSprite(width/2, height/2, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);

  sprite_circle = createSprite(width/2, height/2, 50, 100);
  sprite_circle.addAnimation('circle', ani_circle);
}

function draw(){
  background(255, 255, 0);

  direction += 5;
  //sprite_circle.setSpeed(1, 0);
  //sprite_circle.setSpeed(1, 45);
  //sprite_circle.setSpeed(1, 90);
  //sprite_circle.setSpeed(1, 180);
  sprite_circle.setSpeed(6, direction);


  let targetX = mouseX;
  let targetY = mouseY;
  //attractionPoint(magnitude, pointX, pointY);
  sprite_ghost.attractionPoint(0.2, targetX, targetY);
  sprite_ghost.maxSpeed = 5;

  drawSprites();
}
