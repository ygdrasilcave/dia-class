let words;

let weekday = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
let timeText = "";
let timeText2 = "";

let myVoice = new p5.Speech();
let btn_list;
let sel_voice;
let toggle = true;

let currentVoice;
let currentWord;

function preload(){
  words = loadStrings('../assets/txt/test.txt');
}

function setup(){
  createCanvas(800, 800);

  btn_list = createButton('List Voices');
  btn_list.position(20 , 20);
  btn_list.mousePressed(returnVoiceList);

  sel_voice = createSelect();
  sel_voice.position(20, 80);
  for(let i=0; i<myVoice.voices.length; i++){
    sel_voice.option(myVoice.voices[i]['name']);
  }

  currentVoice = myVoice.voices[11]['name'];
  
  sel_voice.selected(currentVoice);
  sel_voice.changed(voiceSelectEvent);

  myVoice.setVoice(currentVoice);


  textAlign(CENTER, CENTER);
  textSize(42);

}

function voiceSelectEvent(){
  let _item = sel_voice.value();
  currentVoice = _item;
  myVoice.setVoice(_item);

  console.log(myVoice.voices[11])
}

function returnVoiceList(){
  myVoice.listVoices();
  console.log(myVoice.voices.length);
}

function draw(){
  background(255, 255, 0);
  fill(0);
  noStroke();
  textAlign(CENTER);
  text(timeText2, width/2, height/2-200);
  textAlign(CENTER);
  text(currentVoice, width/2, height/2);
  textAlign(CENTER);
  text(currentWord, width/2, height/2+100);
  

  if(second()%10 == 0 && toggle == true){
  //if(minute()%10 == 0 && toggle == true){
    //myVoice.speak("2021년 5월 19일은 휴일입니다. 그런데 수업을 하고 있어서 우울합니다.");
    timeText = "";
    timeText2 = "";

    let now = new Date();
    let wd = weekday[now.getDay()];

    timeText = timeText + year() + "년 " + month() + "월 " + day() + "일, " + wd +", " + hour() + "시, " + minute() + "분 " + second() + "초 입니다.";

    timeText2 = timeText2 + year() + "년 " + month() + "월 " + day() + "일(" + wd +")\n" + hour() + "시, " + minute() + "분 " + second() + "초 입니다.";

    myVoice.speak(timeText);
    console.log(timeText);
    toggle = false;

  }
  else if(second()%10 == 1 && toggle == false){
  //else if(minute()%10 == 1 && toggle == false){
    toggle =true;
  }

}

function keyPressed(){
  if(key == 's'){
    let index = int(random(words.length));
    currentWord = words[index];
    myVoice.speak(currentWord);
  }
}