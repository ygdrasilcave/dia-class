let degree = 0;

function setup(){
  createCanvas(1000, 800);
  background(0);
}

function draw(){
  
  push();
  translate(width/4, height/2);
  fill(180);
  rect(0, 0, 80, 40);
  rotate(PI/4.0);
  fill(255);
  rect(0, 0, 80, 40);
  pop();

  push();
  translate(width/2, height/2);
  degree = degree + 0.02;
  rotate(degree);
  fill(255, 0, 0);
  rect(0, 0, 40, 80);
  fill(255, 255, 0);
  ellipse(150, 0, 30, 30);
  pop();

  push();
  translate((width/4)*3, height/2);
  fill(0, 190, 120);
  rect(0, 0, 100, 20);
  rotate(-PI/4.0);
  fill(0, 255, 0);
  rect(0, 0, 100, 20);
  pop();
}
