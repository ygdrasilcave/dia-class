let assets_folder = '../assets/playSource/';

let ani_floating;
let ani_moving;
let ani_spinning;
let sprite_ghost;

function preload(){
  ani_floating = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');
  ani_moving = loadAnimation(assets_folder+'ghost_walk0001.png', assets_folder+'ghost_walk0004.png');
  ani_spinning = loadAnimation(assets_folder+'ghost_spin0001.png', assets_folder+'ghost_spin0003.png');
}

function setup(){
  createCanvas(800, 800);
  sprite_ghost = createSprite(width/2, height/2, 50, 100);
  sprite_ghost.addAnimation('floating', ani_floating);
  sprite_ghost.addAnimation('moving', ani_moving);
  sprite_ghost.addAnimation('spinning', ani_spinning);
}

function draw(){
  background(255, 255, 0);

  let posX = sprite_ghost.position.x;
  let posY = sprite_ghost.position.y;
  let dist = sqrt((mouseX-posX)*(mouseX-posX) + (mouseY-posY)*(mouseY-posY));

  if(dist > 30){
    sprite_ghost.changeAnimation('moving');
    if(mouseX-posX > 0){
      sprite_ghost.mirrorX(1);
    }else{
      sprite_ghost.mirrorX(-1);
    }
    sprite_ghost.velocity.x = (mouseX-posX)*0.02;
    sprite_ghost.velocity.y = (mouseY-posY)*0.02;
  }else{
    sprite_ghost.changeAnimation('floating');
    sprite_ghost.velocity.x = 0;
    sprite_ghost.velocity.y = 0;
  }

  if(mouseIsPressed == true){
    sprite_ghost.changeAnimation('spinning');
    sprite_ghost.rotation = sprite_ghost.rotation - 5;
  }else{
    sprite_ghost.rotation = 0;
  }

  sprite_ghost.scale = map(mouseY, 0, height, 0.6, 2.0);

  drawSprites();
}
