let assets_folder = '../assets/playSource/';

let ani_triangle, ani_cloud1, ani_cloud2, ani_platform;
let sp_triangle, sp_cloud, sp_platform;
let debug_mode = false;

let gravity = 1;

function preload(){
  ani_triangle = loadAnimation(assets_folder+'triangle.png');
  ani_cloud1 = loadAnimation(assets_folder+'cloud_breathing0001.png', assets_folder+'cloud_breathing0009.png');
  ani_cloud2 = loadAnimation(assets_folder+'asterisk_normal0001.png', assets_folder+'asterisk_normal0003.png');
  ani_platform = loadAnimation(assets_folder+'platform.png');
}

function setup(){
  createCanvas(800, 800);
  sp_triangle = createSprite(random(100, width-100), 0);
  sp_triangle.addAnimation('normal', ani_triangle);

  sp_cloud = createSprite(random(200, width-200), random(100, height/2));
  sp_cloud.addAnimation('normal', ani_cloud1);
  sp_cloud.addAnimation('transformed', ani_cloud2);
  sp_cloud.setCollider('circle', 0, 0, 50);

  sp_platform = createSprite(width/2, height-100);
  sp_platform.addAnimation('normal', ani_platform);
}

//collide, overlap, displace
//overlapPoint, overlapPixel

function draw(){
  background(255, 255, 0);

  sp_triangle.velocity.x = 0;

  if(keyIsDown(LEFT_ARROW)){
    sp_triangle.velocity.x = -5;
  }
  if(keyIsDown(RIGHT_ARROW)){
    sp_triangle.velocity.x = 5;
  }
  if(keyIsDown(UP_ARROW)){
    sp_triangle.velocity.y-=1.1;
  }

  if(sp_platform.overlapPixel(sp_triangle.position.x, sp_triangle.position.y+30)==false){
    sp_triangle.velocity.y += gravity;
  }

  while(sp_platform.overlapPixel(sp_triangle.position.x, sp_triangle.position.y+30)==true){
    sp_triangle.position.y--;
    sp_triangle.velocity.y = 0;
  }

  if(sp_cloud.overlapPoint(sp_triangle.position.x, sp_triangle.position.y)==true){
    sp_cloud.changeAnimation('transformed');
  }else{
    sp_cloud.changeAnimation('normal');
  }

  boundDebug(debug_mode);
  drawSprites();
}

function boundDebug(_b){
  sp_triangle.debug = _b;
  sp_cloud.debug = _b;
  sp_platform.debug = _b;
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debug_mode = !debug_mode;
  }
}
