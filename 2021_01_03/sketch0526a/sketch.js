let myRec = new p5.SpeechRec('ko-KR', resultEvent);
myRec.continuous = true; // do continuous recognition
myRec.initerimResults = true; // allow partial recognition

//function preload(){
//}

let x, y;
let sx, sy;
let dirX, dirY;
let dia;

function setup(){
  createCanvas(800, 800);

  myRec.start();

  //myRec.onResult = resultEvent;
  //myRec.onError = errorEvent;
  //myRec.onEnd = endEvent;

  x = width/2;
  y = height/2;
  sx = 0;
  sy = 0;
  dirX = 1;
  dirY = 1;
  dia = 20;
}

function resultEvent(){
  if(myRec.resultValue == true){
    string = myRec.resultString;
    let words = string.split(' ');
    if(words.length > 0){
      let lastWord = words[words.length-1];
      if(lastWord == '왼쪽'){
        dirX = -1;
        sx = 1;
        sy = 0;
      }else if(lastWord == '오른쪽'){
        dirX = 1;
        sx = 1;
        sy = 0;
      }else if(lastWord == '위로'){
        dirY = -1;
        sy = 1;
        sx = 0;
      }else if(lastWord == '아래로'){
        dirY = 1;
        sy = 1;
        sx = 0;
      }else if(lastWord == '멈춰'){
        sx = 0;
        sy = 0;
      }else if(lastWord == '빠르게'){
        sx = sx*2;
        sy = sy*2;
      }else if(lastWord == '느리게'){
        sx = sx*0.5;
        sy = sy*0.5;
      }else if(lastWord == '커져라'){
        dia = dia + 5;
      }
      console.log(lastWord);
    }
  }
}

function draw(){
  background(255, 255, 0);

  x = x + sx*dirX;
  y = y + sy*dirY;

  fill(0);
  noStroke();
  ellipse(x, y, dia, dia);

  if(x > width){
    x = 0;
  }
  if(x < 0){
    x = width;
  }
  if(y > height){
    y = 0;
  }
  if(y < 0){
    y = height;
  }
}
