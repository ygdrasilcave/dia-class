let angleF = 0;
let scaleF = 1;
let noiseF = 0.0;

let x1 = -500;
let x2 = 500;
let r1 = 0.0;
let r2 = 0.0;
let r1F = 0.0;
let r2F = 0.0;
let y1 = 0;

function setup(){
  createCanvas(800, 800);
  background(0);
  x1 = -500;
  x2 = width + 500;
}

function draw(){
  //background(0);
  fill(0, 15);
  noStroke();
  rect(0, 0, width, height);

  push();
  translate(mouseX, mouseY);
  rotate(angleF);
  scale(scaleF*0.2);
  fill(255);
  rect(-150, -150, 300, 300);
  pop();

  push();
  translate(mouseX, mouseY);
  rotate(-angleF);
  scale(scaleF*0.2);
  fill(0);
  rect(-70, -70, 140, 140);
  pop();

  angleF += radians(1);
  scaleF = noise(noiseF) + 0.5; //0.0-1.0
  noiseF = noiseF + 0.015; //0.005-0.03

  cloud(x1, y1, r1*0.13, 0.8, r1*15+15);
  cloud(x2, height/4, r2*0.098, 1.0, r2*15+15);

  x1 += 1;
  x2 -= 2;
  if(x1 > width + 500){
    x1 = -500;
  }
  if(x2 < -500){
    x2 = width + 500;
  }

  y1 = (height-250) + map(noise(scaleF), 0, 1, -100, 100);

  r1 = sin(r1F);
  r1F += 0.16;

  r2 = cos(r2F);
  r2F += 0.2;
}

function cloud(_x, _y, _r, _s, _e){
  push();

  translate(_x, _y);
  rotate(_r);
  scale(_s);

  fill(255);
  noStroke();
  ellipse(-100, 30, 115, 100);
  ellipse(0, 0, 200, 200);
  ellipse(100, 0, 130, 130);
  
  fill(0);
  ellipse(-20, 0, 15, _e);
  ellipse(20, 0, 15, _e);

  pop();
}