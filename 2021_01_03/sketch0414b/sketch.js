let assets_folder = '../assets/playSource/';

let sleep, explode, glitch, circle;

let explode_rewind = false;

function preload(){
  sleep = loadAnimation(assets_folder+'asterisk_stretching0001.png', assets_folder+'asterisk_stretching0008.png');
  sleep.playing = false;
  //sleep.looping = false;

  explode = loadAnimation(assets_folder+'asterisk_explode0001.png', assets_folder+'asterisk_explode0011.png');

  glitch = loadAnimation(assets_folder+'asterisk.png', assets_folder+'triangle.png', assets_folder+'square.png', assets_folder+'cloud.png', assets_folder+'star.png', assets_folder+'mess.png', assets_folder+'monster.png');
  glitch.playing = false;

  circle = loadAnimation(assets_folder+'asterisk_circle0000.png', assets_folder+'asterisk_circle0008.png');
  circle.looping = false;
}

function setup(){
  createCanvas(1000, 750);
  background(255, 255, 0);

  console.log(circle.getLastFrame());
}

function draw(){
  background(255, 255, 0);

  if(mouseIsPressed == true){
    sleep.play();
    circle.goToFrame(0);
  }else{
    sleep.stop();
    //circle.goToFrame(8);
    circle.goToFrame(circle.getLastFrame());
  }


  if(explode.getFrame() == explode.getLastFrame()){
    explode.changeFrame(7);
  }
  
  /*
  if(explode_rewind == true){
    explode.changeFrame(0);
    explode_rewind = false;
  }else{
    if(explode.getFrame() == explode.getLastFrame()){
      explode.changeFrame(7);
    }
  }
  */

  animation(sleep, width/5, height/2);

  animation(explode, 2*(width/5), height/2);

  animation(glitch, 3*(width/5), height/2);

  animation(circle, 4*(width/5), height/2);
}

function mousePressed(){
  //glitch.nextFrame();
  //glitch.previousFrame();
  let _frame = int(random(0, glitch.getLastFrame()+1));
  glitch.changeFrame(_frame);
  console.log(_frame);


  //explode.rewind();
  //explode_rewind = true;
  explode.changeFrame(0);
}