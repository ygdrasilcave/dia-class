//range (0~100)
var speed = 6;
var bigRadius;
var strokeW = 1;
//list
var shape = ['circle', 'triangle', 'square', 'pentagon', 'star'];
//text input
var label = 'text input';
//toggle button
var drawFill = true;
var drawStroke = true;
//color
var fillColor = '#b4ffff';
var strokeColor = [255, 0, 255];

let myGui, userGui;
let isVisible;

let positionX;

function setup(){
  createCanvas(800, 800);

  bigRadius = 150;
  positionX = width/2;

  myGui = createGui('parameters').setPosition(10, 10);

  //sliderRange(min, max, step);
  sliderRange(0, 20, 2);
  myGui.addGlobals('speed', 'strokeColor', 'drawFill', 'shape');

  sliderRange(0, 300, 0.5);
  myGui.addGlobals('bigRadius', 'label');

  myGui.hide();
  isVisible = false;

  userGui = createGui('change values').setPosition(width-220, 10);
  sliderRange(0, 10, 0.1);
  userGui.addGlobals('drawFill', 'fillColor', 'drawStroke', 'strokeW');

  textSize(36);
  textAlign(CENTER, CENTER);
}

function draw(){
  background(255, 255, 0);

  if(drawFill == true){
    fill(fillColor);
  }else{
    noFill();
  }
  strokeWeight(strokeW);
  if(drawStroke == true){
    stroke(strokeColor);
  }else{
    noStroke();
  }
  if(shape == 'circle'){
    ellipse(positionX, height/2, bigRadius, bigRadius);
  }else if(shape == 'triangle'){
    drawPoly(3, positionX, height/2, bigRadius/2);
  }else if(shape == 'square'){
    rect(positionX-bigRadius/2, height/2-bigRadius/2, bigRadius, bigRadius);
  }else if(shape == 'pentagon'){
    drawPoly(5, positionX, height/2, bigRadius/2);
  }else if(shape == 'star'){
    drawStar(5, positionX, height/2, bigRadius/2);
  }
  fill(0);
  noStroke();
  text(label, positionX, height/2);
  positionX += speed;
  if(positionX > width + bigRadius/2){
    positionX = -bigRadius/2;
  }
}

function drawPoly(numVertex, centerX, centerY, radius){
  beginShape();
  for(let i=0; i<numVertex; i++){
    let angle = ((TWO_PI/numVertex) * i) - HALF_PI;
    let vertexX = centerX + cos(angle)*radius;
    let vertexY = centerY + sin(angle)*radius;
    vertex(vertexX, vertexY);
  }
  endShape(CLOSE);
}

function drawStar(numVertex, centerX, centerY, radius){
  beginShape();
  for(let i=0; i<numVertex*2; i++){
    let angle = ((TWO_PI/(numVertex*2)) * i) - HALF_PI;
    let radius2 = 0;
    if(i%2==0){
      radius2 = radius;
    }else{
      radius2 = radius*0.45;
    }
    let vertexX = centerX + cos(angle)*radius2;
    let vertexY = centerY + sin(angle)*radius2;
    vertex(vertexX, vertexY);
  }
  endShape(CLOSE);
}

function keyPressed(){
  if(key == 'g'){
    if(isVisible == false){
      myGui.show();
      isVisible = true;
    }else{
      myGui.hide();
      isVisible = false;
    }
  }
}
