let assets_folder = '../assets/playSource/';

let ani_circle, ani_box;

let group_circle, group_box;

let debug_mode = false;

function preload(){
  ani_circle = loadAnimation(assets_folder+'asterisk_circle0006.png', assets_folder+'asterisk_circle0008.png');
  ani_box = loadAnimation(assets_folder+'box0001.png', assets_folder+'box0003.png');
}

function setup(){
  createCanvas(800, 800);

  group_circle = new Group();
  group_box = new Group();

  for(let i=0; i<20; i++){
    let sp_circle = createSprite(random(width), random(height));
    sp_circle.addAnimation('normal', ani_circle);
    sp_circle.setCollider('circle', -2, 2, 55);
    sp_circle.setSpeed(random(2,3), random(0,360));
    sp_circle.scale = random(0.5, 1);
    sp_circle.mass = sp_circle.scale;
    //restitution is the dispersion of energy at each bounce
    //if = 1 the circles will bounce forever
    //if < 1 the circles will slow down
    //if > 1 the circles will accelerate until they glitch
    sp_circle.restitution = 1;
    group_circle.add(sp_circle);
  }

  for(let i=0; i<4; i++){
    let sp_box = createSprite(random(width), random(height));
    sp_box.addAnimation('noraml', ani_box);
    //as if with infinite mass
    sp_box.immovable = true;
    if(i%2==0){
      sp_box.rotation = 90;
    }
    group_box.add(sp_box);
  }
  
}

//collide, overlap, displace

function draw(){
  background(255, 255, 0);

  //group_circle.bounce(group_circle);
  group_circle.bounce(group_box);

  for(let i=0; i<group_circle.length; i++){
    let _circle = group_circle[i];
    for(let j=0; j<group_circle.length; j++){
      let _other = group_circle[j];
      if(i != j){
        _circle.bounce(_other);
      }
    }
    if(_circle.position.x < 0){
      _circle.position.x = 1;
      _circle.velocity.x = abs(_circle.velocity.x);
    }
    if(_circle.position.x > width){
      _circle.position.x = width-1;
      _circle.velocity.x = -abs(_circle.velocity.x);
    }
    if(_circle.position.y < 0){
      _circle.position.y = 1;
      _circle.velocity.y = abs(_circle.velocity.y);
    }
    if(_circle.position.y > height){
      _circle.position.y = height-1;
      _circle.velocity.y = -abs(_circle.velocity.y);
    }
  }

  boundDebug(debug_mode);
  drawSprites();
}


function boundDebug(_b){
  for(let i=0; i<group_circle.length; i++){
    let _temp = group_circle[i];
    _temp.debug = _b;
  }
  for(let i=0; i<group_box.length; i++){
    let _temp = group_box[i];
    _temp.debug = _b;
  } 
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debug_mode = !debug_mode;
  }
}
