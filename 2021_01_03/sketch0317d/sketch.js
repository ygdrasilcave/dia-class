let w = 200;
let h = 200;

let rectX = 0;
let rectW = 60;

function setup() {
  // put setup code here
  createCanvas(800,800);
}

function draw() {
  // put drawing code here
  background(0);

  stroke(255, 0, 0);
  strokeWeight(3);
  line(0, 0, width, height);
  line(width, 0, 0, height);

  fill(255);
  stroke(255, 255, 0);
  ellipse(100, height/2, w, h);
  ellipse(width/2, height/2, w, h);
  ellipse(width-100, height/2, w, h);

  rectX = 0;
  fill(255);
  stroke(255, 0, 255);
  strokeWeight(2);
  rect(rectX, height/2 + 150, rectW, 50);
  rectX = rectX + rectW + 10;
  rect(rectX, height/2 + 150, rectW, 50);
  rectX = rectX + rectW + 10;
  rect(rectX, height/2 + 150, rectW, 50);
  rectX = rectX + rectW + 10;
  rect(rectX, height/2 + 150, rectW, 50);
  rectX = rectX + rectW + 10;
  rect(rectX, height/2 + 150, rectW, 50);
  rectX = rectX + rectW + 10;
  rect(rectX, height/2 + 150, rectW, 50);
  rectX = rectX + rectW + 10;
  rect(rectX, height/2 + 150, rectW, 50);

}