let degree = 0;

function setup(){
  createCanvas(1000, 800);
  background(0);
}

function draw(){
  
  translate(mouseX, mouseY);
  rotate(PI/4.0);
  fill(255);
  rect(0, 0, 80, 40);

  degree = degree + 0.02;
  rotate(degree);
  fill(255, 0, 0);
  rect(0, 0, 40, 80);

  rotate(-PI/4.0);
  fill(0, 255, 0);
  rect(0, 0, 100, 20);
}
