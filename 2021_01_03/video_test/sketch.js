let myVideo;
let myVideoThumb;

let playing = false;

function preload(){
	myVideo = createVideo(['../assets/IMG_0824_2.mp4'], onloadCallback);
	myVideo.onended(onendCallback);
	myVideoThumb = loadImage('../assets/logo.png');
}

function setup() {
  createCanvas(1920, 1080);
}

//https://p5js.org/reference/#/p5.MediaElement
function onloadCallback(){
  myVideo.noLoop();
  myVideo.hide();
  //myVideo.volume(0);
  //myVideo.showControls();
}
function onendCallback(){
	playing = false;
}

function draw() {
  	background(255);
  	if(playing == true){
  		image(myVideo, 0, 0, myVideo.width, myVideo.height);
	}else{
		image(myVideoThumb, 0, 0, myVideo.width, myVideo.height);
	}
}

function mouseClicked(){
	if(playing == false){
  		myVideo.play();
  		playing = true;
	}else{
		myVideo.stop();
		playing = false;
	}
}