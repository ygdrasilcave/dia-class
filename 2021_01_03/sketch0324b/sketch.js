let x = [];
let y = [];
let numVertex = 100;

function setup() {
  // put setup code here
  createCanvas(900, 900);
  resetRandomValue();
}

function draw() {
  // put drawing code here
  background(255);

  let numLine = int(map(mouseX, 0, width, 2, 200));
  noFill();
  stroke(0, 90);
  for(let i=0; i<numLine; i++){
    line(0, (height/numLine)*i, width, (height/numLine)*i);
  }
  for(let i=0; i<numLine; i++){
    line((width/numLine)*i, 0, (width/numLine)*i, height);
  }

  //fill(0, 15);
  //noStroke();
  //noFill();
  //stroke(0, 90);
  noStroke();
  fill(0, 35);
  beginShape();
  for(let i=0; i<numVertex; i++){
    //ellipse(x[i], y[i], 20, 20);
    vertex(x[i], y[i]);    
  }
  endShape(CLOSE);
  //noise()

}

function resetRandomValue(){
  for(let i=0; i<numVertex; i++){
    x[i] = random(-width/2, width+width/2);
    y[i] = random(-height/2, height+height/2);
  }
}

function mouseClicked(){
  resetRandomValue();
}