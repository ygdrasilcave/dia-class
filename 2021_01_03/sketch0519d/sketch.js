let myRec = new p5.SpeechRec('ko-KR');

let btn_start;

let string = "버튼을 글릭하고 말해보세요";
let string2 = "";

function preload(){
}

function setup(){
  createCanvas(800, 800);

  btn_start = createButton('start');
  btn_start.style("width", "100px");
  btn_start.position(width/2-50, height-100);
  btn_start.mousePressed(startEvent);

  myRec.onResult = resultEvent;
  myRec.onError = errorEvent;
  myRec.onEnd = endEvent;
}

function resultEvent(){
  if(myRec.resultValue == true){
    string = myRec.resultString;

    if(string == "수업을 끝낼까요"){
      console.log("아주 좋습니다.")
      string2 = "아주 좋습니다."
    }else{
      string2 = "";
    }
  }
}

function errorEvent(){
  string = "못알아 듣겠어요.";
  console.log("error.....");
}

function endEvent(){
  console.log("done!");
}

function startEvent(){
  myRec.start();  
}

function draw(){
  background(255, 255, 0);

  textAlign(CENTER, CENTER);
  textSize(48);
  text(string, width/2, height/2);
  text(string2, width/2, height/2+100);
}
