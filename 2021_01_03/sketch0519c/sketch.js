let myVoice = new p5.Speech();

let input_text;
let btn_speak;
let cb_interrupt;
let sl_volume, sl_rate, sl_pitch;
let bool_interrupt = false;

function preload(){
}

function setup(){
  createCanvas(800, 800);

  myVoice.setVoice(11);

  input_text = createInput("안녕하세요? 반가워요.");
  input_text.style("width", "500px");
  input_text.position(20, 50);

  btn_speak = createButton("Speak");
  btn_speak.style("width", "150px");
  btn_speak.position(20, 100);
  btn_speak.mousePressed(doSpeak);

  cb_interrupt = createCheckbox("interrupt?", bool_interrupt);
  cb_interrupt.position(200, 100);
  cb_interrupt.changed(checkboxEvent);

  sl_volume = createSlider(0.0, 100.0, 50.0);
  sl_volume.position(20, 150);
  sl_volume.mouseReleased(setVolume);

  sl_rate = createSlider(10.0, 200.0, 100.0);
  sl_rate.position(20, 170);
  sl_rate.mouseReleased(setRate);

  sl_pitch = createSlider(1.0, 200.0, 100.0);
  sl_pitch.position(20, 190);
  sl_pitch.mouseReleased(setPitch);
}

function doSpeak(){
  //myVoice.listVoices();
  //console.log(myVoice.voices.length);
  myVoice.interrupt = bool_interrupt;
  myVoice.speak(input_text.value());

  console.log("volume: " + sl_volume.value()/100.0);
  console.log("rate: " + sl_rate.value()/100.0);
  console.log("pitch: " + sl_pitch.value()/100.0)
}

function checkboxEvent(){
  if(this.checked() == true){
    bool_interrupt = true;
  }else{
    bool_interrupt = false;
  }
}

function setVolume(){
  myVoice.setVolume(sl_volume.value()/100.0);
}

function setRate(){
  myVoice.setRate(sl_rate.value()/100.0);
}

function setPitch(){
  myVoice.setPitch(sl_pitch.value()/100.0);
}

function draw(){
  background(255, 255, 0);

  textAlign(LEFT, TOP);
  textSize(16);
  text("volume", 160, 150);
  text("rate", 160, 170);
  text("pitch", 160, 190);
}
