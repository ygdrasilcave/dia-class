let assets_folder = '../assets/playSource/';

let ghost;
let asterisk;

let slider_ghost_frame;
let slider_asterisk_frame;

function preload(){
  ghost = loadAnimation(assets_folder+'ghost_standing0001.png', assets_folder+'ghost_standing0007.png');

  asterisk = loadAnimation(assets_folder+'asterisk.png', assets_folder+'triangle.png', assets_folder+'square.png', assets_folder+'cloud.png', assets_folder+'star.png', assets_folder+'mess.png', assets_folder+'monster.png');
}

function setup(){
  createCanvas(1000, 750);
  background(255, 255, 0);

  console.log(ghost.frameDelay);
  console.log(asterisk.frameDelay);
  //ghost.frameDelay = 30;

  slider_ghost_frame = createSlider(2, 30, 15);
  slider_ghost_frame.style('width', '300px');
  slider_ghost_frame.style('height', '10px');
  slider_ghost_frame.style('transform: rotate(90deg)');
  slider_ghost_frame.position(-120, 150);

  slider_asterisk_frame = createSlider(2, 30, 15);
  slider_asterisk_frame.position(width-350, 10);
  slider_asterisk_frame.style('width', '300px');
}

function draw(){
  background(255, 255, 0);

  //ghost.frameDelay = int(map(mouseY, 0, height, 30, 2));
  ghost.frameDelay = slider_ghost_frame.value();
  asterisk.frameDelay = map(slider_asterisk_frame.value(), 2, 30, 30, 2);

  animation(ghost, 200, height/2);
  animation(asterisk, width/2, height/2);
}

