function setup() {
  // put setup code here
  createCanvas(800,800);
}

function draw() {
  // put drawing code here
  background(250);

  stroke(255, 0, 255);
  line(30, 500, 700, 100);

  stroke(255, 10, 0);
  noFill();
  triangle(450, 180, 750, 300, 550, 500);
  quad(400, 550, 600, 400, 780, 430, 630, 760);
  
  stroke(0, 200, 10);
  noFill();
  ellipse(200, 450, 300, 300);
  arc(100, 400, 450, 450, radians(265), radians(320));

  stroke(0);
  fill(255, 255, 0);
  rect(230, 550, 200, 100);
}