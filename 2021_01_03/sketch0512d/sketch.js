let words = ['안녕하세요', '디지털크래프트 수업입니다', '열심히 공부합시다'];

let myVoice = new p5.Speech();
let btn_list;
let toggle = true;

let currentVoice;

function preload(){
}

function setup(){
  createCanvas(800, 800);

  btn_list = createButton('List Voices');
  btn_list.position(20 , 20);
  btn_list.mousePressed(returnVoiceList);

  //myVoice.setVoice(11);

  textAlign(CENTER, CENTER);
  textSize(48);

}

function returnVoiceList(){
  myVoice.listVoices();
  console.log(myVoice.voices.length);
}

function draw(){
  background(255, 255, 0);

  if(second()%5 == 0 && toggle == true){
    let voiceNum = int(random(myVoice.voices.length));
    myVoice.setVoice(voiceNum);
    let index = int(random(words.length));
    myVoice.speak(words[index]);
    toggle = false;

    console.log(voiceNum);
    currentVoice = myVoice.voices[voiceNum]['name'];
    console.log(currentVoice);

  }else if(second()%5 == 1 && toggle == false){
    toggle = true;
  }

  fill(0);
  noStroke();
  text(currentVoice, width/2, height/2)
}