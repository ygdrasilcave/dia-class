let assets_folder = '../assets/playSource/';

let ani_box, ani_asterisk1, ani_asterisk2, ani_cloud, ani_circle;
let sp_box, sp_asterisk, sp_cloud, sp_circle;

let debug_mode = false;

function preload(){
  ani_box = loadAnimation(assets_folder+'box0001.png', assets_folder+'box0003.png');
  ani_asterisk1 = loadAnimation(assets_folder+'asterisk_normal0001.png', assets_folder+'asterisk_normal0003.png');
  ani_asterisk2 = loadAnimation(assets_folder+'asterisk_circle0006.png', assets_folder+'asterisk_circle0008.png');
  ani_cloud = loadAnimation(assets_folder+'cloud_breathing0001.png', assets_folder+'cloud_breathing0009.png');
  ani_circle = loadAnimation(assets_folder+'plain_circle.png');
}

function setup(){
  createCanvas(800, 800);

  sp_box = createSprite(width/4-50, height/2);
  sp_box.addAnimation('normal', ani_box);

  sp_circle = createSprite(width/2, height/2);
  sp_circle.addAnimation('normal', ani_circle);

  sp_cloud = createSprite(3*(width/4), height/2);
  sp_cloud.addAnimation('normal', ani_cloud);

  sp_asterisk = createSprite(random(width), random(height));
  sp_asterisk.addAnimation('normal', ani_asterisk1);
  sp_asterisk.addAnimation('circle', ani_asterisk2);

  textAlign(CENTER, CENTER);
  textSize(16);
}

//collide, overlap, displace

function draw(){
  background(255, 255, 0);

  sp_asterisk.position.x = mouseX;
  sp_asterisk.position.y = mouseY;

  //If overlapping with box asterisk will be placed
  //in the closest non overlapping position
  sp_asterisk.collide(sp_box);

  //sprite.overlap() returns true if overlapping occours
  //note: by default the check is performed on the images bounding box
  //press mouse button to visualize them
  if(sp_asterisk.overlap(sp_circle) == true){
    sp_asterisk.changeAnimation('circle');
  }else{
    sp_asterisk.changeAnimation('normal');
  }

  //displace is the opposite of collide, the sprite in the parameter will
  //be pushed away but the sprite calling the function
  sp_asterisk.displace(sp_cloud);

  boundDebug(debug_mode);

  //console.log(sp_asterisk.overlap(sp_circle));

  drawSprites();

  fill(0);
  noStroke();
  text("overlap", width/2, height/2);
  text("collide", width/4-50, height/2);
  text("displace", sp_cloud.position.x, sp_cloud.position.y);
}

function boundDebug(_b){
  sp_asterisk.debug = _b;
  sp_circle.debug = _b;
  sp_box.debug = _b;
  sp_cloud.debug = _b;
}

function keyPressed(){
  if(key == 'd' || key == 'D'){
    debug_mode = !debug_mode;
  }
}
