ArrayList<Mover> m;
float matx;
float maty;
float mwx;
float mwy;

void setup() {
  size(600, 600);
  background(0);
  matx = random(-0.08, 0.08);
  maty = random(-0.08, 0.08);
  mwx = random(100, 300);
  mwy = random(100, 300);
  m = new ArrayList<Mover>();
  //m.add(new Mover(matx, maty, mwx, mwy));
  rectMode(CENTER);
}

void draw() {
  background(0);
  beginShape();
  for (Mover _m : m) {
    _m.display();
    strokeWeight(2);
    stroke(255);
    noFill();
    vertex(_m.pos[0].x, _m.pos[0].y);
  }
  endShape();
}

void keyPressed() {
  if (key == ' ') {
    m.add(new Mover(matx, maty, mwx, mwy));
  }
  if (key == 'e') {
    m.clear();
    matx = random(-0.08, 0.08);
    maty = random(-0.08, 0.08);
    mwx = random(100, 300);
    mwy = random(100, 300);
  }
}

