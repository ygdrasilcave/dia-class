import de.looksgood.ani.*;
import de.looksgood.ani.easing.*;

Easing[] easings = { 
  Ani.LINEAR, Ani.QUAD_IN, Ani.QUAD_OUT, Ani.QUAD_IN_OUT, Ani.CUBIC_IN, Ani.CUBIC_IN_OUT, Ani.CUBIC_OUT, Ani.QUART_IN, Ani.QUART_OUT, Ani.QUART_IN_OUT, Ani.QUINT_IN, Ani.QUINT_OUT, Ani.QUINT_IN_OUT, Ani.SINE_IN, Ani.SINE_OUT, Ani.SINE_IN_OUT, Ani.CIRC_IN, Ani.CIRC_OUT, Ani.CIRC_IN_OUT, Ani.EXPO_IN, Ani.EXPO_OUT, Ani.EXPO_IN_OUT, Ani.BACK_IN, Ani.BACK_OUT, Ani.BACK_IN_OUT, Ani.BOUNCE_IN, Ani.BOUNCE_OUT, Ani.BOUNCE_IN_OUT, Ani.ELASTIC_IN, Ani.ELASTIC_OUT, Ani.ELASTIC_IN_OUT
};
int easing_counter = 0;
String[] easingsVariableNames = {
  "LINEAR", "QUAD_IN", "QUAD_OUT", "QUAD_IN_OUT", "CUBIC_IN", "CUBIC_IN_OUT", "CUBIC_OUT", "QUART_IN", "QUART_OUT", "QUART_IN_OUT", "QUINT_IN", "QUINT_OUT", "QUINT_IN_OUT", "SINE_IN", "SINE_OUT", "SINE_IN_OUT", "CIRC_IN", "CIRC_OUT", "CIRC_IN_OUT", "EXPO_IN", "EXPO_OUT", "EXPO_IN_OUT", "BACK_IN", "BACK_OUT", "BACK_IN_OUT", "BOUNCE_IN", "BOUNCE_OUT", "BOUNCE_IN_OUT", "ELASTIC_IN", "ELASTIC_OUT", "ELASTIC_IN_OUT"
};
/*Ani.LINEAR
 Ani.QUAD_IN
 Ani.QUAD_OUT
 Ani.QUAD_IN_OUT
 Ani.CUBIC_IN
 Ani.CUBIC_OUT
 Ani.CUBIC_IN_OUT
 Ani.QUART_IN
 Ani.QUART_OUT
 Ani.QUART_IN_OUT
 Ani.QUINT_IN
 Ani.QUINT_OUT
 Ani.QUINT_IN_OUT
 Ani.SINE_IN
 Ani.SINE_OUT
 Ani.SINE_IN_OUT
 Ani.CIRC_IN
 Ani.CIRC_OUT
 Ani.CIRC_IN_OUT
 Ani.EXPO_IN
 Ani.EXPO_OUT
 Ani.EXPO_IN_OUT
 Ani.BACK_IN
 Ani.BACK_OUT
 Ani.BACK_IN_OUT
 Ani.BOUNCE_IN
 Ani.BOUNCE_OUT
 Ani.BOUNCE_IN_OUT
 Ani.ELASTIC_IN
 Ani.ELASTIC_OUT
 Ani.ELASTIC_IN_OUT*/

float x = 0;
float y = 0;

float whiteVal = 0;
float rotateVal = 0.0;

float px = 0;
float py = 0;

Ani ani_x;
Ani ani_y;
Ani ani_white;
Ani ani_rotate;

boolean bx = true;
boolean by = true;

boolean bxend = false;
boolean byend = false;

int offSetX = 0;
int offSetY = 0;

int b_type = 2;

void setup() {
  //println(easings.length);
  size(1440, 782);
  Ani.init(this);
  //Ani.noAutostart();
  ani_x = new Ani(this, 2.5, "x", width/10, easings[easing_counter], "onEnd:ani_x_end");
  ani_y = new Ani(this, 2.5, "y", width/10, easings[easing_counter], "onEnd:ani_y_end");
  ani_white = new Ani(this, 2.5, "whiteVal", 255, easings[easing_counter]);
  ani_rotate = new Ani(this, 2.5, "rotateVal", TWO_PI, easings[easing_counter]);
  background(0);
  smooth();
  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  text(easingsVariableNames[0], offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+50);
  text(easingsVariableNames[0], offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+65);
  text(ani_x.getDuration()+": "+bx, offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+80);
  text(ani_y.getDuration()+": "+by, offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+95);
  text(nf(ani_rotate.getBegin(), 1, 3)+" -> "+nf(ani_rotate.getEnd(), 1, 3), offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+110);
}

void draw() {
  //background(0);
  if (bxend == true && byend == true) {
    easing_counter++;    
    if (easing_counter > easings.length-1) {
      easing_counter = 0;
    }
    println(easing_counter);
    //background(0);
    bxend = false;
    byend = false;
    int easingX = int(random(0, 31));
    //ani_x.setEasing(easings[easing_counter]);
    ani_x.setEasing(easings[easingX]);
    float xDuration = random(0.5, 5);
    float yDuration = random(0.5, 5);
    ani_x.setDuration(xDuration);
    if (bx == true) {
      ani_x.setBegin(0);
      ani_x.setEnd(width/10);
      ani_x.start();
    } else {
      ani_x.setBegin(width/10);
      ani_x.setEnd(0);
      ani_x.start();
    }
    int easingY = int(random(0, 31));
    ani_y.setEasing(easings[easingY]);
    ani_y.setDuration(yDuration);
    if (by == true) {
      ani_y.setBegin(0);
      ani_y.setEnd(width/10);
      ani_y.start();
    } else {
      ani_y.setBegin(width/10);
      ani_y.setEnd(0);
      ani_y.start();
    }
    if (xDuration > yDuration) {
      ani_white.setDuration(xDuration);
      ani_rotate.setDuration(xDuration);
    } else {
      ani_white.setDuration(yDuration);
      ani_rotate.setDuration(yDuration);
    }
    ani_white.start();
    ani_rotate.setBegin(random(TWO_PI));
    ani_rotate.setEnd(random(TWO_PI));
    ani_rotate.start();
    offSetX++;
    if (offSetX > 9) {
      offSetX = 0;
      offSetY++;
    }
    if (offSetY > 2) {
      offSetY = 0;
      saveFrame("output_######.tif");
      background(0);
      //b_type++;
      //b_type = b_type%3;
    }
    text(easingsVariableNames[easingX], offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+50);
    text(easingsVariableNames[easingY], offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+65);
    text(ani_x.getDuration()+": "+bx, offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+80);
    text(ani_y.getDuration()+": "+by, offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+95);
    text(nf(ani_rotate.getBegin(), 1, 3)+" -> "+nf(ani_rotate.getEnd(), 1, 3), offSetX*(width/10)+(width/10)/2, offSetY*100+(width/10)*3+110);
  }

  pushMatrix();
  translate(offSetX*(width/10)+(width/10)/2, offSetY*(width/10)+(width/10)/2);
  if (b_type != 2) {
    rotate(rotateVal);
  }
  noFill();
  if (b_type == 2) {
    stroke(255);
  } else {
    stroke(whiteVal);
    strokeWeight(1);
  }
  if (b_type == 0) {
    ellipse(0, 0, x, y);
  } else if (b_type == 1) {
    rect(0, 0, x, y);
  } else if (b_type == 2) {
    strokeWeight(3);
    point(x-(width/10)/2, y-(width/10)/2);
    strokeWeight(1);
    line(px, py, x-(width/10)/2, y-(width/10)/2);
    px = x-(width/10)/2;
    py = y-(width/10)/2;
  }
  /*stroke(180);
   strokeWeight(3);
   point(offSetX*(width/10)+x, offSetY*(width/10)+y);*/
  popMatrix();
}

void ani_x_end() {
  //bx = !bx;
  float b = random(0, 1);
  if (b > 0.5) {
    bx = true;
  } else {
    bx = false;
  }
  bxend = true;
}

void ani_y_end() {
  //by = !by;
  float b = random(0, 1);
  if (b > 0.5) {
    by = true;
  } else {
    by = false;
  }
  byend = true;
}


void keyPressed() {
  if (key == 'a') {
    ani_x.setDuration(random(1, 5));
    if (bx == true) {
      ani_x.setBegin(100);
      ani_x.setEnd(400);
      ani_x.start();
    } else {
      ani_x.setBegin(400);
      ani_x.setEnd(100);
      ani_x.start();
    }
  }
  if (key == 'b') {
    ani_y.setDuration(random(1, 5));
    if (by == true) {
      ani_y.setBegin(100);
      ani_y.setEnd(400);
      ani_y.start();
    } else {
      ani_y.setBegin(400);
      ani_y.setEnd(100);
      ani_y.start();
    }
  }
  if (key == ' ') {
    bxend = false;
    byend = false;
    ani_x.setDuration(2.5);
    if (bx == true) {
      ani_x.setBegin(0);
      ani_x.setEnd(width/10);
      ani_x.start();
    } else {
      ani_x.setBegin(width/10);
      ani_x.setEnd(0);
      ani_x.start();
    }
    ani_y.setDuration(2.5);
    if (by == true) {
      ani_y.setBegin(0);
      ani_y.setEnd(width/10);
      ani_y.start();
    } else {
      ani_y.setBegin(width/10);
      ani_y.setEnd(0);
      ani_y.start();
    }
  }
}
