PImage img;
int pixelSize = 10;
int nx;
int ny;
PVector[] pos;
PVector[] vel;
PVector[] target;
color[] col;

void setup() {
  img = loadImage("face_w.jpg");
  size(img.width, img.height);
  nx = int(img.width/pixelSize);
  ny = int(img.height/pixelSize);
  pos = new PVector[nx*ny];
  vel = new PVector[nx*ny];
  target = new PVector[nx*ny];
  col = new color[nx*ny];
  for (int y=0; y<ny; y++) {
    for (int x=0; x<nx; x++) {
      int index = x + y*nx;
      pos[index] = new PVector(random(width), random(height));
      vel[index] = new PVector(random(2.5, 5.5), random(2.5, 5.5));
      target[index] = new PVector(x*pixelSize+pixelSize/2, y*pixelSize+pixelSize/2);
      //col[index] = img.pixels[x*pixelSize + y*pixelSize*nx];
      col[index] = img.pixels[x*pixelSize + y*pixelSize*img.width];
    }
  }
}

boolean targetMove = false;
boolean toggle = true;

void draw() {
  background(0);
  for (int i=0; i<nx*ny; i++) {
    if (targetMove == true) {
      PVector dist = PVector.sub(target[i], pos[i]);
      dist.mult(0.05);
      pos[i].add(dist);
    } else {
      pos[i].add(vel[i]);
    }
    if(pos[i].x < 0 || pos[i].x > width){
      vel[i].x *= -1;
    }
    if(pos[i].y < 0 || pos[i].y > width){
      vel[i].y *= -1;
    }
    
    stroke(col[i]);
    //noStroke();
    //strokeWeight(pixelSize);
    //point(pos[i].x, pos[i].y);
    noFill();
    rect(pos[i].x, pos[i].y, pixelSize, pixelSize);
  }
  
  if(second()%10 == 0 && toggle == true){
    targetMove = !targetMove;
    toggle = false;
  }else if(second()%10 == 1 && toggle == false){
    toggle = true;
  }
}

void keyPressed() {
  if (key == 't') {
    targetMove = !targetMove;
  }
}

