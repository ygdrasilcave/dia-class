class Mover {
  PVector loc;
  PVector ploc;
  PVector t;
  PVector st;
  PVector w;
  float taily = 0;
  float tailt = 0;
  float tailw = 0;
  
  PVector target;
  
  boolean targetB;

  Mover() {
    loc = new PVector();
    ploc = new PVector();
    t = new PVector(0, 0);
    st = new PVector(random(-0.05, 0.05), random(-0.05, 0.05));
    w = new PVector(random(30, width/2), random(30, height/2));
    if (abs(st.x) > abs(st.y)) {
      tailw = abs(st.x)*400;
    } else {
      tailw = abs(st.y)*400;
    }
    
    targetB = false;
    target = new PVector();
  }
  
  Mover(float _stx, float _sty, float _ww, float _wh) {
    loc = new PVector();
    ploc = new PVector();
    t = new PVector(0, 0);
    st = new PVector(_stx, _sty);
    w = new PVector(_ww, _wh);
    if (abs(st.x) > abs(st.y)) {
      tailw = abs(st.x)*400;
    } else {
      tailw = abs(st.y)*400;
    }
    
    targetB = true;
    
    target = new PVector();
  }
  
  void targetMove(PVector _tg){
    target = _tg.get();
    //target.x -= width/2;
    //target.y -= height/2;
       
    PVector speed = PVector.sub(target, loc);
    //speed.normalize();
    speed.mult(0.03);
    
    loc.x += speed.x;
    loc.y += speed.y;
    
    speed.normalize();
    speed.mult(-1);
    pushMatrix();    
    translate(width/2 + loc.x, height/2 + loc.y);
    rotate(speed.heading());
    strokeWeight(3);
    noFill();
    if(targetB == true){
      stroke(255, 0, 255);
    }else{
      stroke(255);
    }
    rect(0, 0, 30, 10);

    beginShape();
    taily = 0;
    for (int i=15; i<tailw*6; i+=3) {      
      vertex(i, taily);
      taily = cos(tailt)*tailw;
      tailt += 0.112;
    }
    endShape();

    fill(255);
    noStroke();
    ellipse(-30, 0, 10, 10);
    t.add(st);
    popMatrix();
    ploc.x = loc.x;
    ploc.y = loc.y;
  }
  
  void display() {
    float x = cos(t.x)*w.x;
    float y = sin(t.y)*w.y;
    loc.x = x;
    loc.y = y;

    pushMatrix();
    translate(width/2 + x, height/2 + y);
    PVector speed = PVector.sub(ploc, loc);
    rotate(speed.heading());
    //fill(255);

    strokeWeight(3);
    noFill();
    if(targetB == true){
      stroke(255, 0, 255);
    }else{
      stroke(255);
    }
    rect(0, 0, 30, 10);

    beginShape();
    taily = 0;
    for (int i=15; i<tailw*6; i+=3) {      
      vertex(i, taily);
      taily = cos(tailt)*tailw;
      tailt += 0.112;
    }
    endShape();

    fill(255);
    noStroke();
    ellipse(-30, 0, 10, 10);
    t.add(st);
    popMatrix();
    ploc.x = loc.x;
    ploc.y = loc.y;
  }
}

