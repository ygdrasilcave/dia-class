Mover[] m = new Mover[10];
Mover t;

PVector tg;

void setup() {
  size(600, 600);
  background(0);
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover();
  }
  rectMode(CENTER);
  
  t = new Mover(random(-0.08, 0.08), random(-0.08, 0.08), random(width/2, width/2), random(height/2, height/2));
  
  tg = new PVector();
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 90);
  rect(width/2, height/2, width, height);
  t.display();
  if (mousePressed) {
    //tg.x = mouseX;
    //tg.y = mouseY;
    tg.x = t.loc.x;
    tg.y = t.loc.y;
    for (int i=0; i<m.length; i++) {
      m[i].targetMove(tg);
    }
  } else {
    for (int i=0; i<m.length; i++) {
      m[i].display();
    }
  }
}

