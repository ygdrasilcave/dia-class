PImage img0, img1;
int pixelSize = 10;
int nx;
int ny;
PVector[] pos;
PVector[] vel;
PVector[] target;
color[] col;

int counter = 0;

void setup() {
  img0 = loadImage("rose.jpg");
  img1 = loadImage("face_w1.jpg");
  size(img0.width, img0.height);
  nx = img0.width/pixelSize;
  ny = img0.height/pixelSize;

  pos = new PVector[nx*ny];
  vel = new PVector[nx*ny];
  target = new PVector[nx*ny];
  col = new color[nx*ny];

  for (int y = 0; y<ny; y++) {
    for (int x = 0; x<nx; x++) {
      int index = x + y*nx;
      pos[index] = new PVector(random(width), random(height));
      vel[index] = new PVector(random(1.5, 5.5), random(1.5, 5.5));
      target[index] = new PVector(x*pixelSize, y*pixelSize);
      col[index] = color(img0.pixels[x*pixelSize + y*pixelSize*width]);
    }
  }
}

boolean targetMove = false;

void keyPressed() {
  if (key == 't') {
    targetMove = !targetMove;    
    if (targetMove == true) {
      for (int y = 0; y<ny; y++) {
        for (int x = 0; x<nx; x++) {
          int index = x + y*nx;
          if (counter == 0) {
            col[index] = color(img0.pixels[x*pixelSize + y*pixelSize*width]);
          } else if (counter == 1) {
            col[index] = color(img1.pixels[x*pixelSize + y*pixelSize*width]);
          }
        }
      }
      counter++;
      if (counter > 1) {
        counter = 0;
      }
    }
  }
}

void draw() {
  background(0);
  for (int i=0; i<nx*ny; i++) {
    if (targetMove == true) {
      PVector dist = PVector.sub(target[i], pos[i]);
      dist.mult(0.02);
      pos[i].add(dist);
    } else {
      pos[i].add(vel[i]);
      if (pos[i].x < 0 || pos[i].x > width) {
        vel[i].x *= -1;
      }
      if (pos[i].y < 0 || pos[i].y > height) {
        vel[i].y *= -1;
      }
    }
    fill(col[i]);
    noStroke();
    rect(pos[i].x, pos[i].y, pixelSize, pixelSize);
  }
}

