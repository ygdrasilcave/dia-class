ArrayList<Ball> b;
IntList randomDrunk;
int count = 0;
boolean toggle = true;
int curSec;
int preSec;

int worldLife = 0;

/*import ddf.minim.*;
 Minim minim;
 AudioInput in;*/

void setup() {
  size(600, 600);
  b = new ArrayList<Ball>();
  background(0);
  randomDrunk = new IntList(); 
  makeRandomDrunk(5);
  textAlign(CENTER, CENTER);
  textSize(42);

  /*minim = new Minim(this);
   in = minim.getLineIn();*/
}

void draw() {
  noStroke();
  fill(0, 10);
  rect(0, 0, width, height);
  //background(0);

  for (int i=0; i < b.size (); i++) {
    Ball _b = b.get(i);
    _b.accX = random(-0.5, 0.5);
    _b.accY = random(-0.5, 0.5);
    _b.update();
    _b.display();
    if (_b.isDead() == true) {
      b.remove(i);
    }
  }
  /*curSec = second();
   //if (curSec != preSec && toggle == true) {
   if (frameCount%10 == 0 && toggle == true) {
   println(randomDrunk.get(count));
   b.add(new Ball(randomDrunk.get(count)));
   count++;
   if (count > randomDrunk.size()-1) {
   count = 0;
   //newRandomDrunk(int(random(3, 10)));
   makeRandomDrunk(5);
   }
   toggle = false;
   //preSec = curSec;
   } else {
   toggle = true;
   }*/
  worldLife++;
  if (worldLife > 1000) {
    b.clear();
    worldLife = 0;
    makeRandomDrunk(5);
    count = 0;
  }

  /*if (in.left.get(256)*100 > 2.3) {
   b.add(new Ball(randomDrunk.get(count)));
   count++;
   if (count > randomDrunk.size()-1) {
   count = 0;
   //newRandomDrunk(int(random(3, 10)));
   makeRandomDrunk(5);
   }
   }
   println(in.left.get(256)*100);*/
}

void mousePressed() {
  println(randomDrunk.get(count));
  b.add(new Ball(randomDrunk.get(count)));
  count++;
  if (count > randomDrunk.size()-1) {
    count = 0;
    //newRandomDrunk(int(random(3, 10)));
    makeRandomDrunk(5);
  }
}

void makeRandomDrunk(int _range) {
  if (randomDrunk != null) {
    randomDrunk.clear();
  }
  for (int i=0; i<_range; i++) {
    randomDrunk.append(i);
  }
  randomDrunk.shuffle();
  println(randomDrunk);
}

