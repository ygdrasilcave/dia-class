class Ball {

  float x, y;
  float speedX, speedY;
  boolean type;
  float accX, accY;
  float maxSpeed;
  int dirX, dirY;

  Ball(int _i) {
    x = width/2;
    y = height/2;
    speedX = 0;
    speedY = 0;
    accX = random(-0.05, 0.05);
    accY = random(-0.05, 0.05);
    maxSpeed = 5;
    dirX = 1;
    dirY = 1;
    
    if (_i%2 == 0) {
      type = true;
    } else {
      type = false;
    }
  }

  void update() {
    speedX += accX;
    speedY += accY;
    if(speedX > maxSpeed){
      speedX = maxSpeed;
    }else if(speedX < -maxSpeed){
      speedX = -maxSpeed;
    }
    if(speedY > maxSpeed){
      speedY = maxSpeed;
    }else if(speedY < -maxSpeed){
      speedY = -maxSpeed;
    }
    x = x + speedX*dirX;
    y = y + speedY*dirY;

    if (x > width-10) {
      x = width-10;
      dirX = dirX*-1;
    }
    if (x < 10) {
      x = 10;
      dirX = dirX*-1;
    }
    if (y > height-10) {
      y = height-10;
      dirY = dirY*-1;
    }
    if (y < 10) {
      y = 10;
      dirY = dirY*-1;
    }
  }

  void display() {
    if (type== true) {
      noStroke();
      fill(255);
    } else {
      stroke(255);
      noFill();
    }
    ellipse(x, y, 20, 20);
  }
}

