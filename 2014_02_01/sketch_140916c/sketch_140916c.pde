Ball[] b;
int ballNum = 100;

void setup() {
  size(600, 600);
  background(0);
  b = new Ball[ballNum];
  for (int i=0; i<ballNum; i++) {
    b[i] = new Ball(i);
  }
}

void draw() {
  background(0);
  for (int i = 0; i<ballNum; i++) {
    b[i].update();
    b[i].display();
  }
}

