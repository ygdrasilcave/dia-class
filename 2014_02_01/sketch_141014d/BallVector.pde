class BallVector {
  PVector pos;
  PVector speed;
  PVector acc;
  float mass;
  float angle;
  float angleSpeed;
  
  BallVector() {
    pos = new PVector(random(width), height);
    speed = new PVector(0, 0);
    acc = new PVector(0.0, 0.0);
    mass = random(1, 6);
    angle = 0;
    angleSpeed = random(-0.1, 0.1);
  }
  
  
  
  void addForce(PVector f){
    PVector force = PVector.div(f, mass);
    //f.div(mass);   
    acc.add(force);
  }

  void update() {
    speed.add(acc);
    pos.add(speed); 
    acc.mult(0);

    if (pos.x > width) {
      pos.x = 0;
      //speed.x *= -1;
    }
    if (pos.x < 0) {
      pos.x = width;
      //speed.x *= -1;
    }
    if (pos.y > height+100) {
      pos.y = -100;
      //speed.y *= -1;
    }
    if (pos.y < -100) {
      pos.y = height+100;
      //speed.y *= -1;
      speed.x = 0;
      speed.y = 0;
    }
    angle += angleSpeed;
  }

  void display() {
    //fill(255);
    //noStroke();
    //noFill();
    //stroke(255);
    //ellipse(pos.x, pos.y, mass*15, mass*15);
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(angle);
    tint(255, 100);
    image(img, 0-(mass*15)/2, 0-(mass*15)/2, mass*15, mass*15);
    popMatrix();
  }
}

