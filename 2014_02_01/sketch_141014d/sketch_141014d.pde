BallVector[] b;
int ballNum = 60;
PVector m = new PVector(0, 0);
float t = 0.0;
PImage img;

void setup() {
  img = loadImage("perfect_bubble.png");
  size(800, 600);
  background(0);
  b = new BallVector[ballNum];
  for (int i=0; i<ballNum; i++) {
    b[i] = new BallVector();
  }
}

void draw() {
  background(0);
  //fill(0, 20);
  //noStroke();
  //rect(0,0,width,height);
  PVector w = new PVector(0.0, -0.2);
  m.x = sin(t);
  t+=0.34;

  for (int i=0; i<ballNum; i++) { 
    PVector mouse = new PVector(mouseX, mouseY);
    PVector attract = PVector.sub(mouse, b[i].pos);
    float dist = attract.mag();
    dist = constrain(dist, 3, 8);
    attract.normalize();
    float force = (20*b[i].mass)/(dist*dist);
    attract.mult(force);
    if (mousePressed) {
      b[i].addForce(attract);
    }
    b[i].addForce(w);
    b[i].addForce(m);
    b[i].update();
    b[i].display();
  }
  //stroke(255);
  //line(0, height/2, width, height/2);
}

