PVector pos;
PVector speed;
PVector acc;

void setup(){
  size(500,500);
  background(0);
  
  pos = new PVector(width/2, height/2);
  speed = new PVector(0, 0);
  acc = new PVector(0.0, 0.0);
}

void draw(){
  background(0);
  
  //acc.x = 0.0;
  //acc.y = 0.4;
  PVector g = new PVector(0.0, 0.4);
  acc.add(g);
  
  if(keyPressed){
    PVector w = new PVector(0.2, 0.0);
    acc.add(w);
  }
  
  speed.add(acc);
  //speed.limit(8);
  pos.add(speed); 
  acc.mult(0);
  
  fill(255);
  noStroke();
  ellipse(pos.x, pos.y, 30, 30);
  
  if(pos.x > width){
    pos.x = width;
    speed.x *= -1;
  }
  if(pos.x < 0){
    pos.x = 0;
    speed.x *= -1;
  }
  if(pos.y > height){
    pos.y = height;
    speed.y *= -1;
  }
  if(pos.y < 0){
    pos.y = 0;
    speed.y *= -1;
  }
  
}
