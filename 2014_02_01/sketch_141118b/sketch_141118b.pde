float x;
float y;
float rx;
float ry;
float t;
float pt;

float mx;
float my;
float mrx;
float mry;
float mt;

float hx;
float hy;
float hrx;
float hry;
float ht;

void setup() {
  size(300, 300);
  x = 0;
  y = 0;
  rx = 100;
  ry = 100;
  t = second()*TWO_PI/60 - HALF_PI;
  mt = minute()*TWO_PI/60 - HALF_PI + t/3600;
  ht = hour()*TWO_PI/12 - HALF_PI + mt/12;
}

void draw() {
  background(0);
  x = rx*cos(t);
  y = ry*sin(t);

  mx = mrx*cos(mt);
  my = mry*sin(mt);

  hx = hrx*cos(ht);
  hy = hry*sin(ht);

  stroke(255);
  line(width/2, 0, width/2, height);
  line(0, height/2, width, height/2);

  pushMatrix();
  strokeWeight(1);
  translate(width/2, height/2);
  fill(255);
  //ellipse(x, y, 10, 10);  
  //ellipse(mx, my, 10, 10);
  line(0, 0, x, y);
  line(0, 0, mx, my);
  line(0, 0, hx, hy);
  //ellipse(0, y, 10, 10);
  //ellipse(x, 0, 10, 10);
  noFill();
  //ellipse(0,0,rx*2, ry*2);
  for (int i=0; i<60; i++) { 
    if (i%5 == 0) {
      strokeWeight(5);
    } else {
      strokeWeight(1);
    }
    line(rx*cos(TWO_PI/60*i), ry*sin(TWO_PI/60*i), (rx-5)*cos(TWO_PI/60*i), (ry-5)*sin(TWO_PI/60*i));
  }
  popMatrix();

  rx = constrain(mouseX, 50, width);
  ry = constrain(mouseY, 50, height);
  mrx = rx-30;
  mry = ry-30;
  hrx = mrx-15;
  hry = mry-15;
  t = second()*TWO_PI/60 - HALF_PI;
  if (pt != t) {
    mt += TWO_PI/3600;
    ht += TWO_PI/(3600*12);
  }
  //mt = minute()*TWO_PI/60 - HALF_PI;

  pt = t;
}

