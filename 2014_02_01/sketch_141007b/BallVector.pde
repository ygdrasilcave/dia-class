class BallVector {
  PVector pos;
  PVector speed;
  PVector acc;
  float mass;
  float frictionC;

  BallVector() {
    pos = new PVector(random(width), 0);
    speed = new PVector(0, 0);
    acc = new PVector(0.0, 0.0);
    mass = random(1, 6);
    frictionC = mass*0.1;
  }
  
  void addForce(PVector f){
    PVector force = PVector.div(f, mass);
    //f.div(mass);   
    acc.add(force);
  }

  void update() {
    speed.add(acc);
    pos.add(speed); 
    acc.mult(0);

    if (pos.x > width) {
      pos.x = 0;
      //speed.x *= -1;
    }
    if (pos.x < 0) {
      pos.x = width;
      //speed.x *= -1;
    }
    if (pos.y > height-(mass*15)/2) {
      pos.y = height-(mass*15)/2;
      speed.y *= -1;
    }
    if (pos.y < 0) {
      pos.y = 0;
      speed.y *= -1;
    }
  }

  void display() {
    //fill(255);
    //noStroke();
    noFill();
    stroke(255);
    ellipse(pos.x, pos.y, mass*15, mass*15);
  }
}

