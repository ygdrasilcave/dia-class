BallVector[] b;
float fC;
int ballNum = 50;

void setup() {
  size(500, 500);
  background(0);
  b = new BallVector[ballNum];
  for (int i=0; i<ballNum; i++) {
    b[i] = new BallVector();
  }
  fC = random(2, 5);
}

void draw() {
  background(0);
  //fill(0, 20);
  //noStroke();
  //rect(0,0,width,height);
  PVector w1 = new PVector(0.2, 0.0);
  PVector w2 = new PVector(-0.2, 0.0);
  PVector g = new PVector(0.0, 0.4);

  for (int i=0; i<ballNum; i++) { 
    PVector gravity = PVector.mult(g, b[i].mass); 
    b[i].addForce(gravity);

    if (keyPressed) {
      if (key == 'a') {
        b[i].addForce(w2);
      }
      if (key == 's') {
        b[i].addForce(w1);
      }
    }

    PVector friction = b[i].speed.get();
    friction.normalize();
    friction.mult(-1);
    if (b[i].pos.y > height/2) {
      friction.mult(b[i].frictionC+fC);
    } else {
      friction.mult(b[i].frictionC);
    }
    //friction.mult(0.05);
    b[i].addForce(friction);

    b[i].update();
    b[i].display();
  }
  stroke(255);
  line(0, height/2, width, height/2);
  text(fC, 30, height/2+30);
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<ballNum; i++) {
      b[i].pos = new PVector(random(width), 0);
      b[i].speed = new PVector(0, 0);
      b[i].acc = new PVector(0, 0);
    }
    fC = random(2, 5);
  }
}

