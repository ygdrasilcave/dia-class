void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  background(0);
  
  PVector u = new PVector(mouseX, mouseY);
  PVector v = new PVector(width/2, height/2);
  
  u.sub(v);
  //u.normalize();
  //u.mult(30);
  u.setMag(30);
  
  //translate(width/2, height/2);
  stroke(255);
  line(width/2, height/2, width/2+u.x, height/2+u.y);
}
