ArrayList<Ball> b;
int count = 0;

void setup() {
  size(600, 600);
  background(0);
  b = new ArrayList<Ball>();
  b.add(new Ball(count));
}

void draw() {
  fill(0, 10);
  noStroke();
  rect(0,0,width,height);
  for(int i=b.size()-1; i>=0; i--){
    Ball _b = b.get(i);
    _b.accX = random(-0.5, 0.5);
    _b.accY = random(-0.5, 0.5);
    _b.update();
    _b.display();
    if(_b.isDead() == true){
      b.remove(i);
    }
    println(_b.lifeTime);
  }
}

void mousePressed(){
  count++;
  b.add(new Ball(count));
}