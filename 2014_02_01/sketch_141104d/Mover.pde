class Mover {
  PVector a;
  PVector at;
  PVector[] pos;
  PVector w;
  PVector vel;

  Mover(float _atx, float _aty, float _wx, float _wy) {
    pos = new PVector[2];
    for (int i=0; i<2; i++) {
      pos[i] = new PVector(0, 0);
    }
    vel = new PVector(0, 0);
    a = new PVector(0, 0);
    at = new PVector(_atx, _aty);
    w = new PVector(_wx, _wy);
  }

  void display() {
    float x = width/2 + cos(a.x)*w.x;
    float y = height/2 + sin(a.y)*w.y;
    pos[0].x = x;
    pos[0].y = y;
    vel = PVector.sub(pos[1], pos[0]);
    strokeWeight(3);
    pushMatrix();
    translate(pos[0].x, pos[0].y);
    rotate(vel.heading());
    stroke(255);
    noFill();
    rect(0, 0, 30, 30);
    ellipse(-30, 0, 20, 20);
    popMatrix();    
    pos[1] = pos[0].get();

    a.add(at);
  }
}

