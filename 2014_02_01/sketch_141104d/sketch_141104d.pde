ArrayList<Mover> m;
float matx;
float maty;
float mwx;
float mwy;

void setup() {
  size(600, 600);
  background(0);
  matx = random(-0.05, 0.05);
  maty = random(-0.05, 0.05);
  mwx = random(10, 250);
  mwy = random(10, 250);
  m = new ArrayList<Mover>();
  m.add(new Mover(matx, maty, mwx, mwy));
  rectMode(CENTER);
}

void draw() {
  background(0);
  for (Mover _m : m) {
    _m.display();
  }
}

void keyPressed(){
  m.add(new Mover(matx, maty, mwx, mwy));
}

