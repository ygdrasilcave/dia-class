class BallVector{
  PVector pos;
  PVector speed;
  PVector acc;
  
  BallVector(){
    pos = new PVector(random(width), random(height/2));
    speed = new PVector(0,0);
    acc = new PVector(0,0);
  }
  
  void addForce(PVector _f){
    PVector f = _f.get();
    acc.add(_f);
  }
  
  void update(){
    speed.add(acc);
    pos.add(speed);
    acc.mult(0);
    
    if(pos.x > width-15){
      pos.x = width-15;
      speed.x *= -1;
    }
    if(pos.x < 15){
      pos.x = 15;
      speed.x *= -1;
    }
    if(pos.y > height-15){
      pos.y = height-15;
      speed.y *= -1;
    }
    if(pos.y < 15){
      pos.y = 15;
      speed.y *= -1;
    }
  }
  
  void display(){
    fill(255);
    noStroke();
    ellipse(pos.x, pos.y, 30, 30);
  }
  
}
