PVector pos;
PVector vel;
PVector target;

boolean targetMove = false;

void setup() {
  size(300, 300);
  background(0);
  pos = new PVector(width/2, height/2);
  vel = new PVector(random(0.5, 1), random(1, 2));
  target = new PVector(width/2, height/2);
}


void draw() {
  background(0);

  if (targetMove == true) {
    target.x = mouseX;
    target.y = mouseY;
    PVector dist = PVector.sub(target, pos);
    dist.mult(0.03);
    pos.add(dist);
  } else {
    pos.add(vel);
  }

  fill(255);
  ellipse(pos.x, pos.y, 10, 10);


  if (pos.x < 0 || pos.x > width) {
    vel.x *= -1;
  }
  if (pos.y < 0 || pos.y > height) {
    vel.y *= -1;
  }
}

void keyPressed(){
  if(key == 't'){
    targetMove = !targetMove;
  }
}

