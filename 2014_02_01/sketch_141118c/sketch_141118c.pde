Mover[] m = new Mover[10];

void setup() {
  size(600, 600);
  background(0);
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover();
  }
  rectMode(CENTER);
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 90);
  rect(width/2,height/2,width,height);
  for (int i=0; i<m.length; i++) {
    m[i].display();
  }
}

