BallVector[] b;

void setup() {
  size(500, 500);
  background(0);
  b = new BallVector[10];
  for (int i=0; i<10; i++) {
    b[i] = new BallVector();
  }
}

void draw() {
  background(0);
  for (int i=0; i<10; i++) {
    PVector g = new PVector(0.0, 0.4);
    //g.mult(b[i].mass);
    b[i].addForce(g);

    if (keyPressed) {
      if (key == 'a') {
        PVector w = new PVector(-0.2, 0.0);
        b[i].addForce(w);
      }
      if (key == 's') {
        PVector w = new PVector(0.2, 0.0);
        b[i].addForce(w);
      }
    }

    b[i].update();
    b[i].display();
  }
}

