class BallVector {
  PVector pos;
  PVector speed;
  PVector acc;
  
  float mass;

  BallVector() {
    pos = new PVector(random(width), 150);
    speed = new PVector(0, 0);
    acc = new PVector(0.0, 0.0);
    mass = random(1,3);
  }
  
  void addForce(PVector f){
    PVector force = PVector.div(f, mass);
    acc.add(force);
  }

  void update() {
    speed.add(acc);
    pos.add(speed); 
    acc.mult(0);

    if (pos.x > width) {
      pos.x = 0;
      //speed.x *= -1;
    }
    if (pos.x < 0) {
      pos.x = width;
      //speed.x *= -1;
    }
    if (pos.y > height) {
      pos.y = height;
      speed.y *= -1;
    }
    if (pos.y < 0) {
      pos.y = 0;
      speed.y *= -1;
    }
  }

  void display() {
    fill(255);
    noStroke();
    ellipse(pos.x, pos.y, mass*15, mass*15);
  }
}

