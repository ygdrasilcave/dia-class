PVector a;
PVector at;
PVector[] pos;
PVector w;
PVector vel;

void setup() {
  size(600, 600);
  background(0);
  pos = new PVector[2];
  for (int i=0; i<2; i++) {
    pos[i] = new PVector(0, 0);
  }
  vel = new PVector(0, 0);
  a = new PVector(0, 0);
  at = new PVector(random(-0.05, 0.05), random(-0.05, 0.05));
  w = new PVector(random(10, 250), random(10, 250));
  rectMode(CENTER);
}

void draw() {
  background(0);
  float x = width/2 + cos(a.x)*w.x;
  float y = height/2 + sin(a.y)*w.y;
  pos[0].x = x;
  pos[0].y = y;
  vel = PVector.sub(pos[1], pos[0]);
  pushMatrix();
  translate(pos[0].x, pos[0].y);
  rotate(vel.heading());
  stroke(255);
  noFill();
  rect(0, 0, 30, 30);
  ellipse(-30, 0, 20, 20);
  popMatrix();    
  pos[1] = pos[0].get();

  a.add(at);
}

