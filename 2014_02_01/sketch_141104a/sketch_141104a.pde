PVector loc;
PVector vel;

void setup(){
  size(600, 600);
  background(0);
  rectMode(CENTER);
  loc = new PVector(50, 50);
  vel = new PVector(1, 3);
}

void draw(){
  background(0);
  loc.add(vel);
  pushMatrix();
  translate(loc.x, loc.y);
  rotate(vel.heading());
  fill(255);
  rect(0,0,30,10);
  rect(30,0, 50, 50);
  popMatrix();
  if(loc.x > width || loc.x < 0){
    vel.x *= -1;
  }
  if(loc.y > height || loc.y < 0){
    vel.y *= -1;
  }
}
