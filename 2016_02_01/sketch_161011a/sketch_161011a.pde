PVector v;

void setup() {
  size(500, 500);
  v = new PVector(200, 100);
}

void draw() {
  v.x = mouseX;
  v.y = mouseY;
  background(0);
  stroke(255);
  ellipse(v.x, v.y, 10, 10);
  line(0, 0, v.x, v.y);
  rect(0, height-30, v.mag(), 20);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(v.heading());
  rect(-50, -10, 100, 20);
  popMatrix();
}