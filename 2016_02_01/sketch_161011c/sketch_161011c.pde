ArrayList<Mover> m;

void setup() {
  size(600, 500);
  background(0);
  m = new ArrayList<Mover>();
}

void draw() {
  //background(0);
  fill(0, 15);
  noStroke();
  rect(0,0,width,height);
  
  for (int i=m.size()-1; i>=0; i--) {
    m.get(i).update();
    m.get(i).display();
  }
}

void keyPressed() {
  if (key == ' ') {
    for (int i=m.size()-1; i>=0; i--) {
      m.get(i).acceleration();
    }
  }
  if (key == 'k') {
    for (int i=m.size()-1; i>=0; i--) {
      m.get(i).deceleration();
    }
  }
  
  if (key == 's') {
    for (int i=m.size()-1; i>=0; i--) {
      m.get(i).freeze();
    }
  }
}

void mouseDragged() {
  m.add(new Mover(mouseX, mouseY));
}