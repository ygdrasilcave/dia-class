PVector v;
PVector u;

void setup(){
  size(600, 600);
  background(0);
  v = new PVector(width/2, height/2);
  u = new PVector(2*(width/3.0), 2*(height/3.0));
}

void draw(){
  background(0);
  stroke(255);
  strokeWeight(5);
  line(0, 0, v.x, v.y);
  strokeWeight(2);
  line(0, 0, u.x, u.y);
  float vm = v.mag();
  float um = u.mag();
  noFill();  
  rect(0, 0, vm, um);
}