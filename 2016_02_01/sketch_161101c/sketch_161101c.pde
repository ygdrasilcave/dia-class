Dot[] dots;
int num = 500;

boolean b = true;
boolean s = false;

IntList sel;

void setup() {
  size(800, 600);
  background(0);
  dots = new Dot[num];
  for (int i = 0; i<num; i++) {
    dots[i] = new Dot();
  }
  sel = new IntList();
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 65);
  rect(0, 0, width, height);

  for (int i=0; i<num; i++) {
    stroke(255);
    strokeWeight(1);

    float d = PVector.dist(dots[i].pos, dots[7].pos);
    if (d < map(mouseX, 0, width, 5, 100)) {
      if (b == false) {
        line(dots[i].pos.x, dots[i].pos.y, dots[7].pos.x, dots[7].pos.y);
      }
      sel.append(i);
    }
    if (s == true) {
      dots[i].display();
    }
  }
  if (b == true) {
    for (int i=0; i<sel.size()-1; i++) {
      strokeWeight(1);
      fill(255, 25);
      beginShape();
      vertex(dots[7].pos.x, dots[7].pos.y);
      vertex(dots[sel.get(i)].pos.x, dots[sel.get(i)].pos.y); 
      vertex(dots[sel.get(i+1)].pos.x, dots[sel.get(i+1)].pos.y);
      endShape();
      //line(dots[sel.get(i)].pos.x, dots[sel.get(i)].pos.y, 
      //  dots[sel.get(i+1)].pos.x, dots[sel.get(i+1)].pos.y);
    }
  }
  sel.clear();

  dots[7].update();
  fill(255);
  noStroke();
  ellipse(dots[7].pos.x, dots[7].pos.y, 20, 20);
}

void keyPressed() {
  if (key == ' ') {
    b = !b;
  }
  if (key == 's') {
    s = !s;
  }
}