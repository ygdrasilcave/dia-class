IntList randomDrunk;

void setup() {
  size(100, 100);
  randomDrunk = new IntList();

  for (int i=0; i<10; i++) {
    randomDrunk.set(i, i+1);
  }
  randomDrunk.shuffle();
}

void draw() {
}

void keyPressed() {
  if (key == 'n') {
    randomDrunk.clear();
    int num = int(random(2, 100));
    for (int i=0; i<num ; i++) {
      randomDrunk.set(i, i+1);
    }
  }
  if (key == ' ') {
    randomDrunk.shuffle();
    println(randomDrunk);
  }
  
}