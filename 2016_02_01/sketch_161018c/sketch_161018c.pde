float cellSizeW, cellSizeH;
int cols = 10;
int rows = 10;

void setup() {
  size(600, 600);
  background(0);
  cellSizeW = width/cols;
  cellSizeH = height/rows;
}

void draw() {
  background(0);
  stroke(255);
  strokeWeight(2);

  PVector m = new PVector(mouseX, mouseY);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      PVector t = new PVector(cellSizeW/2.0 + cellSizeW*x, cellSizeH/2.0 + cellSizeH*y);
      PVector a = PVector.sub(m, t);
      pushMatrix();
      translate(cellSizeW/2.0 + cellSizeW*x, cellSizeH/2.0 + cellSizeH*y);
      rotate(a.heading());

      if (a.mag() < cellSizeW/2.0) {
        line(0, 0, a.mag(), 0);
      } else {
        line(0, 0, cellSizeW/2.0, 0);
      }
      
      //line(0, 0, cellSizeW/2.0, 0);
      
      fill(255);
      ellipse(0, 0, 5, 5);
      
      noFill();
      ellipse(0, 0, cellSizeW, cellSizeH);
      popMatrix();
    }
  }
}