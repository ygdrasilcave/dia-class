const int dataNum = 3;
int index = 0;
int rawData[dataNum];

const int led_00 = 5;
const int led_01 = 6;
const int led_02 = 9;

const int minVal = 0;
const int maxVal = 255;

void setup()
{
  Serial.begin(9600);
  for(int i=0; i<dataNum; i++){
    rawData[i] = 0;
  }
}

void loop()
{
  if ( Serial.available()) {
    if(index == 0){
      for (index; index < dataNum; index ++)
      {
        rawData[index] = Serial.parseInt();
        rawData[index] = constrain(rawData[index], minVal, maxVal);
      }
      analogWrite(led_00, rawData[0]);
      analogWrite(led_01, rawData[1]);
      analogWrite(led_02, rawData[2]);


      Serial.print('H'); //.......................................header
      Serial.print(",");
      Serial.print(rawData[0]);
      Serial.print(',');
      Serial.print(rawData[1]);
      Serial.print(',');
      Serial.println(rawData[2]);
    }
    if (Serial.read() == '\n' && index == dataNum) {
      index = 0;
      delay(10);
    }
  }
}



