int value0_00, value0_01, value0_02;
int value1_00, value1_01, value1_02;

void setup(){
  Serial.begin(9600);
}

void loop(){
  value0_00 = analogRead(0);
  value0_01 = 100;
  value0_02 = 1000;
  value1_00 = analogRead(1);
  value1_01 = 200;
  value1_02 = 2000;
  
  Serial.print('H'); //.......................................header
  Serial.print(",");
  Serial.print(value0_00);
  Serial.print(",");
  Serial.print(value0_01);
  Serial.print(",");
  Serial.print(value0_02);
  Serial.println();
  delay(10);
  Serial.print('F'); //........................................footer
  Serial.print(",");
  Serial.print(value1_00);
  Serial.print(",");
  Serial.print(value1_01);
  Serial.print(",");
  Serial.print(value1_02);
  Serial.println();
  delay(10);
}

