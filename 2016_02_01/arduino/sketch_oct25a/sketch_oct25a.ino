const int sensorPin = 0;
const int ledPin = 9;
int sensorVal = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  sensorVal = analogRead(sensorPin);
  analogWrite(ledPin, map(sensorVal, 0, 1023, 0, 255));
  Serial.println(sensorVal);
  delay(10);
}

