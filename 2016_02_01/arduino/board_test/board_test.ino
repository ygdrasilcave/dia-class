const int sw1 = 7;
const int sw2 = 8;
const int led1 = 5;
const int led2 = 6;
const int led3 = 9;
const int pot1 = 0;
const int pot2 = 1;

void setup() {
  pinMode(sw1, INPUT_PULLUP);
  pinMode(sw2, INPUT_PULLUP);
  pinMode(led3, OUTPUT);
}

void loop() {
  analogWrite(led1, analogRead(pot1) / 4.0);
  analogWrite(led2, analogRead(pot2) / 4.0);
  if (digitalRead(sw1) == 0 && digitalRead(sw2) == 0) {
    digitalWrite(led3, HIGH);
  }else{
    digitalWrite(led3, LOW);
  }
  delay(10);
}
