const int ledPin = 5;
int rawData = 0;

void setup() {
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) {
    rawData = Serial.read();
    if (rawData == 'H') {
      digitalWrite(ledPin, HIGH);
    }else if (rawData == 'L') {
      digitalWrite(ledPin, LOW);
    }else{
      Serial.println("Wrong message! enter H or L");
    }
  }
}



