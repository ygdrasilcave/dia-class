Ball b1;
Ball b2;

void setup(){
  size(600, 600);
  background(0);
  b1 = new Ball();
  b2 = new Ball();
}

void draw(){
}

void keyPressed(){
  if(key == 'a'){
    b1.printMsg();
  }
  if(key == 'b'){
    b2.printMsg();
  }
}

class Ball{
  int counter = 0;
  
  Ball(){
    println("I am a Ball");
  }
  
  void printMsg(){
    println("ball " + counter);
    counter++;
  }
}