class Dot {
  PVector pos;
  PVector vel;

  float t1;
  float t2;

  int type;

  float w;

  boolean baby;

  Dot() {
    pos = new PVector(random(width), random(height));
    vel = new PVector(random(-2, 2), random(-2, 2));
    t1 = 0.0;
    t2 = 0.0;
    type = int(random(0, 2))+1;
    w = 5.0;
    baby = true;
  }

  Dot(float _x, float _y, int _type) {
    pos = new PVector(_x, _y);
    vel = new PVector(random(-2, 2), random(-2, 2));
    t1 = 0.0;
    t2 = 0.0;
    type = _type;
    w = 5.0;
    baby = true;
    noiseSeed(minute()+second());
  }

  void update() {

    if (type == 1) {
      pos.add(vel);
    } else if (type == 2) {
      //vel = PVector.random2D().mult(2);
      vel.x = map(noise(t1), 0.0, 1.0, -2.0, 2.0);
      vel.y = map(noise(t2), 0.0, 1.0, -2.0, 2.0);
      t1 += 0.032;
      t2 += 0.046;
      pos.add(vel);
    }

    if (pos.x > width) {
      pos.x = 0;
    } else if (pos.x < 0) {
      pos.x = width;
    }
    if (pos.y > height) {
      pos.y = 0;
    } else if (pos.y < 0) {
      pos.y = height;
    }
  }

  void display() {
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(vel.heading());
    if (baby == true) {
      if (type == 1) {
        stroke(255, 255, 0);
        strokeWeight(w);
      } else if (type == 2) {
        stroke(0, 255, 255);
        strokeWeight(w);
      }
      point(0, 0);
      strokeWeight(2);
      line(0, 0, w, 0);
    }else{
      strokeWeight(1);
      stroke(255, 0, 0);
      noFill();
      ellipse(0, 0, w, w);
      strokeWeight(2);
      line(0, 0, w, 0);
    }
    popMatrix();
  }

  void grow() {
    w += 5.0;
    if (w > 100) {
      w = 100;
      baby = false;
    }
  }
}