ArrayList<Dot> dots;

void setup() {
  size(800, 600);
  dots = new ArrayList<Dot>();
  background(0);
}

void draw() {
  background(0);

  for (int i=dots.size()-1; i>=0; i--) {

    int other = 0;
    boolean colision = false;

    for (int j=dots.size()-1; j>=0; j--) {
      if (i != j) {
        PVector pos1 = dots.get(i).pos.copy();
        PVector pos2 = dots.get(j).pos.copy();
        float dist = PVector.dist(pos1, pos2);
        if (dist < 100) {
          stroke(255);
          strokeWeight(0.5);
          line(pos1.x, pos1.y, pos2.x, pos2.y);
        }
        if (dist < 10 && colision == false) {
          colision = true;
          if (dots.get(i).w >= dots.get(j).w) {
            other = j;
            dots.get(i).grow();
          }else{
            other = i;
            dots.get(j).grow();
          }
        }
      }
    }

    dots.get(i).update();
    dots.get(i).display();
    
    if(colision == true){
      dots.remove(other);
    }
  }

  fill(255);
  noStroke();
  text(dots.size(), 50, 50);
}

void mousePressed() {
  if (mouseButton == LEFT) {
    dots.add(new Dot(mouseX, mouseY, 1));
  } else if (mouseButton == RIGHT) {
    dots.add(new Dot(mouseX, mouseY, 2));
  }
}