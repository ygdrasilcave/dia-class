Dot[] dots;
int num;

boolean s = false;

IntList sel;

void setup() {
  size(800, 800);
  background(0);
  num = 2500;
  dots = new Dot[num];
  for (int y=0; y<50; y++) {
    for (int x=0; x<50; x++) {
      int index = x + y*50;
      PVector _v = new PVector((width/50)*x, (height/50)*y);
      dots[index] = new Dot(_v);
    }
  }
  sel = new IntList();
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 65);
  rect(0, 0, width, height);

  for (int i=0; i<num; i++) {
    stroke(255);
    strokeWeight(1);

    for (int j=0; j<sel.size(); j++) {
      float d = PVector.dist(dots[i].pos, dots[sel.get(j)].pos);
      if (d < map(mouseX, 0, width, 5, 100)) {
        line(dots[i].pos.x, dots[i].pos.y, dots[sel.get(j)].pos.x, dots[sel.get(j)].pos.y);
      }
    }

    if (s == true) {
      dots[i].display();
    }
  }

  for (int i=0; i<sel.size(); i++) {
    dots[sel.get(i)].update();

    for (int j=0; j<sel.size(); j++) {
      if (i != j) {
        float d = PVector.dist(dots[sel.get(i)].pos, dots[sel.get(j)].pos);
        if (d < 100) {
          strokeWeight(3);
          stroke(255, 255, 0);
          line(dots[sel.get(i)].pos.x, dots[sel.get(i)].pos.y, 
            dots[sel.get(j)].pos.x, dots[sel.get(j)].pos.y);
        }
      }
    }
    fill(255);
    noStroke();
    ellipse(dots[sel.get(i)].pos.x, dots[sel.get(i)].pos.y, 20, 20);
    noFill();
    stroke(255);
    strokeWeight(1.5);
    ellipse(dots[sel.get(i)].pos.x, dots[sel.get(i)].pos.y, 30, 30);
  }
}

void keyPressed() {
  if (key == 's') {
    s = !s;
  }
  if (key == 'n') {
    sel.append(int(random(num)));
  }
  if (key == ' ') {
    sel.clear();
  }
}