Invader[] invaders;
int cols, rows;
float pixelSize;
int counter = 0;
boolean sel = true;
float targetX, targetY;
int dirX, dirY;

void setup() {  
  size(1000, 600);
  background(0);
  cols = 5;
  rows = 5;
  pixelSize = 7;
  invaders = new Invader[132];
  for (int i=0; i<invaders.length; i++) {
    //invaders[i] = new Invader(int(random(3, 10)), int(random(3, 10)), random(3, 10));
    invaders[i] = new Invader(cols, rows, pixelSize);
  }
  dirX = 1;
  dirY = 1;
}

void draw() {
  background(0);
  targetX += dirX*8;
  targetY += dirY*7;

  for (int i=0; i<invaders.length; i++) {
    invaders[i].display(50+cols*2*(pixelSize+2)*(i%11), 30+rows*(pixelSize+3)*(i/11));
  }

  if (frameCount % 2 == 0) {
    for (int i=0; i<invaders.length; i++) {
      if((targetX > (50+cols*2*(pixelSize+2)*(i%11))-30) && (targetX < (50+cols*2*(pixelSize+2)*(i%11))+30)){
        if((targetY > (30+rows*(pixelSize+3)*(i/11)-30)) && (targetY < (30+rows*(pixelSize+3)*(i/11)+30))){
          invaders[i].transform();
        }
      }
      
    }
  }
  
  if(targetX > width || targetX < 0){
    dirX *= -1;
  }
  if(targetY > height || targetY < 0){
    dirY *= -1;
  }
  
  fill(255);
  noStroke();
  ellipse(targetX, targetY, 20, 20);
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<invaders.length; i++) {
      invaders[i].transform();
    }
  }
}