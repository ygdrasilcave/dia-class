int rows, cols;
float w, h;
int[] colors;

void setup() {  
  size(1000, 600);
  background(0); 
  rows = 10;
  cols = 8;
  w = 15;
  h = 15;
  colors = new int[rows*cols];
  for (int i=0; i<colors.length; i++) {
    int _c = int(random(10));
    if (_c > 5) {
      colors[i] = 1;
    } else {
      colors[i] = 0;
    }
  }
}

void draw() {
  background(0);

  pushMatrix();
  translate(mouseX, mouseY);
  pushMatrix();
  translate(0, -(h*rows)*0.5);
  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      stroke(0);
      fill(colors[x+y*cols]*255);
      rect(x*w, y*h, w, h);
    }
  }
  popMatrix();

  pushMatrix();
  translate(-(w*cols), -(h*rows)*0.5);
  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      stroke(0);
      fill(colors[(cols-1-x)+y*cols]*255);
      rect(x*w, y*h, w, h);
    }
  }
  popMatrix();
  popMatrix();
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<colors.length; i++) {
      int _c = int(random(10));
      if (_c > 4) {
        colors[i] = 1;
      } else {
        colors[i] = 0;
      }
    }
  }
}