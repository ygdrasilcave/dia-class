class Dot{
  PVector pos;
  PVector speed;
  
  Dot(){
    pos = new PVector(random(width), random(height));
    speed = new PVector(random(1,2), random(1,2));
  }
  
  void update(){
    pos.add(speed);
    
    if(pos.x < 0 || pos.x > width){
      speed.x = speed.x*-1;
    }
    if(pos.y < 0 || pos.y > height){
      speed.y = speed.y*-1;
    }
  }
  
  void display(){
    stroke(255);
    strokeWeight(5);
    point(pos.x, pos.y);
  } 
}