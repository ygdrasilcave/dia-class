Dot[] dots;
int num = 200;

boolean b = true;

void setup() {
  size(800, 600);
  background(0);
  dots = new Dot[num];
  for (int i = 0; i<num; i++) {
    dots[i] = new Dot();
  }
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 65);
  rect(0, 0, width, height);

  for (int i=0; i<num; i++) {
    stroke(255);
    strokeWeight(1);
    if (b == true) {
      if (i>0) {
        line(dots[i-1].pos.x, dots[i-1].pos.y, dots[i].pos.x, dots[i].pos.y);
      }
      if (i == num-1) {
        line(dots[i].pos.x, dots[i].pos.y, dots[0].pos.x, dots[0].pos.y);
      }
    }

    for (int j=0; j<num; j++) {
      if (i != j) {
        float d = PVector.dist(dots[i].pos, dots[j].pos);
        if (d < map(mouseX, 0, width, 5, 100)) {
          line(dots[i].pos.x, dots[i].pos.y, dots[j].pos.x, dots[j].pos.y);
        }
      }
    }
    dots[i].update();
  }
}

void keyPressed() {
  if (key == ' ') {
    b = !b;
  }
}