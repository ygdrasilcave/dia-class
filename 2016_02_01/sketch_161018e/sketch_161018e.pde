PVector m;

void setup() {
  size(600, 600);

  m = new PVector(width/2, width/2);
  println(m);
  PVector v = new PVector(100, 100);
  //m.add(v); 
  //PVector a = PVector.add(m, v);
  PVector a = m.copy();
  a.add(v);
  println(m);
  println(a);
}

void draw(){
  PVector mouse = new PVector(mouseX, mouseY);
  
  float d = mouse.dist(m);
  println(d);
  
  PVector c = PVector.sub(mouse, m);
  background(0);
  translate(width/2, height/2);
  rotate(c.heading());
  stroke(255);
  line(0, 0, d, 0);
}