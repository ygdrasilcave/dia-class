ArrayList<Mover> m;

PVector gravity;
PVector wind;

void setup() {
  size(600, 500);
  background(0);
  m = new ArrayList<Mover>();
  gravity = new PVector(0, 0.85);
  wind = new PVector(0.25, 0);
}

void draw() {
  //background(0);
  fill(0, 15);
  noStroke();
  rect(0,0,width,height);
  
  for (int i=m.size()-1; i>=0; i--) {
    m.get(i).update(PVector.add(gravity, wind));
    m.get(i).display();
  }
}

void mouseDragged() {
  m.add(new Mover(mouseX, mouseY));
}

void keyPressed(){
  if(key == ' '){
    m.clear();
  }
}