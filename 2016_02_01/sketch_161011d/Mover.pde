class Mover {

  PVector pos;
  PVector speed;
  PVector acc;
  float w, h;

  Mover(float _x, float _y) {
    pos = new PVector(_x, _y);
    speed =  new PVector(0, 0);
    acc = new PVector(0, 0);
    w = random(10, 40);
    h = w/4.0;
  }

  void update(PVector _force) {
    acc.x = _force.x;
    acc.y = _force.y;
    speed.add(acc);
    pos.add(speed);

    if (pos.x < w/2.0) {
      pos.x = w/2.0;
      speed.x *= -1;
    }
    if (pos.x > width-w/2.0) {
      pos.x = width-w/2.0;
      speed.x *= -1;
    }
    if (pos.y < h/2.0) {
      pos.y = h/2.0;
      speed.y *= -1;
    }
    if (pos.y > height-h/2.0) {
      pos.y = height-h/2.0;
      speed.y *= -1;
    }
  }

  void display() {
    pushMatrix();
    translate(pos.x, pos.y);
    rotate(speed.heading());
    fill(255);
    rect(-w/2.0, -h/2.0, w, h);
    //ellipse(50, 0, 20, 20);
    popMatrix();
  }

}