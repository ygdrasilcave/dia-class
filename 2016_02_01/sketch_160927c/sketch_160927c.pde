Invader[] invaders;

void setup() {  
  size(1000, 600);
  background(0);
  invaders = new Invader[120];
  for (int i=0; i<invaders.length; i++) {
    invaders[i] = new Invader(5, 2, 5);
  }
}

void draw() {
  background(0);
  for (int i=0; i<invaders.length; i++) {
    invaders[i].display(50+80*(i%12), 30+60*(i/12));
  }
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<invaders.length; i++) {
      invaders[i].transform();
    }
  }
}