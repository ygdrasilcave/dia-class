class Invader {

  int rows, cols;
  float w, h;
  int[] colors;
  float x, y;

  Invader() {
    rows = int(random(3, 10));
    cols = int(random(3, 10));
    w = random(3, 10);
    h = w;
    colors = new int[rows*cols];
    transform();
    x = random(width);
    y = random(height);
  }
  
  Invader(int _rows, int _cols, float _w){
    rows = _rows;
    cols = _cols;
    w = _w;
    h = _w;
    colors = new int[rows*cols];
    transform();
    x = random(width);
    y = random(height);
  }

  void display(float _x, float _y) {
    pushMatrix();
    translate(_x, _y);
    pushMatrix();
    translate(0, -(h*rows)*0.5);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        stroke(0);
        fill(colors[x+y*cols]*255);
        rect(x*w, y*h, w, h);
      }
    }
    popMatrix();

    pushMatrix();
    translate(-(w*cols), -(h*rows)*0.5);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        stroke(0);
        fill(colors[(cols-1-x)+y*cols]*255);
        rect(x*w, y*h, w, h);
      }
    }
    popMatrix();
    popMatrix();
  }

  void transform() {
    for (int i=0; i<colors.length; i++) {
      int _c = int(random(10));
      if (_c > 4) {
        colors[i] = 1;
      } else {
        colors[i] = 0;
      }
    }
  }
}