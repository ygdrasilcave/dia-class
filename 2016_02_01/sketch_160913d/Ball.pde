class Ball {
  float x, y;
  float xSpeed, ySpeed;
  float xDir, yDir;
  float hue;
  float w;

  Ball() {
    x = random(width);
    y = random(height);
    xSpeed = random(2, 6);
    ySpeed = random(3, 7);
    xDir = 1;
    yDir = 1;
    hue = random(255);
    w = random(10, 20);
  }

  void update() {
    x += xDir*xSpeed;
    y += yDir*ySpeed;

    if (x > width-w/2) {
      x = width-w/2;
      xDir *= -1;
    }
    if (x < w/2) {
      x = w/2;
      xDir *= -1;
    }
    if (y > height-w/2) {
      y = height-w/2;
      yDir *= -1;
    }
    if (y < w/2) {
      y = w/2;
      yDir *= -1;
    }
  }

  void display(boolean b) {
    if (b == true) {
      noStroke();
      fill(hue, 255, 255);
      ellipse(x, y, w, w);
    }else{
      noFill();
      stroke(hue, 255, 255);
      ellipse(x, y, w, w);
    }
  }
}