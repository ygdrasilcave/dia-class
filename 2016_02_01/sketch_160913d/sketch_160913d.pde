Ball[] b;
boolean fillMode = true;

void setup() {
  size(600, 600);
  background(0);
  b = new Ball[1000];

  for (int i=0; i<b.length; i++) {
    b[i] = new Ball();
  }
  colorMode(HSB, 255, 255, 255);
}

void draw() {
  background(0);
  for (int i=0; i<b.length; i++) {
    //b[i].update();
    b[i].display(fillMode);
  }
}

void keyPressed() {
  if (key == ' ') {
    fillMode = !fillMode;
  }
  if (key == 'r') {
    for (int i=0; i<b.length; i++) {
      b[i].update();
    }
  }
}