import processing.serial.*;
Serial myPort;
char header = 'H';
char footer = 'F';

int[] data1;
int[] data2;

void setup() {
  size(600, 500);  
  println(Serial.list());
  //myPort = new Serial(this, Serial.list()[1], 9600);
  myPort = new Serial(this, "COM41", 9600);
  myPort.bufferUntil('\n');
  data1 = new int[3];
  data2 = new int[3];
  for (int i=0; i<3; i++) {
    data1[i] = 0;
    data2[i] = 0;
  }
}


void draw() {
  background(0);
  fill(map(data1[0], 0, 1023, 0, 255), 0, 0);
  rect(0, 0, width/2, height);
  fill(0, map(data2[0], 0, 1023, 0, 255), 0);
  rect(width/2, 0, width/2, height);

  fill(255);
  textAlign(CENTER, CENTER);
  textSize(30);
  for (int i=0; i<3; i++) {
    text(data1[i], width/4, (i+1)*height/4);
    text(data2[i], 3*width/4, (i+1)*height/4);
  }
}

void serialEvent(Serial myPort)
{
  String rawData = myPort.readStringUntil('\n');
  if (rawData != null)
  {
    rawData = trim(rawData);
    println(rawData);
    String [] data = rawData.split(",");
    //println("size of data = " + data.length);
    if (data[0].charAt(0) == header && data.length > 3)
    {
      for ( int i = 1; i < data.length; i++)
      {
        //println("Value " + i + " = " + data[i]);
        data1[i-1] = int(trim(data[i]));
      }
    } else if (data[0].charAt(0) == footer && data.length > 3) {
      for ( int i = 1; i < data.length; i++)
      {
        //println("Value " + i + " = " + data[i]);
        data2[i-1] = int(trim(data[i]));
      }
    }
  }
}