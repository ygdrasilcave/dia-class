import processing.serial.*;
Serial myPort;

int bar_x = 0;
float data = 0;

void setup () {
  size(640, 480);
  //println(Serial.list());
  myPort = new Serial(this, Serial.list()[3], 9600);
  //myPort = new Serial(this, "COM41", 9600);
  myPort.bufferUntil('\n');
  background(0);
}


void draw () {
  if (data > (height/3)*2) {
    stroke(255, 0, 0);
  }
  else if (data > height/3 && data <= (height/3)*2) {
    stroke(255, 255, 0);
  }
  else {
    stroke(0, 255, 0);
  }
  line(bar_x, height, bar_x, height - data);
  
  if (bar_x >= width) {
    bar_x = 0;
    background(0);
  } 
  else {
    bar_x++;
  }
}

void serialEvent (Serial myPort) {
  String rawData = myPort.readStringUntil('\n');
  if (rawData != null) {
    data = float(trim(rawData)); 
    data = map(data, 0, 1023, 0, height);
  }
}