import processing.serial.*;
Serial myPort;
char header = 'H';
int dataNum = 3;

void setup() {
  size(600, 600);
  println(Serial.list());
  myPort = new Serial(this, Serial.list()[3], 9600);  
  //myPort  = new Serial(this, "COM3", 9600);
}

void draw() {
  background(120);
  noStroke();
  fill(map(mouseX, 0, width, 0, 255));
  ellipse(mouseX, mouseY, 80, 80);
  myPort.write(int(map(mouseX, 0, width, 0, 255)) + " " + int(map(mouseY, 0, height, 0, 255)) + " " + int(random(255)) + "\n");
}


void serialEvent(Serial myPort )
{
  String rawData = myPort.readStringUntil('\n');
  if (rawData != null)
  {
    rawData = trim(rawData);
    println(rawData);
    String [] data = rawData.split(",");
    println("size of data = " + data.length);
    if (data[0].charAt(0) == header && data.length > dataNum)
    {
      for ( int i = 1; i < data.length; i++)
      {
        println("Value " + i + " = " + data[i]);
      }
    }
  }
}