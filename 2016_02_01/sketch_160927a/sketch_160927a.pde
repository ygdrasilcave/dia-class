Invader[] invaders;
int cols, rows;
float pixelSize;
int counter = 0;
boolean sel = true;

void setup() {  
  size(1000, 600);
  background(0);
  cols = 5;
  rows = 5;
  pixelSize = 7;
  invaders = new Invader[132];
  for (int i=0; i<invaders.length; i++) {
    //invaders[i] = new Invader(int(random(3, 10)), int(random(3, 10)), random(3, 10));
    invaders[i] = new Invader(cols, rows, pixelSize);
  }
}

void draw() {
  background(0);

  for (int i=0; i<invaders.length; i++) {
    invaders[i].display(50+cols*2*(pixelSize+2)*(i%11), 30+rows*(pixelSize+3)*(i/11));
  }

  if (frameCount % 5 == 0) {
    //for (int i=0; i<invaders.length; i++) {
    invaders[counter].transform();
    invaders[counter].select = sel;
    counter++;
    if (counter > invaders.length-1) {
      counter = 0;
      sel = !sel;
    }
    //}
  }
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<invaders.length; i++) {
      invaders[i].transform();
    }
  }
}