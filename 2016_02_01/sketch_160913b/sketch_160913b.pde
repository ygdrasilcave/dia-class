Ball b1;

void setup(){
  size(600, 600);
  background(0);
  b1 = new Ball();
}

void draw(){
}

void keyPressed(){
  if(key == ' '){
    b1.printMsg();
  }
}

class Ball{
  int counter = 0;
  
  Ball(){
    println("I am a Ball");
  }
  
  void printMsg(){
    println("ball " + counter);
    counter++;
  }
}