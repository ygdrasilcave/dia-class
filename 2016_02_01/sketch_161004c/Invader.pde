class Invader {

  int rows, cols;
  float w, h;
  int[] colors;
  float x, y;
  boolean toDie = false;
  int age = 5;

  Invader() {
    rows = int(random(3, 10));
    cols = int(random(3, 10));
    w = random(3, 10);
    h = w;
    colors = new int[rows*cols];
    transform();
    x = random(width);
    y = random(height);
    age = int(random(2, 10));
  }
  
  Invader(float _x, float _y) {
    rows = int(random(3, 10));
    cols = int(random(3, 10));
    w = random(3, 10);
    h = w;
    colors = new int[rows*cols];
    transform();
    x = _x;
    y = _y;
    age = int(random(2, 10));
  }
  
  Invader(int _rows, int _cols, float _w){
    rows = _rows;
    cols = _cols;
    w = _w;
    h = _w;
    colors = new int[rows*cols];
    transform();
    x = random(width);
    y = random(height);
    age = int(random(2, 10));
  }

  void display(float _x, float _y) {
    pushMatrix();
    translate(_x, _y);
    pushMatrix();
    translate(0, -(h*rows)*0.5);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        stroke(0);
        fill(colors[x+y*cols]*255);
        rect(x*w, y*h, w, h);
      }
    }
    popMatrix();

    pushMatrix();
    translate(-(w*cols), -(h*rows)*0.5);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        stroke(0);
        fill(colors[(cols-1-x)+y*cols]*255);
        rect(x*w, y*h, w, h);
      }
    }
    popMatrix();
    popMatrix();
  }
  
  void display() {
    pushMatrix();
    translate(x, y);
    pushMatrix();
    translate(0, -(h*rows)*0.5);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        stroke(0);
        fill(colors[x+y*cols]*255);
        rect(x*w, y*h, w, h);
      }
    }
    popMatrix();

    pushMatrix();
    translate(-(w*cols), -(h*rows)*0.5);
    for (int y=0; y<rows; y++) {
      for (int x=0; x<cols; x++) {
        stroke(0);
        fill(colors[(cols-1-x)+y*cols]*255);
        rect(x*w, y*h, w, h);
      }
    }
    popMatrix();
    popMatrix();    
  }
  
  void displayAge(){
    fill(255);
    noStroke();
    textAlign(CENTER, CENTER);
    textSize(24);
    text(age, x, y + w*rows*0.5 + 20);
  }

  void transform() {
    for (int i=0; i<colors.length; i++) {
      int _c = int(random(10));
      if (_c > 4) {
        colors[i] = 1;
      } else {
        colors[i] = 0;
      }
    }
  }
  
  void kill(float _x, float _y){
    if(_x > x-w*cols && _x < x+w*cols){
      if(_y > y-w*rows*0.5 && _y < y+w*rows*0.5){
        //toDie = true;
        age--;
        transform();
        if(age <= 0){
          toDie = true;
        }
      }
    }
  }
}