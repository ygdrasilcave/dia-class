Flower[] fl;
int petals = 1;

void setup() {
  size(600, 600);
  fl = new Flower[25];
  for (int i=0; i<fl.length; i++) {
    //fl[i] = new Flower(petals, 60, 20);
    fl[i] = new Flower();
  }
}

void draw() {
  background(0);
  for (int y=0; y < 5; y++) {
    for (int x=0; x < 5; x++) {
      int index = x + y*5;
      fl[index].display(x*100+100, y*100+100);
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<fl.length; i++) {
      fl[i].update();
    }
    petals++;
    if (petals > 30) {
      petals = 1;
    }
  }
  if (key == 'g') {
    for (int i=0; i<fl.length; i++) {
      fl[i].petal++;
      if (fl[i].petal > 30) {
        fl[i].petal = 1;
      }
    }
  }
}