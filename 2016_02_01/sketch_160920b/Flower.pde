class Flower {
  float t = 0.0;
  int petal;
  float w, h;

  Flower(int _num, float _w, float _h) {
    petal = _num;
    w = _w;
    h = _h;
  }
  
  Flower() {
    petal = int(random(2, 30));
    w = random(20, 70);
    h = w/3.0;
  }
  
  void update(){
    t+=0.035;
  }

  void display(float x, float y) {
    for (int i=0; i<petal; i++) {
      pushMatrix();
      translate(x, y);
      rotate(radians((360/petal)*i));
      noStroke();
      fill(255);
      ellipse(abs(sin(t))*w*2, 0, w, h);
      popMatrix();
    }
  }
}