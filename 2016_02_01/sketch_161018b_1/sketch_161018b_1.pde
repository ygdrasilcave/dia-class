PVector c;

void setup(){
  size(600, 600);
  c = new PVector(width/2, height/2);
}

void draw(){
  background(0);
  
  PVector m = new PVector(mouseX, mouseY);
  stroke(255);
  strokeWeight(5);
  
  PVector a = PVector.sub(m, c);
  
  pushMatrix();
  rotate(m.heading());
  line(0, 0, 100, 0);
  popMatrix();
  
  pushMatrix();
  translate(c.x, c.y);
  rotate(a.heading());
  line(0, 0, 100, 0);
  popMatrix();
  
  PVector u = new PVector(500, 300);
  PVector b = PVector.sub(m, u);
  pushMatrix();
  translate(u.x, u.y);
  rotate(b.heading());
  line(0, 0, 100, 0);
  popMatrix();
}