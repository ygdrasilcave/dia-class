class Flower {

  int petal;
  float w, h;
  float t;
  String txt;

  Flower() {
    petal = int(random(3, 20));
    w = random(30, 60);
    h = w/3;
    t = 0.0;
    txt = "flower";
  }

  Flower(int _petal, float _w, float _h, String _txt) {
    petal = _petal;
    w = _w;
    h = _h;
    t = 0.0;
    txt = _txt;
  }

  void update() {
    t += 0.025;
  }

  void grow() {
    petal++;
    if (petal > 20) {
      petal = 1;
    }
  }

  void display(float _x, float _y) {
    for (int i=0; i<petal; i++) {
      pushMatrix();
      translate(_x, _y);
      rotate(radians((360/petal)*i));
      fill(255);
      noStroke();
      //ellipse(abs(sin(t))*100, 0, w, h);
      textSize(w/3);
      text(txt, abs(sin(t))*100, 0);
      popMatrix();
    }
  }
}