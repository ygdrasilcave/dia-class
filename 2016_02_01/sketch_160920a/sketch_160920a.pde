PFont font;
Flower[] fl;

void setup() {
  size(600, 600);
  background(0);
  font = createFont("Georgia", 32);
  textFont(font);
  textAlign(CENTER, CENTER);
  fl = new Flower[25];
  for (int i=0; i<fl.length; i++) {
    //if (i%2 == 0) {
      //fl[i] = new Flower();
    //} else {
      fl[i] = new Flower(5, 60, 20, "Hello");
    //}
  }
}

void draw() {
  background(0);

  for (int y=0; y<5; y++) {
    for (int x=0; x<5; x++) {
      int index = x + y*5;

      fl[index].display(x*100+100, y*100+100);
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<fl.length; i++) {
      fl[i].update();
    }
  }

  if (key == 'g') {
    for (int i=0; i<fl.length; i++) {
      fl[i].grow();
    }
  }
}