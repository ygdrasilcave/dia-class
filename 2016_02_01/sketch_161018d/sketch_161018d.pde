float cellSizeW, cellSizeH;
int cols = 20;
int rows = 20;

void setup() {
  size(600, 600);
  background(0);
  cellSizeW = width/cols;
  cellSizeH = height/rows;
}

void draw() {
  background(0);
  //stroke(255);
  strokeWeight(2);

  PVector m = new PVector(mouseX, mouseY);

  for (int y=0; y<rows; y++) {
    for (int x=0; x<cols; x++) {
      PVector t = new PVector(cellSizeW/2.0 + cellSizeW*x, cellSizeH/2.0 + cellSizeH*y);
      PVector a = PVector.sub(m, t);
      pushMatrix();
      translate(cellSizeW/2.0 + cellSizeW*x, cellSizeH/2.0 + cellSizeH*y);
      rotate(a.heading());

      float c = map(a.mag(), 0, 250, 255, 0);
      
      stroke(c);
      if (a.mag() < cellSizeW/2.0-5) {
        //line(0, 0, a.mag(), 0);
        fill(c);
        ellipse(a.mag(), 0, 10, 10);
      } else {
        //line(0, 0, cellSizeW/2.0, 0);
        fill(c);
        ellipse(cellSizeW/2.0-5, 0, 10, 10);
      }
      
      noFill();
      ellipse(0, 0, cellSizeW, cellSizeH);
      popMatrix();
    }
  }
}