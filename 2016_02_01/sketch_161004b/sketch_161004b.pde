ArrayList<Invader> invaders;

void setup() {
  size(1000, 600);
  background(0);
  invaders = new ArrayList<Invader>();
}

void draw() {
  background(0);
  for (int i=invaders.size()-1; i>=0; i--) {
    invaders.get(i).display();
  }
  println(invaders.size());
}

void keyPressed() {
  if (invaders.size() > 0) {
    int index = int(random(invaders.size()));
    invaders.remove(index);
    println(invaders.size());
  }
}

void mouseClicked() {
  if (mouseButton == LEFT) {
    invaders.add(new Invader(mouseX, mouseY));
  } else if (mouseButton == RIGHT) {
    for (int i=invaders.size()-1; i>=0; i--) {
      invaders.get(i).kill(mouseX, mouseY);
      if(invaders.get(i).toDie == true){
        invaders.remove(i);
      }
    }
  }
}