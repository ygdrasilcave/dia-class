PVector pos;
PVector speed;
PVector acc;

void setup(){
  size(600, 500);
  background(0);
  pos = new PVector(width/2, height/2);
  speed =  new PVector(0, 0);
  acc = new PVector(0, 0);
}

void draw(){
  background(0);

  speed.add(acc);
  pos.add(speed);
  
  translate(pos.x, pos.y);
  rotate(speed.heading());
  rect(-30, -10, 60, 20);
  
  if(pos.x < 30){
    pos.x = 30;
    speed.x *= -1;
  }
  if(pos.x > width-30){
    pos.x = width-30;
    speed.x *= -1;
  }
  if(pos.y < 10){
    pos.y = 10;
    speed.y *= -1;
  }
  if(pos.y > height-10){
    pos.y = height-10;
    speed.y *= -1;
  }
}

void keyPressed(){
  if(key == ' '){
    acc.x = 0.125;
    acc.y = 0.25;
  }
  if(key == 'k'){
    acc.x = 0;
    acc.y = 0;
  }
}