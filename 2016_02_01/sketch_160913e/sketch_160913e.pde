int petal = 12;
float w, h;
float t = 0.0;

void setup() {
  size(600, 600);
  background(0);
  w = 60;
  h = 20;
}

void draw() {
  background(0);

  for (int y=0; y<5; y++) {
    for (int x=0; x<5; x++) {
      for (int i=0; i<petal; i++) {
        pushMatrix();
        translate(x*100+100, y*100+100);
        //translate(width/2, height/2);
        rotate(radians((360/petal)*i));
        fill(255);
        noStroke();
        ellipse(abs(sin(t))*100, 0, w, h);
        popMatrix();
      }
    }
  }

  t += 0.025;
}