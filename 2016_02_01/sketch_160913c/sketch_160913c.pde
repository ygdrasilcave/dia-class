Ball b;

void setup() {
  size(600, 600);
  background(0);
  b = new Ball();
}

void draw() {
  background(0);
  b.update();
  b.display();
}