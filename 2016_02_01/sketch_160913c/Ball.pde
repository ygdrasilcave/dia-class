class Ball {
  float x, y;
  float xSpeed, ySpeed;
  float xDir, yDir;

  Ball() {
    x = random(width);
    y = random(height);
    xSpeed = random(3, 8);
    ySpeed = random(4, 9);
    xDir = 1;
    yDir = 1;
  }

  void update() {
    x += xDir*xSpeed;
    y += yDir*ySpeed;

    if (x > width-10) {
      x = width-10;
      xDir *= -1;
    }
    if (x < 10) {
      x = 10;
      xDir *= -1;
    }
    if (y > height-10) {
      y = height-10;
      yDir *= -1;
    }
    if (y < 10) {
      y = 10;
      yDir *= -1;
    }
  }
  
  void display(){
    fill(255);
    ellipse(x, y, 20, 20);
  }
}