
PImage img;
float[] r;
float[] g;
float[] b;

int pixelSize = 15;

float speedX = 15;
float posX = 0;
float dirX = 1;

void setup() {
  img = loadImage("unnamed.jpg");
  size(img.width, img.height);

  r = new float[img.width*img.height];
  g = new float[img.width*img.height];
  b = new float[img.width*img.height];

  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      int index = x + y*img.width;
      color c = img.pixels[index];
      r[index] = red(c);
      g[index] = green(c);
      b[index] = blue(c);
      noStroke();
      fill(c);
      rect(x, y, 1, 1);
    }
  }
  img = null;
  background(0);
}

void draw() {
  background(map(posX, 0, width, 0, 255));
  for (int y=0; y<height; y+= pixelSize) {
    for (int x=0; x<width; x+= pixelSize) {
      int index = x + y*width;
      fill(r[index], g[index], b[index]);
      noStroke();
      float br = (r[index] + g[index] + b[index])/3;
      float val = map(posX, 0, width, 0, pixelSize);
      float size = map(br, 0, 255, val, pixelSize-val);
      ellipse(x+size/2, y+size/2, size, size);
    } 
  }
  
  posX += dirX*speedX;
  if(posX > width){
    dirX *= -1;
  }
  if(posX < 0){
    dirX *= -1;
  }
  speedX = map(mouseX, 0, width, 0, width/2);
}

