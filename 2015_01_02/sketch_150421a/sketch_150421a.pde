PImage img;

float w = 100;
float h = 100;
float r = 0;

void setup() {
  img = loadImage("lips_kiss_red.png");
  size(800, 600); 
  background(0);
}

void draw() {
  translate(mouseX, mouseY);
  rotate(r);
  tint(255, 10);
  image(img, -w/2, -h/2, w, h);
}

void keyPressed(){
  if(key == 'a'){
    w-=10;
    if(w<5){
      w = 5;
    }
  }else if(key == 'd'){
    w+=10;
    if(w > 200){
      w = 200;
    }
  }
  if(key == 'w'){
    h+=10;
    if(h > 200){
      h = 200;
    }
  }else if(key == 's'){
    h-=10;
    if(h < 5){
      h = 5;
    }
  }
  if(key == 'r'){
    r+=0.2;
  }
  if(key == ' '){
    background(0);
  }
}

