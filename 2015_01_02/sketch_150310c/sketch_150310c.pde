float x = 0;
float y = 0;
float speed = 20;
float t = 0.0;
float speed_t = 0.008;

void setup(){
  size(1000, 800);
  background(255);
}

void draw(){
  stroke(0);
  line(x+cos(t)*speed, y+sin(t)*speed, x+cos(t+PI)*speed, y+sin(t+PI)*speed);
  x = x+speed;
  if(x > width){
    x = 0;
    y = y+speed;
    if(y > height){
      y = 0;
      t = 0;
      speed = random(10, 100);
      speed_t = random(0.002, 0.05);
      background(255);
    }
  }
  t = t+speed_t;
}
