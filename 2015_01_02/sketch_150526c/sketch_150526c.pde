import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

PImage img;

int time;
boolean toggle = true;
int xCount = 0;
int yCount = 0;
boolean finish = false;

void setup() {
  img = loadImage("unnamed.jpg");
  size(img.width, img.height);
  //frameRate(25);
  oscP5 = new OscP5(this, 12000);
  myRemoteLocation = new NetAddress("127.0.0.1", 13000);
  background(0);
  time = millis();
}

void keyPressed() {
  background(0);
}

void draw() {
  //background(0);  
  
  if (time+50 < millis() && finish == false) {
    if (toggle == true) {
      sendData();
      toggle = false;
      time = millis();
      xCount+=10;

      if (xCount > img.width-1) {
        xCount = 0;
        yCount+=10;
        if (yCount > img.height-1) {
          yCount = 0;
          finish = true;
        }
      }

      //println("send");
    }
  } else {
    toggle = true;
  }
  image(img, 0, 0);
}

void sendData() {
  OscMessage myMessage = new OscMessage("/test");  
  myMessage.add(xCount);
  myMessage.add(yCount);
  myMessage.add(red(img.get(xCount, yCount)));
  myMessage.add(green(img.get(xCount, yCount)));
  myMessage.add(blue(img.get(xCount, yCount)));

  oscP5.send(myMessage, myRemoteLocation);
}

