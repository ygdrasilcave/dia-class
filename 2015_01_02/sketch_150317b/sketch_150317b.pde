float x;
float y;
float sx;
float sy;
float r;
float dirX = 1;
float dirY = 1;
float ax;
float ay;
float maxSX = 15;
float maxSY = 10;
int i = 0;
int j = 0;

void setup(){
  size(600, 600);
  background(0);
  x = width/2;
  y = height/2;
  sx = 0;
  sy = 0;
  r = 50;
  ax = random(0.6);
  ay = random(0.4);
  println(sx + " : " + sy);
}

void draw(){
  //background(0);
  fill(0, 10);
  noStroke();
  rect(0,0,width,height);
  stroke(255, 255, 0);
  noFill();
  /*fill(255);
  noStroke();*/
  ellipse(width/2, height/2, x, y);
  stroke(0, 255, 255);
  line(i, height, i, y);
  stroke(255, 0, 255);
  line(0, j, x, j);
  sx += ax;
  sy += ay;
  x = x+dirX*sx;
  y = y+dirY*sy;
  i++;
  j++;
  if(i > width){
    i = 0;
    background(0);
  }
  if(j > height){
    j = 0;
    background(0);
  }

  if(x > width-r/2){
    x = width-r/2;
    dirX = dirX*-1;
    sx = random(6);
    sy = random(10);
  }
  if(x < r/2){
    x = r/2;
    dirX = dirX*-1;
    sx = random(6);
    sy = random(10);
  }
  if(y > height-r/2){
    y = height-r/2;
    dirY *= -1;
    sx = random(6);
    sy = random(10);
  }
  if(y < r/2){
    y = r/2;
    dirY *= -1;
    sx = random(6);
    sy = random(10);
  }
}

void keyPressed(){
  if(key == ' '){
    sx = random(6);
    sy = random(10);
  }
}
