float RAD = 10.0;
int RADdir = 1;

float LAD = 10.0;
int LADdir = 1;

float mouth = 0;

float eyeballx;
float eyebally;
float eyeballsx;
float eyeballsy;
float eyeballdirx=1;
float eyeballdiry=1;

float bodyD = 0;

float noiseT = 0;

float RED = 0.0;
float LED = 0.0;

void setup() {
  size(600, 600);
  background(0);
  eyeballsx = random(0.2, 0.5);
  eyeballsy = random(0.3, 0.7);
}

void draw() {
  noStroke();
  background(0);
  fill(255);
  pushMatrix();
  //body
  translate(width/2, map(noise(noiseT), 0, 1, 100, height-100));
  translate(mouseX, mouseY);
  rotate(radians(bodyD));
  ellipse(0, 0, 150, 200);
  //head
  pushMatrix();
  translate(0, -150);
  ellipse(0, 0, 100, 100);
  pushMatrix();
  fill(0);
  translate(-15, 0);
  ellipse(0, 0, 20, 20);
  fill(255,0,255);
  ellipse(eyeballx, eyebally, 10,10);
  popMatrix();
  pushMatrix();
  translate(15, 0);
  fill(0);
  ellipse(0, 0, 20, 20);
  fill(255,0,255);
  ellipse(eyeballx, eyebally, 10,10);
  popMatrix(); 
  pushMatrix();
  translate(0, 20);
  fill(0);
  rect(0-25,0,50,mouth);
  popMatrix(); 
  popMatrix();
  //leftArm
  fill(255);
  pushMatrix();
  translate(-50, -70);
  rotate(radians(LAD+200));
  ellipse(60, 0, 120, 50);
  popMatrix();  
  //rightArm
  pushMatrix();  
  translate(50, -70);
  rotate(radians(RAD+300));
  ellipse(60, 0, 120, 50);
  popMatrix();  
  //leftLeg
  pushMatrix();
  translate(-60, 80);
  ellipse(0, 0, map(noise(noiseT),0,1,60,120), map(noise(noiseT),0,1,60,120));
  popMatrix();
  //rightLeg
  pushMatrix();
  translate(60, 80);
  ellipse(0, 0, map(noise(noiseT),0,1,60,120), map(noise(noiseT),0,1,60,120));
  popMatrix();
  //ear left
  pushMatrix();
  translate(-25, -190);
  rotate(radians(LED-90));
  ellipse(0, -50, 30, 100);
  popMatrix();

  //ear right
  pushMatrix();
  translate(25, -190);
  rotate(radians(RED-30));
  ellipse(0, -50, 30, 100);
  popMatrix();

  popMatrix();


  RAD += RADdir*2;
  if (RAD < 0) {
    RADdir*=-1;
  }
  if (RAD > 45) {
    RADdir*=-1;
  }

  LAD += LADdir*1;
  if (RAD < 0) {
    LADdir*=-1;
  }
  if (RAD > 45) {
    LADdir*=-1;
  }
  
  mouth += 3;
  if(mouth > 15){
    mouth = 0;
  }
  
  eyeballx += eyeballdirx*eyeballsx;
  eyebally += eyeballdiry*eyeballsy;
  if(eyeballx > 5){
    eyeballdirx *= -1;
  }
  if(eyeballx < -5){
    eyeballdirx *= -1;
  }
  if(eyebally > 5){
    eyeballdiry *= -1;
  }
  if(eyebally < -5){
    eyeballdiry *= -1;
  }
  
  bodyD += 1;
  noiseT += 0.04;
  
  LED = map(noise(noiseT), 0, 1, 0, 120);
  RED = map(noise(noiseT), 0, 1, 0, 160);
}

