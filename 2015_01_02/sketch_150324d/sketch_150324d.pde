float locX;
float locY;

float speedX;
float speedY;
float dirX;
float dirY;

float degree;
float speedDegree;
float dirDegree;

float w;
float h;
float speedW;
float speedH;
float dirW;
float dirH;

void setup() {
  size(600, 600);
  background(0);
  rectMode(CENTER);
  rectInit();
}

void draw() {
  //background(0);
  drawRect(random(255), random(255), random(255));
  if (mousePressed) {
    runRect2(50, 50);
  } else {
    runRect1(50, 50);
  }
  if(keyPressed){
    rectInit();
    background(0);
  }
}

void rectInit() {
  speedDegree = random(1, 3);
  dirDegree = 1;
  speedX = random(2, 4);
  speedY = random(3, 6);
  dirX = 1;
  dirY = 1;
  w = 10;
  h = 10;
  speedW = random(1, 2);
  speedH = speedW;
  dirW = 1;
  dirH = 1;
  locX = width/2;
  locY = height/2;
}

void drawRect(float _r, float _g, float _b) {
  pushMatrix();
  stroke(_r, _g, _b);
  noFill();
  translate(locX, locY);
  rotate(degree);
  rect(0, 0, w, h);
  popMatrix();
}

void runRect1(float _w, float _h) {
  locX += dirX*speedX;
  locY += dirY*speedY;

  w += dirW*speedW;
  h += dirH*speedH;

  if (locX < 0) {
    dirX *= -1;
  }
  if (locX > width) {
    dirX *= -1;
  }
  if (locY < 0) {
    dirY *= -1;
  }
  if (locY > height) {
    dirY *= -1;
  }

  if (w > _w) {
    w = 5;
  }
  if (h > _h) {
    h = 5;
  }

  degree += dirDegree*speedDegree;
}

void runRect2(float _w, float _h) {
  locX += dirX*speedX;
  locY += dirY*speedY;

  w += dirW*speedW;
  h += dirH*speedH;

  if (locX < 0) {
    locX = width;
  }
  if (locX > width) {
    locX = 0;
  }
  if (locY < 0) {
    locY = height;
  }
  if (locY > height) {
    locY = 0;
  }

  if (w > _w) {
    w = 5;
  }
  if (h > _h) {
    h = 5;
  }

  degree += dirDegree*speedDegree;
}

