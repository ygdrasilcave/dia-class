float d1 = 90.0;
float d2 = 0.0;

float speed1;
float speed2;

int dir1 = 1;
int dir2 = -1;

void setup(){
  size(600, 600);
  background(0);
  speed1 = random(1, 3);
  speed2 = random(3, 5);
}


void draw(){
  background(0);
  
  pushMatrix();
  translate(mouseX, mouseY);
  rotate(radians(d1));
  fill(255, 0, 255);
  rect(0-100, 0-100, 200, 200);
  stroke(255, 0, 255);
  strokeWeight(8);
  line(0, 0, 200, 0);
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(d2));
  fill(255, 255, 0);
  noStroke();
  rect(0-20, 0-20, 40, 40);
  popMatrix();
  
  d1 = d1 + dir1*speed1;
  if(d1 > 135){
    dir1 *= -1;
  }
  if(d1 < 45){
    dir1 *= -1;
  }
  d2 = d2 + dir2*speed2;
}
