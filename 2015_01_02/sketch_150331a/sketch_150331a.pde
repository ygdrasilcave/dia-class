void setup() {
  size(800, 800);
  rectMode(CENTER);
}

int num=10;

void draw() {
  background(255);
  for (int i=0; i<num; i++) {
    for (int j=0; j<num; j++) {
      drawSomething((i+j)%2, ((width/num)/2)+i*(width/num), 
      ((height/num)/2)+j*(height/num), 
      width/num, height/num, 10);
    }
  }
}

void drawSomething(int b, float _x, float _y, float _dw, float _dh, int _howMany) {
  float stepw = _dw/_howMany;
  float steph = _dh/_howMany;
  noFill();
  stroke(0);
  pushMatrix();
  translate(_x, _y);
  for (int i=1; i<_howMany+1; i++) {
    //fill(random(255), random(255), random(255));
    if (b == 0) {
      rect(0, 0, i*stepw, i*steph);
    } else {
      ellipse(0, 0, i*stepw, i*steph);
    }
  }
  popMatrix();
}

