float x;
float y;
float sx;
float sy;
float r;
float dirX = 1;
float dirY = 1;
float ax;
float ay;
float maxSX = 15;
float maxSY = 10;

void setup(){
  size(600, 600);
  background(0);
  x = width/2;
  y = height/2;
  sx = 0;
  sy = 0;
  r = 50;
  ax = random(0.6);
  ay = random(0.4);
  println(sx + " : " + sy);
}

void draw(){
  //background(0);
  fill(0, 10);
  noStroke();
  rect(0,0,width,height);
  stroke(255);
  noFill();
  /*fill(255);
  noStroke();*/
  ellipse(x, y, r, r);
  sx += ax;
  sy += ay;
  x = x+dirX*sx;
  y = y+dirY*sy;
  /*if(sx > maxSX){
    ax = 0;
  }
  if(sy > maxSY){
    ay = 0;
  }
  if(x > width){
    x = 0;
  }
  if(x < 0){
    x = width;
  }
  if(y > height){
    y = 0;
  }
  if(y < 0){
    y = height;
  }*/

  if(x > width-r/2){
    x = width-r/2;
    dirX = dirX*-1;
    sx = random(6);
    sy = random(10);
  }
  if(x < r/2){
    x = r/2;
    dirX = dirX*-1;
    sx = random(6);
    sy = random(10);
  }
  if(y > height-r/2){
    y = height-r/2;
    dirY *= -1;
    sx = random(6);
    sy = random(10);
  }
  if(y < r/2){
    y = r/2;
    dirY *= -1;
    sx = random(6);
    sy = random(10);
  }
}

void keyPressed(){
  if(key == ' '){
    sx = random(6);
    sy = random(10);
  }
}
