int rotState = 0;
int colState = 0;
boolean toggle = true;

int rectSize = 100;

void setup(){
  size(800, 800);
  rectMode(CENTER);
}

void draw(){
  drawRect(mouseX, mouseY, rectSize, rectSize, int(random(5, 20)), rotState);
}

void drawRect(float _x, float _y, float _w, float _h, int _howMany, int _b) {
  pushMatrix();
  //noFill();
  //stroke(255);

  noStroke();
  translate(_x, _y);
  for (int i=_howMany-1; i>-1; i--) {
    if (colState == 0) {
      fill(255-(255/_howMany*(i+1)));
    } else if (colState == 1) {
      fill((255/_howMany*(i+1)));
    } else if (colState == 2) {
      fill(random(255), random(255), random(255));
    }
    pushMatrix();
    if (_b == 1) {
      rotate((TWO_PI/_howMany)*(i+1));
    }
    rect(0, 0, (_w/_howMany)*(i+1), (_h/_howMany)*(i+1));
    popMatrix();
  }
  popMatrix();

}

void keyPressed() {
  if (key == ' ') {
    if (rotState == 0) {
      rotState = 1;
    } else {
      rotState = 0;
    }
  }
  if (key == 'i') {
    colState += 1;
    if (colState > 2) {
      colState = 0;
    }
  }
  if(keyCode == UP){
    rectSize++;
  }
  if(keyCode == DOWN){
    rectSize--;
    if(rectSize<5){
      rectSize = 5;
    }
  }
}
