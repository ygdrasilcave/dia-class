PImage img;
PImage img2;
float[] r;
float[] g;
float[] b;
float[] nr;
float[] ng;
float[] nb;

float pixelSize = 1;

void setup(){
  img = loadImage("unnamed.jpg");
  img2 = loadImage("scarlett.jpg");
  size(img.width, img.height);
  r = new float[img.width*img.height];
  g = new float[img.width*img.height];
  b = new float[img.width*img.height];
  nr = new float[img.width*img.height];
  ng = new float[img.width*img.height];
  nb = new float[img.width*img.height];
  for(int y=0; y<img.height; y++){
    for(int x=0; x<img.width; x++){
      int index = x + y*img.width;
      color c = img.pixels[index];
      r[index] = red(c);
      g[index] = green(c);
      b[index] = blue(c);
      color c2 = img2.pixels[index];
      nr[index] = red(c2);
      ng[index] = green(c2);
      nb[index] = blue(c2);
    }
  }
  img = null;
  img2 = null;
  background(0);
}

void draw(){
  background(0);
  for(int y=0; y<height; y+=pixelSize){
    for(int x=0; x<width; x+=pixelSize){
      int index = x+y*width;
      noStroke();
      fill((r[index]+nr[index])/2, (g[index]+ng[index])/2, (b[index]+nb[index])/2);
      rect(x, y, pixelSize, pixelSize);  
    }
  }
}
