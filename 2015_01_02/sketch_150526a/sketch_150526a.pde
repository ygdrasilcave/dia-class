import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

float mx, my;

void setup() {
  size(1000,1000);
  //frameRate(25);
  oscP5 = new OscP5(this,12000);
  myRemoteLocation = new NetAddress("127.0.0.1",12000);
  background(0);
}

void keyPressed(){
  background(0);
}

void draw() {
  //background(0);  
  fill(255);
  ellipse(mx, my, 30, 30);
}

void mouseMoved() {
  OscMessage myMessage = new OscMessage("/test");  
  //myMessage.add(123);
  myMessage.add(float(mouseX));
  myMessage.add(float(mouseY));
  //myMessage.add("some text");
  oscP5.send(myMessage, myRemoteLocation); 
}


void oscEvent(OscMessage theOscMessage) { 
  if(theOscMessage.checkAddrPattern("/test")==true) {
    if(theOscMessage.checkTypetag("ff")) {
      float firstValue = theOscMessage.get(0).floatValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      //print("### received an osc message /test with typetag ifs.");
      println(" values: "+firstValue+", "+secondValue);
      mx = firstValue;
      my = secondValue;
      return;
    }  
  } 
  //println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
}
