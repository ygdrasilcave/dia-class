float x = 0;
float y = 0;
float speedX = 23;
float speedY = 11;
float r = 50;

void setup() {
  size(800, 600);
  background(0);
  x = width/2;
  y = height/2;
}

void draw() {
  //fill(0, 10);
  //noStroke();
  //rect(0, 0, width, height);

  //fill(255);
  //noStroke();
  noFill();
  stroke(255);  
  ellipse(x, y, r, r);
  x = x + speedX;
  if (x > width-r/2) {
    x = width-r/2;
    speedX = speedX*-1;
  }
  if (x < r/2) {
    x = r/2;
    speedX = speedX*-1;
  }
  y = y + speedY;
  if (y > height-r/2) {
    y = height-r/2;
    speedY = speedY*-1;
  }
  if (y < r/2) {
    y = r/2;
    speedY = speedY*-1;
  }
}

