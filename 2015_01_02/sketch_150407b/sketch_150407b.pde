boolean gate = false;
float s = 10;
boolean toggle = true;

void setup() {
  size(600, 600);
  background(0);
}

void draw() {
  if (second()%2 == 0) {
    if (toggle == true) {
      println("event: " + second());
      s = 10;
      gate = true;
      toggle = false;
    }
  } else {
    background(0);
    toggle = true;
  }

  if (gate == true) {
    noFill();
    stroke(255, 0, 0);
    s += 10;
    if (s > 500) {
      s = 10;
      background(0);
      gate = false;
    }
    ellipse(width/2, height/2, s, s);
  } else {
    stroke(255);
    rect(width/2-150, height/2-150, 300, 300);
  }
}

void keyPressed() {
  if (key == ' ') {
  }
}

