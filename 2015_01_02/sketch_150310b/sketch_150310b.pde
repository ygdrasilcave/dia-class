float x = 0;
float y = 0;
float r = 80;
float speed = r/2;

void setup() {
  size(600, 400);
  background(255);
}

void draw() {
  noStroke();
  fill(255, 10);
  rect(0,0,width, height);
  noFill();
  stroke(0);
  ellipse(x, y, r, r);
  //rect(x-r/4, y-r/4, r/2, r/2);
  ellipse(x, y, r+10, r+10);
  x = x+speed;
  if(x > width){
    x = 0;
    y = y+speed;
    if(y > height){
      saveFrame("myWorks_####.jpg");
      background(255);
      y = 0;
      r = random(30, 100);
      speed = random(15, 50);
    }
  }
}

