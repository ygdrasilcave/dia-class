PImage img;
PImage img2;
float[] r;
float[] g;
float[] b;

float maxSize = 60;
float pixelSize;

float ballX;
float ballY;
float speedX = 5;
float speedY = 1;
float dirX = 1;
float dirY = 1;

float t = 0.0;
float ts = 0.06;

void setup(){
  img = loadImage("unnamed.jpg");
  img2 = loadImage("scarlett.jpg");
  size(img.width, img.height);
  r = new float[width*height];
  g = new float[width*height];
  b = new float[width*height];
  for(int y=0; y<height; y++){
    for(int x=0; x<width; x++){
      int index = x + y*width;
      color c = img.pixels[index];
      r[index] = red(c);
      g[index] = green(c);
      b[index] = blue(c);
    }
  }
  img = null;
  img2 = null;
  background(0);
}

void draw(){
  pixelSize = map(mouseY, 0, height, 5, maxSize);
  background(map(ballX, 0, width, 0, 255));
  for(int y=0; y<height; y+=pixelSize){
    for(int x=0; x<width; x+=pixelSize){
      int index = x+y*width;
      noFill();
      strokeWeight(3);
      stroke(r[index], g[index], b[index]);
      float mx = map(ballX, 0, width, 0, pixelSize);
      float br = (r[index]+g[index]+b[index])/3;
      float newSize = map(br, 0, 255, mx, pixelSize-mx);
      pushMatrix();
      translate(x, y);
      rotate(map(br, 0, 255, 0, TWO_PI)+t);
      line(0, 0, newSize, 0);
      popMatrix();
      
    }
  }
  
  t+=ts;
  
  ballX += dirX*speedX;
  ballY += dirY*speedY;
  
  if(ballX < 0 || ballX > width){
    dirX *= -1;
  }
  if(ballY < 0 || ballY > height){
    dirY *= -1;
  }
  fill(255, 0, 0);
  ellipse(ballX, height-50, 20, 20);
}
