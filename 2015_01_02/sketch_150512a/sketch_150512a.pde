
PImage img;
float[] r;
float[] g;
float[] b;

void setup() {
  img = loadImage("unnamed.jpg");
  size(img.width, img.height);

  r = new float[img.width*img.height];
  g = new float[img.width*img.height];
  b = new float[img.width*img.height];

  int counter = 0;
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      color c = img.get(x, y);
      r[counter] = red(c);
      g[counter] = green(c);
      b[counter] = blue(c);
      noStroke();
      fill(c);
      rect(x, y, 1, 1);
      counter++;
    }
  }
}

void draw() {
  int counter = 0;
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      fill(r[counter], g[counter], b[counter]);
      noStroke();
      rect(x, y, 1, 1);
      counter++;
    } 
  }
  
}

