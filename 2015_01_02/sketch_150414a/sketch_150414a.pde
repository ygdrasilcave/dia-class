import processing.sound.*;
SinOsc sine;

boolean a = false;
boolean aToggle = true;
int currTime = 0;
int duration = 500;

void setup() {
  size(600, 600);
  background(0);
  textSize(36);
  textAlign(CENTER, CENTER);
  
  sine = new SinOsc(this);
  sine.play();
  
}

void draw() {
  background(0);
  sine.freq(int(map(mouseY, 0, height, 80, 2000)));
  if (a == true) {    
    if (aToggle == true) {
      currTime = millis();
      aToggle = false;
    }
    fill(255, 0, 0);
    ellipse(width/2, height/2, 100, 100);
    fill(255);
    stroke(0);
    text("event #1: " + (millis()-currTime), width/2, height/2);
    
    sine.amp(1.0);
    
    if (aToggle == false && currTime+duration < millis()) {
      a = false;
      aToggle = true;
    }
  } else {
    fill(0);
    stroke(255);
    ellipse(width/2, height/2, 100, 100);
    fill(255);
    stroke(0);
    text("default event", width/2, height/2);
    
    sine.amp(0.0);
  }
  
  fill(255);
  text("duration: " + duration + " milliseconds", width/2, height-100);

  /*if (frameCount % 180 == 0) {
    if (a == false) {
      a = true;
    }
  }
  */
  duration = int(map(mouseX, 0, width, 10, 3000));
}

void keyPressed() {
  if (key == 'a') {
    if (a == false) {
      a = true;
    }
  }
}

