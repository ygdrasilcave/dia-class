PImage img;
float[] r;
float[] g;
float[] b;

float pixelSize = 10;

void setup() {
  img = loadImage("unnamed.jpg");
  size(img.width*2, img.height);
  r = new float[img.width*img.height];
  g = new float[img.width*img.height];
  b = new float[img.width*img.height];
  for (int y=0; y<img.height; y++) {
    for (int x=0; x<img.width; x++) {
      int index = x + y*img.width;
      color c = img.pixels[index];
      r[index] = red(c);
      g[index] = green(c);
      b[index] = blue(c);
    }
  }
  img = null;
  background(0);
  textSize(pixelSize);
}

void draw() {
  pixelSize = map(mouseX, 0, width, 5, 50);
  textSize(pixelSize);
  background(0);
  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width/2; x+=pixelSize) {
      int index = x+y*width/2;
      noStroke();
      if (r[index] > g[index] && r[index] > b[index]) {
        fill(r[index], g[index], b[index]);
        text("R", x, y);
      } else if (g[index] > r[index] && g[index] > b[index]) {
        fill(r[index], g[index], b[index]);
        text("G", x, y);
      } else if (b[index] > r[index] && b[index] > g[index]) {
        fill(r[index], g[index], b[index]);
        text("B", x, y);
      } else if (g[index] == b[index] && b[index] == r[index] && r[index] == g[index]) {
        if ((r[index] + g[index] + b[index])/3 > 128) {
          fill(r[index], g[index], b[index]);
          text("W", x, y);
        }
      } else if (r[index] == g[index]) {
        fill(r[index], g[index], b[index]);
        text("Y", x, y);
      } else if (r[index] == b[index]) {
        fill(r[index], g[index], b[index]);
        text("M", x, y);
      } else if (g[index] == b[index]) {
        fill(r[index], g[index], b[index]);
        text("C", x, y);
      }

      fill(r[index], g[index], b[index]);
      rect(width/2+x, y, pixelSize, pixelSize);
    }
  }
}

