float t = 0.0;

void setup(){
  size(300, 300);
  background(0);
}

void draw(){
  background(0);
  float x = random(1)*width;
  ellipse(x, 100, 20, 20);
  
  float x2 = noise(t)*width;
  ellipse(x2, 200, 20, 20);
  t+=0.01;
}
