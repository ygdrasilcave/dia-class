float s = 10;

boolean gate = false;

void setup() {
  size(600, 600);
  background(0);
}

void draw() {
  if (gate == true) {
    noFill();
    stroke(255, 0, 0);
    s += 10;
    if (s > 500) {
      s = 10;
      background(0);
      gate = false;
    }
  } else {
    stroke(255);
  }
  ellipse(width/2, height/2, s, s);
}

void keyPressed() {
  if (key == ' ') {
    if (gate == false) {
      gate = true;
      s = 10;
    }
  }
}

