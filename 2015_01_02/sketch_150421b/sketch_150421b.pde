PImage img;
int pixelSize = 1;
float posX=30;
int dir = 1;

void setup() {
  img = loadImage("images.jpg");
  size(img.width, img.height); 
  background(0);
}

void draw() {
  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width; x+=pixelSize) {
      color c = img.get(int(x), int(y));
      fill(c);
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  pixelSize = int(map(posX, 0, width, 1, 15));
  posX+=5*dir;
  if(posX > width){
    posX = width;
    dir*=-1;
  }
  if(posX < 0){
    posX = 0;
    dir*=-1;
  }
  fill(255, 0, 0);
  stroke(0);
  ellipse(posX, height-15, 30, 30);
}

