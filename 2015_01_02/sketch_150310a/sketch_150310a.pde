void setup(){
  size(600, 600);
  background(255, 255, 255);
}

void draw(){
  //background(255);
  float x = random(width);
  float y = random(height);
  float r = random(10, 80);
  
  stroke(map(x, 0, width, 0, 255),0,0);
  //noStroke();
  //fill(255, 255, 0);
  noFill();
  ellipse(x, y, r*2, r*2);
  
  /*stroke(255, 0, 0);
  line(0, height/2, width, height/2);
  line(width/2, 0, width/2, height);*/
}

void keyPressed(){
  if(key == ' '){
    background(255);
  }else{
    saveFrame("myWork_####.jpg");
  }
}
