boolean b = false;
int currTime = 0;
boolean toggle = true;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);

  if (b == true) {
    if (toggle == true) {
      currTime = millis();
      toggle = false;
    }
    fill(255, 0, 0);
    ellipse(width/2, height/2, 300, 300);    
    if (currTime+3000 < millis()) {
      b = false;
      toggle = true;
    }
  } else {
    fill(255);
    ellipse(width/2, height/2, 300, 300);
  }
}

void keyPressed() {
  if (key == 'a') {
    if (b == false) {
      b = true;
    }
  }
}

