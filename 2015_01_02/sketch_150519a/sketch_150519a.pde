PImage img;
float[] r;
float[] g;
float[] b;

float maxSize = 60;
float pixelSize;

float ballX;
float ballY;
float speedX = 5;
float speedY = 1;
float dirX = 1;
float dirY = 1;

void setup(){
  img = loadImage("unnamed.jpg");
  size(img.width, img.height);
  r = new float[img.width*img.height];
  g = new float[img.width*img.height];
  b = new float[img.width*img.height];
  for(int y=0; y<img.height; y++){
    for(int x=0; x<img.width; x++){
      int index = x + y*img.width;
      color c = img.pixels[index];
      r[index] = red(c);
      g[index] = green(c);
      b[index] = blue(c);
    }
  }
  img = null;
  background(0);
}

void draw(){
  pixelSize = map(mouseY, 0, height, 5, maxSize);
  background(map(ballX, 0, width, 0, 255));
  for(int y=0; y<height; y+=pixelSize){
    for(int x=0; x<width; x+=pixelSize){
      int index = x+y*width;
      noStroke();
      fill(r[index], g[index], b[index]);
      float mx = map(ballX, 0, width, 0, pixelSize);
      float br = (r[index]+g[index]+b[index])/3;
      float newSize = map(br, 0, 255, mx, pixelSize-mx);
      ellipse(x+pixelSize/2, y+pixelSize/2, newSize, newSize);
    }
  }
  
  ballX += dirX*speedX;
  ballY += dirY*speedY;
  
  if(ballX < 0 || ballX > width){
    dirX *= -1;
  }
  if(ballY < 0 || ballY > height){
    dirY *= -1;
  }
  fill(255, 0, 0);
  ellipse(ballX, height-50, 20, 20);
}
