PImage img;
int pixelSize = 1;
float posX=30;
int dir = 1;
int colMode = 0;

void setup() {
  img = loadImage("andy_warhol.jpg");
  size(img.width, img.height); 
  background(0);
  pixelSize = 1;
}

void draw() {
  background(0);
  for (int y=0; y<height; y+=pixelSize) {
    for (int x=0; x<width; x+=pixelSize) {
      color c = img.get(x, y);
      if (colMode == 0) {
        fill(red(c), green(c), blue(c));
      } else if (colMode == 1) {
        fill(blue(c), green(c), red(c));
      } else if (colMode == 2) {
        fill(red(c), blue(c), green(c));
      } else if (colMode == 3) {
        fill(green(c), red(c), blue(c));
      } else if (colMode == 4) {
        fill(green(c), blue(c), red(c));
      } else if (colMode == 5) {
        fill(blue(c), red(c), green(c));
      } else if (colMode == 6) {
        fill(255-red(c), 255-green(c), 255-blue(c));
      } else if (colMode == 7) {
        if (brightness(c) > map(mouseY, 0, height, 0, 255)) {
          fill(255);
        } else {
          fill(0);
        }
      }
      /*if(colMode == 0){
       fill(red(c), 0, 0);
       }else{
       fill(0, 0, blue(c));
       }*/
      noStroke();
      rect(x, y, pixelSize, pixelSize);
    }
  }
  /*pixelSize = int(map(posX, 0, width, 4, 15));
   posX += 5*dir;
   if(posX > width){
   posX = width;
   dir*=-1;
   }
   if(posX < 0){
   posX = 0;
   dir*=-1;
   }
   fill(255, 0, 0);
   stroke(0);
   ellipse(posX, height-15, 30, 30);*/

  /*colMode++;
   if (colMode > 7) {
   colMode = 0;
   }*/
}

void keyPressed() {
  if (key == ' ') {
    colMode++;
    if (colMode > 7) {
      colMode = 0;
    }
  }
}

