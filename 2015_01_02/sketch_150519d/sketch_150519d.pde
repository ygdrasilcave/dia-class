PImage img;
float[] r;
float[] g;
float[] b;
float[] nr;
float[] ng;
float[] nb;

float pixelSize = 1;

void setup(){
  img = loadImage("unnamed.jpg");
  size(img.width*2, img.height);
  r = new float[img.width*img.height];
  g = new float[img.width*img.height];
  b = new float[img.width*img.height];
  nr = new float[img.width*img.height];
  ng = new float[img.width*img.height];
  nb = new float[img.width*img.height];
  for(int y=0; y<img.height; y++){
    for(int x=0; x<img.width; x++){
      int index = x + y*img.width;
      color c = img.pixels[index];
      r[index] = red(c);
      g[index] = green(c);
      b[index] = blue(c);
    }
  }
  nr = sort(r);
  ng = sort(g);
  nb = sort(b);
  img = null;
  background(0);
}

void draw(){
  background(0);
  for(int y=0; y<height; y+=pixelSize){
    for(int x=0; x<width/2; x+=pixelSize){
      int index = x+y*width/2;
      noStroke();
      fill(r[index], g[index], b[index]);
      rect(x, y, pixelSize, pixelSize);  
      fill(nr[index], ng[index], nb[index]);
      rect(width/2+x, y, pixelSize, pixelSize); 
    }
  }
}
