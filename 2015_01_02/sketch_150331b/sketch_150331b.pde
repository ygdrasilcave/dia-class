int num = 1;
int numSpeed = 1;
int numDir = 1;
int rotState = 0;
int colState = 0;
boolean toggle = true;

int total = 10;

void setup() {
  size(800, 800);
  background(0);
  rectMode(CENTER);
}

void draw() {
  background(0);
  for (int i=0; i<total; i++) {
    for (int j=0; j<total; j++) {
      drawRect(width/total*i+width/(total*2), width/total*j+height/(total*2), width/total, height/total, num, rotState);
    }
  }

  num += numSpeed*numDir;
  if (num > 100) {
    numDir *= -1;
    num = 100;
  }
  if (num < 1) {
    numDir *= -1;
    num = 1;
  }
}

void drawRect(float _x, float _y, float _w, float _h, int _howMany, int _b) {
  pushMatrix();
  //noFill();
  //stroke(255);

  noStroke();
  translate(_x, _y);
  for (int i=_howMany-1; i>-1; i--) {
    if (colState == 0) {
      fill(255-(255/_howMany*(i+1)));
    } else if (colState == 1) {
      fill((255/_howMany*(i+1)));
    } else if (colState == 2) {
      fill(random(255), random(255), random(255));
    }
    pushMatrix();
    if (_b == 1) {
      rotate((TWO_PI/_howMany)*(i+1));
    }
    rect(0, 0, (_w/_howMany)*(i+1), (_h/_howMany)*(i+1));
    popMatrix();
  }
  popMatrix();

  if (second()%5 == 0) {
    if (toggle == true) {
      colState += 1;
      if (colState > 2) {
        colState = 0;
      }
      if (rotState == 0) {
        rotState = 1;
      } else {
        rotState = 0;
      }
      total = int(random(2, 13));
      toggle = false;
    }
  } else {
    toggle = true;
  }
}

void keyPressed() {
  if (key == ' ') {
    if (rotState == 0) {
      rotState = 1;
    } else {
      rotState = 0;
    }
  }
  if (key == 'i') {
    colState += 1;
    if (colState > 2) {
      colState = 0;
    }
  }
}

