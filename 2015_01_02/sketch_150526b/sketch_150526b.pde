import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

int px, py;
float r, g, b;

void setup() {
  //size(620,387);
  size(800, 600);
  //frameRate(25);
  oscP5 = new OscP5(this,13000);
  myRemoteLocation = new NetAddress("172.26.22.110",12000);
  background(0);
}

void keyPressed(){
  background(0);
}

void draw() {
  //background(0);  
  fill(r, g, b);
  noStroke();
  rect(px, py, 10, 10);
}



void oscEvent(OscMessage theOscMessage) { 
  if(theOscMessage.checkAddrPattern("/test")==true) {
    if(theOscMessage.checkTypetag("iifff")) {
      int dataX = theOscMessage.get(0).intValue();  
      int dataY = theOscMessage.get(1).intValue();
      float dataR = theOscMessage.get(2).floatValue();  
      float dataG = theOscMessage.get(3).floatValue();
      float dataB = theOscMessage.get(4).floatValue();  
      //print("### received an osc message /test with typetag ifs.");
      println(" values: "+dataX+", "+dataY);
      px = dataX;
      py = dataY;
      r = dataR;
      g = dataG;
      b = dataB;
      return;
    }  
  } 
  //println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
  if(theOscMessage.checkAddrPattern("/testb")==true) {
    if(theOscMessage.checkTypetag("iifff")) {
      int dataX = theOscMessage.get(0).intValue();  
      int dataY = theOscMessage.get(1).intValue();
      float dataR = theOscMessage.get(2).floatValue();  
      float dataG = theOscMessage.get(3).floatValue();
      float dataB = theOscMessage.get(4).floatValue();  
      //print("### received an osc message /test with typetag ifs.");
      println(" values: "+dataX+", "+dataY);
      px = dataX;
      py = dataY;
      r = dataR;
      g = dataG;
      b = dataB;
      return;
    }  
  }
}
