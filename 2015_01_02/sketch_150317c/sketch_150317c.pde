float x;
float y;
float sx;
float sy;
float r;
float dirX = 1;
float dirY = 1;
float ax;
float ay;
float maxSX = 15;
float maxSY = 10;
int i = 0;

float px;
float py;

void setup(){
  size(600*2, 600);
  background(0);
  x = width/4;
  y = height/2;
  px = x;
  py = y;
  sx = 0;
  sy = 0;
  r = 20;
  ax = random(0.6);
  ay = random(0.4);
  println(sx + " : " + sy);
}

void draw(){
  sx += ax;
  sy += ay;
  x = x+dirX*sx;
  y = y+dirY*sy;
  
  //background(0);
  fill(0, 10);
  noStroke();
  rect(0, 0,width/2,height);
  
  
  stroke(255, 255, 0);
  noFill();
  /*fill(255);
  noStroke();*/
  ellipse(width/4, height/2, x, y);
  
  stroke(255);
  strokeWeight(3);
  //ellipse(x, y, r, r);
  line(px, py, x, y);
  
  stroke(0, 255, 255);
  line(width/2+i, height, width/2+i, height-y);
  stroke(255, 0, 255);
  line(width/2+i+1, height, width/2+i+1, height-x);
  
  i+=2;
  if(i > width/2){
    i = 0;
    background(0);
  }

  if(x > width/2-r/2){
    x = width/2-r/2;
    dirX = dirX*-1;
    sx = random(6);
    sy = random(10);
  }
  if(x < r/2){
    x = r/2;
    dirX = dirX*-1;
    sx = random(6);
    sy = random(10);
  }
  if(y > height-r/2){
    y = height-r/2;
    dirY *= -1;
    sx = random(6);
    sy = random(10);
  }
  if(y < r/2){
    y = r/2;
    dirY *= -1;
    sx = random(6);
    sy = random(10);
  }
  px = x;
  py = y;
  
  if(frameCount%10 == 0){
    sx = random(6);
    sy = random(10);
  }
}

void keyPressed(){
  if(key == ' '){
    sx = random(6);
    sy = random(10);
  }
}
