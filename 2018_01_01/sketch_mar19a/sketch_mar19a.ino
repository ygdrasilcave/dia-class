void setup() {
  // put your setup code here, to run once:
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  pattern(500);
  pattern(10);
  pattern(100);
  pattern(250);
  pattern(1000);
  pattern(100);
}

void pattern(int d) {
  digitalWrite(12, HIGH);
  digitalWrite(11, LOW);
  delay(d);
  digitalWrite(12, LOW);
  digitalWrite(11, HIGH);
  delay(d);
}

void pattern0() {
  digitalWrite(12, HIGH);
  digitalWrite(11, LOW);
  delay(500);
  digitalWrite(12, LOW);
  digitalWrite(11, HIGH);
  delay(500);
}

void pattern1() {
  digitalWrite(12, HIGH);
  digitalWrite(11, LOW);
  delay(100);
  digitalWrite(12, LOW);
  digitalWrite(11, HIGH);
  delay(100);
}
