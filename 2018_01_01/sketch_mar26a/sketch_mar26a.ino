const int ledPin_01 = 11;
const int ledPin_02 = 12;
const int buttonPin = 2;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(buttonPin, INPUT);
  pinMode(ledPin_01, OUTPUT);
  pinMode(ledPin_02, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int buttonValue = digitalRead(buttonPin);
  
  Serial.println(buttonValue);
  if (buttonValue == 1) {
    pattern(100);
  } else {
    LEDAll(1);
  }
}

void pattern(int d) {
  digitalWrite(ledPin_02, HIGH);
  digitalWrite(ledPin_01, LOW);
  delay(d);
  digitalWrite(ledPin_02, LOW);
  digitalWrite(ledPin_01, HIGH);
  delay(d);
}

void LEDAll(int i) {
  digitalWrite(ledPin_01, i);
  digitalWrite(ledPin_02, i);
}

