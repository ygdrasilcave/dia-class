int sensorValue = 0;

void setup() {
  Serial.begin(9600);
  pinMode(10, OUTPUT);
}

void loop() {
  sensorValue = analogRead(0);
  Serial.println(sensorValue);
  if(sensorValue < 600){
    digitalWrite(10, HIGH);
  }else{
    digitalWrite(10, LOW);
  }
}

