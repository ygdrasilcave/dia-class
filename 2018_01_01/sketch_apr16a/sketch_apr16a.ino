int sensorValue = 0;

void setup() {
  Serial.begin(9600);
  for (int i = 0; i < 7; i++) {
    pinMode(i+7, OUTPUT);
  }
  blinkAll(10);
}

void loop() {
  sensorValue = analogRead(0);
  //sensorValue = (float(sensorValue)/1023) * 6;
  sensorValue = int(map(sensorValue, 0, 1023, 0, 6));
  Serial.println(sensorValue);
  for(int i=0; i<7; i++){
    if(i == sensorValue){
      digitalWrite(i+7, HIGH);
    }else{
      digitalWrite(i+7, LOW);
    }
  }
}

void blinkAll(int _num) {
  for (int n = 0; n < _num; n++) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(i+7, HIGH);
    }
    delay(100);
    for (int i = 0; i < 7; i++) {
      digitalWrite(i+7, LOW);
    }
    delay(100);
  }
}

