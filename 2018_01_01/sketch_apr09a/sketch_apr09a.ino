#define SW0 2
#define SW1 3
int prevState0 = 0;
int currState0 = 0;
int prevState1 = 0;
int currState1 = 0;
int counter = 0;

void setup() {
  pinMode(SW0, INPUT);
  pinMode(SW1, INPUT);

  for (int i = 0; i < 7; i = i + 1) {
    pinMode(i + 7, OUTPUT);
  }

  counter = 10;

  Serial.begin(9600);
}

void loop() {
  currState0 = digitalRead(SW0);
  currState1 = digitalRead(SW1);
  if ( detectRisingEdgeOfSW0() == 1) {
    counter--;
  }
  if ( detectRisingEdgeOfSW1() == 1) {
    counter++;
  }

  if (counter < 7) {
    counter = 13;
  } else if (counter > 13) {
    counter = 7;
  }

  for (int i = 0; i < 7; i = i + 1) {
    if (i + 7 != counter) {
      digitalWrite(i + 7, LOW);
    } else {
      digitalWrite(i + 7, HIGH);
    }
  }
  //digitalWrite(counter, HIGH);


  Serial.println(counter);
}

int detectRisingEdgeOfSW0() {
  int result = 0;
  // rising edge
  if ( (prevState0 == 0) && (currState0 == 1) ) {
    result = 1;
  }
  else {
    result = 0;
  }
  prevState0 = currState0;
  return result;
}

int detectRisingEdgeOfSW1() {
  int result = 0;
  // rising edge
  if ( (prevState1 == 0) && (currState1 == 1) ) {
    result = 1;
  }
  else {
    result = 0;
  }
  prevState1 = currState1;
  return result;
}

