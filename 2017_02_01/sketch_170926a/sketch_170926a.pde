PVector position;
PVector velocity;
PVector acc;
float maxSpeed;

void setup(){
  size(800, 800);
  background(0);
  position = new PVector(width/2, height/2);
  velocity = new PVector(0, 0);
  acc = PVector.random2D();
  acc.mult(0.326);
  maxSpeed = 8;
}

void draw(){
  background(0);
  PVector mouse = new PVector(mouseX, mouseY);
  
  PVector dir = PVector.sub(mouse, position);
  
  dir.normalize();
  dir.mult(0.326);
  acc = dir;
  
  velocity.add(acc);
  velocity.limit(maxSpeed);
  position.add(velocity); 
  ellipse(position.x, position.y, 50, 50);
  
  if(position.x > width){
    position.x = 0;
  }else if(position.x < 0){
    position.x = width;
  }
  if(position.y > height){
    position.y = 0;
  }else if(position.y < 0){
    position.y = height;
  }
}