class Ball {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float w;
  boolean rotateType;
  float t;
  float r, g, b;

  Ball(float _x, float _y, boolean _rt, float _r, float _g, float _b) {
    w = random(5, 20);
    position = new PVector(_x, _y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(random(-0.05, 0.05), random(-0.05, 0.05));
    maxSpeed = random(3, 6);
    rotateType = _rt;
    t = 0.0;
    r = _r;
    g = _g;
    b = _b;
  }

  void display() {
    acceleration = PVector.random2D();
    acceleration.mult(0.368);
    
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    
    float w2 = w + sin(t)*(w*0.5);
    t += 0.26;

    pushMatrix();
    translate(position.x, position.y);
    if (rotateType == true) {
      rotate(velocity.heading());
    }else{
      rotate(0);
    }
    fill(r, g, b);
    noStroke();
    ellipse(0, 0, w2, w2);
    stroke(r, g, b);
    strokeWeight(3);
    line(0, 0, w2, 0);
    popMatrix();
  }

  void checkEdge() {
    /*if (position.x > width-w/2 || position.x < w/2) {
     velocity.x = velocity.x * -1;
     }
     if (position.y > height-w/2 || position.y < w/2) {
     velocity.y = velocity.y * -1;
     }*/
    if (position.x > width) {
      position.x = 0;
    }
    if (position.x < 0) {
      position.x = width;
    }
    if (position.y > height) {
      position.y = 0;
    }
    if (position.y < 0) {
      position.y = height;
    }
  }
};