ArrayList<Ball> balls;
int deadCount;

void setup() {
  size(800, 800);
  background(0);
  balls = new ArrayList<Ball>();

  /*for(int i=0; i<1000; i++){
   balls.add(new Ball());
   }*/
  println(balls.size());

  colorMode(HSB, 255, 255, 255);
  textAlign(CENTER, CENTER);
  deadCount = 0;

  sound_init();
}

void draw() {
  //background(0);
  fill(0, 25);
  noStroke();
  rect(0, 0, width, height);

  PVector center = new PVector(width/2, height/2);

  for (int i = balls.size()-1; i>=0; i--) {
    balls.get(i).display();
    balls.get(i).checkEdge();
    /*float x = balls.get(i).position.x;
     float y = balls.get(i).position.y;
     if(sqrt((width/2 - x)*(width/2 - x) + (height/2 - y)*(height/2 - y)) < 100){
     balls.remove(i);
     }*/
    if (balls.get(i).position.dist(center) < 100) {
      balls.remove(i);
      deadCount++;
    }
  }
  println(balls.size());
  noFill();
  stroke(255);
  //noStroke();
  //rect(350, 350, 100, 100);
  //ellipse(width/2, height/2, 200, 200);

  textSize(24);
  fill(255);
  noStroke();
  text(deadCount, width/2, height/2);

  if (sound_RMS() == true) {
    balls.add(new Ball(random(width), random(height), true, random(255), 255, 255));
  }
}

void mouseMoved() {
  balls.add(new Ball(mouseX, mouseY, true, 255, 0, 255));
}
/*
void keyPressed() {
 if (key == ' ') {
 for (int i = balls.size()-1; i>=0; i--) {
 if (balls.get(i).b == false) {
 balls.get(i).b = true;
 } else {
 balls.get(i).b = false;
 }
 }
 }
 }
 */