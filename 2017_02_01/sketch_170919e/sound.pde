import processing.sound.*;
AudioIn input;
Amplitude rms;
boolean bang = false;

void sound_init() {
  input = new AudioIn(this, 0);
  input.start();
  rms = new Amplitude(this);
  rms.input(input);
}

boolean sound_RMS() {
  input.amp(map(mouseY, 0, height, 0.0, 1.0));
  //map(rms.analyze(), 0, 0.5, 1, 350);
  if(rms.analyze() > 0.025){
    bang = true;
  }else{
    bang = false;
  }
  return bang;
}