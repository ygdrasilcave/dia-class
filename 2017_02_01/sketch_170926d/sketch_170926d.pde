ArrayList<Ball> balls;
int ballsSize_curr;
int ballsSize_pre;
boolean timerStart = false;

void setup() {
  size(800, 800);
  colorMode(HSB, 255, 255, 255);
  background(0);
  balls = new ArrayList<Ball>();
  for (int i=0; i<1000; i++) {
    if (i == 0) {
      balls.add(new Ball(50));
    } else {
      balls.add(new Ball(random(2, 30)));
    }
  }
  sound_init();
}

void draw() {
  background(0);
  ballsSize_pre = balls.size();

  if (balls.size() > 0) {
    for (int i=balls.size()-1; i>=0; i--) {
      Ball b1 = balls.get(i);
      Ball b2;
      float dist;
      boolean dead = false;
      int num = 0;
      for (int j=balls.size()-1; j>=0; j--) {
        if (j != i) {
          b2 = balls.get(j);
          dist = PVector.sub(b1.position, b2.position).mag();
          if (dist < 10) {
            dead = true;
            if (b1.w > b2.w) {
              num = j;
              b1.w = b1.w + 2;
            } else {
              num = i;
              b2.w = b2.w + 2;
            }
          }
        }
      }
      b1.display();
      if (dead == true) {
        balls.remove(num);
      }
      if (timerStart == true) {
        balls.get(i).w = balls.get(i).w - 0.5;
        if(balls.get(i).w < 4){
          balls.get(i).w = 4;
        }
      }
    }
  }

  ballsSize_curr = balls.size();

  if (ballsSize_pre == 2 && ballsSize_curr == 1) {
    print("bang");
    sound_play();
    timerStart = true;
  } else if (ballsSize_curr >= 2) {
    timerStart = false;
  }
  
  if(ballsSize_curr == 1){
    timerStart = true;
  }
}

void keyPressed() {
  if (key == ' ') {
    balls.add(new Ball(random(2, 30)));
  }
}