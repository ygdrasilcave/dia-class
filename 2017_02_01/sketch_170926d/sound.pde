import processing.sound.*;

SinOsc sine;
float freq=400;
float amp=0.5;

Env env;
float attackTime = 0.001;
float sustainTime = 0.004;
float sustainLevel = 0.3;
float releaseTime = 0.4;

void sound_init(){
  sine = new SinOsc(this);
  sine.amp(0.5);
  env  = new Env(this); 
}

void sound_play(){
  sine.play();
  sine.freq(random(120, 600));
  env.play(sine, attackTime, sustainTime, sustainLevel, releaseTime);
}