IntList randomDrunk;
int counter = 0;

void setup() {
  size(200, 200);
  randomDrunk = new IntList();
  
  for(int i=0; i<10; i++){
    randomDrunk.set(i, i);
  }
  println(randomDrunk);
  randomDrunk.shuffle();
  println(randomDrunk);

}

void draw() {
}

void keyPressed(){
  if(key == ' '){
    int randomDrunkNumber = randomDrunk.get(counter);
    println(randomDrunkNumber);
    counter++;
    if(counter > randomDrunk.size()-1){
      counter = 0;
      
      randomDrunk.clear();
      int num = int(random(1, 150));
      num = 5;
      for(int i=0; i<num; i++){
        randomDrunk.set(i, i);
      }      
      randomDrunk.shuffle();
      println(randomDrunk);
      
    }
  }
}