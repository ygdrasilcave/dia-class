import processing.serial.*;
Serial port;

float val1, val2;
float t = 0.0;

void setup() {
  size(600, 600);
  port = new Serial(this, "COM3", 9600);
}

void draw() {
  background(0);
  int r = int(map(mouseX, 0, width, 0, 255));
  int g = int(map(mouseY, 0, width, 0, 255));
  int b = int(abs(sin(t))*255);
  port.write(r + " " + g + " " + b + "\n");
  t += 0.025;
}

/*

 const int redPin = 9;
 const int greenPin = 10;
 const int bluePin = 11;
 
 void setup() {
 Serial.begin(9600);
 }
 
 void loop() {
 while (Serial.available() > 0) {
 int red = Serial.parseInt(); 
 int green = Serial.parseInt(); 
 int blue = Serial.parseInt(); 
 if (Serial.read() == '\n') {
 analogWrite(redPin, red);
 analogWrite(greenPin, green);
 analogWrite(bluePin, blue);
 }
 }
 }
 
 */