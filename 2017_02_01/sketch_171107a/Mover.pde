class Mover {
  PVector acc;
  PVector velocity;
  PVector position;
  float maxSpeed;
  float d;
  float t;
  float nx;
  float ny;
  float nxs;
  float nys;
  float w;
  float ts;
  boolean myType;

  ArrayList<PVector> tail;
  FloatList rotVal;

  int tailNum;
  int tailLength;

  Mover(boolean _b) {
    acc = new PVector(random(-0.02, 0.02), random(-0.03, 0.03));
    velocity = new PVector(0, 0);
    position = new PVector(0, 0);
    maxSpeed = random(3, 8);
    d = 100;
    t = 0;
    nx = 0;
    ny = 0;
    nxs = random(0.005, 0.03);
    nys = random(0.005, 0.03);
    w = random(20, 30);
    ts = 0;
    myType = _b;

    tail = new ArrayList<PVector>();
    rotVal = new FloatList();

    tailNum = int(random(3, 10));
    tailLength = int(random(8, 20));

    for (int i=0; i<tailNum*tailLength; i++) {
      tail.add(new PVector(0, 0));
      rotVal.append(0);
    }
  }

  void update() {    
    for (int i=tail.size()-1; i>=0; i--) {
      if (i == 0) {
        tail.set(i, position.copy());
        rotVal.set(i, velocity.heading());
      } else {
        tail.set(i, tail.get(i-1));
        rotVal.set(i, rotVal.get(i-1));
      }
    }  

    acc.x = map(noise(nx), 0, 1, -1, 1);
    acc.y = map(noise(ny), 0, 1, -1, 1);

    velocity.add(acc);
    velocity.limit(maxSpeed);
    position.add(velocity);

    if (myType == true) {
      d = abs(sin(t))*(velocity.mag()*1.5)*(w*0.2);
      ts = abs(acc.mag())*0.2;
    } else {
      ts = 0.1;
      d = abs(sin(t))*50;
    }


    t = t + ts;    
    nx = nx + nxs;
    ny = ny + nys;
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    if (myType == true) {
      fill(255);
      noStroke();
    } else {
      noFill();
      stroke(255);
      strokeWeight(2);
    }
    rect(0, 0, w, w);
    ellipse(d, 0, w/2.0, w/2.0);
    stroke(255);
    strokeWeight(3);
    line(0, 0, d, 0);    
    popMatrix();

    for (int i=tailNum; i<tail.size(); i+=tailNum) {
      pushMatrix();
      float scale = map(i, 0, tail.size()-1, 1.0, 0.1);
      translate(tail.get(i).x, tail.get(i).y);
      rotate(rotVal.get(i));
      rect(0, 0, w*scale, w*scale);
      popMatrix();

      strokeWeight(3);
      stroke(255);
      if (i < tail.size()-(tailNum+1)) {
        PVector c = tail.get(i);
        PVector p = tail.get(i+tailNum);
        PVector dist = PVector.sub(c, p);
        if (!(dist.mag() > height/2)) { 
          line(tail.get(i).x, tail.get(i).y, tail.get(i+tailNum).x, tail.get(i+tailNum).y);
        }
      }
    }


    PVector c = position.copy();
    PVector p = tail.get(tailNum);
    PVector dist = PVector.sub(c, p);
    if (!(dist.mag() > height/2)) { 
      line(position.x, position.y, tail.get(tailNum).x, tail.get(tailNum).y);
    }

    if (position.x > width+100) {
      position.x = -100;
    } else if (position.x < -100) {
      position.x = width+100;
    }
    if (position.y > height+100) {
      position.y = -100;
    } else if (position.y < -100) {
      position.y = height+100;
    }
  }
};