ArrayList<Mover> movers;

void setup() {
  //size(800, 800);
  fullScreen();
  background(0);
  movers = new ArrayList<Mover>();
  for (int i=0; i<100; i++) {
    if (i%2 == 0) {
      movers.add(new Mover(true));
    } else {
      movers.add(new Mover(false));
    }
  }
  rectMode(CENTER);
}

void draw() {
  background(0);
  
  //fill(0, 20);
  //noStroke();
  //rect(width/2, height/2, width, height);

  for (int i=0; i<movers.size(); i++) {
    Mover m = movers.get(i);
    m.update();
    m.display();
  }
}