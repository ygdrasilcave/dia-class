void setup(){
  size(800, 800);
  background(0);
}

void draw(){
  background(0);  
  PVector mouse = new PVector(mouseX, mouseY);
  PVector center = new PVector(width/2, height/2);
  mouse.sub(center);
  //mouse.normalize();
  //mouse.mult(100);
  mouse.setMag(100);
  
  float angle = mouse.heading();
  
  pushMatrix();
  translate(100, 100);
  rotate(angle);
  rect(0, -5, 50, 10);
  popMatrix();
  
  pushMatrix();
  translate(width/2, height/2);
  stroke(255);
  strokeWeight(3);
  line(0, 0, mouse.x, mouse.y);
  popMatrix();
}