Circle circles;

float t = 0.0;

void setup() {
  size(800, 800);
  background(0);
  circles = new Circle();
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 10);
  rect(0, 0, width, height);

  pushMatrix();
  translate(mouseX, mouseY);
  rotate(t);
  noStroke();
  fill(255);
  ellipse(0, 0, 10, 10);
  ellipse(100+100, 0, 50, 50);
  popMatrix();
  t += 0.035;
}