class Circle{
  float t, tSpeed;
  float centerX, centerY;
  float dist;
  float dia;
  float r, g, b;
  
  Circle(){
    t = 0.0;
    tSpeed = random(0.01, 0.058);
    centerX = width/2;
    centerY = height/2;
    dist = 200;
    dia = 50;
    r = 255;
    g = 255;
    b = 255;
  }
  
  void update(){
    t = t + tSpeed;
  }
  
  void display(){
    pushMatrix();
    noStroke();
    fill(r, g, b);
    translate(centerX, centerY);
    rotate(t);
    ellipse(0, 0, dia*0.1, dia*0.1);
    ellipse(dist, 0, dia, dia);
    popMatrix();
  }
}