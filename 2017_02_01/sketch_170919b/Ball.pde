class Ball {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float w;

  Ball() {
    w = random(5, 20);
    position = new PVector(random(w/2, width-w/2), random(w/2, height-w/2));
    velocity = new PVector(0, 0);
    acceleration = new PVector(random(0.05), random(0.05));
    maxSpeed = random(5, 12);
  }

  void display() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    fill(255);
    ellipse(position.x, position.y, w, w);
  }

  void checkEdge() {
    /*if (position.x > width-w/2 || position.x < w/2) {
      velocity.x = velocity.x * -1;
    }
    if (position.y > height-w/2 || position.y < w/2) {
      velocity.y = velocity.y * -1;
    }*/
    if(position.x > width){
      position.x = 0;
    }
    if(position.x < 0){
      position.x = width;
    }
    if(position.y > height){
      position.y = 0;
    }
    if(position.y < 0){
      position.y = height;
    }
  }
};