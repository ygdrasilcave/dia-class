Ball[] balls;

void setup() {
  size(800, 800);
  background(0);
  balls = new Ball[200];
  for (int i=0; i<balls.length; i++) {
    balls[i] = new Ball();
  }
}

void draw() {
  background(0);
  for (int i=0; i<balls.length; i++) {
    balls[i].display();
    balls[i].checkEdge();
  }
}