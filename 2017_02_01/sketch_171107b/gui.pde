import controlP5.*;
ControlP5 cp5;

void guiInit() {
  cp5 = new ControlP5(this);

  cp5.addButton("reset")
    .setValue(0)
    .setPosition(30, 30)
    .setSize(200, 50)
    ;

  cp5.addSlider("slider1")
    .setPosition(30, 110)
    .setSize(200, 50)
    .setRange(0, 100)
    .setValue(20)
    ;
  // reposition the Label for controller 'slider'
  cp5.getController("slider1").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("slider1").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);

  cp5.addSlider("slider2")
    .setPosition(30, 180)
    .setSize(200, 50)
    .setRange(0, 100)
    .setValue(50)
    ;
  // reposition the Label for controller 'slider'
  cp5.getController("slider2").getValueLabel().align(ControlP5.LEFT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);
  cp5.getController("slider2").getCaptionLabel().align(ControlP5.RIGHT, ControlP5.BOTTOM_OUTSIDE).setPaddingX(0);

  cp5.addButton("add")
    .setValue(0)
    .setPosition(30, 250)
    .setSize(100, 50)
    ;

  cp5.addButton("sub")
    .setValue(0)
    .setPosition(130, 250)
    .setSize(100, 50)
    ;
}

public void reset(int theValue) {
  movers.clear();
  for (int i=0; i<20; i++) {
    if (i%2 == 0) {
      movers.add(new Mover(random(width), random(height), true));
    } else {
      movers.add(new Mover(random(width), random(height), false));
    }
  }
}

public void add(int theValue) {
  for (int i=movers.size()-1; i>=0; i--) {
    Mover m = movers.get(i);
    m.addTail(-1);
  }
}

public void sub(int theValue) {
  for (int i=movers.size()-1; i>=0; i--) {
    Mover m = movers.get(i);
    m.addTail(1);
  }
}

public void slider1(float theValue) {
  //println("a slider event. setting background to "+theValue);
  worldSpeedVal = map(theValue, 0, 100, 0.0, 3.0);
}

public void slider2(float theValue) {
  //println("a slider event. setting background to "+theValue);
  pointNum = int(map(theValue, 0, 100, 3, 12));
}