ArrayList<Mover> movers;

float worldSpeedVal = 1.0;
int pointNum = 3;

void setup() {
  size(1200, 800);
  //fullScreen();
  background(0);
  movers = new ArrayList<Mover>();
  for (int i=0; i<20; i++) {
    if (i%2 == 0) {
      movers.add(new Mover(random(width), random(height), true));
    } else {
      movers.add(new Mover(random(width), random(height), false));
    }
  }
  rectMode(CENTER);
  
  guiInit();
}

void draw() {
  background(0);

  for (int i=movers.size()-1; i>=0; i--) {
    Mover m = movers.get(i);
    m.update(worldSpeedVal);
    if (m.alive == false) {
      movers.remove(i);
    } else {
      m.display();
    }
  }
}