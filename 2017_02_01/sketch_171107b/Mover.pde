class Mover {
  PVector acc;
  PVector velocity;
  PVector position;
  float maxSpeed;
  float d;
  float t;
  float nx;
  float ny;
  float nxs;
  float nys;
  float w;
  float ts;
  boolean myType;
  ArrayList<PVector> tail;
  FloatList tailRot;
  int tailNum;
  int tailDist;
  int age;
  int ageMax;
  boolean alive;

  Mover(float _x, float _y, boolean _b) {
    acc = new PVector(random(-0.02, 0.02), random(-0.03, 0.03));
    velocity = new PVector(0, 0);
    position = new PVector(_x, _y);
    maxSpeed = random(3, 8);
    d = 100;
    t = 0;
    nx = 0;
    ny = 0;
    nxs = random(0.005, 0.03);
    nys = random(0.005, 0.03);
    w = random(20, 30);
    ts = 0;
    myType = _b;

    tailDist = int(random(2, 8));
    tailNum = int(random(4, 20));
    //tailNum = 30;

    tail = new ArrayList<PVector>();
    tailRot = new FloatList();
    for (int i=0; i<tailNum*tailDist; i++) {
      tail.add(new PVector(0, 0));
      tailRot.set(i, 0);
    }

    age = 0;
    ageMax = int(random(1000, 2000));
    alive = true;
  }

  void update(float _wsv) {

    for (int i=tail.size()-1; i>=0; i--) {
      if (i == 0) {
        tail.set(i, position.copy());
        tailRot.set(i, velocity.heading());
      } else {
        tail.set(i, tail.get(i-1));
        tailRot.set(i, tailRot.get(i-1));
      }
    }

    acc.x = map(noise(nx), 0, 1, -1, 1);
    acc.y = map(noise(ny), 0, 1, -1, 1);

    velocity.add(acc);
    velocity.limit(maxSpeed*_wsv);
    position.add(velocity);

    if (myType == true) {
      d = abs(sin(t))*(velocity.mag()*1.5)*(w*0.2);
      ts = abs(acc.mag())*0.2;
    } else {
      ts = 0.1;
      d = abs(sin(t))*50;
    }

    t = t + ts;    
    nx = nx + nxs;
    ny = ny + nys;

    /*age = age+1;
     if(age > ageMax && alive == true){
     alive = false;
     }*/
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    if (myType == true) {
      fill(255);
      noStroke();
    } else {
      noFill();
      strokeWeight(2);
      stroke(255);
    }
    //rect(0, 0, w, w);
    
    form(0, 0, w, pointNum);
    ellipse(d, 0, w/2.0, w/2.0);
    stroke(255);

    strokeWeight(3);
    line(0, 0, d, 0);    
    popMatrix();

    for (int i=tailDist; i<tail.size(); i=i+tailDist) {
      pushMatrix();
      translate(tail.get(i).x, tail.get(i).y);
      rotate(tailRot.get(i));
      float s = map(i, 0, tail.size()-1, 1.0, 0.1);

      if (myType == true) {
        fill(255);
        noStroke();
      } else {
        noFill();
        strokeWeight(2);
        stroke(255);
      }

      //rect(0, 0, w*s, w*s);   
      form(0, 0, w*s, pointNum);
      popMatrix();

      strokeWeight(3);
      stroke(255);
      if (i < tail.size()-(tailDist+1)) {
        PVector c = tail.get(i);
        PVector p = tail.get(i+tailDist);
        PVector dist = PVector.sub(c, p);
        if (!(dist.mag() > height/2)) { 
          line(tail.get(i).x, tail.get(i).y, tail.get(i+tailDist).x, tail.get(i+tailDist).y);
        }
      }
    }
    PVector c = position.copy();
    PVector p = tail.get(tailDist);
    PVector dist = PVector.sub(c, p);
    if (!(dist.mag() > height/2)) { 
      line(position.x, position.y, tail.get(tailDist).x, tail.get(tailDist).y);
    }

    if (position.x > width+100) {
      position.x = -100;
    } else if (position.x < -100) {
      position.x = width+100;
    }
    if (position.y > height+100) {
      position.y = -100;
    } else if (position.y < -100) {
      position.y = height+100;
    }
  }

  void addTail(int _t) {
    tailDist += _t;
    if (tailDist < 1) {
      tailDist = 1;
    }
    if (tailDist > (tail.size()/2)) {
      tailDist = (tail.size()/2);
    }
  }

  void form(float _x, float _y, float _w, int _n) {
    float angle = 360/_n;
    beginShape();
    for (int i=0; i<_n; i++) {
      vertex(_x+cos(radians(angle*i-angle/2))*(_w), _y + sin(radians(angle*i-angle/2))*(_w));
    }
    endShape(CLOSE);
  }
};