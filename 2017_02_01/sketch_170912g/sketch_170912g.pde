PVector v1;

void setup() {
  size(600, 600);
  v1 = new PVector(300, 300);
}

void draw() {
  background(0);
  
  v1.set(mouseX, mouseY);
  
  strokeWeight(3);
  fill(255);
  ellipse(v1.x, v1.y, 20, 20);
  stroke(255);
  
  //line(0, 0, v1.x, v1.y);
  line(0, 0, v1.normalize().x*100, v1.normalize().y*100);
  
  line(width-100, 0, width-100, v1.mag());
}