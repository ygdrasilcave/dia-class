class Mover {
  PVector acc;
  PVector velocity;
  PVector position;
  float maxSpeed;
  float d;
  float t;
  float nx;
  float ny;
  float nxs;
  float nys;
  float w;
  float ts;
  boolean myType;

  Mover(boolean _b) {
    acc = new PVector(random(-0.02, 0.02), random(-0.03, 0.03));
    velocity = new PVector(0, 0);
    position = new PVector(0, 0);
    maxSpeed = random(3, 8);
    d = 100;
    t = 0;
    nx = 0;
    ny = 0;
    nxs = random(0.005, 0.03);
    nys = random(0.005, 0.03);
    w = random(20, 30);
    ts = 0;
    myType = _b;
  }

  void update() {
    acc.x = map(noise(nx), 0, 1, -1, 1);
    acc.y = map(noise(ny), 0, 1, -1, 1);

    velocity.add(acc);
    velocity.limit(maxSpeed);
    position.add(velocity);

    if (myType == true) {
      d = abs(sin(t))*(velocity.mag()*1.5)*(w*0.2);
      ts = abs(acc.mag())*0.2;
    } else {
      ts = 0.1;
      d = abs(sin(t))*50;
    }


    t = t + ts;    
    nx = nx + nxs;
    ny = ny + nys;
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    if (myType == true) {
      fill(255);
      noStroke();
    }else{
      noFill();
      stroke(255);
      strokeWeight(2);
    }
    ellipse(0, 0, w, w);
    ellipse(d, 0, w/2.0, w/2.0);
    stroke(255);
    strokeWeight(3);
    line(0, 0, d, 0);    
    popMatrix();

    if (position.x > width+100) {
      position.x = -100;
    } else if (position.x < -100) {
      position.x = width+100;
    }
    if (position.y > height+100) {
      position.y = -100;
    } else if (position.y < -100) {
      position.y = height+100;
    }
  }
};