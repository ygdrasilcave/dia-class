boolean mouse_prev = false;
boolean mouse_curr = false;
color rect_color;
color ellipse_color;
float w;

void setup() {
  size(800, 400);
  background(0);
  sound_init();
  colorMode(HSB, 255, 255, 255);
  change_color('r');
  change_color('e');
  
  w = width/2.0-(width/2.0)*0.3;
}

void draw() {
  background(0);
  noStroke();
  
  fill(ellipse_color);
  ellipse(width/4.0, height/2.0, w, w);
  fill(rect_color);
  rect((width/4.0)*3.0-w/2.0, height/2.0-w/2.0, w, w);
  
  if (mousePressed == true) {
    mouse_curr = true;
  } else {
    mouse_curr = false;
  }

  if (mouse_prev == false && mouse_curr == true) {
    if (mouseX > (width/4.0)*3.0-w/2.0 && mouseX < (width/4.0)*3.0+w/2.0) {
      if (mouseY > height/2.0-w/2.0 && mouseY < height/2.0+w/2.0) {
        sound_play('r');
        change_color('r');
      }
    }
    if (sqrt((mouseX-width/4.0)*(mouseX-width/4.0) + (mouseY-height/2.0)*(mouseY-height/2.0)) < w/2.0) {
      sound_play('e');
      change_color('e');
    }
  }

  mouse_prev = mouse_curr;
}

void change_color(char _c){
  if(_c == 'r'){
    rect_color = color(random(255), 255, 255);
  }else if(_c == 'e'){
    ellipse_color = color(random(255), 255, 255);
  }
}