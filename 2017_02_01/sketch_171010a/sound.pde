import processing.sound.*;
SinOsc sine;
SqrOsc sqr;
float freq=400;
float amp=0.5;

Env env;
float attackTime = 0.001;
float sustainTime = 0.004;
float sustainLevel = 0.3;
float releaseTime = 0.4;

void sound_init() {
  sine = new SinOsc(this);
  sine.amp(0.5);

  sqr = new SqrOsc(this);
  sqr.amp(0.5);

  env  = new Env(this);
}

void sound_play(char _c) {
  attackTime = random(0.0005, 0.002);
  sustainTime = random(0.002, 0.006);
  sustainLevel = random(0.3, 0.8);
  releaseTime = random(0.2, 0.6);
  if (_c == 'e') {
    sine.play();
    sine.pan(-1);
    sine.freq(random(120, 600));
    env.play(sine, attackTime, sustainTime, sustainLevel, releaseTime);
  }
  if (_c == 'r') {
    sqr.play();
    sqr.pan(1);
    sqr.freq(random(120, 600));
    env.play(sqr, attackTime, sustainTime, sustainLevel, releaseTime);
  }
}