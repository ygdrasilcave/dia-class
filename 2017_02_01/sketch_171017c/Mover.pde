class Mover {
  PVector acc;
  PVector velocity;
  PVector position;
  float maxSpeed;
  float dx1;
  float dy1;
  float dx2;
  float dy2;
  float d1;
  float d2;
  float t;
  float nx;
  float ny;
  float nxs;
  float nys;
  float w;
  float ts;
  boolean myType;
  float theta;

  Mover(boolean _b) {
    acc = new PVector(random(-0.02, 0.02), random(-0.03, 0.03));
    velocity = new PVector(0, 0);
    position = new PVector(0, 0);
    maxSpeed = random(3, 8);
    dx1 = 100;
    dy1 = 100;
    dx2 = 100;
    dy2 = 100;
    t = 0;
    nx = 0;
    ny = 0;
    nxs = random(0.005, 0.03);
    nys = random(0.005, 0.03);
    w = random(20, 30);
    ts = 0;
    myType = _b;
    theta = random(radians(10), radians(35));
    
  }

  void update() {
    acc.x = map(noise(nx), 0, 1, -1, 1);
    acc.y = map(noise(ny), 0, 1, -1, 1);

    velocity.add(acc);
    velocity.limit(maxSpeed);
    position.add(velocity);

    if (myType == true) {
      d1 = abs(sin(t))*(velocity.mag()*1.5)*(w*0.2);
      d2 = abs(cos(t))*(velocity.mag()*1.5)*(w*0.2);
      ts = abs(acc.mag())*0.2;
    } else {
      d1 = abs(sin(t))*50;
      d2 = abs(cos(t))*50;
      ts = 0.1;
    }
    dx1 = -cos(theta)*d1;
    dy1 = -sin(theta)*d1;
    dx2 = -cos(-theta)*d2;
    dy2 = -sin(-theta)*d2;

    t = t + ts;    
    nx = nx + nxs;
    ny = ny + nys;
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    if (myType == true) {
      fill(255);
      noStroke();
    } else {
      noFill();
      stroke(255);
      strokeWeight(2);
    }
    ellipse(0, 0, w, w);
    ellipse(dx1, dy1, w/2.0, w/2.0);
    ellipse(dx2, dy2, w/2.0, w/2.0);
    stroke(255);
    strokeWeight(3);
    line(0, 0, dx1, dy1); 
    line(0, 0, dx2, dy2);    
    popMatrix();

    if (position.x > width+100) {
      position.x = -100;
    } else if (position.x < -100) {
      position.x = width+100;
    }
    if (position.y > height+100) {
      position.y = -100;
    } else if (position.y < -100) {
      position.y = height+100;
    }
  }
};