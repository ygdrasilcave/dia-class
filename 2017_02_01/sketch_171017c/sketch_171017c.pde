ArrayList<Mover> movers;

void setup() {
  //size(800, 800);
  fullScreen();
  background(0);
  movers = new ArrayList<Mover>();
  for (int i=0; i<200; i++) {
    if (i%2 == 0) {
      movers.add(new Mover(true));
    } else {
      movers.add(new Mover(false));
    }
  }
}

void draw() {
  background(0);

  for (int i=0; i<movers.size(); i++) {
    Mover m = movers.get(i);
    m.update();
    m.display();
  }
}