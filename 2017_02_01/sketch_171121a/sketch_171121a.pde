import processing.serial.*;
Serial port;

float val1, val2;

void setup() {
  size(600, 600);
  port = new Serial(this, "COM3", 9600);
  textAlign(CENTER, CENTER);
  textSize(30);
}

void draw() {
  background(0);
  noStroke();
  fill(255);
  text(val1 + " : " + val2, width/2, height-50);

  stroke(255);
  fill(map(val1, 0, 1023, 0, 255));
  ellipse(width/2, height/2, val2/2, val2/2);
}

void serialEvent (Serial myPort) {
  String inString = myPort.readStringUntil('\n');

  if (inString != null) {
    inString = trim(inString);

    String[] list = split(inString, ',');

    if (list.length == 3) {
      if (list[0].equals("H")) {
        val1 = float(list[1]);
        val2 = float(list[2]);
      }
    }
  }
}

/*
int val1;
 int val2;
 
 void setup() {
 Serial.begin(9600);
 }
 
 void loop() {
 val1 = analogRead(0);
 val2 = analogRead(1);
 
 analogWrite(9, val1/4);
 analogWrite(10, val2/4);
 
 Serial.print('H');
 Serial.print(',');
 Serial.print(val1);
 Serial.print(',');
 Serial.println(val2);
 delay(2);
 }
 */