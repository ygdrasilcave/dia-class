Vector a;
Vector b;

void setup(){
  size(300, 300);
  a = new Vector(4,3);
  b = new Vector(2,1);
}

void draw(){
  a.add(b);
  println(a.x + " : " + a.y);
}

class Vector{
  float x, y;
  Vector(float _x, float _y){
    x = _x;
    y = _y;
  }
  
  void add(Vector _v){
    x = x + _v.x;
    y = y + _v.y;
  }
};