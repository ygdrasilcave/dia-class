Circle[] circles;

void setup() {
  size(1920, 1080);
  background(0);
  circles = new Circle[50];

  for (int i=0; i<circles.length; i++) {
    circles[i] = new Circle(true);
  }
}

void draw() {
  //background(0);
  noStroke();
  fill(0, 10);
  rect(0, 0, width, height);
  
  for (int i=0; i<circles.length; i++) {
    circles[i].update();
    circles[i].display();
  }
}