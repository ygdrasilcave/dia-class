class Circle {
  float t, tSpeed;
  float centerX, centerY;
  float dist;
  float dia;
  float r, g, b;
  float t2, t2Speed;
  float t3, t3Speed;
  float CX, CY;

  Circle(boolean _b) {
    t = 0.0;
    tSpeed = random(0.01, 0.058);
    centerX = 0;
    centerY = 0;
    dist = random(50, 250);
    dia = random(10, 80);
    r = 255;
    g = 255;
    b = 255;

    t2 = 0.0;
    t2Speed = random(0.12, 0.35);
    t3 = 0.0;
    t3Speed = random(0.01, 0.058);
    if (_b == true) {
      CX = random(width);
      CY = random(height);
    } else {
      CX = width/2;
      CY = height/2;
    }
  }

  void update() {
    t = t + tSpeed;
    //dist = dist + sin(t2)*0.0;
    t2 = t2 + t2Speed;

    centerX = CX + cos(t3)*100*sin(t2);
    centerY = CY + sin(t3)*100*sin(t2);
    t3 = t3 + t3Speed;
  }

  void display() {
    pushMatrix();
    translate(centerX, centerY);
    rotate(t);
    stroke(r, g, b);
    noFill();
    ellipse(0, 0, dia*0.2, dia*0.2);
    noStroke();
    fill(r, g, b);
    ellipse(dist, 0, dia, dia);
    popMatrix();
  }
}