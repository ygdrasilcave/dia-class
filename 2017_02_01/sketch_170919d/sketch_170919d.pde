ArrayList<Ball> balls;

void setup() {
  size(800, 800);
  background(0);
  balls = new ArrayList<Ball>();

  /*for(int i=0; i<1000; i++){
   balls.add(new Ball());
   }*/
  println(balls.size());
}

void draw() {
  background(0);
  
  for (int i = balls.size()-1; i>=0; i--) {
    balls.get(i).display();
    balls.get(i).checkEdge();
    float x = balls.get(i).position.x;
    float y = balls.get(i).position.y;
    /*if (x > 350 && x < 450 && y > 350 && y < 450) {
      balls.remove(i);
    }*/
    if(sqrt((width/2 - x)*(width/2 - x) + (height/2 - y)*(height/2 - y)) < 100){
      balls.remove(i);
    }
  }
  println(balls.size());
  noFill();
  stroke(255);
  //rect(350, 350, 100, 100);
  ellipse(width/2, height/2, 200, 200);
}

void mouseMoved(){
  balls.add(new Ball(mouseX, mouseY));
}