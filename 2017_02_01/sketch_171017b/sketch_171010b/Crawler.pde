class Crawler {
  PVector position;
  PVector velocity;
  PVector acc;
  float maxSpeed;
  float leng;
  float ts;
  float t;
  float nx, ny, nxs, nys;
  float w;

  Crawler() {
    position = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    acc = new PVector(0, 0);
    maxSpeed = random(4, 10);;
    t = 0.0;
    ts = 0.0;
    w = random(10, 30);
    nxs = random(0.005, 0.03);
    nys = random(0.005, 0.03);
  }

  void update() {
    acc.x = map(noise(nx), 0, 1, -1, 1);
    acc.y = map(noise(ny), 0, 1, -1, 1);
    velocity.add(acc);
    velocity.limit(maxSpeed);
    position.add(velocity);
    ts = abs(acc.mag())*0.2;
    //t = t+ts;
    t = t+0.15;
    //leng = abs(sin(t))*velocity.mag()*(w/2.0);
    leng = abs(sin(t))*(w*2);
    nx += nxs;
    ny += nys;
  }
  
  void update_attract() {
    acc.x = map(noise(nx), 0, 1, -1, 1);
    acc.y = map(noise(ny), 0, 1, -1, 1);

    velocity.add(acc);
    velocity.limit(maxSpeed);
    position.add(velocity);
    ts = abs(acc.mag())*0.2;
    t = t+ts;
    //t = t+0.25;
    leng = abs(sin(t))*velocity.mag()*(w/2.0);
    //leng = abs(sin(t))*(w/2.0);
    nx += nxs;
    ny += nys;
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    strokeWeight(3);
    stroke(255);
    line(0, 0, leng, 0);
    noStroke();
    fill(255);
    ellipse(0, 0, w/2.0, w/2.0);
    ellipse(leng, 0, w, w);
    popMatrix();

    if (position.x > width) {
      position.x = 0;
    } else if (position.x < 0) {
      position.x = width;
    }
    if (position.y > height) {
      position.y = 0;
    } else if (position.y < 0) {
      position.y = height;
    }
  }
};