ArrayList<Crawler> cr;

void setup() {
  size(800, 800);
  background(0);
  cr = new ArrayList<Crawler>();
  for (int i=0; i<20; i++) {
    cr.add(new Crawler());
  }
}

void draw() {
  background(0);
  for (int i=0; i<20; i++) {
    Crawler _cr = cr.get(i);
    _cr.update();
    _cr.display();
  }
}