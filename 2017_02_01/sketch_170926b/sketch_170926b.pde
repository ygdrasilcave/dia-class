ArrayList<Ball> balls;

void setup() {
  size(800, 800);
  colorMode(HSB, 255, 255, 255);
  background(0);
  balls = new ArrayList<Ball>();
  for (int i=0; i<100; i++) {
    balls.add(new Ball());
  }
}

void draw() {
  background(0);
  for (int i=balls.size()-1; i>=0; i--) {
    Ball b1 = balls.get(i);
    b1.display();
  }
}

void mousePressed(){
  balls.clear();
  for (int i=0; i<500; i++) {
    balls.add(new Ball());
  }
}