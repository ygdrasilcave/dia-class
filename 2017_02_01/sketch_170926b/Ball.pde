class Ball {

  PVector position;
  PVector velocity;
  PVector acc;
  float maxSpeed;
  float w;
  float h, s, b;
  float accFactor;

  Ball() {
    position = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    acc = PVector.random2D();
    acc.mult(0.326);
    maxSpeed = random(2, 8);
    w = random(5, 30);
    h = random(255);
    s = 255;
    b = 255;
    accFactor = random(0.286, 0.45);
  }

  void display() {
    PVector mouse = new PVector(mouseX, mouseY);

    PVector dir = PVector.sub(mouse, position);

    dir.normalize();
    dir.mult(accFactor);
    acc = dir;

    velocity.add(acc);
    velocity.limit(maxSpeed);
    position.add(velocity); 
       
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    fill(h, s, b);
    noStroke();
    ellipse(0, 0, w, w);
    noFill();
    stroke(h, s, b);
    strokeWeight(3);
    line(0, 0, w, 0);
    popMatrix();

    if (position.x > width) {
      position.x = 0;
    } else if (position.x < 0) {
      position.x = width;
    }
    if (position.y > height) {
      position.y = 0;
    } else if (position.y < 0) {
      position.y = height;
    }
  }
};