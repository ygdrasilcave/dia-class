PVector v1, v2;
PVector vSpeed;

void setup() {
  size(600, 600);
  v1 = new PVector(40, 20);
  v2 = new PVector(250, 500);
  vSpeed = new PVector(1, 2);
}

void draw() {
  background(0);
  
  ellipse(v1.x, v1.y, 12, 12);
  ellipse(v2.x, v2.y, 12, 12);
  
  //v1.add(vSpeed);
  
  PVector v3 = PVector.add(v1, vSpeed);
  
  ellipse(v3.x, v3.y, 12, 12);
  
  //line(0, 0, v2.x, v2.y);
  
  //v2.add(v1);
  
  //ellipse(v2.x, v2.y, 24, 24);

  //line(0, 0, v1.x, v1.y);
  //line(0, 0, v2.x, v2.y);
}