float w;

void setup(){
  size(800,600);
  background(255);  
  initGUI();
  w = 0;
}

void draw(){
  background(185);
  //println(slider2d.getArrayValue()[0] + " : " + slider2d.getArrayValue()[1]); 
  w = map(sliderValue, 0, 255, 0, height);
  ellipse(width/2, height/2, w, w);
}
  