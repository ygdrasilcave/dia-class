import controlP5.*;
import java.util.*;

ControlP5 cp5;
Accordion accordion;
ColorPicker cp;
Knob myKnobA;
Slider2D slider2d;

int accordionHeight = 25;
int accordionBKHeight = 150;

int sliderValue = 200;

void initGUI() {
  cp5 = new ControlP5(this);
  ControlFont cf = new ControlFont(createFont("Arial", 20));

  Group g1 = cp5.addGroup("basic")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight + 50)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;

  cp5.addSlider("slider")
    .setPosition(10, 10)
    .setSize(50, 150)
    .setRange(0, 255)
    .setValue(128)
    .moveTo(g1)  // move to group space 
    .setFont(cf)
    ;

  cp5.addBang("bang")
    .setPosition (100, 10)
    .setSize(40, 40)
    .setTriggerEvent(Bang.RELEASE)
    .setLabel("changeBackground")
    .moveTo(g1)
    ;

  cp5.addToggle("toggle")
    .setPosition(100, 70)
    .setSize(40, 40)
    .setValue(true)
    .setMode(ControlP5.SWITCH)
    .moveTo(g1)
    ;


  Group g2 = cp5.addGroup("radioButton")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;

  cp5.addRadioButton("radio")
    .setPosition(10, 20)
    .setItemWidth(20)
    .setItemHeight(20)
    .addItem("black", 0)
    .addItem("red", 1)
    .addItem("green", 2)
    .addItem("blue", 3)
    .addItem("grey", 4)
    .setColorLabel(color(255))
    .activate(2)
    .moveTo(g2)
    ;

  Group g3 = cp5.addGroup("colorChange")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;


  cp = cp5.addColorPicker("picker")
    .setPosition(10, 10)
    .setColorValue(color(255, 128, 0, 128))
    .moveTo(g3)
    ;

  Group g4 = cp5.addGroup("knobGroup")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;

  myKnobA = cp5.addKnob("knob")
    .setRange(0, 255)
    .setValue(50)
    .setPosition(10, 10)
    .setRadius(50)
    .setDragDirection(Knob.VERTICAL)
    .moveTo(g4)
    ;

  Group g5 = cp5.addGroup("Scrollable List")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;

  List l = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h");
  /* add a ScrollableList, by default it behaves like a DropdownList */
  cp5.addScrollableList("dropdown")
    .setPosition(10, 10)
    .setSize(200, 100)
    .setBarHeight(20)
    .setItemHeight(20)
    .addItems(l)
    // .setType(ScrollableList.LIST) // currently supported DROPDOWN and LIST
    .moveTo(g5)
    ;

  Group g6 = cp5.addGroup("number box group")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;

  cp5.addNumberbox("numberbox")
    .setPosition(10, 10)
    .setSize(100, 20)
    .setRange(0, 200)
    .setScrollSensitivity(1.1)
    .setValue(50)
    .moveTo(g6)
    ;

  Group g7 = cp5.addGroup("slider2d group")
    .setBackgroundColor(color(0, 64))
    .setBackgroundHeight(accordionBKHeight)
    .setHeight(accordionHeight)
    .setFont(cf)
    ;

  slider2d = cp5.addSlider2D("wave")
    .setPosition(10, 10)
    .setSize(100, 100)
    .setMinMax(0, 0, 100, 100)
    .setValue(50, 50)
    //.disableCrosshair()
    .moveTo(g7)
    ;


  accordion = cp5.addAccordion("acc")
    .setPosition(10, 10)
    .setWidth(280)
    .addItem(g1)
    .addItem(g2)
    .addItem(g3)
    .addItem(g4)
    .addItem(g5)
    .addItem(g6)
    .addItem(g7)
    ;

  accordion.open(4);
}

void slider(int value) {
  println(value);
  sliderValue = value;
}

void bang() {
  println("bang");
}

void toggle(boolean value) {
  if (value == true) {
    println("ON");
  } else {
    println("OFF");
  }
}

void radio(int value) {
  if (value == 0) {
    println(value);
  } else if ( value == 1) {
    println(value);
  } else if ( value == 2) {
    println(value);
  } else if ( value == 3) {
    println(value);
  } else if ( value == 4) {
    println(value);
  }
} 

void picker(int col) {
  println("red: " + red(col) + " blue: " + blue(col) + " green: " + green(col) + " alpha:" + alpha(col));
}

void knob(int value) {
  println("knob value: " + value);
}

void dropdown(int n) {
  println(n, cp5.get(ScrollableList.class, "dropdown").getItem(n));
}

void numberbox(int value) {
  println("numberbox: " + value);
}

/*
void wave(){
 println(slider2d.getArrayValue()[0] + " : " + slider2d.getArrayValue()[1]);
 }
 */