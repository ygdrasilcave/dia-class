int sunNum = 3;
int snuNumDir = 1;
int prevSecond = 0;

int starNum = 1000;
float[] starDist;
float[] starDistSpeed;
float[] starRot;
float[] starSize;

float rotation = 0;
float revolution = 0;

int traceNum = 100;
float[] traceX;
float[] traceY;
int traceIndex = 0;
float theta = 0;

void setup() {
  size(1200, 800);
  background(0);

  starDist = new float[starNum];
  starDistSpeed = new float[starNum];
  starRot = new float[starNum];
  starSize = new float[starNum];
  for (int i=0; i<starNum; i++) {
    starDist[i] = 0;
    starDistSpeed[i] = random(0.25, 5);
    starRot[i] = random(0, 360);
    starSize[i] = random(1, 5);
  }

  traceX = new float[traceNum];
  traceY = new float[traceNum];
  for (int i=0; i<traceNum; i++) {
    traceX[i] = 0;
    traceY[i] = 0;
  }
}

void draw() {
  background(0);

  star(mouseX, mouseY);

  sun(mouseX, mouseY, 40, sunNum);  
  //sun(width/2, height/2, 60, 5);

  earth(mouseX, mouseY, 350, 35, 150);

  if (prevSecond != second()) {
    sunNum = sunNum + snuNumDir;
    if (sunNum > 12) {
      sunNum = 12;
      snuNumDir = -1;
    } else if (sunNum < 3) {
      sunNum = 3;
      snuNumDir = 1;
    }
    prevSecond = second();
  }
}

void sun(float _x, float _y, float _r, int _n) {
  pushMatrix();
  translate(_x, _y);
  fill(255);
  noStroke();
  beginShape();
  for (int i=0; i<_n; i++) {
    float t = radians((360.0/_n)*i);
    float x = cos(t) * _r;
    float y = sin(t) * _r;
    vertex(x, y);
  }  
  endShape(CLOSE);

  for (int i=0; i<_n; i++) {
    float t = radians((360.0/_n)*i);
    pushMatrix();
    rotate(t);
    rect(_r, -(_r*0.25)*0.5, _r, _r*0.25);
    popMatrix();
  }
  popMatrix();
}

void star(float _x, float _y) {
  pushMatrix();
  translate(_x, _y);
  for (int i=0; i<starNum; i++) {
    starDist[i] = starDist[i] + starDistSpeed[i];
    float sx = cos(radians(starRot[i])) * starDist[i];
    float sy = sin(radians(starRot[i])) * starDist[i];
    strokeWeight(starSize[i]);
    stroke(255);
    point(sx, sy);

    if (starDist[i] > width/2) {
      starDist[i] = 0;
      starDistSpeed[i] = random(0.25, 5);
      starRot[i] = random(0, 360);
      starSize[i] = random(1, 5);
    }
  }
  popMatrix();
}


void earth(float _x, float _y, float _dist, float _r, float _dist2) {
  pushMatrix();
  translate(_x, _y);
  pushMatrix();
  float x = cos(radians(revolution)) * (_dist+sin(theta)*20);
  float y = sin(radians(revolution)) * (_dist+sin(theta)*20);
  traceX[traceIndex] = x;
  traceY[traceIndex] = y;
  traceIndex++;
  if (traceIndex > traceNum - 1) {
    traceIndex = 0;
  }
  translate(x, y);
  rotate(radians(rotation));
  noStroke();
  fill(255);
  rect(-(_r*0.5), -(_r*0.5), _r, _r);

  noFill();
  stroke(255);
  strokeWeight(1);
  line(0, 0, _r, 0);
  ellipse(0, 0, _dist2, _dist2);

  pushMatrix();
  translate(_dist2*0.5, 0);
  rotate(radians(rotation));
  fill(255);
  noStroke();
  rect(-(_r*0.25), -(_r*0.25), (_r*0.5), (_r*0.5));
  noFill();
  stroke(255);
  strokeWeight(1);
  line(0, 0, 20, 0);
  popMatrix();

  popMatrix();

  popMatrix();

  rotation = rotation + 3;
  revolution = revolution + 0.5;
  theta = theta + 0.065;

  pushMatrix();
  translate(mouseX, mouseY);
  for (int i=0; i<traceNum; i++) {
    strokeWeight(3);
    stroke(255);
    point(traceX[i], traceY[i]);
  }
  popMatrix();
}
