int rectSize = 10;

void setup() {
  size(800, 800);
}

void draw() {
  background(255, 0, 0);

  /*rect(0, 0, 20, 20);
   rect(20, 0, 20, 20);
   rect(40, 0, 20, 20);*/


  /*for(int i=0; i<width; i = i+20){
   
   for(int j=0; j<height; j = j+20){
   
   fill(255);
   
   rect(i, j, 20, 20);
   
   }
   }*/
   
  rectSize = int(map(mouseX, 0, width, 20, 150));

  for (int i=0; i<width/rectSize; i = i+1) {

    for (int j=0; j<height/rectSize; j = j+1) {

      /*if (j % 2 == 0) {
        if (i % 2 == 0) {
          fill(255);
        } else if (i % 2 == 1) {
          fill(0);
        }
      } 
      else {
        if (i % 2 == 0) {
          fill(0);
        } else if (i % 2 == 1) {
          fill(255);
        }
      }*/
      
      /*if(i%2==0 && j%2==0){
        fill(0);
      }else if(i%2==1 && j%2==0){
        fill(255);
      }else if(i%2==0 && j%2==1){
        fill(255);
      }else if(i%2==1 && j%2==1){
        fill(0);
      }*/
      
      if((i+j)%2==0){
        fill(255);
      }else{
        fill(0);
      }

      rect(rectSize*i, rectSize*j, rectSize, rectSize);
    }
  }
}