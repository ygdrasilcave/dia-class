PImage img;

import oscP5.*;
import netP5.*;

OscP5 oscP5;

float val = 0.0;

void setup(){
  size(600, 600);
  img = loadImage("face.jpg");  
  oscP5 = new OscP5(this,12000);
}

void draw(){
  background(0);
  //image(img, 0, 0, width, height);
    
  //color c = img.pixels[mouseX + mouseY * img.width];
  //fill(r, r, b);
  //ellipse(mouseX, mouseY, 20, 20);
  //int pixelSize = int(map(mouseX, 0, width, 1, 20));
  int pixelSize = 1;
  noStroke();
  for(int x = 0; x < img.width; x+=pixelSize){
    for(int y = 0; y < img.height; y+=pixelSize){
      int index = x + y * img.width;
      color imageColor = img.pixels[index];
      float r = red(imageColor);
      float g = green(imageColor);
      float b = blue(imageColor);
      if(r > val*255){
        fill(255);
      }else{
        fill(0);
      }
      rect(x, y, pixelSize, pixelSize);
    }
  }
}

void oscEvent(OscMessage theOscMessage) {  
  if(theOscMessage.checkAddrPattern("/1/fader5")==true) {
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      //int firstValue = theOscMessage.get(0).intValue();  
      //float secondValue = theOscMessage.get(1).floatValue();
      //String thirdValue = theOscMessage.get(2).stringValue();
      val = value;
      //println(value);
      return;
    }  
  } 
}
