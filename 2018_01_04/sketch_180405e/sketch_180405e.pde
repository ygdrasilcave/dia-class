float x0;
float y0;
float dirX0;
float speedX0;
float dirY0;
float speedY0;
float w0;

float x1;
float y1;
float dirX1;
float speedX1;
float dirY1;
float speedY1;
float w1;

float x2;
float y2;
float dirX2;
float speedX2;
float dirY2;
float speedY2;
float w2;

void setup(){
  size(800, 800);
  background(255);
  w0 = 60;
  dirX0 = 1;
  dirY0 = 1;
  speedX0 = 6;
  speedY0 = 8;
  
  w1 = 60;
  dirX1 = 1;
  dirY1 = 1;
  speedX1 = 4;
  speedY1 = 7;
  
  w2 = 60;
  dirX2 = 1;
  dirY2 = 1;
  speedX2 = 12;
  speedY2 = 9;
  
  println(x0);
}

void draw(){
  background(255);
  
  noFill();
  stroke(0);
  ellipse(x0, y0, w0, w0);
  ellipse(x1, y1, w1, w1);
  ellipse(x2, y2, w2, w2);
  
  x0 = x0 + dirX0*speedX0;
  y0 = y0 + dirY0*speedY0;
  x1 = x1 + dirX1*speedX1;
  y1 = y1 + dirY1*speedY1;
  x2 = x2 + dirX2*speedX2;
  y2 = y2 + dirY2*speedY2;
  
  if(x0 > width){
    x0 = width;
    dirX0 *= -1;  //dirX = dirX * -1;
  }else if(x0 < 0){
    x0 = 0;
    dirX0 *= -1;
  }
  
  if(y0 > height){
    y0 = height;
    dirY0 *= -1;
  }else if(y0 < 0){
    y0 = 0;
    dirY0 *= -1;
  }
  
  if(x1 > width){
    x1 = width;
    dirX1 *= -1;  //dirX = dirX * -1;
  }else if(x1 < 0){
    x1 = 0;
    dirX1 *= -1;
  }
  
  if(y1 > height){
    y1 = height;
    dirY1 *= -1;
  }else if(y1 < 0){
    y1 = 0;
    dirY1 *= -1;
  }
  
  if(x2 > width){
    x2 = width;
    dirX2 *= -1;  //dirX = dirX * -1;
  }else if(x2 < 0){
    x2 = 0;
    dirX2 *= -1;
  }
  
  if(y2 > height){
    y2 = height;
    dirY2 *= -1;
  }else if(y2 < 0){
    y2 = 0;
    dirY2 *= -1;
  }
}
