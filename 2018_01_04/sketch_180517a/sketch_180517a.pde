PImage img;

void setup() {
  size(800, 800);
  img = loadImage("face.jpg");
}

void draw() {
  background(0);
  for (int i=0; i<8; i++) {
    for (int j=0; j<8; j++) {
      if (j%2 == 0) {
        pushMatrix();
        translate(i*100+50, j*100+50);
        rotate(0);
        image(img, -50, -50, 100, 100);
        popMatrix();
      }else{
        pushMatrix();
        translate(i*100+50, j*100+50);
        rotate(PI);
        image(img, -50, -50, 100, 100);
        popMatrix();
      }
    }
  }
}
