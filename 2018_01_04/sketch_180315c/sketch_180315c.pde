void setup() {
  size(800, 600);
  background(255);
  println("setup: " + (400-100));
  //frameRate(60);
}

void draw() {
  background(255);
  fill(255, 255, 0);
  //rect(mouseX-100, mouseY-50, 200, 100);
  line(0, 0, mouseX, mouseY);
  line(800, 0, mouseX, mouseY);
  ellipse(mouseX, mouseY, 200, 100);
  //println("draw =>  " + (mouseX-100));
  
  fill(255, 0, 255);
  strokeWeight(5);
  stroke(0);
  beginShape();
  vertex(mouseX-100+50, mouseY-50);
  vertex(mouseX-100+200-50, mouseY-50);
  vertex(mouseX-100+200, mouseY-50+100);
  vertex(mouseX, mouseY + 50 + 70);
  vertex(mouseX-100, mouseY-50+100);
  endShape(CLOSE);
  
  println(mouseX + " : " + mouseY);
}

void keyPressed(){
  saveFrame("myWork.jpg");
}