int w;
float f;

void setup() {
  size(800, 1000);
  f = 0.8;
}

void draw() {
  background(0);
  /* if(width >= height){
   w = height;
   } else  if(width < height) {
   w = width;
   } else if(width == height){
   w = width;
   }*/
   
  f = map(mouseX, 0, width, 0.1, 1);

  if (width >=height) {
    w = height;
  } else {
    w = width;
  }
  
  noStroke();
  fill(255);
  ellipse(width/2, height/2, w*f, w*f);
  
  stroke(255, 0, 0);
  strokeWeight(3);
  line(width/2-w*(f*0.5), height/2, width/2+w*(f*0.5), height/2);
  ellipse(width/2, height/2-w*(f*0.25), w*(f*0.25), w*(f*0.25));
  ellipse(width/2, height/2+w*(f*0.25), w*(f*0.1), w*(f*0.1));
}