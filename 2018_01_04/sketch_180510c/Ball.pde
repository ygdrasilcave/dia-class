class Ball {
  float x;
  float y;
  float xSpeed;
  float ySpeed;
  float xSpeedDir;
  float ySpeedDir;
  float r, g, b;
  float w;
  int vertexNum = 16;

  Ball() {
    w = random(10, 50);
    x = random(w, width-w);
    y = random(w, height-w);
    xSpeed = random(2, 5);
    ySpeed = random(3, 6);
    if (random(10) < 5) {
      xSpeedDir = -1;
    } else {
      xSpeedDir = 1;
    }
    if (random(10) < 5) {
      ySpeedDir = -1;
    } else {
      ySpeedDir = 1;
    }
    r = random(255);
    g = random(255);
    b = random(255);
  }

  void updateBall() {
    x += xSpeed*xSpeedDir;
    y += ySpeed*ySpeedDir;

    if (x > width-w/2) {
      x = width-w/2;
      xSpeedDir = xSpeedDir*-1;
      vertexNum--;
      if (vertexNum < 3) {
        vertexNum = 3;
      }
    } else if (x < w/2) {
      x = w/2;
      xSpeedDir = xSpeedDir*-1;
      vertexNum--;
      if (vertexNum < 3) {
        vertexNum = 3;
      }
    }

    if (y > height-w/2) {
      y = height-w/2;
      ySpeedDir = ySpeedDir*-1;
      vertexNum--;
      if (vertexNum < 3) {
        vertexNum = 3;
      }
    } else if (y < w/2) {
      y = w/2;
      ySpeedDir = ySpeedDir*-1;
      vertexNum--;
      if (vertexNum < 3) {
        vertexNum = 3;
      }
    }
  }

  void drawBall(boolean _b) {
    if (_b == true) {
      noStroke();
      fill(r, g, b);
    } else {
      stroke(r, g, b);
      noFill();
    }

    beginShape();
    for (int k=0; k<vertexNum; k++) {
      float _t = radians((360/vertexNum)*k);
      float _x = x + cos(_t)*w/2;
      float _y = y + sin(_t)*w/2;
      vertex(_x, _y);
    }
    endShape(CLOSE);
  }

  void resetBall() {
    vertexNum = 16;
  }
}
