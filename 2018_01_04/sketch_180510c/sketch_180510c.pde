Ball[] balls;
int ballNum = 500;
boolean colMode = true;

void setup() {
  size(1000, 800);
  background(0);
  balls = new Ball[ballNum];
  for (int i=0; i<ballNum; i++) {
    balls[i] = new Ball();
  }
}

void draw() {
  background(0);
  for (int i=0; i<ballNum; i++) {
    balls[i].updateBall();
    if (i%2 == 0) {
      balls[i].drawBall(colMode);
    } else {
      balls[i].drawBall(!colMode);
    }
  }
}

void keyPressed() {
  if (key == ' ') {
    for (int i=0; i<ballNum; i++) {
      //balls[i].vertexNum = 16;
      balls[i].resetBall();
    }
  } else if (key == 'a') {
    colMode = true;
  } else if (key == 'b') {
    colMode = false;
  }
}
