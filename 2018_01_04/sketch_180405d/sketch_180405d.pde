float x;
float y;
float speedX = 20;
float dirX = 1;
float speedY = 40;
float dirY = 1;
float w = 260;

void setup(){
  size(800, 800);
  background(255);
  x = width/2;
  y = height/2;
}

void draw(){  
  noFill();
  stroke(0);
  ellipse(x, y, w, w);
  
  x = x + dirX*speedX;
  y = y + dirY*speedY;
  
  if(x > width){
    x = width;
    dirX = -1;
  }else if(x < 0){
    x = 0;
    dirX = 1;
  }
  
  if(y > height){
    y = height;
    dirY = -1;
  }else if(y < 0){
    y = 0;
    dirY = 1;
  }
  
  //speedX = map(mouseX, 0, width, 0, 20);
  //speedY = map(mouseY, 0, width, 0, 20);
}
