Ball[] balls;
int ballNum = 500;

void setup() {
  size(1000, 800);
  background(0);
  balls = new Ball[ballNum];
  for (int i=0; i<ballNum; i++) {
    balls[i] = new Ball();
  }
}

void draw() {
  background(0);
  for (int i=0; i<ballNum; i++) {
    balls[i].updateBall();
    balls[i].drawBall();
  }
}
