class Ball {
  float x;
  float y;
  float xSpeed;
  float ySpeed;
  float xSpeedDir;
  float ySpeedDir;
  float r, g, b;
  float w;
  int shape;

  Ball() {
    w = random(10, 50);
    x = random(w, width-w);
    y = random(w, height-w);
    xSpeed = random(2, 5);
    ySpeed = random(3, 6);
    if (random(10) < 5) {
      xSpeedDir = -1;
    } else {
      xSpeedDir = 1;
    }
    if (random(10) < 5) {
      ySpeedDir = -1;
    } else {
      ySpeedDir = 1;
    }
    r = random(255);
    g = random(255);
    b = random(255);  

    if (random(10) < 9.7) {
      shape = 0;
    } else {
      shape = 1;
    }
  }

  void updateBall() {
    x += xSpeed*xSpeedDir;
    y += ySpeed*ySpeedDir;

    if (x > width-w/2) {
      x = width-w/2;
      xSpeedDir = xSpeedDir*-1;
    } else if (x < w/2) {
      x = w/2;
      xSpeedDir = xSpeedDir*-1;
    }

    if (y > height-w/2) {
      y = height-w/2;
      ySpeedDir = ySpeedDir*-1;
    } else if (y < w/2) {
      y = w/2;
      ySpeedDir = ySpeedDir*-1;
    }
  }

  void drawBall() {
    noStroke();
    fill(r, g, b);
    if (shape == 0) {
      ellipse(x, y, w, w);
    } else if (shape == 1){
      rect(x-w/2, y-w/2, w, w);
    }
  }
}
