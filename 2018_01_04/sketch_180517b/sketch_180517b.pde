PImage img;

void setup(){
  size(600, 600);
  img = loadImage("face.jpg");
}

void draw(){
  background(0);
  //image(img, 0, 0, width, height);
    
  //color c = img.pixels[mouseX + mouseY * img.width];
  //fill(r, r, b);
  //ellipse(mouseX, mouseY, 20, 20);
  //int pixelSize = int(map(mouseX, 0, width, 1, 20));
  int pixelSize = 1;
  noStroke();
  for(int x = 0; x < img.width; x+=pixelSize){
    for(int y = 0; y < img.height; y+=pixelSize){
      int index = x + y * img.width;
      color imageColor = img.pixels[index];
      float r = red(imageColor);
      float g = green(imageColor);
      float b = blue(imageColor);
      if(r > (255/3) * 2){
        fill(255);
      }else if(r > 255/3 && r <= (255/3) * 2){
        fill(255, 255, 0);
      }else{
        fill(0);
      }
      rect(x, y, pixelSize, pixelSize);
    }
  }
}
