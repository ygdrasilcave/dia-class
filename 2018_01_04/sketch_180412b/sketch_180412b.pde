float[] centerX;
float[] centerY;
float[] speedX;
float[] speedY;
float[] dirX;
float[] dirY;
float[] r;
int[] numOfPoints;
float[] red;
float[] green;
float[] blue;
float[] scale;
int num = 500;
float t = 0.0;

void setup() {
  size(800, 800);
  background(255);

  centerX = new float[num];
  centerY = new float[num];
  speedX = new float[num];
  speedY = new float[num];
  dirX = new float[num];
  dirY = new float[num];
  r = new float[num];
  red = new float[num];
  green = new float[num];
  blue = new float[num];
  numOfPoints = new int[num];
  scale = new float[num];
  for (int i=0; i<num; i++) {
    centerX[i] = random(width);
    centerY[i] = random(height);
    speedX[i] = random(1, 4);
    speedY[i] = random(2, 6);
    dirX[i] = 1;
    dirY[i] = 1;
    r[i] = random(10, 80);
    red[i] = random(255);
    green[i] = random(255);
    blue[i] = random(255);
    scale[i] = random(0.1, 0.85);
    numOfPoints[i] = int(random(3, 65));
    if(numOfPoints[i]%2 != 0 && numOfPoints[i] >= 5){
      numOfPoints[i] = numOfPoints[i]+1;
    }
  }
}

void draw() {
  background(255);
  //stroke(0);
  //noFill();
  noStroke();
  for (int n=0; n<num; n++) {
    fill(red[n], green[n], blue[n]);
    beginShape();
    for (int i=0; i<numOfPoints[n]; i++) {
      float x;
      float y;
      if (i%2 == 0) {
        x = cos(radians((360.0/numOfPoints[n])*i)) * r[n];
        y = sin(radians((360.0/numOfPoints[n])*i)) * r[n];
      } else {
        x = cos(radians((360.0/numOfPoints[n])*i)) * (r[n]*scale[n]*sin(t));
        y = sin(radians((360.0/numOfPoints[n])*i)) * (r[n]*scale[n]*sin(t));
      }
      vertex(centerX[n] + x, centerY[n] + y);
    }
    endShape(CLOSE);
    
    centerX[n] += dirX[n]*speedX[n];
    centerY[n] += dirY[n]*speedY[n];
    
    if(centerX[n] > width){
      centerX[n] = width;
      dirX[n] *= -1;
    }else if(centerX[n] < 0){
      centerX[n] = 0;
      dirX[n] *= -1;
    }
    
    if(centerY[n] > height){
      centerY[n] = height;
      dirY[n] *= -1;
    }else if(centerY[n] < 0){
      centerY[n] = 0;
      dirY[n] *= -1;
    }
  }
  
  t += 0.025;
}