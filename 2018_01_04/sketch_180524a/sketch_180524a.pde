int counter = 0;
boolean toggle = true;
float h = 255;
float s = 255;
float b = 255;

int scene = 1;

void setup() {
  size(600, 600);
  background(0);
  rectMode(CENTER);
  colorMode(HSB, 255);
}

void draw() {
  background(0);
  /*fill(h, s, b);
   noStroke();
   rect(width/2, height/2, 200, 200);*/
  /*if (scene == 1) {
   scene1();
   } else if (scene == 2) {
   scene2();
   } else if (scene == 3) {
   scene3();
   } else if (scene == 4) {
   scene4();
   } else if (scene == 5) {
   scene5();
   }*/
  scene(scene);
}

void keyPressed() {
  if (key == 'a' || key == 'A') {
    if (toggle == true) {
      println("a preseed : " + counter);
      counter++;
      toggle = false;
      h = random(255);
      s = 255;
      b = 255;
      scene = 1;
    }
  }
  if (key == 'b' || key == 'B') {
    if (toggle == true) {
      scene = 2;
    }
  }
  if (key == 'g' || key == 'G') {
    if (toggle == true) {
      scene = 3;
    }
  }
  if (key == 'h' || key == 'H') {
    if (toggle == true) {
      scene = 4;
    }
  }
  if (key == '9' || key == '(') {
    if (toggle == true) {
      scene = 5;
    }
  }
}

void keyReleased() {
  toggle = true;
}

void scene1() {
  fill(255);
  rect(width/2, height/2, 200, 200);
}
void scene2() {
  fill(0, 255, 255);
  rect(width/2, height/2, 200, 200);
}
void scene3() {
  fill(120, 255, 255);
  rect(width/2, height/2, 200, 200);
}
void scene4() {
  fill(27, 255, 255);
  rect(width/2, height/2, 200, 200);
}
void scene5() {
  fill(200, 255, 255);
  rect(width/2, height/2, 200, 200);
}

void scene(int _s) {
  if (_s == 1) {
    fill(255);
  } else if (_s == 2) {
    fill(0, 255, 255);
  } else if (_s == 3) {
    fill(120, 255, 255);
  } else if (_s == 4) {
    fill(26, 255, 255);
  } else if (_s == 5) {
    fill(200, 255, 255);
  }
  rect(width/2, height/2, 200, 200);
}
