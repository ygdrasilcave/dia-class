float x;
float y;
float speedX = 6;
float dirX = 1;
float speedY = 6;
float dirY = 1;

float w = 10;
float dirW = 1;
float speedW = 3;

void setup(){
  size(800, 800);
  background(0);
  x = width/2;
  y = height/2;
}

void draw(){
  //background(255);
  //noStroke();
  //fill(0, 5);
  //rect(0, 0, width, height);
  
  noFill();
  stroke(0, map(w, 5, 150, 0, 255), map(w, 5, 150, 0, 255));
  ellipse(x, y, w, w);
  
  x = x + dirX*speedX;
  y = y + dirY*speedY;
  
  w = w + dirW*speedW;
  if(w > 150){
    dirW = dirW * -1;
  }else if(w < 5){
    dirW = dirW * -1;
  }
  
  if(x > width){
    dirX = -1;
  }else if(x < 0){
    dirX = 1;
  }
  
  if(y > height){
    dirY = -1;
  }else if(y < 0){
    dirY = 1;
  }
  
  speedX = map(mouseX, 0, width, 0, 20);
  speedY = map(mouseY, 0, width, 0, 20);
}
