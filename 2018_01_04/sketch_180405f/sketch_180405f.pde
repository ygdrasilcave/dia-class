float[] x;
float[] y;
float[] speedX;
float[] speedY;
float[] dirX;
float[] dirY;
float[] w;
float[] r;
float[] g;
float[] b;

int num = 100;

void setup() {
  size(800, 800);
  background(255);
  x = new float[num];
  y = new float[num];
  speedX = new float[num];
  speedY = new float[num];
  dirX = new float[num];
  dirY = new float[num];
  w = new float[num]; 
  r = new float[num];
  g = new float[num];
  b = new float[num];

  for (int i=0; i<num; i++) {
    x[i] = width/2;
    y[i] = height/2;
    w[i] = random(10, 60);
    speedX[i] = random(2, 9);
    speedY[i] = random(4, 12);
    if (i%2 == 0) {
      dirX[i] = 1;
      dirY[i] = -1;
    } else {
      dirX[i] = -1;
      dirY[i] = 1;
    }
    r[i] = random(255);
    g[i] = random(255);
    b[i] = random(255);
  }
}

void draw() {
  background(255);

  //noFill();
  //stroke(0);
  noStroke();

  for (int i=0; i<num; i++) {
    fill(r[i], g[i], b[i]);
    ellipse(x[i], y[i], w[i], w[i]);

    x[i] = x[i] + dirX[i]*speedX[i];
    y[i] = y[i] + dirY[i]*speedY[i];

    if (x[i] > width) {
      x[i] = width;
      dirX[i] *= -1;
    } else if (x[i] < 0) {
      x[i] = 0;
      dirX[i] *= -1;
    }

    if (y[i] > height) {
      y[i] = height;
      dirY[i] *= -1;
    } else if (y[i] < 0) {
      y[i] = 0;
      dirY[i] *= -1;
    }
  }
}

void keyPressed() {
  if (key == 'u') {
    for (int i = 0; i<num; i++) {
      speedX[i] = speedX[i] + 1;
      speedY[i] = speedY[i] + 1;
    }
  }
  if (key == 'd') {
    for (int i = 0; i<num; i++) {
      speedX[i] = speedX[i] - 1;
      speedY[i] = speedY[i] - 1;
      if (speedX[i] < 0 ) {
        speedX[i] = 0;
      }
      if (speedY[i] < 0) {
        speedY[i] = 0;
      }
    }
  }
}

