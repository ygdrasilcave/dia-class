float subR = 0;
float speedSubR = 1;
float dirSubR = 1;

int numOfPoints = 3;
int prevSecond = 0;

void setup() {
  size(800, 800);
  background(255);
}

void draw() {
  background(255);

  float centerX = mouseX;
  float centerY = mouseY;
  float r = 100;

  fill(0);
  noStroke();
  ellipse(centerX, centerY, 5, 5);

  stroke(0);
  noFill();
  beginShape();
  //vertex(centerX, centerY - r);
  //vertex(centerX + sqrt(3)*r*0.5, centerY + r*0.5);
  //vertex(centerX - sqrt(3)*r*0.5, centerY + r*0.5);

  /*float x0 = cos(radians(0)) * r;
   float y0 = sin(radians(0)) * r;
   float x1 = cos(radians(120)) * r;
   float y1 = sin(radians(120)) * r;
   float x2 = cos(radians(240)) * r;
   float y2 = sin(radians(240)) * r;
   
   vertex(centerX + x0, centerY + y0);
   vertex(centerX + x1, centerY + y1);
   vertex(centerX + x2, centerY + y2);*/

  for (int i=0; i<numOfPoints; i++) {
    float x;
    float y;
    if (i%2 == 0) {
      x = cos(radians((360.0/numOfPoints)*i)) * r;
      y = sin(radians((360.0/numOfPoints)*i)) * r;
    } else {
      x = cos(radians((360.0/numOfPoints)*i)) * (r-subR);
      y = sin(radians((360.0/numOfPoints)*i)) * (r-subR);
    }
    vertex(centerX + x, centerY + y);
  }

  endShape(CLOSE);

  subR = subR + dirSubR*speedSubR;
  if (subR >= r) {
    subR = r;
    dirSubR *= -1;
  } else if (subR <= 5) {
    subR = 5;
    dirSubR *= -1;
  }

  if (prevSecond != second()) {
    numOfPoints++;
    prevSecond = second();
    if(numOfPoints > 80){
      numOfPoints = 3;
    }
  }
}
