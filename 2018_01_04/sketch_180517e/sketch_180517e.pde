import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

float x, y;

void setup() {
  size(600,600);
  oscP5 = new OscP5(this,12000);
  myRemoteLocation = new NetAddress("127.0.0.1",12000);
  background(0);  
}

void draw() {
  noStroke();
  fill(0, 20);
  rect(0, 0, width, height);
  //fill(255);
  //ellipse(x, y, 20, 20);
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(y));
  stroke(255);
  noFill();  
  rect(-x/2, -x/2, x, x);
  popMatrix();
  
}

void keyPressed(){
  background(0);
}


void mouseMoved() {
  OscMessage myMessage = new OscMessage("/test");
  
  /*myMessage.add(int(random(200)));
  myMessage.add(random(200));
  myMessage.add("some text" + int(random(100)));*/
  myMessage.add(float(mouseX));
  myMessage.add(float(mouseY));

  oscP5.send(myMessage, myRemoteLocation); 
}


void oscEvent(OscMessage theOscMessage) {
  if(theOscMessage.checkAddrPattern("/test")==true) {
    //println("pass");
    if(theOscMessage.checkTypetag("ff")) {
      /*int firstValue = theOscMessage.get(0).intValue();  
      float secondValue = theOscMessage.get(1).floatValue();
      String thirdValue = theOscMessage.get(2).stringValue();
      print("### received an osc message /test with typetag ifs.");
      println(" values: "+firstValue+", "+secondValue+", "+thirdValue);*/
      x = theOscMessage.get(0).floatValue();
      y = theOscMessage.get(1).floatValue();
      println("msg : " + x + ", " + y);
      return;
    }  
  } 
}
