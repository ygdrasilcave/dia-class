float t = 0.0;
boolean bang = false;
int prevS;

void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);
  int h = hour();
  int m = minute();
  int s = second();
  println(h + ":" + m + ":" + s);

  drawHour();

  fill(255);
  stroke(255);
  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(s*6 - 90));
  //rect(-50, -3, 400, 6);
  beginShape();
  for (int i=0; i<3; i++) {
    float x = cos(radians(120*i))*25;
    float y = sin(radians(120*i))*25;
    if (i == 0) {
      vertex(x+sin(t)*350 + 20, y);
    } else {
      vertex(x+sin(t)*350, y);
    }
  }
  endShape(CLOSE);
  popMatrix();
  

  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(m*6 - 90));
  rect(-50, -3, 300, 6);
  popMatrix();

  pushMatrix();
  translate(width/2, height/2);
  rotate(radians(((h%12)*30 - 90) + (30.0/60)*m));
  rect(-50, -3, 200, 6);
  popMatrix();

  if (bang == true) {
    t += 0.068;
    if (sin(t) < 0) {
      t = 0.0;
      bang = false;
    }
  }

  if (prevS != s) {
    bang = true;
    t = 0.0;
    prevS = s;
  }
}

void drawHour() {
  fill(255);
  for (int i=0; i<12; i++) {
    float t = radians(360/12 * i);
    float x = cos(t)*(width/2-20) + width/2;
    float y = sin(t)*(height/2-20) + height/2;
    ellipse(x, y, 20, 20);
  }
}

