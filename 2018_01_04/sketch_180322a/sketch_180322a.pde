int w;

void setup(){
  size(200, 200);
  w = 0;
  println(w);
}

void draw(){
  background(0);
  
  if(width > height){
    w = height;
  }else if(width < height){
    w = width;
  }else if(width == height){
    w = width;
  }
  
  /*if(width > height){
    w = height;
  }else{
    w = width;
  }*/
  
  ellipse(width/2, height/2, w*0.8, w*0.8);
}