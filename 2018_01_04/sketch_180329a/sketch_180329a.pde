float stepSize = 20;

void setup(){
  size(800,800);
}

void draw(){
  background(255);
  noFill();
  stroke(0);
  
  stepSize = int(map(mouseX, 0, width, 100, 2));
 
  /*ellipse(width/2 - (stepSize/2)*0, height/2, width - stepSize*0, height - stepSize*0);
  ellipse(width/2 - (stepSize/2)*1, height/2, width - stepSize*1, height - stepSize*1);
  ellipse(width/2 - (stepSize/2)*2, height/2, width - stepSize*2, height - stepSize*2);
  ellipse(width/2 - (stepSize/2)*3, height/2, width - stepSize*3, height - stepSize*3);
  ellipse(width/2 - (stepSize/2)*4, height/2, width - stepSize*4, height - stepSize*4);
  ellipse(width/2 - (stepSize/2)*5, height/2, width - stepSize*5, height - stepSize*5);*/
  
  for(int i = 0; i < int(width/stepSize); i = i+1){
    //println(i);
    ellipse(width/2 - (stepSize/2)*i, height/2, width - stepSize*i, height - stepSize*i);
  }
  
}