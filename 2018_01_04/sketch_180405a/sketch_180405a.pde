float x;
float y;
float speedX = 6;
float dirX = 1;
float speedY = 6;
float dirY = 1;
float w = 30;

void setup(){
  size(800, 800);
  background(255);
  x = width/2;
  y = height/2;
}

void draw(){
  //background(255);
  
  noFill();
  stroke(0);
  rect(x-w/2, y-w/2, w, w);
  
  x = x + dirX*speedX;
  y = y + dirY*speedY;
  
  if(x > width){
    dirX = -1;
  }else if(x < 0){
    dirX = 1;
  }
  
  if(y > height){
    dirY = -1;
  }else if(y < 0){
    dirY = 1;
  }
  
  speedX = map(mouseX, 0, width, 0, 20);
  speedY = map(mouseY, 0, width, 0, 20);
}
