import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

void setup() {
  size(400, 400);
  oscP5 = new OscP5(this, 12000);
  myRemoteLocation = new NetAddress("127.0.0.1", 4559);
}

void draw() {
  background(255, 255, 0);
}

void keyPressed() {
  if (key == '1') {
    OscMessage myMessage = new OscMessage("/trigger");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation);
  }
  if (key == '2') {
    OscMessage myMessage = new OscMessage("/trigger");
    myMessage.add(2);
    oscP5.send(myMessage, myRemoteLocation);
  }
  if (key == '3') {
    OscMessage myMessage = new OscMessage("/trigger");
    myMessage.add(3);
    oscP5.send(myMessage, myRemoteLocation);
  }
  
  if (key == '4') {
    OscMessage myMessage = new OscMessage("/trigger");
    myMessage.add(4);
    int rate;
    if(random(1) > 0.5){
      rate = 1;
    }else{
      rate = -1;
    }
    myMessage.add(rate);
    println(rate);
    oscP5.send(myMessage, myRemoteLocation);
  }
  
  if (key == '5') {
    OscMessage myMessage = new OscMessage("/trigger");
    myMessage.add(5);
    oscP5.send(myMessage, myRemoteLocation);
  }
  
  if (key == '6') {
    OscMessage myMessage = new OscMessage("/trigger");
    myMessage.add(6);
    myMessage.add(int(random(40, 60)));
    oscP5.send(myMessage, myRemoteLocation);
  }
}

/*

loop do
  use_real_time
  val = sync "/osc/trigger"
  
  if val[0] == 1
    #sample "C:/soundAssets/01.wav", amp: 0.5
    sample "/Users/ygdrasilcave/Desktop/soundAssets/01.wav", amp: 0.5
  end
  
  if val[0] == 2
    #sample "C:/soundAssets/02.wav", rate: 2
    sample "/Users/ygdrasilcave/Desktop/soundAssets/02.wav", rate: 2
  end
  
  if val[0] == 3
    #sample "C:/soundAssets/03.wav", rate: 0.5
    sample "/Users/ygdrasilcave/Desktop/soundAssets/03.wav", rate: 0.5
  end
  
  if val[0] == 4
    #sample "C:/soundAssets/04.wav", rate: -1
    sample "/Users/ygdrasilcave/Desktop/soundAssets/04.wav", rate: val[1]
  end
  
  if val[0] == 5
    #sample "C:/soundAssets/05.wav", start: 0.512, finish: 0.6, rate: -1
    sample "/Users/ygdrasilcave/Desktop/soundAssets/05.wav", start: 0.512, finish: 0.6, rate: -1
  end
  
  if val[0] == 6
    play val[1]
  end
  
end
*/
