void setup() {
  size(800, 800);
  background(0);
}

void draw() {
  background(0);
  int h = hour();
  int m = minute();
  int s = second();
  println(h + ":" + m + ":" + s);
  
  float cx = width/2;
  float cy = height/2;

  drawHour();
  
  stroke(255);
  strokeWeight(5);
  float st = radians(s*6-90);
  float sx = cx + cos(st)*(cx-80);
  float sy = cy + sin(st)*(cy-80);
  
  float st1 = radians(s*6-90 + 180);
  float sx1 = cx + cos(st1)*(20);
  float sy1 = cy + sin(st1)*(20);
  fill(255);
  //ellipse(sx, sy, 20, 20);
  line(cx, cy, sx, sy);
  line(cx, cy, sx1, sy1);

  float mt = radians(m*6-90);
  float mx = cx + cos(mt)*(cx-150);
  float my = cy + sin(mt)*(cy-150);
  fill(255);
  //ellipse(mx, my, 20, 20);
  line(cx, cy, mx, my);

  float ht = radians(((h%12)*30-90) + (30.0/60)*m);
  float hx = cx + cos(ht)*(cx-250);
  float hy = cy + sin(ht)*(cy-250);
  fill(255);
  //ellipse(hx, hy, 20, 20);
  line(cx, cy, hx, hy);
}

void drawHour() {
  fill(255);
  noStroke();
  textSize(30);
  textAlign(CENTER, CENTER);
  for (int i=0; i<12; i++) {
    float t = radians((360/12 * i) - 90 + 30);
    float x = width/2 + cos(t)*(width/2-20);
    float y = height/2 + sin(t)*(height/2-20);
    //ellipse(x, y, 10, 10);
    text(i+1, x, y);
  }
}

