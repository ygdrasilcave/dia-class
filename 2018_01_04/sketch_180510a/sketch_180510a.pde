float[] x;
float[] y;
float[] xSpeed;
float[] ySpeed;

void setup() {
  size(1000, 800);
  background(0);
  x = new float[100];
  y = new float[100];
  xSpeed = new float[100];
  ySpeed = new float[100];
  for(int i=0; i<x.length; i++){
    x[i] = width/2;
    y[i] = height/2;
    xSpeed[i] = random(1, 4);
    ySpeed[i] = random(2, 6);
  }
}

void draw() {
  for (int i=0; i<x.length; i++) {
    x[i] = x[i] + xSpeed[i];
    y[i] = y[i] + ySpeed[i];
    drawBall(x[i], y[i], 30);
  }
}
