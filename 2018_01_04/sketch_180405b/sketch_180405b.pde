float w;
float dir;
float speed;

void setup(){
  size(800, 800);
  background(255);
  w = 50;
  dir = 1;
  speed = 6;
}

void draw(){
  background(255);
  
  fill(map(w, 10, width-100, 0, 255), map(w, 10, width-100, 0, 255), 0);
  
  ellipse(width/2, height/2, w, w);
  
  w = w + dir*speed;
  
  if(w > width-100){
    dir = dir*-1;
  }else if(w < 10){
    dir = dir*-1;
  }
}
