int w;
float f;

void setup(){
  size(1200, 600);
  w = 0;
  f = 0.85;
  println(w);
}

void draw(){
  background(0);
  
  if(width > height){
    w = height;
  }else if(width < height){
    w = width;
  }else if(width == height){
    w = width;
  }
  
  f = map(mouseX, 0, width, 0.1, 1.0);
  
  /*if(width > height){
    w = height;
  }else{
    w = width;
  }*/
  noStroke();
  fill(255);
  ellipse(width/2, height/2, w*f, w*f);
  
  stroke(255, 0, 0);
  strokeWeight(3);
  float x1 = (width - w*f)*0.5;
  float x2 = width - x1;
  line(x1, height/2, x2, height/2);
  
  float y1 = w*(f*0.25);
  ellipse(width/2, height/2-y1, w*f*0.25, w*f*0.25);
  ellipse(width/2, height/2+y1, w*f*0.15, w*f*0.15);
}