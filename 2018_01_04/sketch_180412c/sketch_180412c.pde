float t = 0.0;
float speedT = 0.5;

float r = 100;
float speedR = 8;
float dirR = 1;

void setup(){
  size(600, 600);
  background(0);
}

void draw(){
  //background(0);
  
  float centerX = width/2;
  float centerY = height/2;

  float x = centerX + cos(radians(t)) * r;
  float y = centerY + sin(radians(t)) * r;
  fill(255, 0, 0);
  noStroke();
  ellipse(centerX, centerY, 10, 10);
  
  fill(0, 255, 0);
  ellipse(x, centerY, 10, 10);
  fill(0, 0, 255);
  ellipse(centerX, y, 10, 10);
   
  fill(255);
  noStroke();
  rect(x - 5, y - 5, 10, 10);
  
  stroke(255);
  //line(centerX, centerY, x, y);
  
  t = t + speedT;
  
  r = r + dirR*speedR;
  if(r >= 250){
    r = 250;
    dirR *=  -1;
  }else if(r <= 100){
    r = 100;
    dirR *=  -1;
  }
}