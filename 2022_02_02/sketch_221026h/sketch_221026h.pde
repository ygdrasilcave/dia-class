void setup() {
  size(800, 800);
}
void draw() {
  background(255);
  stroke(0);
  noFill();

  float offsetX = map(mouseX, 0, width, 0, width/2);
  float offsetY = map(mouseY, 0, height, 0, height/2);

  for (int i=0; i<360; i+=20) {
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(i));
    bezier(0,0, -offsetX, -offsetY, offsetX, -offsetY, 0,0);
    popMatrix();
  }
}
