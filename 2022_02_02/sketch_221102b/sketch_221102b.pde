float t = 0;

float r = 0;
float rt = 0;

void setup(){
  size(800, 800);
  rectMode(CENTER);
  
  background(255);
}

void draw(){
  //background(255);
  
  float x = width/2 + cos(t)*r;
  float y = height/2 + sin(t)*r;
  
  //fill(0);
  noFill();
  stroke(0);
  rect(x, y, 30, 30);
  
  //rect(x, height/2, 20, 20);
  //rect(width/2, y, 20, 20);
  
  stroke(255, 0, 0);
  //line(width/2, height/2, x, y);
  
  noFill();
  //ellipse(width/2, height/2, r*2, r*2);
  
  t += 0.01;
  r = (sin(rt) * 100) + 200;
  rt += 0.03;
}
