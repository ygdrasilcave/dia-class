Attractor a;
int numMover = 20;
Mover[] m;

void setup() {
  size(800, 800);
  a = new Attractor();
  m = new Mover[numMover];
  for (int i=0; i<numMover; i++) {
    m[i] = new Mover();
  }
}

void draw() {
  background(255);

  for (int i=0; i<numMover; i++) {
    PVector forceOfGravity = a.attract(m[i]);
    m[i].applyForce(forceOfGravity);
    m[i].update();
    m[i].display();
  }

  //a.display();

  /*noFill();
  stroke(0);
  for (int i=0; i<10; i++) {
    ellipse(width/2, height/2, 100*i, 100*i);
  }*/
}
