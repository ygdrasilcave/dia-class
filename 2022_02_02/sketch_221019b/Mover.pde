class Mover {

  PVector location;
  PVector velocity;
  PVector acceleration;
  float mass;

  PVector[] posHistory;
  PVector[] velHistory;
  int numHistory = 20;
  int recordTime = 0;
  int recordInterval = 50;
  float[] wave;
  float waveT;
  float legScale = 2.0;
  float legAngleFactor = 4.0;

  Mover() {
    location = new PVector(random(width), random(height));
    velocity = new PVector(random(-1.0, 1.0), random(-1.0, 1.0));
    acceleration = new PVector(0, 0);
    mass = random(2, 15);
    posHistory = new PVector[numHistory];
    velHistory = new PVector[numHistory];
    wave = new float[numHistory];
    for (int i=0; i<numHistory; i++) {
      posHistory[i] = location.copy();
      velHistory[i] = velocity.copy();
      wave[i] = 1;
    }
    waveT = 0;
    legAngleFactor = random(2, 5);
  }

  void applyForce(PVector _f) {
    PVector force = _f.copy();
    force.div(mass);
    acceleration.add(force);
  }

  void update() {
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
    
    if (recordTime+recordInterval < millis()) {
      posHistory[0] = location.copy();
      velHistory[0] = velocity.copy();
      wave[0] = map(sin(waveT), -1, 1, 0.5, 1);
      for (int i=numHistory-1; i>=1; i--) {
        posHistory[i] = posHistory[i-1];
        velHistory[i] = velHistory[i-1];
        wave[i] = map(sin(waveT), -1, 1, 0.5, 1);
        waveT += 0.35;
      }
      recordTime = millis();
    }
  }

  void display() {
    fill(0, 80);
    stroke(0);
    pushMatrix();
    translate(location.x, location.y);
    rotate(velocity.heading());
    ellipse(0, 0, mass*4, mass*2);
    popMatrix();
    for (int i=0; i<numHistory-1; i++) {
      //ellipse(posHistory[i].x, posHistory[i].y, mass*1.5, mass*1.5);
      line(posHistory[i].x, posHistory[i].y, posHistory[i+1].x, posHistory[i+1].y);
      pushMatrix();
      translate(posHistory[i].x, posHistory[i].y);
      rotate(velHistory[i].heading()+PI/legAngleFactor);
      line(0, 0, mass*wave[i]*legScale, 0);
      ellipse(mass*wave[i]*legScale, 0, mass*0.4, mass*0.4);
      popMatrix();
      
      pushMatrix();
      translate(posHistory[i].x, posHistory[i].y);
      rotate(velHistory[i].heading()-PI/legAngleFactor);
      line(0, 0, mass*wave[i]*legScale, 0);
      ellipse(mass*wave[i]*legScale, 0, mass*0.4, mass*0.4);
      popMatrix();
    }
  }
}
