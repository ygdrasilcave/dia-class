Mover[] m;
int moverNum = 20;

void setup() {
  size(800, 800);

  m = new Mover[moverNum];
  for (int i=0; i<moverNum; i++) {
    m[i] = new Mover();
  }
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw() {
  background(255);

  for (int i=0; i<moverNum; i++) {

    PVector loc = m[i].location.copy();

    float C = 0.02;
    if (loc.y > height/2 && loc.x > 200 && loc.x < width-200) {
      C = 0.9;
    }
    PVector drag = m[i].velocity.copy();
    float speed = drag.mag();
    drag.normalize();
    drag.mult(-1);
    float dragMag = C * speed * speed;
    drag.mult(dragMag);
    m[i].applyForce(drag);

    //float _m = m[i].mass;
    //PVector gravity = new PVector(0.0, 0.1);
    //gravity.mult(_m);

    PVector gravity = new PVector(0.0, 0.98);

    m[i].applyForce(gravity);
    m[i].update();
    m[i].display();
  }

  fill(0, 80);
  noStroke();
  rect(200, height/2, width-400, height/2);
}
