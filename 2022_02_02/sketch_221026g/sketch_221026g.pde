float radius = 20;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  
  float magMax = sqrt(width*width + height*height);
  
  for (int y=0; y<40; y++) {
    for (int x=0; x<40; x++) {
      PVector pos = new PVector(x*radius*2+radius, y*radius*2+radius);
      PVector mouse = new PVector(mouseX, mouseY);
      PVector diff = PVector.sub(mouse, pos);
      //float theta = atan2(diff.y, diff.x);
      float theta = diff.heading();
      float red = map(diff.mag(), 0, magMax-100, 255, 0);
      
      pushMatrix();
      translate(pos.x, pos.y);
      rotate(theta);
      
      fill(red, 0, 0);
      stroke(0);
      rect(-radius*0.5, -radius*0.5 , radius, radius);
      
      noStroke();
      fill(0);
      rect(0, -(radius/3.0)/2.0, radius, radius/3.0);
      
      popMatrix();
    }
  }
}
