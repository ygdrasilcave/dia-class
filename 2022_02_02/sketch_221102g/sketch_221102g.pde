float t = 0;
float t2 = 0;

void setup() {
  size(800, 400);
}

void draw() {
  background(255);

  /*for (float x=0; x<width; x+=30){
    float y = height/2 + cos(t)*(height/2);
    fill(0);
    ellipse(x, y, 30, 30);
  }
  t += 0.05;*/
  
  float aVelocity = map(mouseX, 0, width, 0, 0.3);
  float diff = map(mouseY, 0, height, 0.01, 0.5);
  
  for (int i=0; i<50; i++){
    float x = (width/50)*i;
    float y = cos(t + (diff*i))*(height/2);
    y = y * sin(t2);
    y += height/2;
    fill(0);
    ellipse(x, y, 30, 30);
  }
  
  t += aVelocity;
  t2 += 0.015;
}
