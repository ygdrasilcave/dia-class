float angle = 0;
float angleSpeed = 1;
float dir = 1;
float radius = 50;
float t = 0;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  stroke(5);
  fill(255, 255, 0);
  for (int x=50; x<width; x+=100) {
    for (int y=50; y<height; y+=100) {
      pushMatrix();
      translate(x, y);
      //rotate(t);
      arc(0, 0, radius*2, radius*2, radians(angle), radians(360-angle), PIE);
      popMatrix();
    }
  }
  angle = angle + angleSpeed;
  if (angle > 25 || angle < 0) {
    angleSpeed *= -1;
  }
  t+=0.01;
}
