Mover[] m;
float t = 0.0;

void setup(){
  size(800, 800);
  m = new Mover[100];
  for(int i=0; i<100; i++){
    m[i] = new Mover();
  }
  rectMode(CENTER);
}

void draw(){
  float ctrl = map(mouseX, 0, width, 0, 1);
  //float ctrl = map(cos(t), -1, 1, 0, 1);
  //t += 0.015;
  
  background(255 * ctrl);
  
  for(int i=0; i<100; i++){
    m[i].update(ctrl);
    m[i].display(ctrl);
  }
}
