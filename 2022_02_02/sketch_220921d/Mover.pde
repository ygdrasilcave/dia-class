class Mover {
  PVector location;
  PVector velocity;
  float dia;
  float dia_offset;
  float dia_offset_scale;
  float t = 0.0;
  float t_speed;
  int n;
  float tentacle;

  Mover() {
    location = new PVector(random(width), random(height));
    velocity = new PVector(random(-2, 2), random(-2, 2));
    dia = random(5, 30);
    dia_offset_scale = dia*0.05;
    t_speed = random(0.05, 0.35);
    n = int(random(10, 30));
    tentacle = random(1.2, 2);
  }

  void update(float _ctrl) {
    PVector temp_velocity = PVector.mult(velocity, _ctrl);
    location.add(temp_velocity);

    dia_offset = sin(t) * dia_offset_scale;
    t += t_speed;
    dia += dia_offset * _ctrl;

    if (location.x > width+dia*0.5) {
      location.x = -dia*0.5;
    } else if (location.x < -dia*0.5) {
      location.x = width+dia*0.5;
    }

    if (location.y > height+dia*0.5) {
      location.y = -dia*0.5;
    } else if (location.y < -dia*0.5) {
      location.y = height+dia*0.5;
    }
  }

  void display(float _ctrl) {
    noFill();
    stroke(255 * map(_ctrl, 0, 1, 1, 0));
    ellipse(location.x, location.y, dia, dia);

    for (int i=0; i<n; i++) {
      float _x1 = location.x + cos(radians(360.0/float(n)) * i) * dia*0.5;
      float _y1 = location.y + sin(radians(360.0/float(n)) * i) * dia*0.5;
      float eye = 1.0;
      if(i < 2){
        eye = 1.75;
      }
      float _x2 = location.x + cos(radians(360.0/float(n)) * i) * dia*0.5 * tentacle*eye;
      float _y2 = location.y + sin(radians(360.0/float(n)) * i) * dia*0.5 * tentacle*eye;
      stroke(0);
      line(_x1, _y1, _x2, _y2);
      noStroke();
      if(i < 2){
        fill(255, 0, 0);
      }else{
        fill(0, 0, 0);
      }
      ellipse(_x2, _y2, dia*0.1, dia*0.1);
    }
    //rect(location.x, location.y, dia, dia);
  }
}
