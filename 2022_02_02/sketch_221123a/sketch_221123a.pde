ArrayList<Vehicle> v;
void setup(){
  size(800, 800);
  v = new ArrayList<Vehicle>();
  //v = new Vehicle(width/2, height/2);
}

void draw(){
  background(255);
  for(Vehicle _v: v){
  //for(int i=0; i<v.size(); i++){
    //Vehicle _v = v.get(i);
    _v.steering();
    _v.update();
    _v.display();
  }
}

void mousePressed(){
  v.add(new Vehicle(mouseX, mouseY));
}
