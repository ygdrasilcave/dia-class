class Vehicle {
  PVector position;
  PVector velocity;
  PVector acceleration;

  float r;

  float maxSpeed;
  float maxForce;

  PVector target;
  float targetAngle;
  float targetDist;
  float targetRadius;
  float targetT1;
  float targetT2;
  float targetAngleT;

  Vehicle(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(random(-1, 1), random(-1, 1));
    acceleration = new PVector(0, 0);
    r = random(5, 20);
    maxSpeed = random(2, 5);
    maxForce = random(0.02, 0.09);

    targetAngle = random(PI);
    target = new PVector(r*6 + cos(targetAngle)*r*2, sin(targetAngle)*r*2);
    targetT1 = 0.0;
    targetT2 = 0.0;
    targetAngleT = 0.0;
  }

  void steering() {
    //targetAngle = random(-HALF_PI*0.9, HALF_PI*0.9);
    float currentAngle = velocity.heading();
    
    targetAngle = currentAngle + map(noise(targetAngleT), 0, 1, -HALF_PI, HALF_PI);
    targetDist = r + noise(targetT1)*(r*5);
    targetRadius = r + noise(targetT2)*(r*5);
    target.x = targetDist + cos(targetAngle)*targetRadius;
    target.y = sin(targetAngle)*targetRadius;
    targetT1 += 0.056;
    targetT2 += 0.05;
    targetAngleT += 0.085;

    target.add(position);

    PVector desired = PVector.sub(target, position);
    desired.normalize();
    desired.mult(maxSpeed);
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }

  void applyForce(PVector _force) {
    PVector force = _force.copy();
    acceleration.add(force);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x > width+100) {
      position.x = -100;
    }
    if (position.x < -100) {
      position.x = width+100;
    }
    if (position.y < -100) {
      position.y = height+100;
    }
    if (position.y > height+100) {
      position.y = -100;
    }
  }

  void display() {
    stroke(0);
    noFill();
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    beginShape();
    vertex(r*2, 0);
    vertex(-r, r);
    vertex(-r, -r);
    endShape(CLOSE);
    
    target.sub(position);
    line(r*2, 0, targetDist, 0);
    //ellipse(targetDist, 0, targetRadius*2, targetRadius*2);
    ellipse(target.x, target.y, 5, 5);
    if(target.mag() <= targetDist + targetRadius){
      line(targetDist, 0, target.x, target.y);
    }
    popMatrix();
    
  }
}
