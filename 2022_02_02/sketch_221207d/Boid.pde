class Boid {
  PVector position;
  PVector velocity;
  PVector acceleration;

  float maxSpeed;
  float maxForce;

  float r = 10;

  Boid(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(random(-1, 1), random(-1, 1));
    acceleration = new PVector(0, 0);
    maxSpeed = 3;
    maxForce = 0.05;
  }

  PVector separation(ArrayList<Boid> boids) {
    PVector desiredVelocity = new PVector(0, 0);
    float maxDistance = r*5;
    int count = 0;
    for (Boid other : boids) {
      float distance = PVector.dist(position, other.position);
      if ((distance>0) && (distance<maxDistance)) {
        PVector diff = PVector.sub(position, other.position);
        diff.normalize();
        diff.div(distance);
        desiredVelocity.add(diff);
        count++;
      }
    }
    PVector steeringForce = new PVector(0, 0);
    if (count > 0) {
      desiredVelocity.div(float(count));
      if (desiredVelocity.mag() > 0) {
        desiredVelocity.normalize();
        desiredVelocity.mult(maxSpeed);
        steeringForce = PVector.sub(desiredVelocity, velocity);
        steeringForce.limit(maxForce);
      }
    }
    return steeringForce;
  }

  PVector alignment(ArrayList<Boid> boids) {
    PVector desiredVelocity = new PVector(0, 0);
    float maxDistance = r*8;
    int count = 0;
    for (Boid other : boids) {
      float distance = PVector.dist(position, other.position);
      if ((distance>0) && (distance<maxDistance)) {
        desiredVelocity.add(other.velocity);
        count++;
      }
    }
    PVector steeringForce = new PVector(0, 0);
    if (count > 0) {
      desiredVelocity.div(float(count));
      desiredVelocity.normalize();
      desiredVelocity.mult(maxSpeed);
      steeringForce = PVector.sub(desiredVelocity, velocity);
      steeringForce.limit(maxForce);
    }
    return steeringForce;
  }

  PVector seek(PVector _target) {
    PVector desiredVelocity = PVector.sub(_target, position);
    desiredVelocity.normalize();
    desiredVelocity.mult(maxSpeed);
    PVector force = PVector.sub(desiredVelocity, velocity);
    force.limit(maxForce);
    return force;
  }

  PVector cohesion(ArrayList<Boid> boids) {
    PVector target = new PVector(0, 0);
    float maxDistance = r*10;
    int count = 0;
    for (Boid other : boids) {
      float distance = PVector.dist(position, other.position);
      if ((distance>0) && (distance>maxDistance)) {
        target.add(other.position);
        count++;
      }
    }
    PVector steeringForce = new PVector(0, 0);
    if (count > 0) {
      target.div(float(count));
      steeringForce = seek(target).copy();
    }
    return steeringForce;
  }

  void flock(ArrayList<Boid> boids) {
    PVector sep = separation(boids);
    PVector ali = alignment(boids);
    PVector coh = cohesion(boids);

    sep.mult(separationFactor);
    ali.mult(alignmentFactor);
    coh.mult(cohesionFactor);

    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
  }

  void applyForce(PVector _f) {
    PVector force = _f.copy();
    acceleration.add(force);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x > width+r*3) {
      position.x = -r*3;
    }
    if (position.x < -r*3) {
      position.x = width+r*3;
    }
    if (position.y > height+r*3) {
      position.y = -r*3;
    }
    if (position.y < -r*3) {
      position.y = height+r*3;
    }
  }

  void display() {
    stroke(0);
    noFill();
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(0, 0, r*2, r);
    beginShape();
    vertex(r, -r*0.5);
    vertex(r*2, 0);
    vertex(r, r*0.5);
    endShape(CLOSE);
    /*
    float sumR = 0;
     for (int i=0; i<5; i++) {
     float tempR = r-(r*0.2*i);
     if (i > 0) {
     sumR = sumR + r-(r*0.2*(i-1));
     }
     ellipse(-r-(tempR*0.5)-sumR, 0, tempR, tempR);
     }
     
     ellipse(0, -r*0.5-(r*2.5*0.5), r*0.6, r*2.5);
     ellipse(r*0.5, -r*0.5-(r*2.5*0.5), r*0.6, r*2.5);
     
     ellipse(0, r*0.5+(r*2.5*0.5), r*0.6, r*2.5);
     ellipse(r*0.5, r*0.5+(r*2.5*0.5), r*0.6, r*2.5);
     */
    popMatrix();
  }
}
