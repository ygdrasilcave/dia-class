import oscP5.*;
import netP5.*;

float separationFactor = 1.5;
float alignmentFactor = 0.8;
float cohesionFactor = 0.5;

OscP5 oscP5;
NetAddress myRemoteLocation;

ArrayList<Boid> boids;

void setup() {
  size(1500, 1000);
  rectMode(CENTER);
  boids = new ArrayList<Boid>();
  boids.add(new Boid(width/2, height/2));
  
  oscP5 = new OscP5(this, 12000);
  //myRemoteLocation = new NetAddress("127.0.0.1",12000);
}

void draw() {
  background(255);
  
  for (Boid b : boids) {
    b.flock(boids);
    b.update();
    b.display();
  }
  
}

void mouseDragged(){
  boids.add(new Boid(mouseX, mouseY));
}

void oscEvent(OscMessage theOscMessage) {
  //touch osc
  if(theOscMessage.checkAddrPattern("/1/faderA")==true) {
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      separationFactor = map(value, 0.0, 1.0, 0.1, 2.5);
      //println(value);
      return;
    }  
  } 
  if(theOscMessage.checkAddrPattern("/1/faderB")==true) {
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      alignmentFactor = map(value, 0.0, 1.0, 0.1, 2.5);
      //println(value);
      return;
    }  
  } 
  if(theOscMessage.checkAddrPattern("/1/faderC")==true) {
    if(theOscMessage.checkTypetag("f")) {
      float value = theOscMessage.get(0).floatValue();
      cohesionFactor = map(value, 0.0, 1.0, 0.1, 2.5);
      //println(value);
      return;
    }  
  } 
  //println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
}
