class Boid{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  float maxSpeed;
  float maxForce;
  
  Boid(float _x, float _y){
    position = new PVector(_x, _y);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    maxSpeed = 5;
    maxForce = 0.05;
  }
  
  PVector separation(ArrayList<Boid> boids){
    PVector force = new PVector(0,0);
    return force;
  }
  
  PVector alignment(ArrayList<Boid> boids){
    PVector force = new PVector(0,0);
    return force;
  }
  
  PVector cohesion(ArrayList<Boid> boids){
    PVector force = new PVector(0,0);
    return force;
  }
  
  void flock(ArrayList<Boid> boids){
    PVector sep = separation(boids);
    PVector ali = alignment(boids);
    PVector coh = cohesion(boids);
    
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
  }
  
  void applyForce(PVector _f){
    
  }
  
  void update(){
    
  }
  
  void display(){
    
  }
  
}
