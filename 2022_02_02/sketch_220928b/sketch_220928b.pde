Mover[] m;
boolean targeting = false;

void setup(){
  size(800, 800);
  m = new Mover[100];
  for(int i=0; i<100; i++){
    m[i] = new Mover();
  }
}

void draw(){
  background(255);
  
  if(mousePressed){
    targeting = true;
  }else{
    targeting = false;
  }
  
  for(int i=0; i<100; i++){
    m[i].update(targeting);
    m[i].display();
  }
}
