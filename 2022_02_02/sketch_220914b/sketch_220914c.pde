PVector pos;
PVector vel;

void setup(){
  size(640, 360);
  pos = new PVector(50, 200);
  vel = new PVector(3, 4);
}

void draw(){
  background(255);
  
  pos.add(vel);
  
  if ((pos.x > width) || (pos.x < 0)) {
    vel.x = vel.x * -1;
  }
  if ((pos.y > height) || (pos.y < 0)) {
    vel.y = vel.y * -1;
  }
  
  stroke(0);
  fill(175);
  ellipse(pos.x, pos.y, 16, 16);
}
