float angle = 0;
float angleT = 0;
float radius = 50;
float t = 0;

int id = 0;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  stroke(5);
  fill(255, 255, 0);

  for (int y=0; y<8; y+=1) {
    for (int x=0; x<8; x+=1) {
      int index = x + y*8;
      pushMatrix();
      translate(x*radius*2 + radius, y*radius*2 + radius);
      rotate(t);
      if(index == id){
        arc(0, 0, radius*2, radius*2, radians(25)-radians(angle), radians(360-25)+radians(angle), PIE);
      }else{
        arc(0, 0, radius*2, radius*2, radians(25), radians(360-25), PIE);
      }
      popMatrix();
    }
  }
  angle = map(cos(angleT), -1, 1, 0, 25);
  angleT += 0.5;
  if (angle < 0.15) {
    id++;
    if(id > 8*8-1){
      id = 0;
    }
  }
  t += 0.01;
}
