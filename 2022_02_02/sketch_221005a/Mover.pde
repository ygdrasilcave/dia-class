class Mover{
  
  PVector location;
  PVector velocity;
  PVector acceleration;
  float mass;
  
  Mover(){
    location = new PVector(width/2, 0);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    mass = random(5, 50);
  }
  
  void applyForce(PVector _force){
    //PVector force = _force.copy();
    //force.div(mass);
    
    PVector force = PVector.div(_force, mass);
    acceleration.add(force);
  }
  
  void update(){
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
    
    if(location.x > width){
      location.x = 0;
    }
    if(location.x < 0){
      location.x = width;
    }
    if(location.y > height){
      location.y = 0;
    }
    if(location.y < 0){
      location.y = height;
    }
  }
  
  void display(){
    fill(0);
    noStroke();
    ellipse(location.x, location.y, mass*2, mass*2);
    fill(0);
    text(mass, location.x, location.y+mass*1.3);
  }
}
