Mover m;

void setup(){
  size(800, 800);
  m = new Mover();
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw(){
  background(255);
  PVector gravity = new PVector(0, 0.1);
  PVector wind = new PVector(0.05, 0);
  m.applyForce(gravity);
  m.applyForce(wind);
  m.update();
  m.display();
}
