class Mover {

  PVector location;
  PVector velocity;
  PVector acceleration;
  float mass;
  float topSpeed;

  float C;
  float N = 1;
  
  PImage img;

  Mover() {
    location = new PVector(random(width), 0);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    mass = random(20, 30);
    topSpeed = random(8, 19);
    
    C = random(0.03, 0.5);
  }
  
  void setImage(PImage _img){
    img = _img;
  }

  void applyForce(PVector _force) {
    //PVector force = _force.copy();
    //force.div(mass);

    PVector force = PVector.div(_force, mass);
    acceleration.add(force);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(topSpeed);
    location.add(velocity);
    acceleration.mult(0);

    checkEdges1();
  }

  void checkEdges1() {
    if (location.x > width) {
      location.x = width;
      velocity.x *= -1.0;
    }
    if (location.x < 0.0) {
      location.x = 0.0;
      velocity.x *= -1.0;
    }
    if (location.y > height) {
      location.y = height;
      velocity.y *= -1.0;
    }
    //if (location.y < 0) {
    //  location.y = 0;
    //  velocity.y *= -1;
    //}
  }

  void checkEdges2() {
    if (location.x > width) {
      location.x = 0;
    }
    if (location.x < 0) {
      location.x = width;
    }
    if (location.y > height) {
      location.y = 0;
    }
    if (location.y < 0) {
      location.y = height;
    }
  }

  void display() {
    
    fill(map(C, 0.03, 0.5, 255, 0));
    stroke(0);
    ellipse(location.x, location.y, mass*2, mass*2);
    //fill(0);
    //noStroke();
    //text(mass, location.x, location.y);
  }
}
