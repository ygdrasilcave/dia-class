Mover[] m;
int moverNum = 20;

PImage img1, img2, img3, img4;

void setup() {
  size(800, 400);
  
  m = new Mover[moverNum];
  for (int i=0; i<moverNum; i++) {
    m[i] = new Mover();
  }
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw() {
  background(255);
  PVector gravity = new PVector(0.0, 0.98);
  PVector wind = new PVector(0, 0);
  if (mousePressed == true) {
    if (mouseX > width/2) {
      wind.x = map(mouseX, 0, width/2, 0.5, 0.01);
      wind.y = 0;
    } else {
      wind.x = -map(mouseX, width/2, width, 0.01, 0.5);
      wind.y = 0;
    }
  }

  if (keyPressed == true) {
    if (key == ' ') {
      gravity.mult(-5.0);
    }
  }

  for (int i=0; i<moverNum; i++) {

    PVector friction = m[i].velocity.copy();
    friction.normalize();
    friction.mult(-1);
    float C = m[i].C;
    float N = m[i].N;
    float frictionMag = C*N;
    friction.mult(frictionMag);

    //m[i].applyForce(PVector.mult(gravity, m[i].mass));
    m[i].applyForce(gravity);
    m[i].applyForce(wind);
    m[i].applyForce(friction);
    m[i].update();
    m[i].display();
  }
}
