class Attractor{
  
  PVector location;
  float mass;
  float G;
  
  Attractor(){
    location = new PVector(width/2, height/2);
    mass = 80;
    G = 0.8;
  }
  
  PVector attract(Mover _m){
    PVector dir = PVector.sub(location, _m.location);
    float distance = dir.mag();
    distance = constrain(distance, 5.0, 50.0);
    dir.normalize();
    float strength = (G * mass * _m.mass) / (distance * distance); 
    dir.mult(strength);
    return dir;
  }
  
  void display(){
    fill(0, 80);
    stroke(0);
    ellipse(location.x, location.y, 100, 100);
  }
}
