class Mover {

  PVector location;
  PVector velocity;
  PVector acceleration;
  float mass;

  Mover() {
    location = new PVector(150, 150);
    velocity = new PVector(2, 0);
    acceleration = new PVector(0, 0);
    mass = 10;
  }

  void applyForce(PVector _f) {
    PVector force = _f.copy();
    force.div(mass);
    acceleration.add(force);
  }

  void update() {
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
  }

  void display() {
    fill(0, 80);
    stroke(0);
    ellipse(location.x, location.y, 10, 10);
  }
}
