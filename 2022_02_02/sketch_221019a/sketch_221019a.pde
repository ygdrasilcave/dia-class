Attractor a;
Mover m;

void setup() {
  size(800, 800);
  a = new Attractor();
  m = new Mover();
}

void draw() {
  background(255);

  PVector forceOfGravity = a.attract(m);

  m.applyForce(forceOfGravity);
  m.update();

  a.display();
  m.display();

  noFill();
  stroke(0);
  for (int i=0; i<10; i++) {
    ellipse(width/2, height/2, 100*i, 100*i);
  }
}
