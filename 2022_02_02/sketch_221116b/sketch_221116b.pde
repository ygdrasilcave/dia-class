IntList dice;
int index = 0;
int lastNum = 0;

void setup(){
  size(600, 600);
  dice = new IntList();
  for(int i=0; i<6; i++){
    dice.append(i+1);
  }
  dice.shuffle();
  lastNum = dice.get(dice.size()-1);
  println("size: " + dice.size());
  println(dice);
  println("last Num: " + lastNum);
  
  textAlign(CENTER, CENTER);
  textSize(150);
}

void draw(){
  background(255);
  fill(0);
  text(dice.get(index), width/2, height/2);  
}

void keyPressed(){
  if(key == ' '){
    index++;
    if(index > dice.size()-1){
      index = 0;
      int retry = 0;
      while(true){
        dice.shuffle();
        retry++;
        println(retry);
        if(lastNum != dice.get(0)){
          println(dice);
          lastNum = dice.get(dice.size()-1);
          break;          
        }       
      }
    }
  }
}
