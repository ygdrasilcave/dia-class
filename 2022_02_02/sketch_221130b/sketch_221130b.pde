FlowField f;
ArrayList<Vehicle> v;

void setup() {
  size(800, 800);
  f = new FlowField(20);
  v = new ArrayList<Vehicle>();
}

void draw() {
  background(255);
  f.display();
  for (Vehicle _v : v) {
    _v.follow(f);
    _v.update();
    _v.display();
  }
}

void mousePressed() {
  v.add(new Vehicle(mouseX, mouseY));
}

void keyPressed() {
  if (key == '0') {
    f.init(0);
  }
  if (key == '1') {
    f.init(1);
  }
  if (key == '2') {
    f.init(2);
  }
  if (key == '3') {
    f.init(3);
  }
}
