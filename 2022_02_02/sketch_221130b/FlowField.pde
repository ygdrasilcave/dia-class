class FlowField{
  int w = 40;
  int cols;
  int rows;
  
  PVector[][] field;
  
  FlowField(int _w){
    w = _w;
    rows = int(width/w);
    cols = int(height/w);
    field = new PVector[cols][rows];
    init(0);
  }
  
  void init(int _t){
    float yoff = 0;
    for(int y=0; y<rows; y++){
      float xoff = 0;
      for(int x=0; x<cols; x++){
        float _angle = 0;
        if(_t == 0){
          _angle = random(0, TWO_PI);
        }else if(_t == 1){
          _angle = 0;
        }else if(_t == 2){
          _angle = HALF_PI;
        }else{
          _angle = map(noise(xoff, yoff), 0, 1, 0, TWO_PI);
        }
        float _posX = cos(_angle);
        float _posY = sin(_angle);
        field[x][y] = new PVector(_posX, _posY);
        xoff += 0.085;
      }
      yoff += 0.065;
    }
  }
  
  PVector lookup(PVector _pos){
    int col = int(constrain(_pos.x/w, 0, cols-1));
    int row = int(constrain(_pos.y/w, 0, rows-1));
    return field[col][row].copy();
  }
  
  void display(){
    for(int y=0; y<rows; y++){
      for(int x=0; x<cols; x++){        
        arrow(x, y, field[x][y].heading());
      }
    }
  }
  
  void arrow(float _x, float _y, float _angle){
    pushMatrix();
    translate(_x*w + w*0.5, _y*w + w*0.5);
    rotate(_angle);
    noFill();
    stroke(0);
    //rect(-w*0.5, -w*0.5, w, w);
    //ellipse(0, 0 , 5, 5);
    line(-(w*0.5-4), 0, w*0.5-4, 0);
    line(w*0.5-4, 0, w*0.5-4-4, -4);
    line(w*0.5-4, 0, w*0.5-4-4, 4);
    popMatrix();  
  }
}
