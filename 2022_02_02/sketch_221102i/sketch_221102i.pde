float t2 = 0;

void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  
  noFill();
  stroke(0);
  for(int i=0; i<30; i++){
    float t = (360.0/30) * i;
    pushMatrix();
    translate(width/2, height/2);
    rotate(radians(t));
    //ellipse(map(cos(t2), -1, 1, 0, 300), 0, 100, 30);
    ellipse(50, 0, 100, 30);
    popMatrix();
  }
  
  t2 += 0.05;
}
