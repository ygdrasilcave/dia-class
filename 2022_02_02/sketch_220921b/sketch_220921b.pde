PVector v;
PVector w;

void setup() {
  size(800, 800);
  v = new PVector(100, 50);
  w = new PVector(100, 100);
  
  println("v : " + str(v.mag()));
  println("w : " + str(w.mag()));
}

void draw() {
  background(255);

  //float scale = map(mouseX, 0, width, 0.1, 10);
  //println(scale);
  //v.mult(scale);

  v.setMag(500);
  //println(v);
  
  w.setMag(500);
  //println(w);

  stroke(0);
  strokeWeight(3);
  line(0, 0, v.x, v.y);
  line(0, 0, w.x, w.y);
  
  noFill();
  stroke(255, 0, 0);
  strokeWeight(1);
  ellipse(0 , 0, 1000, 1000);
}
