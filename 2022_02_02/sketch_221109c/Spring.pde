class Spring{
  
  PVector anchor;
  float defaultLength;
  float k = 0.1;
  
  Spring(float _x, float _y){
    anchor = new PVector(_x, _y);
    defaultLength = 400;
  }
  
  PVector springForce(Bob bob){
    PVector diff = PVector.sub(bob.location, anchor);
    float dist = diff.mag();
    float stretch = dist - defaultLength;
    diff.normalize();
    diff.mult(-1 * k * stretch);
    return diff;
  }
  
  void display(Bob bob){
    line(anchor.x, anchor.y, bob.location.x, bob.location.y);
  }
}
