int num = 80;
Bob[] b;
Spring[] s;

void setup() {
  size(800, 800);
  b = new Bob[num];
  s = new Spring[num];
  float t = 0;
  for (int i=0; i<num; i++) {
    b[i] = new Bob((width/float(num))*i, height-(250 + sin(t)*20));
    s[i] = new Spring((width/float(num))*i, height-100);
    t += 0.252;
  }
}

void draw() {
  background(255);
  PVector gravity = new PVector(0, -0.98);
  PVector wind = new PVector(map(mouseX, 0, width, -1, 1), 0);
  for (int i=0; i<num; i++) {
    b[i].applyForce(gravity);
    b[i].applyForce(wind);
    PVector springF = s[i].springForce(b[i]);
    b[i].applyForce(springF);
    b[i].update();
    b[i].display();
    s[i].display(b[i]);
  }
}

void keyPressed(){
  if(key == ' '){
    float t = 0;
    float tSpeed = random(0.05, 0.65);
    for (int i=0; i<num; i++) {
      b[i].resetLocation((width/float(num))*i, height-(500 + sin(t)*100));
      t += tSpeed;
    }
  }
}
