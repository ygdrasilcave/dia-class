class Bob{
  
  PVector location;
  PVector velocity;
  PVector acceleration;
  float mass;
  float damping;
  
  Bob(float _x, float _y){
    location = new PVector(_x, _y);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    mass = 10;
    damping = 0.985;
    //damping = 1;
  }
  
  void resetLocation(float _x, float _y){
    location.set(_x, _y);
  }
  
  void applyForce(PVector _f){
    PVector force = _f.copy();
    force.div(mass);
    acceleration.add(force);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.mult(damping);
    location.add(velocity);
    acceleration.mult(0);
  }
  
  void display(){
    fill(0);
    ellipse(location.x, location.y, 10, 10);
  }
}
