float radius = 20;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  for (int y=0; y<40; y++) {
    for (int x=0; x<40; x++) {
      float _x = x*radius*2+radius;
      float _y = y*radius*2+radius;
      float diffX = mouseX - _x;
      float diffY = mouseY - _y;
      float theta = atan2(diffY, diffX);
      pushMatrix();
      translate(_x, _y);
      rotate(theta);
      noStroke();
      fill(0);
      rect(0, -(radius/3.0)/2.0, radius, radius/3.0);
      noFill();
      stroke(0);
      rect(-radius*0.5, -radius*0.5 , radius, radius);
      popMatrix();
    }
  }
}
