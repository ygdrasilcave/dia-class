class Vehicle{
  
  PVector location;
  PVector velocity;
  PVector acceleration;
  
  float r;
  float maxSpeed;
  float maxForce;
  float br;
  int type;
  int startTime;
  int age;
  boolean alive;
  
  Vehicle(float _x, float _y, float _r, int _type){
    acceleration = new PVector(0,0);
    velocity = new PVector(0,0);
    location = new PVector(_x, _y);
    r = _r;
    maxSpeed = random(3, 8);;
    maxForce = random(0.05, 0.3);
    br = random(50, 150);
    type = _type;
    startTime = millis();
    age = int(random(5000, 15000));
    alive = true;
  }
  
  void applyForce(PVector _f){
    PVector force = _f.copy();
    acceleration.add(force);
  }
  
  void steering(PVector _target){
    PVector desired = PVector.sub(_target, location);    
    if(type == 0){
      //Arrive type 0
      ////desired.normalize();
      ////desired.mult(maxSpeed);
      desired.setMag(maxSpeed);
    }else if(type == 1){
      //Arrive type 1
      desired.limit(maxSpeed);
    }else if(type == 2){
      //Arrive type 2
      float dist = desired.mag();    
      if(dist < br){
        desired.setMag(map(dist, 0, br, 0, maxSpeed));
      }else{
        desired.setMag(maxSpeed);
      }
    }
    
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    location.add(velocity);
    acceleration.mult(0);
    
    if(startTime + age < millis()){
      alive = false;
    }
  }
  
  void display(){   
    pushMatrix();
    translate(location.x, location.y);
    rotate(velocity.heading());
    
    float time_diff = constrain((startTime + (age-1000)) - millis(), 0, 1000);
    float opacity = map(time_diff, 0, 1000, 0, 255);
    
    stroke(0, opacity);
    if(type == 0){
      fill(255, 0, 0, opacity);
    }else if(type == 1){
      fill(0, 225, 0, opacity);
    }else if(type == 2){
      fill(0, 0, 255, opacity);
    }
    beginShape();
    vertex(r*2, 0);
    vertex(-r, r);
    vertex(-r, -r);
    endShape(CLOSE);
    ellipse(0, 0, r*0.2, r*0.2);
    //noFill();
    //ellipse(0, 0, br*2, br*2);
    popMatrix();
  }
}
