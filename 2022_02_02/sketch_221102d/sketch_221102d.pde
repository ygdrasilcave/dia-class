int num = 180;
int numInc = 1;
float rotVal = 0;

int currentTime = 0;

void setup() {
  size(800, 800);
  rectMode(CENTER);
  background(255);
}

void draw() {
  background(255);
  
  float centerX = mouseX;
  float centerY = mouseY;
  
  fill(0);
  float rt = 0;
  pushMatrix();
  translate(centerX, centerY);
  rotate(-HALF_PI);
  for (int i=0; i<num; i++) {
    float t = radians((182.0/num) * i);
    float rMin = cos(rotVal)*100 + 200;
    float rMax = sin(rotVal)*100 + 200;
    float r = map(cos(rt), -1, 1, rMin, rMax);
    //float r = 200;
    float x = cos(t)*r;
    float y = sin(t)*r;
    ellipse(x, y, 10, 10);
    line(0, 0, x, y);
    ellipse(x, -y, 10, 10);
    line(0, 0, x, -y);
    rt += 0.21;
  }
  popMatrix();
  rotVal += 0.02;
  
  if(currentTime + 50 < millis()){
    num += numInc;
    if(num > 200){
      numInc = -1;
    }
    if(num < 30){
      numInc = 1;
    }
    currentTime = millis();
  }
}
