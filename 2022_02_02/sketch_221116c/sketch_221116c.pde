Vehicle v;

void setup(){
  size(800, 800);
  v = new Vehicle(random(width), random(height), random(10, 30));
}

void draw(){
  background(255);
  PVector target = new PVector(mouseX, mouseY);
  v.steering(target);
  v.update();
  v.display();
}
