PVector a;
PVector b;
PVector c;

void setup(){
  size(600, 600);
  a = new PVector(200, 200);
  b = new PVector(250, 50);
  
  background(255);
  stroke(0);
  strokeWeight(3);
  
  line(0, 0, a.x, a.y);
  line(0, 0, b.x, b.y);
  
  a.add(b);
  line(0, 0, a.x, a.y);
}
