Bob b;
Spring s;
int num = 150;

void setup() {
  size(800, 800);
  b = new Bob(width/2, height-250);
  s = new Spring(width/2, height-100);
}

void draw() {
  background(255);
  PVector gravity = new PVector(0, -0.98);
  b.applyForce(gravity);
  PVector springF = s.springForce(b);
  b.applyForce(springF);
  b.update();
  b.display();
  s.display(b);
}
