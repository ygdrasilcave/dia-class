float angle = 0;
float angleSpeed = 1.0;

float radius = 300;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);
  stroke(5);
  
  translate(width/2, height/2);
  
  line(0, 0, radius, 0);
  
  noFill();
  arc(0,0, radius*2, radius*2, 0, radians(angle));
  
  rotate(radians(angle));
  line(0, 0, radius, 0);
  
  angle = angle + angleSpeed;
  if(angle > 360){
    angle = 0;
  }
}
