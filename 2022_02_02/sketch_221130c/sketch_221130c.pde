PVector a = new PVector(-3, 5);
PVector b = new PVector(10, 1);

println(a.dot(b));

float theta = acos(a.dot(b) / (a.mag() * b.mag()));
println(degrees(theta));
