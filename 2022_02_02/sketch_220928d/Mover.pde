class Mover {
  PImage img;
  
  PVector location;
  PVector velocity;
  PVector acceleration;
  boolean myType;
  float maxSpeed;
  float accValue;
  float velLimitFactor;
  float dia;

  Mover(PImage _img) {
    img = _img;
    location = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    float t = random(1);
    if (t > 0.5) {
      myType = true;
      dia = random(50, 100);
    } else {
      myType = false;
      dia = random(100, 150);
    }
    maxSpeed = random(5, 15);
    accValue = random(0.25, 0.75);
    velLimitFactor = random(0.01, 0.06);
  }

  void update(boolean _targeting, PVector _target) {
    //PVector target = new PVector(mouseX, mouseY);
    PVector target = _target.copy();
    acceleration = target.sub(location);
    float dist = acceleration.mag();

    if (_targeting == true) {
      //println(acceleration);
      acceleration.normalize();

      if (myType == true) {
        acceleration.mult(accValue);
      } else {
        acceleration.mult(dist*0.05);
      }
    }else{
      acceleration = PVector.random2D();
      acceleration.mult(0.5);
    }

    velocity.add(acceleration);

    if (_targeting == true) {
      if (myType == true) {
        velocity.limit(maxSpeed);
      } else {
        velocity.limit(dist*velLimitFactor);
      }
    } else {
      velocity.limit(maxSpeed);
    }
    location.add(velocity);
    
    float r = dia*0.5;
    if(location.x > width+r){
      location.x = -r;
    }
    if(location.x < -r){
      location.x = width+r;
    }
    if(location.y > height+r){
      location.y = -r;
    }
    if(location.y < -r){
      location.y = height+r;
    }
  }

  void display() {
    stroke(0);
    strokeWeight(3);
    noFill();
    //ellipse(location.x, location.y, dia, dia);
    
    pushMatrix();
    translate(location.x, location.y);
    rotate(velocity.heading()+HALF_PI);
    //rotate(velocity.heading());
    image(img, -dia*0.5, -dia*0.5, dia, dia);
    popMatrix();
  }
}
