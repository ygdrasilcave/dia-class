Mover[] m;
boolean targeting = false;
PVector target;
boolean softToggle = true;

PImage img;

void setup() {
  size(800, 800);
  
  img = loadImage("fit1.png");
  //img = loadImage("transform1.png");
  
  m = new Mover[20];
  for (int i=0; i<20; i++) {
    m[i] = new Mover(img);
  }
  target = new PVector(width/2, height/2);
}

void draw() {
  background(255);
  /*
  if(mousePressed){
   targeting = true;
   }else{
   targeting = false;
   }
   */

  if (second()%10 == 0 || second()%10 == 1) {
    targeting = true;
    if (softToggle == true) {
      target.x = random(width);
      target.y = random(height);
      softToggle = false;
    }
  } else {
    targeting = false;
    softToggle = true;
  }
  
  for (int i=0; i<20; i++) {
    m[i].update(targeting, target);
    m[i].display();
  }
  
  if(targeting == true){
    noStroke();     
    fill(255, 0, 0);
  }else{
    stroke(255, 0, 0);
    noFill();
  }
  ellipse(target.x, target.y, 20, 20);
}
