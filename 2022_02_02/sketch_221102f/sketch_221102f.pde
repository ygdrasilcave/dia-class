Oscillator[] osc;

void setup(){
  size(800, 800);
  osc = new Oscillator[50];
  for(int i=0; i<50; i++){
    osc[i] = new Oscillator();
  }
}

void draw(){
  background(255);
  for(int i=0; i<50; i++){
    osc[i].oscillate();
    osc[i].display();
  }
}
