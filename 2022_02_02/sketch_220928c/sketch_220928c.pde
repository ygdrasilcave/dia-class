Mover[] m;
boolean targeting = false;
PVector target;
boolean softToggle = true;

void setup() {
  size(800, 800);
  m = new Mover[100];
  for (int i=0; i<100; i++) {
    m[i] = new Mover();
  }
  target = new PVector(width/2, height/2);
}

void draw() {
  background(255);
  /*
  if(mousePressed){
   targeting = true;
   }else{
   targeting = false;
   }
   */

  if (second()%10 == 0 || second()%10 == 1) {
    targeting = true;
    if (softToggle == true) {
      target.x = random(width);
      target.y = random(height);
      softToggle = false;
    }
  } else {
    targeting = false;
    softToggle = true;
  }
  
  for (int i=0; i<100; i++) {
    m[i].update(targeting, target);
    m[i].display(targeting);
  }
  
  if(targeting == true){
    noStroke();    
    fill(255, 0, 0);
  }else{
    stroke(255, 0, 0);
    noFill();
  }
  ellipse(target.x, target.y, 20, 20);
}
