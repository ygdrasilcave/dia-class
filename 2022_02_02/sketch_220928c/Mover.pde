class Mover {
  PVector location;
  PVector velocity;
  PVector acceleration;
  boolean myType;
  float maxSpeed;
  float accValue;
  float velLimitFactor;
  float dia;

  Mover() {
    location = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    float t = random(1);
    if (t > 0.5) {
      myType = true;
      dia = random(5, 10);
    } else {
      myType = false;
      dia = random(10, 20);
    }
    maxSpeed = random(5, 15);
    //maxSpeed = 10;
    accValue = random(0.25, 0.75);
    //accValue = 0.5;
    velLimitFactor = random(0.01, 0.06);
    //velLimitFactor = 0.02;
  }

  void update(boolean _targeting, PVector _target) {
    //PVector target = new PVector(mouseX, mouseY);
    PVector target = _target.copy();
    acceleration = target.sub(location);
    float dist = acceleration.mag();

    if (_targeting == true) {
      //println(acceleration);
      acceleration.normalize();

      if (myType == true) {
        acceleration.mult(accValue);
      } else {
        acceleration.mult(dist*0.05);
      }
    } else {
      acceleration = PVector.random2D();
      acceleration.mult(0.5);
    }

    velocity.add(acceleration);

    if (_targeting == true) {
      if (myType == true) {
        velocity.limit(maxSpeed);
      } else {
        velocity.limit(dist*velLimitFactor);
      }
    } else {
      velocity.limit(maxSpeed);
    }

    location.add(velocity);

    float r = dia*0.5;
    if (location.x > width+r) {
      location.x = -r;
    }
    if (location.x < -r) {
      location.x = width+r;
    }
    if (location.y > height+r) {
      location.y = -r;
    }
    if (location.y < -r) {
      location.y = height+r;
    }
  }

  void display(boolean _targeting) {
    if (_targeting == true) {
      if (myType == true) {
        stroke(255, 0, 0);
        strokeWeight(3);
        noFill();
      } else {
        stroke(255, 0, 255);
        strokeWeight(3);
        noFill();
      }
    } else {
      if (myType == true) {
        stroke(0, 255, 0);
        strokeWeight(3);
        noFill();
      } else {
        stroke(255, 255, 0);
        strokeWeight(3);
        noFill();
      }
    }
    ellipse(location.x, location.y, dia, dia);
  }
}
