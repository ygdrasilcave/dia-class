float t = 0;

void setup(){
  size(800, 800);
  rectMode(CENTER);
}

void draw(){
  background(255);
  
  
  float r = 300;
  float x = width/2 + cos(t)*r;
  float y = height/2 + sin(t)*r;
  
  fill(0);
  noStroke();
  rect(x, y, 50, 50);
  
  rect(x, height/2, 20, 20);
  rect(width/2, y, 20, 20);
  
  stroke(255, 0, 0);
  line(width/2, height/2, x, y);
  
  noFill();
  ellipse(width/2, height/2, 600, 600);
  
  
  t += 0.01;
}
