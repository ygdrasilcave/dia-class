IntList dice;
int index = 0;

void setup(){
  size(600, 600);
  dice = new IntList();
  for(int i=0; i<6; i++){
    dice.append(i+1);
  }
  println(dice.size());
  println(dice);
  
  textAlign(CENTER, CENTER);
  textSize(150);
}

void draw(){
  background(255);
  fill(0);
  text(dice.get(index), width/2, height/2);  
}

void keyPressed(){
  if(key == ' '){
    dice.remove(index);
    int dice_size = dice.size();
    if(dice_size == 0){
      for(int i=0; i<6; i++){
        dice.append(i+1);
      }
    }
    index = int(random(dice_size));
    println("Current Num: " + dice.get(index));
    println("Length:" + dice_size); 
  }
}
