class Vehicle{
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  
  Vehicle(){
    position = new PVector(width/2, height/2);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    maxSpeed = 4;
    maxForce = 0.2;
  }
  
  void follow(FlowField _flow){
    PVector desired = _flow.lookup(position);
    desired.mult(maxSpeed);
    PVector steer = PVector.sub(desired, velocity);
    steer.limit(maxForce);
    applyForce(steer);
  }
  
  void applyForce(PVector _f){
    PVector force = _f.copy();
    acceleration.add(force);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
    
    if(position.x > width+50){
      position.x = -50; 
    }
    if(position.x < -50){
      position.x = width+50; 
    }
    if(position.y > height+50){
      position.y = -50; 
    }
    if(position.y < -50){
      position.y = height+50; 
    }
  }
  
  void display(){
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    stroke(0);
    fill(0, 0, 255);
    rect(-15, -5, 30, 10);
    fill(255);
    ellipse(15, 2.5, 5, 5);
    ellipse(15, -2.5, 5, 5);
    popMatrix();
  }
}
