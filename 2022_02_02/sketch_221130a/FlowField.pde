class FlowField{
  int w = 40;
  int cols;
  int rows;
  
  PVector[][] field;
  
  FlowField(int _w){
    w = _w;
    rows = int(width/w);
    cols = int(height/w);
    field = new PVector[cols][rows];
    init();
  }
  
  void init(){
    for(int y=0; y<rows; y++){
      for(int x=0; x<cols; x++){
        float _angle = random(0, TWO_PI);
        float _posX = cos(_angle);
        float _posY = sin(_angle);
        field[x][y] = new PVector(_posX, _posY);
      }
    }
  }
  
  PVector lookup(PVector _pos){
    int col = int(constrain(_pos.x/w, 0, cols-1));
    int row = int(constrain(_pos.y/w, 0, rows-1));
    return field[col][row];
  }
  
  void display(){
    for(int y=0; y<rows; y++){
      for(int x=0; x<cols; x++){        
        arrow(x, y, field[x][y].heading());
      }
    }
  }
  
  void arrow(float _x, float _y, float _angle){
    pushMatrix();
    translate(_x*w + w*0.5, _y*w + w*0.5);
    rotate(_angle);
    noFill();
    stroke(0);
    //rect(-w*0.5, -w*0.5, w, w);
    //ellipse(0, 0 , 5, 5);
    line(-(w*0.5-4), 0, w*0.5-4, 0);
    line(w*0.5-4, 0, w*0.5-4-4, -4);
    line(w*0.5-4, 0, w*0.5-4-4, 4);
    popMatrix();  
  }
}
