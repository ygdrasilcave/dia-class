FlowField f;
Vehicle v;

void setup(){
  size(800, 800);
  f = new FlowField(40);
  v = new Vehicle();
}

void draw(){
  background(255);
  f.display();
  v.follow(f);
  v.update();
  v.display();
}
