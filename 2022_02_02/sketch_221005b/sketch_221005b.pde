Mover[] m;
int moverNum = 20;

void setup() {
  size(800, 800);
  m = new Mover[moverNum];
  for (int i=0; i<moverNum; i++) {
    m[i] = new Mover();
  }
  textAlign(CENTER, CENTER);
  textSize(20);
}

void draw() {
  background(255);
  PVector gravity = new PVector(0, 0.1);
  PVector wind = new PVector(0.05, 0);
  for (int i=0; i<moverNum; i++) {
    m[i].applyForce(gravity);
    m[i].applyForce(wind);
    m[i].update();
    m[i].display();
  }
}
