ArrayList<Vehicle> v;
int type = 0;

void setup(){
  size(800, 800);
  v = new ArrayList<Vehicle>();
}

void draw(){
  background(255);
  PVector target = new PVector(mouseX, mouseY);
  for(int i=0; i < v.size(); i++){
    Vehicle _v = v.get(i);
    _v.steering(target);
    _v.update();
    _v.display();
  }
}

void mousePressed(){
  v.add(new Vehicle(random(width), random(height), random(10, 30), type));
  type++;
  if(type > 2){
    type = 0;
  }
}
