float t = 0;
float r = 0;

void setup(){
  size(400, 800);
  r = PI*0.1;
}

void draw(){
  background(255);
  
  pushMatrix();
  translate(width/2, 0);
  rotate(HALF_PI + sin(t)*r);  
  line(0, 0, 600, 0);
  ellipse(600, 0, 30, 30);  
  popMatrix();
  
  t += 0.12;
  r -= 0.00058;
  if(r < 0.0){
    r = 0.0;
  }
}

void keyPressed(){
  if(key == ' '){
    r = PI*0.1;
  }
}
