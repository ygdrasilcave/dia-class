class Pendulum{
  
  float len;
  float angle;
  float angleVelocity;
  float angleAcc;
  float damping;
  float anchorX;
  float anchorY;
  
  Pendulum(float _l, float _startAngle, float _ax, float _ay){
    len = _l;
    angle = _startAngle;
    angleVelocity = 0;
    angleAcc = 0;
    damping = 0.995;
    
    anchorX = _ax;
    anchorY = _ay;
  }
  
  void resetStartAngle(float _startAngle){
    angle = _startAngle;
  }
  
  void update(){
    float gravity = 0.4;
    angleAcc = (-1 * sin(angle))/len * gravity;
    //println(angleAcc);
    angleVelocity += angleAcc;
    angle += angleVelocity;
    
    angleVelocity *= damping;
  }
  
  void display(){
    fill(0);
    rect(anchorX-5, anchorY-5, 10, 10);
    float x = anchorX + cos(angle+HALF_PI)*len;
    float y = anchorY + sin(angle+HALF_PI)*len;
    ellipse(x, y, 10, 10);
    //rect(x-15, y-15, 30, 30);
    
    //line(anchorX, anchorY, x, y);
    pushMatrix();
    translate(anchorX, anchorY);
    rotate(angle+HALF_PI);
    line(0, 0, len, 0);
    popMatrix();
  }
}
