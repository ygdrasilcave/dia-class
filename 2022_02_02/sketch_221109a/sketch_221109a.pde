Pendulum[] p;
int num = 150;

void setup() {
  size(800, 800);
  p = new Pendulum[num];
  for (int i=0; i<num; i++) {
    p[i] = new Pendulum(100 + 5*i, PI*0.2, (width/float(num))*i, 10);
  }
}

void draw() {
  background(255);
  for (int i=0; i<num; i++) {
    p[i].update();
    p[i].display();
  }
}

void keyPressed(){
  if(key == ' '){
    float _randAngle = random(-PI*0.25, PI*0.25);
    for (int i=0; i<num; i++) {
      p[i].resetStartAngle(_randAngle);
    }
  }
}
