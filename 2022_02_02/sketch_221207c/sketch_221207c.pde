ArrayList<Boid> boids;

void setup() {
  size(1500, 1000);
  rectMode(CENTER);
  boids = new ArrayList<Boid>();
  boids.add(new Boid(width/2, height/2));
}

void draw() {
  background(255);
  
  for (Boid b : boids) {
    b.flock(boids);
    b.update();
    b.display();
  }
  
}

void mouseDragged(){
  boids.add(new Boid(mouseX, mouseY));
}
