Mover[] m;

void setup() {
  size(800, 800);
  m = new Mover[5];
  for (int i=0; i<5; i++) {
    m[i] = new Mover();
  }
}

void draw() {
  background(255);
  for (int i=0; i<5; i++) {
    m[i].update();
    m[i].display();
  }
}
