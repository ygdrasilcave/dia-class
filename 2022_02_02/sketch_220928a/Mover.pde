class Mover{
  PVector location;
  PVector velocity;
  PVector acceleration;
  float dia;
  float maxSpeed;
  
  Mover(){
    location = new PVector(width/2, height/2);
    velocity = new PVector(0,0);
    
    float xDir = random(1);
    float yDir = random(1);
    if(xDir > 0.5){
      xDir = 1;
    }else{
      xDir = -1;
    }
    if(yDir > 0.5){
      yDir = 1;
    }else{
      yDir = -1;
    }
    
    acceleration = new PVector(random(0.001, 0.01)*xDir, random(0.001, 0.01)*yDir);
    dia = 50; 
    maxSpeed = random(5, 15);;
  }
  
  void update(){
    velocity.add(acceleration);
    //println(velocity);
    velocity.limit(maxSpeed);
    location.add(velocity);
    
    float r = dia*0.5;
    if(location.x > width+r){
      location.x = -r;
    }
    if(location.x < -r){
      location.x = width+r;
    }
    if(location.y > height+r){
      location.y = -r;
    }
    if(location.y < -r){
      location.y = height+r;
    }
  }
  
  void display(){
    strokeWeight(3);
    stroke(0);
    noFill();
    ellipse(location.x, location.y, dia, dia);
  }
}
