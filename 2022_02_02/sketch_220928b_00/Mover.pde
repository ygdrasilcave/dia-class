class Mover {
  PVector location;
  PVector velocity;
  PVector acceleration;
  boolean myType;

  Mover() {
    location = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    float t = random(1);
    if (t > 0.5) {
      myType = true;
    } else {
      myType = false;
    }
  }

  void update() {
    PVector target = new PVector(mouseX, mouseY);
    acceleration = target.sub(location);
    float dist = acceleration.mag();

    //println(acceleration);
    acceleration.normalize();

    if (myType == true) {
      acceleration.mult(0.1);
    } else {
      acceleration.mult(dist*0.05);
    }


    velocity.add(acceleration);

    if (myType == true) {
      velocity.limit(10);
    } else {
      velocity.limit(dist*0.05);
    }
    location.add(velocity);

  }

  void display() {
    stroke(0);
    strokeWeight(3);
    noFill();
    ellipse(location.x, location.y, 50, 50);
  }
}
