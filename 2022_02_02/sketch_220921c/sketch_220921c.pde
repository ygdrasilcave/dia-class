void setup() {
  size(800, 800);
}

void draw() {
  background(255);

  PVector mouse  = new PVector(mouseX, mouseY);
  PVector center = new PVector(width/2, height/2);

  PVector c;

  strokeWeight(3);
  stroke(255, 0, 0);
  line(0, 0, mouse.x, mouse.y);

  stroke(0, 0, 255);
  line(0, 0, center.x, center.y);

  c = PVector.sub(mouse, center);
  stroke(0);
  line(0, 0, c.x, c.y);


  mouse.sub(center);
  //mouse.normalize();
  //mouse.mult(100);
  mouse.setMag(100);
  println(mouse);

  translate(width/2, height/2);
  stroke(0, 255, 0);
  line(0, 0, mouse.x, mouse.y);
}
