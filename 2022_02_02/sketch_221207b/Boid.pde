class Boid{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  float maxSpeed;
  float maxForce;
  
  float r = 20;
  
  Boid(float _x, float _y){
    position = new PVector(_x, _y);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    maxSpeed = 5;
    maxForce = 0.05;
  }
  
  PVector separation(ArrayList<Boid> boids){
    PVector force = new PVector(0,0);
    return force;
  }
  
  PVector alignment(ArrayList<Boid> boids){
    PVector force = new PVector(0,0);
    return force;
  }
  
  PVector cohesion(ArrayList<Boid> boids){
    PVector force = new PVector(0,0);
    return force;
  }
  
  void flock(ArrayList<Boid> boids){
    PVector sep = separation(boids);
    PVector ali = alignment(boids);
    PVector coh = cohesion(boids);
    
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
  }
  
  void applyForce(PVector _f){
    PVector force = _f.copy();
    acceleration.add(force);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }
  
  void display(){
    stroke(0);
    noFill();
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(0, 0, r*2, r);
    beginShape();
    vertex(r, -r*0.5);
    vertex(r*2, 0);
    vertex(r, r*0.5);
    endShape(CLOSE);
    float sumR = 0;
    for(int i=0; i<5; i++){
      float tempR = r-(r*0.2*i);      
      if(i > 0){
        sumR = sumR + r-(r*0.2*(i-1));
      }
      ellipse(-r-(tempR*0.5)-sumR, 0, tempR, tempR);
    }
    
    ellipse(0, -r*0.5-(r*2.5*0.5), r*0.6, r*2.5);
    ellipse(r*0.5, -r*0.5-(r*2.5*0.5), r*0.6, r*2.5);
    
    ellipse(0, r*0.5+(r*2.5*0.5), r*0.6, r*2.5);
    ellipse(r*0.5, r*0.5+(r*2.5*0.5), r*0.6, r*2.5);
    popMatrix();
  }
}
