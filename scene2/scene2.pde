import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

IntList scene = new IntList();
int nextScene = 0;

void setup() {
  size(500, 500);
  for (int i=0; i<7; i++) {
    scene.set(i, i);
  }
  //scene.shuffle();
  println(scene);
  textAlign(CENTER, CENTER);
  
  oscP5 = new OscP5(this,13000);
  myRemoteLocation = new NetAddress("127.0.0.1",15000);
}

void draw() {
  background(255);
  sceneFunc(scene.get(nextScene));

  if (fadeStart == true) {
    transition(fade, next);
  }
}

void sceneFunc(int _num) {
  textSize(48);
  if (_num == 0) {
    background(0);
    fill(255);
    noStroke();
    //rect(width/2-50, height/2-50, 100, 100);
    text("ICH", width/2, height/2);
  } else if (_num == 1) {
    background(0);
    fill(255);
    noStroke();
    //ellipse(width/2, height/2, 100, 100);
    text("LIEBE", width/2, height/2);
  } else if (_num == 2) {
    background(0);
    fill(255);
    noStroke();
    //rect(width/2-50, height/2-50, 100, 100);
    text("DICH", width/2, height/2);
  } else if (_num == 3) {
    background(0);
    fill(255);
    noStroke();
    //ellipse(width/2, height/2, 100, 100);
    text("SO", width/2, height/2);
  } else if (_num == 4) {
    background(0);
    fill(255);
    noStroke();
    text("WIE", width/2, height/2);
  }else if (_num == 5) {
    background(0);
    fill(255);
    noStroke();
    text("DU", width/2, height/2);
  }else if (_num == 6) {
    background(0);
    fill(255);
    noStroke();
    text("MICH", width/2, height/2);
  }
}

int fadeVal = 0;
boolean fade = true;
boolean fadeStart = false;
int next = 0;
void transition(boolean _inOut, int _n) {  
  if (_inOut == true) {
    fadeVal+=20;
    if (fadeVal > 255) {
      fadeVal = 255;
      fade = false;
      /*nextScene += 1;          
      if (nextScene > scene.size()-1) {
        scene.shuffle();
        println(scene);
        nextScene = 0;
      }*/
      nextScene = _n;
      println(nextScene);
    }
  } else {
    fadeVal-=20;
    if (fadeVal < 0) {
      fadeVal = 0;
      fade = true;
      fadeStart = false;
    }
  }
  fill(0, fadeVal);
  noStroke();
  rect(0, 0, width, height);
}

void keyPressed() {
  if (key == '1') {
    fadeStart = true;
    next = 0;
  } else if (key == '2') {
    fadeStart = true;
    next = 1;
  } else if (key == '3') {
    fadeStart = true;
    next = 2;
  } else if (key == '4') {
    fadeStart = true;
    next = 3;
  } else if (key == '5') {
    fadeStart = true;
    next = 4;
  }
}

void oscEvent(OscMessage theOscMessage) { 
  if(theOscMessage.checkAddrPattern("/scene")==true) {
    println("TYPE: " +theOscMessage.typetag());
    if(theOscMessage.checkTypetag("f")) {
      int sceneValue = int(theOscMessage.get(0).floatValue());  
      fadeStart = true;
      next = sceneValue;
      println(next);
      return;
    }
  } 
}

