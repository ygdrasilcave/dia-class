Mover[] m;

void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(255, 0, 255);
  m = new Mover[50];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover();
  }
}

void draw() {
  background(255, 0, 255);

  for (int i=0; i<m.length; i++) {
    m[i].update();
    m[i].display();
  }
}
