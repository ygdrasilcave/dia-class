float gravity;
float wind;

Pendulum[] p;

void setup() {
  size(800, 800);
  gravity = 0.4;
  wind = 0;

  p = new Pendulum[15];
  for (int i=0; i<5; i++) {
    for (int j=0; j<3; j++) {
      PVector _pivot = new PVector((width/5)/2 + (width/5)*i, (height/3)*j);
      int index = i + j*5;
      p[index] = new Pendulum(_pivot, height/3-60, random(5, 45));
    }
  }
  println(p.length);
}

void draw() {
  background(255);
  for (int i=0; i<p.length; i++) {
    p[i].update();
    p[i].display();
  }
}

void mousePressed() {
  for (int i=0; i<p.length; i++) {
    p[i].dragging(mouseX, mouseY);
  }
}

void mouseReleased() {
  for (int i=0; i<p.length; i++) {
    p[i].drag = false;
  }
}

void keyPressed() {
  if (key == '1') {
    wind = -0.2;
  }
  if (key == '2') {
    wind = 0.2;
  }
}

void keyReleased() {
  if (key == '1' || key == '2') {
    wind = 0;
  }
}
