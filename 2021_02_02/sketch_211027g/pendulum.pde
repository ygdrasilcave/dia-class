class Pendulum {
  PVector position;
  PVector pivot;
  float r;
  float angle;
  float aVelocity;
  float aAcceleration;
  float damping;
  boolean drag = false;

  Pendulum(PVector _pivot, float _r, float _angle) {
    position = new PVector();
    pivot = _pivot.copy();
    r = _r;
    aVelocity = 0;
    aAcceleration = 0;
    angle = radians(_angle);
    damping = 0.998;
  }

  void update() {
    if (drag == true) {
      /*PVector mouse = new PVector(mouseX, mouseY);
       PVector diff = PVector.sub(mouse, pivot);
       angle = diff.heading() - HALF_PI;*/
      float diffX = mouseX - pivot.x;
      float diffY = mouseY - pivot.y;
      angle = atan2(diffY, diffX) - HALF_PI;
      aVelocity = 0;
      aAcceleration = 0;
    } else {
      float pendulumForce = (-1*gravity/r) * sin(angle) + (wind/r) * cos(angle);
      aAcceleration = pendulumForce;
      aVelocity += aAcceleration;
      aVelocity *= damping;
      angle += aVelocity;
    }
    position.x = pivot.x + cos(HALF_PI+angle)*r;
    position.y = pivot.y + sin(HALF_PI+angle)*r;
  }

  void display() {
    line(pivot.x, pivot.y, position.x, position.y);
    ellipse(position.x, position.y, 60, 60);
  }

  void dragging(float _mx, float _my) {
    float d = dist(_mx, _my, position.x, position.y);
    if (d < 30) {
      drag = true;
    }
  }
}
