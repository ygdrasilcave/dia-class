Vehicle[] v;
boolean debug = false;

void setup() {
  size(800, 800);
  v = new Vehicle[10];
  for (int i=0; i<v.length; i++) {
    v[i] = new Vehicle(width/2, height/2);
  }
}

void draw() {
  background(255);
  for (int i=0; i<v.length; i++) {
    v[i].newTarget(debug);
    v[i].update();
    v[i].display();
  }

  if (debug == true) {
    noFill();
    stroke(0);
    rect(100, 100, width-200, height-200);
  }
}

void keyPressed() {
  if(key == 'd'){
    debug = !debug;
  }
}
