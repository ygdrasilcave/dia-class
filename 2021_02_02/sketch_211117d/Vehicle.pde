class Vehicle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  float targetTheta;
  float targetCenterDist;
  float targetRadius;
  float targetThetaRange;

  Vehicle(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    maxSpeed = random(1, 6);
    maxForce = random(0.05, 0.25);
    targetTheta = random(TWO_PI);
    targetCenterDist = random(10, 200);
    targetRadius = random(20, 150);
    targetThetaRange = random(0.1, 0.4);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }

  void applyForce(PVector _f) {
    acceleration.add(_f);
  }

  void newTarget(boolean _draw) {

    PVector targetCenter = null;
    PVector target = null;
    PVector desired = null;

    if (position.x < 100) {
      desired = new PVector(maxSpeed, velocity.y);
    } else if (position.x > width-100) {
      desired = new PVector(-maxSpeed, velocity.y);
    } else if (position.y < 100) {
      desired = new PVector(velocity.x, maxSpeed);
    } else if (position.y > height-100) {
      desired = new PVector(velocity.x, -maxSpeed);
    } else {
      targetCenter = velocity.copy();
      targetCenter.normalize();
      targetCenter.mult(targetCenterDist);
      targetCenter.add(position);

      targetTheta = targetTheta + random(-targetThetaRange, targetThetaRange);
      float tx = targetCenter.x + cos(targetTheta)*targetRadius;
      float ty = targetCenter.y + sin(targetTheta)*targetRadius;
      target = new PVector(tx, ty);
      desired = PVector.sub(target, position);
      desired.normalize();
      desired.mult(maxSpeed);
    }
    if (targetCenter != null && target != null) {
      if (_draw == true) {
        stroke(0);
        noFill();
        line(position.x, position.y, targetCenter.x, targetCenter.y);
        line(targetCenter.x, targetCenter.y, target.x, target.y);
        ellipse(targetCenter.x, targetCenter.y, targetRadius*2, targetRadius*2);
        noStroke();
        fill(255, 0, 0);
        ellipse(target.x, target.y, 10, 10);
      }
    }

    actionSelection(desired);
  }

  void actionSelection(PVector _desired) {
    PVector steeringForce = PVector.sub(_desired, velocity);
    steeringForce.limit(maxForce);

    applyForce(steeringForce);
  }

  void display() {
    fill(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-15, -15, 30, 30);
    ellipse(20, 0, 10, 10);
    popMatrix();
  }
}
