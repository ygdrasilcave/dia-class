ArrayList<Vehicle> v;
boolean showTarget = false;

void setup() {
  size(800, 800);
  v = new ArrayList<Vehicle>();
  textAlign(LEFT, CENTER);
  textSize(24);
}

void draw() {
  background(255);
  /*for (int i=0; i < v.size(); i++) {
    v.get(i).newTarget(showTarget);
    v.get(i).update();
    v.get(i).display();
  }*/
  /*for(Vehicle _v : v){
    _v.newTarget(showTarget);
    _v.update();
    _v.display();
  }*/
  
  for (int i=v.size()-1; i>=0; i--) {
    Vehicle _v = v.get(i);
    _v.newTarget(showTarget);
    _v.update();
    _v.display();
    if(_v.age > _v.ageMax){
      v.remove(i);
    }
  }
  
  fill(0);
  noStroke();
  rect(0, 0, 80, 50);
  fill(255);
  text(v.size(), 20, 25);
}

void mousePressed(){
  v.add(new Vehicle(mouseX, mouseY));
}

void keyPressed(){
  if(key == 'a'){
    showTarget = !showTarget;
  }
}
