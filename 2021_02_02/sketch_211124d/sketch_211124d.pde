PVector a = new PVector(-3, 5);
PVector b = new PVector(10, 1);
println(a.dot(b));

PVector c = new PVector(10, 2);
PVector d = new PVector(4, -3);
float angle = PVector.angleBetween(c, d);
println(angle);

float dot = PVector.dot(c, d);
float theta = acos(dot / (c.mag()*d.mag()));
println(theta);
