class Vehicle {
  PVector position;
  PVector velocity;
  PVector acceleration;

  float maxSpeed;
  float maxForce;

  int tailNum = 10;
  float[] t;
  float tSpeed;
  
  boolean arrive = false;

  Vehicle(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    maxSpeed = random(1, 6);
    maxForce = 1;

    t = new float[tailNum];
    for (int i=0; i<tailNum; i++) {
      t[i] = i*0.85;
    }
    tSpeed = 0.225;
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }

  void applyForce(PVector _force) {
    acceleration.add(_force);
  }

  void actionSelection(PVector _target) {
    PVector desired = PVector.sub(_target, position);
    float dist = desired.mag();
    if (dist < 150) {
      float m = map(dist, 0, 150, 0, maxSpeed);
      float ts = map(dist, 0, 150, 0, 0.225);
      desired.normalize();
      desired.mult(m);
      tSpeed = ts;
      arrive = true;
    } else {
      desired.normalize();
      desired.mult(maxSpeed);
      tSpeed = 0.225;
      arrive = false;
    }

    PVector steeringForce = PVector.sub(desired, velocity);
    steeringForce.limit(maxForce);

    applyForce(steeringForce);
  }

  void display1() {
    noFill();
    stroke(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    //ellipse(0, 0, 30, 30);
    //rect(-100, -5, 100, 10);
    for (int i=0; i<tailNum; i++) {
      ellipse(i*-15, sin(t[i])*10, 30*(0.1*(10-i)), 30*(0.1*(10-i)));
      t[i] += tSpeed;
    }
    popMatrix();
  }

  void display2() {
    fill(175);
    stroke(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading() + PI/2);
    beginShape();
    vertex(0, -10*2);
    vertex(-10, 10*2);
    vertex(10, 10*2);
    endShape(CLOSE);
    popMatrix();
  }
}
