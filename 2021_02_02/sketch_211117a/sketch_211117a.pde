Vehicle[] v;

PVector target;

int currentTime;
int interval;

void setup() {
  size(800, 800);
  v = new Vehicle[10];
  for (int i=0; i<v.length; i++) {
    v[i] = new Vehicle(random(width), random(height));
  }
  target = new PVector(random(width), random(height));
  currentTime = millis();
  interval = 1000;
}

void draw() {
  background(255);

  /*if (currentTime + interval < millis()) {
   target = new PVector(random(width), random(height));
   currentTime = millis();
   interval = int(random(500, 5000));
   }*/

  fill(255, 0, 0);
  noStroke();
  ellipse(target.x, target.y, 10, 10);
  noFill();
  stroke(255, 0, 0);
  ellipse(target.x, target.y, 150, 150);

  int arriveTotal = 0;

  for (int i=0; i<v.length; i++) {
    v[i].actionSelection(target);
    v[i].update();
    v[i].display1();
    if(v[i].arrive == true){
      arriveTotal++;
    }
  }
  
  if (arriveTotal == v.length) {
    target = new PVector(random(width), random(height));
  }
}
