class Vehicle {

  PVector position;
  PVector velocity;
  PVector acceleration;

  float maxSpeed;
  float maxForce;

  float w;
  float h;

  boolean bTarget = false;

  Vehicle(float _x, float _y, float _ms, float _mf) {
    maxSpeed = _ms;
    maxForce = _mf;
    position = new PVector(_x, _y);
    velocity = new PVector(maxSpeed, 0);
    acceleration = new PVector(0, 0);

    w = 50;
    h = 25;
  }

  void follow(Path _path) {
    PVector future = velocity.copy();
    future.normalize();
    future.mult(80);
    PVector futurePosition = PVector.add(position, future);

    PVector shortestNormal = null;
    PVector target = null;
    float worldRecord = 1000000;

    for (int i=0; i<_path.points.size()-1; i++) {
      PVector A = PVector.sub(_path.points.get(i+1), _path.points.get(i));
      PVector B = PVector.sub(futurePosition, _path.points.get(i));
      A.normalize();
      float r = B.dot(A);
      A.mult(r);
      PVector normalPoint = PVector.add(_path.points.get(i), A);

      if (normalPoint.x < _path.points.get(i).x || normalPoint.x > _path.points.get(i+1).x) {
        normalPoint = _path.points.get(i+1);
      }

      float distance = PVector.dist(futurePosition, normalPoint);

      if (distance < worldRecord) {
        worldRecord = distance;
        shortestNormal = normalPoint;

        PVector dir = PVector.sub(_path.points.get(i+1), _path.points.get(i));
        dir.normalize();
        dir.mult(50);
        target = PVector.add(shortestNormal, dir);
      }
    }

    if (worldRecord > _path.radius) {
      PVector desiredVelocity = PVector.sub(target, position);
      desiredVelocity.normalize();
      desiredVelocity.mult(maxSpeed);

      PVector steer = PVector.sub(desiredVelocity, velocity);
      steer.limit(maxForce);

      applyForce(steer);
    }

    if (shortestNormal != null) {
      stroke(0);
      strokeWeight(1);
      line(position.x, position.y, futurePosition.x, futurePosition.y);
      line(futurePosition.x, futurePosition.y, shortestNormal.x, shortestNormal.y);

      noStroke();
      fill(0, 0, 255);
      ellipse(futurePosition.x, futurePosition.y, 10, 10);
      ellipse(shortestNormal.x, shortestNormal.y, 10, 10);
    }

    if (target != null) {
      noStroke();
      fill(255, 0, 0);
      ellipse(target.x, target.y, 10, 10);
    }
  }

  void applyForce(PVector _force) {
    acceleration.add(_force);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x > width) {
      position.x = 0;
      position.y = height/2;
    }
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    noStroke();
    fill(0);
    rect(-w*0.5, -h*0.5, w, h);
    ellipse(w*0.6, -h*0.25, h*0.4, h*0.4);
    ellipse(w*0.6, h*0.25, h*0.4, h*0.4);
    popMatrix();
  }
}
