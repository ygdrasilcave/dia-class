class Path {

  ArrayList<PVector> points;

  float radius;

  Path() {
    points = new ArrayList<PVector>();
    radius = 20;
  }

  void addPoint(float _x, float _y) {
    PVector point = new PVector(_x, _y);
    points.add(point);
  }

  void display() {
    noFill();
    
    stroke(0, 100);
    strokeWeight(radius*2);
    beginShape();
    for (PVector _p : points) {
      vertex(_p.x, _p.y);
    }
    endShape();

    stroke(0);
    strokeWeight(1);
    beginShape();
    for (PVector _p : points) {
      vertex(_p.x, _p.y);
    }
    endShape();
  }
}
