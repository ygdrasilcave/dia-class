void setup() {
  size(800, 800);
}

void draw() {
  background(255);

  PVector mouse = new PVector(mouseX, mouseY);
  PVector center = new PVector(width/2, height/2);
  PVector diff = PVector.sub(mouse, center);
  //float angleA = diff.heading();
  float angleA = atan2(diff.y, diff.x);
  pushMatrix();
  translate(width/2, height/2);
  line(0, 0, diff.x, diff.y);
  println(degrees(angleA));
  pushMatrix();
  rotate(angleA);
  fill(0);
  rect(-50, -50, 100, 100);
  ellipse(50, 0, 20, 20);
  popMatrix();
  popMatrix();
}
