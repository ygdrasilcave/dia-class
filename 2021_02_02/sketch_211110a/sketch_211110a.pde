Vehicle v1, v2;

void setup(){
  size(800, 800);
  v1 = new Vehicle(width/2, height/2);
  v2 = new Vehicle(random(width), random(height));
}

void draw(){
  background(255);
  
  PVector target = new PVector(mouseX, mouseY);
  v1.actionSelection(target);
  v1.update();
  v1.display1();
  
  v2.actionSelection(target);
  v2.update();
  v2.display2();
}
