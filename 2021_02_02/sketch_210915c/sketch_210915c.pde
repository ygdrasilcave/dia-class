PVector center;
PVector mouse;

void setup(){
  size(500, 500);
  center = new PVector(width/2, height/2);
  mouse = new PVector(mouseX, mouseY);
  textSize(24);
  textAlign(LEFT, CENTER);
}

void draw(){
  background(255);
  mouse.x = mouseX;
  mouse.y = mouseY;
  
  mouse.sub(center);
  mouse.normalize();
  mouse.mult(100);
  //mouse.setMag(100);
  
  pushMatrix();
  translate(width/2, height/2);
  stroke(0);
  strokeWeight(3);
  line(0, 0, mouse.x, mouse.y);
  popMatrix();
  
  fill(0);
  noStroke();
  text(mouse.mag(), 50, 50);
}
