PVector anchor;
PVector position;
PVector velocity;
PVector acceleration;
float K = 0.1;
float restLength;
float mass;
float damping;

//springForce = -1 * K * X
PVector springForce;
PVector wind;
PVector gravity;

boolean stretch = false;

void setup() {
  size(800, 800);
  anchor = new PVector(width/2, 50);
  restLength = 200;
  position = new PVector(anchor.x, anchor.y+restLength+50);
  springForce = new PVector();
  velocity = new PVector();
  mass = 10;
  damping = 0.98;
  acceleration = new PVector();
  wind = new PVector();
  gravity = new PVector(0, 2);
}

void draw() {
  background(255);

  if (stretch == false) {
    PVector _g = gravity.copy();
    _g.div(mass);
    acceleration.add(_g);
    PVector _w = wind.copy();
    _w.div(mass);
    acceleration.add(_w);

    springForce = PVector.sub(position, anchor);
    float currentLength = springForce.mag();
    float X = currentLength - restLength;
    springForce.normalize();
    springForce.mult(-1 * K * X);
    PVector _sf = springForce.copy();
    _sf.div(mass);
    acceleration.add(_sf);
  }
  velocity.add(acceleration);
  velocity.mult(damping);
  position.add(velocity);
  acceleration.mult(0);

  rect(position.x-25, position.y-25, 50, 50);
  line(anchor.x, anchor.y, position.x, position.y);
}

void keyPressed() {
  if (key == '1') {
    wind.set(2, 0);
  }
  if (key == '2') {
    wind.set(-2, 0);
  }
  if (key == CODED) {
    if (keyCode == UP) {
      stretch = true;
      //acceleration.set(0, 0);
      //velocity.set(0, 0);
      PVector diff = PVector.sub(position, anchor);
      float l = diff.mag();
      l += 5;
      diff.setMag(l);
      position = PVector.add(anchor, diff);
    }
    if (keyCode == DOWN) {
      stretch = true;
      //acceleration.set(0, 0);
      //velocity.set(0, 0);
      PVector diff = PVector.sub(position, anchor);
      float l = diff.mag();
      l -= 5;
      diff.setMag(l);
      position = PVector.add(anchor, diff);
    }
  }
}

void keyReleased() {
  if (key == '1' || key == '2') {
    wind.set(0, 0);
  }
  if (key == CODED) {
    if (keyCode == UP || keyCode == DOWN) {
      stretch = false;
    }
  }
}
