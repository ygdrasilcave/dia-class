class Boid {

  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  float r;

  Boid(float _x, float _y) {
    position = new PVector(_x, _y);
    velocity = new PVector(random(-1, 1), random(-1, 1));
    acceleration = new PVector(0, 0);
    maxSpeed = 2;
    maxForce = 0.02;
    r = 8;
  }

  void applyForce(PVector _f) {
    acceleration.add(_f);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0.0);
  }

  void flock(ArrayList<Boid> _boids) {
    PVector separation = separate(_boids);
    PVector alignment = align(_boids);
    PVector cohesion = cohere(_boids);

    separation.mult(1.5);
    alignment.mult(1.0);
    cohesion.mult(1.0);

    applyForce(separation);
    applyForce(alignment);
    applyForce(cohesion);
  }

  PVector separate(ArrayList<Boid> _boids) {
    float desiredSeparation = 25.0;
    PVector steer = new PVector(0, 0);
    int count = 0;

    for (Boid _other : _boids) {
      float dist = PVector.dist(position, _other.position);
      if ((dist > 0) && (dist < desiredSeparation)) {
        PVector diff = PVector.sub(position, _other.position);
        diff.normalize();
        diff.div(dist);
        steer.add(diff);
        count++;
      }
    }

    if (count > 0) {
      steer.div(float(count));
      steer.normalize();
      steer.mult(maxSpeed);
      //steer = desiredVelocity - velocity
      steer.sub(velocity);
      steer.limit(maxForce);
    }

    return steer;
  }

  PVector align(ArrayList<Boid> _boids) {
    float neighborDist = 50.0;
    PVector sumVelocity = new PVector(0, 0);
    int count = 0;
    for (Boid _other : _boids) {
      float dist = PVector.dist(position, _other.position);
      if ((dist>0) && (dist<neighborDist)) {
        sumVelocity.add(_other.velocity);
        count++;
      }
    }

    if (count > 0) {
      sumVelocity.div(float(count));
      sumVelocity.normalize();
      sumVelocity.mult(maxSpeed);
      //steer = desiredVelocity - velocity
      PVector steer = PVector.sub(sumVelocity, velocity);
      steer.limit(maxForce);
      return steer;
    } else {
      return sumVelocity;
    }
  }

  PVector cohere(ArrayList<Boid> _boids) {
    float neighborDist = 50.0;
    PVector sumPosition = new PVector(0, 0);
    int count = 0;
    for (Boid _other : _boids) {
      float dist = PVector.dist(position, _other.position);
      if ((dist>0) && (dist<neighborDist)) {
        sumPosition.add(_other.position);
        count++;
      }
    }
    if (count > 0) {
      //new center target
      sumPosition.div(float(count));
      //---seek function
      PVector desiredVelocity = PVector.sub(sumPosition, position);
      desiredVelocity.setMag(maxSpeed);
      //steer = desiredVelocity - velocity
      PVector steer = PVector.sub(desiredVelocity, velocity);
      steer.limit(maxForce);
      //---seek funciton
      return steer;
    } else {
      return sumPosition;
    }
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    beginShape();
    vertex(r, 0);
    vertex(-r, r*0.65);
    vertex(-r, -r*0.65);
    endShape(CLOSE);
    popMatrix();

    if (position.x < -r*2) {
      position.x = width + r*2;
    }
    if (position.x > width + r*2) {
      position.x = -r*2;
    }
    if (position.y < -r*2) {
      position.y = height + r*2;
    }
    if (position.y > height + r*2) {
      position.y = -r*2;
    }
  }
}
