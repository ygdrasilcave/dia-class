float x = 10;
float y = 20;
float xSpeed = 1;
float ySpeed = 1;
float radius = 15;
float xDir = 1;
float yDir = 1;

void setup(){
  size(800, 800);
  background(255);
  xSpeed = random(1.5);
  ySpeed = random(2.4);
}

void draw(){
  background(255);
  
  x += xSpeed*xDir;
  y += ySpeed*yDir;
  
  if(x > width-radius){
    x = width-radius;
    xDir *= -1;
  }
  if(x < radius){
    x = radius;
    xDir *= -1;
  }
  if(y > height-radius){
    y = height-radius;
    yDir *= -1;
  }
  if(y < radius){
    y = radius;
    yDir *= -1;
  }
  
  fill(0);
  noStroke();
  ellipse(x, y, radius*2, radius*2);
}
