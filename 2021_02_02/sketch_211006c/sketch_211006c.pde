Mover[] m;
boolean onEarth = false;

void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(255, 0, 255);
  m = new Mover[20];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(random(1, 5), random(200, width-200), 100);
  }
  textAlign(CENTER, CENTER);
  textSize(15);
}

void draw() {
  background(255, 0, 255);

  PVector wind = new PVector(0.056, 0);
  PVector gravity = new PVector(0, 0.1);
  for (int i=0; i<m.length; i++) {
    if (mousePressed) {
      m[i].applyForce(wind);
    }

    if (onEarth == true) {
      PVector g = PVector.mult(gravity, m[i].mass);
      m[i].applyForce(g);
    } else {
      m[i].applyForce(gravity);
    }

    m[i].update();
    m[i].display();
  }
}
