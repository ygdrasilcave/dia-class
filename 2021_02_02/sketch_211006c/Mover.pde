class Mover {
  PVector position;
  PVector vel;
  PVector acc;
  float h;
  float s;
  float b;
  float radius;
  float eyeX;
  float eyeY;
  boolean style;
  float maxVel;
  
  float mass;

  Mover(float _mass, float _x, float _y) {
    //position = new PVector(random(width), random(height));
    position = new PVector(_x, _y);
    vel = new PVector(0,0);
    acc = new PVector(0,0);
    h = random(255);
    s = 255;
    b = 255;
    
    mass = _mass;
    radius = mass*10;
    
    eyeX = random(radius*0.1, radius);
    eyeY = random(radius*0.1, radius);
    float _s = random(10);
    if (_s < 8) {
      style = true;
    } else {
      style = false;
    }
    style = true;
    
    //maxVel = random(0.5, 6);
    maxVel = 5;
  }
  
  void applyForce(PVector _force){
    PVector f = PVector.div(_force, mass);
    acc.add(f);
  }

  void update() {  
    vel.add(acc);
    //vel.limit(maxVel);
    position.add(vel);
    acc.mult(0);

    if (position.x > width-radius) {
      position.x = width-radius;
      vel.x *= -1;
    } else if (position.x < radius) {
      position.x = radius;
      vel.x *= -1;
    }
    if (position.y > height-radius) {
      position.y = height-radius;
      vel.y *= -1;
    } else if (position.y < radius) {
      position.y = radius;
      vel.y *= -1;
    }
  }

  void display() {
    fill(h, s, b);
    if (style == true) {
      ellipse(position.x, position.y, radius*2, radius*2);
    } else {
      float dia = radius*2;
      rect(position.x-radius, position.y-radius, dia, dia);
    }

    fill(h, 0, 0);
    float eyeDia = (radius*2)*0.1;
    ellipse(position.x-eyeX, position.y-eyeY, eyeDia, eyeDia);
    ellipse(position.x+eyeX, position.y-eyeY, eyeDia, eyeDia);
    
    text(maxVel, position.x, position.y + eyeY);
  }
}
