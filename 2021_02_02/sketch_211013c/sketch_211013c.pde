Mover[] m;
boolean onEarth = false;

Liquid l1;
Liquid l2;

void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(255, 0, 255);
  m = new Mover[20];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(random(1, 5), random(200, width-200), 100);
  }
  textAlign(CENTER, CENTER);
  textSize(15);

  l1 = new Liquid(0, height/3, width, 100, 0.1, 1);
  l2 = new Liquid(0, height/3+200, width, 150, 0.1, 6);
}

void draw() {
  background(255, 0, 255);

  l1.display();
  l2.display();

  PVector gravity = new PVector(0.0, 0.1);
  PVector wind = new PVector(0.0, -0.15);

  for (int i=0; i<m.length; i++) {

    if (onEarth == true) {
      PVector g = PVector.mult(gravity, m[i].mass);
      m[i].applyForce(g);
    } else {
      m[i].applyForce(gravity);
    }

    if (mousePressed == true) {
      m[i].applyForce(wind);
    }

    if (m[i].isInside(l1) == true) {
      m[i].drag(l1);
    }
    if (m[i].isInside(l2) == true) {
      m[i].drag(l2);
    }

    m[i].update();
    m[i].display();
  }

  pushMatrix();
  translate(width-50, 50);
  float magOfGravity = gravity.mag();
  strokeWeight(3);
  stroke(200, 255, 255);
  line(0, 0, 0, magOfGravity*1000);
  if (mousePressed) {
    float magOfWind = wind.mag();
    stroke(100, 255, 255);
    line(-25, 0, -25, magOfWind*1000);
  }
  popMatrix();
}
