class Liquid {
  float x, y, w, h;
  float coefficientOfDrag = 0.1;
  float density = 6;
  
  Liquid(float _x, float _y, float _w, float _h, float _c, float _d){
    x = _x;
    y = _y;
    w = _w;
    h = _h;
    coefficientOfDrag = _c;
    density = _d;
  }
  
  void display(){
    noStroke();
    fill(0, 0, map(density, 0, 8, 255, 0));
    rect(x, y, w, h);
  }
}
