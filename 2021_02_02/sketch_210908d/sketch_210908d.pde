PVector v1;
PVector v2;

void setup(){
  size(800, 800);
  background(255);
  v1 = new PVector(300, 400);
  v2 = new PVector(20, 300);
  
  strokeWeight(5);
  stroke(0);
  line(0, 0, v1.x, v1.y);
  stroke(255, 0, 0);
  line(0, 0, v2.x, v2.y);
  
  v1.add(v2);
  //v1.sub(v2);
  stroke(0, 255, 0);
  line(0, 0, v1.x, v1.y);
  
  //noStroke();
  //fill(255, 0, 0);
  //ellipse(v1.x, v1.y, 20, 20);
}

void draw(){
  //background(255);
  
  /*noStroke();
  fill(255, 0, 0);
  ellipse(200, 300, 20, 20);*/
}
