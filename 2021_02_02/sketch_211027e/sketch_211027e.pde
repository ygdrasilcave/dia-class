float r;
float angle;
float aVelocity;
float aAcceleration;
float gravity;

PVector position;
PVector pivot;

float damping;

boolean dragging = false;

float wind;

void setup() {
  size(800, 800);
  position = new PVector();
  pivot = new PVector(width/2, 50);
  r = 400;
  aVelocity = 0;
  aAcceleration = 0;
  angle = radians(45);
  gravity = 0.4;
  damping = 0.998;
  
  wind = 0;
}

void draw() {
  background(255);

  if (dragging == true) {
    /*PVector mouse = new PVector(mouseX, mouseY);
    PVector diff = PVector.sub(mouse, pivot);
    angle = diff.heading() - HALF_PI;*/
    float diffX = mouseX - pivot.x;
    float diffY = mouseY - pivot.y;
    angle = atan2(diffY, diffX) - HALF_PI;
    aVelocity = 0;
    aAcceleration = 0;
  } else {
    float pendulumForce = (-1*gravity/r) * sin(angle) + (wind/r) * cos(angle);
    aAcceleration = pendulumForce;
    aVelocity += aAcceleration;
    aVelocity *= damping;
    angle += aVelocity;
  }

  position.x = pivot.x + cos(HALF_PI+angle)*r;
  position.y = pivot.y + sin(HALF_PI+angle)*r;

  line(pivot.x, pivot.y, position.x, position.y);
  ellipse(position.x, position.y, 60, 60);
}

void mousePressed() {
  float d = dist(mouseX, mouseY, position.x, position.y);
  if (d < 30) {
    dragging = true;
  }
}

void mouseReleased(){
  dragging = false;
}

void keyPressed(){
  if(key == '1'){
    wind = -0.2;
  }
  if(key == '2'){
    wind = 0.2;
  }
}

void keyReleased(){
  if(key == '1' || key == '2'){
    wind = 0;
  }
}
