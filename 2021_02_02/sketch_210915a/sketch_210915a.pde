MyEllipse me1;
MyEllipse me2;
float t = 0.0;

void setup(){
  size(1000, 1000);
  background(0);
  me1 = new MyEllipse(7, 100, 30);
  me2 = new MyEllipse(60, 100, 30);
}

void draw(){
  background(0);
  me1.drawMyEllipse(width/2, height/2, 255, 255, 0);
  me2.drawMyEllipse(mouseX, mouseY, sin(t)*80, 255, 0, 255);
  t+=0.065;
}
