class MyEllipse{
  int num;
  float w;
  float h;

  MyEllipse(int _n, float _w, float _h){
    num = _n;
    w = _w;
    h = _h;
  }
  
  void drawMyEllipse(float _x, float _y, float _r, float _g, float _b){
    stroke(_r, _g, _b);
    noFill();
    for(int i=0; i<num; i++){
      pushMatrix();
      translate(_x, _y);
      rotate(radians((360.0/num)*i));
      ellipse(0, 0, w, h);
      popMatrix();
    }
  }
  
  void drawMyEllipse(float _x, float _y, float _offset, float _r, float _g, float _b){
    stroke(_r, _g, _b);
    noFill();
    for(int i=0; i<num; i++){
      pushMatrix();
      translate(_x, _y);
      rotate(radians((360.0/num)*i));
      ellipse(_offset, 0, w, h);
      popMatrix();
    }
  }
  
  
}
