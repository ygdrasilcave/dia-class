class Mover {
  PVector position;
  PVector vel;
  PVector acc;
  float h;
  float s;
  float b;
  float radius;
  float eyeX;
  float eyeY;
  boolean style;
  float maxVel;

  PVector target;
  boolean targetOn = false;

  Mover() {
    position = new PVector(random(width), random(height));
    vel = new PVector(0, 0);
    acc = new PVector(random(-0.01, 0.01), random(-0.01, 0.01));
    h = random(255);
    s = 255;
    b = 255;
    radius = random(10, 50);
    eyeX = random(radius*0.1, radius);
    eyeY = random(radius*0.1, radius);
    float _s = random(10);
    if (_s < 8) {
      style = true;
    } else {
      style = false;
    }

    maxVel = random(0.5, 6);
  }

  void update(boolean _onOff, PVector _target) {
    targetOn = _onOff;
    if (targetOn == true) {
      target = _target;
      PVector diff = PVector.sub(target, position);
      if (diff.mag() < 100) {
        diff.mult(0);
        vel.mult(0.95);
      } else {
        diff.normalize();
        diff.mult(0.5);
      }
      acc = diff;
    } else {
      acc = PVector.random2D();
      acc.mult(0.5);
    }

    vel.add(acc);
    vel.limit(maxVel);
    position.add(vel);

    if (position.x > width+radius) {
      position.x = -radius;
    } else if (position.x < -radius) {
      position.x = width+radius;
    }
    if (position.y > height+radius) {
      position.y = -radius;
    } else if (position.y < -radius) {
      position.y = height+radius;
    }
  }

  void display() {
    noStroke();
    fill(h, s, b);
    if (style == true) {
      ellipse(position.x, position.y, radius*2, radius*2);
    } else {
      float dia = radius*2;
      rect(position.x-radius, position.y-radius, dia, dia);
    }

    fill(h, 0, 0);
    float eyeDia = (radius*2)*0.1;
    ellipse(position.x-eyeX, position.y-eyeY, eyeDia, eyeDia);
    ellipse(position.x+eyeX, position.y-eyeY, eyeDia, eyeDia);

    text(maxVel, position.x, position.y + eyeY);
  }
}
