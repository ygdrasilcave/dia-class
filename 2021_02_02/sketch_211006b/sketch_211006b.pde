Mover[] m;

PVector targetPos;
boolean targetOn = false;
boolean toggle = true;

PVector targetVel;
PVector targetAcc;

void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(255, 0, 255);
  m = new Mover[20];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover();
  }
  textAlign(CENTER, CENTER);
  textSize(15);

  targetPos = new PVector(random(width), random(height));
  targetVel = new PVector();
  targetAcc = new PVector(random(-0.01, 0.01), random(-0.01, 0.01));
}

void draw() {
  background(255, 0, 255);

  if (second()%10 == 0 && toggle == true) {
    targetOn = !targetOn;
    toggle = false;
  } else if (second()%10 == 1 && toggle == false) {
    toggle = true;
  }

  if (targetOn == true) {
    targetVel.add(targetAcc);
    targetVel.limit(10);
    targetPos.add(targetVel);

    if (targetPos.x > width+100) {
      targetPos.x = -100;
    } else if (targetPos.x < -100) {
      targetPos.x = width+100;
    }
    if (targetPos.y > height+100) {
      targetPos.y = -100;
    } else if (targetPos.y < -100) {
      targetPos.y = height+100;
    }

    //noFill();
    //stroke(255, 255, 255);
    //ellipse(targetPos.x, targetPos.y, 200, 200);
  }

  for (int i=0; i<m.length; i++) {
    m[i].update(targetOn, targetPos);
    m[i].display();
  }
  
  noFill();
  stroke(255, 255, 255);
  ellipse(targetPos.x, targetPos.y, 200, 200);
}
