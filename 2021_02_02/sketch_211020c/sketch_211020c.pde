void setup() {
  size(800, 800);
}

void draw() {
  background(255);

  float w = 100;

  PVector mouse = new PVector(mouseX, mouseY);

  for (int x=0; x<width; x+=w) {
    for (int y=0; y<height; y+=w) {
      PVector pos = new PVector(w*0.5 + x, w*0.5 + y);
      PVector diff = PVector.sub(mouse, pos);
      float angleA = diff.heading();
      //float angleA = atan2(diff.y, diff.x);
      pushMatrix();
      translate(pos.x, pos.y);
      line(0, 0, diff.x, diff.y);
      println(degrees(angleA));
      pushMatrix();
      rotate(angleA);
      fill(0);
      rect(-20, -20, 40, 40);
      ellipse(20, 0, 10, 10);
      popMatrix();
      popMatrix();
    }
  }
}
