float r = 400;
float fr = 0.0;
float fr2 = 0.0;

PVector pivot;
PVector position;

void setup(){
  size(800, 800);
  pivot = new PVector(width/2, 50);
  position = new PVector();
}

void draw(){
  background(255);
  
  pivot.set(mouseX, mouseY);
  
  float t = radians(map(sin(fr), -1, 1, 45, 135));
  float radius = map(cos(fr2), -1, 1, 0.5, 1)*r;
  
  position.x = pivot.x + cos(t)*radius;
  position.y = pivot.y + sin(t)*radius;
  
  line(pivot.x, pivot.y, position.x, position.y);
  rect(position.x-30, position.y-30, 60, 60);
  
  fr += 0.05;
  fr2 += 0.2;
  
}
