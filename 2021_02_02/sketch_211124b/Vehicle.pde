class Vehicle{
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  float rectSize;
  float tt;
  
  Vehicle(PVector _pos){
    position = _pos.copy();
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    rectSize = random(15, 30);
    
    maxSpeed = random(2, 6);
    maxForce = random(0.08, 0.25);
  }
  
  void follow(FlowField _ff){
    PVector desired = _ff.lookup(position);
    desired.normalize();
    desired.mult(maxSpeed);
    actionSelection(desired);
  }
  
  void actionSelection(PVector _desired) {
    PVector steeringForce = PVector.sub(_desired, velocity);
    steeringForce.limit(maxForce);
    applyForce(steeringForce);
  } 
  
  void applyForce(PVector _force){
    acceleration.add(_force);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
  }
  
  void display(){
    fill(0);
    noStroke();
    
    float t = map(sin(tt), -1, 1, 0, 1);
    
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    
    rect(-rectSize*0.5, -rectSize*0.5, rectSize, rectSize);
    beginShape();
    vertex(rectSize*0.5, -rectSize*0.5);
    vertex(rectSize*2, -rectSize*0.5*t);
    vertex(rectSize*0.5, 0);
    vertex(rectSize*2-rectSize*0.3, rectSize*0.5*t);
    vertex(rectSize*0.5, rectSize*0.5);
    endShape();
    
    beginShape();
    vertex(-(rectSize*0.5+rectSize), -rectSize*0.5);
    vertex(-rectSize*0.5, 0);
    vertex(-(rectSize*0.5+rectSize), rectSize*0.5);
    endShape();
    
    fill(255);
    ellipse(rectSize*0.25, 0, rectSize*0.25, rectSize*0.25);
    
    popMatrix();
    
    tt += 0.135;
    
    float margin = rectSize*2;
    if(position.x < -margin){
      position.x = width + margin;
    }
    if(position.x > width+margin){
      position.x = -margin;
    }
    if(position.y < -margin){
      position.y = height + margin;
    }
    if(position.y > height+margin){
      position.y = -margin;
    }
  }
}
