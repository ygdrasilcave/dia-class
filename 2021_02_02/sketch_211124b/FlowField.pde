class FlowField {
  int cols;
  int rows;
  int pixelSize;
  PVector[][] field;

  FlowField(int _ps) {
    pixelSize = _ps;
    cols = int(width/pixelSize);
    rows = int(height/pixelSize);
    field = new PVector[cols][rows];
    init();
  }

  void init() {
    float xoff = 0;
    for (int x=0; x<cols; x+=1) {
      float yoff = 0;
      for (int y=0; y<rows; y+=1) {
        //field[x][y] = new PVector(1, 1);
        //field[x][y] = PVector.random2D();
        /*float theta = random(0, TWO_PI);
         field[x][y] = new PVector(cos(theta), sin(theta));*/
        float theta = map(noise(xoff, yoff), 0, 1, 0, TWO_PI);
        field[x][y] = new PVector(cos(theta), sin(theta));
        yoff += 0.1;
      }
      xoff += 0.1;
    }
  }

  void display() {
    noFill();
    for (int x=0; x<cols; x+=1) {
      for (int y=0; y<rows; y+=1) {
        //stroke(0);
        //rect(x*pixelSize, y*pixelSize, pixelSize, pixelSize);
        float rotateValue = field[x][y].heading();
        arrow(x*pixelSize, y*pixelSize, rotateValue);
      }
    }
  }

  void arrow(float _x, float _y, float _rotateValue) {
    stroke(255, 0, 0);
    pushMatrix();
    translate(_x+pixelSize*0.5, _y+pixelSize*0.5);
    rotate(_rotateValue);
    line(-(pixelSize*0.5)+5, 0, pixelSize*0.5-5, 0);
    line(pixelSize*0.5-5, 0, pixelSize*0.5-5-5, -5);
    line(pixelSize*0.5-5, 0, pixelSize*0.5-5-5, 5);
    popMatrix();
  }
  
  PVector lookup(PVector _pos){
    int _x = constrain(int(_pos.x/pixelSize), 0, cols-1);
    int _y = constrain(int(_pos.y/pixelSize), 0, rows-1);
    return field[_x][_y].copy();
  }
}
