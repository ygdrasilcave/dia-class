class Vehicle{
  
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  float maxSpeed;
  float maxForce;
  
  float w;
  float h;
  
  Vehicle(){
    position = new PVector(width/2, height/2);
    velocity = new PVector(0,0);
    acceleration = new PVector(0,0);
    maxSpeed = 6;
    maxForce = 0.5;
    
    w = 60;
    h = 30;
  }
  
  void follow(Path _path){
    PVector future = velocity.copy();
    future.normalize();
    future.mult(80);
    PVector futurePosition = PVector.add(position, future);
    
    PVector A = PVector.sub(_path.end, _path.start);
    PVector B = PVector.sub(futurePosition, _path.start);
    A.normalize(); 
    float r = B.dot(A);
    A.mult(r);
    PVector normalPoint = PVector.add(_path.start, A);
    PVector dir = normalPoint.copy();
    dir.normalize();
    dir.mult(20);
    PVector target = PVector.add(normalPoint, dir);
    
    PVector desiredVelocity = PVector.sub(target, position);
    desiredVelocity.normalize();
    desiredVelocity.mult(maxSpeed);
    
    PVector steer = PVector.sub(desiredVelocity, velocity);
    steer.limit(maxForce);
    
    applyForce(steer);
    
  }
  
  void applyForce(PVector _force){
    acceleration.add(_force);
  }
  
  void update(){
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);
    
    if(position.x > width + 100){
      position.x = -100;
      position.y = height/2;
    }
  }
  
  void display(){
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-w*0.5, -h*0.5, w, h);
    fill(0);
    ellipse(w*0.6, -h*0.25, h*0.4, h*0.4);
    ellipse(w*0.6, h*0.25, h*0.4, h*0.4);
    popMatrix();
  }
}
