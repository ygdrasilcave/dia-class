class Path {

  PVector start;
  PVector end;
  
  float radius;

  Path() {
    start = new PVector(0, random(height));
    end = new PVector(width, random(height));
    radius = 20;
  }

  void display() {
    stroke(0, 100);
    strokeWeight(radius*2);
    line(start.x, start.y, end.x, end.y);
    
    stroke(0);
    strokeWeight(1);
    line(start.x, start.y, end.x, end.y);
  }
}
