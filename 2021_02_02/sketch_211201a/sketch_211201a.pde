Path p;
Vehicle v;

void setup(){
  size(1000, 500);
  p = new Path();
  v = new Vehicle();
}

void draw(){
  background(255);
  
  p.display();
  
  v.follow(p);
  v.update();
  v.display();
}
