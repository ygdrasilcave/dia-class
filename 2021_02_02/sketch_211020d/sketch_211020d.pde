float t = 0.0;
float r = 0;
float t1 = 0;

void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  
  pushMatrix();
  translate(width/2, height/2);
  rotate(t);
  strokeWeight(5);
  stroke(0);
  line(0, 0, 300, 0);
  noFill();
  rect(300-50, -50, 100, 100);
  popMatrix();
  
  float x = cos(t)*r + width/2;
  float y = sin(t)*r + height/2;
  strokeWeight(5);
  stroke(255, 0, 0);
  line(width/2, width/2, x, y);
  noFill();
  rect(x-50, y-50, 100, 100);
  
  t += 0.01;
  t1 += 0.05;
  r = sin(t1)*300;
}
