float t = 0.0;
float amp = 45;

void setup(){
  size(800, 800);
}

void draw(){
  background(255);
  
  pushMatrix();
  translate(width/2, 50);
  rotate(HALF_PI);
  pushMatrix();
  rotate(radians(sin(t)*amp));  
  line(0, 0, 400, 0);
  rect(400-30, -30, 60, 60);
  popMatrix();
  popMatrix();
  
  t += 0.05;
  amp -= 0.02;
  if(amp < 0){
    amp = 0;
  }
}
