float r;
float angle;
float aVelocity;
float aAcceleration;
float gravity;

PVector position;
PVector pivot;

float damping;

void setup(){
  size(800,800);
  position = new PVector();
  pivot = new PVector(width/2, 50);
  r = 400;
  aVelocity = 0;
  aAcceleration = 0;
  angle = radians(45);
  gravity = 0.4;
  damping = 0.998;
}

void draw(){
  background(255);
  
  float pendulumForce = (-1*gravity/r) * sin(angle);
  aAcceleration = pendulumForce;
  aVelocity += aAcceleration;
  aVelocity *= damping;
  angle += aVelocity;
  
  position.x = pivot.x + cos(HALF_PI+angle)*r;
  position.y = pivot.y + sin(HALF_PI+angle)*r;
  
  line(pivot.x, pivot.y, position.x, position.y);
  ellipse(position.x, position.y, 60, 60);
}
