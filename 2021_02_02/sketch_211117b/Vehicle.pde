class Vehicle {
  PVector position;
  PVector velocity;
  PVector acceleration;
  float maxSpeed;
  float maxForce;
  float targetTheta;
  float targetCenterDist;
  float targetRadius;
  float targetThetaRange;

  Vehicle() {
    position = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    maxSpeed = random(1, 6);
    maxForce = random(0.05, 0.25);
    targetTheta = random(TWO_PI);
    targetCenterDist = random(10, 200);
    targetRadius = random(20, 150);
    targetThetaRange = random(0.1, 0.4);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x < -50) {
      position.x = width+50;
    }
    if (position.x > width+50) {
      position.x = -50;
    }
    if (position.y < -50) {
      position.y = height+50;
    }
    if (position.y > height+50) {
      position.y = -50;
    }
  }

  void applyForce(PVector _f) {
    acceleration.add(_f);
  }

  void newTarget(boolean _draw) {
    PVector targetCenter = velocity.copy();
    targetCenter.normalize();
    targetCenter.mult(targetCenterDist);
    targetCenter.add(position);

    targetTheta = targetTheta + random(-targetThetaRange, targetThetaRange);
    float tx = targetCenter.x + cos(targetTheta)*targetRadius;
    float ty = targetCenter.y + sin(targetTheta)*targetRadius;
    PVector target = new PVector(tx, ty);

    if (_draw == true) {
      stroke(0);
      noFill();
      line(position.x, position.y, targetCenter.x, targetCenter.y);
      line(targetCenter.x, targetCenter.y, target.x, target.y);
      ellipse(targetCenter.x, targetCenter.y, targetRadius*2, targetRadius*2);
      noStroke();
      fill(255, 0, 0);
      ellipse(target.x, target.y, 10, 10);
    }

    actionSelection(target);
  }

  void actionSelection(PVector _target) {
    PVector desired = PVector.sub(_target, position);
    desired.normalize();
    desired.mult(maxSpeed);

    PVector steeringForce = PVector.sub(desired, velocity);
    steeringForce.limit(maxForce);

    applyForce(steeringForce);
  }

  void display() {
    fill(0);
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    rect(-15, -15, 30, 30);
    ellipse(20, 0, 10, 10);
    popMatrix();
  }
}
