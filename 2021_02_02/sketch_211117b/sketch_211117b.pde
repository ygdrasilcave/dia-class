Vehicle[] v;

void setup() {
  size(800, 800);
  v = new Vehicle[30];
  for (int i=0; i<v.length; i++) {
    v[i] = new Vehicle();
  }
}

void draw() {
  background(255);
  for (int i=0; i<v.length; i++) {
    v[i].newTarget(false);
    v[i].update();
    v[i].display();
  }
}
