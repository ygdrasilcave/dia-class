import netP5.*;
import oscP5.*;
OscP5 oscP5;
NetAddress myRemoteLocation;

Flock f;

float separateWeight = 1.5;
float alignWeight = 1.0;
float cohereWeight = 1.0;

void setup() {
  size(800, 800);
  f = new Flock();

  oscP5 = new OscP5(this, 8000);
  myRemoteLocation = new NetAddress("192.168.0.5", 8080);
  
  textAlign(LEFT, CENTER);
  textSize(20);
}

void draw() {
  background(255);
  f.run();
  
  fill(0);
  noStroke();
  text("separate weight: " + separateWeight, 20, 20);
  text("align weight: " + alignWeight, 20, 50);
  text("cohere weight: " + cohereWeight, 20, 80);
}

void mouseDragged() {
  f.addBoid(new Boid(mouseX, mouseY));
}

void oscEvent(OscMessage theOscMessage) {
  if (theOscMessage.checkAddrPattern("/clean__project_2__slider_1")==true) {
    if (theOscMessage.checkTypetag("f")) {
      separateWeight = theOscMessage.get(0).floatValue();
      separateWeight *= 10;
      return;
    }
  } else if (theOscMessage.checkAddrPattern("/clean__project_2__slider_2")==true) {
    if (theOscMessage.checkTypetag("f")) {
      alignWeight = theOscMessage.get(0).floatValue();
      alignWeight *= 5;
      return;
    }
  } else if (theOscMessage.checkAddrPattern("/clean__project_2__slider_3")==true) {
    if (theOscMessage.checkTypetag("f")) {
      cohereWeight = theOscMessage.get(0).floatValue();
      cohereWeight *= 5;
      return;
    }
  }
  //println("### received an osc message. with address pattern "+theOscMessage.addrPattern());
}
