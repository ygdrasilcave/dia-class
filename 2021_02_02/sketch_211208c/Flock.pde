class Flock {

  ArrayList<Boid> boids;

  Flock() {
    boids = new ArrayList<Boid>();
  }

  void run() {
    for (Boid _b : boids) {
      _b.flock(boids);
      _b.update();
      _b.display();
    }
  }
  
  void addBoid(Boid b){
    boids.add(b);
  }
}
