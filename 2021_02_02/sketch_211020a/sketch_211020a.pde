float angle = 0;
float aVelocity = 0;
float aAcceleration = 0.0035;

void setup() {
  size(800, 800);
}

void draw() {
  background(255);

  translate(width/2, height/2);
  rotate(angle);
  strokeWeight(5);
  stroke(0);
  line(-150, 0, 150, 0);
  noStroke();
  fill(0);
  ellipse(-150, 0, 50, 50);
  ellipse(150, 0, 50, 50);

  aVelocity = aVelocity + aAcceleration;
  aVelocity = constrain(aVelocity, -0.18, 0.18);
  angle = angle + aVelocity;
}

void keyPressed() {
  if (key == '1') {
    aAcceleration = 0.0035;
  }
  if(key == '2'){
    aAcceleration = -0.0035;
  }
}
