PVector position;
PVector speed;
float radius = 15;

void setup(){
  size(800, 800);
  background(255);
  position = new PVector(10, 20);
  speed = new PVector(random(2, 3.5), random(1.5, 4));
}

void draw(){
  background(255);
  
  //position = position + speed
  position.add(speed);
  
  if(position.x > width-radius){
    position.x = width-radius;
    speed.x *= -1;
  }
  if(position.x < radius){
    position.x = radius;
    speed.x *= -1;
  }
  if(position.y > height-radius){
    position.y = height-radius;
    speed.y *= -1;
  }
  if(position.y < radius){
    position.y = radius;
    speed.y *= -1;
  }
  
  fill(0);
  noStroke();
  ellipse(position.x, position.y, radius*2, radius*2);
}
