Mover[] m;
boolean onEarth = false;

void setup() {
  size(800, 800);
  colorMode(HSB, 255);
  background(255, 0, 255);
  m = new Mover[20];
  for (int i=0; i<m.length; i++) {
    m[i] = new Mover(random(1, 5), random(200, width-200), 100);
  }
  textAlign(CENTER, CENTER);
  textSize(15);
}

void draw() {
  background(255, 0, 255);

  PVector gravity = new PVector(0.0, 0.1);

  for (int i=0; i<m.length; i++) {

    if (onEarth == true) {
      PVector g = PVector.mult(gravity, m[i].mass);
      m[i].applyForce(g);
    } else {
      m[i].applyForce(gravity);
    }

    if (m[i].position.y > height/2) {      
      PVector dragForce = m[i].vel.copy();
      float speedPow = dragForce.mag()*dragForce.mag();
      float shapeOrArea = 1;
      float coefficientOfDrag = 0.1;
      float density = 6;
      dragForce.normalize();
      dragForce.mult(-0.5*coefficientOfDrag*speedPow*density);      
      m[i].applyForce(dragForce);
    }

    m[i].update();
    m[i].display();
  }
  
  stroke(0);
  line(0, height/2, width, height/2);
}
