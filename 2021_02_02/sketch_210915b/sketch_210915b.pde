PVector p = new PVector(3, 4);

void setup(){
  size(500, 500);
  
  background(255);
  stroke(0);
  line(0, 0, p.x, p.y);
  println(p.mag());
  
  p.mult(100);
  line(0, 0, p.x, p.y);
  println(p.mag());
  println(p.x);
  println(p.y);
}

void draw(){
}
