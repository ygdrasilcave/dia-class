FlowField f;
ArrayList<Vehicle> v;
boolean showFlowField = true;

void setup() {
  size(800, 800);
  f = new FlowField(25);
  v = new ArrayList<Vehicle>();
}

void draw() {
  background(255);
  f.init();
  if (showFlowField == true) {
    f.display();
  }

  for (Vehicle _v : v) {
    _v.follow(f);
    _v.update();
    _v.display();
  }
}

void mousePressed() {
  v.add(new Vehicle(new PVector(mouseX, mouseY)));
}

void keyPressed() {
  if (key == 's') {
    showFlowField = !showFlowField;
  }
}
