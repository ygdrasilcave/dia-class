float gravity;
float wind;

Pendulum[] p;

void setup() {
  size(800, 800);
  gravity = 0.4;
  wind = 0;

  p = new Pendulum[3];
  for (int i=0; i<p.length; i++) {
    PVector _pivot = new PVector(width/2, 100);
    p[i] = new Pendulum(_pivot, i*100+100, 45);
  }
}

void draw() {
  background(255);
  for (int i=0; i<p.length; i++) {
    p[i].update();
    p[i].display();
  }
}

void mousePressed() {
  for (int i=0; i<p.length; i++) {
    p[i].dragging(mouseX, mouseY);
  }
}

void mouseReleased() {
  for (int i=0; i<p.length; i++) {
    p[i].drag = false;
  }
}

void keyPressed() {
  if (key == '1') {
    wind = -0.2;
  }
  if (key == '2') {
    wind = 0.2;
  }
}

void keyReleased() {
  if (key == '1' || key == '2') {
    wind = 0;
  }
}
