class Mover {
  PVector position;
  PVector vel;
  float h;
  float s;
  float b;
  float radius;
  float eyeX;
  float eyeY;

  Mover() {
    position = new PVector(width/2, height/2);
    vel = new PVector(random(-2, 2), random(-2, 2));
    h = random(255);
    s = 255;
    b = 255;
    radius = random(10, 50);
    eyeX = random(radius*0.1, radius);
    eyeY = random(radius*0.1, radius);
  }

  void update() {
    position.add(vel);

    if (position.x > width) {
      position.x = 0;
    }else if (position.x < 0) {
      position.x = width;
    }
    if (position.y > height) {
      position.y = 0;
    } else if (position.y < 0) {
      position.y = height;
    }
  }

  void display() {
    fill(h, s, b);
    ellipse(position.x, position.y, radius*2, radius*2);
    fill(h, 0, 0);
    float eyeDia = (radius*2)*0.1;
    ellipse(position.x-10, position.y-10, eyeDia, eyeDia);
    ellipse(position.x+10, position.y-10, eyeDia, eyeDia);
  }
}
