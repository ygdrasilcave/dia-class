Mover m1;
Mover m2;

void setup(){
  size(800, 800);
  colorMode(HSB, 255);
  background(255, 0, 255);  
  m1 = new Mover();
  m2 = new Mover();
  
}

void draw(){
  background(255, 0, 255);
  
  m1.update();
  m2.update();
  m1.display();
  m2.display();
}
