float t = 0.0;
float t2 = 0.0;
float y=0;
float y2=0;

float x = 0;

void setup() {
  size(1000, 600);
  background(0);
  
}

void draw() {
  if(x > width){
    background(0);
    x=0;
  }
  
  y = map(noise(t), 0, 1, height, 0);;
  y2 = map(noise(t2), 0, 1, height, 0);
  t+=0.009;
  t2+=0.2;
  
  strokeWeight(3);
  stroke(255);
  point(x, y);
  
  stroke(255, 0, 0);
  point(x, y2);
  
  x++;
}
