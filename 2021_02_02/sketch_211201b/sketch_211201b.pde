Path p;
ArrayList<Vehicle> v;

void setup() {
  size(1000, 500);
  p = new Path();
  v = new ArrayList<Vehicle>();
}

void draw() {
  background(255);

  //p.display();

  for (Vehicle _v : v) {
    _v.follow(p);
    _v.update();
    _v.display();
  }
}

void keyPressed(){
  if(key == ' '){
    p.start = new PVector(0, random(height));
    p.end = new PVector(width, random(height));
  }
}

void mousePressed(){
  v.add(new Vehicle(mouseX, mouseY, random(2, 5), random(0.1, 0.3)));
}
