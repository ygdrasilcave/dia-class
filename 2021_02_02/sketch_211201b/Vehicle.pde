class Vehicle {

  PVector position;
  PVector velocity;
  PVector acceleration;

  float maxSpeed;
  float maxForce;

  float w;
  float h;

  boolean bTarget = false;

  Vehicle(float _x, float _y, float _ms, float _mf) {
    maxSpeed = _ms;
    maxForce = _mf;
    position = new PVector(_x, _y);
    velocity = new PVector(maxSpeed, 0);
    acceleration = new PVector(0, 0);

    w = 50;
    h = 25;
  }

  void follow(Path _path) {
    PVector future = velocity.copy();
    future.normalize();
    future.mult(80);
    PVector futurePosition = PVector.add(position, future);

    PVector A = PVector.sub(_path.end, _path.start);
    PVector B = PVector.sub(futurePosition, _path.start);
    A.normalize();
    float r = B.dot(A);
    A.mult(r);
    PVector normalPoint = PVector.add(_path.start, A);

    float distance = PVector.dist(futurePosition, normalPoint);

    PVector target = null;

    if (distance > _path.radius) {
      bTarget = true;

      PVector dir = PVector.sub(_path.end, _path.start);
      dir.normalize();
      dir.mult(50);
      target = PVector.add(normalPoint, dir);

      PVector desiredVelocity = PVector.sub(target, position);
      desiredVelocity.normalize();
      desiredVelocity.mult(maxSpeed);

      PVector steer = PVector.sub(desiredVelocity, velocity);
      steer.limit(maxForce);

      applyForce(steer);
    } else {
      bTarget = false;
    }

    stroke(0);
    strokeWeight(1);
    line(position.x, position.y, futurePosition.x, futurePosition.y);
    line(futurePosition.x, futurePosition.y, normalPoint.x, normalPoint.y);

    noStroke();
    fill(0, 0, 255);
    ellipse(futurePosition.x, futurePosition.y, 10, 10);
    ellipse(normalPoint.x, normalPoint.y, 10, 10);

    if (target != null) {
      noStroke();
      fill(255, 0, 0);
      ellipse(target.x, target.y, 10, 10);
    }
  }

  void applyForce(PVector _force) {
    acceleration.add(_force);
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(maxSpeed);
    position.add(velocity);
    acceleration.mult(0);

    if (position.x > width) {
      position.x = 0;
      position.y = height/2;
    }
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    noStroke();
    fill(0);
    rect(-w*0.5, -h*0.5, w, h);
    ellipse(w*0.6, -h*0.25, h*0.4, h*0.4);
    ellipse(w*0.6, h*0.25, h*0.4, h*0.4);
    popMatrix();
  }
}
