const int switch_pin = 8;
const int led_pin_00 = 2;
const int led_pin_01 = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(switch_pin, INPUT);
  pinMode(led_pin_00, OUTPUT);
  pinMode(led_pin_01, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(switch_pin) == HIGH){
    digitalWrite(led_pin_00, HIGH);
    digitalWrite(led_pin_01, LOW);
    delay(500);
    digitalWrite(led_pin_00, LOW);
    digitalWrite(led_pin_01, HIGH);
    delay(500);
  }else{
    digitalWrite(led_pin_00, LOW);
    digitalWrite(led_pin_01, LOW);
  }
}
