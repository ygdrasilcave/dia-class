int analogValue = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogValue = analogRead(A0);
  
  //analog out pin (PWM) 3,5,6,9,10,11
  //parameter range 0~255
  /*for (int i = 0; i < 256; i++) {
    analogWrite(3, i);
    delay(10);
  }
  for(int i=255; i>=0; i--){
    analogWrite(3, i);
    delay(10);
  }*/
  
  //analogWrite(3, analogValue/4);

  int convertValue = int(map(analogValue, 0, 1023, 0, 255));

  analogWrite(3, convertValue);
  
  Serial.println(convertValue);
}
