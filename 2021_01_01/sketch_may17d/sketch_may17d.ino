#include <Servo.h>

Servo servoMotor;

int minVal = 1023;
int maxVal = 0;

void setup() {
  // put your setup code here, to run once:
  servoMotor.attach(9);
  Serial.begin(9600);

  while (millis() < 10000) {
    int sensorValue = average_02();
    if (sensorValue < minVal) {
      minVal = sensorValue;
    }
    if(sensorValue > maxVal){
      maxVal = sensorValue;
    }
  }

  Serial.print("minVal: ");
  Serial.println(minVal);
  Serial.print("maxVal: ");
  Serial.println(maxVal);
  Serial.println("_______________________");
}

void loop() {
  // put your main code here, to run repeatedly:

  int sensorVal = average_02();

  int _angle = int(map(sensorVal, minVal, maxVal, 0, 180));
  servoMotor.write(_angle);
  delay(5);

  //Serial.println(sensorVal);
}

unsigned long average_01(int _n) {
  long sum = 0;
  for (int i = 0; i < _n; i++) {
    sum = sum + analogRead(A0);
  }
  return sum / _n;
}

unsigned long average_02() {
  static long avg, sample_1, sample_2, sample_3, sample_4, sample_5;
  sample_5 = sample_4;
  sample_4 = sample_3;
  sample_3 = sample_2;
  sample_2 = sample_1;
  sample_1 = analogRead(A0);
  avg = (sample_1 + sample_2 + sample_3 + sample_4 + sample_5) / 5;
  return avg;
}

#define alpha 0.85
unsigned long average_03() {
  static long avg;
  int sensorVal = analogRead(A0);
  avg = avg * alpha + sensorVal * (1 - alpha);
  return avg;
}
