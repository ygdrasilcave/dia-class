#include <Servo.h>

Servo servoMotor;

void setup() {
  // put your setup code here, to run once:
  servoMotor.attach(9);
}

void loop() {
  // put your main code here, to run repeatedly:
  int _angle = int(map(analogRead(A0), 0, 1023, 0, 180));
  servoMotor.write(_angle);
  delay(5);

}
