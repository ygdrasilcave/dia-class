#include <Servo.h>

Servo servoMotor;

int minVal, maxVal;

void setup() {
  // put your setup code here, to run once:
  servoMotor.attach(9);
  Serial.begin(9600);

  minVal = 130;
  maxVal = 910;
}

void loop() {
  // put your main code here, to run repeatedly:
  
  int sensorVal = average_03();
  
  int _angle = int(map(sensorVal, 130, 910, 0, 180));
  servoMotor.write(_angle);
  delay(5);

  Serial.println(sensorVal);
}

unsigned long average_01(int _n){
  long sum = 0;
  for(int i=0; i<_n; i++){
    sum = sum + analogRead(A0);
  }
  return sum/_n;
}

unsigned long average_02(){
  static long avg, sample_1, sample_2, sample_3, sample_4, sample_5;
  sample_5 = sample_4;
  sample_4 = sample_3;
  sample_3 = sample_2;
  sample_2 = sample_1;
  sample_1 = analogRead(A0);
  avg = (sample_1 + sample_2 + sample_3 + sample_4 + sample_5)/5;
  return avg;
}

#define alpha 0.85
unsigned long average_03(){
  static long avg;
  int sensorVal = analogRead(A0);
  avg = avg*alpha + sensorVal*(1-alpha);
  return avg;
}
