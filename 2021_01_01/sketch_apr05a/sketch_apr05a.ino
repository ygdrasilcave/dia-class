int button = 8;
int state = 0;
int counter = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(button, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  state = digitalRead(button);
  if(state == HIGH){
    counter = counter + 1;
  }
  Serial.println(counter);
}
