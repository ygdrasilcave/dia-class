const int ledPin = 7;
int rawData = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
}

void loop() {
  if(Serial.available() > 0){
    rawData = Serial.read();
    if(rawData == 72){
      digitalWrite(ledPin, HIGH);
    }else if(rawData == 76){
      digitalWrite(ledPin, LOW);
    }
  }
}
