const int ssrPin = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(ssrPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ssrPin, HIGH);
  delay(500);
  digitalWrite(ssrPin, LOW);
  delay(500);
}
