#include <Servo.h>

Servo servoMotor;
int ledPin = 13;

void setup() {
  // put your setup code here, to run once:
  servoMotor.attach(9);
  pinMode(ledPin, OUTPUT);
  delay(1000);
  servoMotor.write(0);
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, HIGH);
  for(int i=0; i<180; i++){
    servoMotor.write(i);
    delay(int(map(analogRead(A0), 0, 1023, 5, 200)));
  }

  digitalWrite(ledPin, LOW);
  for(int i=180; i>0; i--){
    servoMotor.write(i);
    delay(int(map(analogRead(A0), 0, 1023, 5, 200)));
  }
}
