const int button_L = 9;
const int button_R = 10;

int currState_L = 0;
int prevState_L = 0;
int currState_R = 0;
int prevState_R = 0;

int counter = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(button_L, INPUT);
  pinMode(button_R, INPUT);

  for (int i = 0; i < 7; i++) {
    pinMode(i + 2, OUTPUT);
  }
  updateLEDs();
}

void loop() {
  // put your main code here, to run repeatedly:
  currState_L = digitalRead(button_L);
  currState_R = digitalRead(button_R);

  if (detectRisingEdge_L() == 1) {
    counter--;
    if (counter < -3) {
      counter = -3;
    }
    updateLEDs();
  }
  if (detectRisingEdge_R() == 1) {
    counter++;
    if (counter > 3) {
      counter = 3;
    }
    updateLEDs();
  }

  prevState_L = currState_L;
  prevState_R = currState_R;

  Serial.println(counter);
}

int detectRisingEdge_L() {
  int result = 0;
  if (currState_L == HIGH && prevState_L == LOW) {
    result = 1;
  } else {
    result = 0;
  }
  return result;
}

int detectRisingEdge_R() {
  int result = 0;
  if (currState_R == HIGH && prevState_R == LOW) {
    result = 1;
  } else {
    result = 0;
  }
  return result;
}

void updateLEDs() {
  for (int i = 0; i < 7; i++) {
    digitalWrite(i + 2, LOW);
  }
  digitalWrite(5 + counter, HIGH);
}
