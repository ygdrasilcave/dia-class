#include <Servo.h>

Servo servoMotor;

void setup() {
  // put your setup code here, to run once:
  servoMotor.attach(9);
  servoMotor.write(0);
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  servoMotor.write(0);
  delay(1000);
  servoMotor.write(90);
  delay(1000);
  servoMotor.write(180);
  delay(1000);
  servoMotor.write(90);
  delay(1000);
}
