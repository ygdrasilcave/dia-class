const int led_pin = 7;
const int button_pin = 8;

void setup() {
  // put your setup code here, to run once:
  pinMode(led_pin, OUTPUT);
  pinMode(button_pin, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:
  int outputValue = digitalRead(button_pin);

  if (outputValue == 0) {
    digitalWrite(led_pin, HIGH);
    delay(100);
    digitalWrite(led_pin, LOW);
    delay(100);
  } else if (outputValue == 1) {
    digitalWrite(led_pin, HIGH);
  }
}
