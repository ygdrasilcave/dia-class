const int switch_pin = 8;
const int led_pin_00 = 2;
const int led_pin_01 = 3;

int bValue = 0;
boolean bToggle = false;

int interval = 1000;
unsigned long currentTime = 0;
boolean ledValue_00 = true;
boolean ledValue_01 = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(switch_pin, INPUT);
  pinMode(led_pin_00, OUTPUT);
  pinMode(led_pin_01, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(switch_pin) == HIGH && bToggle == false) {
    if (bValue == 0) {
      bValue = 1;
    } else if (bValue == 1) {
      bValue = 0;
    }
    bToggle = true;
    currentTime = millis();
  } else if (digitalRead(switch_pin) == LOW && bToggle == true) {
    bToggle = false;
  }

  if (bValue == 1) {
    if (currentTime + interval < millis()) {
      ledValue_00 = !ledValue_00;
      ledValue_01 = !ledValue_01;
      currentTime = millis();
    }
    digitalWrite(led_pin_00, ledValue_00);
    digitalWrite(led_pin_01, ledValue_01);
  } else {
    digitalWrite(led_pin_00, LOW);
    digitalWrite(led_pin_01, LOW);
  }
}
