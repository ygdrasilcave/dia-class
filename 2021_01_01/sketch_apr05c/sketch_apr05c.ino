const int button_00 = 8;
const int button_01 = 9;
int prevState_00 = 0;
int currState_00 = 0;
int prevState_01 = 0;
int currState_01 = 0;

int counter = 0;

const int led_00 = 2;
const int led_01 = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(button_00, INPUT);
  pinMode(button_01, INPUT);
  pinMode(led_00, OUTPUT);
  pinMode(led_01, OUTPUT);
  Serial.begin(9600);

  blinkLEDs();
}

void loop() {
  // put your main code here, to run repeatedly:
  currState_00 = digitalRead(button_00);
  currState_01 = digitalRead(button_01);

  int rightButton = detectRisingEdge_R();
  if(rightButton == 1){
    counter++;
  }
  if(detectRisingEdge_L() == 1){
    counter--;
  }
  Serial.println(counter);

  prevState_00 = currState_00;
  prevState_01 = currState_01;
}

int detectRisingEdge_R(){
  int result = 0;
  if(currState_00 == HIGH && prevState_00 == LOW){
    result = 1;
  }else{
    result = 0;
  }
  return result;
}

int detectRisingEdge_L(){
  int result = 0;
  if(currState_01 == HIGH && prevState_01 == LOW){
    result = 1;
  }else{
    result = 0;
  }
  return result;
}

void blinkLEDs(){
  digitalWrite(led_00, HIGH);
  digitalWrite(led_01, LOW);
  delay(100);
  digitalWrite(led_00, LOW);
  digitalWrite(led_01, HIGH);
  delay(100);

  digitalWrite(led_00, HIGH);
  digitalWrite(led_01, LOW);
  delay(100);
  digitalWrite(led_00, LOW);
  digitalWrite(led_01, HIGH);
  delay(100);

  digitalWrite(led_00, HIGH);
  digitalWrite(led_01, LOW);
  delay(100);
  digitalWrite(led_00, LOW);
  digitalWrite(led_01, HIGH);
  delay(100);
}
