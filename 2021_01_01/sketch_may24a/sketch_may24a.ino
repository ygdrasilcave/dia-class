int inPin_01_01 = 4;
int inPin_01_02 = 5;
int enPin_01 = 9;

int mSpeed_01 = 0;

int potPin = A0;

unsigned long currTime = 0;
boolean dir = true;

void setup() {
  // put your setup code here, to run once:
  pinMode(inPin_01_01, OUTPUT);
  pinMode(inPin_01_02, OUTPUT);
  currTime = millis();
}

void loop() {
  // put your main code here, to run repeatedly:
  mSpeed_01 = analogRead(potPin);
  analogWrite(enPin_01, map(mSpeed_01, 0, 1023, 0, 255));

  if (currTime + 2000 < millis()) {
    dir = !dir;
    motorDirChange(dir);
    currTime = millis();
  }
}


void motorDirChange(boolean _dir) {
  if (_dir == HIGH) {
    digitalWrite(inPin_01_01, HIGH);
    digitalWrite(inPin_01_02, LOW);
  } else if (_dir == LOW) {
    digitalWrite(inPin_01_01, LOW);
    digitalWrite(inPin_01_02, HIGH);
  }
}
