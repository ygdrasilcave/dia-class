import processing.serial.*;
Serial myPort;


void setup() {
  size(800, 800);
  println(Serial.list());
  //myPort = new Serial(this, "COM5", 9600);
  myPort = new Serial(this, Serial.list()[1], 9600);

  textAlign(CENTER, CENTER);
  textSize(60);
}

void draw() {
  //background(8bit red, 8bit green, 8bit blue);
  background(0);

  if (mousePressed == true) {
    if (mouseX > width/2-100 && mouseX < width/2+100 && mouseY > height/2-100 && mouseY < height/2+100) {
      on();
    } else {
      off();
    }
  } else {
    off();
  }
}

void on() {
  stroke(255);  
  fill(255, 0, 0);
  rect(width/2-100, height/2-100, 200, 200);

  fill(0);
  noStroke();
  text("ON", width/2, height/2);
}

void off() {
  stroke(255);  
  fill(0, 255, 0);
  rect(width/2-100, height/2-100, 200, 200);

  fill(0);
  noStroke();
  text("OFF", width/2, height/2);
}
