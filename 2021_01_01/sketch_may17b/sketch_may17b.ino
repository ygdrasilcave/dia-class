#include <Servo.h>

Servo servoMotor;

int minVal, maxVal;

void setup() {
  // put your setup code here, to run once:
  servoMotor.attach(9);
  Serial.begin(9600);

  minVal = 130;
  maxVal = 910;
}

void loop() {
  // put your main code here, to run repeatedly:
  
  int sensorVal = analogRead(A0);
  int _angle = int(map(sensorVal, 130, 910, 0, 180));
  servoMotor.write(_angle);
  delay(5);

  Serial.println(sensorVal);
}
