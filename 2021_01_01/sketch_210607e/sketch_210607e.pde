import processing.serial.*;
Serial myPort;

float t = 0;

void setup() {
  size(800, 800);
  println(Serial.list());
  //myPort = new Serial(this, "COM5", 9600);
  myPort = new Serial(this, Serial.list()[1], 9600);

  textAlign(CENTER, CENTER);
  textSize(60);
}

void draw() {
  //background(8bit red, 8bit green, 8bit blue);
  background(0);
  
  int data0 = int(map(mouseX, 0, width, 0, 255));
  int data1 = int(map(mouseY, 0, width, 0, 255));
  //int data2 = int(noise(t)*255);
  int data2 = int(map(sin(t), -1, 1, 0, 1)*255);
  t += 0.0365;

  stroke(255);
  strokeWeight(10);
  float degree = map(mouseX, 0, width, -180, 0);
  float theta = radians(degree);
  float data3 = degree*-1;
  println(data3);
  float x = width/2 + cos(theta)*300;
  float y = height + sin(theta)*300;
  line(width/2, height, x, y);
  
  myPort.write(data0 +","+ data1 +","+ data2 +","+ data3 + "\n");
}
