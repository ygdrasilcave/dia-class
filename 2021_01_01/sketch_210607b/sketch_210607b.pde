import processing.serial.*;
Serial myPort;

float[] value;

void setup() {
  size(800, 800);
  println(Serial.list());
  //myPort = new Serial(this, "COM5", 9600);
  myPort = new Serial(this, Serial.list()[1], 9600);
  myPort.bufferUntil('\n');

  textAlign(CENTER, CENTER);
  textSize(60);

  value = new float[6];
  for (int i=0; i<6; i++) {
    value[i] = 0;
  }
}

void draw() {
  //background(8bit red, 8bit green, 8bit blue);
  background(255, 255, 255);

  fill(0, map(value[2], 0, 1023, 0, 255));
  stroke(0);
  float w = map(value[0], 0, 1023, 0, width);
  float h = map(value[1], 0, 1023, 0, width);
  ellipse(width/2, height/2, w, h);

  fill(0);
  noStroke();
  text("sensor value", width/2, 100);
  for (int i=0; i<6; i++) {
    text(value[i], width/2, 100+(80*(i+1)));
  }
}

void serialEvent(Serial myPort) {
  String rawData = myPort.readStringUntil('\n');
  //println(rawData);

  if (rawData != null) {
    rawData = trim(rawData);
    String[] data = rawData.split(",");
    //println(data[0]);
    if (data[0].equals("s")==true && data.length == 7) {
      for (int i=0; i<6; i++) {
        value[i] = float(trim(data[i+1]));
      }
    }
  }
}
