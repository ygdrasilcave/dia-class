const int button_pin = 8;
const int led_pin_00 = 2;
const int led_pin_01 = 3;

int old_val = 0;
int curr_val = 0;
int state = 0;

boolean ledValue_00 = true;
boolean ledValue_01 = false;

unsigned long currentTime = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(button_pin, INPUT);
  pinMode(led_pin_00, OUTPUT);
  pinMode(led_pin_01, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  curr_val = digitalRead(button_pin);
  if ((curr_val == HIGH) && (old_val == LOW)) {
    state = 1 - state;
    currentTime = millis();
    ledValue_00 = true;
    ledValue_01 = false;
    delay(10);
  }

  if (state == 1) {
    if (currentTime + 2000 < millis()) {
      ledValue_00 = !ledValue_00;
      ledValue_01 = !ledValue_01;
      currentTime = millis();
    }
    digitalWrite(led_pin_00, ledValue_00);
    digitalWrite(led_pin_01, ledValue_01);
  } else {
    digitalWrite(led_pin_00, LOW);
    digitalWrite(led_pin_01, LOW);
  }

  old_val = curr_val;
}
