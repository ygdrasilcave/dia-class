import processing.serial.*;
Serial myPort;

float data = 0;

void setup(){
  size(800, 800);
  println(Serial.list());
  //myPort = new Serial(this, "COM5", 9600);
  myPort = new Serial(this, Serial.list()[1], 9600);

  textAlign(CENTER, CENTER);
  textSize(60);
}

void draw(){
  //background(8bit red, 8bit green, 8bit blue);
  background(255, 255, 255);
  
  noFill();
  stroke(0);
  //float w = map(data, 0, 1023, 0, width);
  //ellipse(width/2, height/2, w, w);
  
  fill(0);
  noStroke();
  text("sensor value", width/2, height/2);
  text(data, width/2, height/2+80);
}

void serialEvent(Serial myPort){
  String rawData = myPort.readStringUntil('\n');
  println(rawData);
  
  if(rawData != null){
    data = float(trim(rawData));
  }
}
