int ledPins[6] = {2, 3, 4, 7, 8, 9};
int wait = 100;

void setup() {
  // put your setup code here, to run once:
  for(int i=0; i<6; i++){
    pinMode(ledPins[i], OUTPUT);
    digitalWrite(ledPins[i], LOW);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  wait = analogRead(A0);
  wait = map(wait, 0, 1023, 50, 1000);
  
  for(int i=0; i<6; i++){
    digitalWrite(ledPins[i], HIGH);
    delay(wait);
    digitalWrite(ledPins[i], LOW);
  }
  for(int i=4; i>0; i--){
    digitalWrite(ledPins[i], HIGH);
    delay(wait);
    digitalWrite(ledPins[i], LOW);
  }

}
