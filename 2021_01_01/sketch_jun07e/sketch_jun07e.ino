#include <Servo.h>

Servo myServo;

const int ledPin0 = 3;
const int ledPin1 = 5;
const int ledPin2 = 6;

int index = 0;
const int dataNum = 4;
int rawData[dataNum];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  for (int i = 0; i < dataNum; i++) {
    rawData[i] = 0;
  }

  myServo.attach(9);
}

void loop() {
  analogWrite(ledPin0, rawData[0]);
  analogWrite(ledPin1, rawData[1]);
  analogWrite(ledPin2, rawData[2]);
  myServo.write(rawData[3]);

  if (Serial.available() > 0) {
    if (index == 0) {
      for (index; index < dataNum; index++) {
        rawData[index] = Serial.parseInt();
        if (index == 3) {
          rawData[index] = constrain(rawData[index], 0, 180);
        } else {
          rawData[index] = constrain(rawData[index], 0, 255);
        }
      }
    }
    if (Serial.read() == '\n' && index == dataNum) {
      index = 0;
      delay(10);
    }
  }
}
