void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  int sensorVal = analogRead(A0);

  int convertVal = int(map(sensorVal, 10, 960, 0, 255));

  convertVal = constrain(convertVal, 0, 255);

  analogWrite(3, convertVal);

  Serial.println(convertVal);

  /*for (int i = 0; i < 512; i++) {
    analogWrite(3, i);
    delay(50);
    if(i == 255){
      Serial.println(i);
    }
    if(i == 0){
      Serial.println(i);
    }
    if(i == 511){
      Serial.println(i);
    }
  }*/
  
}
