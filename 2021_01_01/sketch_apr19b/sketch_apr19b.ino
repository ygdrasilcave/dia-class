const int ledPin = 5;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  int analogValue = analogRead(A0)/4;
  
  analogWrite(ledPin, analogValue);
}
