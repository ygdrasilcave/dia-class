int button = 8;
int prevState = 0;
int currState = 0;
int counter = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(button, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  currState = digitalRead(button);
  
  //if(detectRisingEdge() == 1){
  //  counter++;
  //}
  if(detectFallingEdge() == 1){
    counter++;
  }
  Serial.println(counter);

  prevState = currState;
}

int detectRisingEdge(){
  int result = 0;  
  if(currState == HIGH && prevState == LOW){
    result = 1;    
  }else{
    result = 0;
  }
  return result;
}

int detectFallingEdge(){
  int result = 0;  
  if(currState == LOW && prevState == HIGH){
    result = 1;    
  }else{
    result = 0;
  }
  return result;
}
