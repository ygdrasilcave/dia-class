int ledPins[6] = {2, 3, 4, 7, 8, 9};
int sensorPin = A0;
int sensorValue = 0;
int ledn = 0;
int ledLevel = 0;

void setup() {
  // put your setup code here, to run once:
  for (int i = 0; i < 6; i++) {
    pinMode(ledPins[i], OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  sensorValue = analogRead(sensorPin); // 0-1023

  /*if (sensorValue < 170) {
    ledn = 0;
    } else if (sensorValue >= 170 && sensorValue < 170*2) {
    ledn = 1;
    } else if (sensorValue >= 170*2 && sensorValue < 170*3) {
    ledn = 2;
    } else if (sensorValue >= 170*3 && sensorValue < 170*4) {
    ledn = 3;
    } else if (sensorValue >= 170*4 && sensorValue < 170*5) {
    ledn = 4;
    } else {
    ledn = 5;
    }

    for (int i = 0; i < 6; i++) {
    if (i <= ledn) {
      digitalWrite(ledPins[i], HIGH);
    } else {
      digitalWrite(ledPins[i], LOW);
    }
    }*/

  ledLevel = int(map(sensorValue, 0, 1023, 0, 6));
  for (int i = 0; i < 6; i++) {
    if (i < ledLevel) {
      digitalWrite(ledPins[i], HIGH);
    } else {
      digitalWrite(ledPins[i], LOW);
    }
  }
}
