import processing.serial.*;
Serial myPort;

float x = 0;
float y = 0;
float speedX = 6;
float speedY = 8;
float dirX = 1;
float dirY = 1;

boolean status = true;


void setup() {
  size(800, 800);
  println(Serial.list());
  //myPort = new Serial(this, "COM5", 9600);
  myPort = new Serial(this, Serial.list()[1], 9600);

  textAlign(CENTER, CENTER);
  textSize(60);
}

void draw() {
  //background(8bit red, 8bit green, 8bit blue);
  background(0);

  x += speedX*dirX;
  y += speedY*dirY;
  fill(255);
  ellipse(x, y, 50, 50);
  
  if(x > width-25){
    x = width-25;
    speedX = random(10, 25);
    dirX = dirX*-1;
    status = !status;
  }
  if(x < 25){
    x = 25;
    speedX = random(10, 25);
    dirX = dirX*-1;
    status = !status;
  }
  
  if(y > height-25){
    y = height-25;
    speedY = random(10, 25);
    dirY = dirY*-1;
    status = !status;
  }
  if(y < 25){
    y = 25;
    speedY = random(10, 25);
    dirY = dirY*-1;
    status = !status;
  }
  
  if(status == true){
    myPort.write("H");
  }else{
    myPort.write("L");
  }
}
